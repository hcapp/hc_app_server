package com.scansee.myaccount.dao;

import com.google.inject.ImplementedBy;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AuthenticateUser;
import com.scansee.common.pojos.MyAccountDetails;
import com.scansee.common.pojos.UserPreference;

/**
 * The MyAccountDAO Interface. {@link ImplementedBy} {@link MyAccountDAOImpl}
 * 
 * @author dileepa_cc
 */
public interface MyAccountDAO
{
	/**
	 * The DAO method for fetching account details from the Database.
	 * 
	 * @param userId
	 *            for which user details for fetching data.
	 * @return The Account Information.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	MyAccountDetails fetchAccountDetails(int userId) throws ScanSeeException;

	/**
	 * The DAO method fetches user details for sending password to user email
	 * id.
	 * 
	 * @param userId
	 *            for which user details to be fetched.
	 * @return User Details Object.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	AuthenticateUser fetchUserDetailsforForgotPassword(Integer userId) throws ScanSeeException;

	/**
	 * The DAO method for fetching user preference Info.
	 * 
	 * @param userId
	 *            for which user payment preferences to be fetched.
	 * @return UserPreferences object.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	UserPreference getUserPaymentPreferecesInfo(Integer userId) throws ScanSeeException;

	/**
	 * The DAO method for storing user preference Info in the DataBase.
	 * 
	 * @param userPreference
	 *            the userPreference in the request.
	 * @return Status SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String saveUserPaymentPreferenceDetails(UserPreference userPreference) throws ScanSeeException;

}
