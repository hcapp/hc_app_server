package com.scansee.myaccount.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AuthenticateUser;
import com.scansee.common.pojos.MyAccountDetails;
import com.scansee.common.pojos.UserPreference;
import com.scansee.common.util.Utility;
import com.scansee.firstuse.query.FirstUseQueries;

/**
 * The class for fetching information required for My Account Module.
 * 
 * @author dileepa_cc
 */
public class MyAccountDAOImpl implements MyAccountDAO
{

	/**
	 * Getting the Logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(MyAccountDAOImpl.class);

	/**
	 * for Jdbc connection.
	 */
	private JdbcTemplate jdbcTemplate;

	/**
	 * To call stored procedue.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To get the datasource from xml..
	 * 
	 * @param dataSource
	 *            input parameter
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * The DAO method for fetching account details from the Database.
	 * 
	 * @param userId
	 *            for which user details for fetching data.
	 * @return The Account Information.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MyAccountDetails fetchAccountDetails(int userId) throws ScanSeeException
	{
		final String methodName = "fetchAccountDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName + "User Id:" + userId);
		MyAccountDetails myAccountDetail = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MyAccount");
			simpleJdbcCall.returningResultSet("myAccountDetails", new BeanPropertyRowMapper<MyAccountDetails>(MyAccountDetails.class));

			final MapSqlParameterSource myAccountParameters = new MapSqlParameterSource();
			myAccountParameters.addValue(ApplicationConstants.USERID, userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(myAccountParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final List<MyAccountDetails> myAccountDetails = (List<MyAccountDetails>) resultFromProcedure.get("myAccountDetails");
					if (null != myAccountDetails && !myAccountDetails.isEmpty())
					{
						myAccountDetail = myAccountDetails.get(0);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

					LOG.error("Error occurred in usp_MyAccount Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return myAccountDetail;
	}

	/**
	 * The DAO method fetches user details for sending password to user email
	 * id.
	 * 
	 * @param userId
	 *            for which user details to be fetched.
	 * @return User Details Object.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	@Override
	public AuthenticateUser fetchUserDetailsforForgotPassword(Integer userId) throws ScanSeeException
	{
		final String methodName = "fetchUserDetailsforForgotPassword";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		AuthenticateUser authenticateUser = null;
		try
		{
			authenticateUser = this.jdbcTemplate.queryForObject(FirstUseQueries.AUHTENTICATEUSERMAILQUERY, new Object[] { userId },
					new RowMapper<AuthenticateUser>() {
						public AuthenticateUser mapRow(ResultSet rs, int rowNum) throws SQLException
						{
							final AuthenticateUser authenticateUser = new AuthenticateUser();
							authenticateUser.setEmail(rs.getString("Email"));
							authenticateUser.setUserName(rs.getString("UserName"));
							authenticateUser.setPassword(rs.getString("Password"));
							authenticateUser.setUserId(rs.getInt(ApplicationConstants.USERID));
							return authenticateUser;
						}
					});
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return authenticateUser;
	}

	/**
	 * The DAO method for fetching user preference Info.
	 * 
	 * @param userId
	 *            for which user payment preferences to be fetched.
	 * @return UserPreferences object.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public UserPreference getUserPaymentPreferecesInfo(Integer userId) throws ScanSeeException
	{

		final String methodName = "getUserPaymentPreferecesInfo in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		UserPreference userPreference = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserPaymentPreferecesDisplay");
			simpleJdbcCall.returningResultSet("UserPreferenceDetails", new BeanPropertyRowMapper<UserPreference>(UserPreference.class));

			final SqlParameterSource fetchUserPreferenceParameters = new MapSqlParameterSource().addValue("UserID", userId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchUserPreferenceParameters);

			final List<UserPreference> userPreferenceInfolst = (List<UserPreference>) resultFromProcedure.get("UserPreferenceDetails");

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					if (userPreferenceInfolst != null && !userPreferenceInfolst.isEmpty())
					{
						userPreference = new UserPreference();
						userPreference = userPreferenceInfolst.get(0);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_UserPaymentPreferecesDisplay Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName + "in DAO layer");
		return userPreference;
	}

	/**
	 * The DAO method for storing user preference Info in the DataBase.
	 * 
	 * @param userPreference
	 *            the userPreference in the request.
	 * @return Status SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	@Override
	public String saveUserPaymentPreferenceDetails(UserPreference userPreference) throws ScanSeeException
	{
		final String methodName = "saveUserPaymentPreference";
		LOG.info(ApplicationConstants.METHODSTART + methodName + "in DAO layer");
		Integer fromProc = 0;
		Map<String, Object> resultFromProcedure = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_SaveUserPaymentPreferences");
			final MapSqlParameterSource userPreferenceParameters = new MapSqlParameterSource();
			userPreferenceParameters.addValue("UserID", userPreference.getUserID());
			userPreferenceParameters.addValue("paymentIntervalID", userPreference.getSelPaymentIntervalID());
			userPreferenceParameters.addValue("paymentTypeID", userPreference.getSelPayoutTypeID());
			userPreferenceParameters.addValue("payoutTypeID", userPreference.getSelPayoutTypeID());
			userPreferenceParameters.addValue("defaultPayout", userPreference.getDefaultPayout());
			/*
			 * userPreferenceParameters.addValue("useDefaultAddress",
			 * userPreference.getUseDefaultAddress());
			 * userPreferenceParameters.addValue("payAddress1",
			 * userPreference.getPayAddress1());
			 * userPreferenceParameters.addValue("payAddress2",
			 * userPreference.getPayAddress2());
			 * userPreferenceParameters.addValue("payCity",
			 * userPreference.getPayCity());
			 * userPreferenceParameters.addValue("payState",
			 * userPreference.getPayState());
			 * userPreferenceParameters.addValue("payPostalCode",
			 * userPreference.getPayPostalCode());
			 */
			userPreferenceParameters.addValue("provisionalPayoutActive", userPreference.getProvisionalPayoutActive());
			if (null != userPreference.getProvisionalPayoutStartDate())
			{
				userPreferenceParameters.addValue("provisionalPayoutStartDate", userPreference.getProvisionalPayoutStartDate());
			}
			else
			{
				userPreferenceParameters.addValue("provisionalPayoutStartDate", null);
			}
			if (null != userPreference.getProvisionalPayoutEndDate())
			{
				userPreferenceParameters.addValue("provisionalPayoutEndDate", userPreference.getProvisionalPayoutEndDate());
			}
			else
			{
				userPreferenceParameters.addValue("provisionalPayoutEndDate", null);

			}
			userPreferenceParameters.addValue("DateModified", Utility.getFormattedDate());
			userPreferenceParameters.registerSqlType("Status", Types.INTEGER);
			userPreferenceParameters.registerSqlType("ErrorNumber", Types.INTEGER);
			userPreferenceParameters.registerSqlType("ErrorMessage", Types.VARCHAR);
			resultFromProcedure = simpleJdbcCall.execute(userPreferenceParameters);
			/**
			 * For getting response from stored procedure ,sp return Result as
			 * response success as 0 failure as 1 it will return list resultset
			 * so casting into map and getting Result param value
			 */
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_UserPaymentPreferecesDisplay Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in saveUserPaymentPreference", ApplicationConstants.DATAACCESSEXCEPTIONCODE, exception);
			throw new ScanSeeException(exception);
		}
		catch (ParseException exception)
		{
			LOG.error("Exception occurred in saveUserPaymentPreference", exception);
			throw new ScanSeeException(exception);
		}
		return response;
	}

}
