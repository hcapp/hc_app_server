package com.scansee.myaccount.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.scansee.common.constants.ScanSeeURLPath;

/**
 * The MyAccountRestEasy Interface. {@link ImplementedBy}
 * {@link MyAccountRestEasyImpl}
 * 
 * @author dileepa_cc
 */
@Path(ScanSeeURLPath.MYACCOUNTBASEURL)
public interface MyAccountRestEasy
{

	/**
	 * This is a RestEasy method to fetch user account details. Method Type:POST
	 * 
	 * @param xml
	 *            containing user details for fetching data.
	 * @return the XML contains user acount details in the reponse.
	 */
	@POST
	@Path(ScanSeeURLPath.MYACCOUNTDETAILS)
	@Produces("text/xml")
	@Consumes("text/xml")
	String fetchAccountDetails(String xml);

	/**
	 * This is a RestEasy method for retrieving password.Method Type:GET.
	 * 
	 * @param userId
	 *            for which password to be sent.
	 * @return the XML containing Success or Failure in response.
	 */

	@GET
	@Path(ScanSeeURLPath.FORGOTPASSWORD)
	@Produces("text/xml;charset=UTF-8")
	String forgotPassword(@QueryParam("userId") Integer userId);

	/**
	 * The RestEasy method for fetching user payment preferences
	 * Information.Method Type:GET.
	 * 
	 * @param userId
	 *            for which user payment preferences information to be fetched.
	 * @return the XML containing user preferences information in the response.
	 */

	@GET
	@Path(ScanSeeURLPath.FETCHUSERPAYMENTINFO)
	@Produces("text/xml;charset=UTF-8")
	String fetchUserPaymentInfo(@QueryParam("userId") Integer userId);

	/**
	 * The RestEasy method for storing user payment preference Info.Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            containg the details of user preferences.
	 * @return the XML containing Succes or Failure code in the response.
	 */

	@POST
	@Path(ScanSeeURLPath.SAVEUSERPAYMENTPREFERENCE)
	@Produces("text/xml")
	@Consumes("text/xml")
	String saveUserPaymentPreference(String xml);

}
