package com.scansee.myaccount.controller;

import static com.scansee.common.util.Utility.formResponseXml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.servicefactory.ServiceFactory;
import com.scansee.myaccount.service.MyAccountService;

/**
 * The class accepts the web service requests for MyAccount Module.
 * 
 * @author dileepa_cc
 */
public class MyAccountRestEasyImpl implements MyAccountRestEasy
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(MyAccountRestEasyImpl.class);

	/**
	 * The variable for return XML.
	 */

	private String responseXML = "";

	/**
	 * The default constructor.
	 */
	public MyAccountRestEasyImpl()
	{

	}

	/**
	 * This is a RestEasy method to fetch user account details. Method Type:POST
	 * 
	 * @param xml
	 *            containing user details for fetching data.
	 * @return the XML contains user acount details in the reponse.
	 */
	@Override
	public String fetchAccountDetails(String xml)
	{
		final String methodName = "fetchAccountDetails in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Xml:" + xml);
		}
		final MyAccountService myAccountService = ServiceFactory.getMyAccountService();
		try
		{
			responseXML = myAccountService.fetchAccountDetails(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{

			LOG.debug("Returned Response: in fetchAccountDetails" + responseXML);

		}
		return responseXML;
	}

	/**
	 * This is a RestEasy method for retrieving password.Method Type:GET.
	 * 
	 * @param userId
	 *            for which password to be sent.
	 * @return the XML containing Success or Failure in response.
	 */
	@Override
	public String forgotPassword(Integer userId)
	{
		final String methodName = "forgotPassword in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Data: userId:" + userId);
		}
		String response = null;
		final MyAccountService myAccountService = ServiceFactory.getMyAccountService();
		try
		{
			LOG.info("User with user id {}  has request for  password", userId);
			response = myAccountService.forgotPassword(userId);
		}

		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response:" + responseXML);
		}
		return response;
	}

	/**
	 * The RestEasy method for fetching user payment preferences
	 * Information.Method Type:GET.
	 * 
	 * @param userId
	 *            for which user payment preferences information to be fetched.
	 * @return the XML containing user preferences information in the response.
	 */

	@Override
	public String fetchUserPaymentInfo(Integer userId)
	{
		final String methodName = "fetchUserPaymentInfo in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Requested UserID:" + userId);
		}
		String response = null;
		final MyAccountService myAccountService = ServiceFactory.getMyAccountService();
		try
		{
			response = myAccountService.fetchUserPaymentInfo(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response:" + responseXML);
		}
		return response;
	}

	/**
	 * The RestEasy method for storing user payment preference Info.Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            containg the details of user preferences.
	 * @return the XML containing Succes or Failure code in the response.
	 */

	@Override
	public String saveUserPaymentPreference(String xml)
	{
		final String methodName = "saveUserPaymentPreference in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final MyAccountService myAccountService = ServiceFactory.getMyAccountService();
		try
		{
			response = myAccountService.saveUserPaymentPreference(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned " + response);
		}
		return response;
	}

}
