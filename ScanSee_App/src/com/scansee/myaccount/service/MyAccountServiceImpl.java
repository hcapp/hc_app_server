package com.scansee.myaccount.service;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.email.EmailComponent;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.helper.BaseDAO;
import com.scansee.common.helper.XstreamParserHelper;
import com.scansee.common.pojos.AppConfiguration;
import com.scansee.common.pojos.AuthenticateUser;
import com.scansee.common.pojos.MyAccountDetails;
import com.scansee.common.pojos.PaymentInterval;
import com.scansee.common.pojos.PaymentType;
import com.scansee.common.pojos.PayoutType;
import com.scansee.common.pojos.UserPreference;
import com.scansee.common.pojos.UserPreferenceDetails;
import com.scansee.common.util.EncryptDecryptPwd;
import com.scansee.common.util.Utility;
import com.scansee.firstuse.dao.FirstUseDAO;
import com.scansee.myaccount.dao.MyAccountDAO;
import com.scansee.shoppinglist.dao.ShoppingListDAO;

/**
 * This Class is used for fetching account details and forgot password.
 * functionality
 * 
 * @author dileepa_cc
 */
public class MyAccountServiceImpl implements MyAccountService
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(MyAccountServiceImpl.class);

	/**
	 * Instance variable for BaseDAO instance.
	 */
	private BaseDAO baseDao;

	/**
	 * Instance variable for MyAccountDAO.
	 */
	private MyAccountDAO myAccountDao;

	/**
	 * Instance variable for FirstUseDAO.
	 */
	private FirstUseDAO firstUseDao;

	/**
	 * Instance variable for Shopping list DAO instance.
	 */

	private ShoppingListDAO shoppingListDao;

	/**
	 * sets the ShoppingListDAO DAO.
	 * 
	 * @param shoppingListDao
	 *            The instance for ShoppingListDAO
	 */
	public void setShoppingListDao(ShoppingListDAO shoppingListDao)
	{
		this.shoppingListDao = shoppingListDao;
	}

	/**
	 * sets the BaseDAO DAO.
	 * 
	 * @param baseDao
	 *            The instance for BaseDAO
	 */
	public void setBaseDao(BaseDAO baseDao)
	{
		this.baseDao = baseDao;
	}

	/**
	 * sets the FirstUse DAO.
	 * 
	 * @param firstUseDao
	 *            The instance for FirstUseDAO
	 */
	public void setFirstUseDao(FirstUseDAO firstUseDao)
	{
		this.firstUseDao = firstUseDao;
	}

	/**
	 * sets the My Account DAO.
	 * 
	 * @param myAccountDao
	 *            The instance for MyAccountDAO
	 */
	public void setMyAccountDao(MyAccountDAO myAccountDao)
	{
		this.myAccountDao = myAccountDao;
	}

	/**
	 * The Service Method for fetching user account details.This method checks
	 * for mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method.
	 * 
	 * @param xml
	 *            containing user details for fetching data.
	 * @return User Account Details object.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	@Override
	public String fetchAccountDetails(String xml) throws ScanSeeException
	{
		int couponsCur;
		int rebateCur;
		int fieldAgentCur;
		int couponsTot;
		int rebateTot;
		int fieldAgentTot;
		final String methodName = "fetchAccountDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AuthenticateUser authenticateUser = (AuthenticateUser) streamHelper.parseXmlToObject(xml);

		String response = null;
		String result = null;
		MyAccountDetails myAccountDetails = null;

		if (null == authenticateUser.getUserId())
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}

		result = firstUseDao.isValidUser(null, authenticateUser.getPassword(), authenticateUser.getUserId());
		if (result.equals(ApplicationConstants.FAILURE))
		{
			response = Utility.formResponseXml(ApplicationConstants.INVALIDUSERNAMEORPASSWORD, ApplicationConstants.INVALIDUSERNAMEORPASSWORDTEXT);

		}
		else
		{
			LOG.info("User is authenticated");
			myAccountDetails = myAccountDao.fetchAccountDetails(authenticateUser.getUserId());
			if (null != myAccountDetails)
			{

				if (!myAccountDetails.getCouponsCur().equals(ApplicationConstants.NOTAPPLICABLE))
				{
					couponsCur = Integer.valueOf(myAccountDetails.getCouponsCur());
				}
				else
				{

					couponsCur = 0;
				}
				if (!myAccountDetails.getRebateCur().equals(ApplicationConstants.NOTAPPLICABLE))
				{
					rebateCur = Integer.valueOf(myAccountDetails.getRebateCur());
				}
				else
				{
					rebateCur = 0;
				}
				if (!myAccountDetails.getFieldAgentCur().equals(ApplicationConstants.NOTAPPLICABLE))
				{
					fieldAgentCur = Integer.valueOf(myAccountDetails.getFieldAgentCur());
				}
				else
				{
					fieldAgentCur = 0;
				}

				if (!myAccountDetails.getCouponsTot().equals(ApplicationConstants.NOTAPPLICABLE))
				{
					couponsTot = Integer.valueOf(myAccountDetails.getCouponsTot());
				}
				else
				{

					couponsTot = 0;
				}
				if (!myAccountDetails.getRebateTot().equals(ApplicationConstants.NOTAPPLICABLE))
				{
					rebateTot = Integer.valueOf(myAccountDetails.getRebateTot());
				}
				else
				{
					rebateTot = 0;
				}
				if (!myAccountDetails.getFieldAgentTot().equals(ApplicationConstants.NOTAPPLICABLE))
				{
					fieldAgentTot = Integer.valueOf(myAccountDetails.getFieldAgentTot());
				}
				else
				{
					fieldAgentTot = 0;
				}

				myAccountDetails.setCurrentBalance(String.valueOf(couponsCur + rebateCur + fieldAgentCur));
				myAccountDetails.setTotalLifetime(String.valueOf(couponsTot + rebateTot + fieldAgentTot));
				response = XstreamParserHelper.produceXMlFromObject(myAccountDetails);
			}
			else
			{

				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);

			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName + "Returned Response:" + response);
		return response;

	}

	/**
	 * The Service method for retrieving password.
	 * 
	 * @param userId
	 *            for which password to be sent.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	@Override
	public String forgotPassword(Integer userId) throws ScanSeeException
	{
		final String methodName = "forgotPassword";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		AuthenticateUser authenticateUser = null;
		EncryptDecryptPwd enryptDecryptpwd;
		String decryptedPassword = null;
		String toAddress = null;
		String subject = null;
		String msgBody = null;
		String fromAddress = null;
		String responseXML = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;
		ArrayList<AppConfiguration> fromAdd = null;
		if (null == userId)
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		authenticateUser = myAccountDao.fetchUserDetailsforForgotPassword(userId);

		if (null != authenticateUser)
		{
			try
			{

				toAddress = authenticateUser.getEmail();
				if (toAddress != null)
				{
					enryptDecryptpwd = new EncryptDecryptPwd();
					decryptedPassword = enryptDecryptpwd.decrypt(authenticateUser.getPassword());
					subject = "Recovered Password";
					msgBody = "Your password is: " + "<b>" + decryptedPassword + "</b>";
					msgBody = Utility.formForgotPasswordEmailTemplateHTML(decryptedPassword);
					emailConf = shoppingListDao.getAppConfig(ApplicationConstants.EMAILCONFIG);
					fromAdd = shoppingListDao.getAppConfig(ApplicationConstants.FORGOTPWDCONFIG);

					for (int j = 0; j < fromAdd.size(); j++)
					{

						if (fromAdd.get(j).getScreenName().equals("FromAddress"))
						{
							fromAddress = fromAdd.get(j).getScreenContent();
						}
					}
					for (int j = 0; j < emailConf.size(); j++)
					{
						if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
						{
							smtpHost = emailConf.get(j).getScreenContent();
						}
						if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
						{
							smtpPort = emailConf.get(j).getScreenContent();
						}
					}
					if (null != smtpHost || null != smtpPort)
					{
						EmailComponent.mailingComponent(fromAddress, toAddress, subject, msgBody, smtpHost, smtpPort);

					}
					responseXML = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.FORGOTPASSWORDTEXT);
					LOG.info("Password sent to user Email Address:" + toAddress);
				}
				responseXML = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.USEREMAILIDNOTEXIST);
				LOG.info("User Emailid not exist:" + toAddress);
			}
			catch (NoSuchAlgorithmException e)
			{
				LOG.error("Exception in Decryption ", e);
				throw new ScanSeeException(e.getMessage());
			}
			catch (NoSuchPaddingException e)
			{
				LOG.error("Exception in user password Decryption ", e);
				throw new ScanSeeException(e.getMessage());
			}
			catch (InvalidKeyException e)
			{
				LOG.error("Password Decryption Exception  ", e);
				throw new ScanSeeException(e.getMessage());
			}
			catch (InvalidAlgorithmParameterException e)
			{
				LOG.error(" Decryption  Password Exception ", e);
				throw new ScanSeeException(e.getMessage());
			}
			catch (InvalidKeySpecException e)
			{
				LOG.error("user Password Exception in Decryption ", e);
				throw new ScanSeeException(e.getMessage());
			}
			catch (IllegalBlockSizeException e)
			{
				LOG.error("exception Decryption Password   ", e);
				throw new ScanSeeException(e.getMessage());
			}
			catch (BadPaddingException e)
			{
				LOG.error("user password decrypted exception ", e);
				throw new ScanSeeException(e.getMessage());
			}
			catch (IOException e)
			{
				LOG.error("Exception in Decryption ", e);
				throw new ScanSeeException(e.getMessage());
			}
			catch (MessagingException e)
			{
				LOG.error("Exception in Sending Mail ", e);
				responseXML = Utility.formResponseXml(ApplicationConstants.INVALIDUSERNAMEORPASSWORD,
						ApplicationConstants.INVALIDUSERNAMEORPASSWORDTEXT);
			}
		}
		else
		{
			LOG.error("Invalid UserId {}", userId);
			responseXML = Utility.formResponseXml(ApplicationConstants.INVALIDEMAILADDRESS, ApplicationConstants.INVALIDUSERIDTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return responseXML;
	}

	/**
	 * The Service method for fetching user preference Info.This method
	 * validates the userId, if it is valid it will call the DAO method.
	 * 
	 * @param userId
	 *            for which user payment preferences information to be fetched.
	 * @return user preference object.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@Override
	public String fetchUserPaymentInfo(Integer userId) throws ScanSeeException
	{
		final String methodName = "fetchUserPaymentInfo";
		LOG.info("In Service...." + ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final UserPreferenceDetails userPreferenceDetails = new UserPreferenceDetails();
		if (null == userId)
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOZIPCODE);
		}

		final List<PaymentInterval> paymentIntervallst = baseDao.getPaymentIntervalDetails();
		boolean checkData = false;
		if (null != paymentIntervallst && !paymentIntervallst.isEmpty())
		{
			userPreferenceDetails.setPaymentIntervals(paymentIntervallst);
			checkData = true;
		}

		final List<PaymentType> paymentTypelst = baseDao.getPaymentTypeDetails();
		if (null != paymentTypelst && !paymentTypelst.isEmpty())
		{
			userPreferenceDetails.setPaymentTypes(paymentTypelst);
			checkData = true;
		}

		final List<PayoutType> payoutTypelst = baseDao.getPayoutDetails();
		if (null != payoutTypelst && !payoutTypelst.isEmpty())
		{
			userPreferenceDetails.setPayoutTypes(payoutTypelst);
			checkData = true;
		}

		UserPreference userPreference = myAccountDao.getUserPaymentPreferecesInfo(userId);
		if (null == userPreference)
		{
			userPreference = new UserPreference();
			userPreference.setIsUserAddExists(false);
			userPreference.setProvisionalPayoutEndDate(null);
			userPreference.setProvisionalPayoutStartDate(null);
			userPreference.setProvisionalPayoutActive(null);
			userPreferenceDetails.setUserPreference(userPreference);
			checkData = true;

		}
		else
		{
			if (null == userPreference.getUserAddress1())
			{
				userPreference.setIsUserAddExists(false);
			}
			else
			{
				userPreference.setIsUserAddExists(true);
			}
			userPreferenceDetails.setUserPreference(userPreference);
			checkData = true;
		}
		if (!checkData)
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}
		else
		{
			response = XstreamParserHelper.produceXMlFromObject(userPreferenceDetails);
		}
		LOG.info("In Service...." + ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The Service method for storing user preference Info.This method checks
	 * for mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method.
	 * 
	 * @param inputXml
	 *            containing userPaymentPrefereces details.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	@Override
	public String saveUserPaymentPreference(String inputXml) throws ScanSeeException
	{
		final String methodName = "saveUserPaymentPreference";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserPreference userPreference = (UserPreference) streamHelper.parseXmlToObject(inputXml);
		if (null != userPreference)
		{

			if ((null == userPreference.getSelPaymentIntervalID()) || (null == userPreference.getSelPaymentTypeID())
					|| (null == userPreference.getSelPayoutTypeID()) || (null == userPreference.getDefaultPayout()))
			{

				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.PAYMENTINFO);
				return response;
			}
			else
			{
				response = myAccountDao.saveUserPaymentPreferenceDetails(userPreference);
			}

		}

		if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
		{
			response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SAVEUSERPAYMENTPREFERENCETEXT);
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}