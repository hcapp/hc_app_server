package com.scansee.myaccount.service;

import com.scansee.common.exception.ScanSeeException;

/**
 * The MyAccountService Interface. {@link ImplementedBy}
 * {@link MyAccountServiceImpl}
 * 
 * @author dileepa_cc
 */
public interface MyAccountService
{
	/**
	 * The Service Method for fetching user account details.This method checks
	 * for mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method.
	 * 
	 * @param xml
	 *            containing user details for fetching data.
	 * @return User Account Details object.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String fetchAccountDetails(String xml) throws ScanSeeException;

	/**
	 * The Service method for retrieving password.
	 * 
	 * @param userId
	 *            for which password to be sent.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String forgotPassword(Integer userId) throws ScanSeeException;

	/**
	 * The Service method for fetching user preference Info.This method
	 * validates the userId, if it is valid it will call the DAO method.
	 * 
	 * @param userId
	 *            for which user payment preferences information to be fetched.
	 * @return user preference object.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String fetchUserPaymentInfo(Integer userId) throws ScanSeeException;

	/**
	 * The Service method for storing user preference Info.This method checks
	 * for mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method.
	 * 
	 * @param inputXml
	 *            containing userPaymentPrefereces details.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String saveUserPaymentPreference(String inputXml) throws ScanSeeException;

}
