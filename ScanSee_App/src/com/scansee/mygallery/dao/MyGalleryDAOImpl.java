package com.scansee.mygallery.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.CategoryInfo;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.CouponsDetails;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.HotDealsListResultSet;
import com.scansee.common.pojos.LoyaltyDetail;
import com.scansee.common.pojos.ProductDetailsRequest;
import com.scansee.common.pojos.RebateDetail;

/**
 * This class for fetching my gallery coupons,loyalty and rebate information
 * 
 * @author sowjanya_d
 */
public class MyGalleryDAOImpl implements MyGalleryDAO
{

	/**
	 * Logger instance.
	 */
	private static final Logger log = LoggerFactory.getLogger(MyGalleryDAOImpl.class.getName());

	/**
	 * for jdbcTemplate connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * This method for to get data source Spring DI.
	 * 
	 * @param dataSource
	 *            for DB operations.
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * This DAOImpl method is used to fetch my gallery coupon information based
	 * on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	@SuppressWarnings({ "unchecked" })
	public CLRDetails getMyGalleryCouponInfo(CLRDetails clrDetails) throws ScanSeeException
	{
		final String methodName = "getMyGalleryCouponInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;
		List<CouponDetails> couponDetailList = null;
		Boolean couponNextPage = false;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryCoupons");

			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, clrDetails.getUserId());
			fetchCouponParameters.addValue(ApplicationConstants.USERID, clrDetails.getUserId());
			fetchCouponParameters.addValue("SearchKey", clrDetails.getSearchKey());
			fetchCouponParameters.addValue(ApplicationConstants.SCREENNAME, ApplicationConstants.ALLCOUPSCREENNAME);
			// for category id validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getCategoryID() == null)
			{
				clrDetails.setCategoryID(0);
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());
			}
			else
			{
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());

			}
			// for retailer id validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getRetailerId() == null)
			{
				clrDetails.setRetailerId(0);
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());
			}
			else
			{
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());

			}
			// for lower limit id validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getLowerLimit() != null)
			{

				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, clrDetails.getLowerLimit());
			}
			else
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
			}

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in getMyGalleryCouponInfo method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");
					if (couponDetailList != null && !couponDetailList.isEmpty())
					{
						clrDetatilsObj = new CLRDetails();
						couponNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						clrDetatilsObj.setCouponDetails(couponDetailList);
						if (couponNextPage != null)
						{
							if (couponNextPage)
							{
								clrDetatilsObj.setNextPage(1);
							}
							else
							{
								clrDetatilsObj.setNextPage(0);
							}
						}

					}

				}
			}
		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return clrDetatilsObj;
	}

	/**
	 * This DAOImpl method is used to fetch my gallery loyalty information based
	 * on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	@SuppressWarnings({ "unchecked" })
	public CLRDetails getMyGalleryLoyaltyInfo(CLRDetails clrDetails) throws ScanSeeException
	{
		final String methodName = "getMyGalleryInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);

		CLRDetails clrDetatilsObj = null;
		List<LoyaltyDetail> loyaltyDetailList = null;
		Boolean loyaltyNextPage = false;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryLoyalty");
			simpleJdbcCall.returningResultSet("LoyaltyDetails", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final MapSqlParameterSource fetchLoyaltyParameters = new MapSqlParameterSource();
			fetchLoyaltyParameters.addValue(ApplicationConstants.USERID, clrDetails.getUserId());
			fetchLoyaltyParameters.addValue("SearchKey", clrDetails.getSearchKey());
			fetchLoyaltyParameters.addValue("ScreenName", ApplicationConstants.ALLCOUPSCREENNAME);
			// for category id validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getCategoryID() == null)
			{
				clrDetails.setCategoryID(0);
				fetchLoyaltyParameters.addValue("CategoryIDs", clrDetails.getCategoryID());
			}
			else
			{
				fetchLoyaltyParameters.addValue("CategoryIDs", clrDetails.getCategoryID());

			}
			// for retailer id validation ,if it is null ,setting zero to

			if (clrDetails.getRetailerId() == null)
			{
				clrDetails.setRetailerId(0);
				fetchLoyaltyParameters.addValue("RetailerID", clrDetails.getRetailerId());
			}
			else
			{
				fetchLoyaltyParameters.addValue("RetailerID", clrDetails.getRetailerId());

			}
			// for lower limit id validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getLowerLimit() != null)
			{
				fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT, clrDetails.getLowerLimit());
			}
			else
			{
				fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
			}

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_GalleryLoyalty method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else
			{
				loyaltyDetailList = (List<LoyaltyDetail>) resultFromProcedure.get("LoyaltyDetails");
				if (loyaltyDetailList != null && !loyaltyDetailList.isEmpty())
				{
					clrDetatilsObj = new CLRDetails();
					loyaltyNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					clrDetatilsObj.setLoyaltyDetails(loyaltyDetailList);
					if (loyaltyNextPage != null)
					{
						if (loyaltyNextPage)
						{
							clrDetatilsObj.setNextPage(1);
						}
						else
						{
							clrDetatilsObj.setNextPage(0);
						}
					}

				}

			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		return clrDetatilsObj;

	}

	/**
	 * This DAOImpl method is used to fetch my gallery of All type coupon
	 * information based on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public CLRDetails getMyGalleryAllTypeCouponInfo(CLRDetails clrDetails) throws ScanSeeException
	{
		final String methodName = "getMyGalleryAllTypeInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;

		List<CouponDetails> couponDetailList = null;

		Boolean couponNextPage = false;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryAllCoupons");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, clrDetails.getUserId());
			fetchCouponParameters.addValue("SearchKey", clrDetails.getSearchKey());
			fetchCouponParameters.addValue(ApplicationConstants.SCREENNAME, ApplicationConstants.ALLCOUPSCREENNAME);
			// for category id validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getCategoryID() == null)
			{
				clrDetails.setCategoryID(0);
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());
			}
			else
			{
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());

			}
			// for retailer id validation ,if it is null ,setting zero to

			if (clrDetails.getRetailerId() == null)
			{
				clrDetails.setRetailerId(0);
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());
			}
			else
			{
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());

			}

			// for lower limit id validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getLowerLimit() != null)
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, clrDetails.getLowerLimit());
			}
			else
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
			}
			//For user tracking
			fetchCouponParameters.addValue(ApplicationConstants.MAINMENUID, clrDetails.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_GalleryAllCoupons method  ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");
					if (couponDetailList != null && !couponDetailList.isEmpty())
					{
						clrDetatilsObj = new CLRDetails();
						couponNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						/**
						 * Setting coupon details list to CLRdetails
						 */
						clrDetatilsObj.setCouponDetails(couponDetailList);
						if (couponNextPage != null)
						{
							if (couponNextPage)
							{
								clrDetatilsObj.setNextPage(1);
							}
							else
							{
								clrDetatilsObj.setNextPage(0);
							}
						}
					}
				}

			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return clrDetatilsObj;
	}

	/**
	 * This DAOImpl method is used to fetch my gallery of All type loyalty
	 * information based on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public CLRDetails getMyGalleryAllTypeLoyaltyInfo(CLRDetails clrDetails) throws ScanSeeException
	{

		final String methodName = "getMyGalleryAllTypeLoyaltyInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;
		List<LoyaltyDetail> loyaltyDetailList = null;
		Boolean loyaltyNextPage = false;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryAllLoyaltyDeals");
			simpleJdbcCall.returningResultSet("LoyaltyDetails", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final MapSqlParameterSource fetchLoyaltyParameters = new MapSqlParameterSource();
			fetchLoyaltyParameters.addValue(ApplicationConstants.USERID, clrDetails.getUserId());
			fetchLoyaltyParameters.addValue("SearchKey", clrDetails.getSearchKey());
			fetchLoyaltyParameters.addValue(ApplicationConstants.SCREENNAME, ApplicationConstants.ALLCOUPSCREENNAME);
			// for category id validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getCategoryID() == null)
			{
				clrDetails.setCategoryID(0);
				fetchLoyaltyParameters.addValue("CategoryIDs", clrDetails.getCategoryID());

			}
			else
			{
				fetchLoyaltyParameters.addValue("CategoryIDs", clrDetails.getCategoryID());

			}
			// for retailer id validation ,if it is null ,setting zero to

			if (clrDetails.getRetailerId() == null)
			{
				clrDetails.setRetailerId(0);
				fetchLoyaltyParameters.addValue("RetailerID", clrDetails.getRetailerId());
			}
			else
			{
				fetchLoyaltyParameters.addValue("RetailerID", clrDetails.getRetailerId());

			}
			// for lower limit validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getLowerLimit() != null)
			{
				fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT, clrDetails.getLowerLimit());
			}
			else
			{
				fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
			}
			//For user tracking
			fetchLoyaltyParameters.addValue(ApplicationConstants.MAINMENUID, clrDetails.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(" Error Occured in usp_GalleryAllLoyaltyDeals method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else
			{
				loyaltyDetailList = (List<LoyaltyDetail>) resultFromProcedure.get("LoyaltyDetails");

				if (loyaltyDetailList != null && !loyaltyDetailList.isEmpty())
				{
					clrDetatilsObj = new CLRDetails();
					loyaltyNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					clrDetatilsObj.setLoyaltyDetails(loyaltyDetailList);
					if (loyaltyNextPage != null)
					{
						if (loyaltyNextPage)
						{
							clrDetatilsObj.setNextPage(1);
						}
						else
						{
							clrDetatilsObj.setNextPage(0);
						}
					}

				}

			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		return clrDetatilsObj;

	}

	/**
	 * This DAOImpl method is used to fetch my gallery of type Expired coupon
	 * information based on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public CLRDetails getMyGalleryExpiredTypeCouponInfo(CLRDetails clrDetails) throws ScanSeeException
	{
		final String methodName = "getMyGalleryExpiredTypeCouponInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;

		List<CouponDetails> couponDetailList = null;

		Boolean couponNextPage = false;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryExpiredCoupons");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, clrDetails.getUserId());
			fetchCouponParameters.addValue("SearchKey", clrDetails.getSearchKey());

			fetchCouponParameters.addValue(ApplicationConstants.SCREENNAME, ApplicationConstants.ALLCOUPSCREENNAME);
			// for category id validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getCategoryID() == null)
			{
				clrDetails.setCategoryID(0);
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());
			}
			else
			{
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());

			}
			// for retailer id validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getRetailerId() == null)
			{
				clrDetails.setRetailerId(0);
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());
			}
			else
			{
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());

			}
			// for lower limit validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getLowerLimit() != null)
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, clrDetails.getLowerLimit());
			}
			else
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
			}

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_GalleryExpiredCoupons method ..errorNum. {} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");
					if (couponDetailList != null && !couponDetailList.isEmpty())
					{
						clrDetatilsObj = new CLRDetails();
						couponNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						clrDetatilsObj.setCouponDetails(couponDetailList);
						if (couponNextPage != null)
						{
							if (couponNextPage)
							{
								clrDetatilsObj.setNextPage(1);
							}
							else
							{
								clrDetatilsObj.setNextPage(0);
							}
						}

					}

				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return clrDetatilsObj;
	}

	/**
	 * This DAOImpl method is used to fetch my gallery of type Expired loyalty
	 * information based on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public CLRDetails getMyGalleryExpiredTypeLoyaltyInfo(CLRDetails clrDetails) throws ScanSeeException
	{

		final String methodName = "getMyGalleryExpiredTypeLoyaltyInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;
		Boolean loyaltyNextPage = false;

		List<LoyaltyDetail> loyaltyDetailList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryExpiredLoyaltyDeals");
			simpleJdbcCall.returningResultSet("LoyaltyDetails", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final MapSqlParameterSource fetchLoyaltyParameters = new MapSqlParameterSource();
			fetchLoyaltyParameters.addValue(ApplicationConstants.USERID, clrDetails.getUserId());
			fetchLoyaltyParameters.addValue("SearchKey", clrDetails.getSearchKey());
			fetchLoyaltyParameters.addValue(ApplicationConstants.SCREENNAME, ApplicationConstants.ALLCOUPSCREENNAME);
			// for category validation ,if it is null ,setting zero to category
			// id.
			if (clrDetails.getCategoryID() == null)
			{
				clrDetails.setCategoryID(0);
				fetchLoyaltyParameters.addValue("CategoryIDs", clrDetails.getCategoryID());
			}
			else
			{
				fetchLoyaltyParameters.addValue("CategoryIDs", clrDetails.getCategoryID());

			}
			// for retailer id validation ,if it is null ,setting zero to

			if (clrDetails.getRetailerId() == null)
			{
				clrDetails.setRetailerId(0);
				fetchLoyaltyParameters.addValue("RetailerID", clrDetails.getRetailerId());
			}
			else
			{
				fetchLoyaltyParameters.addValue("RetailerID", clrDetails.getRetailerId());

			}
			// for lower limit validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getLowerLimit() != null)
			{
				fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT, clrDetails.getLowerLimit());
			}
			else
			{
				fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
			}

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_GalleryExpiredLoyaltyDeals method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else
			{
				loyaltyDetailList = (List<LoyaltyDetail>) resultFromProcedure.get("LoyaltyDetails");

				if (loyaltyDetailList != null && !loyaltyDetailList.isEmpty())
				{
					clrDetatilsObj = new CLRDetails();
					loyaltyNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					clrDetatilsObj.setLoyaltyDetails(loyaltyDetailList);
					if (loyaltyNextPage != null)
					{
						if (loyaltyNextPage)
						{
							clrDetatilsObj.setNextPage(1);
						}
						else
						{
							clrDetatilsObj.setNextPage(0);
						}
					}

				}

			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		return clrDetatilsObj;

	}

	/**
	 * This DAOImpl method is used to fetch my gallery of type Used coupon
	 * information based on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public CLRDetails getMyGalleryUsedTypeCouponInfo(CLRDetails clrDetails) throws ScanSeeException
	{
		final String methodName = "getMyGalleryUsedTypeCouponInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;

		List<CouponDetails> couponDetailList = null;

		Boolean couponNextPage = false;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryUsedCoupons");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, clrDetails.getUserId());
			fetchCouponParameters.addValue("SearchKey", clrDetails.getSearchKey());
			fetchCouponParameters.addValue(ApplicationConstants.SCREENNAME, ApplicationConstants.ALLCOUPSCREENNAME);
			// for category id validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getCategoryID() == null)
			{
				clrDetails.setCategoryID(0);
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());
			}
			else
			{
				fetchCouponParameters.addValue("CategoryIDs", clrDetails.getCategoryID());

			}
			// for retailer id validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getRetailerId() == null)
			{
				clrDetails.setRetailerId(0);
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());
			}
			else
			{
				fetchCouponParameters.addValue("RetailerID", clrDetails.getRetailerId());

			}
			// for lower limit validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getLowerLimit() != null)
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, clrDetails.getLowerLimit());
			}
			else
			{
				fetchCouponParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
			}

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_GalleryUsedCoupons method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");
					if (couponDetailList != null && !couponDetailList.isEmpty())
					{
						clrDetatilsObj = new CLRDetails();
						couponNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						clrDetatilsObj.setCouponDetails(couponDetailList);
						if (couponNextPage != null)
						{
							if (couponNextPage)
							{
								clrDetatilsObj.setNextPage(1);
							}
							else
							{
								clrDetatilsObj.setNextPage(0);
							}
						}

					}

				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return clrDetatilsObj;
	}

	/**
	 * This DAOImpl method is used to fetch my gallery of type Used loyalty
	 * information based on userId.
	 * 
	 * @param clrDetails
	 *            contains user id,type.
	 * @return CLRDetails c,l,r information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public CLRDetails getMyGalleryUsedTypeLoyaltyInfo(CLRDetails clrDetails) throws ScanSeeException
	{
		final String methodName = "getMyGalleryUsedTypeLoyaltyInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;

		List<LoyaltyDetail> loyaltyDetailList = null;

		Boolean loyaltyNextPage = false;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryUsedLoyaltyDeals");
			simpleJdbcCall.returningResultSet("LoyaltyDetails", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final MapSqlParameterSource fetchLoyaltyParameters = new MapSqlParameterSource();
			fetchLoyaltyParameters.addValue(ApplicationConstants.USERID, clrDetails.getUserId());
			fetchLoyaltyParameters.addValue("SearchKey", clrDetails.getSearchKey());
			fetchLoyaltyParameters.addValue(ApplicationConstants.SCREENNAME, ApplicationConstants.ALLCOUPSCREENNAME);
			// for category id validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getCategoryID() == null)
			{
				clrDetails.setCategoryID(0);
				fetchLoyaltyParameters.addValue("CategoryIDs", clrDetails.getCategoryID());
			}
			else
			{
				fetchLoyaltyParameters.addValue("CategoryIDs", clrDetails.getCategoryID());

			}
			// for retailer id validation ,if it is null ,setting zero to

			if (clrDetails.getRetailerId() == null)
			{
				clrDetails.setRetailerId(0);
				fetchLoyaltyParameters.addValue("RetailerID", clrDetails.getRetailerId());
			}
			else
			{
				fetchLoyaltyParameters.addValue("RetailerID", clrDetails.getRetailerId());

			}
			// for lower limit validation ,if it is null ,setting zero to
			// category id.
			if (clrDetails.getLowerLimit() != null)
			{
				fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT, clrDetails.getLowerLimit());
			}
			else
			{
				fetchLoyaltyParameters.addValue(ApplicationConstants.LOWERLIMIT, 0);
			}

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_GalleryUsedLoyaltyDeals method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else
			{
				loyaltyDetailList = (List<LoyaltyDetail>) resultFromProcedure.get("LoyaltyDetails");
				if (loyaltyDetailList != null && !loyaltyDetailList.isEmpty())
				{
					clrDetatilsObj = new CLRDetails();
					loyaltyNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					clrDetatilsObj.setLoyaltyDetails(loyaltyDetailList);
					if (loyaltyNextPage != null)
					{
						if (loyaltyNextPage)
						{
							clrDetatilsObj.setNextPage(1);
						}
						else
						{
							clrDetatilsObj.setNextPage(0);
						}
					}

				}
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		return clrDetatilsObj;

	}

	/**
	 * This DAOImpl method is used to redeem coupon information based on userId.
	 * 
	 * @param couponDetailsObj
	 *            contains user id,coupon id ..
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public String userRedeemCoupon(CouponDetails couponDetailsObj) throws ScanSeeException
	{
		final String methodName = "userRedeemCoupon in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = null;
		String responseFromProc = null;
		Integer usedFlag = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryRedeemCoupon");

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, couponDetailsObj.getUserId());
			fetchCouponParameters.addValue("CouponID", couponDetailsObj.getCouponId());
			fetchCouponParameters.addValue("UserCouponClaimTypeID", couponDetailsObj.getUserCouponClaimTypeID());
			fetchCouponParameters.addValue("CouponPayoutMethod", couponDetailsObj.getCouponPayoutMethod());
			fetchCouponParameters.addValue("RetailLocationID", couponDetailsObj.getRetailLocationID());
			fetchCouponParameters.addValue("CouponClaimDate", couponDetailsObj.getCouponClaimDate());
			
			//For user tracking
			fetchCouponParameters.addValue("CouponListID", couponDetailsObj.getCouponListID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_GalleryRedeemCoupon method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					fromProc = (Integer) resultFromProcedure.get("Status");
					if (fromProc == 0)
					{
						responseFromProc = ApplicationConstants.SUCCESS;

						usedFlag = (Integer) resultFromProcedure.get("UsedFlag");

						if (usedFlag != null)
						{
							if (usedFlag == 1)
							{
								responseFromProc = ApplicationConstants.CLRALLREADYREDEEMRESPONSETEXT;
							}
						}

					}
					else
					{
						responseFromProc = ApplicationConstants.FAILURE;
					}
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		return responseFromProc;
	}

	/**
	 * This DAOImpl method is used to redeem loyalty information based on
	 * userId.
	 * 
	 * @param loyaltyDetailObj
	 *            contains user id,loyalty deal id ..
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public String userRedeemLoyalty(LoyaltyDetail loyaltyDetailObj) throws ScanSeeException
	{
		final String methodName = "userRedeemLoyalty in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = null;
		String responseFromProc = null;
		Integer usedFlag = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryRedeemLoyalty");

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, loyaltyDetailObj.getUserId());
			fetchCouponParameters.addValue("UserClaimTypeID", loyaltyDetailObj.getUserClaimTypeID());
			fetchCouponParameters.addValue("LoyaltyDealID", loyaltyDetailObj.getLoyaltyDealId());
			fetchCouponParameters.addValue("APIPartnerID", loyaltyDetailObj.getaPIPartnerID());
			fetchCouponParameters.addValue("LoyaltyDealRedemptionDate", loyaltyDetailObj.getLoyaltyDealRedemptionDate());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_GalleryRedeemLoyalty method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					fromProc = (Integer) resultFromProcedure.get("Status");
					if (fromProc == 0)
					{
						responseFromProc = ApplicationConstants.SUCCESS;
						usedFlag = (Integer) resultFromProcedure.get("UsedFlag");

						if (usedFlag != null)
						{
							if (usedFlag == 1)
							{
								responseFromProc = ApplicationConstants.CLRALLREADYREDEEMRESPONSETEXT;
							}
						}
					}
					else
					{
						responseFromProc = ApplicationConstants.FAILURE;
					}
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		return responseFromProc;
	}

	/**
	 * This DAOImpl method is used to redeem rebate information based on userId.
	 * 
	 * @param rebateDetailObj
	 *            contains user id,rebate id ..
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public String userRedeemRebate(RebateDetail rebateDetailObj) throws ScanSeeException
	{

		final String methodName = "userRedeemRebate in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = null;
		String responseFromProc = null;
		Integer usedFlag = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryRedeemRebate");

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue(ApplicationConstants.USERID, rebateDetailObj.getUserId());
			fetchCouponParameters.addValue("RebateID", rebateDetailObj.getRebateId());
			fetchCouponParameters.addValue("RetailLocationID", rebateDetailObj.getRetailLocationID());
			fetchCouponParameters.addValue("RedemptionStatusID", rebateDetailObj.getRedemptionStatusID());
			fetchCouponParameters.addValue("PurchaseAmount", rebateDetailObj.getPurchaseAmount());
			fetchCouponParameters.addValue("PurchaseDate", rebateDetailObj.getPurchaseDate());
			fetchCouponParameters.addValue("ProductSerialNumber", rebateDetailObj.getProductSerialNumber());
			fetchCouponParameters.addValue("ScanImage", rebateDetailObj.getScanImage());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_GalleryRedeemRebate method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					fromProc = (Integer) resultFromProcedure.get("Status");
					if (fromProc == 0)
					{
						responseFromProc = ApplicationConstants.SUCCESS;
						usedFlag = (Integer) resultFromProcedure.get("UsedFlag");

						if (usedFlag != null)
						{
							if (usedFlag == 1)
							{
								responseFromProc = ApplicationConstants.CLRALLREADYREDEEMRESPONSETEXT;
							}
						}
					}
					else
					{
						responseFromProc = ApplicationConstants.FAILURE;
					}
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return responseFromProc;
	}

	/**
	 * This DAOImpl method is used to delete used rebate information based on
	 * used coupon id.
	 * 
	 * @param couponDetailsObj
	 *            contains used coupon id.
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public String deleteUsedCoupon(CouponDetails couponDetailsObj) throws ScanSeeException
	{
		final String methodName = "userRedeemRebate in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = null;
		String responseFromProc = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryDeleteUsedCoupons");

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue("UserCouponGalleryID", couponDetailsObj.getUserCouponGalleryID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_GalleryDeleteUsedCoupons method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					fromProc = (Integer) resultFromProcedure.get("Status");
					if (fromProc == 0)
					{
						responseFromProc = ApplicationConstants.SUCCESS;
					}
					else
					{
						responseFromProc = ApplicationConstants.FAILURE;
					}
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return responseFromProc;
	}

	/**
	 * This DAOImpl method is used to delete used loyalty information based on
	 * used loyalty id.
	 * 
	 * @param loyaltyDetailObj
	 *            contains used loyalty id.
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public String deleteUsedLoyalty(LoyaltyDetail loyaltyDetailObj) throws ScanSeeException
	{
		final String methodName = "deleteUsedLoyalty in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = null;
		String responseFromProc = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryDeleteUsedLoyaltyDeal");

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue("UserLoyaltyGalleryID", loyaltyDetailObj.getUserLoyaltyGalleryID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_GalleryDeleteUsedLoyaltyDeal method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					fromProc = (Integer) resultFromProcedure.get("Status");
					if (fromProc == 0)
					{
						responseFromProc = ApplicationConstants.SUCCESS;
					}
					else
					{
						responseFromProc = ApplicationConstants.FAILURE;
					}
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return responseFromProc;
	}

	/**
	 * This DAOImpl method is used to delete used rebate information based on
	 * used rebate id.
	 * 
	 * @param rebateDetailObj
	 *            contains used rebate id.
	 * @return Success or failure based on operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public String deleteUsedRebate(RebateDetail rebateDetailObj) throws ScanSeeException
	{
		final String methodName = "deleteUsedRebate in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = null;
		String responseFromProc = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryDeleteUsedRebate");

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue("UserRebateGalleryID", rebateDetailObj.getUserRebateGalleryID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_GalleryDeleteUsedRebate method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					fromProc = (Integer) resultFromProcedure.get("Status");
					if (fromProc == 0)
					{
						responseFromProc = ApplicationConstants.SUCCESS;
					}
					else
					{
						responseFromProc = ApplicationConstants.FAILURE;
					}
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return responseFromProc;
	}

	/**
	 * This DAOImpl method is used to retrive Categories
	 * 
	 * @param
	 * @return category list.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	public ArrayList<CategoryInfo> retriveCategory() throws ScanSeeException
	{
		final String methodName = "retriveCategory in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<CategoryInfo> categoryInfoList = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_RetrieveCategory");

			simpleJdbcCall.returningResultSet("retrivecategory", new BeanPropertyRowMapper<CategoryInfo>(CategoryInfo.class));
			final MapSqlParameterSource fetchCategoryInfo = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCategoryInfo);
			categoryInfoList = (ArrayList<CategoryInfo>) resultFromProcedure.get("retrivecategory");

			if (null != resultFromProcedure.get(ApplicationConstants.STATUS))
			{
				log.info("Error Occured in usp_RetrieveCategory method");
				throw new ScanSeeException();
			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return categoryInfoList;
	}

	/**
	 * Method for fetching coupon retailer list for searching . layer.
	 * 
	 * @param
	 * @return response as String contains retailer id and name
	 */
	@Override
	public ArrayList<LoyaltyDetail> getCouponRetailer() throws ScanSeeException
	{
		final String methodName = "getCouponRetailer in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<LoyaltyDetail> categoryInfoList = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FetchCouponRetailers");

			simpleJdbcCall.returningResultSet("retrivecategory", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));
			final MapSqlParameterSource fetchCategoryInfo = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCategoryInfo);
			categoryInfoList = (ArrayList<LoyaltyDetail>) resultFromProcedure.get("retrivecategory");

			if (null != resultFromProcedure.get(ApplicationConstants.STATUS))
			{
				log.info("Error Occured in usp_FetchCouponRetailers method");
				throw new ScanSeeException();
			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return categoryInfoList;
	}

	/**
	 * Method for fetching coupon retailer list for searching . layer.
	 * 
	 * @param
	 * @return response as String contains retailer id and name
	 */
	@Override
	public ArrayList<LoyaltyDetail> getLoyaltyRetailer() throws ScanSeeException
	{
		final String methodName = "getLoyaltyRetailer in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<LoyaltyDetail> categoryInfoList = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FetchLoyaltyRetailers");

			simpleJdbcCall.returningResultSet("retrivecategory", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));
			final MapSqlParameterSource fetchCategoryInfo = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCategoryInfo);
			categoryInfoList = (ArrayList<LoyaltyDetail>) resultFromProcedure.get("retrivecategory");

			if (null != resultFromProcedure.get(ApplicationConstants.STATUS))
			{
				log.info("Error Occured in usp_FetchLoyaltyRetailers method");
				throw new ScanSeeException();
			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return categoryInfoList;
	}

	/**
	 * Method to fetch all coupons for Product from database.
	 * @param objCoupDetReq of ProductDetailsRequest object.
	 * @return CouponsDetails object containing coupon details
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public CouponsDetails getAllCouponsByLocation(ProductDetailsRequest objCoupDetReq) throws ScanSeeException {

		final String methodName = "getAllCouponsByLocation in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> objCouponDetailsList = null;
		CouponsDetails objCouponsDetails = null;
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryAllCouponsByLocation");
			simpleJdbcCall.returningResultSet("couponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCoupDetails = new MapSqlParameterSource();
			fetchCoupDetails.addValue(ApplicationConstants.USERID, objCoupDetReq.getUserId());
			fetchCoupDetails.addValue("SearchKey", objCoupDetReq.getSearchKey());
			fetchCoupDetails.addValue("BusinessCategoryIDs", objCoupDetReq.getBusCatIDs());
			fetchCoupDetails.addValue("RetailID", objCoupDetReq.getRetailID());
			fetchCoupDetails.addValue("Latitude", objCoupDetReq.getLatitude());
			fetchCoupDetails.addValue("Longitude", objCoupDetReq.getLongitude());
			fetchCoupDetails.addValue("PostalCode", objCoupDetReq.getPostalcode());
			fetchCoupDetails.addValue("PopulationCenterID", objCoupDetReq.getPopCentId());
			fetchCoupDetails.addValue("LowerLimit", objCoupDetReq.getLowerLimit());
			fetchCoupDetails.addValue(ApplicationConstants.SCREENNAME, ApplicationConstants.ALLCOUPSCREENNAME);
			fetchCoupDetails.addValue(ApplicationConstants.MAINMENUID, objCoupDetReq.getMainMenuID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCoupDetails);
			
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(" Error Occured in usp_GalleryAllCouponsByLocation method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else	{
				objCouponDetailsList = (List<CouponDetails>) resultFromProcedure.get("couponDetails");
				
				if (objCouponDetailsList != null && !objCouponDetailsList.isEmpty())	{
					final Boolean nxtPageFlag = (Boolean) resultFromProcedure.get("NxtPageFlag");
					final Integer maxCnt = (Integer) resultFromProcedure.get("MaxCnt");
					objCouponsDetails = new CouponsDetails();
					objCouponsDetails.setCouponDetail(objCouponDetailsList);
					if (nxtPageFlag) {
						objCouponsDetails.setNextPageFlag(1);
					} else {
						objCouponsDetails.setNextPageFlag(0);
					}
					objCouponsDetails.setMaxCnt(maxCnt);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		return objCouponsDetails;
	}

	/**
	 * Method to fetch all coupons for Product form database.
	 * @param objCoupDetReq of ProductDetailsRequest object.
	 * @return CouponsDetails object containing coupon details
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public CouponsDetails getAllCouponsByProduct(ProductDetailsRequest objCoupDetReq) throws ScanSeeException {
		final String methodName = "getAllCouponsByLocation in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> objCouponDetailsList = null;
		CouponsDetails objCouponsDetails = null;
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryAllCouponsByProduct");
			simpleJdbcCall.returningResultSet("couponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCoupDetails = new MapSqlParameterSource();
			fetchCoupDetails.addValue(ApplicationConstants.USERID, objCoupDetReq.getUserId());
			fetchCoupDetails.addValue("SearchKey", objCoupDetReq.getSearchKey());
			fetchCoupDetails.addValue("CategoryIDs", objCoupDetReq.getCatIDs());
			fetchCoupDetails.addValue("Latitude", objCoupDetReq.getLatitude());
			fetchCoupDetails.addValue("Longitude", objCoupDetReq.getLongitude());
			fetchCoupDetails.addValue("PostalCode", objCoupDetReq.getPostalcode());
			fetchCoupDetails.addValue("PopulationCenterID", objCoupDetReq.getPopCentId());
			fetchCoupDetails.addValue("LowerLimit", objCoupDetReq.getLowerLimit());
			fetchCoupDetails.addValue(ApplicationConstants.SCREENNAME, ApplicationConstants.ALLCOUPSCREENNAME);
			fetchCoupDetails.addValue(ApplicationConstants.MAINMENUID, objCoupDetReq.getMainMenuID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCoupDetails);
			
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(" Error Occured in usp_GalleryAllCouponsByProduct method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else	{
				objCouponDetailsList = (List<CouponDetails>) resultFromProcedure.get("couponDetails");
				
				if (objCouponDetailsList != null && !objCouponDetailsList.isEmpty())	{
					final Boolean nxtPageFlag = (Boolean) resultFromProcedure.get("NxtPageFlag");
					final Integer maxCnt = (Integer) resultFromProcedure.get("MaxCnt");
					objCouponsDetails = new CouponsDetails();
					objCouponsDetails.setCouponDetail(objCouponDetailsList);
					if (nxtPageFlag)	{
						objCouponsDetails.setNextPageFlag(1);
					} else {
						objCouponsDetails.setNextPageFlag(0);
					}
					objCouponsDetails.setMaxCnt(maxCnt);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		return objCouponsDetails;
	}

	/**
	 * Method to fetch population centers for coupons for Location.
	 * @param userId as request
	 * @return CouponsDetails object containing population center list
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public CouponsDetails getCoupPopulationCentresForLoc(Integer userId) throws ScanSeeException {
		final String methodName = "getCoupPopulationCentresForLoc in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> objCouponDetailsList = null;
		CouponsDetails objCouponsDetails = null;
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_CouponLocationPopulationCentres");
			simpleJdbcCall.returningResultSet("couponPopulationCenters", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCoupDetails = new MapSqlParameterSource();
			fetchCoupDetails.addValue(ApplicationConstants.USERID, userId);
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCoupDetails);
			
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(" Error Occured in usp_CouponLocationPopulationCentres method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else	{
				objCouponDetailsList = (List<CouponDetails>) resultFromProcedure.get("couponPopulationCenters");
				
				if (objCouponDetailsList != null && !objCouponDetailsList.isEmpty())	{
					objCouponsDetails = new CouponsDetails();
					objCouponsDetails.setCouponDetail(objCouponDetailsList);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		return objCouponsDetails;
	}

	/**
	 * Method to get population centers for coupons for product.
	 * @param userId as request.
	 * @return CouponsDetails object containing population center list
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public CouponsDetails getCoupPopulationCentresForProd(Integer userId) throws ScanSeeException {
		final String methodName = "getCoupPopulationCentresForLoc in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> objCouponDetailsList = null;
		CouponsDetails objCouponsDetails = null;
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_CouponProductPopulationCentres");
			simpleJdbcCall.returningResultSet("couponPopulationCenters", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCoupDetails = new MapSqlParameterSource();
			fetchCoupDetails.addValue(ApplicationConstants.USERID, userId);
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCoupDetails);
			
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(" Error Occured in usp_GalleryAllCouponsByProduct method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else	{
				objCouponDetailsList = (List<CouponDetails>) resultFromProcedure.get("couponPopulationCenters");
				
				if (objCouponDetailsList != null && !objCouponDetailsList.isEmpty())	{
					objCouponsDetails = new CouponsDetails();
					objCouponsDetails.setCouponDetail(objCouponDetailsList);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		return objCouponsDetails;
	}

	/**
	 * Method to get business categories for coupons for Locations.
	 * @param objProdDetailsReq ProductDetailsRequest object
	 * @return CouponsDetails object containing business categories
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public CouponsDetails getCoupLocationBusinessCategory(ProductDetailsRequest objProdDetailsReq) throws ScanSeeException {
		final String methodName = "getCoupLocationBusinessCategory in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> objCouponDetailsList = null;
		CouponsDetails objCouponsDetails = null;
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_CouponBusinessCategoryDisplay");
			simpleJdbcCall.returningResultSet("couponPopulationCenters", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCoupDetails = new MapSqlParameterSource();
			fetchCoupDetails.addValue(ApplicationConstants.USERID, objProdDetailsReq.getUserId());
			fetchCoupDetails.addValue("Latitude", objProdDetailsReq.getLatitude());
			fetchCoupDetails.addValue("Longitude", objProdDetailsReq.getLongitude());
			fetchCoupDetails.addValue("PostalCode", objProdDetailsReq.getPostalcode());
			fetchCoupDetails.addValue("PopulationCenterID", objProdDetailsReq.getPopCentId());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCoupDetails);
			
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(" Error Occured in SP usp_CouponBusinessCategoryDisplay ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else	{
				objCouponDetailsList = (List<CouponDetails>) resultFromProcedure.get("couponPopulationCenters");
				
				if (objCouponDetailsList != null && !objCouponDetailsList.isEmpty())	{
					objCouponsDetails = new CouponsDetails();
					objCouponsDetails.setCouponDetail(objCouponDetailsList);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		return objCouponsDetails;
	}

	/** 
	 * Method to get retailers for selected business category.
	 * @param objProdDetailsReq ProductDetailsRequest object
	 * @return CouponsDetails object containing retailer list
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public CouponsDetails getRetailerForBusinessCategory(ProductDetailsRequest objProdDetailsReq) throws ScanSeeException {
		final String methodName = "getRetailerForBusinessCategory in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> objCouponDetailsList = null;
		CouponsDetails objCouponsDetails = null;
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_CouponRetailerDisplay");
			simpleJdbcCall.returningResultSet("couponPopulationCenters", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCoupDetails = new MapSqlParameterSource();
			fetchCoupDetails.addValue(ApplicationConstants.USERID, objProdDetailsReq.getUserId());
			fetchCoupDetails.addValue("Latitude", objProdDetailsReq.getLatitude());
			fetchCoupDetails.addValue("Longitude", objProdDetailsReq.getLongitude());
			fetchCoupDetails.addValue("PostalCode", objProdDetailsReq.getPostalcode());
			fetchCoupDetails.addValue("PopulationCenterID", objProdDetailsReq.getPopCentId());
			fetchCoupDetails.addValue("BusinessCategoryID", objProdDetailsReq.getBusCatIDs());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCoupDetails);
			
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(" Error Occured in getRetailerForBusinessCategory method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else	{
				objCouponDetailsList = (List<CouponDetails>) resultFromProcedure.get("couponPopulationCenters");
				
				if (objCouponDetailsList != null && !objCouponDetailsList.isEmpty())	{
					objCouponsDetails = new CouponsDetails();
					objCouponsDetails.setCouponDetail(objCouponDetailsList);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		return objCouponsDetails;
	}

	/**
	 * Method to get categories for coupons for products.
	 * @param objProdDetailsReq ProductDetailsRequest object
	 * @return CouponsDetails object containing business categories
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public CouponsDetails getCoupProductCategory(ProductDetailsRequest objProdDetailsReq) throws ScanSeeException {
		final String methodName = "getCoupProductCategory in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> objCouponDetailsList = null;
		CouponsDetails objCouponsDetails = null;
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_CouponProductCategoryDisplay");
			simpleJdbcCall.returningResultSet("couponPopulationCenters", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCoupDetails = new MapSqlParameterSource();
			fetchCoupDetails.addValue(ApplicationConstants.USERID, objProdDetailsReq.getUserId());
			fetchCoupDetails.addValue("Latitude", objProdDetailsReq.getLatitude());
			fetchCoupDetails.addValue("Longitude", objProdDetailsReq.getLongitude());
			fetchCoupDetails.addValue("PostalCode", objProdDetailsReq.getPostalcode());
			fetchCoupDetails.addValue("PopulationCenterID", objProdDetailsReq.getPopCentId());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCoupDetails);
			
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(" Error Occured in getCoupProductCategory method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else	{
				objCouponDetailsList = (List<CouponDetails>) resultFromProcedure.get("couponPopulationCenters");
				
				if (objCouponDetailsList != null && !objCouponDetailsList.isEmpty())	{
					objCouponsDetails = new CouponsDetails();
					objCouponsDetails.setCouponDetail(objCouponDetailsList);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		return objCouponsDetails;
	}

	/**
	 * Method to get gallery coupons for All/Used/Expired in Location.
	 * @param objCoupDetReq ProductDetailsRequest object
	 * @return CouponsDetails object containing coupons 
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public CouponsDetails getGalleryCouponsByLocation(ProductDetailsRequest objCoupDetReq) throws ScanSeeException {
		final String methodName = "getGalleryCouponsByLocation in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> objCouponDetailsList = null;
		CouponsDetails objCouponsDetails = null;
		
		String storeProcedureName = null;
		
		if (objCoupDetReq.getType().equalsIgnoreCase("Claimed"))	{
			storeProcedureName = "usp_GalleryCouponsByLocation";
		}
		else if (objCoupDetReq.getType().equalsIgnoreCase("Used"))	{
			storeProcedureName = "usp_GalleryUsedCouponsByLocation";
		}
		else if (objCoupDetReq.getType().equalsIgnoreCase("Expired"))	{
			storeProcedureName = "usp_GalleryExpiredCouponsByLocation";
		}
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName(storeProcedureName);
			simpleJdbcCall.returningResultSet("couponPopulationCenters", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCoupDetails = new MapSqlParameterSource();
			fetchCoupDetails.addValue(ApplicationConstants.USERID, objCoupDetReq.getUserId());
			fetchCoupDetails.addValue("SearchKey", objCoupDetReq.getSearchKey());
			fetchCoupDetails.addValue("LowerLimit", objCoupDetReq.getLowerLimit());
			fetchCoupDetails.addValue("ScreenName", ApplicationConstants.ALLCOUPSCREENNAME);
			fetchCoupDetails.addValue(ApplicationConstants.MAINMENUID, objCoupDetReq.getMainMenuID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCoupDetails);
			
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(" Error Occured in SP " + storeProcedureName + " ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else	{
				objCouponDetailsList = (List<CouponDetails>) resultFromProcedure.get("couponPopulationCenters");
				final Integer maxCnt = (Integer) resultFromProcedure.get("MaxCnt");
				final Boolean nextpageFlag = (Boolean) resultFromProcedure.get("NxtPageFlag");
				
				if (objCouponDetailsList != null && !objCouponDetailsList.isEmpty())	{
					objCouponsDetails = new CouponsDetails();
					objCouponsDetails.setCouponDetail(objCouponDetailsList);
					objCouponsDetails.setMaxCnt(maxCnt);
					if (nextpageFlag)	{
						objCouponsDetails.setNextPageFlag(1);
					}
					else	{
						objCouponsDetails.setNextPageFlag(0);
					}
					
					int maxRowNum = objCouponDetailsList.get(0).getRowNumber();		
					int num1;
					for (int i = 1; i < objCouponDetailsList.size(); i++)	{
						num1 = objCouponDetailsList.get(i).getRowNumber();
						if (maxRowNum < num1)	{
							maxRowNum = num1;
						}
					}
					objCouponsDetails.setMaxRowNum(maxRowNum);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		return objCouponsDetails;
	}

	/**
	 * Method to get gallery coupons for All/Used/Expired in Products.
	 * @param objCoupDetReq ProductDetailsRequest object
	 * @return CouponsDetails object containing coupons sorted by category
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public CouponsDetails getGalleryCouponsByProduct(ProductDetailsRequest objCoupDetReq) throws ScanSeeException {
		final String methodName = "getGalleryCouponsByProduct in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> objCouponDetailsList = null;
		CouponsDetails objCouponsDetails = null;
		
		String storeProcedureName = null;
		
		if (objCoupDetReq.getType().equalsIgnoreCase("Claimed"))	{
			storeProcedureName = "usp_GalleryCouponsByProduct";
		}
		else if (objCoupDetReq.getType().equalsIgnoreCase("Used"))	{
			storeProcedureName = "usp_GalleryUsedCouponsByProduct";
		}
		else if (objCoupDetReq.getType().equalsIgnoreCase("Expired"))	{
			storeProcedureName = "usp_GalleryExpiredCouponsByProduct";
		}
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName(storeProcedureName);
			simpleJdbcCall.returningResultSet("couponPopulationCenters", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCoupDetails = new MapSqlParameterSource();
			fetchCoupDetails.addValue(ApplicationConstants.USERID, objCoupDetReq.getUserId());
			fetchCoupDetails.addValue("SearchKey", objCoupDetReq.getSearchKey());
			fetchCoupDetails.addValue("LowerLimit", objCoupDetReq.getLowerLimit());
			fetchCoupDetails.addValue("ScreenName", ApplicationConstants.ALLCOUPSCREENNAME);
			fetchCoupDetails.addValue(ApplicationConstants.MAINMENUID, objCoupDetReq.getMainMenuID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCoupDetails);
			
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(" Error Occured in SP " + storeProcedureName + " ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else	{
				objCouponDetailsList = (List<CouponDetails>) resultFromProcedure.get("couponPopulationCenters");
				
				if (objCouponDetailsList != null && !objCouponDetailsList.isEmpty())	{
					objCouponsDetails = new CouponsDetails();
					final Boolean nextpageFlag = (Boolean) resultFromProcedure.get("NxtPageFlag");
					final Integer maxCnt = (Integer) resultFromProcedure.get("MaxCnt");
					
					int maxRowNum = objCouponDetailsList.get(0).getRowNumber();	
					int num1;
					for (int i = 1; i < objCouponDetailsList.size(); i++)	{
						num1 = objCouponDetailsList.get(i).getRowNumber();
						if (maxRowNum < num1)	{
							maxRowNum = num1;
						}
					}
					
					objCouponsDetails.setCouponDetail(objCouponDetailsList);
					if (nextpageFlag)	{
						objCouponsDetails.setNextPageFlag(1);
					}
					else	{
						objCouponsDetails.setNextPageFlag(0);
					}
					objCouponsDetails.setMaxCnt(maxCnt);
					objCouponsDetails.setMaxRowNum(maxRowNum);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		return objCouponsDetails;
	}

	/**
	 * Method to get gallery HotDeals for All/Used/Expired in Location.
	 * @param objCoupDetReq ProductDetailsRequest object
	 * @return HotDealsListResultSet object containing HotDeals sorted by retailers and category.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public HotDealsListResultSet getGalleryHotDealByLocation(ProductDetailsRequest objCoupDetReq) throws ScanSeeException {
		final String methodName = "getGalleryHotDealByLocation in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsDetails> hdDetailsList = null;
		HotDealsListResultSet objHdDetails = null;
		
		String storeProcedureName = null;
		
		if (objCoupDetReq.getType().equalsIgnoreCase("Claimed"))	{
			storeProcedureName = "usp_GalleryHotDealsByLocation";
		}
		else if (objCoupDetReq.getType().equalsIgnoreCase("Used"))	{
			storeProcedureName = "usp_GalleryUsedHotDealsByLocation";
		}
		else if (objCoupDetReq.getType().equalsIgnoreCase("Expired"))	{
			storeProcedureName = "usp_GalleryExpiredHotDealsByLocation";
		}
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName(storeProcedureName);
			simpleJdbcCall.returningResultSet("couponPopulationCenters", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

			final MapSqlParameterSource fetchCoupDetails = new MapSqlParameterSource();
			fetchCoupDetails.addValue(ApplicationConstants.USERID, objCoupDetReq.getUserId());
			fetchCoupDetails.addValue("SearchKey", objCoupDetReq.getSearchKey());
			fetchCoupDetails.addValue("LowerLimit", objCoupDetReq.getLowerLimit());
			fetchCoupDetails.addValue("ScreenName", ApplicationConstants.ALLCOUPSCREENNAME);
			fetchCoupDetails.addValue(ApplicationConstants.MAINMENUID, objCoupDetReq.getMainMenuID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCoupDetails);
			
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(" Error Occured in SP " + storeProcedureName + " ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else	{
				hdDetailsList = (List<HotDealsDetails>) resultFromProcedure.get("couponPopulationCenters");
				
				final Integer maxCnt = (Integer) resultFromProcedure.get("MaxCnt");
				final Boolean nxtPageFlag = (Boolean) resultFromProcedure.get("NxtPageFlag");
				
				if (hdDetailsList != null && !hdDetailsList.isEmpty())	{
					objHdDetails = new HotDealsListResultSet();
					objHdDetails.setHdDetailsList(hdDetailsList);
					objHdDetails.setMaxCnt(maxCnt);
					if (nxtPageFlag)	{
						objHdDetails.setNextPage(1);
					}
					else	{
						objHdDetails.setNextPage(0);
					}
					int maxRowNum = hdDetailsList.get(0).getRowNumber();	
					int num1;
					for (int i = 1; i < hdDetailsList.size(); i++)	{
						num1 = hdDetailsList.get(i).getRowNumber();
						if (maxRowNum < num1)	{
							maxRowNum = num1;
						}
					}
					objHdDetails.setMaxRowNum(maxRowNum);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		return objHdDetails;
	}

	/**
	 * Method to get gallery Hot deals for All/Used/Expired in Products.
	 * @param objCoupDetReq ProductDetailsRequest object
	 * @return HotDealsListResultSet object containing hotdeals sorted by category
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public HotDealsListResultSet getGalleryHotDealByProduct(ProductDetailsRequest objCoupDetReq) throws ScanSeeException {
		final String methodName = "getGalleryHotDealByProduct in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsDetails> hdDetailsList = null;
		HotDealsListResultSet objHdDetails = null;
		
		String storeProcedureName = null;
		
		if (objCoupDetReq.getType().equalsIgnoreCase("Claimed"))	{
			storeProcedureName = "usp_GalleryHotDealsByProduct";
		}
		else if (objCoupDetReq.getType().equalsIgnoreCase("Used"))	{
			storeProcedureName = "usp_GalleryUsedHotDealsByProduct";
		}
		else if (objCoupDetReq.getType().equalsIgnoreCase("Expired"))	{
			storeProcedureName = "usp_GalleryExpiredHotDealsByProduct";
		}
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName(storeProcedureName);
			simpleJdbcCall.returningResultSet("couponPopulationCenters", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

			final MapSqlParameterSource fetchCoupDetails = new MapSqlParameterSource();
			fetchCoupDetails.addValue(ApplicationConstants.USERID, objCoupDetReq.getUserId());
			fetchCoupDetails.addValue("SearchKey", objCoupDetReq.getSearchKey());
			fetchCoupDetails.addValue("LowerLimit", objCoupDetReq.getLowerLimit());
			fetchCoupDetails.addValue("ScreenName", ApplicationConstants.ALLCOUPSCREENNAME);
			fetchCoupDetails.addValue(ApplicationConstants.MAINMENUID, objCoupDetReq.getMainMenuID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCoupDetails);
			
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(" Error Occured in SP " + storeProcedureName + " ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else	{
				hdDetailsList = (List<HotDealsDetails>) resultFromProcedure.get("couponPopulationCenters");
				final Integer maxCnt = (Integer) resultFromProcedure.get("MaxCnt");
				final Boolean nxtPageFlag = (Boolean) resultFromProcedure.get("NxtPageFlag");
				
				if (hdDetailsList != null && !hdDetailsList.isEmpty())	{
					objHdDetails = new HotDealsListResultSet();
					objHdDetails.setHdDetailsList(hdDetailsList);
					objHdDetails.setMaxCnt(maxCnt);
					if (nxtPageFlag)	{
						objHdDetails.setNextPage(1);
					}
					else	{
						objHdDetails.setNextPage(0);
					}
					int maxRowNum = hdDetailsList.get(0).getRowNumber();	
					int num1;
					for (int i = 1; i < hdDetailsList.size(); i++)	{
						num1 = hdDetailsList.get(i).getRowNumber();
						if (maxRowNum < num1)	{
							maxRowNum = num1;
						}
					}
					objHdDetails.setMaxRowNum(maxRowNum);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		return objHdDetails;
	}

}
