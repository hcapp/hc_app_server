package com.scansee.mygallery.dao;

import java.util.ArrayList;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.CategoryInfo;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.CouponsDetails;
import com.scansee.common.pojos.HotDealsListResultSet;
import com.scansee.common.pojos.LoyaltyDetail;
import com.scansee.common.pojos.ProductDetailsRequest;
import com.scansee.common.pojos.RebateDetail;

/**
 * The Interface for MyGalleryDAO. ImplementedBy {@link MyGalleryDAOImpl}
 * 
 * @author sowjanya_d
 */
public interface MyGalleryDAO
{
	/**
	 * This method get the my gallery CLR from the database.
	 * 
	 * @param clrDetails
	 *            as request
	 * @return String XMl with list of CLRDetails for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	//CLRDetails getMyGalleryInfo(CLRDetails clrDetails) throws ScanSeeException;

	/**
	 * This method get the my gallery of Type All CLR info from the database.
	 * 
	 * @param clrDetails
	 *            as request
	 * @return String XMl with list of CLRDetails for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 *//*
	CLRDetails getMyGalleryAllTypeInfo(CLRDetails clrDetails) throws ScanSeeException;*/

	/**
	 * This method get the my gallery of Type Expired CLR from the database.
	 * 
	 * @param clrDetails
	 *            as request
	 * @return String XMl with list of CLRDetails for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
//	CLRDetails getMyGalleryExpiredTypeInfo(CLRDetails clrDetails) throws ScanSeeException;

	/**
	 * This method get the my gallery of Type Used CLR from the database.
	 * 
	 * @param clrDetails
	 *            as request
	 * @return String XMl with list of CLRDetails for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	//CLRDetails getMyGalleryUsedTypeInfo(CLRDetails clrDetails) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods based on user id and coupon id.
	 * 
	 * @param couponDetailsObj
	 *            request parameter.
	 * @return String XML with my gallery details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String userRedeemCoupon(CouponDetails couponDetailsObj) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to get the c,l,r based on user id.
	 * 
	 * @param loyaltyDetailObj
	 *            request parameter.
	 * @return String XMLwith success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String userRedeemLoyalty(LoyaltyDetail loyaltyDetailObj) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to get the c,l,r based on user id.
	 * 
	 * @param rebateDetail
	 *            request parameter.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String userRedeemRebate(RebateDetail rebateDetail) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to remove the used coupon.
	 * 
	 * @param couponDetailsObj
	 *            request parameter.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String deleteUsedCoupon(CouponDetails couponDetailsObj) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to remove the used loyalty.
	 * 
	 * @param loyaltyDetailObj
	 *            request parameter.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String deleteUsedLoyalty(LoyaltyDetail loyaltyDetailObj) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to remove the used rebate.
	 * 
	 * @param rebateDetailObj
	 *            request parameter.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String deleteUsedRebate(RebateDetail rebateDetailObj) throws ScanSeeException;
	
	 CLRDetails getMyGalleryAllTypeLoyaltyInfo(CLRDetails clrDetails) throws ScanSeeException;
	 
	 CLRDetails getMyGalleryAllTypeCouponInfo(CLRDetails clrDetails) throws ScanSeeException;
	 
	 CLRDetails getMyGalleryExpiredTypeCouponInfo(CLRDetails clrDetails) throws ScanSeeException;
	 CLRDetails getMyGalleryExpiredTypeLoyaltyInfo(CLRDetails clrDetails) throws ScanSeeException;
	 
	  CLRDetails getMyGalleryUsedTypeCouponInfo(CLRDetails clrDetails) throws ScanSeeException;
	  
	  CLRDetails getMyGalleryUsedTypeLoyaltyInfo(CLRDetails clrDetails) throws ScanSeeException;
	  
	  CLRDetails getMyGalleryCouponInfo(CLRDetails clrDetails) throws ScanSeeException;
	  CLRDetails getMyGalleryLoyaltyInfo(CLRDetails clrDetails) throws ScanSeeException;
	  
	  /**
		 * This DAO Interface is used to retrive Categories
		 * 
		 * @param 
		 * @return category list.
		 * @throws ScanSeeException
		 *             The exceptions are caught and a ScanSee Exception defined for
		 *             the application will be thrown which is caught in the
		 *             Controller layer
		 */
		ArrayList<CategoryInfo> retriveCategory() throws ScanSeeException;
		/**
		 * Method for fetching coupon retailer list for searching .
		 * layer.
		 * 
		 * @param 
		 * @return response as String contails retailer id and name
		 */
		
		ArrayList<LoyaltyDetail>  getCouponRetailer()throws ScanSeeException;
		
		/**
		 * Method for fetching loyalty retailer list for searching .
		 * layer.
		 * 
		 * @param 
		 * @return response as String contails retailer id and name
		 */
		
		ArrayList<LoyaltyDetail>  getLoyaltyRetailer() throws ScanSeeException;
		
		/**
		 * Method to fetch all coupons for Product from database.
		 * @param objCoupDetReq of ProductDetailsRequest object.
		 * @return CouponsDetails object containing coupon details
		 * @throws ScanSeeException
		 *             If any exception occurs ScanSeeException will be thrown.
		 */
		CouponsDetails getAllCouponsByLocation(ProductDetailsRequest objCoupDetReq) throws ScanSeeException;
		
		/**
		 * Method to fetch all coupons for Product form database.
		 * @param objCoupDetReq of ProductDetailsRequest object.
		 * @return CouponsDetails object containing coupon details
		 * @throws ScanSeeException
		 *             If any exception occurs ScanSeeException will be thrown.
		 */
		CouponsDetails getAllCouponsByProduct(ProductDetailsRequest objCoupDetReq) throws ScanSeeException;
		
		/**
		 * Method to fetch population centers for coupons for Location.
		 * @param userId as request
		 * @return CouponsDetails object containing population center list
		 * @throws ScanSeeException
		 *             If any exception occurs ScanSeeException will be thrown.
		 */
		CouponsDetails getCoupPopulationCentresForLoc(Integer userId) throws ScanSeeException;
		
		/**
		 * Method to get population centers for coupons for product.
		 * @param userId as request
		 * @return CouponsDetails object containing population center list
		 * @throws ScanSeeException
		 *             If any exception occurs ScanSeeException will be thrown.
		 */
		CouponsDetails getCoupPopulationCentresForProd(Integer userId) throws ScanSeeException;
		
		/**
		 * Method to get business categories for coupons for Locations.
		 * @param objProdDetailsReq ProductDetailsRequest object
		 * @return CouponsDetails object containing business categories
		 * @throws ScanSeeException
		 *             If any exception occurs ScanSeeException will be thrown.
		 */
		CouponsDetails getCoupLocationBusinessCategory(ProductDetailsRequest objProdDetailsReq) throws ScanSeeException;
		
		/** 
		 * Method to get retailers for selected business category.
		 * @param objProdDetailsReq ProductDetailsRequest object
		 * @return CouponsDetails object containing retailer list
		 * @throws ScanSeeException
		 *             If any exception occurs ScanSeeException will be thrown.
		 */
		CouponsDetails getRetailerForBusinessCategory(ProductDetailsRequest objProdDetailsReq) throws ScanSeeException;
		
		/**
		 * Method to get categories for coupons for products.
		 * @param objProdDetailsReq  ProductDetailsRequest object
		 * @return CouponsDetails object containing business categories
		 * @throws ScanSeeException
		 *             If any exception occurs ScanSeeException will be thrown.
		 */
		CouponsDetails getCoupProductCategory(ProductDetailsRequest objProdDetailsReq) throws ScanSeeException;
		
		/**
		 * Method to get gallery coupons for All/Used/Expired in Location.
		 * @param objCoupDetReq ProductDetailsRequest object
		 * @return CouponsDetails object containing coupons 
		 * @throws ScanSeeException
		 *             If any exception occurs ScanSeeException will be thrown.
		 */
		CouponsDetails getGalleryCouponsByLocation(ProductDetailsRequest objCoupDetReq) throws ScanSeeException;
		
		/**
		 * Method to get gallery coupons for All/Used/Expired in Products.
		 * @param objCoupDetReq ProductDetailsRequest object
		 * @return CouponsDetails object containing coupons sorted by category
		 * @throws ScanSeeException
		 *             If any exception occurs ScanSeeException will be thrown.
		 */
		CouponsDetails getGalleryCouponsByProduct(ProductDetailsRequest objCoupDetReq) throws ScanSeeException;
		
		/**
		 * Method to get gallery HotDeals for All/Used/Expired in Location.
		 * @param objCoupDetReq ProductDetailsRequest object
		 * @return HotDealsListResultSet object containing hotdeals sorted by retailers and category.
		 * @throws ScanSeeException
		 *             If any exception occurs ScanSeeException will be thrown.
		 */
		HotDealsListResultSet getGalleryHotDealByLocation(ProductDetailsRequest objCoupDetReq) throws ScanSeeException;
		
		/**
		 * Method to get gallery Hot deals for All/Used/Expired in Products.
		 * @param objCoupDetReq ProductDetailsRequest object
		 * @return HotDealsListResultSet object containing hotdeals sorted by category
		 * @throws ScanSeeException
		 *             If any exception occurs ScanSeeException will be thrown.
		 */
		HotDealsListResultSet getGalleryHotDealByProduct(ProductDetailsRequest objCoupDetReq) throws ScanSeeException;
		
}
