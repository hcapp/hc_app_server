package com.scansee.mygallery.service;

import com.scansee.common.exception.ScanSeeException;

/**
 * The MyGalleryService interface. ImplementedBy {@link MyGalleryServiceImpl}
 * 
 * @author sowjanya_d
 */
public interface MyGalleryService
{

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to get the c,l,r based on user id.
	 * 
	 * @param xml
	 *            request parameter.
	 * @return String XML with my gallery details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String getMyGalleryInfo(String xml) throws ScanSeeException;
	
	
	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods based on user id and coupon id.
	 * 
	 * @param xml
	 *            request parameter.
	 * @return String XML with my gallery details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String userRedeemCoupon(String xml) throws ScanSeeException;

	
	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to get the c,l,r based on user id.
	 * 
	 * @param xml
	 *            request parameter.
	 * @return String XML with my gallery details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String userRedeemLoyalty(String xml) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to get the c,l,r based on user id.
	 * 
	 * @param xml
	 *            request parameter.
	 * @return String XML with my gallery details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String userRedeemRebate(String xml) throws ScanSeeException;
	
	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to delete used coupon.
	 * 
	 * @param xml
	 *            request parameter.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String deleteUsedCoupon(String xml) throws ScanSeeException;
	

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to delete used loyalty.
	 * 
	 * @param xml
	 *            request parameter.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String deleteUsedLoyalty(String xml) throws ScanSeeException;
	
	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to delete used rebate.
	 * 
	 * @param xml
	 *            request parameter.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String deleteUsedRebate(String xml) throws ScanSeeException;

	/**
	 * The service method for retriving category list.
	 * 
	 * @param 
	 * @return String containing category list.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String retriveCategory() throws ScanSeeException;

	/**
	 * Method for fetching coupon retailer list for searching .
	 * layer.
	 * 
	 * @param 
	 * @return response as String contails retailer id and name
	 */
	
	String getCouponRetailer()throws ScanSeeException;
	
	/**
	 * Method for fetching loyalty retailer list for searching.
	 * 
	 * @param 
	 * @return response as String contains retailer id and name
	 */
	
	String getLoyaltyRetailer() throws ScanSeeException;
	
	/**
	 * Method to get all coupons for Product.
	 * @param xml as request
	 * @return xml containing coupon details
	 * @throws ScanSeeException is thrown
	 */
	String getAllCouponsByProduct(String xml) throws ScanSeeException;
	
	/**
	 * Method to get all coupons for Location.
	 * @param xml as request
	 * @return xml containing coupon details
	 * @throws ScanSeeException is thrown
	 */
	String getAllCouponsByLocation(String xml) throws ScanSeeException;
	
	/**
	 * Method to get population centers for coupons for Location.
	 * @param userId as request
	 * @return xml containing population center list
	 * @throws ScanSeeException is thrown
	 */
	String getCoupPopulationCentresForLoc(Integer userId) throws ScanSeeException;
	
	/**
	 * Method to get population centers for coupons for product.
	 * @param userId as request
	 * @return xml containing population center list
	 * @throws ScanSeeException is thrown
	 */
	String getCoupPopulationCentresForProd(Integer userId) throws ScanSeeException;
	
	/**
	 * Method to get business categories for coupons for Locations.
	 * @param xml as request
	 * @return xml containing business categories
	 * @throws ScanSeeException is thrown
	 */
	String getCoupLocationBusinessCategory(String xml) throws ScanSeeException;
	
	/**
	 * Method to get retailers for selected business category.
	 * @param xml as request
	 * @return xml containing retailer list
	 * @throws ScanSeeException is thrown
	 */
	String getRetailerForBusinessCategory(String xml) throws ScanSeeException;
	
	/**
	 * Method to get categories for coupons for products.
	 * @param xml as request
	 * @return xml containing business categories
	 * @throws ScanSeeException is thrown
	 */
	String getCoupProductCategory(String xml) throws ScanSeeException;
	
	/**
	 * Method to get gallery coupons for All/Used/Expired in Location.
	 * @param xml as request
	 * @return xml containing coupons 
	 * @throws ScanSeeException is thrown
	 */
	String getGalleryCouponsByLocation(String xml) throws ScanSeeException;
	
	/**
	 * Method to get gallery coupons for All/Used/Expired in Products.
	 * @param xml as request
	 * @return xml containing coupons sorted by category
	 * @throws ScanSeeException is thrown
	 */
	String getGalleryCouponsByProduct(String xml) throws ScanSeeException;
	
	/**
	 * Method to get gallery HotDeals for All/Used/Expired in Location.
	 * @param xml as request
	 * @return xml containing hotdeals sorted by retailers and category.
	 * @throws ScanSeeException is thrown
	 */
	String getGalleryHotDealByLocation(String xml) throws ScanSeeException;
	
	/**
	 * Method to get gallery Hot deals for All/Used/Expired in Products.
	 * @param xml as request
	 * @return xml containing hotdeals sorted by category
	 * @throws ScanSeeException is thrown
	 */
	String getGalleryHotDealByProduct(String xml) throws ScanSeeException;
}
