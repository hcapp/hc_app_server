package com.scansee.mygallery.service;

import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.pojos.RetailerDetail;

/**
 * This class for sorting wish list history based on date.
 * 
 * @author Shyamsundhar_hm
 */
public class MygalleryCouponSort implements Comparator<RetailerDetail>
{
	
	private static final Logger log = LoggerFactory.getLogger(MygalleryCouponSort.class.getName());

	/**
	 * This method is to compare two products.
	 * 
	 * @param wishListResultSet1
	 *            -As parameter
	 * @param wishListResultSet2
	 *            -As parameter
	 * @return scanDateComp
	 */
	public int compare(RetailerDetail cLRDetailslist1, RetailerDetail cLRDetailslist2)
	{

		String catName1;
		String catName2;
		int scanDateComp = 0;
		

		try
		{
			catName1 = cLRDetailslist1.getCateName();
			catName2 = cLRDetailslist2.getCateName();
			scanDateComp = catName1.compareTo(catName2);

		}
		catch (Exception exception)
		{
			log.info("exception in MygalleryCouponSort " + exception);
		}

		return scanDateComp;

	}

}
