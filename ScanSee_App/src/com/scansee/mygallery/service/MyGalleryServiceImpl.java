package com.scansee.mygallery.service;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.helper.BaseDAO;
import com.scansee.common.helper.XstreamParserHelper;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.CategoryDetail;
import com.scansee.common.pojos.CategoryInfo;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.CouponsDetails;
import com.scansee.common.pojos.HotDealsListResultSet;
import com.scansee.common.pojos.LoyaltyDetail;
import com.scansee.common.pojos.ProductDetailsRequest;
import com.scansee.common.pojos.RebateDetail;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.common.util.Utility;
import com.scansee.firstuse.dao.FirstUseDAO;
import com.scansee.mygallery.dao.MyGalleryDAO;

/**
 * The MyGalleryServiceImpl implementation class implements the MyGalleryService
 * Interface. The methods of this class are called from the my gallery
 * Controller Layer. The methods contain call to the my gallery DAO layer.
 * 
 * @author sowjanya_d
 */
public class MyGalleryServiceImpl implements MyGalleryService
{
	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(MyGalleryServiceImpl.class);

	/**
	 * Instance variable for my gallery DAO instance.
	 */

	private MyGalleryDAO myGalleryDao;
	
	/**
	 * Variable type for FirstUse.
	 */
	private FirstUseDAO firstUseDao;

	/**
	 * for setting myGalleryDao.
	 * 
	 * @param myGalleryDao
	 */
	public void setMyGalleryDao(MyGalleryDAO myGalleryDao)
	{
		this.myGalleryDao = myGalleryDao;
	}

	/**
	 * Instance variable for baseDao.
	 */
	@SuppressWarnings("unused")
	private BaseDAO baseDao;

	/**
	 * this method for setting base DAO. Instance variable for baseDao.
	 */

	public void setBaseDao(BaseDAO baseDao)
	{
		this.baseDao = baseDao;
	}
	
	/**
	 * Setter method for FirstUseDAO.
	 * 
	 * @param firstUseDAO
	 *            the object of type FirstUseDAO
	 */
	public void setFirstUseDao(FirstUseDAO firstUseDao)
	{
		this.firstUseDao = firstUseDao;
	}

	/**
	 * The service method for fetching my gallery information. This method
	 * validates the userId, if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            for which has user id and type based on this it fetches the my
	 *            gallery info.
	 * @return XML containing my gallery information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String getMyGalleryInfo(String xml) throws ScanSeeException
	{
		final String methodName = "getMyGalleryInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		CLRDetails CLRDetailsObj = null;

		final CLRDetails clrDetails = (CLRDetails) parser.parseXmlToObject(xml);

		if (null == clrDetails.getUserId() || clrDetails.getType() == null)

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			Integer mainMenuID;
			if(clrDetails.getMainMenuID() == null)	{
				UserTrackingData objUserTrackingData = new UserTrackingData();
				objUserTrackingData.setUserID(clrDetails.getUserId());
				objUserTrackingData.setModuleID(clrDetails.getModuleID());
				objUserTrackingData.setPostalCode(clrDetails.getZipcode());
				objUserTrackingData.setLatitude(clrDetails.getLat());
				objUserTrackingData.setLongitude(clrDetails.getLng());
				mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);
				clrDetails.setMainMenuID(mainMenuID);
			}
			else	{
				mainMenuID = clrDetails.getMainMenuID();
			}
			
			if (clrDetails.getType().equalsIgnoreCase("Allcoups"))
			{
				// Type - All gallery coupon information
				CLRDetailsObj = myGalleryDao.getMyGalleryAllTypeCouponInfo(clrDetails);
				if (CLRDetailsObj != null)
				{
					CLRDetailsObj = MygalleryHelper.getCouponByCategory(CLRDetailsObj);
				}
			}
			else if (clrDetails.getType().equalsIgnoreCase("Allloys"))
			{
				// Type - All gallery loyalty information
				CLRDetailsObj = myGalleryDao.getMyGalleryAllTypeLoyaltyInfo(clrDetails);
				if (CLRDetailsObj != null)
				{
					CLRDetailsObj = MygalleryHelper.getLoyaltysByRetCatgory(CLRDetailsObj);
				}

			}
			else if (clrDetails.getType().equalsIgnoreCase("Expiredcoups"))
			{
				// Type - Expired gallery coupon information
				CLRDetailsObj = myGalleryDao.getMyGalleryExpiredTypeCouponInfo(clrDetails);
				if (CLRDetailsObj != null)
				{
					CLRDetailsObj = MygalleryHelper.getCouponByCategory(CLRDetailsObj);

				}
			}
			else if (clrDetails.getType().equalsIgnoreCase("Expiredloys"))
			{
				// Type - Expired gallery loyalty information
				CLRDetailsObj = myGalleryDao.getMyGalleryExpiredTypeLoyaltyInfo(clrDetails);
				if (CLRDetailsObj != null)
				{
					CLRDetailsObj = MygalleryHelper.getLoyaltysByRetCatgory(CLRDetailsObj);
				}
			}
			else if (clrDetails.getType().equalsIgnoreCase("Usedcoups"))
			{
				// Type - Used gallery coupon information
				CLRDetailsObj = myGalleryDao.getMyGalleryUsedTypeCouponInfo(clrDetails);
				if (CLRDetailsObj != null)
				{
					CLRDetailsObj = MygalleryHelper.getCouponByCategory(CLRDetailsObj);

				}
			}
			else if (clrDetails.getType().equalsIgnoreCase("Usedloys"))
			{
				// Type - Used gallery loyalty information
				CLRDetailsObj = myGalleryDao.getMyGalleryUsedTypeLoyaltyInfo(clrDetails);
				if (CLRDetailsObj != null)
				{
					CLRDetailsObj = MygalleryHelper.getLoyaltysByRetCatgory(CLRDetailsObj);
				}
			}
			else if (clrDetails.getType().equalsIgnoreCase("Mycoups"))
			{
				// Type - my gallery coupon information
				CLRDetailsObj = myGalleryDao.getMyGalleryCouponInfo(clrDetails);

				if (CLRDetailsObj == null)
				{

					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.COUPONNOTFOUNDTEXT);
					return response;
				}
				else
				{

					CLRDetailsObj = MygalleryHelper.getCouponByCategory(CLRDetailsObj);

				}
			}
			else if (clrDetails.getType().equalsIgnoreCase("Myloys"))
			{
				// Type - my gallery loyalty information
				CLRDetailsObj = myGalleryDao.getMyGalleryLoyaltyInfo(clrDetails);
				if (CLRDetailsObj == null)
				{

					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.LOYALTYNOTFOUNDTEXT);
					return response;
				}
				else
				{

					CLRDetailsObj = MygalleryHelper.getLoyaltysByRetCatgory(CLRDetailsObj);
				}
			}

			if (null != CLRDetailsObj)
			{
				CLRDetailsObj.setMainMenuID(mainMenuID);
				response = XstreamParserHelper.produceXMlFromObject(CLRDetailsObj);
			}
			else
			{
				String mmID = null;
				if(mainMenuID != null){
					mmID = mainMenuID.toString();
				} else{
					mmID = ApplicationConstants.STRING_ZERO;
				}
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT, "mainMenuID", mmID);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method for redeem coupon . This method validates the userId,
	 * if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            for which has user id and type based on this it redeems coupon
	 * @return String containing success or failure in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String userRedeemCoupon(String xml) throws ScanSeeException
	{
		final String methodName = "userRedeemCoupon ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		CouponDetails couponDetailsObj = null;
		couponDetailsObj = (CouponDetails) parser.parseXmlToObject(xml);
		if (null == couponDetailsObj.getUserId())

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			response = myGalleryDao.userRedeemCoupon(couponDetailsObj);

			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{

				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.COUPONREDEEMRESPONSETEXT);
			}
			else if (ApplicationConstants.CLRALLREADYREDEEMRESPONSETEXT.equalsIgnoreCase(response))
			{

				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.CLRALLREADYREDEEMRESPONSETEXT);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		return response;
	}

	/**
	 * The service method for redeem loyalty . This method validates the userId,
	 * if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            for which has user id and type based on this it redeems coupon
	 * @return String containing success or failure in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String userRedeemLoyalty(String xml) throws ScanSeeException
	{
		final String methodName = "userRedeemLoyalty";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		LoyaltyDetail loyaltyDetailObj = null;
		loyaltyDetailObj = (LoyaltyDetail) parser.parseXmlToObject(xml);
		if (null == loyaltyDetailObj.getUserId())

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			response = myGalleryDao.userRedeemLoyalty(loyaltyDetailObj);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{

				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.LOYALTYREDEEMRESPONSETEXT);
			}
			else if (ApplicationConstants.CLRALLREADYREDEEMRESPONSETEXT.equalsIgnoreCase(response))
			{

				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.CLRALLREADYREDEEMRESPONSETEXT);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		return response;
	}

	/**
	 * The service method for redeem rebate . This method validates the userId,
	 * if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            for which has user id and type based on this it redeems coupon
	 * @return String containing success or failure in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String userRedeemRebate(String xml) throws ScanSeeException
	{

		final String methodName = "userRedeemRebate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		RebateDetail rebateDetailObj = null;
		rebateDetailObj = (RebateDetail) parser.parseXmlToObject(xml);
		if (null == rebateDetailObj.getUserId())

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			response = myGalleryDao.userRedeemRebate(rebateDetailObj);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{

				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.REBATEREDEEMRESPONSETEXT);
			}
			else if (ApplicationConstants.CLRALLREADYREDEEMRESPONSETEXT.equalsIgnoreCase(response))
			{

				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.CLRALLREADYREDEEMRESPONSETEXT);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		return response;
	}

	/**
	 * The service method for deleting used coupon . This method validates the
	 * userId, if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            for which has user id and used coupon id
	 * @return String containing success or failure in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String deleteUsedCoupon(String xml) throws ScanSeeException
	{
		final String methodName = "deleteUsedCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		CouponDetails couponDetailsObj = null;
		couponDetailsObj = (CouponDetails) parser.parseXmlToObject(xml);
		if (null == couponDetailsObj.getUserId())

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			response = myGalleryDao.deleteUsedCoupon(couponDetailsObj);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{

				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.DELETEPRODUCTTEXT);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		return response;
	}

	/**
	 * The service method for deleting used loyalty . This method validates the
	 * userId, if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            for which has user id and used loyalty id
	 * @return String containing success or failure in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@Override
	public String deleteUsedLoyalty(String xml) throws ScanSeeException
	{
		final String methodName = "deleteUsedLoyalty";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		LoyaltyDetail loyaltyDetailObj = null;
		loyaltyDetailObj = (LoyaltyDetail) parser.parseXmlToObject(xml);
		if (null == loyaltyDetailObj.getUserId())

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			response = myGalleryDao.deleteUsedLoyalty(loyaltyDetailObj);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{

				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.DELETEPRODUCTTEXT);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		return response;
	}

	/**
	 * The service method for deleting used rebate . This method validates the
	 * userId, if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            for which has user id and used rebate id
	 * @return String containing success or failure in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String deleteUsedRebate(String xml) throws ScanSeeException
	{
		final String methodName = "deleteUsedRebate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		RebateDetail rebateDetailObj = null;
		rebateDetailObj = (RebateDetail) parser.parseXmlToObject(xml);
		if (null == rebateDetailObj.getUserId())

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			response = myGalleryDao.deleteUsedRebate(rebateDetailObj);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{

				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.DELETEPRODUCTTEXT);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		return response;
	}

	/**
	 * The service method for retrieving category list.
	 * 
	 * @param
	 * @return String containing category list.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	public String retriveCategory() throws ScanSeeException
	{
		final String methodName = "retriveCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		CategoryDetail categoryDetails = null;
		ArrayList<CategoryInfo> categoryInfoList = null;
		categoryInfoList = myGalleryDao.retriveCategory();

		if (null != categoryInfoList && !categoryInfoList.isEmpty())
		{
			categoryDetails = new CategoryDetail();
			categoryDetails.setSearchCategorys(categoryInfoList);
			response = XstreamParserHelper.produceXMlFromObject(categoryDetails);
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}
		return response;
	}

	/**
	 * Method for fetching coupon retailer list for searching . layer.
	 * 
	 * @param
	 * @return response as String contains retailer id and name
	 */
	@Override
	public String getCouponRetailer() throws ScanSeeException
	{
		final String methodName = "getCouponRetailer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		ArrayList<LoyaltyDetail> retCategorylst = null;

		retCategorylst = myGalleryDao.getCouponRetailer();

		if (null != retCategorylst && !retCategorylst.isEmpty())
		{
			response = XstreamParserHelper.produceXMlFromObject(retCategorylst);
			response = response.replaceAll("<list>", "<CoupRetLst>");
			response = response.replaceAll("</list>", "</CoupRetLst>");
			response = response.replaceAll("<LoyaltyDetail>", "<CoupRet>");
			response = response.replaceAll("</LoyaltyDetail>", "</CoupRet>");

		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}
		return response;
	}

	/**
	 * Method for fetching coupon retailer list for searching . layer.
	 * 
	 * @param
	 * @return response as String contains retailer id and name
	 */
	@Override
	public String getLoyaltyRetailer() throws ScanSeeException
	{
		final String methodName = "getLoyaltyRetailer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		ArrayList<LoyaltyDetail> retCategorylst = null;

		retCategorylst = myGalleryDao.getLoyaltyRetailer();

		if (null != retCategorylst && !retCategorylst.isEmpty())
		{

			response = XstreamParserHelper.produceXMlFromObject(retCategorylst);
			response = response.replaceAll("<list>", "<LoyRetLst>");
			response = response.replaceAll("</list>", "</LoyRetLst>");
			response = response.replaceAll("<LoyaltyDetail>", "<LoyRet>");
			response = response.replaceAll("</LoyaltyDetail>", "</LoyRet>");
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}
		return response;
	}

	/**
	 * Method to get all coupons for Location.
	 * Uses XstreamParserHelper class to get request parameter from XML to object conversion.
	 * @param xml as reqest.
	 * @return xml containing coupon details
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String getAllCouponsByLocation(String xml) throws ScanSeeException {
		final String methodName = "getAllCouponsByProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		CouponsDetails objCouponsDetails = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetailsRequest objCoupDetailsReq = (ProductDetailsRequest) parser.parseXmlToObject(xml);
		if (objCoupDetailsReq.getUserId() == null || (objCoupDetailsReq.getMainMenuID() == null && objCoupDetailsReq.getModuleID() == null))	{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else	{
			Integer mainMenuID;
			if (objCoupDetailsReq.getMainMenuID() == null)	{
				UserTrackingData objUserTrackingData = new UserTrackingData();
				objUserTrackingData.setUserID(objCoupDetailsReq.getUserId());
				objUserTrackingData.setModuleID(objCoupDetailsReq.getModuleID());
				objUserTrackingData.setPostalCode(objCoupDetailsReq.getPostalcode());
				objUserTrackingData.setLatitude(objCoupDetailsReq.getLatitude());
				objUserTrackingData.setLongitude(objCoupDetailsReq.getLongitude());
				mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);
				objCoupDetailsReq.setMainMenuID(mainMenuID);
			}
			else	{
				mainMenuID = objCoupDetailsReq.getMainMenuID();
			}

			objCouponsDetails = myGalleryDao.getAllCouponsByLocation(objCoupDetailsReq);
			
			if (objCouponsDetails != null && (!objCouponsDetails.getCouponDetail().isEmpty() && objCouponsDetails.getCouponDetail() != null))	{
				CouponsDetails objCouponsDetailsRetSort = new CouponsDetails();
				//To sort coupons by retailers distance
				objCouponsDetailsRetSort = MygalleryHelper.sortAllCouponsRetailersByDistanceForLocation(objCouponsDetails.getCouponDetail());
				
				CouponsDetails objCouponDetailsSorted = new CouponsDetails();
				ArrayList<RetailersDetails> retDetailsList = new ArrayList<RetailersDetails>();			
				RetailersDetails objRetDetails = null;
				CouponsDetails coupsDetails = null;
				
				//To sort retailer location by distance
				for (int i = 0; i < objCouponsDetailsRetSort.getRetDetailsList().size(); i++)	{
					coupsDetails = new CouponsDetails();
					objRetDetails = new RetailersDetails();
					coupsDetails = MygalleryHelper.sortRetailerCoupByDist(objCouponsDetailsRetSort.getRetDetailsList().get(i).getCouponDetailsList());
					objRetDetails.setRetName(objCouponsDetailsRetSort.getRetDetailsList().get(i).getRetName());
					objRetDetails.setRetId(objCouponsDetailsRetSort.getRetDetailsList().get(i).getRetId());
					objRetDetails.setRetDetailsList(coupsDetails.getRetDetailsList());
					retDetailsList.add(objRetDetails);
				}
				objCouponDetailsSorted.setRetDetailsList(retDetailsList);
				objCouponDetailsSorted.setNextPageFlag(objCouponsDetails.getNextPageFlag());
				objCouponDetailsSorted.setMainMenuID(mainMenuID);
				objCouponDetailsSorted.setMaxCnt(objCouponsDetails.getMaxCnt());
								
				response = XstreamParserHelper.produceXMlFromObject(objCouponDetailsSorted);
			}
			else	{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT, "mainMenuID", mainMenuID.toString());
			}
		}
		return response;
	}

	/**
	 * Method to get all coupons for Product.
	 * Uses XstreamParserHelper class to get request parameter from XML to object conversion.
	 * @param xml as reqest.
	 * @return xml containing coupon details
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String getAllCouponsByProduct(String xml) throws ScanSeeException {
		final String methodName = "getAllCouponsByProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		CouponsDetails objCouponsDetails = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetailsRequest objCoupDetailsReq = (ProductDetailsRequest) parser.parseXmlToObject(xml);
		if (objCoupDetailsReq.getUserId() == null || (objCoupDetailsReq.getMainMenuID() == null && objCoupDetailsReq.getModuleID() == null))	{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else	{
			Integer mainMenuID;
			if (objCoupDetailsReq.getMainMenuID() == null)	{
				UserTrackingData objUserTrackingData = new UserTrackingData();
				objUserTrackingData.setUserID(objCoupDetailsReq.getUserId());
				objUserTrackingData.setModuleID(objCoupDetailsReq.getModuleID());
				objUserTrackingData.setPostalCode(objCoupDetailsReq.getPostalcode());
				objUserTrackingData.setLatitude(objCoupDetailsReq.getLatitude());
				objUserTrackingData.setLongitude(objCoupDetailsReq.getLongitude());
				mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);
				objCoupDetailsReq.setMainMenuID(mainMenuID);
			}
			else	{
				mainMenuID = objCoupDetailsReq.getMainMenuID();
			}
			
			objCouponsDetails = myGalleryDao.getAllCouponsByProduct(objCoupDetailsReq);
			
			//To sort coupons by category
			if (objCouponsDetails != null && (!objCouponsDetails.getCouponDetail().isEmpty() && objCouponsDetails.getCouponDetail() != null))	{
				CouponsDetails objCouponDetails = new CouponsDetails();
				objCouponDetails = MygalleryHelper.sortAllCouponsByCatForProduct(objCouponsDetails.getCouponDetail());
				objCouponDetails.setNextPageFlag(objCouponsDetails.getNextPageFlag());
				objCouponDetails.setMainMenuID(mainMenuID);
				objCouponDetails.setMaxCnt(objCouponsDetails.getMaxCnt());
				response = XstreamParserHelper.produceXMlFromObject(objCouponDetails);
			}
			else	{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT, "mainMenuID", mainMenuID.toString());
			}
		}
		
		return response;
	}

	/**
	 * Method to get population centers for coupons for Location.
	 * @param userId as reqest.
	 * @return xml containing population center list
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String getCoupPopulationCentresForLoc(Integer userId) throws ScanSeeException {
		final String methodName = "getCoupPopulationCentresForLoc";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		CouponsDetails objCouponsDetails = null;
		
		objCouponsDetails = myGalleryDao.getCoupPopulationCentresForLoc(userId);
		
		if (objCouponsDetails != null)	{
			response = XstreamParserHelper.produceXMlFromObject(objCouponsDetails);
		}
		else	{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}
		return response;
	}

	/**
	 * Method to get population centers for coupons for product.
	 * @param userId as reqest.
	 * @return xml containing population center list
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String getCoupPopulationCentresForProd(Integer userId) throws ScanSeeException {
		final String methodName = "getCoupPopulationCentresForProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		CouponsDetails objCouponsDetails = null;
		
		objCouponsDetails = myGalleryDao.getCoupPopulationCentresForProd(userId);
		
		if (objCouponsDetails != null)	{
			response = XstreamParserHelper.produceXMlFromObject(objCouponsDetails);
		}
		else	{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}
		return response;
	}

	/**
	 * Method to get business categories for coupons for Locations.
	 * @param xml as reqest.
	 * @return xml containing business categories
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String getCoupLocationBusinessCategory(String xml) throws ScanSeeException {
		final String methodName = "getCoupLocationBusinessCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		CouponsDetails objCouponsDetails = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetailsRequest objProdDetailsReq = (ProductDetailsRequest) parser.parseXmlToObject(xml);
		
		if (objProdDetailsReq.getUserId() == null)	{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else	{
			objCouponsDetails = myGalleryDao.getCoupLocationBusinessCategory(objProdDetailsReq);
		
			if (objCouponsDetails != null && !objCouponsDetails.getCouponDetail().isEmpty() && objCouponsDetails.getCouponDetail() != null)	{
				response = XstreamParserHelper.produceXMlFromObject(objCouponsDetails);
			}
			else	{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		return response;
	}

	/**
	 * Method to get retailers for selected business category.
	 * @param xml as reqest.
	 * @return xml containing retailer list
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String getRetailerForBusinessCategory(String xml) throws ScanSeeException {
		final String methodName = "getRetailerForBusinessCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		CouponsDetails objCouponsDetails = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetailsRequest objProdDetailsReq = (ProductDetailsRequest) parser.parseXmlToObject(xml);
		
		if (objProdDetailsReq.getUserId() == null || objProdDetailsReq.getBusCatIDs() == null)	{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else	{
			objCouponsDetails = myGalleryDao.getRetailerForBusinessCategory(objProdDetailsReq);
		
			if (objCouponsDetails != null && !objCouponsDetails.getCouponDetail().isEmpty() && objCouponsDetails.getCouponDetail() != null)	{
				response = XstreamParserHelper.produceXMlFromObject(objCouponsDetails);
			}
			else	{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		return response;
	}

	/**
	 * Method to get categories for coupons for products.
	 * @param xml as reqest.
	 * @return xml containing business categories
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String getCoupProductCategory(String xml) throws ScanSeeException	{
		final String methodName = "getCoupProductCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		CouponsDetails objCouponsDetails = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetailsRequest objProdDetailsReq = (ProductDetailsRequest) parser.parseXmlToObject(xml);
		
		if (objProdDetailsReq.getUserId() == null)	{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else	{
			objCouponsDetails = myGalleryDao.getCoupProductCategory(objProdDetailsReq);
		
			if (objCouponsDetails != null && !objCouponsDetails.getCouponDetail().isEmpty() && objCouponsDetails.getCouponDetail() != null)	{
				response = XstreamParserHelper.produceXMlFromObject(objCouponsDetails);
			}
			else	{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		return response;
	}

	/**
	 * Method to get gallery coupons for All/Used/Expired in Location.
	 * @param xml as reqest.
	 * @return xml containing coupons 
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String getGalleryCouponsByLocation(String xml) throws ScanSeeException {
		final String methodName = "getGalleryCouponsByLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetailsRequest objCouponsRequest = (ProductDetailsRequest) parser.parseXmlToObject(xml);
		CouponsDetails objCoupDetailsResponse = null;
		if (objCouponsRequest.getUserId() == null || objCouponsRequest.getType() == null)	{
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else	{
			Integer mainMenuID;
			if (objCouponsRequest.getMainMenuID() == null)	{
				UserTrackingData objUserTrackingData = new UserTrackingData();
				objUserTrackingData.setUserID(objCouponsRequest.getUserId());
				objUserTrackingData.setModuleID(objCouponsRequest.getModuleID());
				objUserTrackingData.setPostalCode(objCouponsRequest.getPostalcode());
				objUserTrackingData.setLatitude(objCouponsRequest.getLatitude());
				objUserTrackingData.setLongitude(objCouponsRequest.getLongitude());
				mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);
				objCouponsRequest.setMainMenuID(mainMenuID);
			}
			else	{
				mainMenuID = objCouponsRequest.getMainMenuID();
			}
			objCoupDetailsResponse = myGalleryDao.getGalleryCouponsByLocation(objCouponsRequest);
			
			if (objCoupDetailsResponse != null && (!objCoupDetailsResponse.getCouponDetail().isEmpty() && objCoupDetailsResponse.getCouponDetail() != null))	{
				
				//Sorting retailers and in each retailers coupon are sorted by category			
  				CouponsDetails objCouponsDetailsRetSort = new CouponsDetails();
				objCouponsDetailsRetSort = MygalleryHelper.sortAllCouponsRetailersByNameForLocation(objCoupDetailsResponse.getCouponDetail());
				CouponsDetails objCouponDetailsSorted = new CouponsDetails();
				ArrayList<RetailersDetails> retDetailsList = new ArrayList<RetailersDetails>();			
				RetailersDetails objRetDetails = null;
				CouponsDetails coupsDetails = null;
				//For sorting coupons by distance for each retailer
				for (int i = 0; i < objCouponsDetailsRetSort.getRetDetailsList().size(); i++)	{
					coupsDetails = new CouponsDetails();
					objRetDetails = new RetailersDetails();
					coupsDetails = MygalleryHelper.sortRetailerCoupByDist(objCouponsDetailsRetSort.getRetDetailsList().get(i).getCouponDetailsList());
					objRetDetails.setRetName(objCouponsDetailsRetSort.getRetDetailsList().get(i).getRetName());
					objRetDetails.setRetId(objCouponsDetailsRetSort.getRetDetailsList().get(i).getRetId());
					objRetDetails.setRetLocId(objCouponsDetailsRetSort.getRetDetailsList().get(i).getRetLocId());
					objRetDetails.setRetDetailsList(coupsDetails.getRetDetailsList());
					retDetailsList.add(objRetDetails);
				}
				objCouponDetailsSorted.setRetDetailsList(retDetailsList);
				objCouponDetailsSorted.setNextPageFlag(objCoupDetailsResponse.getNextPageFlag());
				objCouponDetailsSorted.setMainMenuID(mainMenuID);
				objCouponDetailsSorted.setMaxCnt(objCoupDetailsResponse.getMaxCnt());	
				objCouponDetailsSorted.setMaxRowNum(objCoupDetailsResponse.getMaxRowNum());		
						
				response = XstreamParserHelper.produceXMlFromObject(objCouponDetailsSorted);
			}
			else	{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT, "mainMenuID", mainMenuID.toString());
			}
		}
		
		return response;
	}
	
	/**
	 * Method to get gallery coupons for All/Used/Expired in Products.
	 * @param xml as reqest.
	 * @return xml containing coupons sorted by category
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String getGalleryCouponsByProduct(String xml) throws ScanSeeException {
		final String methodName = "getGalleryCouponsByProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetailsRequest objCouponsRequest = (ProductDetailsRequest) parser.parseXmlToObject(xml);
		CouponsDetails objCoupDetailsResponse = null;
		if (objCouponsRequest.getUserId() == null || objCouponsRequest.getType() == null)	{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else	{
			Integer mainMenuID;
			if (objCouponsRequest.getMainMenuID() == null)	{
				UserTrackingData objUserTrackingData = new UserTrackingData();
				objUserTrackingData.setUserID(objCouponsRequest.getUserId());
				objUserTrackingData.setModuleID(objCouponsRequest.getModuleID());
				objUserTrackingData.setPostalCode(objCouponsRequest.getPostalcode());
				objUserTrackingData.setLatitude(objCouponsRequest.getLatitude());
				objUserTrackingData.setLongitude(objCouponsRequest.getLongitude());
				mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);
				objCouponsRequest.setMainMenuID(mainMenuID);
			}
			else	{
				mainMenuID = objCouponsRequest.getMainMenuID();
			}
			objCoupDetailsResponse = myGalleryDao.getGalleryCouponsByProduct(objCouponsRequest);
			
			//To sort coupons by category
			if (objCoupDetailsResponse != null && (!objCoupDetailsResponse.getCouponDetail().isEmpty() && objCoupDetailsResponse.getCouponDetail() != null))	{
				CouponsDetails objCouponDetails = new CouponsDetails();
				objCouponDetails = MygalleryHelper.sortAllCouponsByCatForProduct(objCoupDetailsResponse.getCouponDetail());
				objCouponDetails.setNextPageFlag(objCoupDetailsResponse.getNextPageFlag());
				objCouponDetails.setMainMenuID(mainMenuID);
				objCouponDetails.setMaxCnt(objCoupDetailsResponse.getMaxCnt());
				objCouponDetails.setMaxRowNum(objCoupDetailsResponse.getMaxRowNum());
				response = XstreamParserHelper.produceXMlFromObject(objCouponDetails);
			}
			else	{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT, "mainMenuID", mainMenuID.toString());
			}
		}
		
		return response;
	}

	/**
	 * Method to get gallery HotDeals for All/Used/Expired in Location.
	 * @param xml as reqest.
	 * @return xml containing hotdeals sorted by retailers and category.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String getGalleryHotDealByLocation(String xml) throws ScanSeeException {
		final String methodName = "getGalleryHotDealByLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetailsRequest objCouponsRequest = (ProductDetailsRequest) parser.parseXmlToObject(xml);
		HotDealsListResultSet objHdDetails = null;
		if (objCouponsRequest.getUserId() == null || objCouponsRequest.getType() == null)	{
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else	{
			Integer mainMenuID;
			if (objCouponsRequest.getMainMenuID() == null)	{
				final UserTrackingData objUserTrackingData = new UserTrackingData();
				objUserTrackingData.setUserID(objCouponsRequest.getUserId());
				objUserTrackingData.setModuleID(objCouponsRequest.getModuleID());
				objUserTrackingData.setPostalCode(objCouponsRequest.getPostalcode());
				objUserTrackingData.setLatitude(objCouponsRequest.getLatitude());
				objUserTrackingData.setLongitude(objCouponsRequest.getLongitude());
				mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);
				objCouponsRequest.setMainMenuID(mainMenuID);
			}
			else	{
				mainMenuID = objCouponsRequest.getMainMenuID();
			}
			objHdDetails = myGalleryDao.getGalleryHotDealByLocation(objCouponsRequest);
			
			if (objHdDetails != null && (!objHdDetails.getHdDetailsList().isEmpty() && objHdDetails.getHdDetailsList() != null))	{

				/*
				 * Sorting Hot deal by retailers and in each retailer is sorted by category for hot deals
				 */								  
 				HotDealsListResultSet objHotDealResultRetSorted = new HotDealsListResultSet();
				objHotDealResultRetSorted = MygalleryHelper.sortHotDealsListByRetailer(objHdDetails.getHdDetailsList());				
				HotDealsListResultSet objHotDealsSorted = new HotDealsListResultSet();
				ArrayList<RetailersDetails> retDetailsList = new ArrayList<RetailersDetails>();				
				RetailersDetails objRetailerDetails = null;
				HotDealsListResultSet objHotDealsResult = null;
				
				/*
				 * For sorting HotDeals by distance for each retailer
				 */
				for (int i = 0; i < objHotDealResultRetSorted.getRetDetailsList().size(); i++)	{
					objRetailerDetails = new RetailersDetails();
					objHotDealsResult = new HotDealsListResultSet();
					objHotDealsResult = MygalleryHelper.getHotDealsListByRetailerLoc(objHotDealResultRetSorted.getRetDetailsList().get(i).gethDDetailsList());
					objRetailerDetails.setRetName(objHotDealResultRetSorted.getRetDetailsList().get(i).getRetName());
					objRetailerDetails.setRetId(objHotDealResultRetSorted.getRetDetailsList().get(i).getRetId());
					objRetailerDetails.setRetDetailsList((ArrayList<RetailersDetails>) objHotDealsResult.getRetDetailsList());
					retDetailsList.add(objRetailerDetails);
				}
				objHotDealsSorted.setRetDetailsList(retDetailsList);			
				objHotDealsSorted.setMaxCnt(objHdDetails.getMaxCnt());
				objHotDealsSorted.setMainMenuID(mainMenuID);
				objHotDealsSorted.setNextPage(objHdDetails.getNextPage());
				objHotDealsSorted.setMaxRowNum(objHdDetails.getMaxRowNum());		
			
				response = XstreamParserHelper.produceXMlFromObject(objHotDealsSorted);
			}
			else	{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT, "mainMenuID", mainMenuID.toString());
			}
		}
		
		return response;
	}

	/**
	 * Method to get gallery Hot deals for All/Used/Expired in Products.
	 * @param xml as reqest.
	 * @return xml containing hotdeals sorted by category
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String getGalleryHotDealByProduct(String xml) throws ScanSeeException {
		final String methodName = "getGalleryHotDealByProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetailsRequest objCouponsRequest = (ProductDetailsRequest) parser.parseXmlToObject(xml);
		HotDealsListResultSet objHdDetails = null;
		if (objCouponsRequest.getUserId() == null || objCouponsRequest.getType() == null)	{
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else	{
			//To get mainMenuID from moduleID
			Integer mainMenuID;
			if (objCouponsRequest.getMainMenuID() == null)	{
				final UserTrackingData objUserTrackingData = new UserTrackingData();
				objUserTrackingData.setUserID(objCouponsRequest.getUserId());
				objUserTrackingData.setModuleID(objCouponsRequest.getModuleID());
				objUserTrackingData.setPostalCode(objCouponsRequest.getPostalcode());
				objUserTrackingData.setLatitude(objCouponsRequest.getLatitude());
				objUserTrackingData.setLongitude(objCouponsRequest.getLongitude());
				mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);
				objCouponsRequest.setMainMenuID(mainMenuID);
			}
			else	{
				mainMenuID = objCouponsRequest.getMainMenuID();
			}
			objHdDetails = myGalleryDao.getGalleryHotDealByProduct(objCouponsRequest);
			
			//To sort Hotdeals by category
			if (objHdDetails != null && (!objHdDetails.getHdDetailsList().isEmpty() && objHdDetails.getHdDetailsList() != null))	{
				HotDealsListResultSet objHotDealResultSetSorted = new HotDealsListResultSet();
				objHotDealResultSetSorted = MygalleryHelper.sortHotDealsListByCategory(objHdDetails.getHdDetailsList());
				objHotDealResultSetSorted.setMaxCnt(objHdDetails.getMaxCnt());
				objHotDealResultSetSorted.setMainMenuID(mainMenuID);
				objHotDealResultSetSorted.setNextPage(objHdDetails.getNextPage());
				objHotDealResultSetSorted.setMaxRowNum(objHdDetails.getMaxRowNum());
				response = XstreamParserHelper.produceXMlFromObject(objHotDealResultSetSorted);
			}
			else	{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT, "mainMenuID", mainMenuID.toString());
			}
		}	
		return response;
	}

}
