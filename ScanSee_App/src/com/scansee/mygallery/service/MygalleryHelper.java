package com.scansee.mygallery.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.helper.SortCoupLocationByRetDist;
import com.scansee.common.helper.SortCouponLocByRetailer;
import com.scansee.common.helper.SortCouponProdByCategory;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.CategoryInfo;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.CouponsDetails;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.HotDealsListResultSet;
import com.scansee.common.pojos.LoyaltyDetail;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.util.Utility;

public class MygalleryHelper
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(MygalleryHelper.class);

	/**
	 * for MygalleryHelper constructor.
	 */
	protected MygalleryHelper()
	{
		LOG.info("Inside MygalleryHelper class");
	}

	public static CLRDetails getLoyaltysByRetCatgory(CLRDetails cLRDetailsObj2)

	{
		CLRDetails clrDetails = new CLRDetails();

		List<RetailerDetail> retailDetailList = null;
		RetailerDetail retailerCatDetail = null;

		List<RetailersDetails> loyaltyByReatilerCatList = new ArrayList<RetailersDetails>();

		CLRDetails cLRDetailsObj = null;

		MygalleryHelper mygalleryHelperObj = new MygalleryHelper();

		List<LoyaltyDetail> loyaltyDetailsList = null;

		List<LoyaltyDetail> loyaltyDetailsFinalList = null;

		String cateName = null;

		HashMap<String, RetailerDetail> cLRDetailsMapCat = null;

		List<RetailersDetails> loyaltyByReatiler = mygalleryHelperObj.getLoyaltyByRetailer(cLRDetailsObj2);

		// for pagination
		if (cLRDetailsObj2 != null)
		{
			clrDetails.setNextPage(cLRDetailsObj2.getNextPage());
		}

		for (RetailersDetails retailersDetailsCat : loyaltyByReatiler)
		{
			loyaltyDetailsList = retailersDetailsCat.getLoyaltyDetails();
			retailDetailList = new ArrayList<RetailerDetail>();
			cLRDetailsMapCat = new HashMap<String, RetailerDetail>();
			for (int i = 0; i < loyaltyDetailsList.size(); i++)
			{
				LoyaltyDetail loyaltyDetail = loyaltyDetailsList.get(i);

				/*
				 * } for (LoyaltyDetail loyaltyDetail : loyaltyDetailsList) {
				 */

				cateName = loyaltyDetail.getCateName();

				if (cLRDetailsMapCat.containsKey(cateName))

				{
					retailerCatDetail = cLRDetailsMapCat.get(cateName);
					loyaltyDetailsFinalList = retailerCatDetail.getLoyaltyDetails();
					LoyaltyDetail loyaltydetailInfo = new LoyaltyDetail();
					loyaltydetailInfo.setRowNum(loyaltyDetail.getRowNum());
					loyaltydetailInfo.setLoyaltyDealId(loyaltyDetail.getLoyaltyDealId());
					loyaltydetailInfo.setLoyaltyDealName(loyaltyDetail.getLoyaltyDealName());
					loyaltydetailInfo.setLoyaltyDealDiscountType(loyaltyDetail.getLoyaltyDealDiscountType());
					loyaltydetailInfo.setLoyaltyDealDiscountAmount(loyaltyDetail.getLoyaltyDealDiscountAmount());
					loyaltydetailInfo.setLoyaltyDealDiscountPct(loyaltyDetail.getLoyaltyDealDiscountPct());
					loyaltydetailInfo.setLoyaltyDealDescription(loyaltyDetail.getLoyaltyDealDescription());
					loyaltydetailInfo.setMerchantId(loyaltyDetail.getMerchantId());
					loyaltydetailInfo.setLoyaltyDealDateAdded(loyaltyDetail.getLoyDealDateAdded());
					loyaltydetailInfo.setLoyaltyDealStartDate(loyaltyDetail.getStartDate());
					loyaltydetailInfo.setLoyaltyDealExpireDate(loyaltyDetail.getEndDate());

					loyaltydetailInfo.setImagePath(loyaltyDetail.getImagePath());
					loyaltydetailInfo.setaPIPartnerID(loyaltyDetail.getaPIPartnerID());
					loyaltydetailInfo.setaPIPartName(loyaltyDetail.getaPIPartName());
					loyaltydetailInfo.setaPIPartImage(loyaltyDetail.getaPIPartImage());
					loyaltydetailInfo.setFavFlag(loyaltyDetail.getFavFlag());
					loyaltydetailInfo.setLoyaltyListID(loyaltyDetail.getLoyaltyListID());

					loyaltyDetailsFinalList.add(loyaltydetailInfo);
					retailerCatDetail.setLoyaltyDetails(loyaltyDetailsFinalList);

					// retailDetailList.add(retailerCatDetail);

				}
				else
				{

					retailerCatDetail = new RetailerDetail();
					loyaltyDetailsFinalList = new ArrayList<LoyaltyDetail>();
					retailerCatDetail.setCateName(loyaltyDetail.getCateName());
					retailerCatDetail.setCateId(loyaltyDetail.getCateId());

					LoyaltyDetail loyaltydetailInfo = new LoyaltyDetail();
					loyaltydetailInfo.setRowNum(loyaltyDetail.getRowNum());
					loyaltydetailInfo.setLoyaltyDealId(loyaltyDetail.getLoyaltyDealId());
					loyaltydetailInfo.setLoyaltyDealName(loyaltyDetail.getLoyaltyDealName());
					loyaltydetailInfo.setLoyaltyDealDiscountType(loyaltyDetail.getLoyaltyDealDiscountType());
					loyaltydetailInfo.setLoyaltyDealDiscountAmount(loyaltyDetail.getLoyaltyDealDiscountAmount());
					loyaltydetailInfo.setLoyaltyDealDiscountPct(loyaltyDetail.getLoyaltyDealDiscountPct());
					loyaltydetailInfo.setLoyaltyDealDescription(loyaltyDetail.getLoyaltyDealDescription());
					loyaltydetailInfo.setLoyaltyDealDateAdded(loyaltyDetail.getLoyDealDateAdded());
					loyaltydetailInfo.setLoyaltyDealStartDate(loyaltyDetail.getStartDate());
					loyaltydetailInfo.setLoyaltyDealExpireDate(loyaltyDetail.getEndDate());
					loyaltydetailInfo.setMerchantId(loyaltyDetail.getMerchantId());
					loyaltydetailInfo.setImagePath(loyaltyDetail.getImagePath());
					loyaltydetailInfo.setaPIPartnerID(loyaltyDetail.getaPIPartnerID());
					loyaltydetailInfo.setaPIPartName(loyaltyDetail.getaPIPartName());
					loyaltydetailInfo.setaPIPartImage(loyaltyDetail.getaPIPartImage());
					loyaltydetailInfo.setFavFlag(loyaltyDetail.getFavFlag());
					loyaltydetailInfo.setLoyaltyListID(loyaltyDetail.getLoyaltyListID());

					loyaltyDetailsFinalList.add(loyaltydetailInfo);

					retailerCatDetail.setLoyaltyDetails(loyaltyDetailsFinalList);

					retailDetailList.add(retailerCatDetail);

				}

				cLRDetailsMapCat.put(cateName, retailerCatDetail);

			}

			//Implemented logic to sort loyalty categories alphabetically. 
			final MygalleryCouponSort sortCouponCatgories = new MygalleryCouponSort();
			Collections.sort(retailDetailList, sortCouponCatgories);
			
			retailersDetailsCat.setRetailerDetail(retailDetailList);
			retailersDetailsCat.setLoyaltyDetails(null);
			loyaltyByReatilerCatList.add(retailersDetailsCat);

		}
		clrDetails.setLoycatretgrplst(loyaltyByReatilerCatList);

		return clrDetails;

	}

	/**
	 * For displaying coupons by category wise.
	 * 
	 * @param cLRDetailsObj
	 * @return
	 */

	public static CLRDetails getCouponByCategory(CLRDetails cLRDetailsObj)

	{
		CLRDetails clrDetails = new CLRDetails();
		List<CouponDetails> couponDetailList = null;
		couponDetailList = cLRDetailsObj.getCouponDetails();

		if (couponDetailList != null && !couponDetailList.isEmpty())
		{
			clrDetails.setNextPage(cLRDetailsObj.getNextPage());
		}
		RetailerDetail clrDetailsObj = null;
		final HashMap<String, RetailerDetail> cLRDetailsMap = new HashMap<String, RetailerDetail>();
		List<CouponDetails> coupongrouplst = null;

		String cateName = null;
		for (CouponDetails couponDetail : couponDetailList)
		{
			cateName = couponDetail.getCateName();

			
			if (cLRDetailsMap.containsKey(cateName))

			{
				clrDetailsObj = cLRDetailsMap.get(cateName);
				coupongrouplst = clrDetailsObj.getCouponDetails();
				if (coupongrouplst != null)
				{
					CouponDetails couponDetailsObj = new CouponDetails();

					couponDetailsObj.setRowNum(couponDetail.getRowNum());
					couponDetailsObj.setCouponId(couponDetail.getCouponId());
					couponDetailsObj.setUserCouponGalleryID(couponDetail.getUserCouponGalleryID());
					couponDetailsObj.setCouponName(couponDetail.getCouponName());
					couponDetailsObj.setCouponDiscountType(couponDetail.getCouponDiscountType());
					couponDetailsObj.setCouponDiscountAmount(couponDetail.getCouponDiscountAmount());
					couponDetailsObj.setCouponDiscountPct(couponDetail.getCouponDiscountPct());
					couponDetailsObj.setCoupDesptn(couponDetail.getCoupDesptn());
					couponDetailsObj.setCouponDateAdded(couponDetail.getCoupDateAdded());
					couponDetailsObj.setCouponStartDate(couponDetail.getCoupStartDate());
					couponDetailsObj.setCouponExpireDate(couponDetail.getCoupExpireDate());
					couponDetailsObj.setCouponURL(couponDetail.getCouponURL());
					couponDetailsObj.setCouponImagePath(couponDetail.getCouponImagePath());
					couponDetailsObj.setFavFlag(couponDetail.getFavFlag());
					couponDetailsObj.setViewableOnWeb(couponDetail.isViewableOnWeb());
					couponDetailsObj.setCouponListID(couponDetail.getCouponListID());
					couponDetailsObj.setCoupFavFlag(couponDetail.getCoupFavFlag());

					coupongrouplst.add(couponDetailsObj);

					clrDetailsObj.setCouponDetails(coupongrouplst);

				}
			}

			else
			{
				clrDetailsObj = new RetailerDetail();
				clrDetailsObj.setCateId(couponDetail.getCateId());
				clrDetailsObj.setCateName(couponDetail.getCateName());

				coupongrouplst = new ArrayList<CouponDetails>();

				CouponDetails couponDetailsObj = new CouponDetails();

				couponDetailsObj.setRowNum(couponDetail.getRowNum());
				couponDetailsObj.setCouponId(couponDetail.getCouponId());
				couponDetailsObj.setUserCouponGalleryID(couponDetail.getUserCouponGalleryID());
				couponDetailsObj.setCouponName(couponDetail.getCouponName());
				couponDetailsObj.setCouponDiscountType(couponDetail.getCouponDiscountType());
				couponDetailsObj.setCouponDiscountAmount(couponDetail.getCouponDiscountAmount());
				couponDetailsObj.setCouponDiscountPct(couponDetail.getCouponDiscountPct());
				couponDetailsObj.setCoupDesptn(couponDetail.getCoupDesptn());
				couponDetailsObj.setCouponDateAdded(couponDetail.getCoupDateAdded());
				couponDetailsObj.setCouponStartDate(couponDetail.getCoupStartDate());
				couponDetailsObj.setCouponExpireDate(couponDetail.getCoupExpireDate());
				couponDetailsObj.setCouponURL(couponDetail.getCouponURL());
				couponDetailsObj.setCouponImagePath(couponDetail.getCouponImagePath());
				couponDetailsObj.setFavFlag(couponDetail.getFavFlag());
				couponDetailsObj.setViewableOnWeb(couponDetail.isViewableOnWeb());
				couponDetailsObj.setCouponListID(couponDetail.getCouponListID());
				couponDetailsObj.setCoupFavFlag(couponDetail.getCoupFavFlag());
				
				coupongrouplst.add(couponDetailsObj);
				clrDetailsObj.setCouponDetails(coupongrouplst);

			}
			cLRDetailsMap.put(cateName, clrDetailsObj);
		}

		final Set<Map.Entry<String, RetailerDetail>> set = cLRDetailsMap.entrySet();

		final ArrayList<RetailerDetail> cLRDetailslist = new ArrayList<RetailerDetail>();

		for (Map.Entry<String, RetailerDetail> entry : set)
		{
			cLRDetailslist.add(entry.getValue());
		}
		
		//Implemented logic to sort coupon categories alphabetically. 
		final MygalleryCouponSort sortCouponCatgories = new MygalleryCouponSort();
		Collections.sort(cLRDetailslist, sortCouponCatgories);
		
	
		clrDetails.setLoygrpbyRetlst(cLRDetailslist);

		return clrDetails;

	}

	public List<RetailersDetails> getLoyaltyByRetailer(CLRDetails cLRDetailsObj)

	{
		CLRDetails clrDetails = new CLRDetails();
		List<LoyaltyDetail> loyaltyDetailList = null;
		loyaltyDetailList = cLRDetailsObj.getLoyaltyDetails();
		RetailersDetails retailersDetailsObj = null;
		Integer retailerId = null;
		List<LoyaltyDetail> loyaltylst = null;

		List<LoyaltyDetail> loyaltylstExist = null;
		List<RetailersDetails> loyaltyByReatiler = new ArrayList<RetailersDetails>();
		RetailersDetails loyaltyDetailObj = null;
		if (loyaltyDetailList != null && !loyaltyDetailList.isEmpty())
		{
			clrDetails.setNextPage(cLRDetailsObj.getNextPage());
		}

		final HashMap<Integer, RetailersDetails> cLRDetailsMap = new HashMap<Integer, RetailersDetails>();

		// Group by Retailer
		for (LoyaltyDetail loyaltyDetail : loyaltyDetailList)
		{
			retailerId = loyaltyDetail.getRetId();

			if (cLRDetailsMap.containsKey(retailerId))

			{
				loyaltyDetailObj = cLRDetailsMap.get(retailerId);
				loyaltylstExist = loyaltyDetailObj.getLoyaltyDetails();
				loyaltylstExist.add(loyaltyDetail);
			}
			else
			{
				loyaltylst = new ArrayList<LoyaltyDetail>();

				retailersDetailsObj = new RetailersDetails();
				retailersDetailsObj.setRetId(loyaltyDetail.getRetId());
				retailersDetailsObj.setRetName(loyaltyDetail.getRetName());
				retailersDetailsObj.setRetImage(loyaltyDetail.getRetImagePath());
				loyaltylst.add(loyaltyDetail);
				retailersDetailsObj.setLoyaltyDetails(loyaltylst);
				loyaltyByReatiler.add(retailersDetailsObj);

			}

			cLRDetailsMap.put(retailerId, retailersDetailsObj);

		}
		//Implemented logic to sort coupon categories alphabetically. 
		final MygalleryLoyaltySort sortLoyaltyRetailerName = new MygalleryLoyaltySort();
		Collections.sort(loyaltyByReatiler, sortLoyaltyRetailerName);

		return loyaltyByReatiler;

	}
	
	/**
	 * Method to sort coupons by Retailer by distance.
	 * @param couponDetailsResultList as request.
	 * @return objCouponsDetails in response/
	 */
	public static CouponsDetails sortAllCouponsRetailersByDistanceForLocation(List<CouponDetails> couponDetailsResultList)	{
		CouponsDetails objCouponsDetails = new CouponsDetails();
		String key = null;
		final HashMap<String, RetailersDetails> retDetailsInfoMap = new HashMap<String, RetailersDetails>();
		ArrayList<CouponDetails> couponList;
		RetailersDetails retDetails;
		
		for (CouponDetails coupDetailsList : couponDetailsResultList)	{
			key = coupDetailsList.getRetName();
			if (!"".equals(Utility.isNull(key)))	{
				key = Character.toUpperCase(key.charAt(0)) + key.substring(1);
				if (retDetailsInfoMap.containsKey(key))	{
					retDetails = retDetailsInfoMap.get(key);
					couponList = retDetails.getCouponDetailsList();
					if (null != couponList)	{
						final CouponDetails coupDet = new CouponDetails();
						coupDet.setCouponId(coupDetailsList.getCouponId());
						coupDet.setCouponName(coupDetailsList.getCouponName());
						coupDet.setUserCoupGallId(coupDetailsList.getUserCoupGallId());
						coupDet.setIsDateFormated(true);
						coupDet.setCouponStartDate(coupDetailsList.getCouponStartDate());
						coupDet.setCouponExpireDate(coupDetailsList.getCouponExpireDate());
						coupDet.setIsDateFormated(null);
						coupDet.setCouponImagePath(coupDetailsList.getCouponImagePath());
						coupDet.setCouponURL(coupDetailsList.getCouponURL());
						coupDet.setBusCatId(coupDetailsList.getBusCatId());
						coupDet.setBusCatName(coupDetailsList.getBusCatName());
						coupDet.setAddress(coupDetailsList.getAddress());
						coupDet.setRetLocId(coupDetailsList.getRetLocId());
						coupDet.setCity(coupDetailsList.getCity());
						coupDet.setPostalCode(coupDetailsList.getPostalCode());
						coupDet.setCoupDesc(coupDetailsList.getCoupDesc());
						coupDet.setDistance(coupDetailsList.getDistance());
						coupDet.setClaimFlag(coupDetailsList.getClaimFlag());
						coupDet.setRedeemFlag(coupDetailsList.getRedeemFlag());
						coupDet.setNewFlag(coupDetailsList.getNewFlag());
						coupDet.setUsed(coupDetailsList.getUsed());
						coupDet.setCouponDiscountAmount(coupDetailsList.getCouponDiscountAmount());
						coupDet.setCouponDiscountPct(coupDetailsList.getCouponDiscountPct());
						coupDet.setCouponListID(coupDetailsList.getCouponListID());
						coupDet.setCouponURL(coupDetailsList.getCouponURL());
						coupDet.setViewableOnWeb(coupDetailsList.isViewableOnWeb());
						coupDet.setUsedFlag(coupDetailsList.getUsedFlag());
						couponList.add(coupDet);
						if (retDetails.getMinRetDist() > coupDetailsList.getDistance())	{
							retDetails.setMinRetDist(coupDetailsList.getDistance());
						}
						retDetails.setCouponDetailsList(couponList);
					}
				}
				else	{
					retDetails = new RetailersDetails();
					retDetails.setRetId(coupDetailsList.getRetId());
					retDetails.setRetName(coupDetailsList.getRetName());
					retDetails.setMinRetDist(coupDetailsList.getDistance());
					couponList = new ArrayList<CouponDetails>();
					final CouponDetails coupDet = new CouponDetails();
					coupDet.setCouponId(coupDetailsList.getCouponId());
					coupDet.setCouponName(coupDetailsList.getCouponName());
					coupDet.setUserCoupGallId(coupDetailsList.getUserCoupGallId());
					coupDet.setIsDateFormated(true);
					coupDet.setCouponStartDate(coupDetailsList.getCouponStartDate());
					coupDet.setCouponExpireDate(coupDetailsList.getCouponExpireDate());
					coupDet.setIsDateFormated(null);
					coupDet.setCouponImagePath(coupDetailsList.getCouponImagePath());
					coupDet.setCouponURL(coupDetailsList.getCouponURL());
					coupDet.setBusCatId(coupDetailsList.getBusCatId());
					coupDet.setBusCatName(coupDetailsList.getBusCatName());
					coupDet.setAddress(coupDetailsList.getAddress());
					coupDet.setRetLocId(coupDetailsList.getRetLocId());
					coupDet.setCity(coupDetailsList.getCity());
					coupDet.setPostalCode(coupDetailsList.getPostalCode());
					coupDet.setCoupDesc(coupDetailsList.getCoupDesc());
					coupDet.setDistance(coupDetailsList.getDistance());
					coupDet.setClaimFlag(coupDetailsList.getClaimFlag());
					coupDet.setRedeemFlag(coupDetailsList.getRedeemFlag());
					coupDet.setNewFlag(coupDetailsList.getNewFlag());
					coupDet.setUsed(coupDetailsList.getUsed());
					coupDet.setCouponDiscountAmount(coupDetailsList.getCouponDiscountAmount());
					coupDet.setCouponDiscountPct(coupDetailsList.getCouponDiscountPct());
					coupDet.setCouponListID(coupDetailsList.getCouponListID());
					coupDet.setCouponURL(coupDetailsList.getCouponURL());
					coupDet.setViewableOnWeb(coupDetailsList.isViewableOnWeb());
					coupDet.setUsedFlag(coupDetailsList.getUsedFlag());
					couponList.add(coupDet);
					retDetails.setCouponDetailsList(couponList);
				}
				retDetailsInfoMap.put(key, retDetails);
			}
		}
		final Set<Map.Entry<String, RetailersDetails>> set = retDetailsInfoMap.entrySet();
		final ArrayList<RetailersDetails> retDetLst = new ArrayList<RetailersDetails>();
		for (Map.Entry<String, RetailersDetails> entry : set)
		{
			retDetLst.add(entry.getValue());
		}	
		SortCoupLocationByRetDist objSortCoupByRet = new SortCoupLocationByRetDist();
		Collections.sort(retDetLst, objSortCoupByRet);
		objCouponsDetails.setRetDetailsList(retDetLst);
		return objCouponsDetails;
	}
	
	/**
	 * Method to sort coupons by Retailer name.
	 * @param couponDetailsResultList as request.
	 * @return objCouponsDetails in response/
	 */
	public static CouponsDetails sortAllCouponsRetailersByNameForLocation(List<CouponDetails> couponDetailsResultList)	{
		CouponsDetails objCouponsDetails = new CouponsDetails();
		String key = null;
		final HashMap<String, RetailersDetails> retDetailsInfoMap = new HashMap<String, RetailersDetails>();
		ArrayList<CouponDetails> couponList;
		RetailersDetails retDetails;
		
		for (CouponDetails coupDetailsList : couponDetailsResultList)	{
			key = coupDetailsList.getRetName();
			if (!"".equals(Utility.isNull(key)))	{
				key = Character.toUpperCase(key.charAt(0)) + key.substring(1);
				if (retDetailsInfoMap.containsKey(key))	{
					retDetails = retDetailsInfoMap.get(key);
					couponList = retDetails.getCouponDetailsList();
					if (null != couponList)	{
						final CouponDetails coupDet = new CouponDetails();
						coupDet.setCouponId(coupDetailsList.getCouponId());
						coupDet.setCouponName(coupDetailsList.getCouponName());
						coupDet.setUserCoupGallId(coupDetailsList.getUserCoupGallId());
						coupDet.setIsDateFormated(true);
						coupDet.setCouponStartDate(coupDetailsList.getCouponStartDate());
						coupDet.setCouponExpireDate(coupDetailsList.getCouponExpireDate());
						coupDet.setIsDateFormated(null);
						coupDet.setCouponImagePath(coupDetailsList.getCouponImagePath());
						coupDet.setCouponURL(coupDetailsList.getCouponURL());
						coupDet.setBusCatId(coupDetailsList.getBusCatId());
						coupDet.setBusCatName(coupDetailsList.getBusCatName());
						coupDet.setAddress(coupDetailsList.getAddress());
						coupDet.setRetLocId(coupDetailsList.getRetLocId());
						coupDet.setCity(coupDetailsList.getCity());
						coupDet.setPostalCode(coupDetailsList.getPostalCode());
						coupDet.setCoupDesc(coupDetailsList.getCoupDesc());
						coupDet.setDistance(coupDetailsList.getDistance());
						coupDet.setClaimFlag(coupDetailsList.getClaimFlag());
						coupDet.setRedeemFlag(coupDetailsList.getRedeemFlag());
						coupDet.setNewFlag(coupDetailsList.getNewFlag());
						coupDet.setUsed(coupDetailsList.getUsed());
						coupDet.setCouponDiscountAmount(coupDetailsList.getCouponDiscountAmount());
						coupDet.setCouponDiscountPct(coupDetailsList.getCouponDiscountPct());
						coupDet.setCouponListID(coupDetailsList.getCouponListID());
						coupDet.setCouponURL(coupDetailsList.getCouponURL());
						coupDet.setViewableOnWeb(coupDetailsList.isViewableOnWeb());
						coupDet.setUsedFlag(coupDetailsList.getUsedFlag());
						couponList.add(coupDet);
						retDetails.setCouponDetailsList(couponList);
					}
				}
				else	{
					retDetails = new RetailersDetails();
					retDetails.setRetId(coupDetailsList.getRetId());
					retDetails.setRetName(coupDetailsList.getRetName());
					couponList = new ArrayList<CouponDetails>();
					final CouponDetails coupDet = new CouponDetails();
					coupDet.setCouponId(coupDetailsList.getCouponId());
					coupDet.setCouponName(coupDetailsList.getCouponName());
					coupDet.setUserCoupGallId(coupDetailsList.getUserCoupGallId());
					coupDet.setIsDateFormated(true);
					coupDet.setCouponStartDate(coupDetailsList.getCouponStartDate());
					coupDet.setCouponExpireDate(coupDetailsList.getCouponExpireDate());
					coupDet.setIsDateFormated(null);
					coupDet.setCouponImagePath(coupDetailsList.getCouponImagePath());
					coupDet.setCouponURL(coupDetailsList.getCouponURL());
					coupDet.setBusCatId(coupDetailsList.getBusCatId());
					coupDet.setBusCatName(coupDetailsList.getBusCatName());
					
					//Need to verify to remove this address
					coupDet.setAddress(coupDetailsList.getAddress());
					coupDet.setRetLocId(coupDetailsList.getRetLocId());
					
					coupDet.setCity(coupDetailsList.getCity());
					coupDet.setPostalCode(coupDetailsList.getPostalCode());
					coupDet.setCoupDesc(coupDetailsList.getCoupDesc());
					coupDet.setDistance(coupDetailsList.getDistance());
					coupDet.setClaimFlag(coupDetailsList.getClaimFlag());
					coupDet.setRedeemFlag(coupDetailsList.getRedeemFlag());
					coupDet.setNewFlag(coupDetailsList.getNewFlag());
					coupDet.setUsed(coupDetailsList.getUsed());
					coupDet.setCouponDiscountAmount(coupDetailsList.getCouponDiscountAmount());
					coupDet.setCouponDiscountPct(coupDetailsList.getCouponDiscountPct());
					coupDet.setCouponListID(coupDetailsList.getCouponListID());
					coupDet.setCouponURL(coupDetailsList.getCouponURL());
					coupDet.setViewableOnWeb(coupDetailsList.isViewableOnWeb());
					coupDet.setUsedFlag(coupDetailsList.getUsedFlag());
					couponList.add(coupDet);
					retDetails.setCouponDetailsList(couponList);
				}
				retDetailsInfoMap.put(key, retDetails);
			}
		}
		final Set<Map.Entry<String, RetailersDetails>> set = retDetailsInfoMap.entrySet();
		final ArrayList<RetailersDetails> retDetLst = new ArrayList<RetailersDetails>();
		for (Map.Entry<String, RetailersDetails> entry : set)
		{
			retDetLst.add(entry.getValue());
		}	
		SortCouponLocByRetailer objSortCoupByRet = new SortCouponLocByRetailer();
		Collections.sort(retDetLst, objSortCoupByRet);
		objCouponsDetails.setRetDetailsList(retDetLst);
		return objCouponsDetails;
	}
	
	/**
	 * Method to sort coupons by category in product tab.
	 * @param couponDetailsResultList as request.
	 * @return objCouponsDetails in response/
	 */
	public static CouponsDetails sortAllCouponsByCatForProduct(List<CouponDetails> couponDetailsResultList)	{
		CouponsDetails objCouponsDetails = new CouponsDetails();
		String key = null;
		final HashMap<String, CategoryInfo> couponCategoryInfoMap = new HashMap<String, CategoryInfo>();
		ArrayList<CouponDetails> couponList;
		CategoryInfo categoryInfo = null;
		
		for (CouponDetails coupDetailsList : couponDetailsResultList)	{
			key = coupDetailsList.getCatName();	
			if (!"".equals(Utility.isNull(key)))	{
				key = Character.toUpperCase(key.charAt(0)) + key.substring(1);
				if (couponCategoryInfoMap.containsKey(key))	{
					categoryInfo = couponCategoryInfoMap.get(key);
					couponList = categoryInfo.getCouponDetailsList();
					if (null != couponList)	{
						final CouponDetails objCouponDetails = new CouponDetails();
						objCouponDetails.setCouponId(coupDetailsList.getCouponId());
						objCouponDetails.setCouponName(coupDetailsList.getCouponName());
						objCouponDetails.setUserCoupGallId(coupDetailsList.getUserCoupGallId());
						objCouponDetails.setIsDateFormated(true);
						objCouponDetails.setCouponStartDate(coupDetailsList.getCouponStartDate());
						objCouponDetails.setCouponExpireDate(coupDetailsList.getCouponExpireDate());
						objCouponDetails.setIsDateFormated(null);
						objCouponDetails.setCouponImagePath(coupDetailsList.getCouponImagePath());
						objCouponDetails.setCouponURL(coupDetailsList.getCouponURL());
						objCouponDetails.setCoupDesc(coupDetailsList.getCoupDesc());
						objCouponDetails.setDistance(coupDetailsList.getDistance());
						objCouponDetails.setClaimFlag(coupDetailsList.getClaimFlag());
						objCouponDetails.setRedeemFlag(coupDetailsList.getRedeemFlag());
						objCouponDetails.setNewFlag(coupDetailsList.getNewFlag());
						objCouponDetails.setUsed(coupDetailsList.getUsed());
						objCouponDetails.setCoupToIssue(coupDetailsList.getCoupToIssue());
						objCouponDetails.setCouponDiscountAmount(coupDetailsList.getCouponDiscountAmount());
						objCouponDetails.setCouponDiscountPct(coupDetailsList.getCouponDiscountPct());
						objCouponDetails.setCouponListID(coupDetailsList.getCouponListID());
						objCouponDetails.setViewableOnWeb(coupDetailsList.isViewableOnWeb());
						objCouponDetails.setUsedFlag(coupDetailsList.getUsedFlag());
						couponList.add(objCouponDetails);
						categoryInfo.setCouponDetailsList(couponList);
					}
				}
				else	{
					categoryInfo = new CategoryInfo();
					categoryInfo.setCategoryID(coupDetailsList.getCatId());
					categoryInfo.setCategoryName(coupDetailsList.getCatName());
					couponList = new ArrayList<CouponDetails>();
					final CouponDetails objCouponDetails = new CouponDetails();
					objCouponDetails.setCouponId(coupDetailsList.getCouponId());
					objCouponDetails.setCouponName(coupDetailsList.getCouponName());
					objCouponDetails.setUserCoupGallId(coupDetailsList.getUserCoupGallId());
					objCouponDetails.setIsDateFormated(true);
					objCouponDetails.setCouponStartDate(coupDetailsList.getCouponStartDate());
					objCouponDetails.setCouponExpireDate(coupDetailsList.getCouponExpireDate());
					objCouponDetails.setIsDateFormated(null);
					objCouponDetails.setCouponImagePath(coupDetailsList.getCouponImagePath());
					objCouponDetails.setCouponURL(coupDetailsList.getCouponURL());
					objCouponDetails.setCoupDesc(coupDetailsList.getCoupDesc());
					objCouponDetails.setDistance(coupDetailsList.getDistance());
					objCouponDetails.setClaimFlag(coupDetailsList.getClaimFlag());
					objCouponDetails.setRedeemFlag(coupDetailsList.getRedeemFlag());
					objCouponDetails.setNewFlag(coupDetailsList.getNewFlag());
					objCouponDetails.setUsed(coupDetailsList.getUsed());
					objCouponDetails.setCoupToIssue(coupDetailsList.getCoupToIssue());
					objCouponDetails.setCouponDiscountAmount(coupDetailsList.getCouponDiscountAmount());
					objCouponDetails.setCouponDiscountPct(coupDetailsList.getCouponDiscountPct());
					objCouponDetails.setCouponListID(coupDetailsList.getCouponListID());
					objCouponDetails.setViewableOnWeb(coupDetailsList.isViewableOnWeb());
					objCouponDetails.setUsedFlag(coupDetailsList.getUsedFlag());
					couponList.add(objCouponDetails);
					categoryInfo.setCouponDetailsList(couponList);
				}
				couponCategoryInfoMap.put(key, categoryInfo);
			}
		}
		
		final Set<Map.Entry<String, CategoryInfo>> set = couponCategoryInfoMap.entrySet();
		final ArrayList<CategoryInfo> categoryInfolst = new ArrayList<CategoryInfo>();
		for (Map.Entry<String, CategoryInfo> entry : set)
		{
			categoryInfolst.add(entry.getValue());
		}
		SortCouponProdByCategory objSortCoupByCat = new SortCouponProdByCategory();
		Collections.sort(categoryInfolst, objSortCoupByCat);
		objCouponsDetails.setCategoryInfoList(categoryInfolst);
		return objCouponsDetails;
	}
	
	/**
	 * Method to sort coupons by business category.
	 * @param couponDetailsResultList
	 * @return objCouponsDetails in response.
	 */
	public static CouponsDetails sortAllCouponsByBusinessCat(List<CouponDetails> couponDetailsResultList)	{
		CouponsDetails objCouponsDetails = new CouponsDetails();
		String key = null;
		final HashMap<String, CategoryInfo> categoryInfoMap = new HashMap<String, CategoryInfo>();
		ArrayList<CouponDetails> couponList;
		CategoryInfo categoryInfo;
		
		for (CouponDetails coupDetailsList : couponDetailsResultList)	{
			key = coupDetailsList.getBusCatName();
			if (!"".equals(Utility.isNull(key)))	{
				key = Character.toUpperCase(key.charAt(0)) + key.substring(1);
				if (categoryInfoMap.containsKey(key))	{
					categoryInfo = categoryInfoMap.get(key);
					couponList = categoryInfo.getCouponDetailsList();
					if (null != couponList)	{
						final CouponDetails coupDet = new CouponDetails();
						coupDet.setCouponId(coupDetailsList.getCouponId());
						coupDet.setCouponName(coupDetailsList.getCouponName());
						coupDet.setUserCoupGallId(coupDetailsList.getUserCoupGallId());
						coupDet.setCouponStartDate(coupDetailsList.getCouponStartDate());
						coupDet.setCouponExpireDate(coupDetailsList.getCouponExpireDate());
						coupDet.setCouponImagePath(coupDetailsList.getCouponImagePath());
						coupDet.setCouponURL(coupDetailsList.getCouponURL());
						coupDet.setAddress(coupDetailsList.getAddress());
						coupDet.setCity(coupDetailsList.getCity());
						coupDet.setPostalCode(coupDetailsList.getPostalCode());
						coupDet.setCoupDesc(coupDetailsList.getCoupDesc());
						coupDet.setDistance(coupDetailsList.getDistance());
						coupDet.setClaimFlag(coupDetailsList.getClaimFlag());
						coupDet.setRedeemFlag(coupDetailsList.getRedeemFlag());
						coupDet.setNewFlag(coupDetailsList.getNewFlag());
						coupDet.setUsed(coupDetailsList.getUsed());
						coupDet.setCouponDiscountAmount(coupDetailsList.getCouponDiscountAmount());
						coupDet.setCouponListID(coupDetailsList.getCouponListID());
						coupDet.setCouponURL(coupDetailsList.getCouponURL());
						coupDet.setViewableOnWeb(coupDetailsList.isViewableOnWeb());
						coupDet.setUsedFlag(coupDetailsList.getUsedFlag());
						coupDet.setCouponDiscountPct(coupDetailsList.getCouponDiscountPct());
						coupDet.setRetName(coupDetailsList.getRetName());
						coupDet.setRetId(coupDetailsList.getRetId());
						coupDet.setRetLocId(coupDetailsList.getRetLocId());
						couponList.add(coupDet);
						categoryInfo.setCouponDetailsList(couponList);
					}
				}
				else	{
					categoryInfo = new CategoryInfo();
					categoryInfo.setCategoryID(coupDetailsList.getBusCatId());
					categoryInfo.setCategoryName(coupDetailsList.getBusCatName());
					couponList = new ArrayList<CouponDetails>();
					final CouponDetails coupDet = new CouponDetails();
					coupDet.setCouponId(coupDetailsList.getCouponId());
					coupDet.setCouponName(coupDetailsList.getCouponName());
					coupDet.setUserCoupGallId(coupDetailsList.getUserCoupGallId());
					coupDet.setCouponStartDate(coupDetailsList.getCouponStartDate());
					coupDet.setCouponExpireDate(coupDetailsList.getCouponExpireDate());
					coupDet.setCouponImagePath(coupDetailsList.getCouponImagePath());
					coupDet.setCouponURL(coupDetailsList.getCouponURL());
					coupDet.setAddress(coupDetailsList.getAddress());
					coupDet.setCity(coupDetailsList.getCity());
					coupDet.setPostalCode(coupDetailsList.getPostalCode());
					coupDet.setCoupDesc(coupDetailsList.getCoupDesc());
					coupDet.setDistance(coupDetailsList.getDistance());
					coupDet.setClaimFlag(coupDetailsList.getClaimFlag());
					coupDet.setRedeemFlag(coupDetailsList.getRedeemFlag());
					coupDet.setNewFlag(coupDetailsList.getNewFlag());
					coupDet.setUsed(coupDetailsList.getUsed());
					coupDet.setCouponDiscountAmount(coupDetailsList.getCouponDiscountAmount());
					coupDet.setCouponListID(coupDetailsList.getCouponListID());
					coupDet.setCouponURL(coupDetailsList.getCouponURL());
					coupDet.setViewableOnWeb(coupDetailsList.isViewableOnWeb());
					coupDet.setUsedFlag(coupDetailsList.getUsedFlag());
					coupDet.setCouponDiscountPct(coupDetailsList.getCouponDiscountPct());
					coupDet.setRetName(coupDetailsList.getRetName());
					coupDet.setRetId(coupDetailsList.getRetId());
					coupDet.setRetLocId(coupDetailsList.getRetLocId());
					
					couponList.add(coupDet);
					categoryInfo.setCouponDetailsList(couponList);
				}
				categoryInfoMap.put(key, categoryInfo);
			}
		}
		final Set<Map.Entry<String, CategoryInfo>> set = categoryInfoMap.entrySet();
		final ArrayList<CategoryInfo> categoryLst = new ArrayList<CategoryInfo>();
		for (Map.Entry<String, CategoryInfo> entry : set)
		{
			categoryLst.add(entry.getValue());
		}
		SortCouponProdByCategory objSortCoupByRet = new SortCouponProdByCategory();
		Collections.sort(categoryLst, objSortCoupByRet);
		objCouponsDetails.setCategoryInfoList(categoryLst);
		
		return objCouponsDetails;
	}
	/**
	 * Method to sort HotDealList by category.
	 * @param hotDealDetailsList
	 * @return
	 */
	public static HotDealsListResultSet sortHotDealsListByCategory(List<HotDealsDetails> hotDealDetailsList)	{
		HotDealsListResultSet objHotDeals = new HotDealsListResultSet();
		String key = null;
		final HashMap<String, CategoryInfo> hDInfoMap = new HashMap<String, CategoryInfo>();
		ArrayList<HotDealsDetails> hdDetailsList;
		CategoryInfo categoryInfo = null;
		
		for (HotDealsDetails hDDetails : hotDealDetailsList)	{
			key = hDDetails.getCatName();
			if (!"".equals(Utility.isNull(key)))	{
				if (hDInfoMap.containsKey(key))	{
					categoryInfo = hDInfoMap.get(key);
					hdDetailsList = categoryInfo.gethDDetailsList();
					if (null != hdDetailsList)	{
						final HotDealsDetails objHDDetails = new HotDealsDetails();
						objHDDetails.setHotDealName(hDDetails.getHotDealName());
						objHDDetails.setHotDealId(hDDetails.getHotDealId());
						objHDDetails.setHotDealImagePath(hDDetails.getHotDealImagePath());
						objHDDetails.setHdURL(hDDetails.getHdURL());
						objHDDetails.setIsDateFormated(false);
						objHDDetails.sethDStartDate(hDDetails.gethDStartDate());
						objHDDetails.sethDEndDate(hDDetails.gethDEndDate());
						objHDDetails.setIsDateFormated(null);
						objHDDetails.sethDDiscountAmount(hDDetails.gethDDiscountAmount());
						objHDDetails.sethDDiscountPct(hDDetails.gethDDiscountPct());
						objHDDetails.sethDExpDate(hDDetails.gethDExpDate());
						objHDDetails.sethDGallId(hDDetails.gethDGallId());
						objHDDetails.sethDDesc(hDDetails.gethDDesc());
						objHDDetails.setUsedFlag(hDDetails.getUsedFlag());
						
						objHDDetails.setAddress(hDDetails.getAddress());
						objHDDetails.setRetId(hDDetails.getRetId());
						objHDDetails.setRetName(hDDetails.getRetName());
						objHDDetails.setRetLocId(hDDetails.getRetLocId());
						
						hdDetailsList.add(objHDDetails);
						categoryInfo.sethDDetailsList(hdDetailsList);
					}
				}
				else	{
					categoryInfo = new CategoryInfo();
					if (null != hDDetails.getCatId())	{
						categoryInfo.setCategoryID(hDDetails.getCatId());
					}
					else	{
						categoryInfo.setCategoryID(0);
					}
					categoryInfo.setCategoryName(hDDetails.getCatName());
					hdDetailsList = new ArrayList<HotDealsDetails>();
					final HotDealsDetails objHDDetails = new HotDealsDetails();
					objHDDetails.setHotDealName(hDDetails.getHotDealName());
					objHDDetails.setHotDealId(hDDetails.getHotDealId());
					objHDDetails.setHotDealImagePath(hDDetails.getHotDealImagePath());
					objHDDetails.setHdURL(hDDetails.getHdURL());
					objHDDetails.setIsDateFormated(false);
					objHDDetails.sethDStartDate(hDDetails.gethDStartDate());
					objHDDetails.sethDEndDate(hDDetails.gethDEndDate());
					objHDDetails.setIsDateFormated(null);
					objHDDetails.sethDDiscountAmount(hDDetails.gethDDiscountAmount());
					objHDDetails.sethDDiscountPct(hDDetails.gethDDiscountPct());
					objHDDetails.sethDExpDate(hDDetails.gethDExpDate());
					objHDDetails.sethDGallId(hDDetails.gethDGallId());
					objHDDetails.sethDDesc(hDDetails.gethDDesc());
					objHDDetails.setUsedFlag(hDDetails.getUsedFlag());
					
					objHDDetails.setAddress(hDDetails.getAddress());
					objHDDetails.setRetId(hDDetails.getRetId());
					objHDDetails.setRetName(hDDetails.getRetName());
					objHDDetails.setRetLocId(hDDetails.getRetLocId());
					
					hdDetailsList.add(objHDDetails);
					categoryInfo.sethDDetailsList(hdDetailsList);
				}
				hDInfoMap.put(key, categoryInfo);
			}
		}
		final Set<Map.Entry<String, CategoryInfo>> set = hDInfoMap.entrySet();
		final ArrayList<CategoryInfo> categoryLst = new ArrayList<CategoryInfo>();
		for (Map.Entry<String, CategoryInfo> entry : set)	{
			categoryLst.add(entry.getValue());
		}
		SortCouponProdByCategory objSortCoupByRet = new SortCouponProdByCategory();
		Collections.sort(categoryLst, objSortCoupByRet);
		objHotDeals.setCategoryInfoList(categoryLst);	
		return objHotDeals;
	}
	
	/**
	 * Method to sort HotDeal list by retailer.
	 * @param hotDealDetailsList as request
	 * @return objHotDeals HotDealsListResultSet object.
	 */
	public static HotDealsListResultSet sortHotDealsListByRetailer(List<HotDealsDetails> hotDealDetailsList)	{
		HotDealsListResultSet objHotDeals = new HotDealsListResultSet();
		String key = null;
		final HashMap<String, RetailersDetails> hDInfoMap = new HashMap<String, RetailersDetails>();
		ArrayList<HotDealsDetails> hdDetailsList;
		RetailersDetails objRetailersDetails = null;
		
		for (HotDealsDetails hDDetails : hotDealDetailsList)	{
			key = hDDetails.getRetName();
			if (!"".equals(Utility.isNull(key)))	{
				if (hDInfoMap.containsKey(key))	{
					objRetailersDetails = hDInfoMap.get(key);
					hdDetailsList = objRetailersDetails.gethDDetailsList();
					if (null != hdDetailsList)	{
						final HotDealsDetails objHDDetails = new HotDealsDetails();
						objHDDetails.setHotDealName(hDDetails.getHotDealName());
						objHDDetails.setHotDealId(hDDetails.getHotDealId());
						objHDDetails.setHotDealImagePath(hDDetails.getHotDealImagePath());
						objHDDetails.setHdURL(hDDetails.getHdURL());
						objHDDetails.sethDStartDate(hDDetails.gethDStartDate());
						objHDDetails.sethDEndDate(hDDetails.gethDEndDate());
						objHDDetails.sethDDiscountAmount(hDDetails.gethDDiscountAmount());
						objHDDetails.sethDDiscountPct(hDDetails.gethDDiscountPct());
						objHDDetails.sethDExpDate(hDDetails.gethDExpDate());
						objHDDetails.sethDGallId(hDDetails.gethDGallId());
						objHDDetails.sethDDesc(hDDetails.gethDDesc());
						objHDDetails.setUsedFlag(hDDetails.getUsedFlag());
						objHDDetails.setAddress(hDDetails.getAddress());
						objHDDetails.setRetLocId(hDDetails.getRetLocId());
						hdDetailsList.add(objHDDetails);
						objRetailersDetails.sethDDetailsList(hdDetailsList);
					}
				}
				else	{
					objRetailersDetails = new RetailersDetails();
					objRetailersDetails.setRetId(hDDetails.getRetId());
					objRetailersDetails.setRetName(hDDetails.getRetName());
//					objRetailersDetails.setRetLocId(hDDetails.getRetLocId());
					hdDetailsList = new ArrayList<HotDealsDetails>();
					final HotDealsDetails objHDDetails = new HotDealsDetails();
					objHDDetails.setHotDealName(hDDetails.getHotDealName());
					objHDDetails.setHotDealId(hDDetails.getHotDealId());
					objHDDetails.setHotDealImagePath(hDDetails.getHotDealImagePath());
					objHDDetails.setHdURL(hDDetails.getHdURL());
					objHDDetails.sethDStartDate(hDDetails.gethDStartDate());
					objHDDetails.sethDEndDate(hDDetails.gethDEndDate());
					objHDDetails.sethDDiscountAmount(hDDetails.gethDDiscountAmount());
					objHDDetails.sethDDiscountPct(hDDetails.gethDDiscountPct());
					objHDDetails.sethDExpDate(hDDetails.gethDExpDate());
					objHDDetails.sethDGallId(hDDetails.gethDGallId());
					objHDDetails.sethDDesc(hDDetails.gethDDesc());
					objHDDetails.setUsedFlag(hDDetails.getUsedFlag());
					objHDDetails.setAddress(hDDetails.getAddress());
					objHDDetails.setRetLocId(hDDetails.getRetLocId());
					hdDetailsList.add(objHDDetails);
					objRetailersDetails.sethDDetailsList(hdDetailsList);
				}
				hDInfoMap.put(key, objRetailersDetails);
			}
		}
		final Set<Map.Entry<String, RetailersDetails>> set = hDInfoMap.entrySet();
		final ArrayList<RetailersDetails> retailerDetailsList = new ArrayList<RetailersDetails>();
		for (Map.Entry<String, RetailersDetails> entry : set)	{
			retailerDetailsList.add(entry.getValue());
		}
		SortCouponLocByRetailer objSortCoupByRet = new SortCouponLocByRetailer();
		Collections.sort(retailerDetailsList, objSortCoupByRet);
		objHotDeals.setRetDetailsList(retailerDetailsList);	
		return objHotDeals;
	}
	
	/**
	 * Method to sort Coupons by distance and arrange by retailers.
	 * @param couponDetailsResultList as request
	 * @return objCouponsDetails CouponsDetails object
	 */
	public static CouponsDetails sortRetailerCoupByDist(List<CouponDetails> couponDetailsResultList)	{
		final CouponsDetails objCouponsDetails = new CouponsDetails();
		String key = null;
		final HashMap<String, RetailersDetails> retDetailsInfoMap = new HashMap<String, RetailersDetails>();
		ArrayList<CouponDetails> couponList;
		RetailersDetails retDetails;
		
		for (CouponDetails coupDetailsList : couponDetailsResultList)	{
			key = coupDetailsList.getRetLocId().toString();
			if (!"".equals(Utility.isNull(key)))	{
				if (retDetailsInfoMap.containsKey(key))	{
					retDetails = retDetailsInfoMap.get(key);
					couponList = retDetails.getCouponDetailsList();
					if (null != couponList)	{
						final CouponDetails coupDet = new CouponDetails();
						coupDet.setCouponId(coupDetailsList.getCouponId());
						coupDet.setCouponName(coupDetailsList.getCouponName());
						coupDet.setUserCoupGallId(coupDetailsList.getUserCoupGallId());
						coupDet.setIsDateFormated(true);
						coupDet.setCouponStartDate(coupDetailsList.getCouponStartDate());
						coupDet.setCouponExpireDate(coupDetailsList.getCouponExpireDate());
						coupDet.setIsDateFormated(null);
						coupDet.setCouponImagePath(coupDetailsList.getCouponImagePath());
						coupDet.setCouponURL(coupDetailsList.getCouponURL());
						coupDet.setCoupDesc(coupDetailsList.getCoupDesc());
						coupDet.setClaimFlag(coupDetailsList.getClaimFlag());
						coupDet.setRedeemFlag(coupDetailsList.getRedeemFlag());
						coupDet.setNewFlag(coupDetailsList.getNewFlag());
						coupDet.setUsed(coupDetailsList.getUsed());
						coupDet.setCouponDiscountAmount(coupDetailsList.getCouponDiscountAmount());
						coupDet.setCouponDiscountPct(coupDetailsList.getCouponDiscountPct());
						coupDet.setCouponListID(coupDetailsList.getCouponListID());
						coupDet.setCouponURL(coupDetailsList.getCouponURL());
						coupDet.setViewableOnWeb(coupDetailsList.isViewableOnWeb());
						coupDet.setUsedFlag(coupDetailsList.getUsedFlag());
						couponList.add(coupDet);
						retDetails.setCouponDetailsList(couponList);
					}
				}
				else	{
					retDetails = new RetailersDetails();
					retDetails.setRetLocId(coupDetailsList.getRetLocId());
					retDetails.setRetailerAddress(coupDetailsList.getAddress());
					retDetails.setCity(coupDetailsList.getCity());
					retDetails.setState(coupDetailsList.getState());
					retDetails.setPostalCode(coupDetailsList.getPostalCode());
					retDetails.setMinRetDist(coupDetailsList.getDistance());
					couponList = new ArrayList<CouponDetails>();
					final CouponDetails coupDet = new CouponDetails();
					coupDet.setCouponId(coupDetailsList.getCouponId());
					coupDet.setCouponName(coupDetailsList.getCouponName());
					coupDet.setUserCoupGallId(coupDetailsList.getUserCoupGallId());
					coupDet.setIsDateFormated(true);
					coupDet.setCouponStartDate(coupDetailsList.getCouponStartDate());
					coupDet.setCouponExpireDate(coupDetailsList.getCouponExpireDate());
					coupDet.setIsDateFormated(null);
					coupDet.setCouponImagePath(coupDetailsList.getCouponImagePath());
					coupDet.setCouponURL(coupDetailsList.getCouponURL());
					coupDet.setCoupDesc(coupDetailsList.getCoupDesc());
					coupDet.setClaimFlag(coupDetailsList.getClaimFlag());
					coupDet.setRedeemFlag(coupDetailsList.getRedeemFlag());
					coupDet.setNewFlag(coupDetailsList.getNewFlag());
					coupDet.setUsed(coupDetailsList.getUsed());
					coupDet.setCouponDiscountAmount(coupDetailsList.getCouponDiscountAmount());
					coupDet.setCouponDiscountPct(coupDetailsList.getCouponDiscountPct());
					coupDet.setCouponListID(coupDetailsList.getCouponListID());
					coupDet.setCouponURL(coupDetailsList.getCouponURL());
					coupDet.setViewableOnWeb(coupDetailsList.isViewableOnWeb());
					coupDet.setUsedFlag(coupDetailsList.getUsedFlag());
					couponList.add(coupDet);
					retDetails.setCouponDetailsList(couponList);
				}
				retDetailsInfoMap.put(key, retDetails);
			}
		}
		final Set<Map.Entry<String, RetailersDetails>> set = retDetailsInfoMap.entrySet();
		final ArrayList<RetailersDetails> retDetLst = new ArrayList<RetailersDetails>();
		for (Map.Entry<String, RetailersDetails> entry : set)
		{
			retDetLst.add(entry.getValue());
		}	
		SortCoupLocationByRetDist objSortCoupByRet = new SortCoupLocationByRetDist();
		Collections.sort(retDetLst, objSortCoupByRet);
		objCouponsDetails.setRetDetailsList(retDetLst);
		return objCouponsDetails;
	}
	
	/**
	 * Method to get HotDeal List by reailers. 
	 * @param hotDealDetailsList as request
	 * @return  objHotDeals HotDealsListResultSet object
	 */
	public static HotDealsListResultSet getHotDealsListByRetailerLoc(List<HotDealsDetails> hotDealDetailsList)	{
		final HotDealsListResultSet objHotDeals = new HotDealsListResultSet();
		String key = null;
		final HashMap<String, RetailersDetails> hDInfoMap = new HashMap<String, RetailersDetails>();
		ArrayList<HotDealsDetails> hdDetailsList;
		RetailersDetails objRetInfo = null;
		
		for (HotDealsDetails hDDetails : hotDealDetailsList)	{
			key = hDDetails.getRetLocId().toString();
			if (!"".equals(Utility.isNull(key)))	{
				if (hDInfoMap.containsKey(key))	{
					objRetInfo = hDInfoMap.get(key);
					hdDetailsList = objRetInfo.gethDDetailsList();
					if (null != hdDetailsList)	{
						final HotDealsDetails objHDDetails = new HotDealsDetails();
						objHDDetails.setHotDealName(hDDetails.getHotDealName());
						objHDDetails.setHotDealId(hDDetails.getHotDealId());
						objHDDetails.setHotDealImagePath(hDDetails.getHotDealImagePath());
						objHDDetails.setHdURL(hDDetails.getHdURL());
						objHDDetails.setIsDateFormated(false);
						objHDDetails.sethDStartDate(hDDetails.gethDStartDate());
						objHDDetails.sethDEndDate(hDDetails.gethDEndDate());
						objHDDetails.setIsDateFormated(null);
						objHDDetails.sethDDiscountAmount(hDDetails.gethDDiscountAmount());
						objHDDetails.sethDDiscountPct(hDDetails.gethDDiscountPct());
						objHDDetails.sethDExpDate(hDDetails.gethDExpDate());
						objHDDetails.sethDGallId(hDDetails.gethDGallId());
						objHDDetails.sethDDesc(hDDetails.gethDDesc());
						objHDDetails.setUsedFlag(hDDetails.getUsedFlag());
						hdDetailsList.add(objHDDetails);
						objRetInfo.sethDDetailsList(hdDetailsList);
					}
				}
				else	{
					objRetInfo = new RetailersDetails();
					objRetInfo.setRetLocId(hDDetails.getRetLocId());
					objRetInfo.setRetailerAddress(hDDetails.getAddress());
					hdDetailsList = new ArrayList<HotDealsDetails>();
					final HotDealsDetails objHDDetails = new HotDealsDetails();
					objHDDetails.setHotDealName(hDDetails.getHotDealName());
					objHDDetails.setHotDealId(hDDetails.getHotDealId());
					objHDDetails.setHotDealImagePath(hDDetails.getHotDealImagePath());
					objHDDetails.setHdURL(hDDetails.getHdURL());
					objHDDetails.setIsDateFormated(false);
					objHDDetails.sethDStartDate(hDDetails.gethDStartDate());
					objHDDetails.sethDEndDate(hDDetails.gethDEndDate());
					objHDDetails.setIsDateFormated(null);
					objHDDetails.sethDDiscountAmount(hDDetails.gethDDiscountAmount());
					objHDDetails.sethDDiscountPct(hDDetails.gethDDiscountPct());
					objHDDetails.sethDExpDate(hDDetails.gethDExpDate());
					objHDDetails.sethDGallId(hDDetails.gethDGallId());
					objHDDetails.sethDDesc(hDDetails.gethDDesc());
					objHDDetails.setUsedFlag(hDDetails.getUsedFlag());
					hdDetailsList.add(objHDDetails);
					objRetInfo.sethDDetailsList(hdDetailsList);
				}
				hDInfoMap.put(key, objRetInfo);
			}
		}
		final Set<Map.Entry<String, RetailersDetails>> set = hDInfoMap.entrySet();
		final ArrayList<RetailersDetails> retDetailsLst = new ArrayList<RetailersDetails>();
		for (Map.Entry<String, RetailersDetails> entry : set)	{
			retDetailsLst.add(entry.getValue());
		}
//		SortCouponProdByCategory objSortCoupByRet = new SortCouponProdByCategory();
//		Collections.sort(categoryLst, objSortCoupByRet);
		objHotDeals.setRetDetailsList(retDetailsLst);	
		return objHotDeals;
	}
}
