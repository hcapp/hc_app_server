package com.scansee.mygallery.service;

import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.pojos.RetailersDetails;
import com.scansee.mygallery.dao.MyGalleryDAOImpl;

public class MygalleryLoyaltySort implements Comparator<RetailersDetails>
{
	
	/**
	 * Logger instance.
	 */
	private static final Logger log = LoggerFactory.getLogger(MyGalleryDAOImpl.class.getName());
	
	/**
	 * This method is to compare two products.
	 * 
	 * @param wishListResultSet1
	 *            -As parameter
	 * @param wishListResultSet2
	 *            -As parameter
	 * @return scanDateComp
	 */
	public int compare(RetailersDetails cLRDetailslist1, RetailersDetails cLRDetailslist2)
	{

		String catName1;
		String catName2;
		int scanDateComp = 0;
	

		try
		{
			catName1 = cLRDetailslist1.getRetName();
			catName2 = cLRDetailslist2.getRetName();
			scanDateComp = catName1.compareTo(catName2);

		}
		catch (Exception exception)
		{
			log.info("exception in MygalleryLoyaltySort " + exception);
		}

		return scanDateComp;

	}

}
