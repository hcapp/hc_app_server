package com.scansee.mygallery.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.servicefactory.ServiceFactory;
import com.scansee.common.util.Utility;
import com.scansee.mygallery.service.MyGalleryService;

/**
 * The MyGalleryRestEasy class has methods to accept requests for my gallery
 * module from client. The method is selected based on the URL Path and the type
 * of request(GET,POST). It invokes the appropriate method in Service layer.
 * 
 * @author sowjanya_d
 */
public class MyGalleryRestEasyImpl implements MyGalleryRestEasy
{
	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(MyGalleryRestEasyImpl.class);
	/**
	 * debugger flag for logging.
	 */
	private static final boolean ISDEBUGENABLED = LOG.isDebugEnabled();

	/**
	 * Method for fetching gallery Info. The method is called from the service
	 * layer.
	 * 
	 * @param xml
	 *            as the request.
	 * @return response as String .
	 */
	@Override
	public String getMyGalleryInfo(String xml)
	{

		final String methodName = " getMyGalleryInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client" + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.getMyGalleryInfo(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is : " + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method for Redeeming Coupon. The method is called from the service layer.
	 * 
	 * @param xml
	 *            as the request.
	 * @return response as String .
	 */

	@Override
	public String userRedeemCoupon(String xml)
	{
		final String methodName = "userRedeemCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client : " + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.userRedeemCoupon(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is : " + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method for Redeeming loyalty. The method is called from the service
	 * layer.
	 * 
	 * @param xml
	 *            as the request.
	 * @return response as String .
	 */

	@Override
	public String userRedeemLoyalty(String xml)
	{
		final String methodName = "userRedeemLoyalty";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client: " + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.userRedeemLoyalty(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is : " + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method for Redeeming Rebate. The method is called from the service layer.
	 * 
	 * @param xml
	 *            as the request.
	 * @return response as String .
	 */

	@Override
	public String userRedeemRebate(String xml)
	{
		final String methodName = "userRedeemRebate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug(" Request from Client: " + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.userRedeemRebate(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is : " + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method for deleting used coupon. The method is called from the service
	 * layer.
	 * 
	 * @param xml
	 *            as the request containing used gallery coupon id and user id
	 * @return response as String .
	 */

	@Override
	public String deleteUsedCoupon(String xml)
	{
		final String methodName = "deleteUsedCoupon";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client: " + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.deleteUsedCoupon(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is : " + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method for deleting used loyalty. The method is called from the service
	 * layer.
	 * 
	 * @param xml
	 *            as the request containing used gallery loyalty id and user id
	 * @return response as String .
	 */

	@Override
	public String deleteUsedLoyalty(String xml)
	{
		final String methodName = "deleteUsedLoyalty";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client:" + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.deleteUsedLoyalty(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method for deleting used rebate. The method is called from the service
	 * layer.
	 * 
	 * @param xml
	 *            as the request containing used gallery rebate id and user id
	 * @return response as String .
	 */
	@Override
	public String deleteUsedRebate(String xml)
	{
		final String methodName = "deleteUsedRebate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client:" + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.deleteUsedRebate(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method for retriving category list. The method is called from the service
	 * layer.
	 * 
	 * @param
	 * @return response as String contails categoryID and its details.
	 */
	@Override
	public String retriveCategory()
	{
		final String methodName = "retriveCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client:");
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.retriveCategory();
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method for fetching coupon retailer list for searching . layer.
	 * 
	 * @param
	 * @return response as String contains retailer id and name
	 */
	@Override
	public String getCouponRetailer()
	{
		final String methodName = "getCouponRetailer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client:");
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.getCouponRetailer();
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method for fetching loyalty retailer list for searching . layer.
	 * 
	 * @param
	 * @return response as String contains retailer id and name
	 */
	@Override
	public String getLoyaltyRetailer()
	{
		final String methodName = "getLoyaltyRetailer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client:");
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.getLoyaltyRetailer();
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method to get all coupons for Location.
	 * @param xml as request
	 * @return xml containing coupon details
	 */
	@Override
	public String getAllCouponsByLocation(String xml) {
		final String methodName = "getAllCouponsByLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client:" + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			xml = xml.replaceAll("<searchKey>", "<searchKey><![CDATA[");
			xml = xml.replaceAll("</searchKey>", "]]></searchKey>");
			responseXml = myGalleryService.getAllCouponsByLocation(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method to get all coupons for Product.
	 * @param xml as request
	 * @return xml containing coupon details
	 */
	@Override
	public String getAllCouponsByProduct(String xml) {
		final String methodName = "getAllCouponsByProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client:");
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			xml = xml.replaceAll("<searchKey>", "<searchKey><![CDATA[");
			xml = xml.replaceAll("</searchKey>", "]]></searchKey>");
			responseXml = myGalleryService.getAllCouponsByProduct(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method to get population centers for coupons for Location.
	 * @param userId as request
	 * @return xml containing population center list
	 */
	@Override
	public String getCoupPopulationCentresForLoc(Integer userId) {
		final String methodName = "couponLocationPopulationCentres";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client: userId " + userId);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.getCoupPopulationCentresForLoc(userId);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method to get population centers for coupons for product.
	 * @param userId as request
	 * @return xml containing population center list
	 */
	@Override
	public String getCoupPopulationCentresForProd(Integer userId) {
		final String methodName = "getCoupPopulationCentresForProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client: userId - " + userId);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.getCoupPopulationCentresForProd(userId);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method to get business categories for coupons for Locations.
	 * @param xml as request
	 * @return xml containing business categories
	 */
	@Override
	public String getCoupLocationBusinessCategory(String xml) {
		final String methodName = "getCoupLocationBusinessCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client: userId - " + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.getCoupLocationBusinessCategory(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method to get retailers for selected business category.
	 * @param xml as request
	 * @return xml containing retailer list
	 */
	@Override
	public String getRetailerForBusinessCategory(String xml) {
		final String methodName = "getRetailerForBusinessCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client: userId - " + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.getRetailerForBusinessCategory(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method to get categories for coupons for products.
	 * 
	 * @param xml as request
	 * @return xml containing business categories.
	 */
	@Override
	public String getCoupProductCategory(String xml) {
		final String methodName = "getCoupProductCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client: userId - " + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			responseXml = myGalleryService.getCoupProductCategory(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method to get gallery coupons for All/Used/Expired in Location.
	 * @param xml as request
	 * @return xml containing coupons 
	 */
	@Override
	public String getGalleryCouponsByLocation(String xml) {
		final String methodName = "getGalleryCouponsByLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client: " + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			xml = xml.replaceAll("<searchKey>", "<searchKey><![CDATA[");
			xml = xml.replaceAll("</searchKey>", "]]></searchKey>");
			responseXml = myGalleryService.getGalleryCouponsByLocation(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}
	
	/**
	 * Method to get gallery coupons for All/Used/Expired in Products.
	 * @param xml as request
	 * @return xml containing coupons sorted by category
	 */
	@Override
	public String getGalleryCouponsByProduct(String xml) {
		final String methodName = "getGalleryCouponsByProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client: " + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			xml = xml.replaceAll("<searchKey>", "<searchKey><![CDATA[");
			xml = xml.replaceAll("</searchKey>", "]]></searchKey>");
			responseXml = myGalleryService.getGalleryCouponsByProduct(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method to get gallery_HotDeals by Location.
	 * @param xml as request
	 * @return xml containing hotdeals sorted by retailers and category.
	 */
	@Override
	public String getGalleryHotDealByLocation(String xml) {
		final String methodName = "getGalleryHotDealLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client: " + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			xml = xml.replaceAll("<searchKey>", "<searchKey><![CDATA[");
			xml = xml.replaceAll("</searchKey>", "]]></searchKey>");
			responseXml = myGalleryService.getGalleryHotDealByLocation(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method to get gallery_HotDeals by Product.
	 * @param xml as request
	 * @return xml containing hotdeals sorted by category
	 */
	@Override
	public String getGalleryHotDealByProduct(String xml) {
		final String methodName = "getGalleryHotDealProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request from Client: " + xml);
		}
		final MyGalleryService myGalleryService = ServiceFactory.getMyGalleryService();
		try
		{
			xml = xml.replaceAll("<searchKey>", "<searchKey><![CDATA[");
			xml = xml.replaceAll("</searchKey>", "]]></searchKey>");
			responseXml = myGalleryService.getGalleryHotDealByProduct(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

}
