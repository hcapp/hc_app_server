package com.scansee.mygallery.controller;

import static com.scansee.common.constants.ScanSeeURLPath.DELETEUSEDCOUPON;
import static com.scansee.common.constants.ScanSeeURLPath.DELETEUSEDLOYALTY;
import static com.scansee.common.constants.ScanSeeURLPath.DELETEUSEDREBATE;
import static com.scansee.common.constants.ScanSeeURLPath.GETCOUPRETLST;
import static com.scansee.common.constants.ScanSeeURLPath.GETLOYRETLST;
import static com.scansee.common.constants.ScanSeeURLPath.GETMYGALLERYINFO;
import static com.scansee.common.constants.ScanSeeURLPath.MYGALLERY;
import static com.scansee.common.constants.ScanSeeURLPath.RETRIVECATEGORY;
import static com.scansee.common.constants.ScanSeeURLPath.USERREDEEMCOUPON;
import static com.scansee.common.constants.ScanSeeURLPath.USERREDEEMLOYALTY;
import static com.scansee.common.constants.ScanSeeURLPath.USERREDEEMREBATE;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.inject.ImplementedBy;
import com.scansee.common.constants.ScanSeeURLPath;

/**
 * This rest easy for my gallery module.{@link ImplementedBy}
 * {@link MyGalleryRestEasyImpl}.
 * 
 * @author sowjanya_d
 */
@Path(MYGALLERY)
public interface MyGalleryRestEasy
{

	/**
	 * Method for fetching the Coupon Info,Loyalty Info and Rebate Info. accepts
	 * XML with the add rebate requests and returns XML with Coupon Info or with
	 * Error code. Calls the corresponding method in the service layer.
	 * 
	 * @param xml
	 *            used to for Fetching coupons,loyalty and rebate information.
	 * @return String response coupons will be returned.
	 */

	@POST
	@Path(GETMYGALLERYINFO)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getMyGalleryInfo(String xml);

	/**
	 * Method for Redeeming the coupon Calls the corresponding method in the
	 * service layer.
	 * 
	 * @param xml
	 *            used for redeeming the coupon.
	 * @return String response containing success or failure will be returned.
	 */

	@POST
	@Path(USERREDEEMCOUPON)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String userRedeemCoupon(String xml);

	/**
	 * Method for Redeeming the loyalty Calls the corresponding method in the
	 * service layer.
	 * 
	 * @param xml
	 *            used for redeeming the coupon.
	 * @return String response containing success or failure will be returned.
	 */
	@POST
	@Path(USERREDEEMLOYALTY)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String userRedeemLoyalty(String xml);

	/**
	 * Method for Redeeming the rebate Calls the corresponding method in the
	 * service layer.
	 * 
	 * @param xml
	 *            used for redeeming the coupon.
	 * @return String response containing success or failure will be returned.
	 */

	@POST
	@Path(USERREDEEMREBATE)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String userRedeemRebate(String xml);

	/**
	 * Method for Deleting the coupon Calls the corresponding method in the
	 * service layer.
	 * 
	 * @param xml
	 *            used for delete used coupon.
	 * @return String response containing success or failure will be returned.
	 */

	@POST
	@Path(DELETEUSEDCOUPON)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String deleteUsedCoupon(String xml);

	/**
	 * Method for Deleting the used loyalty Calls the corresponding method in
	 * the service layer.
	 * 
	 * @param xml
	 *            used for deleting used loyalty.
	 * @return String response containing success or failure will be returned.
	 */

	@POST
	@Path(DELETEUSEDLOYALTY)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String deleteUsedLoyalty(String xml);

	/**
	 * Method for Deleting the used rebate Calls the corresponding method in the
	 * service layer.
	 * 
	 * @param xml
	 *            used for deleting the used rebate.
	 * @return String response containing success or failure will be returned.
	 */

	@POST
	@Path(DELETEUSEDREBATE)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String deleteUsedRebate(String xml);
	
	/**
	 * Method for retriving category list. The method is called from the service
	 * layer.
	 * 
	 * @param 
	 * @return response as String contails categoryID and its details.
	 */
	@GET
	@Path(RETRIVECATEGORY)
	@Produces("text/xml;charset=UTF-8")
	String retriveCategory();

	/**
	 * Method for fetching coupon retailer list for searching .
	 * layer.
	 * 
	 * @param 
	 * @return response as String contails retailer id and name
	 */
	@GET
	@Path(GETCOUPRETLST)
	@Produces("text/xml;charset=UTF-8")
	String getCouponRetailer();
	
	/**
	 * Method for fetching loyalty retailer list for searching .
	 * layer.
	 * 
	 * @param 
	 * @return response as String contains retailer id and name
	 */
	@GET
	@Path(GETLOYRETLST)
	@Produces("text/xml;charset=UTF-8")
	String getLoyaltyRetailer();
	
	/**
	 * Method to get all coupons for Location.
	 * 
	 * @param xml as request
	 * @return xml containing coupon details.
	 */
	@POST
	@Path(ScanSeeURLPath.GETALLCOUPBYLOC)
	@Produces("text/xml")
	@Consumes("text/xml")
	String getAllCouponsByLocation(String xml);
	
	/**
	 * Method to get all coupons for Product.
	 * 
	 * @param xml as request
	 * @return xml containing coupon details
	 */
	@POST
	@Path(ScanSeeURLPath.GETALLCOUPBYPROD)
	@Produces("text/xml")
	@Consumes("text/xml")
	String getAllCouponsByProduct(String xml);
	
	/**
	 * Method to get population centers for coupons for Location.
	 * 
	 * @param userId as request
	 * @return xml containing population center list
	 */
	@GET
	@Path(ScanSeeURLPath.COUPLOCPOPCENT)
	@Produces("text/xml")
	String getCoupPopulationCentresForLoc(@QueryParam("userId") Integer userId);
	
	/**
	 * Method to get population centers for coupons for product.
	 * 
	 * @param userId as request
	 * @return xml containing population center list
	 */
	@GET
	@Path(ScanSeeURLPath.COUPPRODPOPCENT)
	@Produces("text/xml")
	String getCoupPopulationCentresForProd(@QueryParam("userId") Integer userId);
	
	/**
	 * Method to get business categories for coupons for Locations.
	 * 
	 * @param xml as request
	 * @return xml containing business categories
	 */
	@POST
	@Path(ScanSeeURLPath.COUPLOCBUSCAT)
	@Produces("text/xml")
	@Consumes("text/xml")
	String getCoupLocationBusinessCategory(String xml);
	
	/**
	 * Method to get retailers for selected business category.
	 * @param xml as request
	 * @return xml containing retailer list
	 */
	@POST
	@Path(ScanSeeURLPath.RETFORBUSCAT)
	@Produces("text/xml")
	@Consumes("text/xml")
	String getRetailerForBusinessCategory(String xml);
	
	/**
	 * Method to get categories for coupons for products.
	 * @param xml as request
	 * @return xml containing business categories
	 */
	@POST
	@Path(ScanSeeURLPath.COUPPRODCAT)
	@Produces("text/xml")
	@Consumes("text/xml")
	String getCoupProductCategory(String xml);
	
	/**
	 * Method to get gallery coupons for All/Used/Expired in Location.
	 * @param xml as request
	 * @return xml containing coupons 
	 */
	@POST
	@Path(ScanSeeURLPath.GALLCOUPBULOC)
	@Produces("text/xml")
	@Consumes("text/xml")
	String getGalleryCouponsByLocation(String xml);
	
	/**
	 * Method to get gallery coupons for All/Used/Expired in Products.
	 * @param xml as request
	 * @return xml containing coupons sorted by category
	 */
	@POST
	@Path(ScanSeeURLPath.GALLCOUPBYPROD)
	@Produces("text/xml")
	@Consumes("text/xml")
	String getGalleryCouponsByProduct(String xml);
	
	/**
	 * Method to get gallery_HotDeals by Location.
	 * 
	 * @param xml as request
	 * @return xml containing hotdeals sorted by retailers and category.
	 */
	@POST
	@Path(ScanSeeURLPath.GALLHDBYLOC)
	@Produces("text/xml")
	@Consumes("text/xml")
	String getGalleryHotDealByLocation(String xml);
	
	/**
	 * Method to get gallery_HotDeals by Product.
	 * 
	 * @param xml as request
	 * @return xml containing hotdeals sorted by category
	 */
	@POST
	@Path(ScanSeeURLPath.GALLHDBYPROD)
	@Produces("text/xml")
	@Consumes("text/xml")
	String getGalleryHotDealByProduct(String xml);

}
