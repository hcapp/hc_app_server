package com.scansee.find.controller;

import static com.scansee.common.constants.ScanSeeURLPath.FINDCATSEARCH;
import static com.scansee.common.constants.ScanSeeURLPath.FINDLOCATIONDETAILS;
import static com.scansee.common.constants.ScanSeeURLPath.FINDRETSEARCH;
import static com.scansee.common.constants.ScanSeeURLPath.FINDSSCATEGORY;
import static com.scansee.common.constants.ScanSeeURLPath.FINDSSRETSEARCH;
import static com.scansee.common.constants.ScanSeeURLPath.FINDURL;
import static com.scansee.common.constants.ScanSeeURLPath.GETCATEGORY;
import static com.scansee.common.constants.ScanSeeURLPath.GETUSRRAD;
import static com.scansee.common.constants.ScanSeeURLPath.FINDGOOGLERETSEARCH;
import static com.scansee.common.constants.ScanSeeURLPath.FINDSCANSEERETSEARCH;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.scansee.common.constants.ScanSeeURLPath;

/**
 * This interface for finding location services and details.
 * 
 * @author shyamsundara_hm
 */
@Path(FINDURL)
public interface FindRestEasy
{

	/**
	 * This is a RestEasy WebService Method to find services given the
	 * coordinates. Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */

	@POST
	@Path(FINDCATSEARCH)
	@Produces("text/xml;charset=UTF-8")
	String findService(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching Google categories
	 * information. Method Type:GET
	 * 
	 * @param userId
	 *            as request parameter
	 * @return XML containing success or failure in the response.
	 */
	@POST
	@Path(GETCATEGORY)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getCategory(String xml);

	/**
	 * This is a RestEasy WebService Method to find location details given the
	 * coordinates. Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */

	@POST
	@Path(FINDLOCATIONDETAILS)
	@Produces("text/xml;charset=UTF-8")
	String findLocationDetails(String xml);

	/**
	 * This is a RestEasy WebService Method to find location details given the
	 * keyword and coordinates. Method Type:POST.
	 * 
	 * @param xml as request
	 * @return XML containing the search details in the response.
	 */

	@POST
	@Path(FINDRETSEARCH)
	@Produces("text/xml;charset=UTF-8")
	String findRetSearch(String xml);

	/**
	 * This is a RestEasy WebService Method to find services given the
	 * coordinates. Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */

	@POST
	@Path(FINDSSCATEGORY)
	@Produces("text/xml;charset=UTF-8")
	String findSSCategory(String xml);

	/**
	 * This is a RestEasy WebService Method to find services given the
	 * coordinates. Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */

	@POST
	@Path(FINDSSRETSEARCH)
	@Produces("text/xml;charset=UTF-8")
	String findSSRetailerSearch(String xml);

	/**
	 * This is Rest Easy Web service for getting radius details from database
	 * 
	 * @return returns response XML Containing radius details if exception it
	 *         will return error message XML.
	 */

	@GET
	@Path(GETUSRRAD)
	@Produces("text/xml")
	String getUserRadius(@QueryParam("userId") Integer userId, @QueryParam("modName") String moduleName);
	
	/**
	 * This is Rest Easy Web service for User Tracking. 
	 * Should be called to insert the Google Retailers to database. 
	 * 
	 * @param xml
	 */
	@POST
	@Path(ScanSeeURLPath.UTGOOGLERET)
	@Consumes("text/xml")
	void userTrackingGoogleRetailers(String xml);
	
	/**
	 * This is a RestEasy WebService Method to find location details for Google data given the
	 * keyword and coordinates. Method Type : POST.
	 * 
	 * @param xml as request
	 * @return XML containing the Google data search details in the response.
	 */

	@POST
	@Path(FINDGOOGLERETSEARCH)
	@Produces("text/xml;charset=UTF-8")
	String findGoogleRetSearch(String xml);
	
	/**
	 * This is a RestEasy WebService Method to find location details for ScanSee data given the
	 * keyword and coordinates. Method Type : POST.
	 * 
	 * @param xml as request
	 * @return XML containing the ScanSee data search details in the response.
	 */

	@POST
	@Path(FINDSCANSEERETSEARCH)
	@Produces("text/xml;charset=UTF-8")
	String findScanSeeRetSearch(String xml);
	
	/**
	 * This is a Rest Easy Web service for category search for 
	 * retailers given coordinates from ScanSee database
	 * @param xml
	 * @return Retailers List
	 */
	@POST
	@Path(ScanSeeURLPath.FINDSCANSEECATSEARCH)
	@Consumes("text/xml;charset=UTF-8")
	@Produces("text/xml;charset=UTF-8")
	String findCatSearchForScanSeeData(String xml);
	
	/**
	 * This is a Rest Easy Web service for category search for 
	 * retailers given coordinates from google results
	 * @param xml
	 * @return Retailers List
	 */
	@POST
	@Path(ScanSeeURLPath.FINDGOOGLECATSEARCH)
	@Consumes("text/xml")
	@Produces("text/xml;charset=UTF-8")
	String findCatSearchForGoogleData(String xml);
}
