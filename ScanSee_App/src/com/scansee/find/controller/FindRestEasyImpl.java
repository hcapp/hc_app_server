package com.scansee.find.controller;

import static com.scansee.common.util.Utility.formResponseXml;

import org.apache.log4j.Logger;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.servicefactory.ServiceFactory;
import com.scansee.find.service.FindService;
import com.scansee.hotdeals.controller.HotDealsRestEasyImpl;

/**
 * This class implements FindRestEasy methods and call the find service methods.
 * 
 * @author shyamsundara_hm
 */
public class FindRestEasyImpl implements FindRestEasy
{
	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = Logger.getLogger(HotDealsRestEasyImpl.class);

	/**
	 * This is a RestEasy WebService Method to find services given the
	 * coordinates. Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */
	@Override
	public String findService(String xml)
	{
		final String methodName = "findService in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final FindService findService = ServiceFactory.getFindService();
		try
		{
			// <type> Arts & Culture </type>
			xml = xml.replace("<type>", "<type><![CDATA[");
			xml = xml.replace("</type>", "]]></type>");
			response = findService.findService(xml);
			

		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned :" + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching Hot Deals category
	 * information. Method Type:GET
	 * 
	 * @param userId
	 *            as request parameter
	 * @return XML containing success or failure in the response.
	 */
	@Override
	public String getCategory(String xml)
	{
		final String methodName = "getCategory in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Requested xml -->" + xml);
		}
		String response = null;
		final FindService findService = ServiceFactory.getFindService();
		try
		{
			response = findService.getCategoryInfo(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method to find services given the
	 * coordinates. Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */
	@Override
	public String findLocationDetails(String xml)
	{
		final String methodName = "findLocationDetails in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final FindService findService = ServiceFactory.getFindService();
		try
		{
			response = findService.findLocationDetails(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method to find location details given the
	 * keyword and  coordinates. Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the search details in the response.
	 */
	@Override
	public String findRetSearch(String xml)
	{
		final String methodName = "findLocationSearch in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final FindService findService = ServiceFactory.getFindService();
		try {
			// <type> Arts & Culture </type>
			xml = xml.replace("<keyword>", "<keyword><![CDATA[");
			xml = xml.replace("</keyword>", "]]></keyword>");
			response = findService.findRetSearch(xml);
		} catch (ScanSeeException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String findSSCategory(String xml)
	{
		final String methodName = "findService in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final FindService findService = ServiceFactory.getFindService();
		
			// <type> Arts & Culture </type>
			xml = xml.replace("<type>", "<type><![CDATA[");
			xml = xml.replace("</type>", "]]></type>");
			//response = findService.findSSCategory(xml);
			

		
	/*	catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}*/
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned :" + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String findSSRetailerSearch(String xml)
	{

		final String methodName = "findService in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final FindService findService = ServiceFactory.getFindService();
		
			// <type> Arts & Culture </type>
			xml = xml.replace("<keyword>", "<keyword><![CDATA[");
			xml = xml.replace("</keyword>", "]]></keyword>");
		//	response = findService.findSSRetailerSearch(xml);
			

		
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned :" + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String getUserRadius(Integer userId, String moduleName)
	{
		final String methodName = "getUserRadius in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Requested userID -->" + userId +"module name"+moduleName);
		}
		String response = null;
		final FindService findService = ServiceFactory.getFindService();
		try
		{
			response = findService.getUserRadius(userId,moduleName);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is Rest Easy Web service for User Tracking. 
	 * Should be called to insert the Google Retailers to database. 
	 * 
	 * @param xml
	 */
	@Override
	public void userTrackingGoogleRetailers(String xml) {
		final String methodName = "userTrackingGoogleRetailers in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request xml -->" + xml);
		}
		String response = null;
		final FindService findService = ServiceFactory.getFindService();
		try
		{
			response = findService.userTrackingGoogleRetailers(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
	}
	
	/**
	 * This is a RestEasyImpl WebService Method to find location details for Google data given the
	 * keyword and coordinates.
	 * 
	 * @param xml as request.
	 * @return XML containing the Google data search details in the response.
	 */
	
	@Override
	public String findGoogleRetSearch(String inputXml)
	{
		final String methodName = "findGoogleRetSearch";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final FindService findService = ServiceFactory.getFindService();
		try {
			inputXml = inputXml.replace("<keyword>", "<keyword><![CDATA[");
			inputXml = inputXml.replace("</keyword>", "]]></keyword>");
			inputXml = inputXml.replace("<pagetoken>", "<pagetoken><![CDATA[");
			inputXml = inputXml.replace("</pagetoken>", "]]></pagetoken>");
			response = findService.findGoogleRetSearch(inputXml);
		} catch (ScanSeeException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	
	/**
	 * This is a RestEasyImpl WebService Method to find location details for ScanSee data given the
	 * keyword and coordinates.
	 * 
	 * @param xml as request.
	 * @return XML containing the ScanSee data search details in the response.
	 */
	@Override
	public String findScanSeeRetSearch(String inputXml)
	{
		final String methodName = "findScanSeeRetSearch";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final FindService findService = ServiceFactory.getFindService();
		try {
			inputXml = inputXml.replace("<keyword>", "<keyword><![CDATA[");
			inputXml = inputXml.replace("</keyword>", "]]></keyword>");
			response = findService.findScanSeeRetSearch(inputXml);
		} catch (ScanSeeException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	

	/**
	 * This is a Rest Easy Web service for category search for 
	 * retailers from ScanSee given coordinates. 
	 * @param xml
	 * @return xml containing retailer list
	 */
	@Override
	public String findCatSearchForScanSeeData(String xml) {
		final String methodName = "findCatSearchForScanSeeData";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if(LOG.isDebugEnabled())	{
			LOG.debug("Request xml --> " + xml);
		}
		String response = null;
		final FindService findService = ServiceFactory.getFindService();
		try	{
			xml = xml.replace("<type>", "<type><![CDATA[");
			xml = xml.replace("</type>", "]]></type>");
			response = findService.findCatSearchForScanSeeData(xml);
		}
		catch(ScanSeeException exception)	{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if(LOG.isDebugEnabled())	{
			LOG.debug("Response returned " + methodName);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a Rest Easy Web service for category search for 
	 * retailers from Google results given coordinates.
	 * @param xml
	 * @return Retailers List
	 */
	@Override
	public String findCatSearchForGoogleData(String xml) {
		final String methodName = "findCatSearchForSSData";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if(LOG.isDebugEnabled())	{
			LOG.debug("Request xml --> " + xml);
		}
		String response = null;
		final FindService findService = ServiceFactory.getFindService();
		try	{
			xml = xml.replace("<type>", "<type><![CDATA[");
			xml = xml.replace("</type>", "]]></type>");
			xml = xml.replace("<pagetoken>", "<pagetoken><![CDATA[");
			xml = xml.replace("</pagetoken>", "]]></pagetoken>");
			response = findService.findCatSearchForGoogleData(xml);
		}
		catch(ScanSeeException exception)	{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if(LOG.isDebugEnabled())	{
			LOG.debug("Response returned " + methodName);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
}
