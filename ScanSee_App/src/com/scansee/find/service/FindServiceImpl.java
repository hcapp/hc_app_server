package com.scansee.find.service;

import static com.scansee.common.util.Utility.isEmpty;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.externalapi.ExternalAPIManager;
import com.scansee.common.helper.XstreamParserHelper;
import com.scansee.common.pojos.Categories;
import com.scansee.common.pojos.GoogleCategory;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.common.util.Utility;
import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.findplace.FindPlaceService;
import com.scansee.externalapi.findplace.FindPlaceserviceImpl;
import com.scansee.externalapi.google.pojos.ServiceSearchResponse;
import com.scansee.externalapi.google.pojos.ServiceSerachRequest;
import com.scansee.find.dao.FindDAO;
import com.scansee.firstuse.dao.FirstUseDAO;
import com.scansee.firstuse.service.FirstUseServiceImpl;
import com.scansee.shoppinglist.dao.ShoppingListDAO;

/**
 * this class used to implement find service methods and call methods in DAO
 * layer.
 * 
 * @author shyamsundara_hm
 */
public class FindServiceImpl implements FindService
{
	/**
	 * Instance variable for Shopping list DAO instance.
	 */

	private ShoppingListDAO shoppingListDao;
	/**
	 * Instance variable for find DAO instance.
	 */

	private FindDAO findDAO;
	
	/**
	 * Instance variable for firstUse DAO instance.
	 */
	private FirstUseDAO firstUseDao;

	/**
	 * Getting the logger instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(FirstUseServiceImpl.class.getName());

	/**
	 * This methods to get find DAO.
	 * 
	 * @return the findDAO
	 */
	public FindDAO getFindDAO()
	{
		return findDAO;
	}

	/**
	 * This method to set find DAO.
	 * 
	 * @param findDAO
	 *            the findDAO to set
	 */
	public void setFindDAO(FindDAO findDAO)
	{
		this.findDAO = findDAO;
	}

	/**
	 * sets the ShoppingListDAO DAO.
	 * 
	 * @param shoppingListDao
	 *            The instance for ShoppingListDAO
	 */
	public void setShoppingListDao(ShoppingListDAO shoppingListDao)
	{
		this.shoppingListDao = shoppingListDao;
	}
	
	/**
	 * sets the FirstUseDAO
	 * 
	 * @param firstUseDao
	 * 				The instance for FirstUseDAO
	 */
	public void setFirstUseDao(FirstUseDAO firstUseDao) 
	{
		this.firstUseDao = firstUseDao;
	}

	/**
	 * The service method to find retailers given the coordinates.
	 * 
	 * @param xml
	 *            as request
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */
	@Override
	public String findService(String xml) throws ScanSeeException
	{
		final String methodName = "findService";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		double onemileinmetere = 1609.34;
		StringBuffer catType = new StringBuffer();
		String type = null;
		Integer defaultRedius = null;
		//ArrayList<AppConfiguration> stockInfoList = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		ArrayList<ServiceSearchResponse> serviceSearchResponselst = null;
		final ServiceSerachRequest placeSearchRequest = (ServiceSerachRequest) streamHelper.parseXmlToObject(xml);
		ExternalAPIInformation externalAPIInformation = new ExternalAPIInformation();

		if (null == placeSearchRequest.getUserId() || isEmpty(placeSearchRequest.getLat()) || isEmpty(placeSearchRequest.getLng())
				|| isEmpty(placeSearchRequest.getType()))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		} else {
			// Fetching user radius from App configuration table.
			/*
			 * stockInfoList =
			 * shoppingListDao.getAppConfig(ApplicationConstants.
			 * CONFIGURATIONDEFAULTRADIUS); for (int i = 0; i <
			 * stockInfoList.size(); i++) { if
			 * (stockInfoList.get(i).getScreenName
			 * ().equals(ApplicationConstants.DEFAULTRADIUSSCREEN)) { if
			 * (stockInfoList.get(i).getScreenContent() != null) { defaultRedius
			 * = Integer.parseInt(stockInfoList.get(i).getScreenContent());
			 * placeSearchRequest.setRadius(String.valueOf(defaultRedius)); } }
			 * }
			 */
			defaultRedius = findDAO.getUserRadius(Integer.valueOf(placeSearchRequest.getUserId()), ApplicationConstants.FINDMODULENAME);
			externalAPIInformation = getExternalAPIInformation(ApplicationConstants.FINDMODULENAME);
			if (null != externalAPIInformation.getSerchParameters() && null != externalAPIInformation.getVendorList())
			{
				List<GoogleCategory> categorylst = findDAO.getAllCategories(placeSearchRequest.getType());
				if (!categorylst.isEmpty())
				{
					for (GoogleCategory googleCategory : categorylst)
					{
						catType.append(googleCategory.getCatName());
						catType.append("|");
					}
					type = catType.toString();
					type = type.substring(0, type.length() - 1);
					LOG.info("Category type::::::::::::" + type);
				}

				final ServiceSerachRequest searchParams = new ServiceSerachRequest();
				searchParams.setLocation(placeSearchRequest.getLat() + "," + placeSearchRequest.getLng());
				searchParams.setLat(placeSearchRequest.getLat());
				searchParams.setLng(placeSearchRequest.getLng());
				searchParams.setRadius(String.valueOf(defaultRedius * onemileinmetere));
				searchParams.setType(type);
				searchParams.setValues();
				serviceSearchResponselst = getGoogleAPIData(externalAPIInformation, searchParams);
			}
			if (serviceSearchResponselst != null && !serviceSearchResponselst.isEmpty())
			{
				response = XstreamParserHelper.produceXMlFromObject(serviceSearchResponselst);
				response = response.replaceAll("<ServiceSearchResponse>", "");
				response = response.replaceAll("<placeSearchlst>", "");
				response = response.replaceAll("</ServiceSearchResponse>", "");
				response = response.replaceAll("</placeSearchlst>", "");
				response = response.replaceAll("<list>", "");
				response = response.replaceAll("</list>", "");

				response = "<ServiceSearchResponse><placeSearchlst>".concat(response);
				response = response.concat("</placeSearchlst></ServiceSearchResponse>");
			}
			// Integrating with ScanSee data
			RetailersDetails retailersDetailsObj = null;

			retailersDetailsObj = findSSCategory(placeSearchRequest);
			String findRetailerResponse = null;
			if (retailersDetailsObj != null)
			{
				findRetailerResponse = XstreamParserHelper.produceXMlFromObject(retailersDetailsObj);
				findRetailerResponse = findRetailerResponse.replaceAll("<RetailerDetails>", "<scanseeData>");
				findRetailerResponse = findRetailerResponse.replaceAll("</RetailerDetails>", "</scanseeData>");
				if (response != null) {
					response = response.replaceAll("<ServiceSearchResponse>", "<ServiceSearchResponse><googleData><poweredby>Google</poweredby>");
					response = response.replaceAll("</ServiceSearchResponse>", "</googleData>" + findRetailerResponse + "</ServiceSearchResponse>");
				} else {
					findRetailerResponse = findRetailerResponse.replaceAll("<scanseeData>", "<ServiceSearchResponse><scanseeData>");
					findRetailerResponse = findRetailerResponse.replaceAll("</scanseeData>", "</scanseeData></ServiceSearchResponse>");
					response = findRetailerResponse;
				}
			} else {
				if (null != response) {
					response = response.replaceAll("<ServiceSearchResponse>", "<ServiceSearchResponse><googleData><poweredby>Google</poweredby>");
					response = response.replaceAll("</ServiceSearchResponse>", "</googleData></ServiceSearchResponse>");
				} else {
					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
				}
			}
		}
		return response;
	}

	/**
	 * This method is used to get external API information.
	 * 
	 * @param moduleName
	 *            -Requested module name.
	 * @return ExternalAPIInformation
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public ExternalAPIInformation getExternalAPIInformation(String moduleName) throws ScanSeeException
	{
		final String methodName = " getExternalAPIInformation ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ExternalAPIManager externalAPIManager = new ExternalAPIManager(moduleName, shoppingListDao);
		final ExternalAPIInformation externalAPIInformation = externalAPIManager.getExternalAPIInformation();
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return externalAPIInformation;
	}

	/**
	 * This service implementation method for getting Category services. Calls
	 * the XStreams helper class methods for parsing.
	 * 
	 * @param userId
	 *            as request parameter
	 * @return response string with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getCategoryInfo(String xml) throws ScanSeeException
	{
		final String methodName = "getCategoryInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Categories categories = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserTrackingData objTrackingData = (UserTrackingData) streamHelper.parseXmlToObject(xml);
		
		if (null == objTrackingData.getUserID())
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			//For user tracking
			Integer mainMenuID = null;
			if(objTrackingData.getModuleID() != null)	{
				mainMenuID = firstUseDao.userTrackingModuleClick(objTrackingData);
			}
			else	{
				mainMenuID = objTrackingData.getMainMenuID();
			}
			
			final List<GoogleCategory> categorylst = findDAO.getCategoryDetail();
			categories = new Categories();
			categories.setCatList(categorylst);
//			categories.setMainMenuID(mainMenuID);
			
			if (categories != null && !categories.getCatList().isEmpty())
			{
				categories.setMainMenuID(mainMenuID);
				response = XstreamParserHelper.produceXMlFromObject(categories);

//				response = response.replaceAll("<list>", "<CategoryList>");
//				response = response.replaceAll("</list>", "</CategoryList>");

			}
			else
			{
				String strMMId = null;
				if(null != mainMenuID)	{
					strMMId = mainMenuID.toString();
				} else	{
					strMMId = ApplicationConstants.STRING_ZERO;
				}
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND,	ApplicationConstants.NOCATEGORYFOUNDTEXT, "mainMenuID", strMMId);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This service implementation method for getting hot deals Category. Calls
	 * the XStreams helper class methods for parsing.
	 * 
	 * @param xml
	 *            as request parameter
	 * @return response string with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String findLocationDetails(String xml) throws ScanSeeException
	{
		final String methodName = "findService";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ServiceSerachRequest placeSearchRequest = (ServiceSerachRequest) streamHelper.parseXmlToObject(xml);
		ExternalAPIInformation externalAPIInformation = new ExternalAPIInformation();
		if (null == placeSearchRequest.getUserId() || isEmpty(placeSearchRequest.getReference()) || isEmpty(placeSearchRequest.getLanguage()))

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			externalAPIInformation = getExternalAPIInformation(ApplicationConstants.FINDMODULENAME);
			if (null != externalAPIInformation.getSerchParameters() && null != externalAPIInformation.getVendorList())
			{

				final ServiceSerachRequest searchParams = new ServiceSerachRequest();
				searchParams.setReference(placeSearchRequest.getReference());
				searchParams.setLanguage(placeSearchRequest.getLanguage());
				searchParams.setLocationValues();

				FindPlaceService findPlaceService = new FindPlaceserviceImpl();
				response = findPlaceService.processFindLocationRequest(externalAPIInformation, searchParams);

			}
			if (null == response)
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
			else if (response != null)
			{
				if (response.equalsIgnoreCase("<LocationInfo/>"))
				{
					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
				}
			}

		}
		return response;
	}

	/**
	 * The service method for searching retailer by name or keyword to fetch
	 * location details for given the coordinates and keyword.
	 * 
	 * @param xml as request.
	 * @return response based on success or failure.
	 * @throws ScanSeeException for exception.
	 */
	@Override
	public String findRetSearch(String xml) throws ScanSeeException
	{
		final String methodName = "findLocationSearch";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		double onemileinmetere = 1609.34;
		Integer defaultRedius = null;

		//ArrayList<AppConfiguration> stockInfoList = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		ArrayList<ServiceSearchResponse> serviceSearchResponselst = null;
		final ServiceSerachRequest placeSearchRequest = (ServiceSerachRequest) streamHelper.parseXmlToObject(xml);
		ExternalAPIInformation externalAPIInformation = new ExternalAPIInformation();
		if (null == placeSearchRequest.getUserId() || isEmpty(placeSearchRequest.getLat()) || isEmpty(placeSearchRequest.getLng())
				|| isEmpty(placeSearchRequest.getKeyword()))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		} else {
			// Fetching user radius from App configuration table.
			/*
			 * stockInfoList =
			 * shoppingListDao.getAppConfig(ApplicationConstants.
			 * CONFIGURATIONDEFAULTRADIUS); for (int i = 0; i <
			 * stockInfoList.size(); i++) { if
			 * (stockInfoList.get(i).getScreenName
			 * ().equals(ApplicationConstants.DEFAULTRADIUSSCREEN)) { if
			 * (stockInfoList.get(i).getScreenContent() != null) { defaultRedius
			 * = Integer.parseInt(stockInfoList.get(i).getScreenContent());
			 * placeSearchRequest.setRadius(String.valueOf(defaultRedius)); } }
			 * }
			 */

			defaultRedius = findDAO.getUserRadius(Integer.valueOf(placeSearchRequest.getUserId()), ApplicationConstants.FINDMODULENAME);
			externalAPIInformation = getExternalAPIInformation(ApplicationConstants.FINDMODULENAME);
			if (null != externalAPIInformation.getSerchParameters() && null != externalAPIInformation.getVendorList())
			{
				final ServiceSerachRequest searchParams = new ServiceSerachRequest();
				searchParams.setLocation(placeSearchRequest.getLat() + "," + placeSearchRequest.getLng());
				searchParams.setLat(placeSearchRequest.getLat());
				searchParams.setLng(placeSearchRequest.getLng());
				searchParams.setRadius(String.valueOf(defaultRedius * onemileinmetere));
				searchParams.setKeyword(placeSearchRequest.getKeyword());
				searchParams.setValues();
				serviceSearchResponselst = getGoogleAPIData(externalAPIInformation, searchParams);
			}
			if (serviceSearchResponselst != null && !serviceSearchResponselst.isEmpty())
			{
				response = XstreamParserHelper.produceXMlFromObject(serviceSearchResponselst);
				response = response.replaceAll("<ServiceSearchResponse>", "");
				response = response.replaceAll("<placeSearchlst>", "");
				response = response.replaceAll("</ServiceSearchResponse>", "");
				response = response.replaceAll("</placeSearchlst>", "");
				response = response.replaceAll("<list>", "");
				response = response.replaceAll("</list>", "");
				response = "<ServiceSearchResponse><placeSearchlst>".concat(response);
				response = response.concat("</placeSearchlst></ServiceSearchResponse>");
			}
			// Integrating with ScanSee data
			RetailersDetails retailersDetailsObj = null;

			retailersDetailsObj = findDAO.findSSRetailerSearch(placeSearchRequest);
			String findRetailerResponse = null;
			if (retailersDetailsObj != null)
			{
				findRetailerResponse = XstreamParserHelper.produceXMlFromObject(retailersDetailsObj);
				findRetailerResponse = findRetailerResponse.replaceAll("<RetailerDetails>", "<scanseeData>");
				findRetailerResponse = findRetailerResponse.replaceAll("</RetailerDetails>", "</scanseeData>");
				if (response != null) {
					response = response.replaceAll("<ServiceSearchResponse>", "<ServiceSearchResponse><googleData><poweredby>Google</poweredby>");
					response = response.replaceAll("</ServiceSearchResponse>", "</googleData>" + findRetailerResponse + "</ServiceSearchResponse>");
				} else {
					findRetailerResponse = findRetailerResponse.replaceAll("<scanseeData>", "<ServiceSearchResponse><scanseeData>");
					findRetailerResponse = findRetailerResponse.replaceAll("</scanseeData>", "</scanseeData></ServiceSearchResponse>");
					response = findRetailerResponse;
				}
			} else {
				if (null != response)
				{
					response = response.replaceAll("<ServiceSearchResponse>", "<ServiceSearchResponse><googleData><poweredby>Google</poweredby>");
					response = response.replaceAll("</ServiceSearchResponse>", "</googleData></ServiceSearchResponse>");
				} else {
					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
				}
			}
		}
		return response;
	}

	public RetailersDetails findSSCategory(ServiceSerachRequest placeSearchRequest) throws ScanSeeException
	{
		final String methodName = "findService";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailersDetails retailersDetailsObj = null;
		retailersDetailsObj = findDAO.fetchRetailerDetails(placeSearchRequest);
		return retailersDetailsObj;
	}

	public RetailersDetails findSSRetailerSearch(ServiceSerachRequest placeSearchRequest) throws ScanSeeException
	{
		final String methodName = "findService";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailersDetails retailersDetailsObj = null;
		retailersDetailsObj = findDAO.findSSRetailerSearch(placeSearchRequest);
		return retailersDetailsObj;
	}

/*	
 * Below service is not used.
 * Same services is seperated (to fetch ScanSee or Google retailers by location).
 * 1. to fetch ScanSee  retailers by location data (service method findScanSeeRetSearch)
 * 2. to fetch Google retailers by location data (service method findGoogleRetSearch)
 */
/*public ArrayList<ServiceSearchResponse> getGoogleAPIData(ExternalAPIInformation externalAPIInformation, ServiceSerachRequest searchParams)
	{
		ServiceSearchResponse serviceSearchResponseObj = null;
		ArrayList<ServiceSearchResponse> serviceSearchResponselst = null;
		FindPlaceService findPlaceService = new FindPlaceserviceImpl();
		String paginationToken = null;
		serviceSearchResponseObj = findPlaceService.processFindRequest(externalAPIInformation, searchParams);
		if (serviceSearchResponseObj != null && serviceSearchResponseObj.getPlaceSearchlst().size() != 0)
		{
			serviceSearchResponselst = new ArrayList<ServiceSearchResponse>();
			if (serviceSearchResponseObj.getPagetoken() != null)
			{
				paginationToken = serviceSearchResponseObj.getPagetoken();
				serviceSearchResponseObj.setPagetoken(null);
				serviceSearchResponseObj.setPoweredby(null);
				serviceSearchResponselst.add(serviceSearchResponseObj);
				searchParams.setPagetoken(paginationToken);
				searchParams.setValues();
				serviceSearchResponseObj = findPlaceService.processFindRequest(externalAPIInformation, searchParams);
				if (serviceSearchResponseObj != null)
				{
					if (serviceSearchResponseObj.getPagetoken() != null)
					{
						paginationToken = serviceSearchResponseObj.getPagetoken();
						serviceSearchResponseObj.setPagetoken(null);
						serviceSearchResponseObj.setPoweredby(null);
						serviceSearchResponselst.add(serviceSearchResponseObj);
						searchParams.setPagetoken(paginationToken);
						searchParams.setValues();
						serviceSearchResponseObj = findPlaceService.processFindRequest(externalAPIInformation, searchParams);
						if (serviceSearchResponseObj != null) {
							serviceSearchResponseObj.setPagetoken(null);
							serviceSearchResponseObj.setPoweredby(null);
							serviceSearchResponselst.add(serviceSearchResponseObj);
						}
					} else {
						serviceSearchResponseObj.setPoweredby(null);
						serviceSearchResponselst.add(serviceSearchResponseObj);
					}
				}
			} else {
				serviceSearchResponseObj.setPoweredby(null);
				serviceSearchResponselst.add(serviceSearchResponseObj);
			}
		}
		return serviceSearchResponselst;
	}
*/	
	
	public ArrayList<ServiceSearchResponse> getGoogleAPIData(ExternalAPIInformation externalAPIInformation, ServiceSerachRequest searchParams)
	{
		ServiceSearchResponse serviceSearchResponseObj = null;
		ArrayList<ServiceSearchResponse> serviceSearchResponselst = new ArrayList<ServiceSearchResponse>();
		FindPlaceService findPlaceService = new FindPlaceserviceImpl();
		serviceSearchResponseObj = findPlaceService.processFindRequest(externalAPIInformation, searchParams);
		if (serviceSearchResponseObj != null && serviceSearchResponseObj.getPlaceSearchlst().size() != 0)
		{
			serviceSearchResponseObj.setPoweredby(ApplicationConstants.POWEREDBY);
			serviceSearchResponselst.add(serviceSearchResponseObj);
			String pageTokenToCheck = null;
			int pageTokenLenth = 0;
			
			if(serviceSearchResponseObj != null)	{
				pageTokenToCheck = serviceSearchResponseObj.getPagetoken();
			}
			if(pageTokenToCheck != null)	{
				pageTokenLenth = pageTokenToCheck.length();
			}
			
			if(serviceSearchResponseObj != null && pageTokenLenth > 1000)	{
				if(!serviceSearchResponselst.isEmpty() && serviceSearchResponselst != null)	{
					serviceSearchResponselst.get(0).setPagetoken(null);
				}
			}
			else if(serviceSearchResponseObj != null && pageTokenLenth > 300)	{
				searchParams.setPagetoken(pageTokenToCheck);
				searchParams.setValues();
				ServiceSearchResponse serviceSearchResponseObj1 = findPlaceService.processFindRequest(externalAPIInformation, searchParams);
				if(serviceSearchResponseObj1 == null)	{
					if(!serviceSearchResponselst.isEmpty() && serviceSearchResponselst != null)	{
						serviceSearchResponselst.get(0).setPagetoken(null);
					}
				}
			}
			searchParams.setValues();			
		}
		return serviceSearchResponselst;
	}

	/**
	 * The service method to fetch user configured radius from database.
	 * 
	 * @param userId
	 *            , module name as request parameter
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */
	@Override
	public String getUserRadius(Integer userId, String moduleName) throws ScanSeeException
	{
		final String methodName = "getUserRadius";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Integer usrRadius = null;
		if (null == userId)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			usrRadius = findDAO.getUserRadius(userId, moduleName);
			if (usrRadius != null)
			{

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOCATEGORYFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String userTrackingGoogleRetailers(String xml) throws ScanSeeException {
		final String methodName = "userTrackingGoogleRetailers";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final RetailerDetail objRetailerDetail = (RetailerDetail) streamHelper.parseXmlToObject(xml);
		if (objRetailerDetail.getMainMenuID() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			response = findDAO.userTrackingGoogleRetailers(objRetailerDetail);
			
			if(response.equalsIgnoreCase(ApplicationConstants.SUCCESSRESPONSETEXT))	{
				response = ApplicationConstants.SUCCESSRESPONSETEXT;
			}
			else	{
				response = ApplicationConstants.FAILURE;
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method for searching retailer by name or keyword to fetch
	 * location details for given the coordinates and keyword.
	 * 
	 * @param strInputXml as request.
	 * @return response based on success or failure.
	 * @throws ScanSeeException for exception.
	 */
	@Override
	public String findGoogleRetSearch(String strInputXml) throws ScanSeeException
	{
		final String methodName = "findGoogleRetSearch";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		double oneMileInMeter = 1609.34;
		Integer idefaultRadius = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		ArrayList<ServiceSearchResponse> arServiceSearchResplist = null;
		final ServiceSerachRequest objServiceSearchReq = (ServiceSerachRequest) streamHelper.parseXmlToObject(strInputXml);
		ExternalAPIInformation externalAPIInformation = new ExternalAPIInformation();
		if (null == objServiceSearchReq.getUserId() || isEmpty(objServiceSearchReq.getLat()) || isEmpty(objServiceSearchReq.getLng())
				|| isEmpty(objServiceSearchReq.getKeyword()))
		{
			strResponse = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		} else {
			idefaultRadius = findDAO.getUserRadius(Integer.valueOf(objServiceSearchReq.getUserId()), ApplicationConstants.FINDMODULENAME);
			externalAPIInformation = getExternalAPIInformation(ApplicationConstants.FINDMODULENAME);
			if (null != externalAPIInformation.getSerchParameters() && null != externalAPIInformation.getVendorList())
			{
				final ServiceSerachRequest objSearchParam = new ServiceSerachRequest();
				objSearchParam.setLocation(objServiceSearchReq.getLat() + "," + objServiceSearchReq.getLng());
				objSearchParam.setLat(objServiceSearchReq.getLat());
				objSearchParam.setLng(objServiceSearchReq.getLng());
				objSearchParam.setRadius(String.valueOf(idefaultRadius * oneMileInMeter));
				objSearchParam.setKeyword(objServiceSearchReq.getKeyword());
				objSearchParam.setPagetoken(objServiceSearchReq.getPagetoken());
				objSearchParam.setValues();
				arServiceSearchResplist = getGoogleAPIData(externalAPIInformation, objSearchParam);
			}
			if (arServiceSearchResplist != null && !arServiceSearchResplist.isEmpty()) {
				strResponse = XstreamParserHelper.produceXMlFromObject(arServiceSearchResplist);
				/*strResponse = strResponse.replaceAll("<ServiceSearchResponse>", "");
				strResponse = strResponse.replaceAll("<placeSearchlst>", "");
				strResponse = strResponse.replaceAll("</ServiceSearchResponse>", "");
				strResponse = strResponse.replaceAll("</placeSearchlst>", "");*/
				strResponse = strResponse.replaceAll("<list>", "");
				strResponse = strResponse.replaceAll("</list>", "");
				/*strResponse = "<ServiceSearchResponse><googleData><poweredby>Google</poweredby><placeSearchlst>".concat(strResponse);
				strResponse = strResponse.concat("</placeSearchlst></googleData></ServiceSearchResponse>");*/
			} else {
				strResponse = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		return strResponse;
	}
	
	
	/**
	 * The service method for searching retailer by name or keyword to fetch
	 * location details for given the coordinates and keyword.
	 * 
	 * @param strInputXml as request.
	 * @return response based on success or failure.
	 * @throws ScanSeeException for exception.
	 */
	@Override
	public String findScanSeeRetSearch(String strInputXml) throws ScanSeeException
	{
		final String methodName = "findScanSeeRetSearch";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		RetailersDetails objRetDetails = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ServiceSerachRequest objServiceSearchReq = (ServiceSerachRequest) streamHelper.parseXmlToObject(strInputXml);
		if (null == objServiceSearchReq.getUserId() || isEmpty(objServiceSearchReq.getLat()) || isEmpty(objServiceSearchReq.getLng())
				|| isEmpty(objServiceSearchReq.getKeyword()))
		{
			strResponse = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		} else {
			objRetDetails = findDAO.findSSRetailerSearch(objServiceSearchReq);
			if (objRetDetails != null)
			{
				strResponse = XstreamParserHelper.produceXMlFromObject(objRetDetails);
				/*strResponse = strResponse.replaceAll("<RetailerDetails>", "<scanseeData>");
				strResponse = strResponse.replaceAll("</RetailerDetails>", "</scanseeData>");
				strResponse = strResponse.replaceAll("<scanseeData>", "<ServiceSearchResponse>");
				strResponse = strResponse.replaceAll("</scanseeData>", "</ServiceSearchResponse>");*/
			} else {
				strResponse = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		return strResponse;
	}
	
	
	/**
	 * The service method to find retailers from ScanSee database given the coordinates. 
	 * 
	 * @param xml
	 *            as request
	 * @return xml containing retailer list
	 * @throws ScanSeeException for exception
	 */
	@Override
	public String findCatSearchForScanSeeData(String xml) throws ScanSeeException {
		final String methodName = "findCatSearchForScanSeeData";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ServiceSerachRequest placeSearchRequest = (ServiceSerachRequest) streamHelper.parseXmlToObject(xml);

		if (null == placeSearchRequest.getUserId() || isEmpty(placeSearchRequest.getLat()) || isEmpty(placeSearchRequest.getLng())
				|| isEmpty(placeSearchRequest.getType()))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else	{
			RetailersDetails retailersDetailsObj = null;
			
			retailersDetailsObj = findDAO.fetchRetailerDetails(placeSearchRequest);
			if(retailersDetailsObj != null)	{
				response = XstreamParserHelper.produceXMlFromObject(retailersDetailsObj);
			}
			else	{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method to find retailers from Google APIs given the coordinates.
	 * 
	 * @param xml
	 *            as request
	 * @return xml containing retailer list
	 * @throws ScanSeeException for exception
	 */
	@Override
	public String findCatSearchForGoogleData(String xml) throws ScanSeeException {
		final String methodName = "findService";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		double onemileinmetere = 1609.34;
		StringBuffer catType = new StringBuffer();
		String type = null;
		Integer defaultRedius = null;
		//ArrayList<AppConfiguration> stockInfoList = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		ArrayList<ServiceSearchResponse> serviceSearchResponselst = null;
		final ServiceSerachRequest placeSearchRequest = (ServiceSerachRequest) streamHelper.parseXmlToObject(xml);
		ExternalAPIInformation externalAPIInformation = new ExternalAPIInformation();

		if (null == placeSearchRequest.getUserId() || isEmpty(placeSearchRequest.getLat()) || isEmpty(placeSearchRequest.getLng()) || isEmpty(placeSearchRequest.getType())) {
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else {
			defaultRedius = findDAO.getUserRadius(Integer.valueOf(placeSearchRequest.getUserId()), ApplicationConstants.FINDMODULENAME);
			externalAPIInformation = getExternalAPIInformation(ApplicationConstants.FINDMODULENAME);
			if (null != externalAPIInformation.getSerchParameters() && null != externalAPIInformation.getVendorList()) {
				List<GoogleCategory> categorylst = findDAO.getAllCategories(placeSearchRequest.getType());

				if (!categorylst.isEmpty()) {
					for (GoogleCategory googleCategory : categorylst) {
						catType.append(googleCategory.getCatName());
						catType.append("|");
					}
					type = catType.toString();
					type = type.substring(0, type.length() - 1);
					LOG.info("Category type::::::::::::" + type);
				}

				final ServiceSerachRequest searchParams = new ServiceSerachRequest();
				searchParams.setLocation(placeSearchRequest.getLat() + "," + placeSearchRequest.getLng());
				searchParams.setLat(placeSearchRequest.getLat());
				searchParams.setLng(placeSearchRequest.getLng());
				searchParams.setRadius(String.valueOf(defaultRedius	* onemileinmetere));
				searchParams.setType(type);		
				searchParams.setPagetoken(placeSearchRequest.getPagetoken());
				searchParams.setValues();
				serviceSearchResponselst = getGoogleAPIData(externalAPIInformation,	searchParams);
			}
			if (serviceSearchResponselst != null && !serviceSearchResponselst.isEmpty()) {
				response = XstreamParserHelper.produceXMlFromObject(serviceSearchResponselst);
//				response = response.replaceAll("<ServiceSearchResponse>", "");
//				response = response.replaceAll("<placeSearchlst>", "");
//				response = response.replaceAll("</ServiceSearchResponse>", "");
//				response = response.replaceAll("</placeSearchlst>", "");
				response = response.replaceAll("<list>", "");
				response = response.replaceAll("</list>", "");
//				response = response.replaceAll("<placeSearchlst/>", "");
//				response = "<ServiceSearchResponse><placeSearchlst>".concat(response);
//				response = response.concat("</placeSearchlst></ServiceSearchResponse>");
			} else {
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}
