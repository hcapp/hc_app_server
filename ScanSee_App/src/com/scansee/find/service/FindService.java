package com.scansee.find.service;

import com.scansee.common.exception.ScanSeeException;

/**
 * This interface for calling methods in DAO layer.
 * 
 * @author shyamsundara_hm
 */
public interface FindService
{

	/**
	 * The service method to find retailers given the coordinates.
	 * 
	 * @param xml
	 *            as request
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */
	String findService(String xml) throws ScanSeeException;

	/**
	 * The service method For displaying Categories. No query parameter
	 * 
	 * @param userId
	 *            as request parameter
	 * @return String based success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */

	String getCategoryInfo(String xml) throws ScanSeeException;

	/**
	 * The service method to find retailers given the coordinates.
	 * 
	 * @param xml
	 *            as request
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */
	String findLocationDetails(String xml) throws ScanSeeException;
	
	/**
	 * The service method to find location details for  given the coordinates and keyword.
	 * 
	 * @param xml as request.
	 * @return xml based on success or failure
	 * @throws ScanSeeException for exception.
	 */
	String findRetSearch(String xml) throws ScanSeeException;
	/**
	 * The service method to fetch user configured radius from database.
	 * 
	 * @param userId, module name
	 *            as request parameter
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */
	 String getUserRadius(Integer userId, String moduleName)throws ScanSeeException;
	// String findSSCategory(String xml) throws ScanSeeException;	
	 
	 /**
		 * The service method to find location details for  given the coordinates and keyword.
		 * 
		 * @param xml
		 *            as request
		 * @return xml based on success or failure
		 * @throws ScanSeeException
		 *             for exception
		 */
		
	// String findSSRetailerSearch(String xml) throws ScanSeeException;	
	 
	 /**
	  * This is service for User Tracking for Google retaailers
	  * 
	  * 
	  * @param xml
	  * @return
	  * @throws ScanSeeException
	  */
	 	String userTrackingGoogleRetailers(String xml) throws ScanSeeException;

		/**
		 * The service method to find location details for Google data given the keyword and coordinates.
		 * 
		 * @param xml as request.
		 * @return xml based on success or failure
		 * @throws ScanSeeException for exception.
		 */
		String findGoogleRetSearch(String xml) throws ScanSeeException;
		
		/**
		 * The service method to find location details for ScanSee data given the keyword and coordinates.
		 * 
		 * @param xml as request.
		 * @return xml based on success or failure
		 * @throws ScanSeeException for exception.
		 */
		String findScanSeeRetSearch(String xml) throws ScanSeeException;
		
		/**
		 * The service method to find retailers from ScanSee database given the coordinates.
		 * 
		 * @param xml
		 *            as request
		 * @return xml containing retailer list
		 * @throws ScanSeeException for exception
		 */
		String findCatSearchForScanSeeData(String xml) throws ScanSeeException;

		/**
		 * The service method to find retailers from Google APIs given the coordinates.
		 * 
		 * @param xml
		 *            as request
		 * @return xml containing retailer list
		 * @throws ScanSeeException for exception
		 */
		String findCatSearchForGoogleData(String xml) throws ScanSeeException;

}
