package com.scansee.find.dao;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.GoogleCategory;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.externalapi.google.pojos.ServiceSerachRequest;
import com.scansee.find.query.FindQueries;
import com.scansee.hotdeals.dao.HotDealsDAOImpl;

/**
 * This methods implements find DAO methods and communicate with Database.
 * 
 * @author shyamsundara_hm
 */
public class FindDAOImpl implements FindDAO
{

	/**
	 * for getting logger...
	 */

	private static final Logger log = Logger.getLogger(HotDealsDAOImpl.class.getName());

	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To get the datasource from xml..
	 * 
	 * @param dataSource
	 *            for setDataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * This DAO Implementation method for getting hot deals category list . No
	 * query parameter.
	 * 
	 * @return response based on success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public List<GoogleCategory> getCategoryDetail() throws ScanSeeException
	{
		final String methodName = "getCategoryDetail in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<GoogleCategory> categorylst = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FindCategoryDisplay");
			simpleJdbcCall.returningResultSet("Categorylist", new BeanPropertyRowMapper<GoogleCategory>(GoogleCategory.class));

			final SqlParameterSource fetchCategoryParameters = new MapSqlParameterSource();

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCategoryParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					categorylst = (List<GoogleCategory>) resultFromProcedure.get("Categorylist");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_FindCategoryDisplay Store Procedure error number: {} " + errorNum + " and error message: {}"
							+ errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getCategoryDetail", exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return categorylst;
	}

	/**
	 * This DAO Implementation method for getting user default radius .
	 * 
	 * @param userId
	 *            as request parameter.
	 * @return response based on success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public Integer getUserDefaultRadius(Integer userId) throws ScanSeeException
	{
		final String methodName = "fetchUserDetailsforForgotPassword";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer radius = null;
		try
		{
			radius = this.jdbcTemplate.queryForObject(FindQueries.FETCHUSERPREFEREDRADIUS, new Object[] { userId }, Integer.class);

		}
		catch (EmptyResultDataAccessException exception)
		{
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return radius;
	}

	/**
	 * This DAO Implementation method for getting find service categories.
	 * 
	 * @param mainCat
	 *            as request parameter.
	 * @return response based on success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<GoogleCategory> getAllCategories(String mainCat) throws ScanSeeException
	{
		final String methodName = "getCategoryDetail in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<GoogleCategory> categorylst = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FindCategoryForAPI");
			simpleJdbcCall.returningResultSet("Categorylist", new BeanPropertyRowMapper<GoogleCategory>(GoogleCategory.class));

			final SqlParameterSource fetchCategoryParameters = new MapSqlParameterSource().addValue("ApiPartnerName", "Google").addValue(
					"CategoryName", mainCat);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCategoryParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					categorylst = (List<GoogleCategory>) resultFromProcedure.get("Categorylist");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_FindCategoryDisplay Store Procedure error number: {} " + errorNum + " and error message: {}"
							+ errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getCategoryDetail", exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return categorylst;
	}

	/**
	 * This Rest Easy method for fetching retailer store details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id and page id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	@Override
	public RetailersDetails fetchRetailerDetails(ServiceSerachRequest serviceSerachRequest) throws ScanSeeException
 {
		final String methodName = "fetchRetailerStoreDetails";
		if (log.isDebugEnabled()) {
			log.debug(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + serviceSerachRequest.getUserId());
		}
		if (serviceSerachRequest.getLastVisitedNo() == null) {
			serviceSerachRequest.setLastVisitedNo(0);
		}
		// String responseFromProc = null;
		// Integer fromProc = null;
		RetailersDetails retailersDetailsObj = null;
		List<RetailerDetail> retailStoreslst = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FindRetailerListPagination");
			simpleJdbcCall.returningResultSet("retailerStoreDetails", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource(); 
			scanQueryParams.addValue("ApiPartnerName", ApplicationConstants.FINDAPIPATNERNAME);
			scanQueryParams.addValue(ApplicationConstants.USERID, serviceSerachRequest.getUserId());
			scanQueryParams.addValue("CategoryName", serviceSerachRequest.getType());
			scanQueryParams.addValue("Latitude", serviceSerachRequest.getLat());
			scanQueryParams.addValue("Longitude", serviceSerachRequest.getLng());
			scanQueryParams.addValue("Radius", null);
			scanQueryParams.addValue("LowerLimit", serviceSerachRequest.getLastVisitedNo());
			scanQueryParams.addValue("ScreenName", ApplicationConstants.FINDSCREENNAME);
			// For user tracking
			scanQueryParams.addValue(ApplicationConstants.MAINMENUID, serviceSerachRequest.getMainMenuID());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			retailStoreslst = (List<RetailerDetail>) resultFromProcedure.get("retailerStoreDetails");
			Integer maxCnt = (Integer) resultFromProcedure.get("MaxCnt");
			boolean nxtPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
			Integer nextPage = 0;
			if (nxtPage == true) {
				nextPage = 1;
			}

			if (null == resultFromProcedure.get("ErrorNumber")) {
				if (null != retailStoreslst && !retailStoreslst.isEmpty()) {
					retailersDetailsObj = new RetailersDetails();
					retailersDetailsObj.setPoweredby(ApplicationConstants.FINDAPIPATNERNAME);
					retailersDetailsObj.setRetailerDetail(retailStoreslst);
					retailersDetailsObj.setMaxCnt(maxCnt);
					retailersDetailsObj.setNextPage(nextPage);

					log.info("Products found for the search");
				}

			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED	+ "usp_FindRetailerListPagination ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
			}
		} catch (DataAccessException exception) {
			log.error("Exception occurred in fetchRetailerStoreDetails", exception);
			throw new ScanSeeException(exception);
		}
		return retailersDetailsObj;
	}

	/**
	 * This Rest Easy method for fetching search retailer details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id and page id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	@Override
	public RetailersDetails findSSRetailerSearch(ServiceSerachRequest serviceSerachRequest) throws ScanSeeException
	{
		final String methodName = "findSSRetailerSearch";
		log.info(ApplicationConstants.METHODSTART + methodName);
		RetailersDetails objRetDetails = null;
		List<RetailerDetail> retailStoreslst = null;
		Integer findRetSeaID = null;
		Integer iMaxCount = 0;
		boolean bNxtPageFlag = false;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FindRetailerSearchPagination");

			simpleJdbcCall.returningResultSet("searchRetailerDetails", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource objRetSearchParam = new MapSqlParameterSource();
			objRetSearchParam.addValue(ApplicationConstants.USERID, serviceSerachRequest.getUserId());
			objRetSearchParam.addValue("SearchKey", serviceSerachRequest.getKeyword());
			objRetSearchParam.addValue("Latitude", serviceSerachRequest.getLat());
			objRetSearchParam.addValue("Longitude", serviceSerachRequest.getLng());
			objRetSearchParam.addValue("Radius", null);
			if (serviceSerachRequest.getLastVisitedNo() == null) {
				serviceSerachRequest.setLastVisitedNo(0);
			}
			objRetSearchParam.addValue("LowerLimit", serviceSerachRequest.getLastVisitedNo());
			objRetSearchParam.addValue("ScreenName", ApplicationConstants.FINDSCREENNAME);	
			//for user tracking
			objRetSearchParam.addValue(ApplicationConstants.MAINMENUID, serviceSerachRequest.getMainMenuID());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objRetSearchParam);
			retailStoreslst = (List<RetailerDetail>) resultFromProcedure.get("searchRetailerDetails");
			findRetSeaID = (Integer) resultFromProcedure.get("FindRetailerSearchID");
			iMaxCount = (Integer) resultFromProcedure.get("MaxCnt");
			bNxtPageFlag = (Boolean) resultFromProcedure.get("NxtPageFlag");
			if (null == resultFromProcedure.get("ErrorNumber"))
			{
				if (null != retailStoreslst && !retailStoreslst.isEmpty())
				{
					objRetDetails = new RetailersDetails();
					objRetDetails.setPoweredby(ApplicationConstants.FINDAPIPATNERNAME);
					objRetDetails.setRetailerDetail(retailStoreslst);
					objRetDetails.setFindRetSeaID(findRetSeaID);
					objRetDetails.setMaxCnt(iMaxCount);
					if (bNxtPageFlag == false) {
						objRetDetails.setNextPage(0);
					} else {
						objRetDetails.setNextPage(1);
					}
				}
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_FindRetailerSearchPagination ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
			}
		} catch (DataAccessException exception) {
			log.error("Exception occurred in findSSRetailerSearch", exception);
			throw new ScanSeeException(exception);
		}
		return objRetDetails;
	}

	/**
	 * The service method to fetch user configured radius from database.
	 * 
	 * @param userId
	 *            , module name as request parameter
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */
	@Override
	public Integer getUserRadius(Integer userId, String moduleName) throws ScanSeeException
	{
		final String methodName = "getUserRadius in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer usrRadius = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetUserModuleRadius");

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", userId);
			scanQueryParams.addValue("ModuleName", moduleName);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					usrRadius = (Integer) resultFromProcedure.get("Radius");

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_GetUserModuleRadius Store Procedure error number: {} " + errorNum + " and error message: {}"
							+ errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getUserRadius", exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return usrRadius;
	}

	/**
	 * This is service for User Tracking. 
	 * To add user clicked google retailer to database
	 * 
	 * @param RetailerDetail object 
	 * @return success or failure response
	 * @throws ScanSeeException
	 */
	@Override
	public String userTrackingGoogleRetailers(RetailerDetail objRetailerDetail) throws ScanSeeException {
		final String methodName = "userTrackingProdSmartSearch in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserTrackingGoogleRetailers");

			final MapSqlParameterSource googleRetData = new MapSqlParameterSource();

			googleRetData.addValue(ApplicationConstants.MAINMENUID, objRetailerDetail.getMainMenuID());
			googleRetData.addValue(ApplicationConstants.FINDCATEGORYID, objRetailerDetail.getFindCatID());
			googleRetData.addValue("RetailName", objRetailerDetail.getRetailerName());
			googleRetData.addValue("Address", objRetailerDetail.getCompleteAddress());
			googleRetData.addValue("Distance", objRetailerDetail.getDistance());
			googleRetData.addValue(ApplicationConstants.FINDRETAILERSEARCHID, objRetailerDetail.getFindRetSeaID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(googleRetData);
			
			if(null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))	{
				response = ApplicationConstants.SUCCESSRESPONSETEXT;
			}
			else	{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_UserTrackingGoogleRetailers Store Procedure error number: {} " + errorNum + " and error message: {}"
						+ errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());
		}
		
		log.info(ApplicationConstants.METHODEND + methodName);
		
		return response;
	}

	
}
