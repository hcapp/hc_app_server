package com.scansee.find.dao;

import java.util.List;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.GoogleCategory;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.externalapi.google.pojos.ServiceSerachRequest;

/**
 * This interface for defining find service method and calls method in Find DAO
 * layer.
 * 
 * @author shyamsundara_hm
 */
public interface FindDAO
{

	/**
	 * The DAO method fetches Category Details. no Query parameter
	 * 
	 * @return HotDealsCategoryInfo.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	List<GoogleCategory> getCategoryDetail() throws ScanSeeException;

	/**
	 * The DAO method fetches Category Details. no Query parameter
	 * 
	 * @param userId
	 *            as request parameter
	 * @return HotDealsCategoryInfo.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	Integer getUserDefaultRadius(Integer userId) throws ScanSeeException;

	/**
	 * The DAO method fetches Category Details. no Query parameter
	 * 
	 * @param mainCat
	 *            as request parameter.
	 * @return HotDealsCategoryInfo.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<GoogleCategory> getAllCategories(String mainCat) throws ScanSeeException;

	/**
	 * The DAO method fetches retailer details. no Query parameter
	 * 
	 * @param mainCat
	 *            as request parameter.
	 * @return HotDealsCategoryInfo.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	RetailersDetails fetchRetailerDetails(ServiceSerachRequest serviceSerachRequest) throws ScanSeeException;
	
	/**
	 * The DAO method fetches retailer details. no Query parameter
	 * 
	 * @param mainCat
	 *            as request parameter.
	 * @return HotDealsCategoryInfo.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	RetailersDetails findSSRetailerSearch(ServiceSerachRequest serviceSerachRequest) throws ScanSeeException;
	/**
	 * The service method to fetch user configured radius from database.
	 * 
	 * @param userId, module name
	 *            as request parameter
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */
	Integer getUserRadius(Integer userId, String moduleName) throws ScanSeeException;
	
	/**
	 * This is service for User Tracking. 
	 * To add user clicked google retailer to database
	 * 
	 * @param RetailerDetail object 
	 * @return success or failure response
	 * @throws ScanSeeException
	 */
	String userTrackingGoogleRetailers(RetailerDetail objRetailerDetail) throws ScanSeeException;
}
