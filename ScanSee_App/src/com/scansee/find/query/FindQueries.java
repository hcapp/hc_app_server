package com.scansee.find.query;
/**
 * For Find module queries.
 * 
 * @author shyamsundara_hm
 */
public class FindQueries
{
	
	/**
	 * For fetching user preferred radius.
	 */
	public static final String FETCHUSERPREFEREDRADIUS = "select LocaleRadius from UserPreference where UserID=?";
}
