package com.scansee.common.helper;

import java.util.List;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.CountryCode;
import com.scansee.common.pojos.EducationalLevelInfo;
import com.scansee.common.pojos.IncomeRange;
import com.scansee.common.pojos.MaritalStatusInfo;
import com.scansee.common.pojos.PaymentInterval;
import com.scansee.common.pojos.PaymentType;
import com.scansee.common.pojos.PayoutType;
import com.scansee.common.pojos.ProductDetail;

/**
 * The Interface for BaseDAO. ImplementedBy {@link BaseDAOImpl}
 * 
 * @author team
 */
public interface BaseDAO
{
	/**
	 * The method for fetching product details.
	 * 
	 * @param productId
	 *            request parameter
	 * @param userId
	 *            request parameter
	 * @param retailId
	 *            request parameter
	 * @return The ProductDetail Information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetail getProductDetail(int userId, int productId, int retailId, Integer saleListID, Integer prodListID, Integer mainMenuID, Integer scanID) throws ScanSeeException;

	/**
	 * The method for fetching Income ranges.
	 * 
	 * @return The IncomeRange as List type.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<IncomeRange> getIncomeRanges() throws ScanSeeException;

	/**
	 * The method for fetching country codes .
	 * 
	 * @return The CountryCode as List type.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<CountryCode> getCountryCodes() throws ScanSeeException;

	/**
	 * The method for fetching payment interval details.
	 * 
	 * @return The PaymentInterval as List type.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<PaymentInterval> getPaymentIntervalDetails() throws ScanSeeException;

	/**
	 * The method for fetching payment type details.
	 * 
	 * @return The PaymentType as List type.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<PaymentType> getPaymentTypeDetails() throws ScanSeeException;

	/**
	 * The method for fetching pay out type details.
	 * 
	 * @return The PayoutType as List type.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<PayoutType> getPayoutDetails() throws ScanSeeException;
    
	/**
	 * The method for getting education level informations.
	 * @return EducationalLevelInfo as List type
	 * @throws ScanSeeException
	 *         The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<EducationalLevelInfo> getEducationalLevel() throws ScanSeeException;
    
	/**
	 * The method for getting marital status information.
	 * @return MaritalStatusInfo as list type
	 * @throws ScanSeeException
	 *         The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<MaritalStatusInfo> getMaritalStatusInfo() throws ScanSeeException;
	
	/**
	 * This method updates the Pushnotification flag of the product to 0
	 * @param productId
	 *         -As int parameter
	 * @param userId
	 *         -As integer parameter
	 * @return String 
	 * @throws ScanSeeException
	 *            The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String upDatePushNotFlag(int productId,int userId) throws ScanSeeException;
}
