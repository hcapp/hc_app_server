package com.scansee.common.helper;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.CountryCode;
import com.scansee.common.pojos.EducationalLevelInfo;
import com.scansee.common.pojos.IncomeRange;
import com.scansee.common.pojos.MaritalStatusInfo;
import com.scansee.common.pojos.PaymentInterval;
import com.scansee.common.pojos.PaymentType;
import com.scansee.common.pojos.PayoutType;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.managesettings.query.ManageSettingQueries;

/**
 * The class for BaseDaoImpl. Implementing {@link BaseDAO} methods
 * 
 * @author team
 */
public class  BaseDaoImpl implements BaseDAO
{

	/**
	 * Getting the Logger Instance.
	 */

	private static Logger log = LoggerFactory.getLogger(BaseDaoImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;

	/**
	 * To set ParameterizedBeanPropertyRowMapper to map POJOs.
	 */
	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */

	/**
	 * To call stored procedue.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To get the datasource from xml..
	 * 
	 * @param dataSource
	 *            of the connected db server.
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.setResultsMapCaseInsensitive(true);
	}
      
	/**
	 * The method for fetching product details.
	 * 
	 * @param productId
	 *            request parameter
	 * @param userId
	 *            request parameter
	 * @param retailId
	 *            request parameter
	 * @return The ProductDetail as Product details.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public ProductDetail getProductDetail(int userId, int productId, int retailId, Integer saleListID, Integer prodListID, Integer mainMenuID, Integer scanID) throws ScanSeeException
	{
		final String methodName = "getProductDetail";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail productDetail = null;
		String retailerId = null;
		try
		{
			if (retailId != 0)
			{
				retailerId = String.valueOf(retailId);
			}
			log.info("fetching Product information for ID {}", productId);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ProductDetails");
			simpleJdbcCall.returningResultSet("shoppingProductDetail", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
			
			final MapSqlParameterSource shoppingProductParameters = new MapSqlParameterSource();
			shoppingProductParameters.addValue("ProductID", productId);
			shoppingProductParameters.addValue("UserID", userId);
			shoppingProductParameters.addValue("ScanLongitude", null);
			shoppingProductParameters.addValue("ScanLatitude", null);
			shoppingProductParameters.addValue("RetailID", retailerId);
			//For user tracking.
			shoppingProductParameters.addValue("SaleListID", saleListID);
			shoppingProductParameters.addValue(ApplicationConstants.PRODUCTLISTID, prodListID);
			shoppingProductParameters.addValue(ApplicationConstants.MAINMENUID, mainMenuID);
			shoppingProductParameters.addValue(ApplicationConstants.SCANTYPEID, scanID);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(shoppingProductParameters);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					final List<ProductDetail> productDetaillst = (List<ProductDetail>) resultFromProcedure.get("shoppingProductDetail");
					if (null != productDetaillst && !productDetaillst.isEmpty())
					{
						productDetail = productDetaillst.get(0);
					}
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
					final Integer errorNum = (Integer) resultFromProcedure.get("ErrorNumber");
					log.error("Error occurred in getProductDetail Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getProductDetail", exception);
			return null;
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetail;
	}

	/**
	 * The method for fetching income ranges.
	 * 
	 * @return The IncomeRange as List.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public List<IncomeRange> getIncomeRanges() throws ScanSeeException
	{
		final String methodName = "getIncomeRanges";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<IncomeRange> incomeRanges = null;
		try
		{
			// simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			incomeRanges = this.jdbcTemplate.query(ManageSettingQueries.FETCHINCOMERANGESQUERY, ParameterizedBeanPropertyRowMapper
					.newInstance(IncomeRange.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return incomeRanges;
	}

	/**
	 * The method for fetching payment interval details.
	 * 
	 * @return The PaymentInterval as List type.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public List<PaymentInterval> getPaymentIntervalDetails() throws ScanSeeException
	{
		log.error("Inside getPaymentIntervalDetails method");
		List<PaymentInterval> paymentInterval = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			paymentInterval = simpleJdbcTemplate.query(ManageSettingQueries.GETPAYMENTINTERVALDETAILSQUERY, new BeanPropertyRowMapper<PaymentInterval>(PaymentInterval.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error("Exception occurred in getPaymentIntervalDetails", exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getPaymentIntervalDetails", exception);
			throw new ScanSeeException(exception);
		}

		return paymentInterval;
	}

	/**
	 * The method for fetching country codes .
	 * 
	 * @return The CountryCode as List type.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public List<CountryCode> getCountryCodes() throws ScanSeeException
	{
		final String methodName = "getCountryCodes";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CountryCode> countryCodes = null;
		try
		{
			// simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			countryCodes = this.jdbcTemplate.query(ManageSettingQueries.FETCHCOUNTRYCODESQUERY, ParameterizedBeanPropertyRowMapper
					.newInstance(CountryCode.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return countryCodes;
	}

	/**
	 * The method for fetching payment type details.
	 * 
	 * @return The PaymentType as List type.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public List<PaymentType> getPaymentTypeDetails() throws ScanSeeException
	{
		log.error("Inside getPaymentTypeDetails method");
		List<PaymentType> paymentTypes = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			paymentTypes = simpleJdbcTemplate.query(ManageSettingQueries.GETPAYMENTTYPEDETAILSQUERY, new BeanPropertyRowMapper<PaymentType>(PaymentType.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error("Exception occurred in getPaymentTypeDetails", exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getPaymentTypeDetails", exception);
			throw new ScanSeeException(exception);
		}

		return paymentTypes;
	}

	/**
	 * The method for fetching pay out type details.
	 * 
	 * @return The PayoutType as List type.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public List<PayoutType> getPayoutDetails() throws ScanSeeException
	{
		log.error("Inside getPayoutDetails method");
		List<PayoutType> payoutType = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			payoutType = simpleJdbcTemplate.query(ManageSettingQueries.GETPAYOUTDETAILSQUERY, new BeanPropertyRowMapper<PayoutType>(PayoutType.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error("Exception occurred in getPayoutDetails", exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getPayoutDetails", exception);
			throw new ScanSeeException(exception);
		}

		return payoutType;
	}

	/**
	 * The method for fetching EducationalLevelInfo..
	 * 
	 * @return educationalLevelInfo List.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public List<EducationalLevelInfo> getEducationalLevel() throws ScanSeeException
	{
		final String methodName = "getEducationalLevel";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<EducationalLevelInfo> eduLevelInfoList = null;
		try
		{

			eduLevelInfoList = this.jdbcTemplate.query(ManageSettingQueries.FETCHEDUCATIONALLEVELQUERY, ParameterizedBeanPropertyRowMapper
					.newInstance(EducationalLevelInfo.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return eduLevelInfoList;
	}

	/**
	 * The method for fetching MaritalStatus inforamtions.
	 * 
	 * @return List of MaritalStatus objects.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public List<MaritalStatusInfo> getMaritalStatusInfo() throws ScanSeeException
	{
		final String methodName = "getMaritalStatusInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<MaritalStatusInfo> maritalStatusList = null;
		try
		{

			maritalStatusList = this.jdbcTemplate.query(ManageSettingQueries.FETCHMARITALSTATUSQUERY, ParameterizedBeanPropertyRowMapper
					.newInstance(MaritalStatusInfo.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return maritalStatusList;
	}
	
	/**
	 * This method updates the Pushnotification flag of the product to 0.
	 * @param productId
	 *          - product to be updated.
	 * @param userId
	 *           - user to be updated.
	 * @return response, DB update success or failure.
	 * @throws ScanSeeException 
	 *           The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String upDatePushNotFlag(int productId, int userId) throws ScanSeeException
	{
		final String methodName = "upDatePushNotFlag";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			log.info("fetching upDatePushNotFlag  for UserProductId {}", productId);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WishListDisablePushNotification");
			
			final MapSqlParameterSource shoppingProductParameters = new MapSqlParameterSource();
			//shoppingProductParameters.addValue("UserProductID", productId);
			shoppingProductParameters.addValue("UserId", userId);
			shoppingProductParameters.addValue("ProductId", productId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(shoppingProductParameters);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					response = ApplicationConstants.SUCCESS;
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
					final Integer errorNum = (Integer) resultFromProcedure.get("ErrorNumber");
					log.error("Error occurred in usp_WishListDisablePushNotification Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getProductDetail", exception);
			response = ApplicationConstants.FAILURE;
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}
