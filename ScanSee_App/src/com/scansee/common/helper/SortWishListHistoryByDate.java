package com.scansee.common.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.pojos.WishListResultSet;

/**
 * This class for sorting wish list history based on date.
 * @author Shyamsundhar_hm
 */
public class SortWishListHistoryByDate implements Comparator<WishListResultSet>
{

	/**
	 * Logger instance.
	 */
	private static final Logger log = LoggerFactory.getLogger(SortWishListHistoryByDate.class.getName());
	
	/**
	 * This method is to compare two products.
	 * @param wishListResultSet1
	 *          -As parameter
	 * @param wishListResultSet2
	 *          -As parameter 
	 * @return scanDateComp                 
	 */
	public int compare(WishListResultSet wishListResultSet1, WishListResultSet wishListResultSet2)
	{

		log.info("inside SortWishListHistoryByDate class in compare method");
		Date date1;
		Date date2;
		int scanDateComp=0;
		final SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");

		try
		{
			date1 = formatter.parse(wishListResultSet1.getWishListAddDate());
			date2 = formatter.parse(wishListResultSet2.getWishListAddDate());
			scanDateComp = date1.compareTo(date2);
			
		}
		catch (ParseException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return scanDateComp;

	}
	
	
}
