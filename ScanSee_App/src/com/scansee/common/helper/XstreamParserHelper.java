package com.scansee.common.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AddRemoveSBProducts;
import com.scansee.common.pojos.AddSLRequest;
import com.scansee.common.pojos.AlertedProducts;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.Categories;
import com.scansee.common.pojos.Category;
import com.scansee.common.pojos.CategoryDetail;
import com.scansee.common.pojos.CategoryRequest;
import com.scansee.common.pojos.CellFireInfoValue;
import com.scansee.common.pojos.CellFireInfoValueList;
import com.scansee.common.pojos.CellFireMerchant;
import com.scansee.common.pojos.CellFireRequest;
import com.scansee.common.pojos.CountryCode;
import com.scansee.common.pojos.CountryCodes;
import com.scansee.common.pojos.CountryDetails;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.CouponsDetails;
import com.scansee.common.pojos.CreateOrUpdateUserPreference;
import com.scansee.common.pojos.DisplayMainMenu;
import com.scansee.common.pojos.DisplayMainMenuResultSet;
import com.scansee.common.pojos.EducationalLevelDetails;
import com.scansee.common.pojos.EducationalLevelInfo;
import com.scansee.common.pojos.ExternalAPIInformation;
import com.scansee.common.pojos.FindNearByDetail;
import com.scansee.common.pojos.FindNearByDetails;
import com.scansee.common.pojos.FindNearByIntactResponse;
import com.scansee.common.pojos.FindNearByResponse;
import com.scansee.common.pojos.GoogleCategory;
import com.scansee.common.pojos.HotDealAPIResultSet;
import com.scansee.common.pojos.HotDealsCategoryInfo;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.HotDealsListRequest;
import com.scansee.common.pojos.HotDealsListResultSet;
import com.scansee.common.pojos.HotDealsResultSet;
import com.scansee.common.pojos.IncomeRange;
import com.scansee.common.pojos.IncomeRanges;
import com.scansee.common.pojos.LoyaltyCards;
import com.scansee.common.pojos.LoyaltyDetail;
import com.scansee.common.pojos.LoyaltyDetails;
import com.scansee.common.pojos.MainCategoryDetail;
import com.scansee.common.pojos.MaritalStatusDetails;
import com.scansee.common.pojos.MaritalStatusInfo;
import com.scansee.common.pojos.MyAccountDetails;
import com.scansee.common.pojos.PaymentInterval;
import com.scansee.common.pojos.PaymentType;
import com.scansee.common.pojos.PayoutType;
import com.scansee.common.pojos.ProductAttributes;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.ProductDetailsRequest;
import com.scansee.common.pojos.ProductRatingReview;
import com.scansee.common.pojos.ProductReview;
import com.scansee.common.pojos.ProductReviews;
import com.scansee.common.pojos.PushNotification;
import com.scansee.common.pojos.RebateDetail;
import com.scansee.common.pojos.RebateDetails;
import com.scansee.common.pojos.RetailerCreatedPages;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailerInfo;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.ScreenTextInfo;
import com.scansee.common.pojos.SectionContent;
import com.scansee.common.pojos.ShareProductInfo;
import com.scansee.common.pojos.ShareProductRetailerInfo;
import com.scansee.common.pojos.ShoppingBasketProducts;
import com.scansee.common.pojos.ShoppingCartProducts;
import com.scansee.common.pojos.ShoppingListDetails;
import com.scansee.common.pojos.ShoppingListRetailerCategoryList;
import com.scansee.common.pojos.SubCategoryDetail;
import com.scansee.common.pojos.ThisLocationRequest;
import com.scansee.common.pojos.ThisLocationRetailerInfo;
import com.scansee.common.pojos.TickerInfo;
import com.scansee.common.pojos.TutorialMedia;
import com.scansee.common.pojos.TutorialMediaResultSet;
import com.scansee.common.pojos.UserInfo;
import com.scansee.common.pojos.UserMediaInfo;
import com.scansee.common.pojos.UserNotesDetails;
import com.scansee.common.pojos.UserPreference;
import com.scansee.common.pojos.UserPreferenceDetails;
import com.scansee.common.pojos.UserRatingInfo;
import com.scansee.common.pojos.UserRegistrationInfo;
import com.scansee.common.pojos.UserSettings;
import com.scansee.common.pojos.WishListHistoryDetails;
import com.scansee.common.pojos.WishListProducts;
import com.scansee.common.pojos.WishListResultSet;
import com.scansee.common.util.Utility;
import com.scansee.externalapi.google.pojos.ServiceResult;
import com.scansee.externalapi.shopzilla.pojos.Offer;
import com.scansee.externalapi.shopzilla.pojos.OnlineStores;
import com.thoughtworks.xstream.XStream;

/**
 * The class having xstream alias names for response xmls. This class provides
 * method for Marshalling and Unamarhalling.
 * 
 * @author manjunatha_gh
 */
public class XstreamParserHelper
{
	/**
	 * for getting logger.
	 */

	private final Logger log = LoggerFactory.getLogger(XstreamParserHelper.class);
	/**
	 * for xstream initializes with null.
	 */
	private static XStream xstream;

	/**
	 * This method for alias names for response xmls.
	 * 
	 * @return XStream
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	private static XStream getXStreamParser() throws ScanSeeException
	{
		try
		{
			if (xstream == null)
			{
				xstream = new XStream();
				// xstream.setClassLoader(com.scansee.common.pojos.BaseObject.class.getClassLoader());
				xstream.alias("AuthenticateUser", com.scansee.common.pojos.AuthenticateUser.class);
				xstream.alias("UserRegistrationInfo", com.scansee.common.pojos.UserRegistrationInfo.class);
				xstream.alias("UpdateUserInfo", com.scansee.common.pojos.UpdateUserInfo.class);
				xstream.alias("UserRetailPreferences", com.scansee.common.pojos.UserRetailPreferences.class);
				xstream.alias("DemoGrapyDetails", com.scansee.common.pojos.DemoGrapyDetails.class);
				xstream.aliasField("DemoGrapyDetails", UserRegistrationInfo.class, "demoGrapyDetails");
				xstream.alias("UserInfo", UserInfo.class);
				xstream.alias("IncomeRange", IncomeRange.class);
				xstream.alias("EducationalLevelInfo", EducationalLevelInfo.class);
				xstream.alias("MaritalStatusInfo", MaritalStatusInfo.class);
				xstream.alias("CountryCode", CountryCode.class);
				xstream.alias("ThisLocationRequest", ThisLocationRequest.class);
				xstream.alias("UserRegistrationInfo", UserRegistrationInfo.class);
				xstream.alias("UserNotesDetails", UserNotesDetails.class);
				xstream.alias("RetailerDetail", RetailerDetail.class);
				xstream.alias("RetailerDetails", RetailersDetails.class);
				xstream.alias("ProductDetailsRequest", ProductDetailsRequest.class);
				xstream.alias("ProductDetails", ProductDetails.class);
				xstream.alias("ProductDetail", ProductDetail.class);
				xstream.alias("CategoryRequest", CategoryRequest.class);
				xstream.alias("CategoryDetail", CategoryDetail.class);
				xstream.alias("MainCategoryDetail", MainCategoryDetail.class);
				xstream.alias("SubCategoryDetail", SubCategoryDetail.class);
				xstream.alias("ShoppingListDetails", ShoppingListDetails.class);
				xstream.alias("OnlineStoresRequest", com.scansee.externalapi.shopzilla.pojos.OnlineStoresRequest.class);
				xstream.alias("RetailerInfo", RetailerInfo.class);

				// xstream.alias("HotDealsCategoryInfo",
				// HotDealsCategoryInfo.class);

				xstream.alias("AddSLRequest", AddSLRequest.class);
				xstream.alias("AddShoppingList", AddSLRequest.class);
				xstream.alias("AddShoppingCart", AddSLRequest.class);
				xstream.alias("AddSLHistoryItems", AddSLRequest.class);
				xstream.alias("AddRemoveSBProducts", AddRemoveSBProducts.class);
				xstream.alias("ShoppingCartProducts", ShoppingCartProducts.class);
				xstream.alias("ShoppingBasketProducts", ShoppingBasketProducts.class);
				xstream.alias("MyAccountDetails", MyAccountDetails.class);
				xstream.alias("TutorialMedia", TutorialMedia.class);
				xstream.alias("TutorialMediaResultSet", TutorialMediaResultSet.class);

				xstream.alias("UserMediaInfo", UserMediaInfo.class);

				xstream.alias("DisplayMainMenuResultSet", DisplayMainMenuResultSet.class);
				xstream.alias("DisplayMainMenu", DisplayMainMenu.class);

				xstream.aliasField("RetailerInfo", AddSLRequest.class, "retailerDetails");

				xstream.alias("HotDealsListRequest", HotDealsListRequest.class);

				xstream.alias("ShareProductInfo", ShareProductInfo.class);

				xstream.alias("AddRemoveCLR", com.scansee.common.pojos.AddRemoveCLR.class);
				xstream.alias("CouponsDetails", com.scansee.common.pojos.CouponsDetails.class);
				xstream.alias("RebateDetails", com.scansee.common.pojos.RebateDetails.class);
				xstream.alias("LoyaltyDetails", com.scansee.common.pojos.LoyaltyDetails.class);
				// xstream.alias("FindNearByDetails",
				// com.scansee.common.pojos.FindNearByDetails.class);
				xstream.alias("FindNearByLowestPrice", com.scansee.common.pojos.FindNearByLowestPrice.class);

				xstream.alias("AdvertisementHitReq", com.scansee.common.pojos.AdvertisementHitReq.class);

				xstream.alias("Categories", com.scansee.common.pojos.Categories.class);

				

				xstream.alias("SDKInfo", com.scansee.common.pojos.SDKInfo.class);
				xstream.alias("CouponDetails", CouponDetails.class);
				xstream.alias("RebateDetail", RebateDetail.class);
				xstream.alias("LoyaltyDetail", LoyaltyDetail.class);
				xstream.alias("WishListResultSet", WishListResultSet.class);
				xstream.alias("WishListResultSet", WishListResultSet.class);
				xstream.alias("CreateOrUpdateUserPreference", CreateOrUpdateUserPreference.class);

				xstream.alias("ProductDetail", ProductDetail.class);

				xstream.alias("ProductRetailerInfo", ShareProductRetailerInfo.class);

				xstream.addImplicitCollection(TutorialMediaResultSet.class, "tutorialMedia");
				xstream.addImplicitCollection(ShoppingListDetails.class, "retailerDetails");

				xstream.addImplicitCollection(RetailerInfo.class, "productDetails");

				xstream.addImplicitCollection(RetailersDetails.class, "retailerDetail");

				xstream.addImplicitCollection(RetailersDetails.class, "thisLocationRetailerInfo");
				xstream.alias("ThisLocationRetailerDetail", ThisLocationRetailerInfo.class);

				xstream.addImplicitCollection(ProductDetails.class, "productDetail");
				xstream.addImplicitCollection(CategoryDetail.class, "mainCategoryDetaillst");
				xstream.addImplicitCollection(MainCategoryDetail.class, "subCategoryDetailLst");

				/*
				 * xstream.addImplicitCollection(ShoppingCartProducts.class,
				 * "productDetail");
				 * xstream.addImplicitCollection(ShoppingBasketProducts.class,
				 * "productDetail");
				 */

				xstream.addImplicitCollection(ShoppingCartProducts.class, "retailerDetails");
				xstream.addImplicitCollection(ShoppingBasketProducts.class, "retailerDetails");

				// xstream.addImplicitCollection(HotDealsCategoryInfo.class,
				// "hotDealsCategoryInfo");
				xstream.addImplicitCollection(CouponsDetails.class, "couponDetail");
				xstream.addImplicitCollection(RebateDetails.class, "rebateDetail");
				xstream.addImplicitCollection(LoyaltyDetails.class, "loyaltyDetail");

				xstream.addImplicitCollection(IncomeRanges.class, "incomeRange");
				xstream.addImplicitCollection(EducationalLevelDetails.class, "educationalInfo");
				xstream.addImplicitCollection(MaritalStatusDetails.class, "maritalInfo");
				xstream.addImplicitCollection(CountryCodes.class, "countryCode");

			//	xstream.addImplicitCollection(CountryCodes.class, "countryCode");

				xstream.addImplicitCollection(DisplayMainMenuResultSet.class, "displayMainmenu");

				xstream.alias("UserPreferenceDetails", UserPreferenceDetails.class);
				xstream.alias("UserPreference", UserPreference.class);
				xstream.alias("PaymentType", PaymentType.class);
				xstream.alias("PaymentInterval", PaymentInterval.class);
				xstream.alias("PayoutType", PayoutType.class);
				xstream.aliasField("UserPreference", UserPreferenceDetails.class, "userPreference");

				xstream.alias("HotDealsResultSet", HotDealsResultSet.class);
				xstream.alias("HotDealsListResultSet", HotDealsListResultSet.class);
				xstream.alias("HotDealsCategoryInfo", com.scansee.common.pojos.HotDealsCategoryInfo.class);
				xstream.addImplicitCollection(HotDealsCategoryInfo.class, "hotDealsDetailsArrayLst");
				xstream.addImplicitCollection(HotDealsListResultSet.class, "hotDealsCategoryInfo");
				xstream.alias("HotDealsDetails", HotDealsDetails.class);
				xstream.alias("HotDealsListResultSet", HotDealsListResultSet.class);

				xstream.alias("FindNearByDetail", FindNearByDetail.class);
				xstream.alias("FindNearByDetail", com.scansee.common.pojos.FindNearByDetail.class);
				xstream.alias("FindNearByDetails", FindNearByDetails.class);
				xstream.addImplicitCollection(FindNearByDetails.class, "findNearByDetail");
				xstream.alias("UserRatingInfo", UserRatingInfo.class);
				xstream.alias("ExternalAPIInformation", ExternalAPIInformation.class);

				xstream.alias("ProductReviews", ProductReviews.class);
				xstream.alias("ProductReview", ProductReview.class);

				xstream.alias("ProductRatingReview", ProductRatingReview.class);

				xstream.alias("ShoppingListRetailerCategoryList", ShoppingListRetailerCategoryList.class);
				xstream.addImplicitCollection(ShoppingListRetailerCategoryList.class, "retailerInfolst");
				xstream.alias("MasterSLRetailers", com.scansee.common.pojos.MainSLRetailerCategory.class);
				xstream.addImplicitCollection(RetailerInfo.class, "categoryDetails");

				xstream.alias("FindNearByDetailsResultSet", FindNearByIntactResponse.class);
				xstream.alias("FindNearByDetails", FindNearByResponse.class);
				xstream.addImplicitCollection(FindNearByIntactResponse.class, "findNearByDetails");
				xstream.alias("ScreenTextInfo", ScreenTextInfo.class);
				xstream.addImplicitCollection(ScreenTextInfo.class, "sectionContent");
				xstream.alias("SectionContentInfo", com.scansee.common.pojos.SectionContent.class);
				// xstream.alias("SectionContent", SectionContent.class);
				xstream.alias("RetailerInfoReq", com.scansee.common.pojos.RetailerInfoReq.class);

				xstream.alias("CLRDetails", com.scansee.common.pojos.CLRDetails.class);
				
				xstream.addImplicitCollection(ScreenTextInfo.class, "sectionContent");

				xstream.alias("CountryDetails", CountryDetails.class);
				xstream.addImplicitCollection(CountryDetails.class, "stateInfo");
				xstream.addImplicitCollection(CountryDetails.class, "cityInfo");
				// for

				xstream.alias("TickerInfo", TickerInfo.class);
				xstream.addImplicitCollection(SectionContent.class, "tickerInfolst");
				xstream.alias("HotDealsCategoryInfo", HotDealsCategoryInfo.class);

				// for shopping list category
				xstream.alias("ShoppingListCategoryList", ShoppingListRetailerCategoryList.class);
				xstream.addImplicitCollection(ShoppingListRetailerCategoryList.class, "categoryInfolst");
				
				
				// for Cart and basket shopping list products
				xstream.addImplicitCollection(ShoppingCartProducts.class, "categoryDetails");
				xstream.addImplicitCollection(ShoppingBasketProducts.class, "categoryDetails");

				xstream.alias("WishListHistoryDetails", WishListHistoryDetails.class);
				xstream.addImplicitCollection(WishListHistoryDetails.class, "productHistoryInfo");
				xstream.alias("WishListProducts", WishListProducts.class);
				xstream.alias("AlertedProducts", AlertedProducts.class);
				xstream.addImplicitCollection(AlertedProducts.class, "alertProductDetails");
				xstream.alias("ProdcutAttributes", ProductAttributes.class);

				xstream.alias("APIDeals", HotDealAPIResultSet.class);

				xstream.alias("ServiceSerachRequest", com.scansee.externalapi.google.pojos.ServiceSerachRequest.class);

				xstream.addImplicitCollection(HotDealsCategoryInfo.class, "hotDealAPIResultSetLst");
				// Used for renamimg the field name in the class
				// xstream.aliasField("APIDeals", HotDealsCategoryInfo.class,
				// "hotDealAPIResultSetLst");
				xstream.aliasField("HotDealsDetailsList", HotDealAPIResultSet.class, "hotDealsDetailslst");

				xstream.alias("AlertedProducts", AlertedProducts.class);
				xstream.addImplicitCollection(AlertedProducts.class, "alertProductDetails");
				// xstream.addImplicitCollection(CouponsDetails.class,
				// "productNamesLst");
				xstream.addImplicitCollection(CouponsDetails.class, "couponDetail");
				// xstream.addImplicitCollection(RebateDetails.class,
				// "productNamesLst");
				// xstream.addImplicitCollection(LoyaltyDetails.class,
				// "productNamesLst");
				xstream.addImplicitCollection(LoyaltyDetails.class, "loyaltyDetail");
				//xstream.alias("Category", com.scansee.common.pojos.Category.class);
				
			xstream.alias("APICategory", GoogleCategory.class);
			//for category 
			//xstream.alias("CategoryInfo", com.scansee.common.pojos.CategoryInfo.class);
			xstream.alias("Category", Category.class);
			xstream.addImplicitCollection(Categories.class, "categoryInfo");
			xstream.addImplicitCollection(Category.class, "productDetails");
			xstream.alias("StateCityLst", CountryCodes.class);
			xstream.alias("PushNotification", PushNotification.class);
		
			xstream.alias("OnlineStores", OnlineStores.class);
			xstream.alias("Offer", Offer.class);
			xstream.addImplicitCollection(ProductDetails.class, "specialOfferlst");
			
			xstream.alias("RetailerCreatedPages", RetailerCreatedPages.class);
			
			xstream.alias("Result", ServiceResult.class);
			xstream.alias("ServiceSearchResponse", com.scansee.externalapi.google.pojos.ServiceSearchResponse.class);
			xstream.alias("UserSettings", UserSettings.class);
			
			xstream.alias("InfoValue", CellFireInfoValue.class);
			
			xstream.alias("CFUpdateUser", CellFireInfoValueList.class);
			
			xstream.addImplicitCollection(CellFireInfoValueList.class, "infoValList");
			
		//	xstream.alias("ServiceSearch", ServiceSearchResponse.class);
		//	xstream.addImplicitCollection(ServiceSearchResponse.class, "placeSearchlst");
			//For coupon search category list
			xstream.alias("CategoryInfo", com.scansee.common.pojos.CategoryInfo.class);
			xstream.addImplicitCollection(CategoryDetail.class, "searchCategorys");
			
			
			xstream.addImplicitCollection(CLRDetails.class, "loygrpbyRetlst");
			
			// below code for CellFire implementation
			
			xstream.alias("CFMerchant", CellFireMerchant.class);
			xstream.alias("Lcards", LoyaltyCards.class);
			xstream.alias("CFMerchant", CellFireMerchant.class);
			xstream.addImplicitCollection(LoyaltyCards.class, "cellFireMerchant");
			xstream.alias("CFRequest", CellFireRequest.class);

			//for user tracking
			xstream.alias("UserTrackingData", com.scansee.common.pojos.UserTrackingData.class);
			}
		}
		catch (Exception exception)
		{
			throw new ScanSeeException(exception);
		}
		return xstream;

	}

	/**
	 * This method produces the response xml.
	 * 
	 * @param object
	 *            as requested xstream object.
	 * @return outPutXml as String
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public static String produceXMlFromObject(Object object) throws ScanSeeException
	{
		String outPutXml = "";
		xstream = getXStreamParser();
		outPutXml = xstream.toXML(object);
		return Utility.removeSpecialChars(outPutXml);
	}

	/**
	 * This method parses input xml to required object.
	 * 
	 * @param xml
	 *            as requested .
	 * @return Object
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public Object parseXmlToObject(String xml) throws ScanSeeException
	{
		log.info("In parseXmlToObject method");
		Object obj = null;
		try
		{
			xstream = getXStreamParser();

			obj = xstream.fromXML(xml);
		}
		catch (ClassCastException exception)
		{
			log.error("In ClassCastException:", exception);
			throw new ScanSeeException(exception);
		}
		catch (Exception exception)
		{
			log.error("In exception:", exception);
			throw new ScanSeeException(exception);
		}

		return obj;
	}

	/**
	 * Main method.
	 * 
	 * @param args
	 *            as the request. * @throws ScanSeeException The exceptions are
	 *            caught and a ScanSee Exception defined for the application is
	 *            thrown which is caught in the Controller layer.
	 */
	
	
	public static void main(String[] args)
	{
/*
		CellFireMerchant obj1 = new CellFireMerchant();

		obj1.setMerchId("1");
		obj1.setMerchName("xyz");
		obj1.setCardNumber("232323");
		obj1.setMerchLogoSmlImg("sddfdf");
		obj1.setMerchLogoBigImg("dfdsf");

		CellFireMerchant obj2 = new CellFireMerchant();

		obj2.setMerchId("1");
		obj2.setMerchName("xyz");
		obj2.setCardNumber("232323");
		obj2.setMerchLogoSmlImg("sddfdf");
		obj2.setMerchLogoBigImg("dfdsf");
		
		List<CellFireMerchant> list = new ArrayList<CellFireMerchant>();
		
		list.add(obj1);
		list.add(obj2);
		
		
		LoyaltyCards lc = new LoyaltyCards();
		
		lc.setCellFireMerchant(list);
		
		xstream = new XStream();
		
		xstream.alias("CFMerchant", CellFireMerchant.class);
		xstream.alias("Lcards", LoyaltyCards.class);
		
		xstream.alias("CFMerchant", CellFireMerchant.class);
		xstream.alias("Locards", LoyaltyCards.class);
		
		xstream.addImplicitCollection(LoyaltyCards.class, "cellFireMerchant");
			
//		System.out.println(xstream.toXML(lc));
		
		String xml = "<Locards><CFMerchant><merchId>1</merchId><merchName>xyz</merchName><merchLogoSmlImg>sddfdf</merchLogoSmlImg><merchLogoBigImg>dfdsf</merchLogoBigImg><cardNumber>232323</cardNumber></CFMerchant>" +
					"<CFMerchant><merchId>1</merchId><merchName>xyz</merchName><merchLogoSmlImg>sddfdf</merchLogoSmlImg><merchLogoBigImg>dfdsf</merchLogoBigImg><cardNumber>232323</cardNumber>" +
					"</CFMerchant></Locards>";		LoyaltyCards lloo = (LoyaltyCards) xstream.fromXML(xml);
		
//		System.out.println(lloo.getCellFireMerchant().size());

 */
	}
}
