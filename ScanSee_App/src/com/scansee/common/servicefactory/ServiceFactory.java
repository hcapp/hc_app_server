package com.scansee.common.servicefactory;

import com.scansee.clrgallery.service.CLRGalleryService;
import com.scansee.clrgallery.service.CLRGalleryServiceImpl;
import com.scansee.find.service.FindService;
import com.scansee.find.service.FindServiceImpl;
import com.scansee.firstuse.service.FirstUseService;
import com.scansee.firstuse.service.FirstUseServiceImpl;
import com.scansee.hotdeals.service.HotDealsService;
import com.scansee.hotdeals.service.HotDealsServiceImpl;
import com.scansee.manageloyaltycard.service.ManageLoyaltyCardService;
import com.scansee.manageloyaltycard.service.ManageLoyaltyCardServiceImpl;
import com.scansee.managesettings.service.ManageSettingsService;
import com.scansee.managesettings.service.ManageSettingsServiceImpl;
import com.scansee.myaccount.service.MyAccountService;
import com.scansee.myaccount.service.MyAccountServiceImpl;
import com.scansee.mygallery.service.MyGalleryService;
import com.scansee.mygallery.service.MyGalleryServiceImpl;
import com.scansee.ratereview.service.RateReviewService;
import com.scansee.scannow.service.ScanNowService;
import com.scansee.scannow.service.ScanNowServiceImpl;
import com.scansee.shoppinglist.service.ShoppingListService;
import com.scansee.shoppinglist.service.ShoppingListServiceImpl;
import com.scansee.thislocation.service.ThisLocationService;
import com.scansee.thislocation.service.ThisLocationServiceImpl;
import com.scansee.wishlist.service.WishListService;
import com.scansee.wishlist.service.WishListServiceImpl;

/**
 * The class is for handling user defined ScanSeeException.
 * 
 * @author =
 */
public class ServiceFactory
{
	/**
	 * Constructor.
	 */
	private ServiceFactory()
	{
	}

	/**
	 * @return the FirstUseService
	 */
	public static FirstUseService getFirstUseService()
	{
		return (FirstUseService) ScanSeeServices.getBean("firstUseService", FirstUseServiceImpl.class);
	}

	/**
	 * @return the ManageSettingsService
	 */
	public static ManageSettingsService getManageSettingsService()
	{
		return (ManageSettingsService) ScanSeeServices.getBean("manageSettingService", ManageSettingsServiceImpl.class);
	}

	/**
	 * @return the ThisLocationService
	 */
	public static ThisLocationService getThisLocationService()
	{
		return (ThisLocationService) ScanSeeServices.getBean("thisLocationService", ThisLocationServiceImpl.class);
	}

	/**
	 * @return the ScanNowService
	 */
	public static ScanNowService getScanNowService()
	{
		return (ScanNowService) ScanSeeServices.getBean("scanNowService", ScanNowServiceImpl.class);
	}

	/**
	 * @return the ShoppingListService
	 */
	public static ShoppingListService getShoppingListService()
	{
		return (ShoppingListService) ScanSeeServices.getBean("shoppingListService", ShoppingListServiceImpl.class);

	}

	/**
	 * @return the HotDealsService
	 */
	public static HotDealsService getHotDealsService()
	{
		return (HotDealsService) ScanSeeServices.getBean("hotDealsService", HotDealsServiceImpl.class);
	}

	/**
	 * @return the MyAccountService
	 */
	public static MyAccountService getMyAccountService()
	{
		return (MyAccountService) ScanSeeServices.getBean("myAccountService", MyAccountServiceImpl.class);
	}

	/**
	 * @return the WishListService
	 */
	public static WishListService getWishListService()
	{

		return (WishListService) ScanSeeServices.getBean("wishListService", WishListServiceImpl.class);
	}

	/**
	 * @return the RateReviewService
	 */
	public static RateReviewService getRateReviewService()
	{

		return (RateReviewService) ScanSeeServices.getBean("rateReviewService", RateReviewService.class);
	}

	/**
	 * @return the CLRGalleryService
	 */
	public static CLRGalleryService getCLRGalleryService()
	{
		return (CLRGalleryService) ScanSeeServices.getBean("clrGalleryService", CLRGalleryServiceImpl.class);

	}

	/**
	 * @return the MyG
	 */
	public static MyGalleryService getMyGalleryService()
	{
		return (MyGalleryService) ScanSeeServices.getBean("myGalleryService", MyGalleryServiceImpl.class);

	}

	/**
	 * @return the Find
	 */
	public static FindService getFindService()
	{
		return (FindService) ScanSeeServices.getBean("findService", FindServiceImpl.class);

	}
	
	

	/**
	 * @return the Find
	 */
	public static ManageLoyaltyCardService getManageLoyaltyCardService()
	{
		return (ManageLoyaltyCardService) ScanSeeServices.getBean("manageLoyaltyCardService", ManageLoyaltyCardServiceImpl.class);

	}
}
