package com.scansee.common.servicefactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * The class is for handling user defined ScanSeeException.
 * 
 * @author =
 */
public class ScanSeeServices
{
	/**
	 * context declared as ClassPathXmlApplicationContext.
	 */
	private static ClassPathXmlApplicationContext context;

	/**
	 * Getting the logger instance.
	 */

	private static Logger log = LoggerFactory.getLogger(ScanSeeServices.class.getName());

	/**
	 * Constructor.
	 */

	private ScanSeeServices()
	{

	}

	/**
	 * Method for getting context instance.
	 * 
	 * @return context.
	 */
	private static ClassPathXmlApplicationContext getContext()
	{

		log.info("Inside getContext() method");

		if (context == null)
		{
//			context = new ClassPathXmlApplicationContext("ScanSee-service.xml","net/bull/javamelody/monitoring-spring.xml");
			context = new ClassPathXmlApplicationContext("ScanSee-service.xml");
		}
		return context;
	}

	/**
	 * The method for get bean .
	 * 
	 * @param property
	 * @param clazz
	 *            are request parameters.
	 * @return getContext().getBean(property, clazz).
	 
	 */
	public static <T> Object getBean(final String property, final Class<T> clazz)
	{

		log.info("Inside getBean() method");

		return getContext().getBean(property, clazz);
	}
}
