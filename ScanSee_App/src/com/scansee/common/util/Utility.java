package com.scansee.common.util;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.LoyaltyDetail;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.RebateDetail;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailersDetails;

/**
 * The Utility class common methods used in the application Includes methods for
 * forming ResponseXML,getting formatted date.
 * 
 * @author manjunatha_gh.
 */

public class Utility {

	/**
	 * Getting the logger instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(Utility.class);

	/**
	 * variable for checking Debugging.
	 */
	private static final boolean ISDEBUGENABLED = LOG.isDebugEnabled();
	/**
	 * The constant for &#x0.
	 */

	private static final char[] NULL = "&#x0;".toCharArray();

	/**
	 * The constant for &amp.
	 */

	private static final char[] AMP = "&amp;".toCharArray();

	/**
	 * The constant for &lt.
	 */

	private static final char[] LT = "&lt;".toCharArray();

	/**
	 * The constant for &gt.
	 */

	private static final char[] GT = "&gt;".toCharArray();

	/**
	 * The constant for &quot.
	 */

	private static final char[] QUOT = "&quot;".toCharArray();

	/**
	 * The constant for &apos.
	 */

	private static final char[] APOS = "&apos;".toCharArray();

	/**
	 * The constant for HTML Praragraph opening tag.
	 */

	private static final char[] HTMLPARAGRAPHOPEN = "<p>".toCharArray();
	/**
	 * The constant for HTML Praragraph Closing tag.
	 */

	private static final char[] HTMLPARAGRAPHCLOSE = "</p>".toCharArray();
	/**
	 * The constant for HTML Bold Opening tag.
	 */
	private static final char[] HTMLBOLDOPEN = "<b>".toCharArray();

	/**
	 * The constant for HTML Bold Closing tag.
	 */
	private static final char[] HTMLBOLDCLOSE = "</b>".toCharArray();
	/**
	 * The constant for HTML Break tag.
	 */
	private static final char[] HTMLBREAKCLOSE = "<br/>".toCharArray();

	/**
	 * The constant for HTML Break tag.
	 */
	private static final char[] HTMLBREAK = "<br>".toCharArray();

	/**
	 * The constant for HTML List Open tag.
	 */
	private static final char[] HTMLLISTITEMOPEN = "<li>".toCharArray();
	/**
	 * The constant for HTML List closing tag.
	 */
	private static final char[] HTMLLISTITEMCLOSE = "</li>".toCharArray();
	/**
	 * The constant for HTML Unordered List Opening tag.
	 */
	private static final char[] HTMUNORDEREDLISTOPEN = "<ul>".toCharArray();

	/**
	 * The constant for HTML Unordered List Closing tag.
	 */
	private static final char[] HTMLUNORDEREDLISTCLOSE = "</ul>".toCharArray();

	/**
	 * variable for URL.
	 */
	private static final String URL = "http://maps.google.com/maps/geo?output=json";

	/**
	 * variable for DEFAULT_KEY.
	 */
	private static final String DEFAULT_KEY = "YOUR_GOOGLE_API_KEY";
	/**
	 * The method to remove HTML tags.
	 * 
	 * @param inPutXml
	 *            The input xml.
	 * @return the xml with special charcters removed.
	 */

	/**
	 * Created for Geo code sensor parameter
	 */
	public static final String SENSOR = "true";

	public static String removeHTMLTags(String inPutXml) {
		final String methodName = "removeHTMLTags";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String outPutxML = "";
		if (null != inPutXml) {
			outPutxML = null;
			outPutxML = inPutXml.replaceAll(new String(HTMLPARAGRAPHOPEN), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLPARAGRAPHCLOSE), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLBREAK), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLBREAKCLOSE), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLLISTITEMOPEN), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLLISTITEMCLOSE), "");
			outPutxML = outPutxML.replaceAll(new String(HTMUNORDEREDLISTOPEN), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLUNORDEREDLISTCLOSE), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLBOLDOPEN), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLBOLDCLOSE), "");
		}
		return outPutxML;
	}

	/**
	 * this method returns the xml String for the given Values.
	 * 
	 * @param errorCode
	 *            The error code.
	 * @param errorResponse
	 *            The error response.
	 * @return response with formed response.
	 */

	public static String formResponseXml(String errorCode, String errorResponse) {
		final String methodName = "formResponseXml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuilder response = new StringBuilder();
		response.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		response.append("<response>");
		response.append("<responseCode>");
		response.append(errorCode);
		response.append("</responseCode>");
		response.append("<responseText>");
		response.append(errorResponse);
		response.append("</responseText>");
		response.append("</response>");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response.toString();
	}

	/**
	 * this method returns the xml String for the given Values.
	 * 
	 * @param errorCode
	 *            The error code.
	 * @param errorResponse
	 *            The error response.
	 * @param addElementName
	 *            The element name.
	 * @param addElementValue
	 *            The element value.
	 * @return response with formed response.
	 */

	public static String formResponseXml(String errorCode,
			String errorResponse, String addElementName, String addElementValue) {

		final String methodName = "formResponseXml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuilder response = new StringBuilder();
		response.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		response.append("<response>");
		response.append("<responseCode>");
		response.append(errorCode);
		response.append("</responseCode>");
		response.append("<responseText>");
		response.append(errorResponse);
		response.append("</responseText>");
		response.append("<" + addElementName + ">");
		response.append(addElementValue);
		response.append("</" + addElementName + ">");
		response.append("</response>");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response.toString();
	}

	/**
	 * this method returns the xml String for the given Values.
	 * 
	 * @param errorCode
	 *            The error code.
	 * @param errorResponse
	 *            The error response.
	 * @param addElementName
	 *            The element name.
	 * @param addElementValue
	 *            The element value.
	 * @return response with formed response.
	 */

	public static String formResponseXml(String errorCode,
			String errorResponse, String addElementName,
			String addElementValue, String addElementName2,
			String addElementValue2) {

		final String methodName = "formResponseXml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuilder response = new StringBuilder();
		response.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		response.append("<response>");
		response.append("<responseCode>");
		response.append(errorCode);
		response.append("</responseCode>");
		response.append("<responseText>");
		response.append(errorResponse);
		response.append("</responseText>");
		response.append("<" + addElementName + ">");
		response.append(addElementValue);
		response.append("</" + addElementName + ">");
		response.append("<" + addElementName2 + ">");
		response.append(addElementValue2);
		response.append("</" + addElementName2 + ">");
		response.append("</response>");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response.toString();
	}

	public static String formResponseXml(String errorCode,
			String errorResponse, String addElementName,
			String addElementValue, String addElementName2,
			String addElementValue2, String addElementName3,
			String addElementValue3) {

		final String methodName = "formResponseXml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuilder response = new StringBuilder();
		response.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		response.append("<response>");
		response.append("<responseCode>");
		response.append(errorCode);
		response.append("</responseCode>");
		response.append("<responseText>");
		response.append(errorResponse);
		response.append("</responseText>");
		response.append("<" + addElementName + ">");
		response.append(addElementValue);
		response.append("</" + addElementName + ">");
		response.append("<" + addElementName2 + ">");
		response.append(addElementValue2);
		response.append("</" + addElementName2 + ">");
		response.append("<" + addElementName3 + ">");
		response.append(addElementValue3);
		response.append("</" + addElementName3 + ">");
		response.append("</response>");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response.toString();
	}
	
	public static String formResponseXml(String errorCode,
			String errorResponse, String addElementName,
			String addElementValue, String addElementName2,
			String addElementValue2, String addElementName3,
			String addElementValue3, String addElementName4,
			String addElementValue4, String addElementName5,
			String addElementValue5) {

		final String methodName = "formResponseXml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuilder response = new StringBuilder();
		response.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		response.append("<response>");
		response.append("<responseCode>");
		response.append(errorCode);
		response.append("</responseCode>");
		response.append("<responseText>");
		response.append(errorResponse);
		response.append("</responseText>");
		response.append("<" + addElementName + ">");
		response.append(addElementValue);
		response.append("</" + addElementName + ">");
		response.append("<" + addElementName2 + ">");
		response.append(addElementValue2);
		response.append("</" + addElementName2 + ">");
		response.append("<" + addElementName3 + ">");
		response.append(addElementValue3);
		response.append("</" + addElementName3 + ">");
		response.append("<" + addElementName4 + ">");
		response.append(addElementValue4);
		response.append("</" + addElementName4 + ">");
		response.append("<" + addElementName5 + ">");
		response.append(addElementValue5);
		response.append("</" + addElementName5 + ">");
		response.append("</response>");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response.toString();
	}

	public static String formResponseXml(String errorCode,
			String errorResponse, String addElementName,
			String addElementValue, String addElementName2,
			String addElementValue2, String addElementName3,
			String addElementValue3, String addElementName4,
			String addElementValue4, String addElementName5,
			String addElementValue5, String addElementName6, String addElementValue6, String addElementName7, String addElementValue7) {

		final String methodName = "formResponseXml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuilder response = new StringBuilder();
		response.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		response.append("<response>");
		response.append("<responseCode>");
		response.append(errorCode);
		response.append("</responseCode>");
		response.append("<responseText>");
		response.append(errorResponse);
		response.append("</responseText>");
		response.append("<" + addElementName + ">");
		response.append(addElementValue);
		response.append("</" + addElementName + ">");
		response.append("<" + addElementName2 + ">");
		response.append(addElementValue2);
		response.append("</" + addElementName2 + ">");
		response.append("<" + addElementName3 + ">");
		response.append(addElementValue3);
		response.append("</" + addElementName3 + ">");
		response.append("<" + addElementName4 + ">");
		response.append(addElementValue4);
		response.append("</" + addElementName4 + ">");
		response.append("<" + addElementName5 + ">");
		response.append(addElementValue5);
		response.append("</" + addElementName5 + ">");
		response.append("<" + addElementName6 + ">");
		response.append(addElementValue6);
		response.append("</" + addElementName6 + ">");
		response.append("<" + addElementName7 + ">");
		response.append(addElementValue7);
		response.append("</" + addElementName7 + ">");
		response.append("</response>");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response.toString();
	}

	/**
	 * The method to get the current date formated in yyyy-MM-dd HH:mm:ss
	 * format.
	 * 
	 * @return The current date formatted in yyyy-MM-dd HH:mm:ss forma
	 * @throws java.text.ParseException
	 *             The exception of type java.text.ParseException
	 */

	public static java.sql.Timestamp getFormattedDate()
			throws java.text.ParseException {
		final String methodName = "getFormattedDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final Date date = new Date();
		java.sql.Timestamp sqltDate = null;
		/*
		 * formatting the current date.
		 */
		// final DateFormat formater = new SimpleDateFormat("yyyy-dd-MM");
		final DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
		java.util.Date parsedUtilDate;
		parsedUtilDate = formater.parse(formater.format(date));
		sqltDate = new java.sql.Timestamp(parsedUtilDate.getTime());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return sqltDate;
	}

	/**
	 * The method to get the current date formated in yyyy-MM-dd HH:mm:ss
	 * format.
	 * 
	 * @param dbdate
	 *            -As parameter
	 * @return The current date formatted in yyyy-MM-dd HH:mm:ss forma
	 */

	public static String convertDBdate(String dbdate) {
		final String methodName = "convertDBdate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String javaDate = null;
		try {
			// In side method >>> convertDBdate
			SimpleDateFormat formatter, FORMATTER;
			// formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = formatter.parse(dbdate);
			FORMATTER = new SimpleDateFormat("MM-dd-yyyy");
			// LOG.info("NewDate-->" + FORMATTER.format(date));
			javaDate = FORMATTER.format(date);
		} catch (ParseException exception) {
			LOG.info("Exception in convertDBdate method"
					+ exception.getMessage());
			return null;
		}
		return javaDate;
	}

	/**
	 * The method for formatting the date in yyyy-MM-dd HH:mm:ss format.
	 * 
	 * @param userDate
	 *            -As parameter
	 * @return The date formatted in yyyy-MM-dd HH:mm:ss format.
	 * @throws java.text.ParseException
	 *             - exception.
	 */

	public static Date getFormattedUserDate(String userDate)
			throws java.text.ParseException {

		final String methodName = "getFormattedUserDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName + userDate);

		java.util.Date utilDate = null;
		final SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		utilDate = dateFormat.parse(userDate);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new java.sql.Timestamp(utilDate.getTime());

	}

	/**
	 * The method for formatting the date in yyyy-MM-dd HH:mm:ss format.
	 * 
	 * @param userDate
	 *            date to be formated.
	 * @return The date formatted in yyyy-MM-dd HH:mm:ss format.
	 * @throws java.text.ParseException
	 *             excpetion.
	 */
	public static Date getFormattedUserDateNoTimeStamp(String userDate)
			throws java.text.ParseException {

		final String methodName = "getFormattedUserDateNoTimeStamp";
		LOG.info(ApplicationConstants.METHODSTART + methodName + userDate);

		java.util.Date utilDate = null;
		final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		utilDate = dateFormat.parse(userDate);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new java.sql.Timestamp(utilDate.getTime());
	}

	/**
	 * The method for checking if a date is valid.
	 * 
	 * @param inDate
	 *            The date to be validated.
	 * @return true or false based on validation result.
	 */

	public static boolean isValidDate(String inDate) {

		final String methodName = "isValidDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName + "date"
				+ inDate);

		final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/DD/yyyy");

		try {
			// parse the inDate parameter
			dateFormat.parse(inDate.trim());
		} catch (ParseException pe) {
			return false;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return true;
	}

	/**
	 * The method to trim the whitespaces of a string.
	 * 
	 * @param inputString
	 *            The string which needs to be trimmed.
	 * @return The trimmed string.
	 */

	public static String trimString(String inputString) {

		final String methodName = "trimString";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (null != inputString && "".equals(inputString)) {
			inputString = inputString.trim();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return inputString;
	}

	/**
	 * The method to remove the special characters.
	 * 
	 * @param inPutXml
	 *            The input xml.
	 * @return the xml with special charcters removed.
	 */

	public static String removeSpecialChars(String inPutXml) {

		final String methodName = "removeSpecialChars";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String outPutxML = "";
		if (null != inPutXml) {
			outPutxML = null;
			outPutxML = inPutXml.replaceAll(new String(AMP), "&");
			outPutxML = outPutxML.replaceAll(new String(GT), ">");
			outPutxML = outPutxML.replaceAll(new String(LT), "<");
			outPutxML = outPutxML.replaceAll(new String(NULL), "null");
			// outPutxML = outPutxML.replaceAll(new String(QUOT), "\"\"");
			outPutxML = outPutxML.replaceAll(new String(QUOT), "\"");
			outPutxML = outPutxML.replaceAll(new String(APOS), "'");
			outPutxML = outPutxML.replaceAll("\n", "");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return outPutxML;
	}

	/**
	 * This method is used to remove special characters.
	 * 
	 * @param inPutXml
	 *            -As parameter
	 * @return outPutxML
	 */
	public static String removeSpecialCharsReverse(String inPutXml) {

		final String methodName = "removeSpecialChars";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String outPutxML = "";
		if (null != inPutXml) {
			outPutxML = null;
			outPutxML = inPutXml.replaceAll("&", new String(AMP));
			// outPutxML = outPutxML.replaceAll( "<" , new String(LT));
			/*
			 * outPutxML = outPutxML.replaceAll(new String(GT), ">"); outPutxML
			 * = outPutxML.replaceAll(new String(LT), "<"); outPutxML =
			 * outPutxML.replaceAll(new String(NULL), "null"); outPutxML =
			 * outPutxML.replaceAll(new String(QUOT), "\"\""); outPutxML =
			 * outPutxML.replaceAll(new String(APOS), "'");
			 */
			// outPutxML = outPutxML.replaceAll("\n", "");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return outPutxML;
	}

	/**
	 * The method for getting comma separated values.
	 * 
	 * @param shoppingProductDetails
	 *            The List of product details.
	 * @return The string with comma separated values.
	 */

	public static String getCommaSepartedValues(
			List<ProductDetail> shoppingProductDetails) {

		final String methodName = "getCommaSepartedValues";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final int listSize = shoppingProductDetails.size();
		final StringBuilder productId = new StringBuilder();
		int i = 0;
		for (ProductDetail productDetail : shoppingProductDetails) {
			i++;

			productId.append(productDetail.getProductId());
			if (i != listSize) {
				productId.append(",");
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productId.toString();
	}

	/**
	 * The method for getting comma separated values.
	 * 
	 * @param shoppingProductDetails
	 *            The List of product details.
	 * @return The string with comma separated values.
	 */

	public static String getCommaSepartedUserProductID(
			List<ProductDetail> shoppingProductDetails) {

		final String methodName = "getCommaSepartedValues";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final int listSize = shoppingProductDetails.size();
		final StringBuilder userProductId = new StringBuilder();
		int i = 0;
		for (ProductDetail productDetail : shoppingProductDetails) {
			i++;

			userProductId.append(productDetail.getUserProductId());
			if (i != listSize) {
				userProductId.append(",");
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return userProductId.toString();
	}

	/**
	 * The method to get Comma Separated Product ID values.
	 * 
	 * @param shoppingProductDetails
	 *            The Product Details list to be made as comma separated.
	 * @return The string with comma separated values.
	 */

	public static String getCommaSepartedUserProdIdValues(
			List<ProductDetail> shoppingProductDetails) {

		final String methodName = "getCommaSepartedUserProdIdValues";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final int listSize = shoppingProductDetails.size();
		final StringBuilder userProductId = new StringBuilder();
		int i = 0;
		for (ProductDetail productDetail : shoppingProductDetails) {
			i++;

			userProductId.append(productDetail.getUserProductId());
			if (i != listSize) {
				userProductId.append(",");
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return userProductId.toString();
	}

	/**
	 * The method for null checks.
	 * 
	 * @param arg
	 *            the input string
	 * @return the response string.
	 */

	public static String nullCheck(String arg) {

//		final String methodName = "nullCheck";
//		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (null != arg && !"".equals(arg)) {
			arg.trim();
		}
//		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arg;
	}

	/**
	 * This method is used to check nulls.
	 * 
	 * @param arg
	 *            -As parameter
	 * @return arg
	 */
	public static String isNull(String arg) {

//		final String methodName = "isNull";
//		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (null != arg && !"".equals(arg)) {
			arg.trim();
		} else {
			arg = "";
		}
//		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arg;
	}

	/**
	 * This method is used to check Empty strings or "".
	 * 
	 * @param arg
	 *            -As parameter
	 * @return arg
	 */
	public static boolean isEmptyOrNullString(String arg) {

		final String methodName = "isEmptyString";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final boolean emptyString;

		if (null == arg || "".equals(arg.trim())) {
			emptyString = true;
		} else {
			emptyString = false;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emptyString;
	}

	/**
	 * The method for forming HTML with product Info.
	 * 
	 * @param productDetail
	 *            product object.
	 * @param retailerDetail
	 *            retailer object
	 * @param userId
	 *            int parameter
	 * @return The email template.
	 * @throws NoSuchAlgorithmException
	 *             - Exception related to algorithm
	 * @throws NoSuchPaddingException
	 *             - Exception related to Padding
	 * @throws InvalidKeyException
	 *             - Exception related to InvalidKey
	 * @throws InvalidAlgorithmParameterException
	 *             - Exception related to InvalidAlgorithmParameter
	 * @throws InvalidKeySpecException
	 *             - Exception related to InvalidKeySpec
	 * @throws IllegalBlockSizeException
	 *             - Exception related to IllegalBlockSize
	 * @throws BadPaddingException
	 *             - Exception related to BadPaddings
	 */

	public static String formProductInfoHTML(List<ProductDetail> productDetail,
			RetailerDetail retailerDetail, int userId, String shareURL)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException,
			InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException {

		final String methodName = "formProductInfoHTML";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String productKey = null;
		String productId = Integer
				.toString(productDetail.get(0).getProductId());
		String decryptedUserId = Integer.toString(userId);

		String key = decryptedUserId + "/" + productId;
		EncryptDecryptPwd enryptDecryptpwd = null;
		enryptDecryptpwd = new EncryptDecryptPwd();
		productKey = enryptDecryptpwd.encrypt(key);

		/*
		 * final String productInfo =
		 * "<html><head></head><body><b>Product Name:</b>" +
		 * productDetail.getProductName() + "<br><b>Product Information:</b>" +
		 * productDetail.getProductDescription() + "" +
		 * "<br><b>Manufacturer Name:</b>" +
		 * productDetail.getManufacturersName() + "<br><b>Weight:</b>" +
		 * productDetail.getWeight() + "<br><b>Weight Units:</b>" +
		 * productDetail.getWeightUnits() +
		 * "<br><b>Product Expiration Date:</b>" +
		 * productDetail.getProductExpDate() +
		 * "<br><br><img height=\"250\" width=\"250\" src=\"http://10.11.204.10:8080/"
		 * + productDetail.getImagePath() + "\"/></body></html>";
		 */
		final StringBuffer emailTemplate = new StringBuffer(
				"<html><head></head>");
		emailTemplate
				.append("<body> <h4> I found this for you and thought you might be interested:</h4>\n");
		emailTemplate.append("<table border=\"0\">");
		emailTemplate.append("<tr><td width=\"300\" rowspan=\"7\">");

		if (productDetail.get(0).getImagePath() != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(productDetail
						.get(0).getImagePath())) {
			emailTemplate.append("<img height=\"250\" width=\"250\" src=\""
					+ productDetail.get(0).getImagePath() + "\"/></tr>");
		}
		emailTemplate
				.append("<tr><td width=\"300\" height=\"36\"><b>Product Name:</b> "
						+ productDetail.get(0).getShareProdName()
						+ "</td></tr>");
		emailTemplate
				.append("<tr><td width=\"300\" height=\"36\"><b>Product Information:</b> "
						+ productDetail.get(0).getShareProdLongDesc()
						+ "</td></tr>");

		if (null != retailerDetail) {

			emailTemplate
					.append("<tr><td width=\"300\" height=\"36\"><b>Retailer Name:</b> "
							+ retailerDetail.getRetailerName() + "</td></tr>");
			emailTemplate
					.append("<tr><td width=\"300\" height=\"36\"><b>Address:</b> "
							+ retailerDetail.getRetaileraddress1()
							+ ","
							+ retailerDetail.getCity()
							+ ","
							+ retailerDetail.getState()
							+ ","
							+ retailerDetail.getPostalCode() + "</td></tr>");
			emailTemplate
					.append("<tr><td width=\"300\" height=\"36\"><b>Sale Price:</b> "
							+ retailerDetail.getSalePrice() + "</td></tr>");

		}
		emailTemplate
				.append("<tr><td width=\"300\" height=\"36\"><b>Manufacturer Name:</b> "
						+ productDetail.get(0).getManufacturersName()
						+ "</td></tr>");
		emailTemplate
				.append("<tr><td width=\"300\" height=\"36\"><b>Weight:</b> "
						+ productDetail.get(0).getWeight() + "</td></tr>");
		emailTemplate
				.append("<tr><td width=\"300\" height=\"36\"><b>Weight Units:</b> "
						+ productDetail.get(0).getWeightUnits() + "</td></tr>");
		emailTemplate
				.append("<tr><td width=\"300\" height=\"36\"><b>Product Expiration Date:</b> "
						+ productDetail.get(0).getProductExpDate()
						+ "</td></tr>");
		emailTemplate.append("</table>");

		emailTemplate.append("<tr><td><h4>More info <a href=\"" + shareURL
				+ "productKey=" + productKey
				+ "\"/>Click here</a></h4></td></tr>\n");
		emailTemplate.append("<tr><td></td></tr>\n");
		if (productDetail.get(0).getScanseeimg() != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(productDetail
						.get(0).getScanseeimg())) {
			emailTemplate
					.append("<tr><td><img height=\"40\" width=\"150\" src=\""
							+ productDetail.get(0).getScanseeimg()
							+ "\"/></td></tr>\n");
		}
		if (productDetail.get(0).getFacebookimg() != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(productDetail
						.get(0).getFacebookimg())) {
			emailTemplate
					.append("<tr><td><img height=\"50\" width=\"50\" src=\""
							+ productDetail.get(0).getFacebookimg() + "\"/>\n");
		}
		if (productDetail.get(0).getTwitterimg() != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(productDetail
						.get(0).getTwitterimg())) {
			emailTemplate.append("<img  height=\"50\" width=\"50\" src=\""
					+ productDetail.get(0).getTwitterimg() + "\"/>");
		}
		if (productDetail.get(0).getArrowscanseeimg() != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(productDetail
						.get(0).getArrowscanseeimg())) {
			emailTemplate.append("<img height=\"52\" width=\"52\" src=\""
					+ productDetail.get(0).getArrowscanseeimg()
					+ "\"/></td></tr>\n");
		}
		emailTemplate
				.append("<tr><td><h4>Donating 50% of Profits to Higher Education.   </h4></td></tr>");
		emailTemplate
				.append("<tr><td>              more information at <a href=\"www.ScanSee.com\">www.scansee.com</a></td></tr>");
		emailTemplate.append("</table>");
		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");

		LOG.info("HTML Body" + emailTemplate.toString());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emailTemplate.toString();
	}

	/**
	 * The method for forming ForgotPassword Email Template HTMl.
	 * 
	 * @param password
	 *            The password.
	 * @return mailData The email template for Forgot password.
	 */

	public static String formForgotPasswordEmailTemplateHTML(String password) {
		final String methodName = "formForgotPasswordEmailTemplateHTML";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String mailData = "<html><head></head><body><p>Here is the account information that you requested.<br/>Your password: "
				+ "<b>"
				+ password
				+ "</b><br/><br/>Log in now:<br/>http://www.scansee.com or through your application.<br/><br/>Please note:  For your account security, we only provide your password in this email. "
				+ "Once you have logged in to your account, you can change your password. To keep your account secure, we recommend that you delete this email and keep a copy of your log-in name"
				+ " and password in a secure place.<br/><br/>If you have any additional questions about your account log-in, reply to this email or email us at support@scansee.com. For the fastest response, please include the text of this message along with your"
				+ " question.<br/><br/></p><p>Sincerely,<br/>Scansee Support</p></body></html>";

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return mailData;
	}

	/**
	 * This method takes any number and sets precision of 2 decimal with
	 * rounding.
	 * 
	 * @param value
	 *            values which will be formated.
	 * @return value formated values.
	 */
	public static String formatDecimalValue(String value) {
		final String methodName = "formatDecimalValue";
		if (ISDEBUGENABLED) {
			LOG.info(ApplicationConstants.METHODSTART + methodName);
		}
		if (null != value && !"".equals(value)
				&& !(value.equals(ApplicationConstants.NOTAPPLICABLE))) {
			final Double price = new Double(value);
			final DecimalFormat df = new DecimalFormat("########0.00");
			value = df.format(price);
		} else {
			value = ApplicationConstants.NOTAPPLICABLE;
		}
		if (ISDEBUGENABLED) {
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}
		return value;

	}

	/**
	 * this method returns the xml String for the given Values.
	 * 
	 * @param errorCode
	 *            The error code.
	 * @param errorResponse
	 *            The error response.
	 * @param responseMap
	 *            The response map.
	 * @return response with formed response.
	 */

	public static String formResponseXml(String errorCode,
			String errorResponse, Map<String, String> responseMap) {

		final String methodName = "formResponseXml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Iterator<String> itr = responseMap.keySet().iterator();
		final StringBuilder response = new StringBuilder();
		response.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		response.append("<response>");
		response.append("<responseCode>");
		response.append(errorCode);
		response.append("</responseCode>");
		response.append("<responseText>");
		response.append(errorResponse);
		response.append("</responseText>");
		while (itr.hasNext()) {
			String key = itr.next();
			String value = responseMap.get(key);
			response.append("<" + key + ">");
			response.append(value);
			response.append("</" + key + ">");
		}
		response.append("</response>");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response.toString();
	}

	/**
	 * The method to get the current date formated in yyyy-MM-dd HH:mm:ss
	 * format.
	 * 
	 * @return The current date formatted in yyyy-MM-dd HH:mm:ss forma
	 */

	public static String getFormattedCurrentDate() {
		final String methodName = "getFormattedDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final Date date = new Date();
		/*
		 * formatting the current date.
		 */
		String currentDateStr = null;
		try {
			final DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
			final Date parsedUtilDate = formater.parse(formater.format(date));
			currentDateStr = formater.format(parsedUtilDate);
		} catch (ParseException exception) {
			LOG.info("Exception in convertDBdate method"
					+ exception.getMessage());
			return ApplicationConstants.NOTAPPLICABLE;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return currentDateStr;
	}

	/**
	 * Thsi method is used to round the values to nearest value.
	 * 
	 * @param distance
	 *            -As String parameter
	 * @return distance rounded value
	 */
	public static String roundNearestValues(String distance) {
		if (null != distance && !"".equals(distance)) {
			try {
				final Float fltDistance = new Float(distance);
				distance = String.valueOf(Math.round(fltDistance));
			} catch (NumberFormatException exception) {
				LOG.info("Exception in roundNearestValues method"
						+ exception.getMessage());
				distance = ApplicationConstants.NOTAPPLICABLE;
			}

		} else {
			distance = ApplicationConstants.NOTAPPLICABLE;
		}
		return distance;
	}

	/**
	 * This method is used to form xml.
	 * 
	 * @param rootTag
	 *            -The root tag
	 * @param responseMap
	 *            -As parameter
	 * @return response
	 */
	public static String formResponseXml(String rootTag,
			Map<String, String> responseMap) {
		final String methodName = "formResponseXml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Iterator<String> itr = responseMap.keySet().iterator();
		final StringBuilder response = new StringBuilder();
		response.append("<" + rootTag + ">");
		while (itr.hasNext()) {
			String key = itr.next();
			String value = responseMap.get(key);
			response.append("<" + key + ">");
			response.append(value);
			response.append("</" + key + ">");
		}
		response.append("</" + rootTag + ">");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response.toString();
	}

	/**
	 * The method for null checks.
	 * 
	 * @param arg
	 *            the input string
	 * @return the response string.
	 */

	public static String checkNull(String arg) {

		final String methodName = "nullCheck";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (null != arg && !"".equals(arg)) {
			arg.trim();
		} else {

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arg;
	}

	public static boolean isEmpty(String value) {
		final String methodName = "isEmptyString";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final boolean emptyString;

		if ("".equals(value)) {
			emptyString = true;
		} else {
			emptyString = false;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emptyString;
	}

	/**
	 * Validate Email Id with regular expression
	 * 
	 * @param strEmailId
	 *            strEmailId for validation
	 * @return true valid strEmailId, false invalid strEmailId
	 */
	public static boolean validateEmailId(final String strEmailId) {

		Pattern pattern = null;
		Matcher matcher;
		final String EMAIL_PATTERN = ApplicationConstants.EMAIL_PATTERN;

		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(strEmailId);
		return matcher.matches();

	}

	public static void main(String[] args) {
		// String shopMinPrice="sdf";
		// System.out.println("value is " + !"".equals(shopMinPrice));
	}

	/**
	 * This method for sharing coupon information through mail.
	 * 
	 * @param shareCouponLst
	 *            contains coupon information
	 * @return response
	 */
	public static String formCouponInfoHTML(List<CouponDetails> shareCouponLst)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException,
			InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException {

		final String methodName = "formCouponInfoHTML";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		/*
		 * String productKey = null; String productId =
		 * Integer.toString(productDetail.getProductId()); String
		 * decryptedUserId = Integer.toString(userId); String key =
		 * decryptedUserId + "/" + productId; EncryptDecryptPwd enryptDecryptpwd
		 * = null; enryptDecryptpwd = new EncryptDecryptPwd(); productKey =
		 * enryptDecryptpwd.encrypt(key);
		 */

		final StringBuffer emailTemplate = new StringBuffer(
				"<html><head></head>");
		emailTemplate
				.append("<body> <h4> I found this for you and thought you might be interested:</h4>\n");
		emailTemplate.append("<table border=\"0\">");
		emailTemplate.append("<tr>");
		// emailTemplate.append("<img height=\"250\" width=\"250\" src=\"" +
		// ShareProductInfo.getImagePath() + "\"/>");
		emailTemplate.append("<td  height=\"36\"><b>Coupon Name:</b> "
				+ shareCouponLst.get(0).getCouponName() + "</td></tr>");

		if (shareCouponLst.get(0).getCouponURL() != null
				&& !shareCouponLst.get(0).getCouponURL().equals("N/A")) {
			emailTemplate
					.append("<tr><td  height=\"36\"><b>Coupon URL:</b> <a href=\""
							+ shareCouponLst.get(0).getCouponURL()
							+ "\">"
							+ shareCouponLst.get(0).getCouponURL()
							+ "</a></td></tr>");
		}
		if (shareCouponLst.get(0).getCouponImagePath() != null
				&& !shareCouponLst.get(0).getCouponImagePath().equals("N/A")) {
			String imagePath = shareCouponLst.get(0).getCouponImagePath();
			if (imagePath != "") {
				int index = imagePath.lastIndexOf('/');
				if (index > 0) {
					String imageName = imagePath.substring(
							imagePath.lastIndexOf('/') + 1, imagePath.length());
					if (!"imageNotFound.png".equals(imageName)) {
						emailTemplate
								.append("<tr><td  height=\"36\"><b>Coupon Image:</b> <img height=\"40\" width=\"40\" src=\""
										+ shareCouponLst.get(0)
												.getCouponImagePath()
										+ "\"/></td></tr>");
						// emailTemplate.append("<img height=\"250\" width=\"250\" src=\""
						// + productDetail.get(0).getImagePath() + "\"/></tr>");
					}
				}
			}

		}
		emailTemplate
				.append("<tr><td  height=\"36\"><b>Coupon Start Date:</b> "
						+ shareCouponLst.get(0).getCouponStartDate()
						+ "</td></tr>");
		emailTemplate
				.append("<tr><td  height=\"36\"><b>Coupon Expire Date:</b> "
						+ shareCouponLst.get(0).getCouponExpireDate()
						+ "</td></tr>");
		emailTemplate.append("</table>");
		emailTemplate.append("<tr><td></td></tr>\n");
		if (shareCouponLst.get(0).getScanseeimg() != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(shareCouponLst
						.get(0).getScanseeimg())) {
			emailTemplate
					.append("<tr><td><img height=\"40\" width=\"150\" src=\""
							+ shareCouponLst.get(0).getScanseeimg()
							+ "\"/></td></tr>\n");
		}
		if (shareCouponLst.get(0).getFacebookimg() != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(shareCouponLst
						.get(0).getFacebookimg())) {
			emailTemplate
					.append("<tr><td><img height=\"50\" width=\"50\"  src=\""
							+ shareCouponLst.get(0).getFacebookimg() + "\"/>\n");
		}
		if (shareCouponLst.get(0).getTwitterimg() != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(shareCouponLst
						.get(0).getTwitterimg())) {
			emailTemplate.append("<img height=\"50\" width=\"50\" src=\""
					+ shareCouponLst.get(0).getTwitterimg() + "\"/>");
		}
		if (shareCouponLst.get(0).getArrowscanseeimg() != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(shareCouponLst
						.get(0).getArrowscanseeimg())) {
			emailTemplate.append("<img  height=\"52\" width=\"52\" src=\""
					+ shareCouponLst.get(0).getArrowscanseeimg()
					+ "\"/></td></tr>\n");
		}
		emailTemplate
				.append("<tr><td><h4>Donating 50% of Profits to Higher Education.   </h4></td></tr>");
		emailTemplate
				.append("<tr><td>              more information at <a href=\"www.ScanSee.com\">www.scansee.com</a></td></tr>");

		emailTemplate.append("</table>");

		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");

		LOG.info("HTML Body" + emailTemplate.toString());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emailTemplate.toString();
	}

	/**
	 * This method for sharing hotdeal information through mail.
	 * 
	 * @param hotDealsDetailslst
	 *            contains hot deal information
	 * @return response
	 */
	public static String formHotdealInfoHTML(List<HotDealsDetails> hotDealsDetailslst)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException,
			InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException {
		
		final String methodName = "formHotdealInfoHTML";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		//To format date
		for(HotDealsDetails hdDetails : hotDealsDetailslst)	{
			hdDetails.setIsDateFormated(false);
			hdDetails.sethDStartDate(hdDetails.gethDStartDate());
			hdDetails.sethDEndDate(hdDetails.gethDEndDate());
			hdDetails.setIsDateFormated(null);
		}
		
		final StringBuffer emailTemplate = new StringBuffer("<html><head></head>");
		emailTemplate.append("<body> <h4> I found this for you and thought you might be interested:</h4>\n");
		emailTemplate.append("<table border=\"0\">");
		emailTemplate.append("<tr>");
		// emailTemplate.append("<img height=\"250\" width=\"250\" src=\"" +
		// ShareProductInfo.getImagePath() + "\"/>");
		
		//To remove CDATA in hot deal name
		String hdName = hotDealsDetailslst.get(0).getHotDealName();
		if(hdName.contains("<![CDATA["))	{
			hdName = hdName.substring(9);
			hdName = hdName.substring(0, hdName.length() - 3);
		}
		

		emailTemplate.append("<td height=\"36\"><b>HotDeal Name:</b> " + hdName + "</td></tr>");
		if (hotDealsDetailslst.get(0).getHdURL() != null && !hotDealsDetailslst.get(0).getHdURL().equals("N/A")) {
			emailTemplate.append("<tr><td  height=\"36\"><b>HotDeal URL:</b> <a href=\""
							+ hotDealsDetailslst.get(0).getHdURL()
							+ "\">"
							+ hotDealsDetailslst.get(0).getHdURL()
							+ "</a></td></tr>");
		}

		emailTemplate.append("<tr><td  height=\"36\"><b>HotDeal Start Date:</b> " + hotDealsDetailslst.get(0).gethDStartDate() + "</td></tr>");
		if(hotDealsDetailslst.get(0).gethDEndDate() != null)	{
			emailTemplate.append("<tr><td  height=\"36\"><b>HotDeal End Date:</b> "	+ hotDealsDetailslst.get(0).gethDEndDate() + "</td></tr>");
		}
		emailTemplate.append("<tr><td></td></tr>\n");
		if (hotDealsDetailslst.get(0).getScanseeimg() != null && !ApplicationConstants.NOTAPPLICABLE.equals(hotDealsDetailslst.get(0).getScanseeimg())) {
			emailTemplate.append("<tr><td><img height=\"40\" width=\"150\" src=\"" + hotDealsDetailslst.get(0).getScanseeimg() + "\"/></td></tr>\n");
		}
		if (hotDealsDetailslst.get(0).getFacebookimg() != null && !ApplicationConstants.NOTAPPLICABLE.equals(hotDealsDetailslst.get(0).getFacebookimg())) {
			emailTemplate.append("<tr><td><img height=\"50\" width=\"50\" src=\"" + hotDealsDetailslst.get(0).getFacebookimg() + "\"/>\n");
		}
		if (hotDealsDetailslst.get(0).getTwitterimg() != null && !ApplicationConstants.NOTAPPLICABLE.equals(hotDealsDetailslst.get(0).getTwitterimg())) {
			emailTemplate.append("<img height=\"50\" width=\"50\" src=\"" + hotDealsDetailslst.get(0).getTwitterimg() + "\"/>");
		}
		if (hotDealsDetailslst.get(0).getArrowscanseeimg() != null && !ApplicationConstants.NOTAPPLICABLE.equals(hotDealsDetailslst.get(0).getArrowscanseeimg())) {
			emailTemplate.append("<img height=\"52\" width=\"52\" src=\"" + hotDealsDetailslst.get(0).getArrowscanseeimg() + "\"/></td></tr>\n");
		}
		emailTemplate.append("<tr><td><h4>Donating 50% of Profits to Higher Education.   </h4></td></tr>");
		emailTemplate.append("<tr><td>              more information at <a href=\"www.ScanSee.com\">www.scansee.com</a></td></tr>");

		emailTemplate.append("</table>");

		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");

		LOG.info("HTML Body" + emailTemplate.toString());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emailTemplate.toString();
	}

	/**
	 * This method for sharing coupon information through mail.
	 * 
	 * @param shareCouponLst
	 *            contains coupon information
	 * @return response
	 */
	public static String formRebateInfoHTML(List<RebateDetail> rebateDetaillst)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException,
			InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException {

		final String methodName = "formRebateInfoHTML";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		/*
		 * String productKey = null; String productId =
		 * Integer.toString(productDetail.getProductId()); String
		 * decryptedUserId = Integer.toString(userId); String key =
		 * decryptedUserId + "/" + productId; EncryptDecryptPwd enryptDecryptpwd
		 * = null; enryptDecryptpwd = new EncryptDecryptPwd(); productKey =
		 * enryptDecryptpwd.encrypt(key);
		 */

		final StringBuffer emailTemplate = new StringBuffer(
				"<html><head></head>");
		emailTemplate
				.append("<body> <h4> I found this for you and thought you might be interested:</h4>\n");
		emailTemplate.append("<table border=\"0\">");
		emailTemplate.append("<tr>");
		// emailTemplate.append("<img height=\"250\" width=\"250\" src=\"" +
		// ShareProductInfo.getImagePath() + "\"/>");
		emailTemplate.append("<td  height=\"36\"><b>Rebate Name :</b> "
				+ rebateDetaillst.get(0).getRebateName() + "</td></tr>");

		if (rebateDetaillst.get(0).getRebateURL() != null
				&& !rebateDetaillst.get(0).getRebateURL().equals("N/A")) {
			emailTemplate
					.append("<tr><td  height=\"36\"><b>Rebate URL :</b> <a href=\""
							+ rebateDetaillst.get(0).getRebateURL()
							+ "\">"
							+ rebateDetaillst.get(0).getRebateURL()
							+ " </a></td></tr>");
		}
		if (rebateDetaillst.get(0).getImagePath() != null
				&& !rebateDetaillst.get(0).getImagePath().equals("N/A")) {
			emailTemplate
					.append("<tr><td  height=\"36\"><b>Rebate Image :</b> "
							+ rebateDetaillst.get(0).getImagePath()
							+ "</td></tr>");
		}
		emailTemplate
				.append("<tr><td  height=\"36\"><b>Rebate Start Date :</b> "
						+ rebateDetaillst.get(0).getRebateStartDate()
						+ "</td></tr>");
		emailTemplate.append("<tr><td  height=\"36\"><b>Rebate End Date :</b> "
				+ rebateDetaillst.get(0).getRebateEndDate() + "</td></tr>");
		emailTemplate.append("</table>");

		// emailTemplate.append("<h4>More info <a href=\"" + shareURL +
		// "productKey=" + productKey + "\"/>Click here</a></h4>\n");

		emailTemplate
				.append("<h4>Check out  <a href=\"www.ScanSee.com\">www.ScanSee.com</a> for more.</h4>\n");
		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");

		LOG.info("HTML Body" + emailTemplate.toString());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emailTemplate.toString();
	}

	/**
	 * This method for sharing loyalty information through mail.
	 * 
	 * @param shareCouponLst
	 *            contains coupon information
	 * @return response
	 */
	public static String formLoyaltyInfoHTML(
			List<LoyaltyDetail> loyaltyDetaillst)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException,
			InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException {

		final String methodName = "formLoyaltyInfoHTML";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		/*
		 * String productKey = null; String productId =
		 * Integer.toString(productDetail.getProductId()); String
		 * decryptedUserId = Integer.toString(userId); String key =
		 * decryptedUserId + "/" + productId; EncryptDecryptPwd enryptDecryptpwd
		 * = null; enryptDecryptpwd = new EncryptDecryptPwd(); productKey =
		 * enryptDecryptpwd.encrypt(key);
		 */

		final StringBuffer emailTemplate = new StringBuffer(
				"<html><head></head>");
		emailTemplate
				.append("<body> <h4> I found this for you and thought you might be interested:</h4>\n");
		emailTemplate.append("<table border=\"0\">");
		emailTemplate.append("<tr>");
		// emailTemplate.append("<img height=\"250\" width=\"250\" src=\"" +
		// ShareProductInfo.getImagePath() + "\"/>");
		emailTemplate.append("<td  height=\"36\"><b>LoyaltyDeal Name :</b> "
				+ loyaltyDetaillst.get(0).getLoyaltyDealName() + "</td></tr>");

		if (loyaltyDetaillst.get(0).getLoyaltyURL() != null
				&& !loyaltyDetaillst.get(0).getLoyaltyURL().equals("N/A")) {
			emailTemplate
					.append("<tr><td  height=\"36\"><b>LoyaltyDeal URL:</b> <a href=\""
							+ loyaltyDetaillst.get(0).getLoyaltyURL()
							+ "\">"
							+ loyaltyDetaillst.get(0).getLoyaltyURL()
							+ "</a></td></tr>");
		}
		if (loyaltyDetaillst.get(0).getImagePath() != null
				&& !loyaltyDetaillst.get(0).getImagePath().equals("N/A")) {
			emailTemplate
					.append("<tr><td  height=\"36\"><b>LoyaltyDeal Image:</b> "
							+ loyaltyDetaillst.get(0).getImagePath()
							+ "</td></tr>");
		}

		emailTemplate
				.append("<tr><td  height=\"36\"><b>LoyaltyDeal Start Date :</b> "
						+ loyaltyDetaillst.get(0).getLoyaltyDealStartDate()
						+ "</td></tr>");
		emailTemplate
				.append("<tr><td  height=\"36\"><b>LoyaltyDeal Expire Date :</b> "
						+ loyaltyDetaillst.get(0).getLoyaltyDealExpireDate()
						+ "</td></tr>");
		emailTemplate.append("</table>");

		// emailTemplate.append("<h4>More info <a href=\"" + shareURL +
		// "productKey=" + productKey + "\"/>Click here</a></h4>\n");

		emailTemplate
				.append("<h4>Check out  <a href=\"www.ScanSee.com\">www.ScanSee.com</a> for more.</h4>\n");
		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");

		LOG.info("HTML Body" + emailTemplate.toString());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emailTemplate.toString();
	}

	/**
	 * This method for sharing special offer information through mail.
	 * 
	 * @param hotDealsDetailslst
	 *            contains hot deal information
	 * @return response
	 */
	public static String formSpecailOffInfoHtml(
			List<RetailersDetails> specialOfflst)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException,
			InvalidKeySpecException, IllegalBlockSizeException,
			BadPaddingException {

		final String methodName = "formSpecailOffInfoHtml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuffer emailTemplate = new StringBuffer(
				"<html><head></head>");
		emailTemplate
				.append("<body> <h4> I found this for you and thought you might be interested:</h4>\n");
		emailTemplate.append("<table border=\"0\">");
		emailTemplate.append("<tr>");
		// emailTemplate.append("<img height=\"250\" width=\"250\" src=\"" +
		// ShareProductInfo.getImagePath() + "\"/>");
		// emailTemplate.append("<td height=\"36\"><b></b> " +
		// specialOfflst.get(0).getRetPageTitle() + "</td></tr>");
		if (specialOfflst.get(0).getQrUrl() != null
				&& !specialOfflst.get(0).getQrUrl().equals("N/A")) {
			emailTemplate
					.append("<tr> <td  height=\"36\"><b></b>Click here for more information :"
							+ "<a href="
							+ specialOfflst.get(0).getQrUrl()
							+ ">Retailer Special Offer" + "</a></td></tr>");
		}

		emailTemplate.append("<tr><td></td></tr>\n");
		if (specialOfflst.get(0).getScanseeimg() != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(specialOfflst
						.get(0).getScanseeimg())) {
			emailTemplate
					.append("<tr><td><img height=\"40\" width=\"150\" src=\""
							+ specialOfflst.get(0).getScanseeimg()
							+ "\"/></td></tr>\n");
		}
		if (specialOfflst.get(0).getFacebookimg() != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(specialOfflst
						.get(0).getFacebookimg())) {
			emailTemplate
					.append("<tr><td><img height=\"50\" width=\"50\" src=\""
							+ specialOfflst.get(0).getFacebookimg() + "\"/>\n");
		}
		if (specialOfflst.get(0).getTwitterimg() != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(specialOfflst
						.get(0).getTwitterimg())) {
			emailTemplate.append("<img height=\"50\" width=\"50\" src=\""
					+ specialOfflst.get(0).getTwitterimg() + "\"/>");
		}
		if (specialOfflst.get(0).getArrowscanseeimg() != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(specialOfflst
						.get(0).getArrowscanseeimg())) {
			emailTemplate.append("<img height=\"52\" width=\"52\" src=\""
					+ specialOfflst.get(0).getArrowscanseeimg()
					+ "\"/></td></tr>\n");
		}
		emailTemplate
				.append("<tr><td><h4>Donating 50% of Profits to Higher Education.   </h4></td></tr>");
		emailTemplate
				.append("<tr><td>              more information at <a href=\"www.ScanSee.com\">www.scansee.com</a></td></tr>");
		emailTemplate.append("</table>");
		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");

		LOG.info("HTML Body" + emailTemplate.toString());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emailTemplate.toString();
	}

	// for geocode api

	public static String getGeocodePostalcode(String latlong) throws Exception {
		// url
		// =http://maps.googleapis.com/maps/api/geocode/xml?latlng=40.714224,-73.961452&sensor=true

		URL url = new URL(ApplicationConstants.GEOCODEURL + "latlng=" + latlong
				+ "&sensor=" + SENSOR);
		LOG.info("URL is : " + url);
		URLConnection conn = url.openConnection();
		ByteArrayOutputStream output = new ByteArrayOutputStream(1024);
		IOUtils.copy(conn.getInputStream(), output);
		output.close();
		String postalCode = null;
		JSONObject jsonObj = new JSONObject(output.toString());

		try {

			String Status = jsonObj.getString("status");
			if (Status.equalsIgnoreCase("OK")) {
				JSONArray Results = jsonObj.getJSONArray("results");
				JSONObject zero = Results.getJSONObject(0);
				JSONArray address_components = zero
						.getJSONArray("address_components");

				for (int i = 0; i < address_components.length(); i++) {
					JSONObject zero2 = address_components.getJSONObject(i);
					String long_name = zero2.getString("long_name");
					JSONArray mtypes = zero2.getJSONArray("types");
					String Type = mtypes.getString(0);

					if (!long_name.equals(null) || long_name.length() > 0
							|| long_name != "") {

						if (Type.equalsIgnoreCase("postal_code")) {
							postalCode = long_name;
						}
					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return postalCode;
	}

	/**
	 * Validate Email Id with regular expression
	 * 
	 * @param strEmailId
	 *            strEmailId for validation
	 * @return true valid strEmailId, false invalid strEmailId
	 */
	public static boolean validateSpecialChar(final String strValue) {
		boolean isValid = false;
		Pattern p = Pattern.compile("[^a-zA-Z0-9]");
		if (p.matcher(strValue).find()) {
			isValid = true;
		}
		return isValid;
	}

	/**
	 * To remove extra double quotes
	 * 
	 * @param str
	 * @return
	 */
	public static String removeDoubleQuotes(String str) {
		if (str != null) {
			int t = str.length();
			if (str.charAt(0) == '"')
				str = str.substring(1, t--);
			if (str.charAt(--t) == '"')
				str = str.substring(0, t);

		}
		return str;
	}

	/**
	 * A private constructor to avoid instantiating.
	 */

	private Utility() {

	}

	/**
	 * The method for forming HTML with retailer Info.
	 * 
	 * @param productDetail
	 *            product object.
	 * @param retailerDetail
	 *            retailer object
	 * @param userId
	 *            int parameter
	 * @return The email template.
	 * @throws NoSuchAlgorithmException
	 *             - Exception related to algorithm
	 * @throws NoSuchPaddingException
	 *             - Exception related to Padding
	 * @throws InvalidKeyException
	 *             - Exception related to InvalidKey
	 * @throws InvalidAlgorithmParameterException
	 *             - Exception related to InvalidAlgorithmParameter
	 * @throws InvalidKeySpecException
	 *             - Exception related to InvalidKeySpec
	 * @throws IllegalBlockSizeException
	 *             - Exception related to IllegalBlockSize
	 * @throws BadPaddingException
	 *             - Exception related to BadPaddings
	 */

	public static String formShareAppInfoHTML(List<RetailersDetails> arRetailersDetailsList) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, InvalidKeySpecException, IllegalBlockSizeException, BadPaddingException {
		final String methodName = "formShareAppInfoHTML";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final StringBuffer emailTemplate = new StringBuffer("<html><head></head>");
		emailTemplate.append("<body> <h4> "	+ ApplicationConstants.APPSITE_HEADING + "</h4>\n");
		emailTemplate.append("<table border=\"0\">");
		emailTemplate.append("<tr><td width=\"300\" rowspan=\"7\">");

		if (arRetailersDetailsList.get(0).getRetImage() != null	&& !ApplicationConstants.NOTAPPLICABLE.equals(arRetailersDetailsList.get(0).getRetImage())) {
			emailTemplate.append("<img height=\"250\" width=\"250\" src=\""	+ arRetailersDetailsList.get(0).getRetImage() + "\"/></tr>");
		}
		emailTemplate.append("<tr><td width=\"300\" height=\"36\"><b>Retailer Name:</b> " + arRetailersDetailsList.get(0).getRetName() + "</td></tr>");
		emailTemplate.append("<tr><td width=\"300\" height=\"36\"><b>Address:</b> "	+ arRetailersDetailsList.get(0).getRetailerAddress()
						+ "," + arRetailersDetailsList.get(0).getCity() + "," + arRetailersDetailsList.get(0).getState() + ","
						+ arRetailersDetailsList.get(0).getPostalCode()	+ "</td></tr>");
		emailTemplate.append("<tr><td width=\"300\" height=\"36\"><b>Visit:</b> " + arRetailersDetailsList.get(0).getQrUrl() + "</td></tr>");
		emailTemplate.append("</table>");

		emailTemplate.append("<h4>Check out  <a href=\"www.ScanSee.com\">www.ScanSee.com</a> for more.</h4>\n");
		emailTemplate.append("</body>");
		emailTemplate.append("</html>\n");

		LOG.info("HTML Body" + emailTemplate.toString());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emailTemplate.toString();
	}

}