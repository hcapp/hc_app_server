package com.scansee.common.pojos;

import java.util.List;

/**
 * The POJO class for Categories.
 * 
 * @author shyamsundara_hm
 */

public class Categories extends BaseObject
{
	/**
	 * Variable userId declared as Integer.
	 */
	private Integer userId;
	/**
	 * Variable category declared as List.
	 */
	private List<Category> categoryInfo;
	
	/**
	 * For GooleCategory list
	 */
	private List<GoogleCategory> catList;
	
	/**
	 * For mainMenuID
	 */
	private Integer mainMenuID;
	
	/**
	 * Gets the value of the userId property.
	 * 
	 * @return the userId
	 */
	public Integer getUserId()
	{
		return userId;
	}
	/**
	 * Sets the value of the userId property.
	 * 
	 * @param userId
	 *            as of type Integer.
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	/**
	 * @return the categoryInfo
	 */
	public List<Category> getCategoryInfo()
	{
		return categoryInfo;
	}
	/**
	 * @param categoryInfo the categoryInfo to set
	 */
	public void setCategoryInfo(List<Category> categoryInfo)
	{
		this.categoryInfo = categoryInfo;
	}
	
	/**
	 * To get {@link GoogleCategory} list
	 * @return list of {@link GoogleCategory} catList object
	 */
	public List<GoogleCategory> getCatList() {
		return catList;
	}
	
	/**
	 * To set {@link GoogleCategory} list
	 * @param catList
	 */
	public void setCatList(List<GoogleCategory> catList) {
		this.catList = catList;
	}

	public Integer getMainMenuID() {
		return mainMenuID;
	}
	public void setMainMenuID(Integer mainMenuID) {
		this.mainMenuID = mainMenuID;
	}
}
