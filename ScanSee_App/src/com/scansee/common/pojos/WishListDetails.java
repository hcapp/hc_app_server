package com.scansee.common.pojos;

import java.util.Date;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.util.Utility;

/**
 * The POJO class for WishListDetails.
 * @author Syamsundhar_hm
 */
public class WishListDetails extends BaseObject
{
	
	/**
	 * Variable userProductId declared as Integer.
	 */
	private Integer userProductId;
	/**
	 * Variable productId declared as Integer.
	 */
	private Integer productId;
	/**
	 * Variable productName declared as String.
	 */
	private String productName;
	/**
	 * Variable productDescription declared as String.
	 */
	private String productDescription;
	/**
	 * Variable productShortDescription declared as String.
	 */
	private String productShortDescription;
	/**
	 * Variable productLongDescription declared as String.
	 */
	private String productLongDescription;
	/**
	 * Variable manufacturersName declared as String.
	 */
	private String manufacturersName;
	/**
	 * Variable imagePath declared as String.
	 */
	private String imagePath;
	/**
	 * Variable modelNumber declared as String.
	 */
	private String modelNumber;
	/**
	 * Variable suggestedRetailPrice declared as String.
	 */
	private String suggestedRetailPrice;
	/**
	 * Variable productWeight declared as String.
	 */
	private String productWeight;
	/**
	 * Variable productExpDate declared as Date.
	 */
	private Date productExpDate;
	/**
	 * Variable weightUnits declared as String.
	 */
	private String weightUnits;
	/**
	 * Variable rowNumber declared as Integer.
	 */
	private Integer rowNumber;
	/**
	 * Variable rebateId declared as Integer.
	 */
	private Integer rebateId;
	/**
	 * Variable deviceId declared as Integer.
	 */
	private Integer deviceId;
	/**
	 * Variable scanLongitude declared as Double.
	 */
	private Double scanLongitude;
	/**
	 * Variable ScanLatitude declared as Double.
	 */
	private Double ScanLatitude;
	/**
	 * Variable FieldAgentRequest declared as Integer.
	 */
	private Integer FieldAgentRequest;
	/**
	 * Variable ScanTypeID declared as Integer.
	 */
	private Integer ScanTypeID;
	/**
	 * Variable coupons declared as Integer.
	 */
	private Integer coupons;
	/**
	 * Variable rebates declared as Integer.
	 */
	private Integer rebates;
	/**
	 * Variable loyalties declared as Integer.
	 */
	private Integer loyalties;
	/**
	 * Variable coupon_Status declared as String.
	 */
	private String coupon_Status;
	/**
	 * Variable rebate_Status declared as String.
	 */
	private String rebate_Status;
	/**
	 * Variable loyalty_Status declared as String.
	 */
	private String loyalty_Status;
	/**
	 * Variable ShopCartItem declared as Integer.
	 */
	private Integer ShopCartItem;
	/**
	 * Variable ShopListItem declared as Integer.
	 */
	private Integer ShopListItem;
	/**
	 * Variable usage declared as String.
	 */
	// this is for click on add/ del shopping cart represent usage
	private String usage;
	/**
	 * Variable productVideoUrl declared as String.
	 */
	private String productVideoUrl;
	
	/**
	 * product price is used for findnearbyRetailer method.
	 */
	private double productPrice;
	
	/**
	 * Distance is used for findnearbyRetailer method.
	 */
	private Integer distance;

	/**
	 * gets the value of userProductId property.
	 * @return the userProductId
	 */
	public Integer getUserProductId()
	{
		return userProductId;
	}

	/**
	 * Sets the value of userProductId property.
	 * @param userProductId
	 *            the userProductId to set
	 */
	public void setUserProductId(Integer userProductId)
	{
		this.userProductId = userProductId;
	}

	/**
	 *  gets the value of userProductId property.
	 * @return the shopCartItem
	 */
	public Integer getShopCartItem()
	{
		return ShopCartItem;
	}

	/**
	 * Sets the value of shopCartItem property.
	 * @param shopCartItem
	 *            the shopCartItem to set
	 */
	public void setShopCartItem(Integer shopCartItem)
	{
		ShopCartItem = shopCartItem;
	}

	/**
	 *  gets the value of userProductId property.
	 * @return the shopListItem
	 */
	public Integer getShopListItem()
	{
		return ShopListItem;
	}

	/**
	 * Sets the value of shopListItem property.
	 * @param shopListItem
	 *            the shopListItem to set
	 */
	public void setShopListItem(Integer shopListItem)
	{
		ShopListItem = shopListItem;
	}

	/**
	 *  gets the value of userProductId property.
	 * @return the distance
	 */
	public Integer getDistance()
	{
		return distance;
	}

	/**
	 * Sets the value of distance property.
	 * @param distance
	 *            the distance to set
	 */
	public void setDistance(Integer distance)
	{
		this.distance = distance;
	}

	

	/**
	 *  gets the value of productPrice property.
	 * @return the productPrice
	 */
	public double getProductPrice()
	{
		return productPrice;
	}

	/**
	 * Sets the value of productPrice property.
	 * @param productPrice
	 *            the productPrice to set
	 */
	public void setProductPrice(double productPrice)
	{
		this.productPrice = productPrice;
	}

	/**
	 * Gets the value of the ScanTypeID property.
	 * 
	 * @return the ScanTypeID
	 */
	public Integer getScanTypeID()
	{
		return ScanTypeID;
	}

	/**
	 * Sets the value of the scanTypeID property.
	 * 
	 * @param scanTypeID
	 *            as of type Integer.
	 */
	public void setScanTypeID(Integer scanTypeID)
	{
		ScanTypeID = scanTypeID;
	}

	/**
	 * Gets the value of the productWeight property.
	 * 
	 * @return the productWeight
	 */
	public String getProductWeight()
	{
		return productWeight;
	}

	/**
	 * Sets the value of the productWeight property.
	 * 
	 * @param productWeight
	 *            as of type String.
	 */
	public void setProductWeight(String productWeight)
	{
		this.productWeight = productWeight;
	}

	/**
	 * Gets the value of the rebateId property.
	 * 
	 * @return the rebateId
	 */
	public Integer getRebateId()
	{
		return rebateId;
	}

	/**
	 * Sets the value of the rebateId property.
	 * 
	 * @param rebateId
	 *            as of type Integer.
	 */
	public void setRebateId(Integer rebateId)
	{
		this.rebateId = rebateId;
	}

	/**
	 * Gets the value of the deviceId property.
	 * 
	 * @return the deviceId
	 */
	public Integer getDeviceId()
	{
		return deviceId;
	}

	/**
	 * Sets the value of the deviceId property.
	 * 
	 * @param deviceId
	 *            as of type Integer.
	 */
	public void setDeviceId(Integer deviceId)
	{
		this.deviceId = deviceId;
	}

	/**
	 * Gets the value of the scanLongitude property.
	 * 
	 * @return the scanLongitude
	 */
	public Double getScanLongitude()
	{
		return scanLongitude;
	}

	/**
	 * Sets the value of the scanLongitude property.
	 * 
	 * @param scanLongitude
	 *            as of type Double.
	 */
	public void setScanLongitude(Double scanLongitude)
	{
		this.scanLongitude = scanLongitude;
	}

	/**
	 * Gets the value of the ScanLatitude property.
	 * 
	 * @return the ScanLatitude
	 */
	public Double getScanLatitude()
	{
		return ScanLatitude;
	}

	/**
	 * Sets the value of the scanLatitude property.
	 * 
	 * @param scanLatitude
	 *            as of type Double.
	 */
	public void setScanLatitude(Double scanLatitude)
	{
		ScanLatitude = scanLatitude;
	}

	/**
	 * Gets the value of the FieldAgentRequest property.
	 * 
	 * @return the FieldAgentRequest
	 */
	public Integer getFieldAgentRequest()
	{
		return FieldAgentRequest;
	}

	/**
	 * Sets the value of the fieldAgentRequest property.
	 * 
	 * @param fieldAgentRequest
	 *            as of type Integer.
	 */
	public void setFieldAgentRequest(Integer fieldAgentRequest)
	{
		FieldAgentRequest = fieldAgentRequest;
	}

	/**
	 *  gets the value of rowNumber property.
	 * @return the rowNumber
	 */
	public Integer getRowNumber()
	{
		return rowNumber;
	}

	/**
	 * Sets the value of the rowNumber property.
	 * @param rowNumber
	 *            the rowNumber to set
	 */
	public void setRowNumber(Integer rowNumber)
	{
		this.rowNumber = rowNumber;
	}

	/**
	 *  gets the value of productDescription property.
	 * @return the productDescription
	 */
	public String getProductDescription()
	{
		return productDescription;
	}

	/**
	 * Sets the value of the productDescription property.
	 * @param productDescription
	 *            the productDescription to set
	 */
	public void setProductDescription(String productDescription)
	{
		this.productDescription = productDescription;
	}

	/**
	 *  gets the value of productId property.
	 * @return the productId
	 */
	public Integer getProductId()
	{
		return productId;
	}

	/**
	 * Sets the value of the productId property.
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 *  gets the value of productName property.
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * Sets the value of the productName property.
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 *  gets the value of manufacturersName property.
	 * @return the manufacturersName
	 */
	public String getManufacturersName()
	{
		return manufacturersName;
	}

	/**
	 * Sets the value of the imagePath property.
	 * @param manufacturersName
	 *            the manufacturersName to set
	 */
	public void setManufacturersName(String manufacturersName)
	{
		this.manufacturersName = manufacturersName;
	}

	/**
	 *  gets the value of imagePath property.
	 * @return the imagePath
	 */
	public String getImagePath()
	{
		return imagePath;
	}

	/**
	 * Sets the value of the modelNumber property.
	 * @param imagePath
	 *            the imagePath to set
	 */
	public void setImagePath(String imagePath)
	{
		if (null == imagePath)
		{
			this.imagePath = ApplicationConstants.IMAGENOTFOUND;
		}
		else
		{
			this.imagePath = imagePath;
		}

	}

	/**
	 *  gets the value of modelNumber property.
	 * @return the modelNumber
	 */
	public String getModelNumber()
	{
		return modelNumber;
	}

	/**
	 * Sets the value of the fieldAgentRequest property.
	 * @param modelNumber
	 *            the modelNumber to set
	 */
	public void setModelNumber(String modelNumber)
	{
		if (null == modelNumber)
		{
			this.modelNumber = "N/A";
		}
		else
		{
			this.modelNumber = modelNumber;
		}

	}

	/**
	 *  gets the value of suggestedRetailPrice property.
	 * @return the suggestedRetailPrice
	 */
	public String getSuggestedRetailPrice()
	{
		return suggestedRetailPrice;
	}

	/**
	 * Sets the value of the suggestedRetailPrice property.
	 * @param suggestedRetailPrice
	 *            the suggestedRetailPrice to set
	 */
	public void setSuggestedRetailPrice(String suggestedRetailPrice)
	{
		if (null == suggestedRetailPrice)
		{
			this.suggestedRetailPrice = "N/A";
		}
		else
		{
			this.suggestedRetailPrice = suggestedRetailPrice;
		}

	}

	/**
	 *  gets the value of productWeight property.
	 * @return the weight
	 */
	public String getWeight()
	{
		return productWeight;
	}

	/**
	 * Sets the value of the weight property.
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(String weight)
	{
		if (null == productWeight)
		{
			this.productWeight = "N/A";
		}
		else
		{
			this.productWeight = weight;
		}

	}

	/**
	 *  gets the value of productExpDate property.
	 * @return the productExpDate
	 */
	public Date getProductExpDate()
	{
		return productExpDate;
	}

	/**
	 * Sets the value of the productExpDate property.
	 * @param productExpDate
	 *            the productExpDate to set
	 */
	public void setProductExpDate(Date productExpDate)
	{
		this.productExpDate = productExpDate;

	}

	/**
	 *  gets the value of weightUnits property.
	 * @return the weightUnits
	 */
	public String getWeightUnits()
	{
		return weightUnits;
	}

	/**
	 * Sets the value of the weightUnits property.
	 * @param weightUnits
	 *            the weightUnits to set
	 */
	public void setWeightUnits(String weightUnits)
	{
		if (null == weightUnits)
		{
			this.weightUnits = "N/A";
		}
		else
		{
			this.weightUnits = weightUnits;
		}

	}

	/**
	 *  gets the value of coupons property.
	 * @return the coupons
	 */
	public Integer getCoupons()
	{
		return coupons;
	}

	/**
	 * Sets the value of the coupons property.
	 * @param coupons
	 *            the coupons to set
	 */
	public void setCoupons(Integer coupons)
	{
		this.coupons = coupons;
	}

	/**
	 *  gets the value of rebates property.
	 * @return the rebates
	 */
	public Integer getRebates()
	{
		return rebates;
	}

	/**
	 * Sets the value of the rebates property.
	 * @param rebates
	 *            the rebates to set
	 */
	public void setRebates(Integer rebates)
	{
		this.rebates = rebates;
	}

	/**
	 *  gets the value of loyalties property.
	 * @return the loyalties
	 */
	public Integer getLoyalties()
	{
		return loyalties;
	}

	/**
	 * Sets the value of the loyalties property.
	 * @param loyalties
	 *            the loyalties to set
	 */
	public void setLoyalties(Integer loyalties)
	{
		this.loyalties = loyalties;
	}

	/**
	 *  gets the value of coupon_Status property.
	 * @return the coupon_Status
	 */
	public String getCoupon_Status()
	{
		return coupon_Status;
	}

	/**
	 * Sets the value of the coupon_Status property.
	 * @param couponStatus
	 *            the coupon_Status to set
	 */
	public void setCoupon_Status(String couponStatus)
	{
		coupon_Status = couponStatus;
	}

	/**
	 *  gets the value of rebate_Status property.
	 * @return the rebate_Status
	 */
	public String getRebate_Status()
	{
		return rebate_Status;
	}

	/**
	 * Sets the value of the rebateStatus property.
	 * @param rebateStatus
	 *            the rebate_Status to set
	 */
	public void setRebate_Status(String rebateStatus)
	{
		rebate_Status = rebateStatus;
	}

	/**
	 *  gets the value of loyalty_Status property.
	 * @return the loyalty_Status
	 */
	public String getLoyalty_Status()
	{
		return loyalty_Status;
	}

	/**
	 * Sets the value of the loyaltyStatus property.
	 * @param loyaltyStatus
	 *            the loyalty_Status to set
	 */
	public void setLoyalty_Status(String loyaltyStatus)
	{
		loyalty_Status = loyaltyStatus;
	}

	/**
	 *  gets the value of productShortDescription property.
	 * @return productShortDescription The short description for product.
	 */

	public String getProductShortDescription()
	{
		return productShortDescription;
	}

	/**
	 * Sets the value of the productLongDescription property.
	 * @param productShortDescription
	 *            The short description for product.
	 */

	public void setProductShortDescription(String productShortDescription)
	{
		productShortDescription = Utility.removeHTMLTags(productShortDescription);
		this.productShortDescription = productShortDescription;
	}

	/**
	 *  gets the value of productLongDescription property.
	 * @return productLongDescription The long description for product.
	 */

	public String getProductLongDescription()
	{
		return productLongDescription;
	}

	/**
	 * Sets the value of the productLongDescription property.
	 * @param productLongDescription
	 *            The long description for product.
	 */

	public void setProductLongDescription(String productLongDescription)
	{
		productLongDescription = Utility.removeHTMLTags(productLongDescription);
		this.productLongDescription = productLongDescription;
	}

	/**
	 *  gets the value of usage property.
	 * @return the usage
	 */
	public String getUsage()
	{
		return usage;
	}

	/**
	 * Sets the value of the usage property.
	 * @param usage
	 *            the usage to set
	 */
	public void setUsage(String usage)
	{
		this.usage = usage;
	}

	/**
	 *  gets the value of productVideoUrl property.
	 * @return productVideoUrl The url path for productVideo
	 */

	public String getProductVideoUrl()
	{
		return productVideoUrl;
	}

	/**
	 * Sets the value of the productVideoUrl property.
	 * @param productVideoUrl
	 *            The url path for productVideo
	 */

	public void setProductVideoUrl(String productVideoUrl)
	{
		this.productVideoUrl = productVideoUrl;
	}

}
