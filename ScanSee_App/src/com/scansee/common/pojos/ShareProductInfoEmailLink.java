package com.scansee.common.pojos;

import com.scansee.common.constants.ApplicationConstants;

/**
 * The ShareProductInfoEmailLink contains setter and getter methods for
 * ShareProductInfoEmailLink fields.
 * 
 * @author dileepa_cc
 */
public class ShareProductInfoEmailLink
{
	/**
	 * for productName.
	 */
	private String productName;
	/**
	 * for emailTemplateURL.
	 */
	private String emailTemplateURL;
	/**
	 * for imagePath.
	 */
	private String imagePath;

	/**
	 * To get emailTemplateURL.
	 * 
	 * @return the emailTemplateURL
	 */
	public String getEmailTemplateURL()
	{
		return emailTemplateURL;
	}

	/**
	 * To set emailTemplateURL.
	 * 
	 * @param emailTemplateURL
	 *            the emailTemplateURL to set
	 */
	public void setEmailTemplateURL(String emailTemplateURL)
	{
		this.emailTemplateURL = emailTemplateURL;
	}

	/**
	 * To get imagePath.
	 * 
	 * @return the imagePath
	 */
	public String getImagePath()
	{
		return imagePath;
	}

	/**
	 * To set imagePath.
	 * 
	 * @param imagePath
	 *            the imagePath to set
	 */
	public void setImagePath(String imagePath)
	{
		if (imagePath == null)
		{
			this.imagePath = ApplicationConstants.IMAGENOTFOUND;
		}
		else
		{
			this.imagePath = imagePath;
		}
	}

	/**
	 * To get productName.
	 * 
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * To set productName.
	 * 
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

}
