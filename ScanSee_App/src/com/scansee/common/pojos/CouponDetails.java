package com.scansee.common.pojos;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.util.Utility;

/**
 * The class has getter and setter methods for CouponDetails.
 * 
 * @author sowjanya_d
 */
public class CouponDetails extends BaseObject
{

	/**
	 * for ViewableOnWeb to check coupon is added from web
	 */
//	private boolean viewableOnWeb;	
	private Boolean viewableOnWeb;
	/**
	 * coupon CouponTermsAndConditions
	 */
	private String termsAndConditions;
	/**
	 * for used flag
	 */
	private Integer usedFlag;
	/**
	 * for user gallery coupon id
	 */
	private Integer userCouponGalleryID;
	/**
	 * for coupon share info through twitter
	 */
	private String titleText;

	/**
	 * for coupon share info
	 */
	private String titleText2;

	/**
	 * for userId
	 */
	private Integer userId;
	/**
	 * for user coupon claim type id
	 */
	private Integer userCouponClaimTypeID;
	/**
	 * for CouponPayoutMethod
	 */
	private String couponPayoutMethod;
	/**
	 * for RetailLocationID
	 */
	private Integer retailLocationID;
	/**
	 * for CouponClaimDate
	 */
	private String couponClaimDate;
	/**
	 * for coupon url
	 */

	private String couponURL;
	/**
	 * for coupon expired or not
	 */
	private Integer couponExpired;
	/**
	 * for product Id.
	 */
	private Integer productId;
	/**
	 * for coupon image path.
	 */
	private String imagePath;
	/**
	 * for product name.
	 */
	private String productName;
	/**
	 * for rowNum.
	 * 
	 * @return the rowNum
	 */
	private Integer rowNum;

	/**
	 * Variable couponId declared as of type Integer.
	 */
	private Integer couponId;
	/**
	 * Variable couponName declared as of type String.
	 */
	private String couponName;
	/**
	 * Variable couponDiscountType declared as of type String.
	 */
	private String couponDiscountType;
	/**
	 * Variable couponDiscountAmount declared as of type String.
	 */
	private String couponDiscountAmount;
	/**
	 * Variable couponDiscountPct declared as of type String.
	 */
	private String couponDiscountPct;
	/**
	 * Variable couponDescription declared as of type String.
	 */
	private String couponShortDescription;
	/**
	 * Variable couponDescription declared as of type String.
	 */
	private String couponLongDescription;
	/**
	 * Variable couponDateAdded declared as of type String.
	 */
	private String couponDateAdded;
	/**
	 * Variable couponStartDate declared as of type String.
	 */
	private String couponStartDate;
	/**
	 * Variable couponExpireDate declared as of type String.
	 */
	private String couponExpireDate;
	/**
	 * Variable usage declared as of type String.
	 */
	private String usage;

	/**
	 * Variable usage declared as of type String.
	 */
	private String couponImagePath;

	/**
	 * Variable added declared as of type String.
	 */
	private String added;
	/**
	 * for Fav_Flag
	 */
	private Boolean favFlag;
	/**
	 * for category id
	 */
	private Integer cateId;
	/**
	 * for category name
	 */
	private String cateName;
	/**
	 * for coupon description
	 */
	private String coupDesptn;
	
	/**
	 * Variable coupDateAdded declared as of type String.
	 *  
	 */
	private String coupDateAdded;
	
	/**
	 * Variable couponStartDate declared as of type String.
	 */
	private String coupStartDate;
	
	/**
	 * Variable couponExpireDate declared as of type String.
	 */
	private String coupExpireDate;
	
	/**
	 * for facebook image.
	 */
	private String facebookimg;

	/**
	 * for scansee image.
	 */
	private String scanseeimg;
	/**
	 * for twitter image
	 */
	private String twitterimg;
	/**
	 * arrow scansee image.
	 */
	private String arrowscanseeimg;
	
	/**
	 * For retailer list ID
	 */
	private Integer retListID;

	/**
	 * For coupon List ID
	 */
	private Integer couponListID;
	
	/**
	 * For coupon fav flag
	 */
	private Integer coupFavFlag;
	
	/**
	 * For Coupon expired flag
	 */
	private String expFlag;

	/**
	 * For BusinessCategoryID
	 */
	private Integer busCatId;
	
	/**
	 * For BusinessCategoryName
	 */ 
	private String busCatName;
	
	/**
	 * For Category ID
	 */
	private Integer catId;
	
	/**
	 * For Categoty Name
	 */
	private String catName;
	
	/**
	 * For RetailerID
	 */
	private Integer retId;
	
	/**
	 * For Retailer Name
	 */
	private String retName;
	
	/**
	 * For RetailLocationID
	 */
	private Integer retLocId;
	
	/**
	 * For address
	 */
	private String address;
	
	/**
	 * For city
	 */
	private String city;
	
	/**
	 * For state
	 */
	private String state;
	
	/**
	 * For postalCode
	 */
	private String postalCode;
	
	/**
	 * For coupon description
	 */
	private String coupDesc;
	
	/**
	 * For distance
	 */
	private Float distance;
	
	/**
	 * For claim flag
	 */
	private Integer claimFlag;
	
	/**
	 * For redeem flag
	 */
	private Integer redeemFlag;
	
	/**
	 * For new flag
	 */
	private Integer newFlag;
	
	/**
	 * For used count flag
	 */
	private Integer used;
	
	/**
	 * Number of coupon to issue
	 */
	private Integer coupToIssue;
	
	/**
	 * For Population center ID
	 */
	private Integer popCentId;
	
	/**
	 * For DMA name
	 */
	private String dmaName;
	
	/**
	 * For Coupon Count
	 */
	private Integer coupCount;
	/**
	 * for Fav_Flag
	 */
	private Boolean  productFlag;
	/**
	 * for Fav_Flag
	 */
	private Boolean locationFlag;
	
	/**
	 * For Population center ID
	 */
	private Integer coupProductFlag;
	/**
	 * For Population center ID
	 */
	private Integer coupLocationFlag;
	
	private Integer userCoupGallId;
	
	private Integer rowNumber;
	
	private Boolean isDateFormated;

	/**
	 * @return the couponImagePath
	 */
	public String getCouponImagePath()
	{
		return couponImagePath;
	}

	/**
	 * @param couponImagePath
	 *            the couponImagePath to set
	 */
	public void setCouponImagePath(String couponImagePath)
	{

		if (couponImagePath == null || "".equals(couponImagePath))
		{
			this.couponImagePath = ApplicationConstants.IMAGENOTFOUND;
		}
		else
		{
			this.couponImagePath = couponImagePath;
		}
	}

	/**
	 * Gets the value of the couponId property.
	 * 
	 * @return possible object is {@link Integer }
	 */
	public Integer getCouponId()
	{
		return couponId;
	}

	/**
	 * Sets the value of the couponId property.
	 * 
	 * @param couponId
	 *            as of type Integer.
	 */
	public void setCouponId(Integer couponId)
	{
		this.couponId = couponId;
	}

	/**
	 * Gets the value of the couponName property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponName()
	{
		return couponName;
	}

	/**
	 * Sets the value of the couponName property.
	 * 
	 * @param couponName
	 *            as of type String.
	 */
	public void setCouponName(String couponName)
	{
		if (couponName == null)
		{
			this.couponName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.couponName = couponName;
		}
	}

	/**
	 * Gets the value of the couponDiscountType property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponDiscountType()
	{
		return couponDiscountType;
	}

	/**
	 * Sets the value of the couponDiscountType property.
	 * 
	 * @param couponDiscountType
	 *            as of type String.
	 */
	public void setCouponDiscountType(String couponDiscountType)
	{
		if (null == couponDiscountType || "".equals(couponDiscountType))
		{
			this.couponDiscountType = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.couponDiscountType = couponDiscountType;
		}
	}

	/**
	 * Gets the value of the couponDiscountAmount property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponDiscountAmount()
	{
		return couponDiscountAmount;
	}

	/**
	 * Sets the value of the couponDiscountAmount property.
	 * 
	 * @param couponDiscountAmount
	 *            as of type String.
	 */
	public void setCouponDiscountAmount(String couponDiscountAmount)
	{
		if (couponDiscountAmount == null)
		{
			this.couponDiscountAmount = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{

			if (!couponDiscountAmount.contains("$") && !ApplicationConstants.NOTAPPLICABLE.equals(couponDiscountAmount))
			{
				// this.couponDiscountAmount =
				// Utility.formatDecimalValue(couponDiscountAmount);
				this.couponDiscountAmount = "$" + Utility.formatDecimalValue(couponDiscountAmount);
			}
			else
			{
				this.couponDiscountAmount = couponDiscountAmount;
			}
		}

	}

	/**
	 * Gets the value of the couponDiscountPct property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponDiscountPct()
	{
		return couponDiscountPct;
	}

	/**
	 * Sets the value of the couponDiscountPct property.
	 * 
	 * @param couponDiscountPct
	 *            as of type String.
	 */
	public void setCouponDiscountPct(String couponDiscountPct)
	{
		if (couponDiscountPct == null)
		{
			this.couponDiscountPct = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.couponDiscountPct = couponDiscountPct;
		}
	}

	/**
	 * Gets the value of the couponDateAdded property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponDateAdded()
	{
		return couponDateAdded;
	}

	/**
	 * Sets the value of the couponDateAdded property.
	 * 
	 * @param couponDateAdded
	 *            as of type String.
	 */
	public void setCouponDateAdded(String couponDateAdded)
	{
		if (couponDateAdded != null)
		{

			this.couponDateAdded = couponDateAdded;
			this.couponDateAdded = Utility.convertDBdate(couponDateAdded);
		}
		else
		{
			this.couponDateAdded = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	/**
	 * Gets the value of the couponStartDate property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponStartDate()
	{
		return couponStartDate;
	}

	/**
	 * Sets the value of the couponStartDate property.
	 * 
	 * @param couponStartDate
	 *            as of type String.
	 */
	public void setCouponStartDate(String couponStartDate)
	{

		if (couponStartDate != null)
		{

			if(null != isDateFormated && isDateFormated == true)	{
				this.couponStartDate = couponStartDate;
			}
			else	{
				this.couponStartDate = couponStartDate;
				this.couponStartDate = Utility.convertDBdate(couponStartDate);
			}

		}
		else
		{
			this.couponStartDate = ApplicationConstants.CLSTARTDATETEXT;
		}

	}

	/**
	 * Gets the value of the couponExpireDate property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getCouponExpireDate()
	{
		return couponExpireDate;
	}

	/**
	 * Sets the value of the couponExpireDate property.
	 * 
	 * @param couponExpireDate
	 *            as of type String.
	 */
	public void setCouponExpireDate(String couponExpireDate)
	{
		if (couponExpireDate != null)
		{

			if(null != isDateFormated && isDateFormated == true)	{
				this.couponExpireDate = couponExpireDate;
			}
			else	{
				this.couponExpireDate = couponExpireDate;
				this.couponExpireDate = Utility.convertDBdate(couponExpireDate);
			}

		}
		else
		{
			this.couponExpireDate = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	/**
	 * Gets the value of the usage property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getUsage()
	{
		return usage;
	}

	/**
	 * Sets the value of the usage property.
	 * 
	 * @param usage
	 *            as of type String.
	 */
	public void setUsage(String usage)
	{
		if (usage == null)
		{
			this.usage = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.usage = usage;
		}
	}

	/**
	 * @return the couponShortDescription
	 */
	public String getCouponShortDescription()
	{
		return couponShortDescription;
	}

	/**
	 * @param couponShortDescription
	 *            the couponShortDescription to set
	 */
	public void setCouponShortDescription(String couponShortDescription)
	{
		if (null == couponShortDescription || "".equalsIgnoreCase(couponShortDescription))
		{
			this.couponShortDescription = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.couponShortDescription = couponShortDescription;
		}

	}

	/**
	 * @return the couponLongDescription
	 */
	public String getCouponLongDescription()
	{
		return couponLongDescription;
	}

	/**
	 * @param couponLongDescription
	 *            the couponLongDescription to set
	 */
	public void setCouponLongDescription(String couponLongDescription)
	{
		if (null == couponLongDescription || "null".equals(couponLongDescription))
		{
			this.couponLongDescription = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.couponLongDescription = couponLongDescription;
		}
	}

	/**
	 * to fetch rowNum.
	 * 
	 * @return the rowNum
	 */
	public Integer getRowNum()
	{
		return rowNum;
	}

	/**
	 * to set rowNum.
	 * 
	 * @param rowNum
	 *            the rowNum to set
	 */
	public void setRowNum(Integer rowNum)
	{
		this.rowNum = rowNum;
	}

	/**
	 * to fetch added.
	 * 
	 * @return the added
	 */
	public String getAdded()
	{
		return added;
	}

	/**
	 * to set added.
	 * 
	 * @param added
	 *            the added to set
	 */
	public void setAdded(String added)
	{
		this.added = added;
	}

	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath()
	{
		return imagePath;
	}

	/**
	 * @param imagePath
	 *            the imagePath to set
	 */
	public void setImagePath(String imagePath)
	{
		if (imagePath == null || "".equals(imagePath))
		{

			this.imagePath = ApplicationConstants.IMAGENOTFOUND;
		}
		else
		{
			this.imagePath = imagePath;
		}
	}

	public Integer getProductId()
	{
		return productId;
	}

	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	public Integer getCouponExpired()
	{
		return couponExpired;
	}

	public void setCouponExpired(Integer couponExpired)
	{
		this.couponExpired = couponExpired;
	}

	public String getCouponURL()
	{
		return couponURL;
	}

	public void setCouponURL(String couponURL)
	{
		if (couponURL == null || "".equals(couponURL))
		{
			this.couponURL = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.couponURL = couponURL;
		}
	}

	public Integer getUserId()
	{
		return userId;
	}

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	public Integer getUserCouponClaimTypeID()
	{
		return userCouponClaimTypeID;
	}

	public void setUserCouponClaimTypeID(Integer userCouponClaimTypeID)
	{
		this.userCouponClaimTypeID = userCouponClaimTypeID;
	}

	public String getCouponPayoutMethod()
	{
		return couponPayoutMethod;
	}

	public void setCouponPayoutMethod(String couponPayoutMethod)
	{
		this.couponPayoutMethod = couponPayoutMethod;
	}

	public Integer getRetailLocationID()
	{
		return retailLocationID;
	}

	public void setRetailLocationID(Integer retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}

	public String getCouponClaimDate()
	{
		return couponClaimDate;
	}

	public void setCouponClaimDate(String couponClaimDate)
	{
		this.couponClaimDate = couponClaimDate;
	}

	public String getTitleText()
	{
		return titleText;
	}

	public void setTitleText(String titleText)
	{
		this.titleText = titleText;
	}

	public String getTitleText2()
	{
		return titleText2;
	}

	public void setTitleText2(String titleText2)
	{
		this.titleText2 = titleText2;
	}

	public Integer getUserCouponGalleryID()
	{
		return userCouponGalleryID;
	}

	public void setUserCouponGalleryID(Integer userCouponGalleryID)
	{
		this.userCouponGalleryID = userCouponGalleryID;
	}

	public Integer getUsedFlag()
	{
		return usedFlag;
	}

	public void setUsedFlag(Integer usedFlag)
	{
		this.usedFlag = usedFlag;
	}

	public String getTermsAndConditions()
	{
		return termsAndConditions;
	}

	public void setTermsAndConditions(String termsAndConditions)
	{

		if (null == termsAndConditions || "".equalsIgnoreCase(termsAndConditions))
		{
			this.termsAndConditions = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.termsAndConditions = termsAndConditions;
		}
	}

//	public boolean isViewableOnWeb()
//	{
//		return viewableOnWeb;
//	}

//	public void setViewableOnWeb(boolean viewableOnWeb)
//	{
//		this.viewableOnWeb = viewableOnWeb;
//	}
	
	public Boolean isViewableOnWeb()
	{
		return viewableOnWeb;
	}
	
	public void setViewableOnWeb(Boolean viewableOnWeb)
	{
		if(viewableOnWeb != null)	{
			this.viewableOnWeb = viewableOnWeb;
		}
		else	{
			this.viewableOnWeb = false;
		}
	}

	public Boolean getFavFlag()
	{
		return favFlag;
	}

	public void setFavFlag(Boolean favFlag)
	{
		this.favFlag = favFlag;
	}

	public Integer getCateId()
	{
		return cateId;
	}

	public void setCateId(Integer cateId)
	{
		this.cateId = cateId;
	}

	public String getCateName()
	{
		return cateName;
	}

	public void setCateName(String cateName)
	{
		this.cateName = cateName;
	}

	public String getCoupDesptn()
	{
		return coupDesptn;
	}

	public void setCoupDesptn(String coupDesptn)
	{
		if (coupDesptn == null || "".equals(coupDesptn))
		{
			this.coupDesptn = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.coupDesptn = coupDesptn;
		}
	}

	/**
	 * To get coupDate Added
	 * @return coupDateAdded
	 */
	public String getCoupDateAdded() {
		return coupDateAdded;
	}

	/**
	 * to set coupDateAdded
	 * @param coupDateAdded
	 */
	public void setCoupDateAdded(String coupDateAdded) {
		this.coupDateAdded = coupDateAdded;
	}
	
	/**
	 * To get coupStartDate
	 * @return String coupStartDate
	 */
	public String getCoupStartDate() {
		return coupStartDate;
	}

	/**
	 * To set String coupStartDate
	 * @param coupStartDate
	 */
	public void setCoupStartDate(String coupStartDate) {
		this.coupStartDate = coupStartDate;
	}

	/**
	 * To get coupExpireDate
	 * @return String coupExpireDate
	 */
	public String getCoupExpireDate() {
		return coupExpireDate;
	}

	/**
	 * To set String coupExpireDate
	 * @param coupExpireDate
	 */
	public void setCoupExpireDate(String coupExpireDate) {
		this.coupExpireDate = coupExpireDate;
	}

	public String getFacebookimg() {
		return facebookimg;
	}

	public void setFacebookimg(String facebookimg) {
		this.facebookimg = facebookimg;
	}

	public String getScanseeimg() {
		return scanseeimg;
	}

	public void setScanseeimg(String scanseeimg) {
		this.scanseeimg = scanseeimg;
	}

	public String getTwitterimg() {
		return twitterimg;
	}

	public void setTwitterimg(String twitterimg) {
		this.twitterimg = twitterimg;
	}

	public String getArrowscanseeimg() {
		return arrowscanseeimg;
	}

	public void setArrowscanseeimg(String arrowscanseeimg) {
		this.arrowscanseeimg = arrowscanseeimg;
	}
	
	public Integer getCouponListID() {
		return couponListID;
	}

	public void setCouponListID(Integer couponListID) {
		this.couponListID = couponListID;
	}
	
	public Integer getRetListID() {
		return retListID;
	}

	public void setRetListID(Integer retListID) {
		this.retListID = retListID;
	}
	
	public Integer getCoupFavFlag() {
		return coupFavFlag;
	}

	public void setCoupFavFlag(Integer coupFavFlag) {
		this.coupFavFlag = coupFavFlag;
	}
	
	public String getExpFlag() {
		return expFlag;
	}

	public void setExpFlag(String expFlag) {
		this.expFlag = expFlag;
	}

	public Integer getBusCatId() {
		return busCatId;
	}

	public void setBusCatId(Integer busCatId) {
		this.busCatId = busCatId;
	}

	public String getBusCatName() {
		return busCatName;
	}

	public void setBusCatName(String busCatName) {
		this.busCatName = busCatName;
	}

	public Integer getRetId() {
		return retId;
	}

	public void setRetId(Integer retId) {
		this.retId = retId;
	}

	public String getRetName() {
		return retName;
	}

	public void setRetName(String retName) {
		this.retName = retName;
	}

	public Integer getRetLocId() {
		return retLocId;
	}

	public void setRetLocId(Integer retLocId) {
		this.retLocId = retLocId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		if(null != address)	{
			this.address = address;
		}
		else	{
			this.address = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCoupDesc() {
		return coupDesc;
	}

	public void setCoupDesc(String coupDesc) {
		if(null != coupDesc)	{
			this.coupDesc = coupDesc;
		}
		else	{
			this.coupDesc = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	public Float getDistance() {
		return distance;
	}

	public void setDistance(Float distance) {
		this.distance = distance;
	}

	public Integer getClaimFlag() {
		return claimFlag;
	}

	public void setClaimFlag(Integer claimFlag) {
		this.claimFlag = claimFlag;
	}

	public Integer getRedeemFlag() {
		return redeemFlag;
	}

	public void setRedeemFlag(Integer redeemFlag) {
		this.redeemFlag = redeemFlag;
	}

	public Integer getNewFlag() {
		return newFlag;
	}

	public void setNewFlag(Integer newFlag) {
		this.newFlag = newFlag;
	}

	public Integer getUsed() {
		return used;
	}

	public void setUsed(Integer used) {
		this.used = used;
	}

	public Integer getCatId() {
		return catId;
	}

	public void setCatId(Integer catId) {
		this.catId = catId;
	}

	public String getCatName() {
		return catName;
	}

	public void setCatName(String catName) {
		this.catName = catName;
	}

	public Integer getCoupToIssue() {
		return coupToIssue;
	}

	public void setCoupToIssue(Integer coupToIssue) {
		this.coupToIssue = coupToIssue;
	}

	public Integer getPopCentId() {
		return popCentId;
	}

	public void setPopCentId(Integer popCentId) {
		this.popCentId = popCentId;
	}

	public String getDmaName() {
		return dmaName;
	}

	public void setDmaName(String dmaName) {
		this.dmaName = dmaName;
	}

	public Integer getCoupCount() {
		return coupCount;
	}

	public void setCoupCount(Integer coupCount) {
		this.coupCount = coupCount;
	}

	public Boolean getProductFlag() {
		return productFlag;
	}

	public void setProductFlag(Boolean productFlag) {
		this.productFlag = productFlag;
	}

	public Boolean getLocationFlag() {
		return locationFlag;
	}

	public void setLocationFlag(Boolean locationFlag) {
		this.locationFlag = locationFlag;
	}

	public Integer getCoupProductFlag() {
		return coupProductFlag;
	}

	public void setCoupProductFlag(Integer coupProductFlag) {
		this.coupProductFlag = coupProductFlag;
	}

	public Integer getCoupLocationFlag() {
		return coupLocationFlag;
	}

	public void setCoupLocationFlag(Integer coupLocationFlag) {
		this.coupLocationFlag = coupLocationFlag;
	}

	public Integer getUserCoupGallId() {
		return userCoupGallId;
	}

	public void setUserCoupGallId(Integer userCoupGallId) {
		this.userCoupGallId = userCoupGallId;
	}

	public Integer getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(Integer rowNumber) {
		this.rowNumber = rowNumber;
	}

	public Boolean getIsDateFormated() {
		return isDateFormated;
	}

	public void setIsDateFormated(Boolean isDateFormated) {
		this.isDateFormated = isDateFormated;
	}

}
