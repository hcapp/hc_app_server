package com.scansee.common.pojos;

/**
 * this class for handling user payment information.
 * 
 * @author shyamsundara_hm
 */
public class PaymentType extends BaseObject
{

	/**
	 * for paymentTypeID.
	 */
	private Integer paymentTypeID;
	/**
	 * for paymentTypeName.
	 */
	private String paymentTypeName;

	/**
	 * this method for getting paymentTypeID.
	 * 
	 * @return the paymentTypeID
	 */
	public Integer getPaymentTypeID()
	{
		return paymentTypeID;
	}

	/**
	 * this method for setting paymentTypeID.
	 * 
	 * @param paymentTypeID
	 *            the paymentTypeID to set
	 */
	public void setPaymentTypeID(Integer paymentTypeID)
	{
		this.paymentTypeID = paymentTypeID;
	}

	/**
	 * this method for getting paymentTypeName.
	 * 
	 * @return paymentTypeName
	 */
	public String getPaymentTypeName()
	{
		return paymentTypeName;
	}

	/**
	 * thid method for setting paymentTypeName.
	 * 
	 * @param paymentTypeName
	 *            as request parameter.
	 */
	public void setPaymentTypeName(String paymentTypeName)
	{
		this.paymentTypeName = paymentTypeName;
	}

}
