package com.scansee.common.pojos;

/**
 * The DealData class contains setter and getter methods.
 * @author dileepa_cc
 *
 */
public class DealData
{
	
	/**
	 * The apiPartnerName declared as String.
	 */
	private String apiPartnerName;
	/**
	 * The hotDealsListResultSet declared as object.
	 */
	private HotDealsListResultSet hotDealsListResultSet ;
	
	/**
	 * To get apiPartnerName.
	 * @return the apiPartnerName
	 */
	public String getApiPartnerName()
	{
		return apiPartnerName;
	}
	/**
	 * To set apiPartnerName.
	 * @param apiPartnerName the apiPartnerName to set
	 */
	public void setApiPartnerName(String apiPartnerName)
	{
		this.apiPartnerName = apiPartnerName;
	}
	/**
	 * To get hotDealsListResultSet.
	 * @return the hotDealsListResultSet
	 */
	public HotDealsListResultSet getHotDealsListResultSet()
	{
		return hotDealsListResultSet;
	}
	/**
	 * To set hotDealsListResultSet.
	 * @param hotDealsListResultSet the hotDealsListResultSet to set
	 */
	public void setHotDealsListResultSet(HotDealsListResultSet hotDealsListResultSet)
	{
		this.hotDealsListResultSet = hotDealsListResultSet;
	}
	
}
