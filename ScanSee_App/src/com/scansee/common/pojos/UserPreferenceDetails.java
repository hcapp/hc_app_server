package com.scansee.common.pojos;

import java.util.List;

/**
 * this pojo for UserPreferenceDetails request.
 * 
 * @author shyamsundara_hm
 */

public class UserPreferenceDetails extends BaseObject
{

	/**
	 * for paymentIntervals.
	 */
	private List<PaymentInterval> paymentIntervals;
	/**
	 * for paymentTypes.
	 */
	private List<PaymentType> paymentTypes;
	/**
	 * for payoutTypes.
	 */
	private List<PayoutType> payoutTypes;
	/**
	 * for userPreference.
	 */
	private UserPreference userPreference;

	/**
	 * This method for getting userPreference.
	 * 
	 * @return the userPreference
	 */
	public UserPreference getUserPreference()
	{
		return userPreference;
	}

	/**
	 * this method for setting userPreference.
	 * 
	 * @param userPreference
	 *            the userPreference to set
	 */
	public void setUserPreference(UserPreference userPreference)
	{
		this.userPreference = userPreference;
	}

	/**
	 * this method for getting paymentTypes.
	 * 
	 * @return paymentTypes
	 */
	public List<PaymentType> getPaymentTypes()
	{
		return paymentTypes;
	}

	/**
	 * this method for setting paymentTypes.
	 * 
	 * @param paymentTypes
	 *            as request parameter.
	 */

	public void setPaymentTypes(List<PaymentType> paymentTypes)
	{
		this.paymentTypes = paymentTypes;
	}

	/**
	 * this method for getting paymentIntervals.
	 * 
	 * @return paymentIntervals
	 */
	public List<PaymentInterval> getPaymentIntervals()
	{
		return paymentIntervals;
	}

	/**
	 * this method for setting paymentIntervals.
	 * 
	 * @param paymentIntervals
	 *            as request parameter.
	 */
	public void setPaymentIntervals(List<PaymentInterval> paymentIntervals)
	{
		this.paymentIntervals = paymentIntervals;
	}

	/**
	 * this method for getting payoutTypes.
	 * 
	 * @return payoutTypes
	 */
	public List<PayoutType> getPayoutTypes()
	{
		return payoutTypes;
	}

	/**
	 * this method for setting payoutTypes.
	 * 
	 * @param payoutTypes
	 *            as request parameter.
	 */
	public void setPayoutTypes(List<PayoutType> payoutTypes)
	{
		this.payoutTypes = payoutTypes;
	}

}
