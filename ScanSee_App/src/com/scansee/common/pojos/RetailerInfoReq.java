package com.scansee.common.pojos;

/**
 * The POJO class for RetailerInfoReq.
 * 
 * @author dileepa_cc
 */
public class RetailerInfoReq
{
	/**
	 * for prefered Retailers userid.
	 */
	private Integer userID;
	/**
	 * for prefered retailers pagination.
	 */
	private Integer lastVisitedRecord;
	/**
	 * The retailerId property.
	 */
	private Integer retailerId;

	/**
	 * The getter for retailerId property.
	 * 
	 * @return retailerId
	 */

	public Integer getRetailerId()
	{
		return retailerId;
	}

	/**
	 * The setter for retailerId property.
	 * 
	 * @param retailerId
	 *            as a parameter
	 */

	public void setRetailerId(Integer retailerId)
	{
		this.retailerId = retailerId;
	}

	/**
	 * For getting userID.
	 * 
	 * @return the userID
	 */
	public Integer getUserID()
	{
		return userID;
	}

	/**
	 * For setting userID.
	 * 
	 * @param userID
	 *            the userID to set
	 */
	public void setUserID(Integer userID)
	{
		this.userID = userID;
	}

	/**
	 * For getting lastVisitedRecord.
	 * 
	 * @return the lastVisitedRecord
	 */
	public Integer getLastVisitedRecord()
	{
		return lastVisitedRecord;
	}

	/**
	 * For setting lastVisitedRecord.
	 * 
	 * @param lastVisitedRecord
	 *            the lastVisitedRecord to set
	 */
	public void setLastVisitedRecord(Integer lastVisitedRecord)
	{
		this.lastVisitedRecord = lastVisitedRecord;
	}
}
