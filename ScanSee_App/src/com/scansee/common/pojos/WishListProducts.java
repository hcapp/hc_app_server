/**
 * 
 */
package com.scansee.common.pojos;

import java.util.ArrayList;

/**
 * This pojo class contains setter and getter methods for WishListProducts.
 * @author shyamsundara_hm
 *
 */
public class WishListProducts
{	
	/**
	 * For maxCount
	 */
	private Integer maxCount;
	/**
	 * For NextPage
	 */
	private Integer NextPage;
	/**
	 * The alertProducts declared as AlertedProducts object.
	 */
	private AlertedProducts alertProducts;
	
	/**
	 * for wish list history details.
	 */
	private ArrayList<WishListResultSet> productInfo;
	
	/**
	 * Variable mainMenuID
	 */
	private Integer mainMenuID;

	/**
	 * To get alertProducts.
	 * @return the alertProducts
	 */
	public AlertedProducts getAlertProducts()
	{
		return alertProducts;
	}

	/**
	 * Gets the value of alertProducts property.
	 * @param alertProducts the alertProducts to set
	 */
	public void setAlertProducts(AlertedProducts alertProducts)
	{
		this.alertProducts = alertProducts;
	}

	/**
	 * Sets the value of alertProducts property.
	 * @return the productInfo
	 */
	public ArrayList<WishListResultSet> getProductInfo()
	{
		return productInfo;
	}

	/**
	 * Gets the value of productInfo property.
	 * @param productInfo the productInfo to set
	 */
	public void setProductInfo(ArrayList<WishListResultSet> productInfo)
	{
		this.productInfo = productInfo;
	}
	
	public Integer getMainMenuID() {
		return mainMenuID;
	}

	public void setMainMenuID(Integer mainMenuID) {
		this.mainMenuID = mainMenuID;
	}

	/**
	 * @return the maxCount
	 */
	public Integer getMaxCount()
	{
		return maxCount;
	}

	/**
	 * @param maxCount the maxCount to set
	 */
	public void setMaxCount(Integer maxCount)
	{
		this.maxCount = maxCount;
	}

	/**
	 * @return the nextPage
	 */
	public Integer getNextPage()
	{
		return NextPage;
	}

	/**
	 * @param nextPage the nextPage to set
	 */
	public void setNextPage(Integer nextPage)
	{
		NextPage = nextPage;
	}
	
}
