package com.scansee.common.pojos;

import java.util.List;

/**
 * 
 * The POJO class for ShoppingListDetails.
 * @author shyamsundara_hm
 *
 */
public class ShoppingListDetails extends BaseObject
{

	/**
	 * the list of type retailerDetails.
	 */
	private List<RetailerInfo> retailerDetails;

	/**
	 * for getting retailers details.
	 * @return the retailerDetails
	 */
	public List<RetailerInfo> getRetailerDetails()
	{
		return retailerDetails;
	}

	/**for setting retailers details.
	 * @param retailerDetails the retailerDetails to set
	 */
	public void setRetailerDetails(List<RetailerInfo> retailerDetails)
	{
		this.retailerDetails = retailerDetails;
	}

}
