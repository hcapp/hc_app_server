package com.scansee.common.pojos;

public class CellFireRequest
{

	String referenceId;
	String cardNumber;
	String merchantId;
	String retailId;
	String cfLoyaltyId;
	String cfContentVersion;
	boolean isFirstTimeClip;
	String waitTime;

	public String getReferenceId()
	{
		return referenceId;
	}

	public void setReferenceId(String referenceId)
	{
		this.referenceId = referenceId;
	}

	public String getCardNumber()
	{
		return cardNumber;
	}

	public void setCardNumber(String cardNumber)
	{
		this.cardNumber = cardNumber;
	}

	public String getMerchantId()
	{
		return merchantId;
	}

	public void setMerchantId(String merchantId)
	{
		this.merchantId = merchantId;
	}

	/**
	 * @return the retailId
	 */
	public String getRetailId()
	{
		return retailId;
	}

	/**
	 * @param retailId the retailId to set
	 */
	public void setRetailId(String retailId)
	{
		this.retailId = retailId;
	}

	/**
	 * @return the cfLoyaltyId
	 */
	public String getCfLoyaltyId()
	{
		return cfLoyaltyId;
	}

	/**
	 * @param cfLoyaltyId the cfLoyaltyId to set
	 */
	public void setCfLoyaltyId(String cfLoyaltyId)
	{
		this.cfLoyaltyId = cfLoyaltyId;
	}

	/**
	 * @return the cfContentVersion
	 */
	public String getCfContentVersion()
	{
		return cfContentVersion;
	}

	/**
	 * @param cfContentVersion the cfContentVersion to set
	 */
	public void setCfContentVersion(String cfContentVersion)
	{
		this.cfContentVersion = cfContentVersion;
	}

	/**
	 * @return the isFirstTimeClip
	 */
	public boolean isFirstTimeClip()
	{
		return isFirstTimeClip;
	}

	/**
	 * @param isFirstTimeClip the isFirstTimeClip to set
	 */
	public void setFirstTimeClip(boolean isFirstTimeClip)
	{
		this.isFirstTimeClip = isFirstTimeClip;
	}

	/**
	 * @return the waitTime
	 */
	public String getWaitTime()
	{
		return waitTime;
	}

	/**
	 * @param waitTime the waitTime to set
	 */
	public void setWaitTime(String waitTime)
	{
		this.waitTime = waitTime;
	}

	


}
