package com.scansee.common.pojos;

/**
 * This class for sending advertisement request.
 * 
 * @author shyamsundara_hm
 */
public class AdvertisementHitReq extends BaseObject
{
	/**
	 * for userId.
	 */
	private Integer userId;
	/**
	 * for advertisementId.
	 */
	private Integer advertisementId;

	/**
	 * this method for getting userId.
	 * 
	 * @return userId.
	 */

	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * This method for setting userId.
	 * 
	 * @param userId
	 *            as request parameter.
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * This method for getting advertisementId.
	 * 
	 * @return advertisementId
	 */
	public Integer getAdvertisementId()
	{
		return advertisementId;
	}

	/**
	 * this method for setting advertisementId.
	 * 
	 * @param advertisementId
	 *            as request parameter
	 */
	public void setAdvertisementId(Integer advertisementId)
	{
		this.advertisementId = advertisementId;
	}

}
