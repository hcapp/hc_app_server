package com.scansee.common.pojos;

/**
 * The ShoppingListResponse class contains setter and getter methods for fields.
 * @author manjunath_gh
 *
 */
public class ShoppingListResponse extends BaseObject{
	
	/**
	 * The x declared as string.
	 */
	private String x;
	
	/**
	 * The y declared as string.
	 */
	private String y;
	
	/**
	 * The z declared as string.
	 */
	private String z;
	
	/**
	 * To get x.
	 * @return x To get
	 */
	public String getX() {
		return x;
	}
	/**
	 * To set x.
	 * @param x 
	 *      To set.
	 */
	public void setX(String x) {
		this.x = x;
	}
	
	/**
	 * To egt y.
	 * @return y To get
	 */
	public String getY() {
		return y;
	}
	
	/**
	 * To set y.
	 * @param y 
	 *       To set
	 */
	public void setY(String y) {
		this.y = y;
	}
	
	/**
	 * To get z.
	 * @return z To get
	 */
	public String getZ() {
		return z;
	}
	
	/**
	 * To set z.
	 * @param z
	 * 
	 *      To set.
	 */
	public void setZ(String z) {
		this.z = z;
	}


}
