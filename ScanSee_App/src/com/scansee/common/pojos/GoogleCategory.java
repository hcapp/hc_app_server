package com.scansee.common.pojos;

public class GoogleCategory
{

	
	private String CatName;
	private String CatDisName;
	private String CatImgPth;
	private Integer catID;
	/**
	 * @return the catName
	 */
	public String getCatName()
	{
		return CatName;
	}
	/**
	 * @param catName the catName to set
	 */
	public void setCatName(String catName)
	{
		CatName = catName;
	}

	/**
	 * @return the catDisName
	 */
	public String getCatDisName()
	{
		return CatDisName;
	}
	/**
	 * @param catDisName the catDisName to set
	 */
	public void setCatDisName(String catDisName)
	{
		CatDisName = catDisName;
	}
	/**
	 * @return the catImgPth
	 */
	public String getCatImgPth()
	{
		return CatImgPth;
	}
	/**
	 * @param catImgPth the catImgPth to set
	 */
	public void setCatImgPth(String catImgPth)
	{
		CatImgPth = catImgPth;
	}
	
	public Integer getCatID() {
		return catID;
	}
	
	public void setCatID(Integer catID) {
		this.catID = catID;
	}
}
