package com.scansee.common.pojos;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.util.Utility;

/**
 * this pojo for handling user preference details.
 * 
 * @author shyamsundara_hm
 */

public class UserPreference extends BaseObject
{
	/**
	 * for localeID.
	 */
	private Integer localeID;
	/**
	 * for userID.
	 */
	private Integer userID;
	/**
	 * for localeRadius.
	 */
	private Integer localeRadius;
	/**
	 * for scannerSilent.
	 */
	private Integer scannerSilent;
	/**
	 * for displayCoupons.
	 */
	private Integer displayCoupons;
	/**
	 * for displayRebates.
	 */
	private Integer displayRebates;
	/**
	 * for displayLoyaltyRewards.
	 */
	private Integer displayLoyaltyRewards;
	/**
	 * for displayFieldAgent.
	 */
	private Integer displayFieldAgent;
	/**
	 * for fieldAgentRadius.
	 */
	private Integer fieldAgentRadius;
	/**
	 * for savingsActivated.
	 */
	private Integer savingsActivated;
	/**
	 * for sleepStatus.
	 */
	private Integer sleepStatus;
	/**
	 * for sleepActivationDate.
	 */
	private String sleepActivationDate;
	/**
	 * for dateModified.
	 */
	private String dateModified;
	/**
	 * for defaultPayout.
	 */
	private Double defaultPayout;
	/**
	 * for useDefaultAddress.
	 */
	private Integer useDefaultAddress;
	/**
	 * for payAddress1.
	 */
	private String payAddress1;
	/**
	 * for payAddress2.
	 */
	private String payAddress2;
	/**
	 * for payCity.
	 */
	private String payCity;
	/**
	 * for payState.
	 */
	private String payState;
	/**
	 * for payPostalCode.
	 */
	private String payPostalCode;
	/**
	 * for provisionalPayoutActive.
	 */
	private Integer provisionalPayoutActive;
	/**
	 * for provisionalPayoutStartDate.
	 */
	private String provisionalPayoutStartDate;
	/**
	 * for provisionalPayoutEndDate.
	 */
	private String provisionalPayoutEndDate;
	/**
	 * for selPaymentIntervalID.
	 */
	private Integer selPaymentIntervalID;
	/**
	 * for selPaymentTypeID.
	 */
	private Integer selPaymentTypeID;
	/**
	 * for selPayoutTypeID.
	 */
	private Integer selPayoutTypeID;

	/**
	 * used for user default addrress1.
	 */
	private String userAddress1;

	/**
	 * used for user default addrress2.
	 */
	private String userAddress2;

	/**
	 * used for user default address information.
	 */
	private String userCity;

	/**
	 * used for user default address information.
	 */
	private String userState;

	/**
	 * used for user default address information.
	 */
	private String userPostalCode;

	/**
	 * Used this flag to check if User address exists.
	 */
	private Boolean isUserAddExists;

	/**
	 * for getting selPaymentIntervalID.
	 * 
	 * @return selPaymentIntervalID
	 */
	public Integer getSelPaymentIntervalID()
	{
		return selPaymentIntervalID;
	}

	/**
	 * for setting selPaymentIntervalID.
	 * 
	 * @param selPaymentIntervalID
	 *            as request parameter.
	 */
	public void setSelPaymentIntervalID(Integer selPaymentIntervalID)
	{
		this.selPaymentIntervalID = selPaymentIntervalID;
	}

	/**
	 * for getting selPaymentTypeID.
	 * 
	 * @return selPaymentTypeID
	 */
	public Integer getSelPaymentTypeID()
	{
		return selPaymentTypeID;
	}

	/**
	 * for setting selPaymentTypeID.
	 * 
	 * @param selPaymentTypeID
	 *            as request parameter.
	 */
	public void setSelPaymentTypeID(Integer selPaymentTypeID)
	{
		this.selPaymentTypeID = selPaymentTypeID;
	}

	/**
	 * for getting selPayoutTypeID.
	 * 
	 * @return selPayoutTypeID
	 */
	public Integer getSelPayoutTypeID()
	{
		return selPayoutTypeID;
	}

	/**
	 * for setting selPayoutTypeID.
	 * 
	 * @param selPayoutTypeID
	 *            as request parameter.
	 */
	public void setSelPayoutTypeID(Integer selPayoutTypeID)
	{
		this.selPayoutTypeID = selPayoutTypeID;
	}

	/**
	 * this method for getting defaultPayout.
	 * 
	 * @return the defaultPayout
	 */
	public Double getDefaultPayout()
	{
		return defaultPayout;
	}

	/**
	 * this method for setting defaultPayout.
	 * 
	 * @param defaultPayout
	 *            the defaultPayout to set
	 */
	public void setDefaultPayout(Double defaultPayout)
	{
		this.defaultPayout = defaultPayout;
	}

	/**
	 * this method for getting useDefaultAddress.
	 * 
	 * @return the useDefaultAddress
	 */
	public Integer getUseDefaultAddress()
	{
		return useDefaultAddress;
	}

	/**
	 * this method for setting useDefaultAddress.
	 * 
	 * @param useDefaultAddress
	 *            the useDefaultAddress to set
	 */
	public void setUseDefaultAddress(Integer useDefaultAddress)
	{
		this.useDefaultAddress = useDefaultAddress;
	}

	/**
	 * this method for getting payAddress1.
	 * 
	 * @return the payAddress1
	 */
	public String getPayAddress1()
	{
		return payAddress1;
	}

	/**
	 * this method for setting payAddress1.
	 * 
	 * @param payAddress1
	 *            the payAddress1 to set
	 */
	public void setPayAddress1(String payAddress1)
	{
		this.payAddress1 = payAddress1;
	}

	/**
	 * this method for getting payAddress2.
	 * 
	 * @return the payAddress2
	 */
	public String getPayAddress2()
	{
		return payAddress2;
	}

	/**
	 * this method for setting payAddress2.
	 * 
	 * @param payAddress2
	 *            the payAddress2 to set
	 */
	public void setPayAddress2(String payAddress2)
	{
		this.payAddress2 = payAddress2;
	}

	/**
	 * this method for getting payCity.
	 * 
	 * @return the payCity
	 */
	public String getPayCity()
	{
		return payCity;
	}

	/**
	 * this method for setting payCity.
	 * 
	 * @param payCity
	 *            the payCity to set
	 */
	public void setPayCity(String payCity)
	{
		this.payCity = payCity;
	}

	/**
	 * this method for getting payState.
	 * 
	 * @return the payState
	 */
	public String getPayState()
	{
		return payState;
	}

	/**
	 * this method for setting payState.
	 * 
	 * @param payState
	 *            the payState to set
	 */
	public void setPayState(String payState)
	{
		this.payState = payState;
	}

	/**
	 * this method for getting payPostalCode.
	 * 
	 * @return the payPostalCode
	 */
	public String getPayPostalCode()
	{
		return payPostalCode;
	}

	/**
	 * this method for setting payPostalCode.
	 * 
	 * @param payPostalCode
	 *            the payPostalCode to set
	 */
	public void setPayPostalCode(String payPostalCode)
	{
		this.payPostalCode = payPostalCode;
	}

	/**
	 * this method for getting provisionalPayoutActive.
	 * 
	 * @return the provisionalPayoutActive
	 */
	public Integer getProvisionalPayoutActive()
	{
		return provisionalPayoutActive;
	}

	/**
	 * this method for setting provisionalPayoutActive.
	 * 
	 * @param provisionalPayoutActive
	 *            the provisionalPayoutActive to set
	 */
	public void setProvisionalPayoutActive(Integer provisionalPayoutActive)
	{
		// Assigned default value as 0
		if (null == provisionalPayoutActive)
		{
			this.provisionalPayoutActive = 0;

		}
		else
		{
			this.provisionalPayoutActive = provisionalPayoutActive;
		}

	}

	/**
	 * this method for getting provisionalPayoutStartDate.
	 * 
	 * @return the provisionalPayoutStartDate
	 */
	public String getProvisionalPayoutStartDate()
	{
		return provisionalPayoutStartDate;
	}

	/**
	 * this method for setting provisionalPayoutStartDate.
	 * 
	 * @param provisionalPayoutStartDate
	 *            the provisionalPayoutStartDate to set
	 */
	public void setProvisionalPayoutStartDate(String provisionalPayoutStartDate)
	{

		if (provisionalPayoutStartDate != null)
		{

			this.provisionalPayoutStartDate = provisionalPayoutStartDate;
			this.provisionalPayoutStartDate = Utility.convertDBdate(provisionalPayoutStartDate);
		}

		else
		{
			this.provisionalPayoutStartDate = Utility.getFormattedCurrentDate();
		}
	}

	/**
	 * this method for getting provisionalPayoutEndDate.
	 * 
	 * @return the provisionalPayoutEndDate
	 */
	public String getProvisionalPayoutEndDate()
	{
		return provisionalPayoutEndDate;
	}

	/**
	 * this method for setting provisionalPayoutEndDate.
	 * 
	 * @param provisionalPayoutEndDate
	 *            the provisionalPayoutEndDate to set
	 */
	public void setProvisionalPayoutEndDate(String provisionalPayoutEndDate)
	{

		if (provisionalPayoutEndDate != null)
		{

			this.provisionalPayoutEndDate = provisionalPayoutEndDate;
			this.provisionalPayoutEndDate = Utility.convertDBdate(provisionalPayoutEndDate);
		}
		else
		{
			this.provisionalPayoutEndDate = Utility.getFormattedCurrentDate();
		}
	}

	/**
	 * this method for getting localeID.
	 * 
	 * @return the localeID
	 */
	public Integer getLocaleID()
	{
		return localeID;
	}

	/**
	 * this method for setting localeID.
	 * 
	 * @param localeID
	 *            the localeID to set
	 */
	public void setLocaleID(Integer localeID)
	{
		this.localeID = localeID;
	}

	/**
	 * this method getting userID.
	 * 
	 * @return the userID
	 */
	public Integer getUserID()
	{
		return userID;
	}

	/**
	 * this method for setting userID.
	 * 
	 * @param userID
	 *            the userID to set
	 */
	public void setUserID(Integer userID)
	{
		this.userID = userID;
	}

	/**
	 * this method for getting localeRadius.
	 * 
	 * @return the localeRadius
	 */
	public Integer getLocaleRadius()
	{
		return localeRadius;
	}

	/**
	 * this method for setting localeRadius.
	 * 
	 * @param localeRadius
	 *            the localeRadius to set
	 */
	public void setLocaleRadius(Integer localeRadius)
	{
		this.localeRadius = localeRadius;
	}

	/**
	 * this method for getting scannerSilent.
	 * 
	 * @return the scannerSilent
	 */
	public Integer getScannerSilent()
	{
		return scannerSilent;
	}

	/**
	 * this method for setting scannerSilent.
	 * 
	 * @param scannerSilent
	 *            the scannerSilent to set
	 */
	public void setScannerSilent(Integer scannerSilent)
	{
		this.scannerSilent = scannerSilent;
	}

	/**
	 * this method for getting displayCoupons.
	 * 
	 * @return the displayCoupons
	 */
	public Integer getDisplayCoupons()
	{
		return displayCoupons;
	}

	/**
	 * this method for setting displayCoupons.
	 * 
	 * @param displayCoupons
	 *            the displayCoupons to set
	 */
	public void setDisplayCoupons(Integer displayCoupons)
	{
		this.displayCoupons = displayCoupons;
	}

	/**
	 * this method getting displayRebates.
	 * 
	 * @return the displayRebates
	 */
	public Integer getDisplayRebates()
	{
		return displayRebates;
	}

	/**
	 * this method for setting displayRebates.
	 * 
	 * @param displayRebates
	 *            the displayRebates to set
	 */
	public void setDisplayRebates(Integer displayRebates)
	{
		this.displayRebates = displayRebates;
	}

	/**
	 * this method for getting displayLoyaltyRewards.
	 * 
	 * @return the displayLoyaltyRewards
	 */
	public Integer getDisplayLoyaltyRewards()
	{
		return displayLoyaltyRewards;
	}

	/**
	 * this method for setting displayLoyaltyRewards.
	 * 
	 * @param displayLoyaltyRewards
	 *            the displayLoyaltyRewards to set
	 */
	public void setDisplayLoyaltyRewards(Integer displayLoyaltyRewards)
	{
		this.displayLoyaltyRewards = displayLoyaltyRewards;
	}

	/**
	 * this method for getting displayFieldAgent.
	 * 
	 * @return the displayFieldAgent
	 */
	public Integer getDisplayFieldAgent()
	{
		return displayFieldAgent;
	}

	/**
	 * this method for setting displayFieldAgent.
	 * 
	 * @param displayFieldAgent
	 *            the displayFieldAgent to set
	 */
	public void setDisplayFieldAgent(Integer displayFieldAgent)
	{
		this.displayFieldAgent = displayFieldAgent;
	}

	/**
	 * this method for getting fieldAgentRadius.
	 * 
	 * @return the fieldAgentRadius
	 */
	public Integer getFieldAgentRadius()
	{
		return fieldAgentRadius;
	}

	/**
	 * this method for setting fieldAgentRadius.
	 * 
	 * @param fieldAgentRadius
	 *            the fieldAgentRadius to set
	 */
	public void setFieldAgentRadius(Integer fieldAgentRadius)
	{
		this.fieldAgentRadius = fieldAgentRadius;
	}

	/**
	 * this method for getting savingsActivated.
	 * 
	 * @return the savingsActivated
	 */
	public Integer getSavingsActivated()
	{
		return savingsActivated;
	}

	/**
	 * this method for setting savingsActivated.
	 * 
	 * @param savingsActivated
	 *            the savingsActivated to set
	 */
	public void setSavingsActivated(Integer savingsActivated)
	{
		this.savingsActivated = savingsActivated;
	}

	/**
	 * this method for getting sleepStatus.
	 * 
	 * @return the sleepStatus
	 */
	public Integer getSleepStatus()
	{
		return sleepStatus;
	}

	/**
	 * this method for setting sleepStatus.
	 * 
	 * @param sleepStatus
	 *            the sleepStatus to set
	 */
	public void setSleepStatus(Integer sleepStatus)
	{
		this.sleepStatus = sleepStatus;
	}

	/**
	 * this method for getting sleepActivationDate.
	 * 
	 * @return the sleepActivationDate
	 */
	public String getSleepActivationDate()
	{
		return sleepActivationDate;
	}

	/**
	 * this method for setting sleepActivationDate.
	 * 
	 * @param sleepActivationDate
	 *            the sleepActivationDate to set
	 */
	public void setSleepActivationDate(String sleepActivationDate)
	{
		if (sleepActivationDate != null)
		{

			this.sleepActivationDate = sleepActivationDate;
			this.sleepActivationDate = Utility.convertDBdate(sleepActivationDate);
		}

		else
		{
			this.sleepActivationDate = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	/**
	 * this method for getting dateModified.
	 * 
	 * @return the dateModified
	 */
	public String getDateModified()
	{
		return dateModified;
	}

	/**
	 * this method for setting dateModified.
	 * 
	 * @param dateModified
	 *            the dateModified to set
	 */
	public void setDateModified(String dateModified)
	{
		this.dateModified = dateModified;
	}

	/**
	 * This method for getting userAddress1.
	 * 
	 * @return the userAddress1
	 */
	public String getUserAddress1()
	{
		return userAddress1;
	}

	/**
	 * This method for setting userAddress1.
	 * 
	 * @param userAddress1
	 *            the userAddress1 to set
	 */
	public void setUserAddress1(String userAddress1)
	{
		if (null == userAddress1)
		{
			this.userAddress1 = "N/A";
		}
		else
		{
			this.userAddress1 = userAddress1;
		}
	}

	/**
	 * This method for getting userAddress2.
	 * 
	 * @return the userAddress2
	 */
	public String getUserAddress2()
	{
		return userAddress2;
	}

	/**
	 * This method for setting userAddress2.
	 * 
	 * @param userAddress2
	 *            the userAddress2 to set
	 */
	public void setUserAddress2(String userAddress2)
	{
		if (null == userAddress2)
		{
			this.userAddress2 = "N/A";
		}
		else
		{
			this.userAddress2 = userAddress2;
		}

	}

	/**
	 * This method for getting userCity.
	 * 
	 * @return the userCity
	 */
	public String getUserCity()
	{
		return userCity;
	}

	/**
	 * This method for setting userCity.
	 * 
	 * @param userCity
	 *            the userCity to set
	 */
	public void setUserCity(String userCity)
	{
		if (null == userCity)
		{
			this.userCity = "N/A";
		}
		else
		{
			this.userCity = userCity;
		}

	}

	/**
	 * This method for getting userState.
	 * 
	 * @return the userState
	 */
	public String getUserState()
	{
		return userState;
	}

	/**
	 * This method for setting userState.
	 * 
	 * @param userState
	 *            the userState to set
	 */
	public void setUserState(String userState)
	{
		if (null == userState)
		{
			this.userState = "N/A";
		}
		else
		{
			this.userState = userState;
		}

	}

	/**
	 * This method for getting userPostalCode.
	 * 
	 * @return the userPostalCode
	 */
	public String getUserPostalCode()
	{
		return userPostalCode;
	}

	/**
	 * This method for setting userPostalCode.
	 * 
	 * @param userPostalCode
	 *            the userPostalCode to set
	 */
	public void setUserPostalCode(String userPostalCode)
	{
		if (null == userPostalCode)
		{
			this.userPostalCode = "N/A";
		}
		else
		{
			this.userPostalCode = userPostalCode;
		}

	}

	/**
	 * This method for getting isUserAddExists.
	 * 
	 * @return the isUserAddExists
	 */
	public Boolean getIsUserAddExists()
	{
		return isUserAddExists;
	}

	/**
	 * This method for setting isUserAddExists.
	 * 
	 * @param isUserAddExists
	 *            the isUserAddExists to set
	 */
	public void setIsUserAddExists(Boolean isUserAddExists)
	{
		this.isUserAddExists = isUserAddExists;
	}

}
