package com.scansee.common.pojos;

/**
 * The POJO for ShoppingListRequest. 
 *@author sourab_r
 */

public class ShoppingListRequest extends BaseObject{
	
	/**
	 * The userId property.
	 */
	private Integer userId;
	
	/**
	 * To get userId.
	 * @return The getter for userId property.
	 */
	
	public Integer getuserId() {
		return userId;
	}
	
	/**
	 * To set userId.
	 * @param userId The setter for userId property.
	 */
	public void setuserId(Integer userId) {
		this.userId = userId;
	}


}
