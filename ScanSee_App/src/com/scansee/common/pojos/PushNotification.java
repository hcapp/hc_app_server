package com.scansee.common.pojos;

public class PushNotification
{

	private Integer userId;
	private Integer productId;
	private String deviceId;
	private String userTokenID;
	
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public Integer getProductId()
	{
		return productId;
	}
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}
	public String getDeviceId()
	{
		return deviceId;
	}
	public void setDeviceId(String deviceId)
	{
		this.deviceId = deviceId;
	}
	public String getUserTokenID()
	{
		return userTokenID;
	}
	public void setUserTokenID(String userTokenID)
	{
		this.userTokenID = userTokenID;
	}
	
}
