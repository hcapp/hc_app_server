/**
 * 
 */
package com.scansee.common.pojos;

import java.util.List;

/**
 * @author manjunatha_gh
 *
 */
public class LoyaltyCards
{

	private List<CellFireMerchant> cellFireMerchant = null;

	/**
	 * @return the cellFireMerchant
	 */
	public List<CellFireMerchant> getCellFireMerchant()
	{
		return cellFireMerchant;
	}

	/**
	 * @param cellFireMerchant the cellFireMerchant to set
	 */
	public void setCellFireMerchant(List<CellFireMerchant> cellFireMerchant)
	{
		this.cellFireMerchant = cellFireMerchant;
	}
}
