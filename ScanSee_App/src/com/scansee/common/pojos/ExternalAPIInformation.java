package com.scansee.common.pojos;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This class contains setter and getter methods.
 * @author dileepa_cc
 *
 */
public class ExternalAPIInformation
{
	/**
	 * vendorList declared as list.
	 */
	private ArrayList <ExternalAPIVendor> vendorList ;
	
	/**
	 * serchParameters declared as HashMap.
	 */
			
	private HashMap<Integer, ArrayList <ExternalAPISearchParameters>> serchParameters;

	/**
	 * To get vendorList.
	 * @return the vendorList
	 */
	public ArrayList<ExternalAPIVendor> getVendorList()
	{
		return vendorList;
	}

	/**
	 * To set vendorList.
	 * @param vendorList the vendorList to set
	 */
	public void setVendorList(ArrayList<ExternalAPIVendor> vendorList)
	{
		this.vendorList = vendorList;
	}

	/**
	 * To get serchParameters property.
	 * @return the serchParameters
	 */
	public HashMap<Integer, ArrayList<ExternalAPISearchParameters>> getSerchParameters()
	{
		return serchParameters;
	}

	/**
	 * To set serchParameters property.
	 * @param serchParameters the serchParameters to set
	 */
	public void setSerchParameters(HashMap<Integer, ArrayList<ExternalAPISearchParameters>> serchParameters)
	{
		this.serchParameters = serchParameters;
	}

}
