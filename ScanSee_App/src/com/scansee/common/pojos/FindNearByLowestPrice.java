package com.scansee.common.pojos;

import com.scansee.common.constants.ApplicationConstants;

/**
 * This class for capturing findnear by details with lowest price.
 * 
 * @author shyamsundara_hm
 */
public class FindNearByLowestPrice extends BaseObject

{
	/**
	 * for lowestPrice.
	 */
	private String lowestPrice;

	/**
	 * for totalRetailers.
	 */
	private Integer totalRetailers;

	/**
	 * for imagePath.
	 */
	private String imagePath;

	/**
	 * for productName.
	 */
	private String productName;

	/**
	 * to get lowestPrice.
	 * 
	 * @return lowestPrice
	 */

	public String getLowestPrice()
	{
		return lowestPrice;
	}

	/**
	 * for setting lowestPrice.
	 * 
	 * @param lowestPrice
	 *            to be set
	 */
	public void setLowestPrice(String lowestPrice)
	{
		this.lowestPrice = lowestPrice;
	}

	/**
	 * for getting totalRetailers.
	 * 
	 * @return totalRetailers
	 */
	public Integer getTotalRetailers()
	{
		return totalRetailers;
	}

	/**
	 * for setting totalRetailers.
	 * 
	 * @param totalRetailers
	 *            to be set
	 */
	public void setTotalRetailers(Integer totalRetailers)
	{
		this.totalRetailers = totalRetailers;
	}

	/**
	 * for getting imagePath.
	 * 
	 * @return imagePath
	 */
	public String getImagePath()
	{
		return imagePath;
	}

	/**
	 * for setting imagePath.
	 * 
	 * @param imagePath
	 *            to be set
	 */
	public void setImagePath(String imagePath)
	{
		if (imagePath == null)
		{
			this.imagePath = ApplicationConstants.IMAGENOTFOUND;
		}
		else
		{
			this.imagePath = imagePath;
		}
	}

	/**
	 * for getting productName.
	 * 
	 * @return productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * for setting productName.
	 * 
	 * @param productName
	 *            to be set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

}
