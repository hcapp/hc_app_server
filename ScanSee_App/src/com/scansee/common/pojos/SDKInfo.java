package com.scansee.common.pojos;

/**
 * The POJO class for fetching  SDKInformation.
 * @author shyamsundara_hm
 */

public class SDKInfo extends BaseObject
{

	/**
	 * The sdkInfo property.
	 */

	private String sdkInfo;
	
	/**
	 * for fetching sdkInfo.
	 * @return The getter for sdkInfo property.
	 */

	public String getSdkInfo()
	{
		return sdkInfo;
	}

	/**
	 * for setting sdkInfo.
	 * @param sdkInfo
	 *            The setter for sdkInfo property.
	 */

	public void setSdkInfo(String sdkInfo)
	{
		this.sdkInfo = sdkInfo;
	}

}
