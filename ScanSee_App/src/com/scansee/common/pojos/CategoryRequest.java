package com.scansee.common.pojos;

/**
 * The POJO class for CategoryRequest.
 * 
 * @author shyamsundara_hm
 */

public class CategoryRequest extends BaseObject
{

	/**
	 * for userId.
	 */
	private Integer userId;
	/**
	 * for retailerId.
	 */
	private Integer retailerId;
	
	/**
	 * for retailerLocationId.
	 */
	private Integer retailerLocationId;

	

	/**
	 * This method for getting userId.
	 * 
	 * @return the userId.
	 */
	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * This method for setting userId.
	 * 
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * This method for getting retailerId.
	 * 
	 * @return the retailerId
	 */
	public Integer getRetailerId()
	{
		return retailerId;
	}

	/**
	 * This method for setting retailer id.
	 * 
	 * @param retailerId
	 *            the retailerId to set
	 */
	public void setRetailerId(Integer retailerId)
	{
		this.retailerId = retailerId;
	}
	/**
	 * This method for getting retailerLocationId.
	 * 
	 * @return the retailerLocationId
	 */
	public Integer getRetailerLocationId()
	{
		return retailerLocationId;
	}
	/**
	 * This method for setting retailerLocationId.
	 * 
	 * @param retailerLocationId
	 *            the retailerLocationId to set
	 */
	public void setRetailerLocationId(Integer retailerLocationId)
	{
		this.retailerLocationId = retailerLocationId;
	}

}
