package com.scansee.common.pojos;

import java.util.List;

/**
 * The CreateOrUpdateRetailerPreferences contains setter and getter methods.
 * @author sourab_r
 *
 */
public class CreateOrUpdateRetailerPreferences
{
	
   /**
    * The userId declared as integer.
    */
	private Integer userId;
	
	/**
	 * The retailerInfoReq declared as list.
	 */
	private List<RetailerInfoReq> retailerInfoReq ;
	
	/**
	 * To get userId.
	 * @return userId To get
	 */
	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * To set userId.
	 * @param userId
	 *       To set 
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * To get retailerInfoReq.
	 * @return retailerInfoReq To get
	 */
	public List<RetailerInfoReq> getRetailerInfoReq()
	{
		return retailerInfoReq;
	}

	/**
	 * To set retailerInfoReq.
	 * @param retailerInfoReq
	 *          -To set
	 */
	public void setRetailerInfoReq(List<RetailerInfoReq> retailerInfoReq)
	{
		this.retailerInfoReq = retailerInfoReq;
	}


}
