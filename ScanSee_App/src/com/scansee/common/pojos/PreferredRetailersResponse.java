package com.scansee.common.pojos;

import java.util.List;

/**
 * The PreferredRetailersResponse contains setter and getter methods.
 * @author sourab_r
 *
 */
public class PreferredRetailersResponse
{
	/**
	 * The userId declared as integer.
	 */
	private Integer userId;
	
	/**
	 * The retailersRes declared as List.
	 */
    private List<RetailersRes> retailersRes;
    
	/**
	 * Gets the value of userId property.
	 * @return userId To get
	 */
	public Integer getUserId()
	{
		return userId;
	}
	
	/**
	 * The userId to set.
	 * @param userId
	 *       To set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	/**
	 * To get retailersRes list.
	 * @return retailersRes To get
	 */
	public List<RetailersRes> getRetailersRes()
	{
		return retailersRes;
	}
	
	/**
	 * The retailersRes to set.
	 * @param retailersRes 
	 *          To set
	 */
	public void setRetailersRes(List<RetailersRes> retailersRes)
	{
		this.retailersRes = retailersRes;
	}
	
	

}
