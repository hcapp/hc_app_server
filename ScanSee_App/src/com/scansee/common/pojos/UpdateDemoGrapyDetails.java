package com.scansee.common.pojos;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * The class has getter and setter methods for UpdateDemoGrapyDetails.
 * 
 * @author saurab_r
 */

public class UpdateDemoGrapyDetails extends BaseObject
{
	/**
	 * the gender property.
	 */
	protected String gender;
	
	/**
	 * the dob property.
	 */
	protected XMLGregorianCalendar dob;

	/**
	 * 
	 * the income property.
	 */
	protected Integer income;

	/**
	 * the numberOfChildren property.
	 */
	protected Integer numberOfChildren;
	
	/**
	 * numberOfPets property.
	 */
	protected Integer numberOfPets;
	/**
	 * the ownResidence property.
	 */

	protected String ownResidence;
	
	/**
	 * the userName property.
	 */
	protected String userName;

	/**
	 * Gets the value of the gender property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getGender()
	{
		return gender;
	}

	/**
	 * Sets the value of the gender property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setGender(String value)
	{
		this.gender = value;
	}

	/**
	 * Gets the value of the dob property.
	 * 
	 * @return possible object is {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getDob()
	{
		return dob;
	}

	/**
	 * Sets the value of the dob property.
	 * 
	 * @param value
	 *            allowed object is {@link XMLGregorianCalendar }
	 */
	public void setDob(XMLGregorianCalendar value)
	{
		this.dob = value;
	}

	/**
	 * Gets the value of the income property.
	 * 
	 * @return possible object is {@link Integer }
	 */
	public Integer getIncome()
	{
		return income;
	}

	/**
	 * Sets the value of the income property.
	 * 
	 * @param value
	 *            allowed object is {@link Integer }
	 */
	public void setIncome(Integer value)
	{
		this.income = value;
	}

	/**
	 * Gets the value of the numberOfChildren property.
	 * @return numberOfChildren
	 */
	public Integer getNumberOfChildren()
	{
		return numberOfChildren;
	}

	/**
	 * Sets the value of the numberOfChildren property.
	 * @param value
	 *           To set
	 */
	public void setNumberOfChildren(Integer value)
	{
		this.numberOfChildren = value;
	}

	/**
	 * Gets the value of the numberOfPets property.
	 * @return numberOfPets
	 */
	public Integer getNumberOfPets()
	{
		return numberOfPets;
	}

	/**
	 * Sets the value of the numberOfPets property.
	 *  @param value
	 *           To set
	 */
	public void setNumberOfPets(Integer value)
	{
		this.numberOfPets = value;
	}

	/**
	 * Gets the value of the ownResidence property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getOwnResidence()
	{
		return ownResidence;
	}

	/**
	 * Sets the value of the ownResidence property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setOwnResidence(String value)
	{
		this.ownResidence = value;
	}

	/**
	 * Gets the value of the userName property.
	 * 
	 * @return possible object is {@link String }
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * Sets the value of the userName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 */
	public void setUserName(String value)
	{
		this.userName = value;
	}

}
