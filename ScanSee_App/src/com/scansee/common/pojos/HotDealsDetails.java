package com.scansee.common.pojos;

import java.util.List;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.util.Utility;

/**
 * The POJO class for HotDealsDetails.
 * 
 * @author shyamsundara_hm.
 */
public class HotDealsDetails extends BaseObject
{

	/**
	 * for hot deal id
	 */
	private Integer productHotDealID;
	/**
	 * for coupon share info through twitter
	 */
	private String titleText;

	/**
	 * for coupon share info
	 */
	private String titleText2;
	/**
	 * for city
	 */
	private String city;
	/**
	 * for hot deal expired
	 */
	private Integer hotDealExpired;
	/**
	 * for api image path.
	 */
	private String apiPartnerImagePath;
	/**
	 * for rowNumber.
	 * 
	 * @return the rowNumber
	 */
	private Integer rowNumber;

	/**
	 * Variable apiPartnerId declared as Integer.
	 */
	private Integer apiPartnerId;
	/**
	 * Variable apiPartnerName declared as String.
	 */
	private String apiPartnerName;
	/**
	 * Variable hotDealName declared as String.
	 */
	private String hotDealName;
	/**
	 * Variable hotDealId declared as Integer.
	 */
	private Integer hotDealId;
	/**
	 * Variable hotDealImagePath declared as String.
	 */
	private String hotDealImagePath;
	/**
	 * Variable hDshortDescription declared as String.
	 */
	private String hDshortDescription;
	/**
	 * Variable hDLognDescription declared as String.
	 */
	private String hDLognDescription;
	/**
	 * Variable hDPrice declared as String.
	 */
	private String hDPrice;
	/**
	 * Variable hDSalePrice declared as String.
	 */
	private String hDSalePrice;
	/**
	 * Variable hDTermsConditions declared as String.
	 */
	private String hDTermsConditions;
	/**
	 * Variable hdURL declared as String.
	 */
	private String hdURL;
	/**
	 * Variable hDStartDate declared as String.
	 */
	private String hDStartDate;
	/**
	 * Variable hDEndDate declared as String.
	 */
	private String hDEndDate;
	/**
	 * Variable hDDiscountType declared as String.
	 */
	private String hDDiscountType;
	/**
	 * Variable hDDiscountAmount declared as double.
	 */
	private String hDDiscountAmount;
	/**
	 * Variable hDDiscountPct declared as double.
	 */
	private String hDDiscountPct;
	/**
	 * Variable productID declared as Integer.
	 */
	private Integer productID;

	/**
	 * The rowNumber property.
	 */

	private String distance;

	/**
	 * for hot deal category id.
	 */
	private Integer cateId;
	/**
	 * for hot deal category name
	 */
	private String catName;
	
	/**
	 * for facebook image.
	 */
	private String facebookimg;

	/**
	 * for scansee image.
	 */
	private String scanseeimg;
	/**
	 * for twitter image
	 */
	private String twitterimg;
	/**
	 * arrow scansee image.
	 */
	private String arrowscanseeimg;
	
	/**
	 * For retailer list ID
	 */
	private Integer retListID;
	
	/**
	 * for hot deal list ID
	 */
	private Integer hotDealListID;

	/**
	 * For new Flag
	 */
	Integer newFlag;
	
	/**
	 * For external flag
	 */
	Integer extFlag;
	
	/**
	 * For claim flag
	 */
	Integer claimFlag;
	
	/**
	 * For redeem flag
	 */
	Integer redeemFlag;
	
	/**
	 * For user Name
	 */
	String userName;
	
	/**
	 * For retailer name
	 */
	String retName;
	
	/**
	 * For retailer location IDs
	 */
	String retLocIDs;

	/**
	 * For retialer location
	 */
	String retLocs;

	/**
	 * Variable for hot deal expire date
	 */
	private String hDExpDate;
	
	/**
	 * For coupon code
	 */
	private String coupCode;
	
	private String hDGallId;
	
	private String hDDesc;
	
	private Integer usedFlag;
	
	private Integer catId;
	
	private Integer retId;
	
	private Integer retListId;
	
	private String address;
	
	private Integer retLocId;
	
	private Boolean isDateFormated;
	
	/**
	 * To get distance.
	 * 
	 * @return the distance
	 */
	public String getDistance()
	{
		return distance;
	}

	/**
	 * To set distance.
	 * 
	 * @param distance
	 *            the distance to set
	 */
	public void setDistance(String distance)
	{
		this.distance = distance;
	}

	/**
	 * To get rowNumber.
	 * 
	 * @return rowNumber To get
	 */
	public Integer getRowNumber()
	{
		return rowNumber;
	}

	/**
	 * To set rowNumber.
	 * 
	 * @param rowNumber
	 *            the rowNumber to set
	 */
	public void setRowNumber(Integer rowNumber)
	{
		this.rowNumber = rowNumber;
	}

	/**
	 * Gets the value of the hDPrice property.
	 * 
	 * @return the hDPrice
	 */
	public String gethDPrice()
	{
		return hDPrice;
	}

	/**
	 * Sets the value of the hDPrice property.
	 * 
	 * @param hDPrice
	 *            as of type String.
	 */

	public void sethDPrice(String hDPrice)
	{
		if (null == hDPrice)
		{
			this.hDPrice = ApplicationConstants.NOTAPPLICABLE;
		}
		else if (null != hDPrice && !ApplicationConstants.NOTAPPLICABLE.equals(hDPrice) && !hDPrice.contains("$"))
		{
			this.hDPrice = "$" + Utility.formatDecimalValue(hDPrice);
		}
		else
		{
			this.hDPrice = hDPrice;
		}
		/*
		 * else if(null != hDPrice &&
		 * !ApplicationConstants.NOTAPPLICABLE.equals(hDPrice)&&
		 * !hDPrice.contains("$")) { Double d = new Double(hDPrice); if (d <= 0)
		 * { this.hDPrice = ApplicationConstants.NOTAPPLICABLE; }else {
		 * this.hDPrice = "$" + Utility.formatDecimalValue(hDPrice); } }
		 */

	}

	/**
	 * Gets the value of the hDSalePrice property.
	 * 
	 * @return the hDSalePrice
	 */
	public String gethDSalePrice()
	{
		return hDSalePrice;
	}

	/**
	 * Sets the value of the hDSalePrice property.
	 * 
	 * @param hDSalePrice
	 *            as of type String.
	 */
	public void sethDSalePrice(String hDSalePrice)
	{
		if (null == hDSalePrice)
		{
			this.hDSalePrice = ApplicationConstants.NOTAPPLICABLE;
		}
		else if (null != hDSalePrice && !ApplicationConstants.NOTAPPLICABLE.equals(hDSalePrice) && !hDSalePrice.contains("$"))
		{
			this.hDSalePrice = "$" + Utility.formatDecimalValue(hDSalePrice);
		}
		else
		{
			this.hDSalePrice = hDSalePrice;
		}
		/*
		 * else if(null != hDSalePrice &&
		 * !ApplicationConstants.NOTAPPLICABLE.equals(hDSalePrice)&&
		 * !hDSalePrice.contains("$")) { Double d = new Double(hDSalePrice); if
		 * (d <= 0) { this.hDSalePrice = ApplicationConstants.NOTAPPLICABLE;
		 * }else { this.hDSalePrice = "$" +
		 * Utility.formatDecimalValue(hDSalePrice); } }
		 */

	}

	/**
	 * Gets the value of the productID property.
	 * 
	 * @return the productID
	 */
	public Integer getProductID()
	{
		return productID;
	}

	/**
	 * Sets the value of the productID property.
	 * 
	 * @param productID
	 *            as of type String.
	 */
	public void setProductID(Integer productID)
	{
		this.productID = productID;
	}

	/**
	 * Gets the value of the hDDiscountType property.
	 * 
	 * @return the hDDiscountType
	 */
	public String gethDDiscountType()
	{
		return hDDiscountType;
	}

	/**
	 * Sets the value of the hDDiscountType property.
	 * 
	 * @param hDDiscountType
	 *            as of type String.
	 */
	public void sethDDiscountType(String hDDiscountType)
	{
		if (null == hDDiscountType)
		{
			this.hDDiscountType = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.hDDiscountType = hDDiscountType;
		}
	}

	/**
	 * Gets the value of the hotDealImagePath property.
	 * 
	 * @return the hotDealImagePath
	 */
	public String getHotDealImagePath()
	{
		return hotDealImagePath;
	}

	/**
	 * Sets the value of the hotDealImagePath property.
	 * 
	 * @param hotDealImagePath
	 *            as of type String.
	 */
	public void setHotDealImagePath(String hotDealImagePath)
	{
		if (null == hotDealImagePath)
		{
			this.hotDealImagePath = ApplicationConstants.IMAGENOTFOUND;
		}
		else
		{
			this.hotDealImagePath = hotDealImagePath;
		}

	}

	/**
	 * Gets the value of the apiPartnerId property.
	 * 
	 * @return the apiPartnerId
	 */
	public Integer getApiPartnerId()
	{
		return apiPartnerId;
	}

	/**
	 * Sets the value of the apiPartnerId property.
	 * 
	 * @param apiPartnerId
	 *            as of type String.
	 */
	public void setApiPartnerId(Integer apiPartnerId)
	{
		this.apiPartnerId = apiPartnerId;
	}

	/**
	 * Gets the value of the apiPartnerName property.
	 * 
	 * @return the apiPartnerName
	 */
	public String getApiPartnerName()
	{
		return apiPartnerName;
	}

	/**
	 * Sets the value of the apiPartnerName property.
	 * 
	 * @param apiPartnerName
	 *            as of type String.
	 */
	public void setApiPartnerName(String apiPartnerName)
	{
		if (null == apiPartnerName)
		{
			this.apiPartnerName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.apiPartnerName = apiPartnerName;
		}
	}

	/**
	 * Gets the value of the hotDealName property.
	 * 
	 * @return the hotDealName
	 */
	public String getHotDealName()
	{
		return hotDealName;
	}

	/**
	 * Sets the value of the hotDealName property.
	 * 
	 * @param hotDealName
	 *            as of type String.
	 */
	public void setHotDealName(String hotDealName)
	{
		hotDealName = Utility.removeHTMLTags(hotDealName);
		if (null == hotDealName)
		{
			this.hotDealName = ApplicationConstants.NOTAPPLICABLE;
		}
		else if(hotDealName.contains("<![CDATA["))
		{
			this.hotDealName = hotDealName;
		}
		else	{
			this.hotDealName = "<![CDATA[" + hotDealName + "]]>";
		}

	}

	/**
	 * Gets the value of the hotDealId property.
	 * 
	 * @return the hotDealId
	 */
	public Integer getHotDealId()
	{
		return hotDealId;
	}

	/**
	 * Sets the value of the hotDealId property.
	 * 
	 * @param hotDealId
	 *            as of type String.
	 */
	public void setHotDealId(Integer hotDealId)
	{
		this.hotDealId = hotDealId;
	}

	/**
	 * Gets the value of the hDshortDescription property.
	 * 
	 * @return the hDshortDescription
	 */
	public String gethDshortDescription()
	{
		return hDshortDescription;
	}

	/**
	 * Sets the value of the hDshortDescription property.
	 * 
	 * @param hDshortDescription
	 *            as of type String.
	 */
	public void sethDshortDescription(String hDshortDescription)
	{
		hDshortDescription = Utility.removeHTMLTags(hDshortDescription);
		
		if (null == hDshortDescription || "".equals(hDshortDescription))
		{
			this.hotDealName = ApplicationConstants.NOTAPPLICABLE;
		}
		else if(hDshortDescription.contains("<![CDATA["))	
		{
			this.hDshortDescription = hDshortDescription;		
		}
		else	
		{
			this.hDshortDescription = "<![CDATA[" + hDshortDescription + "]]>";
		}
	}

	/**
	 * Gets the value of the hDLognDescription property.
	 * 
	 * @return the hDLognDescription
	 */
	public String gethDLognDescription()
	{
		return hDLognDescription;
	}

	/**
	 * Sets the value of the hDLognDescription property.
	 * 
	 * @param hDLognDescription
	 *            as of type String.
	 */
	public void sethDLognDescription(String hDLognDescription)
	{
//		hDLognDescription = Utility.removeHTMLTags(hDLognDescription);
//		hDLognDescription = hDLognDescription.replaceAll("&#xd;", "");
		if (null == hDLognDescription || "".equals(hDLognDescription))
		{
			this.hDLognDescription = ApplicationConstants.NOTAPPLICABLE;
		}
		else if(hDLognDescription.contains("<![CDATA["))
		{
			this.hDLognDescription = "<![CDATA[" + hDLognDescription + "]]>";
		}
		else
		{
			this.hDLognDescription = hDLognDescription;
		}

	}

	/**
	 * Gets the value of the hDTermsConditions property.
	 * 
	 * @return the hDTermsConditions
	 */
	public String gethDTermsConditions()
	{
		return hDTermsConditions;
	}

	/**
	 * Sets the value of the hDTermsConditions property.
	 * 
	 * @param hDTermsConditions
	 *            as of type String.
	 */
	public void sethDTermsConditions(String hDTermsConditions)
	{
//		hDTermsConditions = Utility.removeHTMLTags(hDTermsConditions);
//		hDTermsConditions = hDTermsConditions.replaceAll("&#xd;", "");
		
		if (null == hDTermsConditions || "".equals(hDTermsConditions))
		{
			this.hDTermsConditions = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			if(hDTermsConditions.contains("<![CDATA["))	{
				this.hDTermsConditions = hDTermsConditions;
			}
			else	{
				this.hDTermsConditions = "<![CDATA[" + hDTermsConditions + "]]>";
			}
				
		}

	}

	/**
	 * Gets the value of the hdURL property.
	 * 
	 * @return the hdURL
	 */
	public String getHdURL()
	{
		return hdURL;
	}

	/**
	 * Sets the value of the hdURL property.
	 * 
	 * @param hdURL
	 *            as of type String.
	 */
	public void setHdURL(String hdURL)
	{
		if (null == hdURL || hdURL.equals(""))
		{
			this.hdURL = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.hdURL = hdURL;
		}
	}

	/**
	 * Gets the value of the hDStartDate property.
	 * 
	 * @return the hDStartDate
	 */
	public String gethDStartDate()
	{
		return hDStartDate;
	}

	/**
	 * Sets the value of the hDStartDate property.
	 * 
	 * @param hDStartDate
	 *            as of type String.
	 */
	public void sethDStartDate(String hDStartDate)
	{
		if (hDStartDate != null)
		{
			if(null != isDateFormated && isDateFormated == false)	{
				this.hDStartDate = Utility.convertDBdate(hDStartDate);
			}
			else	{
				this.hDStartDate = hDStartDate;
			}
		}

		else
		{
			this.hDStartDate = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	/**
	 * Gets the value of the hDEndDate property.
	 * 
	 * @return the hDEndDate
	 */
	public String gethDEndDate()
	{
		return hDEndDate;
	}

	/**
	 * Sets the value of the hDEndDate property.
	 * 
	 * @param hDEndDate
	 *            as of type String.
	 */
	public void sethDEndDate(String hDEndDate)
	{
		if (hDEndDate != null)
		{
			if(null != isDateFormated && isDateFormated == false)	{
				this.hDEndDate = Utility.convertDBdate(hDEndDate);
			}
			else	{
				this.hDEndDate = hDEndDate;
			}
		}
		else
		{
			this.hDEndDate = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	/**
	 * gets the value of hDDiscountAmount.
	 * 
	 * @return the hDDiscountAmount
	 */
	public String gethDDiscountAmount()
	{
		return hDDiscountAmount;
	}

	/**
	 * sets the value of hDDiscountAmount.
	 * 
	 * @param hDDiscountAmount
	 *            the hDDiscountAmount to set
	 */
	public void sethDDiscountAmount(String hDDiscountAmount)
	{
		if (null == hDDiscountAmount)
		{
			this.hDDiscountAmount = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.hDDiscountAmount = hDDiscountAmount;
		}
	}

	/**
	 * gets the value of hDDiscountPct.
	 * 
	 * @return the hDDiscountPct
	 */
	public String gethDDiscountPct()
	{
		return hDDiscountPct;
	}

	/**
	 * sets the value of hDDiscountPct.
	 * 
	 * @param hDDiscountPct
	 *            the hDDiscountPct to set
	 */
	public void sethDDiscountPct(String hDDiscountPct)
	{
		if (null == hDDiscountPct)
		{
			this.hDDiscountPct = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.hDDiscountPct = hDDiscountPct;
		}
	}

	/**
	 * @return the apiPartnerImagePath
	 */
	public String getApiPartnerImagePath()
	{
		return apiPartnerImagePath;
	}

	/**
	 * @param apiPartnerImagePath
	 *            the apiPartnerImagePath to set
	 */
	public void setApiPartnerImagePath(String apiPartnerImagePath)
	{
		if (apiPartnerImagePath == null || "".equals(apiPartnerImagePath))
		{
			this.apiPartnerImagePath = ApplicationConstants.IMAGENOTFOUND;
		}
		else
		{
			this.apiPartnerImagePath = apiPartnerImagePath;
		}
	}

	public Integer getHotDealExpired()
	{
		return hotDealExpired;
	}

	public void setHotDealExpired(Integer hotDealExpired)
	{
		this.hotDealExpired = hotDealExpired;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		
			this.city = city;
		
	}

	public String getTitleText()
	{
		return titleText;
	}

	public void setTitleText(String titleText)
	{
		this.titleText = titleText;
	}

	public String getTitleText2()
	{
		return titleText2;
	}

	public void setTitleText2(String titleText2)
	{
		this.titleText2 = titleText2;
	}

	public Integer getProductHotDealID()
	{
		return productHotDealID;
	}

	public void setProductHotDealID(Integer productHotDealID)
	{
		this.productHotDealID = productHotDealID;
	}

	public Integer getCateId()
	{
		return cateId;
	}

	public void setCateId(Integer cateId)
	{
		this.cateId = cateId;
	}

	public String getCatName()
	{
		return catName;
	}

	public void setCatName(String catName)
	{
		this.catName = catName;
	}

	public String getFacebookimg() {
		return facebookimg;
	}

	public void setFacebookimg(String facebookimg) {
		this.facebookimg = facebookimg;
	}

	public String getScanseeimg() {
		return scanseeimg;
	}

	public void setScanseeimg(String scanseeimg) {
		this.scanseeimg = scanseeimg;
	}

	public String getTwitterimg() {
		return twitterimg;
	}

	public void setTwitterimg(String twitterimg) {
		this.twitterimg = twitterimg;
	}

	public String getArrowscanseeimg() {
		return arrowscanseeimg;
	}

	public void setArrowscanseeimg(String arrowscanseeimg) {
		this.arrowscanseeimg = arrowscanseeimg;
	}
	
	public Integer getRetListID() {
		return retListID;
	}

	public void setRetListID(Integer retListID) {
		this.retListID = retListID;
	}

	public Integer getHotDealListID() {
		return hotDealListID;
	}

	public void setHotDealListID(Integer hotDealListID) {
		this.hotDealListID = hotDealListID;
	}

	public Integer getNewFlag() {
		return newFlag;
	}

	public void setNewFlag(Integer newFlag) {
		this.newFlag = newFlag;
	}

	public Integer getExtFlag() {
		return extFlag;
	}

	public void setExtFlag(Integer extFlag) {
		this.extFlag = extFlag;
	}
	
	public Integer getClaimFlag() {
		return claimFlag;
	}

	public void setClaimFlag(Integer claimFlag) {
		this.claimFlag = claimFlag;
	}

	public Integer getRedeemFlag() {
		return redeemFlag;
	}

	public void setRedeemFlag(Integer redeemFlag) {
		this.redeemFlag = redeemFlag;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		if(null == userName || "".equalsIgnoreCase(userName))	{
			this.userName = ApplicationConstants.NOTAPPLICABLE;
		}
		else	{
			this.userName = userName;
		}
	}

	public String getRetName() {
		return retName;
	}

	public void setRetName(String retName) {
		this.retName = retName;
	}
	
	public String getRetLocIDs() {
		return retLocIDs;
	}

	public void setRetLocIDs(String retLocIDs) {
		this.retLocIDs = retLocIDs;
	}

	public String getRetLocs() {
		return retLocs;
	}

	public void setRetLocs(String retLocs) {
		this.retLocs = retLocs;
	}
	
	public String gethDExpDate() {
		return hDExpDate;
	}

	public void sethDExpDate(String hDExpDate) {
		if(null != hDExpDate)	{
			if(null != isDateFormated && isDateFormated == false)	{
				this.hDExpDate = Utility.convertDBdate(hDExpDate);
			}
			else	{
				this.hDExpDate = hDExpDate;
			}
		}
		else	{
			this.hDExpDate = ApplicationConstants.NOTAPPLICABLE;
		}
	}
	
	public String getCoupCode() {
		return coupCode;
	}

	public void setCoupCode(String coupCode) {
		if(null != coupCode)	{	
			this.coupCode = coupCode;
		}
		else	{
			this.coupCode = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	public String gethDGallId() {
		return hDGallId;
	}

	public void sethDGallId(String hDGallId) {
		this.hDGallId = hDGallId;
	}

	public String gethDDesc() {
		return hDDesc;
	}

	public void sethDDesc(String hDDesc) {
		if( null != hDDesc)	{
			this.hDDesc = hDDesc;
		}
		else {
			this.hDDesc = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	public Integer getUsedFlag() {
		return usedFlag;
	}

	public void setUsedFlag(Integer usedFlag) {
		this.usedFlag = usedFlag;
	}

	public Integer getCatId() {
		return catId;
	}

	public void setCatId(Integer catId) {
		this.catId = catId;
	}

	public Integer getRetId() {
		return retId;
	}

	public void setRetId(Integer retId) {
		this.retId = retId;
	}

	public Integer getRetListId() {
		return retListId;
	}

	public void setRetListId(Integer retListId) {
		this.retListId = retListId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		if(null != address)	{
			this.address = address;
		}
		else	{
			this.address = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	public Integer getRetLocId() {
		return retLocId;
	}

	public void setRetLocId(Integer retLocId) {
		this.retLocId = retLocId;
	}

	public Boolean getIsDateFormated() {
		return isDateFormated;
	}

	public void setIsDateFormated(Boolean isDateFormated) {
		this.isDateFormated = isDateFormated;
	}

}
