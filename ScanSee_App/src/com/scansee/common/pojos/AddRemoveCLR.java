package com.scansee.common.pojos;

/**
 * This is for AddRemoveCLR request.
 * 
 * @author saurabh_r
 */
public class AddRemoveCLR extends BaseObject
{
	/**
	 * for productid..
	 */
	private Integer productId;
	/**
	 * for userId.
	 */
	private Integer userId;
	/**
	 * for couponId.
	 */
	private Integer couponId;
	/**
	 * for loyaltyDealId.
	 */
	private Integer loyaltyDealId;
	/**
	 * for rebateId.
	 */
	private Integer rebateId;
	/**
	 * for addRemoveFlag.
	 */

	private String addRemoveFlag;
	
	/**
	 * variable for merchant id like cellfire
	 * 
	 */
	private String merchantId;
	
	String cardNumber;
	
	private Integer coupListID;

	/**
	 * to get userId.
	 * 
	 * @return userId
	 */
	
	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * to set userId.
	 * 
	 * @param userId
	 *            to be set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * to get couponId.
	 * 
	 * @return couponId
	 */
	public Integer getCouponId()
	{
		return couponId;
	}

	/**
	 * to set couponId to be set.
	 * 
	 * @param couponId
	 *            to be set
	 */
	public void setCouponId(Integer couponId)
	{
		this.couponId = couponId;
	}

	/**
	 * to get addRemoveFlag.
	 * 
	 * @return addRemoveFlag
	 */
	public String getAddRemoveFlag()
	{
		return addRemoveFlag;
	}

	/**
	 * to set addRemoveFlag.
	 * 
	 * @param addRemoveFlag
	 *            to be set.
	 */
	public void setAddRemoveFlag(String addRemoveFlag)
	{
		this.addRemoveFlag = addRemoveFlag;
	}

	/**
	 * to get loyaltyDealId.
	 * 
	 * @return loyaltyDealId.
	 */
	public Integer getloyaltyDealId()
	{
		return loyaltyDealId;
	}

	/**
	 * to be set loyaltyDealId.
	 * 
	 * @param loyaltyDealId
	 *            to be set.
	 */
	public void setloyaltyDealId(Integer loyaltyDealId)
	{
		this.loyaltyDealId = loyaltyDealId;
	}

	/**
	 * to get rebateId.
	 * 
	 * @return rebateId
	 */
	public Integer getRebateId()
	{
		return rebateId;
	}

	/**
	 * to set rebateId.
	 * 
	 * @param rebateId
	 *            to be set.
	 */
	public void setRebateId(Integer rebateId)
	{
		this.rebateId = rebateId;
	}

	/**
	 * for getting the productId.
	 * @return the productId
	 */
	public Integer getProductId()
	{
		return productId;
	}

	/**
	 * for setting the productId. 
	 * @param productId the productId to set
	 */
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 * @return the merchantId
	 */
	public String getMerchantId()
	{
		return merchantId;
	}

	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(String merchantId)
	{
		this.merchantId = merchantId;
	}

	public String getCardNumber()
	{
		return cardNumber;
	}

	public void setCardNumber(String cardNumber)
	{
		this.cardNumber = cardNumber;
	}

	public Integer getCoupListID() {
		return coupListID;
	}

	public void setCoupListID(Integer coupListID) {
		this.coupListID = coupListID;
	}
}
