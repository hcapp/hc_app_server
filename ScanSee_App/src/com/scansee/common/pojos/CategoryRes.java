package com.scansee.common.pojos;


/**
 * The CategoryRes class contains setter and getter methods.
 * @author murali_pnvb
 *
 */
public class CategoryRes {
   
	/**
	 * The categoryName declared as string.
	 */
	private String categoryName;
	/**
	 * The categoryName declared as string.
	 */
	private String categoryID;
	/**
	 * To get categoryName.
	 * @return categoryName to get.
	 */
	public String getCategoryName()
	{
		return categoryName;
	}
    /**
     * To set categoryName.
     * @param categoryName 
     *           -To set
     */
	public void setCategoryName(String categoryName)
	{
		this.categoryName = categoryName;
	}
	/**
	 * @return the categoryID
	 */
	public String getCategoryID()
	{
		return categoryID;
	}
	/**
	 * @param categoryID the categoryID to set
	 */
	public void setCategoryID(String categoryID)
	{
		this.categoryID = categoryID;
	}
	

}
