package com.scansee.common.pojos;

public class UserSettings {

	private Integer userId = null;
	private Integer localeRadius = null;
	private Boolean pushNotify = null;
	private String deviceId = null;
	
	/**
	 * get userId
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * set userId
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	/**
	 * get wish list radius
	 */
	public Integer getLocaleRadius() {
		return localeRadius;
	}
	/**
	 * set wish list radius
	 */
	public void setLocaleRadius(Integer localeRadius) {
		this.localeRadius = localeRadius;
	}
	
	/**
	 * get push notification alert on/off
	 */
	public Boolean getPushNotify() {
		return pushNotify;
	}
	/**
	 * set push notification alert on/off
	 * @param PushNotify
	 */
	public void setPushNotify(Boolean pushNotify) {
		this.pushNotify = pushNotify;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
}
