package com.scansee.common.pojos;
import java.util.List;
public class CellFireInfoValueList
{
	
	int userId;
	
	List<CellFireInfoValue> infoValList;



	public List<CellFireInfoValue> getInfoValList()
	{
		return infoValList;
	}

	public void setInfoValList(List<CellFireInfoValue> infoValList)
	{
		this.infoValList = infoValList;
	}

	public int getUserId()
	{
		return userId;
	}

	public void setUserId(int userId)
	{
		this.userId = userId;
	}
	
	
	
}
