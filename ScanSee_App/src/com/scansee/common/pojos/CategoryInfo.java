package com.scansee.common.pojos;

import java.util.ArrayList;
import java.util.List;

import com.scansee.common.constants.ApplicationConstants;

/**
 * For hotdeals category information.
 * 
 * @author shyamsundara_hm
 */
public class CategoryInfo
{
	/**
	 * for categoryID.
	 */
	private Integer categoryID;
	/**
	 * for parentCategoryName.
	 */
	private String parentCategoryName;
	/**
	 * for parentCategoryID.
	 */
	private Integer parentCategoryID;

	/**
	 * for category name
	 */
	private String categoryName;
	
	/**
	 * For categort image path
	 */
	private String catImg;
	
	private ArrayList<CouponDetails> couponDetailsList;
	
	private String busCatName;
	
	private Integer busCatId;
	
	private ArrayList<HotDealsDetails> hDDetailsList;
	
	/**
	 * for categoryID.
	 * 
	 * @return the categoryID
	 */
	public Integer getCategoryID()
	{
		return categoryID;
	}

	/**
	 * To set categoryID.
	 * 
	 * @param categoryID
	 *            the categoryID to set
	 */
	public void setCategoryID(Integer categoryID)
	{
		this.categoryID = categoryID;
	}

	/**
	 * To get parentCategoryName.
	 * 
	 * @return the parentCategoryName
	 */
	public String getParentCategoryName()
	{
		return parentCategoryName;
	}

	/**
	 * To set parentCategoryName.
	 * 
	 * @param parentCategoryName
	 *            the parentCategoryName to set
	 */
	public void setParentCategoryName(String parentCategoryName)
	{
		this.parentCategoryName = parentCategoryName;
	}

	/**
	 * To get parentCategoryID.
	 * 
	 * @return the parentCategoryID
	 */
	public Integer getParentCategoryID()
	{
		return parentCategoryID;
	}

	/**
	 * To set parentCategoryID.
	 * 
	 * @param parentCategoryID
	 *            the parentCategoryID to set
	 */
	public void setParentCategoryID(Integer parentCategoryID)
	{
		this.parentCategoryID = parentCategoryID;
	}

	public String getCategoryName()
	{
		return categoryName;
	}

	public void setCategoryName(String categoryName)
	{

		if (categoryName == null || "".equals(categoryName))
		{
			this.categoryName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.categoryName = "<![CDATA[" + categoryName + "]]>";
		}
	}

	/**
	 * To get category image path
	 * @return catImg
	 */
	public String getCatImg() {
		return catImg;
	}

	/**
	 * To set category image path
	 * @param catImg
	 */
	public void setCatImg(String catImg) {
		this.catImg = catImg;
	}

	public ArrayList<CouponDetails> getCouponDetailsList() {
		return couponDetailsList;
	}

	public void setCouponDetailsList(ArrayList<CouponDetails> couponDetailsList) {
		this.couponDetailsList = couponDetailsList;
	}

	public String getBusCatName() {
		return busCatName;
	}

	public void setBusCatName(String busCatName) {
		if(null != busCatName)	{
			this.busCatName = busCatName;
		}
		else	{
			this.busCatName = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	public Integer getBusCatId() {
		return busCatId;
	}

	public void setBusCatId(Integer busCatId) {
		this.busCatId = busCatId;
	}

	public ArrayList<HotDealsDetails> gethDDetailsList() {
		return hDDetailsList;
	}

	public void sethDDetailsList(ArrayList<HotDealsDetails> hDDetailsList) {
		this.hDDetailsList = hDDetailsList;
	}

//	public ArrayList<CategoryInfo> getCatInfoList() {
//		return catInfoList;
//	}
//
//	public void setCatInfoList(ArrayList<CategoryInfo> catInfoList) {
//		this.catInfoList = catInfoList;
//	}

}
