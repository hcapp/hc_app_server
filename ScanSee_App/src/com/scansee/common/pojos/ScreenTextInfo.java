package com.scansee.common.pojos;

import java.util.ArrayList;

import com.scansee.common.constants.ApplicationConstants;

/**
 *This pojo for handling First Use Welcome screen information.
 * 
 * @author shyamsundara_hm
 */

public class ScreenTextInfo
{

	/**
	 * Variable sectionContent declared as ArrayList. 
	 */
	private ArrayList<SectionContent> sectionContent;

	/**
	 * welcomeImage variable declared as String.
	 */
	private String welcomeImage;

	/**
	 * WelcomeVideo variable declared as String.
	 */

	private String WelcomeVideo;

	/**
	 * for Screen name.
	 */
	private String screenName;

	/**
	 * for getting screen Name.
	 * 
	 * @return the screenName
	 */
	public String getScreenName()
	{
		return screenName;
	}

	/**
	 * for setting the screenName.
	 * 
	 * @param screenName
	 *            the screenName to set
	 */
	public void setScreenName(String screenName)
	{
		if (screenName == null)
		{
			this.screenName = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.screenName = screenName;
		}
	}

	/**
	 * for getting the SectionContent.
	 * 
	 * @return the sectionContent
	 */
	public ArrayList<SectionContent> getSectionContent()
	{
		return sectionContent;
	}

	/**
	 * for setting the SectionContent.
	 * 
	 * @param sectionContent
	 *            the sectionContent to set
	 */
	public void setSectionContent(ArrayList<SectionContent> sectionContent)
	{
		this.sectionContent = sectionContent;
	}

	/**
	 * for getting the welcomeImage.
	 * 
	 * @return the welcomeImage
	 */
	public String getWelcomeImage()
	{
		return welcomeImage;
	}

	/**
	 * for setting the welcomeImage.
	 * 
	 * @param welcomeImage
	 *            the welcomeImage to set
	 */
	public void setWelcomeImage(String welcomeImage)
	{
		this.welcomeImage = welcomeImage;
	}

	/**
	 * for getting the welcome Video.
	 * 
	 * @return the welcomeVideo
	 */
	public String getWelcomeVideo()
	{
		return WelcomeVideo;
	}

	/**
	 * for setting the welcomeVideo.
	 * 
	 * @param welcomeVideo
	 *            the welcomeVideo to set
	 */
	public void setWelcomeVideo(String welcomeVideo)
	{
		WelcomeVideo = welcomeVideo;
	}

}
