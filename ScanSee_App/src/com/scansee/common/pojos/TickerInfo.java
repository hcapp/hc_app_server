package com.scansee.common.pojos;

import com.scansee.common.constants.ApplicationConstants;

/**
 * This class contains setter and getter methods for fields.
 * @author shyamsundhar_hm
 *
 */
public class TickerInfo
{

	/**
	 * The ticker declared as String.
	 */
	private String ticker;

	/**
	 * To get ticker.
	 * @return the ticker
	 */
	public String getTicker()
	{
		return ticker;
	}

	/**
	 * To set ticker.If ticker is null the value will be not applicable.
	 * @param ticker the ticker to set
	 */
	public void setTicker(String ticker)
	{
		if(ticker==null)
		{
			this.ticker=ApplicationConstants.NOTAPPLICABLE;
		}else{
		this.ticker = ticker;
		}
	}
}
