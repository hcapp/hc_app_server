package com.scansee.common.pojos;

import java.util.ArrayList;
/**
 * Thye ProductReviews contains setter and getter methods for review fields.
 * @author dileepa_cc
 *
 */
public class ProductReviews
{
	
	/**
	 * The totalReviews declared as String.
	 */
	private String totalReviews;
	/**
	 * The productReviewslist declared as ArrayList.
	 */
	private ArrayList<ProductReview> productReviewslist;
	
	/**
	 * To get totalReviews property.
	 * @return the totalReviews
	 */
	public String getTotalReviews()
	{
		return totalReviews;
	}
	/**
	 * To set totalReviews property.
	 * @param totalReviews the totalReviews to set
	 */
	public void setTotalReviews(String totalReviews)
	{
		this.totalReviews = totalReviews+" "+"Reviews";
	}
	/**
	 * To get productReviewslist property.
	 * @return the productReviewslist
	 */
	public ArrayList<ProductReview> getProductReviewslist()
	{
		return productReviewslist;
	}
	/**
	 * To set productReviewslist property.
	 * @param productReviewslist the productReviewslist to set
	 */
	public void setProductReviewslist(ArrayList<ProductReview> productReviewslist)
	{
		this.productReviewslist = productReviewslist;
	}
	
}
