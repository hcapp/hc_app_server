package com.scansee.common.pojos;

import com.scansee.common.constants.ApplicationConstants;

/**
 * this POJO is created for QR code special offers.
 * 
 * @author shyamsundara_hm
 */
public class RetailerCreatedPages
{

	/**
	 * for page link
	 */
	private String pageLink;

	/**
	 * for pagetitle
	 */
	private String pageTitle;
	/**
	 * for retailer image;
	 */
	private String pageImage;

	/**
	 * for qr page id
	 */
	private Long pageID;
	
	/**
	 * For retailer list ID
	 */
	private Integer retListID;
	
	/**
	 * For anything page list ID
	 */
	private Integer anythingPageListID;

	public String getPageLink()
	{
		return pageLink;
	}

	public void setPageLink(String pageLink)
	{
		if (pageLink == null || pageLink.equals(""))
		{
			this.pageLink = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.pageLink = pageLink;
		}
	}

	public String getPageTitle()
	{
		return pageTitle;
	}

	public void setPageTitle(String pageTitle)
	{
		if (pageTitle == null || pageTitle.equals(""))
		{
			this.pageTitle = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.pageTitle = pageTitle;
		}
	}

	public Long getPageID()
	{
		return pageID;
	}

	public void setPageID(Long pageID)
	{
		this.pageID = pageID;
	}

	public String getPageImage()
	{
		return pageImage;
	}

	public void setPageImage(String pageImage)
	{
		if (pageImage == null || pageImage.equals(""))
		{
			this.pageImage = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.pageImage = pageImage;
		}
	}

	/**
	 * 
	 * @return retListID
	 */
	public Integer getRetListID() {
		return retListID;
	}

	/**
	 * 
	 * @param retListID
	 */
	public void setRetListID(Integer retListID) {
		this.retListID = retListID;
	}

	/**
	 * 
	 * @return anythingPageListID
	 */
	public Integer getAnythingPageListID() {
		return anythingPageListID;
	}

	/**
	 * 
	 * @param anythingPageListID
	 */
	public void setAnythingPageListID(Integer anythingPageListID) {
		this.anythingPageListID = anythingPageListID;
	}
}
