package com.scansee.common.pojos;

/**
 *  pojo class for Email Decline.
 * @author pradip_k
 *
 */
public class EmailDecline
{
	/**
	 * screenName declared as String.
	 */
	private String screenName;

	/**
	 * screenContent declared as String.
	 */
	private String screenContent;

	/**
	 * configurationType declared as String.
	 */

	private String configurationType;

	/**
	 * for getting the configuration Type.
	 * 
	 * @return the configurationType
	 */

	public String getConfigurationType()
	{
		return configurationType;
	}

	/**
	 * for setting configuration Type.
	 * 
	 * @param configurationType
	 *            the configurationType to set
	 */
	public void setConfigurationType(String configurationType)
	{
		this.configurationType = configurationType;
	}

	/**
	 * for getting the Screen Name.
	 * 
	 * @return the screenName
	 */

	public String getScreenName()
	{
		return screenName;
	}

	/**
	 * for setting the screen Name.
	 * 
	 * @param screenName
	 *            the screenName to set
	 */
	public void setScreenName(String screenName)
	{
		this.screenName = screenName;
	}

	/**
	 * for getting the screen content.
	 * 
	 * @return the screenContent
	 */
	public String getScreenContent()
	{
		return screenContent;
	}

	/**
	 * for setting the ScreenContent.
	 * 
	 * @param screenContent
	 *            the screenContent to set
	 */

	public void setScreenContent(String screenContent)
	{
		this.screenContent = screenContent;
	}
}
