package com.scansee.common.pojos;

import java.util.List;
/**
 * The POJO class for HotDealsCategoryInfo.
 * 
 * @author shyamsundara_hm.
 */
public class HotDealsListResultSet
{
	
	/**
	 * for category flag
	 */
	private Integer categoryFlag;
	/**
	 * Flag for Pagination which tell whether next set of records available or not.
	 */
	private Integer nextPage;
	
	/**
	 * Variable FavCat declared as integer.
	 */
	private Integer FavCat;
	
	/**
	 * Variable hotDealsCategoryInfo declared as List.
	 */
	private List<HotDealsCategoryInfo> hotDealsCategoryInfo;
	
	
	private List<HotDealsResultSet> hotDealsListResponselst;
	
	private List<HotDealAPIResultSet> hdAPIResult;
	
	private List<Category> category;
	
	private Integer mainMenuID;
	
	private List<HotDealsDetails> hdDetailsList;

	private List<CategoryInfo> categoryInfoList;
	
	private List<RetailersDetails> retDetailsList;
	
	private Integer maxCnt;
	
	private Integer maxRowNum;
	
	
	
	/**
	 * Gets the value of favCat.
	 * @return the favCat
	 */
	public Integer getFavCat()
	{
		return FavCat;
	}

	/**
	 * Sets the value of favCat.
	 * @param favCat the favCat to set
	 */
	public void setFavCat(Integer favCat)
	{
		FavCat = favCat;
	}

	/**
	 * Gets the value of nextPage.
	 * @return the nextPage
	 */
	public Integer getNextPage()
	{
		return nextPage;
	}

	/**
	 * Sets the value of nextPage.
	 * @param nextPage the nextPage to set
	 */
	public void setNextPage(Integer nextPage)
	{
		this.nextPage = nextPage;
	}


	/**
	 * Gets the value of the hotDealsCategoryInfo property.
	 * 
	 * @return the hotDealsCategoryInfo
	 */
	public List<HotDealsCategoryInfo> getHotDealsCategoryInfo()
	{
		return hotDealsCategoryInfo;
	}

	/**
	 * Sets the value of the hotDealsCategoryInfo property.
	 * @param hotDealsCategoryInfo as of type List.
	 * 
	 */
	public void setHotDealsCategoryInfo(List<HotDealsCategoryInfo> hotDealsCategoryInfo)
	{
		this.hotDealsCategoryInfo = hotDealsCategoryInfo;
	}

	/**This method return hotDealsListResponselst value.
	 * @return the hotDealsListResponselst
	 */
	public List<HotDealsResultSet> getHotDealsListResponselst()
	{
		return hotDealsListResponselst;
	}

	/** This method set the value to hotDealsListResponselst.
	 * @param hotDealsListResponselst the hotDealsListResponselst to set
	 */
	public void setHotDealsListResponselst(List<HotDealsResultSet> hotDealsListResponselst)
	{
		this.hotDealsListResponselst = hotDealsListResponselst;
	}

	public Integer getCategoryFlag()
	{
		return categoryFlag;
	}

	public void setCategoryFlag(Integer categoryFlag)
	{
		this.categoryFlag = categoryFlag;
	}

	/*
	 * private String categoryName; private Integer categoryId; private Integer
	 * lastVistedProductNo; private List<HotDealsDetails> hotDealsDetails;
	 * public String getCategoryName() { return categoryName; } public void
	 * setCategoryName(String categoryName) { this.categoryName = categoryName;
	 * } public Integer getCategoryId() { return categoryId; } public void
	 * setCategoryId(Integer categoryId) { this.categoryId = categoryId; }
	 *//**
	 * @return the hotDealsDetails
	 */
	/*
	 * public List<HotDealsDetails> getHotDealsDetails() { return
	 * hotDealsDetails; }
	 *//**
	 * @param hotDealsDetails
	 *            the hotDealsDetails to set
	 */
	/*
	 * public void setHotDealsDetails(List<HotDealsDetails> hotDealsDetails) {
	 * this.hotDealsDetails = hotDealsDetails; }
	 *//**
	 * @return the lastVistedProductNo
	 */
	/*
	 * public Integer getLastVistedProductNo() { return lastVistedProductNo; }
	 *//**
	 * @param lastVistedProductNo
	 *            the lastVistedProductNo to set
	 */
	/*
	 * public void setLastVistedProductNo(Integer lastVistedProductNo) {
	 * this.lastVistedProductNo = lastVistedProductNo; }
	 */

	public Integer getMainMenuID() {
		return mainMenuID;
	}

	public void setMainMenuID(Integer mainMenuID) {
		this.mainMenuID = mainMenuID;
	}

	/**
	 * @return the category
	 */
	public List<Category> getCategory()
	{
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(List<Category> category)
	{
		this.category = category;
	}

	public List<HotDealsDetails> getHdDetailsList() {
		return hdDetailsList;
	}

	public void setHdDetailsList(List<HotDealsDetails> hdDetailsList) {
		this.hdDetailsList = hdDetailsList;
	}

	public Integer getMaxCnt() {
		return maxCnt;
	}

	public void setMaxCnt(Integer maxCnt) {
		this.maxCnt = maxCnt;
	}

	public List<CategoryInfo> getCategoryInfoList() {
		return categoryInfoList;
	}

	public void setCategoryInfoList(List<CategoryInfo> categoryInfoList) {
		this.categoryInfoList = categoryInfoList;
	}

	public List<RetailersDetails> getRetDetailsList() {
		return retDetailsList;
	}

	public void setRetDetailsList(List<RetailersDetails> retDetailsList) {
		this.retDetailsList = retDetailsList;
	}

	public Integer getMaxRowNum() {
		return maxRowNum;
	}

	public void setMaxRowNum(Integer maxRowNum) {
		this.maxRowNum = maxRowNum;
	}

	public List<HotDealAPIResultSet> getHdAPIResult() {
		return hdAPIResult;
	}

	public void setHdAPIResult(List<HotDealAPIResultSet> hdAPIResult) {
		this.hdAPIResult = hdAPIResult;
	}


}
