package com.scansee.common.pojos;

import com.scansee.common.constants.ApplicationConstants;

public class CellFireMerchant
{
	private String retailId;
	private String merchId;
	private String merchName;
	private String merchLogoSmlImg;
	private String merchLogoBigImg;
	private String cardNumber;
	private String loyaltyProgramName;
	private String cardSupportMessage;
	private String loyaltySmallImg;
	private String loyaltyLargeImg;
	private String cardInfo;
	/**
	 * @return the retailId
	 */
	public String getRetailId()
	{
		return retailId;
	}
	/**
	 * @param retailId the retailId to set
	 */
	public void setRetailId(String retailId)
	{
		this.retailId = retailId;
	}
	/**
	 * @return the merchId
	 */
	public String getMerchId()
	{
		return merchId;
	}
	/**
	 * @param merchId the merchId to set
	 */
	public void setMerchId(String merchId)
	{
		this.merchId = merchId;
	}
	/**
	 * @return the merchName
	 */
	public String getMerchName()
	{
		return merchName;
	}
	/**
	 * @param merchName the merchName to set
	 */
	public void setMerchName(String merchName)
	{
		this.merchName = merchName;
	}
	/**
	 * @return the merchLogoSmlImg
	 */
	public String getMerchLogoSmlImg()
	{
		return merchLogoSmlImg;
	}
	/**
	 * @param merchLogoSmlImg the merchLogoSmlImg to set
	 */
	public void setMerchLogoSmlImg(String merchLogoSmlImg)
	{
		this.merchLogoSmlImg = merchLogoSmlImg;
	}
	/**
	 * @return the merchLogoBigImg
	 */
	public String getMerchLogoBigImg()
	{
		return merchLogoBigImg;
	}
	/**
	 * @param merchLogoBigImg the merchLogoBigImg to set
	 */
	public void setMerchLogoBigImg(String merchLogoBigImg)
	{
		this.merchLogoBigImg = merchLogoBigImg;
	}
	/**
	 * @return the cardNumber
	 */
	public String getCardNumber()
	{
		return cardNumber;
	}
	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber)
	{
		if( null == cardNumber || "".equals(cardNumber)){
			this.cardNumber = ApplicationConstants.NOTAPPLICABLE;
		}else{
			this.cardNumber = cardNumber;	
		}
		
	}
	public String getLoyaltyProgramName()
	{
		return loyaltyProgramName;
	}
	public void setLoyaltyProgramName(String loyaltyProgramName)
	{
		this.loyaltyProgramName = loyaltyProgramName;
	}
	public String getCardSupportMessage()
	{
		return cardSupportMessage;
	}
	public void setCardSupportMessage(String cardSupportMessage)
	{
		this.cardSupportMessage = cardSupportMessage;
	}
	public String getLoyaltySmallImg()
	{
		return loyaltySmallImg;
	}
	public void setLoyaltySmallImg(String loyaltySmallImg)
	{
		this.loyaltySmallImg = loyaltySmallImg;
	}
	public String getLoyaltyLargeImg()
	{
		return loyaltyLargeImg;
	}
	public void setLoyaltyLargeImg(String loyaltyLargeImg)
	{
		this.loyaltyLargeImg = loyaltyLargeImg;
	}
	public String getCardInfo()
	{
		return cardInfo;
	}
	public void setCardInfo(String cardInfo)
	{
		this.cardInfo = cardInfo;
	}

	
	
	
}
