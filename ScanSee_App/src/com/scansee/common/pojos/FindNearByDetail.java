package com.scansee.common.pojos;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.util.Utility;

/**
 * this pojo for handling find near by details.
 * 
 * @author shyamsundara_hm
 */
public class FindNearByDetail extends BaseObject
{

	/**
	 * for retailerName.
	 */
	private String retailerName;
	/**
	 * for distance.
	 */
	private String distance;
	/**
	 * for productName.
	 */
	private String productName;
	/**
	 * for productPrice.
	 */
	private String productPrice;
	/**
	 * for retailerId.
	 */
	private Integer retailerId;
	/**
	 * for imagePath.
	 */
	private String imagePath;
	/**
	 * for latitude.
	 */
	private double latitude;
	/**
	 * for longitude.
	 */
	private double longitude;
	
	private Integer retListID;

	/**
	 * to get retailerName.
	 * 
	 * @return retailerName
	 */
	public String getRetailerName()
	{
		return retailerName;
	}

	/**
	 * for setting retailerName.
	 * 
	 * @param retailerName
	 *            to be set.
	 */
	public void setRetailerName(String retailerName)
	{
		this.retailerName = retailerName;
	}

	

	/**
	 * for getting productName.
	 * 
	 * @return productName
	 */
	public String getProductName()
	{
		return productName;
	}

	/**
	 * for setting productName.
	 * 
	 * @param productName
	 *            to be set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}

	/**
	 * for getting productPrice.
	 * 
	 * @return productPrice
	 */
	public String getProductPrice()
	{
		return productPrice;
	}

	/**
	 * for setting productPrice.
	 * 
	 * @param productPrice
	 *            to be set
	 */
	public void setProductPrice(String productPrice)
	{
		this.productPrice = Utility.formatDecimalValue(productPrice);
	}

	/**
	 * for getting retailerId.
	 * 
	 * @return retailerId
	 */
	public Integer getRetailerId()
	{
		return retailerId;
	}

	/**
	 * for setting retailerId.
	 * 
	 * @param retailerId
	 *            to be set
	 */
	public void setRetailerId(Integer retailerId)
	{
		this.retailerId = retailerId;
	}

	/**
	 * to get imagePath.
	 * 
	 * @return imagePath
	 */
	public String getImagePath()
	{
		return imagePath;
	}

	/**
	 * for setting imagePath.
	 * 
	 * @param imagePath
	 *            to be set
	 */
	public void setImagePath(String imagePath)
	{
		if(imagePath == null || "".equals(imagePath))
		{
			this.imagePath=ApplicationConstants.IMAGENOTFOUND;
		}else{
		this.imagePath = imagePath;
		}
	}

	/**
	 * for getting latitude.
	 * 
	 * @return latitude
	 */
	public double getLatitude()
	{
		return latitude;
	}

	/**
	 * for setting latitude.
	 * 
	 * @param latitude
	 *            to be set
	 */
	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}

	/**
	 * for getting longitude.
	 * 
	 * @return longitude
	 */
	public double getLongitude()
	{
		return longitude;
	}

	/**
	 * for setting longitude.
	 * 
	 * @param longitude
	 *            to be set
	 */
	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}

	public String getDistance()
	{
		return distance;
	}

	public void setDistance(String distance)
	{
		if(distance !=null)
		{
			this.distance =  Utility.formatDecimalValue(distance);
			}
	}

	public Integer getRetListID() {
		return retListID;
	}

	public void setRetListID(Integer retListID) {
		this.retListID = retListID;
	}

}
