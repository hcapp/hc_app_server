package com.scansee.common.pojos;

/**
 * This pojo for add ,remove shopping basket products.
 * 
 * @author shyamsundara_hm
 */
public class AddRemoveSBProducts extends BaseObject
{
	/**
	 * for userId.
	 */
	protected String userId;
	/**
	 * for ShoppingCartProducts reference cartProducts.
	 */

	protected ShoppingCartProducts cartProducts;
	/**
	 * for ShoppingBasketProducts reference basketProducts.
	 */

	protected ShoppingBasketProducts basketProducts;
	
	/**
	 * For mainMenuID
	 */
	private Integer mainMenuID;

	/**
	 * For maxCount
	 */
	private Integer maxCount;
	/**
	 * For NextPage
	 */
	private Integer NextPage;
	/**
	 * to get userId.
	 * 
	 * @return userId
	 */

	public String getUserId()
	{
		return userId;
	}

	/**
	 * to set userId.
	 * 
	 * @param userId
	 *            to be set.
	 */
	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	/**
	 * to get shopping cart products.
	 * 
	 * @return cartProducts
	 */
	public ShoppingCartProducts getCartProducts()
	{
		return cartProducts;
	}

	/**
	 * for setting cartProducts.
	 * 
	 * @param cartProducts
	 *            to be set.
	 */
	public void setCartProducts(ShoppingCartProducts cartProducts)
	{
		this.cartProducts = cartProducts;
	}

	/**
	 * for getting ShoppingBasketProducts.
	 * 
	 * @return basketProducts
	 */
	public ShoppingBasketProducts getBasketProducts()
	{
		return basketProducts;
	}

	/**
	 * for setting basket products.
	 * 
	 * @param basketProducts
	 *            to be set.
	 */

	public void setBasketProducts(ShoppingBasketProducts basketProducts)
	{
		this.basketProducts = basketProducts;
	}
	
	public Integer getMainMenuID() {
		return mainMenuID;
	}

	public void setMainMenuID(Integer mainMenuID) {
		this.mainMenuID = mainMenuID;
	}

	/**
	 * @return the maxCount
	 */
	public Integer getMaxCount()
	{
		return maxCount;
	}

	/**
	 * @param maxCount the maxCount to set
	 */
	public void setMaxCount(Integer maxCount)
	{
		this.maxCount = maxCount;
	}

	/**
	 * @return the nextPage
	 */
	public Integer getNextPage()
	{
		return NextPage;
	}

	/**
	 * @param nextPage the nextPage to set
	 */
	public void setNextPage(Integer nextPage)
	{
		NextPage = nextPage;
	}
}
