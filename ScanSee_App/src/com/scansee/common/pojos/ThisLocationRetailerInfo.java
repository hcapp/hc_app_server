package com.scansee.common.pojos;

import com.scansee.common.constants.ApplicationConstants;

/**
 * The ThisLocationRetailerInfo contains setter and getter methods for ThisLocationRetailerInfo fields.
 * @author dileepa_cc
 *
 */
public class ThisLocationRetailerInfo
{
	/**
	 * The rowNumber property.
	 */

	private Integer rowNumber;

	/**
	 * The retailerId property.
	 */

	private Integer retailerId;

	/**
	 * The retailerName property.
	 */

	private String retailerName;

	/**
	 * The retailLocationID property.
	 */
	private Integer retailLocationID;
	/**
	 * Variable for distance.
	 */
	private String distance;
	
	/**
	 * Variable for retailAddress.
	 */
	private String retailAddress;
	/**
	 * The imagePath property.
	 */

	private String logoImagePath;

	/**
	 * The bannerAdImagePath property.
	 */

	private String bannerAdImagePath;
	/**
	 * The ribbonAdImagePath property.
	 */

	private String ribbonAdImagePath;
	/**
	 * @return the rowNumber
	 */
	
	/**
	 * The ribbonAdURL property.
	 */

	private String ribbonAdURL;
	
	/**
	 * for sale flag to indicate retailer has sale or not
	 */
	private Boolean saleFlag;
	
	/**
	 * For latitude
	 */
	private String latitude;
	
	/**
	 * For longitude
	 */
	private String longitude;
	
	/**
	 * for ribben ad url
	 */
	private String splashAdID;
	
	/**
	 * For retailer List ID
	 */
	private Integer retListID;
	
	/**
	 * For retailer group button image path
	 */
	private String retGroupImg;
	
	/**
	 * To get ribbonAdURL.
	 * @return the ribbonAdURL
	 */
	public String getRibbonAdURL()
	{
		return ribbonAdURL;
	}
	/**
	 * To set ribbonAdURL.
	 * @param ribbonAdURL the ribbonAdURL to set
	 */
	public void setRibbonAdURL(String ribbonAdURL)
	{
		this.ribbonAdURL = ribbonAdURL;
	}
	
	/**
	 * To get rowNumber.
	 * @return rowNumber To get
	 */
	public Integer getRowNumber()
	{
		return rowNumber;
	}
	/**
	 * To set rowNumber.
	 * @param rowNumber the rowNumber to set
	 */
	public void setRowNumber(Integer rowNumber)
	{
		this.rowNumber = rowNumber;
	}
	/**
	 *  To get retailerId.
	 * @return the retailerId
	 */
	public Integer getRetailerId()
	{
		return retailerId;
	}
	/**
	 * To set retailerId.
	 * @param retailerId the retailerId to set
	 */
	public void setRetailerId(Integer retailerId)
	{
		this.retailerId = retailerId;
	}
	/**
	 *  To get retailerName.
	 * @return the retailerName
	 */
	public String getRetailerName()
	{
		return retailerName;
	}
	/**
	 * To set retailerName.
	 * @param retailerName the retailerName to set
	 */
	public void setRetailerName(String retailerName)
	{
		this.retailerName = retailerName;
	}
	/**
	 *  To get retailLocationID.
	 * @return the retailLocationID
	 */
	public Integer getRetailLocationID()
	{
		return retailLocationID;
	}
	/**
	 * To set retailLocationID.
	 * @param retailLocationID the retailLocationID to set
	 */
	public void setRetailLocationID(Integer retailLocationID)
	{
		this.retailLocationID = retailLocationID;
	}
	/**
	 *  To get distance.
	 * @return the distance
	 */
	public String getDistance()
	{
		return distance;
	}
	/**
	 * To set distance.
	 * @param distance the distance to set
	 */
	public void setDistance(String distance)
	{
		this.distance = distance;
	}
	/**
	 *  To get retailAddress.
	 * @return the retailAddress
	 */
	public String getRetailAddress()
	{
		return retailAddress;
	}
	/**
	 * To set retailAddress.
	 * @param retailAddress the retailAddress to set
	 */
	public void setRetailAddress(String retailAddress)
	{
		this.retailAddress = retailAddress;
	}
	/**
	 *  To get logoImagePath.
	 * @return the logoImagePath
	 */
	public String getLogoImagePath()
	{
		return logoImagePath;
	}
	/**
	 * To set logoImagePath.
	 * @param logoImagePath the logoImagePath to set
	 */
	public void setLogoImagePath(String logoImagePath)
	{
		if(logoImagePath==null)
		{
			this.logoImagePath=ApplicationConstants.IMAGENOTFOUND;
		}else{
		this.logoImagePath = logoImagePath;
		}
	}
	/**
	 *  To get bannerAdImagePath.
	 * @return the bannerAdImagePath
	 */
	public String getBannerAdImagePath()
	{
		return bannerAdImagePath;
	}
	/**
	 * To set bannerAdImagePath.
	 * @param bannerAdImagePath the bannerAdImagePath to set
	 */
	public void setBannerAdImagePath(String bannerAdImagePath)
	{
		if(bannerAdImagePath==null)
		{
			this.bannerAdImagePath=ApplicationConstants.IMAGENOTFOUND;
		}else
		{
		this.bannerAdImagePath = bannerAdImagePath;
		}
	}
	/**
	 *  To get ribbonAdImagePath.
	 * @return the ribbonAdImagePath
	 */
	public String getRibbonAdImagePath()
	{
		return ribbonAdImagePath;
	}
	/**
	 * To set ribbonAdImagePath.
	 * @param ribbonAdImagePath the ribbonAdImagePath to set
	 */
	public void setRibbonAdImagePath(String ribbonAdImagePath)
	{
		if(ribbonAdImagePath==null)
		{
			this.ribbonAdImagePath=ApplicationConstants.IMAGENOTFOUND;
		}else{
		this.ribbonAdImagePath = ribbonAdImagePath;
		}
	}
	public Boolean getSaleFlag()
	{
		return saleFlag;
	}
	public void setSaleFlag(Boolean saleFlag)
	{
		this.saleFlag = saleFlag;
	}
	public String getSplashAdID()
	{
		return splashAdID;
	}
	public void setSplashAdID(String splashAdID)
	{
		this.splashAdID = splashAdID;
	}
	
	public String getLatitude() {
		return latitude;
	}
	
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	public String getLongitude() {
		return longitude;
	}
	
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Integer getRetListID() {
		return retListID;
	}
	
	public void setRetListID(Integer retListID) {
		this.retListID = retListID;
	}
	
	public String getRetGroupImg() {
		return retGroupImg;
	}
	
	public void setRetGroupImg(String retGroupImg) {
		this.retGroupImg = retGroupImg;
	}
}
