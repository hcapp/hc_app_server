package com.scansee.common.pojos;

import java.util.List;
/**
 * This pojo for listing find nearby details.
 * @author shyamsundara_hm
 *
 */
public class FindNearByDetails extends BaseObject
{
	/**
	 * for findNearby.
	 */
	private boolean findNearBy;
	/**
	 * to list findNearByDetails.
	 */
	private List<FindNearByDetail> findNearByDetail;

	/**
	 * to get findNearBy.
	 * @return the findNearBy
	 */
	public boolean getFindNearBy() {
		return findNearBy;
	}

	/**to set findNearBy.
	 * @param findNearBy the findNearBy to set
	 */
	public void setFindNearBy(boolean findNearBy) {
		this.findNearBy = findNearBy;
	}

	/**
	 * for listing findnearbydetail.
	 * @return findNearByDetail
	 */
	public List<FindNearByDetail> getFindNearByDetail()
	{
		return findNearByDetail;
	}
/**
 * for setting findNearByDetail .
 * @param findNearByDetail to be set
 */
	public void setFindNearByDetail(List<FindNearByDetail> findNearByDetail)
	{
		this.findNearByDetail = findNearByDetail;
	}
	

}
