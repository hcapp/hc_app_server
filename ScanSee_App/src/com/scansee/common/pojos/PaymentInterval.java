package com.scansee.common.pojos;
/**
 * This class  for handling user paymentinterval informaton.
 * @author shyamsundara_hm
 *
 */
public class PaymentInterval extends BaseObject
{
/**
 * for paymentIntervalID.
 */
	    private Integer paymentIntervalID;
	    /**
	     * for payMonthly.
	     */
		private Integer payMonthly;
		/**
		 * for payIntervalAmount.
		 */
		private Double payIntervalAmount;
		
		
		/**
		 * this method for getting paymentIntervalID.
		 * @return the paymentIntervalID
		 */
		public Integer getPaymentIntervalID()
		{
			return paymentIntervalID;
		}
		/**
		 * this method for setting paymentIntervalID.
		 * @param paymentIntervalID the paymentIntervalID to set
		 */
		public void setPaymentIntervalID(Integer paymentIntervalID)
		{
			this.paymentIntervalID = paymentIntervalID;
		}
		/**
		 * this method for getting payMonthly.
		 * @return the payMonthly
		 */
		public Integer getPayMonthly()
		{
			return payMonthly;
		}
		/**
		 * this method for setting payMonthly.
		 * @param payMonthly the payMonthly to set
		 */
		public void setPayMonthly(Integer payMonthly)
		{
			this.payMonthly = payMonthly;
		}
		/**
		 * this method for getting payIntervalAmount.
		 * @return the payIntervalAmount
		 */
		public Double getPayIntervalAmount()
		{
			return payIntervalAmount;
		}
		/**
		 * this method for setting payIntervalAmount.
		 * @param payIntervalAmount the payIntervalAmount to set
		 */
		public void setPayIntervalAmount(Double payIntervalAmount)
		{
			this.payIntervalAmount = payIntervalAmount;
		}
		
}
