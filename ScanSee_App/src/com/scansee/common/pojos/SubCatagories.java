package com.scansee.common.pojos;

import java.util.List;

/**
 * The POJO for subcategories.
 * @author sourab_r
 */

public class SubCatagories

{

	/**
	 * The list for storing the type subCategory.
	 */
	private List<SubCategory> subCategory;

	/**
	 * To get subCategory.
	 * @return The list of type subCategory.
	 */

	public List<SubCategory> getSubCategory()
	{
		return subCategory;
	}

	/**
	 * To set subCategory.
	 * @param subCategory
	 *            The list of type subCategory.
	 */

	public void setSubCategory(List<SubCategory> subCategory)
	{
		this.subCategory = subCategory;
	}

}
