package com.scansee.common.pojos;

/**
 * The POJO class for UserMediaInfo.
 * @author Saurab_r 
 */

public class UserMediaInfo extends BaseObject
{
	/**
	 * The userId in the request.
	 */

	private Integer userId;

	/**
	 * The mediaId in the request.
	 */

	private Integer mediaId;

	/**
	 * To get userId.
	 * @return userId The userId in the request.
	 */

	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * To set userId.
	 * @param userId
	 *            The userId in the request.
	 */

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * To get mediaId.
	 * @return The mediaId in the request.
	 */

	public Integer getMediaId()
	{
		return mediaId;
	}

	/**
	 * To set mediaId.
	 * @param mediaId
	 *            The mediaId in the request.
	 */

	public void setMediaId(Integer mediaId)
	{
		this.mediaId = mediaId;
	}

}
