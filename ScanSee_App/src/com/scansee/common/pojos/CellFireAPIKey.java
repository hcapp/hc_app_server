package com.scansee.common.pojos;

public class CellFireAPIKey
{

	// Declared String for CellFire API-Key parameter name
	String apiKeyParameterName = "Api-Key";

	// Declared String for CellFire API-Key parameter Value
	String apiKeyParameterValue;

	// Declared String for CellFire tracking code parameter name
	String trackingCodeParameterName = "tlc";

	// Declared String for CellFire tracking code parameter Value
	String trackingCodeParameterValue;
	
	
	String parnerId;

	public String getApiKeyParameterName()
	{
		return apiKeyParameterName;
	}

	public void setApiKeyParameterName(String apiKeyParameterName)
	{
		this.apiKeyParameterName = apiKeyParameterName;
	}

	public String getApiKeyParameterValue()
	{
		return apiKeyParameterValue;
	}

	public void setApiKeyParameterValue(String apiKeyParameterValue)
	{
		this.apiKeyParameterValue = apiKeyParameterValue;
	}

	public String getTrackingCodeParameterName()
	{
		return trackingCodeParameterName;
	}

	public void setTrackingCodeParameterName(String trackingCodeParameterName)
	{
		this.trackingCodeParameterName = trackingCodeParameterName;
	}

	public String getTrackingCodeParameterValue()
	{
		return trackingCodeParameterValue;
	}

	public void setTrackingCodeParameterValue(String trackingCodeParameterValue)
	{
		this.trackingCodeParameterValue = trackingCodeParameterValue;
	}

	public String getParnerId()
	{
		return parnerId;
	}

	public void setParnerId(String parnerId)
	{
		this.parnerId = parnerId;
	}
	
	
	
}
