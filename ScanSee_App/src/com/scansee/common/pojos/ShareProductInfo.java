package com.scansee.common.pojos;

/**
 * The POJO class for ShareProductInfo.
 * 
 * @author dileepa_cc
 */

public class ShareProductInfo extends BaseObject
{
	/**
	 * for layalty id
	 */
	private Integer loyaltyId;
	/**
	 * for rebate id
	 */
	private Integer rebateId;
	/**
	 * for hotdeal id
	 */
	private Integer hotdealId;
	/**
	 * for coupon Id
	 */
	private Integer couponId;
	/**
	 * The toEmail property.
	 */

	private String toEmail;

	/**
	 * The productId property.
	 */

	private Integer productId;

	/**
	 * The userId property.
	 */

	private Integer userId;

	/**
	 * The userId property.
	 */

	private Integer isFromThisLocation;
	/**
	 * The retailerId property.
	 */
	private Integer retailerId;

	/**
	 * The retailerLocationId property.
	 */
	private Integer retailerLocationId;

	private String clrFlag;

	/**
	 * for pageID
	 */
	private Long pageId;
	/**
	 * The shareType property.
	 */
	private String shareType;
	/**
	 * The userId property.
	 */

	private Integer mainMenuID;
	/**
	 * For getting isFromThisLocation.
	 * 
	 * @return the isFromThisLocation
	 */
	public Integer getIsFromThisLocation()
	{
		return isFromThisLocation;
	}

	/**
	 * For setting isFromThisLocation.
	 * 
	 * @param isFromThisLocation
	 *            the isFromThisLocation to set
	 */
	public void setIsFromThisLocation(Integer isFromThisLocation)
	{
		this.isFromThisLocation = isFromThisLocation;
	}

	/**
	 * For getting retailerId.
	 * 
	 * @return the retailerId
	 */
	public Integer getRetailerId()
	{
		return retailerId;
	}

	/**
	 * For setting retailerId.
	 * 
	 * @param retailerId
	 *            the retailerId to set
	 */
	public void setRetailerId(Integer retailerId)
	{
		this.retailerId = retailerId;
	}

	/**
	 * For getting retailerLocationId..
	 * 
	 * @return the retailerLocationId
	 */
	public Integer getRetailerLocationId()
	{
		return retailerLocationId;
	}

	/**
	 * For setting retailerLocationId.
	 * 
	 * @param retailerLocationId
	 *            the retailerLocationId to set
	 */
	public void setRetailerLocationId(Integer retailerLocationId)
	{
		this.retailerLocationId = retailerLocationId;
	}

	/**
	 * For getting toEmail.
	 * 
	 * @return toEmail To get
	 */
	public String getToEmail()
	{
		return toEmail;
	}

	/**
	 * For setting toEmail.
	 * 
	 * @param toEmail
	 *            The toEmail property.
	 */

	public void setToEmail(String toEmail)
	{
		this.toEmail = toEmail;
	}

	/**
	 * For getting productId.
	 * 
	 * @return The productId property.
	 */

	public Integer getProductId()
	{
		return productId;
	}

	/**
	 * For setting productId.
	 * 
	 * @param productId
	 *            The productId property.
	 */

	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 * For getting userId.
	 * 
	 * @return The userId property.
	 */

	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * For setting userId.
	 * 
	 * @param userId
	 *            The userId property.
	 */

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	public Integer getCouponId()
	{
		return couponId;
	}

	public void setCouponId(Integer couponId)
	{
		this.couponId = couponId;
	}

	public Integer getHotdealId()
	{
		return hotdealId;
	}

	public void setHotdealId(Integer hotdealId)
	{
		this.hotdealId = hotdealId;
	}

	public String getClrFlag()
	{
		return clrFlag;
	}

	public void setClrFlag(String clrFlag)
	{
		this.clrFlag = clrFlag;
	}

	public Integer getLoyaltyId()
	{
		return loyaltyId;
	}

	public void setLoyaltyId(Integer loyaltyId)
	{
		this.loyaltyId = loyaltyId;
	}

	public Integer getRebateId()
	{
		return rebateId;
	}

	public void setRebateId(Integer rebateId)
	{
		this.rebateId = rebateId;
	}

	public Long getPageId()
	{
		return pageId;
	}

	public void setPageId(Long pageId)
	{
		this.pageId = pageId;
	}

	/**
	 * @return the shareType
	 */
	public String getShareType()
	{
		return shareType;
	}

	/**
	 * @param shareType the shareType to set
	 */
	public void setShareType(String shareType)
	{
		this.shareType = shareType;
	}

	/**
	 * @return the mainMenuID
	 */
	public Integer getMainMenuID()
	{
		return mainMenuID;
	}

	/**
	 * @param mainMenuID the mainMenuID to set
	 */
	public void setMainMenuID(Integer mainMenuID)
	{
		this.mainMenuID = mainMenuID;
	}

}
