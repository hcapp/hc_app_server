package com.scansee.common.pojos;

/**
 *  pojo class for ExternalAPI Search Parameters.
 * @author pradip_k
 *
 */
public class ExternalAPISearchParameters
{
	/**
	 * apiParameter declared as String.
	 */
	private String apiParameter;
	
	/**
	 * apiParamtervalue declared as String.
	 */
	private String apiParamtervalue;
	
	/**
	 * apiDynamic declared as integer.
	 */
	private Integer apiDynamic;

	/**
	 * To get apiParameter property.
	 * @return the apiParameter
	 */
	public String getApiParameter()
	{
		return apiParameter;
	}

	/**
	 * To set apiParameter property.
	 * @param apiParameter
	 *            the apiParameter to set
	 */
	public void setApiParameter(String apiParameter)
	{
		this.apiParameter = apiParameter;
	}

	/**
	 * To get apiParamtervalue property.
	 * @return the apiParamtervalue
	 */
	public String getApiParamtervalue()
	{
		return apiParamtervalue;
	}

	/**
	 * To set apiParamtervalue property.
	 * @param apiParamtervalue
	 *            the apiParamtervalue to set
	 */
	public void setApiParamtervalue(String apiParamtervalue)
	{
		this.apiParamtervalue = apiParamtervalue;
	}

	/**
	 * To get apiDynamic property.
	 * @return the apiDynamic
	 */
	public Integer getApiDynamic()
	{
		return apiDynamic;
	}

	/**
	 * To set apiDynamic property.
	 * @param apiDynamic
	 *            the apiDynamic to set
	 */
	public void setApiDynamic(Integer apiDynamic)
	{
		this.apiDynamic = apiDynamic;
	}

}
