package com.scansee.common.pojos;

/**
 * The POJO class for RetailersRes.
 * @author sourab_r
 */

public class RetailersRes
{
	/**
	 * The retailerId property.
	 */

	private Integer retailerId;
	/**
	 * The retailerName property.
	 */

	private String retailerName;

	/**
	 * To get retailerId property.
	 * @return The retailerId property.
	 */

	public Integer getRetailerId()
	{
		return retailerId;
	}

	/**
	 * To set retailerId.
	 * @param retailerId
	 *            The retailerId property.
	 */

	public void setRetailerId(Integer retailerId)
	{
		this.retailerId = retailerId;
	}

	/**
	 * To get retailerName.
	 * @return The getter for retailerName property.
	 */

	public String getRetailerName()
	{
		return retailerName;
	}

	/**
	 * To set retailerName.
	 * @param retailerName
	 *            The setter for retailerName property.
	 */

	public void setRetailerName(String retailerName)
	{
		this.retailerName = retailerName;
	}

}
