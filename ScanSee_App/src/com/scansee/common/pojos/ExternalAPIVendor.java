package com.scansee.common.pojos;

/**
 *  pojo class for External API Vendor.
 * @author pradip_k
 *
 */
public class ExternalAPIVendor
{
    /**
     * The apiURLPath declared as String.
     */
	private String apiURLPath;
	
	/**
     * The apiUsagePriority declared as String.
     */
	private Integer apiUsagePriority;
	
	/**
     * The apiKey declared as String.
     */
	private String apiKey;
	
	/**
     * The apiUsageID declared as String.
     */
	private String apiUsageID;
	
	/**
     * The apiPartnerID declared as String.
     */
	private String apiPartnerID;
	
	/**
     * The vendorName declared as String.
     */
	private String vendorName;
	
	/**
	 * To get apiURLPath.
	 * @return the apiURLPath
	 */
	public String getApiURLPath()
	{
		return apiURLPath;
	}
	/**
	 * To set apiURLPath.
	 * @param apiURLPath the apiURLPath to set
	 */
	public void setApiURLPath(String apiURLPath)
	{
		this.apiURLPath = apiURLPath;
	}
	/**
	 * To get apiKey.
	 * @return the apiKey
	 */
	public String getApiKey()
	{
		return apiKey;
	}
	/**
	 * To set apiKey.
	 * @param apiKey the apiKey to set
	 */
	public void setApiKey(String apiKey)
	{
		this.apiKey = apiKey;
	}
	/**
	 * To get apiUsageID.
	 * @return the apiUsageID
	 */
	public String getApiUsageID()
	{
		return apiUsageID;
	}
	/**
	 * To set apiUsageID value.
	 * @param apiUsageID the apiUsageID to set
	 */
	public void setApiUsageID(String apiUsageID)
	{
		this.apiUsageID = apiUsageID;
	}
	/**
	 * To get apiPartnerID property.
	 * @return the apiPartnerID
	 */
	public String getApiPartnerID()
	{
		return apiPartnerID;
	}
	/**
	 * To set apiPartnerID property.
	 * @param apiPartnerID the apiPartnerID to set
	 */
	public void setApiPartnerID(String apiPartnerID)
	{
		this.apiPartnerID = apiPartnerID;
	}
	/**
	 * To get vendorName property.
	 * @return the vendorName
	 */
	public String getVendorName()
	{
		return vendorName;
	}
	/**
	 * To set vendorName property.
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName)
	{
		this.vendorName = vendorName;
	}
	/**
	 * To get apiUsagePriority value.
	 * @return the apiUsagePriority
	 */
	public Integer getApiUsagePriority()
	{
		return apiUsagePriority;
	}
	/**
	 * To set apiUsagePriority value.
	 * @param apiUsagePriority the apiUsagePriority to set
	 */
	public void setApiUsagePriority(Integer apiUsagePriority)
	{
		this.apiUsagePriority = apiUsagePriority;
	}


}
