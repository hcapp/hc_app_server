package com.scansee.common.pojos;

import java.util.List;

import com.scansee.common.constants.ApplicationConstants;

/**
 * The POJO class for ThisLocationRequest.
 * 
 * @author shyamsundara_hm
 */

public class ThisLocationRequest extends BaseObject
{

	/**
	 * For preferred Radius.
	 */
	private Double preferredRadius;
	/**
	 * for zipcode.
	 */
	private String zipcode;
	/**
	 * for userid.
	 */
	private Integer userId;
	/**
	 * for gpsEnabled.
	 */
	private Boolean gpsEnabled;
	/**
	 * for longitude.
	 */
	private Double longitude;
	/**
	 * for latitude.
	 */
	private Double latitude;
	/**
	 * for lastVisitedRecord.
	 */
	private Integer lastVisitedRecord;

	/**
	 * productid for Shoppinglist findNearBy method,in othercases it should be
	 * null.
	 */

	private Integer productId;

	/**
	 * Variable for Screen name For pagination.
	 */
	private String screenName;
	/**
	 * variable for Postal code.
	 */
	private String postalCode;

	/**
	 * for radius
	 */
	private String radius;
	/**
	 * for distance in mails
	 */
	private String distInMiles;
	
	/**
	 * for city name
	 */
	private String cityName;
	
	/**
	 * For retailerGroupID
	 */
	private Integer retGroupID;
	
	/**
	 * For Retail Affiliate ID
	 */
	private Integer retAffID;

	/**
	 * For Retailer Affiliate Name
	 */
	private String retAffName;
	
	/**
	 * 
	 */
	private String retAffImg;

	/**
	 * For category ID
	 */
	private String catID;
	
	/**
	 * For partner ID
	 */
	private Integer partnerID;
	
	/**
	 * For maximum count
	 */
	private Integer maxCnt;
	
	/**
	 * For partner list
	 */
	private List<ThisLocationRequest> partnerList;
	
	/**
	 * For search key
	 */
	private String searchKey;
	
	/**
	 * For mainMenuID. User tracking.
	 */
	private Integer mainMenuID;
	
	/**
	 * For mainMenuID. User tracking.
	 */
	private Integer locDetailsID;
	
	/**
	 * Is located on map boolean value
	 */
	private Boolean locOnMap;
	
	/**
	 * FOR MODULE ID
	 */
	private Integer moduleID;
	
	/**
	 * For alerted product ID
	 */
	private Integer alertProdID;

	/**
	 * This method for getting productid.
	 * 
	 * @return the productId.
	 */
	public Integer getProductId()
	{
		return productId;
	}

	/**
	 * this method return Screen Name.
	 * 
	 * @return the screenName.
	 */
	public String getScreenName()
	{
		return screenName;
	}

	/**
	 * this method set the Screen name.
	 * 
	 * @param screenName
	 *            the screenName to set.
	 */
	public void setScreenName(String screenName)
	{
		this.screenName = screenName;
	}

	/**
	 * This method for setting product id.
	 * 
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}

	/**
	 * This method for checking gpsenabled or not.
	 * 
	 * @return the gpsEnabled
	 */
	public Boolean isGpsEnabled()
	{
		return gpsEnabled;
	}

	/**
	 * This method for setting gpsenabled value.
	 * 
	 * @param gpsEnabled
	 *            the gpsEnabled to set
	 */
	public void setGpsEnabled(Boolean gpsEnabled)
	{
		this.gpsEnabled = gpsEnabled;
	}

	/**
	 * This method for getting userId.
	 * 
	 * @return the userId
	 */
	public Integer getUserId()
	{
		return userId;
	}

	/**
	 * this method for setting userid.
	 * 
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	

	/**
	 * this method for getting longitude value.
	 * 
	 * @return longitude
	 */
	public Double getLongitude()
	{
		return longitude;
	}

	/**
	 * This method for setting logitude value.
	 * 
	 * @param longitude
	 *            as request parameter.
	 */
	public void setLongitude(Double longitude)
	{
		this.longitude = longitude;
	}

	/**
	 * This method for getting latitude.
	 * 
	 * @return latitude
	 */
	public Double getLatitude()
	{
		return latitude;
	}

	/**
	 * This method for setting latitude.
	 * 
	 * @param latitude
	 *            as request parameter.
	 */
	public void setLatitude(Double latitude)
	{
		this.latitude = latitude;
	}

	/**
	 * This method for getting lastvisistedRecord.
	 * 
	 * @return lastVisitedRecord
	 */
	public Integer getLastVisitedRecord()
	{
		return lastVisitedRecord;
	}

	/**
	 * This method for setting lastvisitedRecord.
	 * 
	 * @param lastVisitedCounter
	 *            as request parameter.
	 */
	public void setLastVisitedRecord(Integer lastVisitedCounter)
	{
		this.lastVisitedRecord = lastVisitedCounter;
	}

	

	/**
	 * for getting zipcode.
	 * 
	 * @return the zipcode
	 */
	public String getZipcode()
	{
		return zipcode;
	}

	/**
	 * for setting zipcode.
	 * 
	 * @param zipcode
	 *            the zipcode to set
	 */
	public void setZipcode(String zipcode)
	{
		this.zipcode = zipcode;
	}

	public String getRadius()
	{
		return radius;
	}

	public void setRadius(String radius)
	{
		if (radius == null)
		{
			this.radius = ApplicationConstants.NOTAPPLICABLE;

		}
		else
		{
			this.radius = radius;
		}
	}

	public String getDistInMiles()
	{
		return distInMiles;
	}

	public void setDistInMiles(String distInMiles)
	{
		if (distInMiles == null)
		{
			this.distInMiles = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.distInMiles = distInMiles;
		}
	}

	public Double getPreferredRadius()
	{
		return preferredRadius;
	}

	public void setPreferredRadius(Double preferredRadius)
	{
		this.preferredRadius = preferredRadius;
	}

	public String getPostalCode()
	{
		return postalCode;
	}

	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * to get city name
	 * @return cityName
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * to set city name
	 * @param cityName
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * To get retailerGroupID
	 * @return Integer retailerGroupID
	 */
	public Integer getRetGroupID() {
		return retGroupID;
	}

	/**
	 * To set retailerGroupID
	 * @param retGroupID
	 */
	public void setRetGroupID(Integer retGroupID) {
		this.retGroupID = retGroupID;
	}

	/**
	 * To get retAffID
	 * @return Integer retAffID
	 */
	public Integer getRetAffID() {
		return retAffID;
	}

	/**
	 * To set retAffID
	 * @param retAffID
	 */
	public void setRetAffID(Integer retAffID) {
		this.retAffID = retAffID;
	}
	
	/**
	 * To get retailer affiliate name
	 * @return retAffName
	 */
	public String getRetAffName() {
		return retAffName;
	}

	/**
	 * To set retailer affiliate name
	 * @param retAffName
	 */
	public void setRetAffName(String retAffName) {
		this.retAffName = retAffName;
	}
	
	/**
	 * To get Category ID
	 * @return catID
	 */
	public String getCatID() {
		return catID;
	}

	/**
	 * To set category ID
	 * @param catID
	 */
	public void setCatID(String catID) {
		this.catID = catID;
	}
	
	/**
	 * To get partner ID
	 * @return partnerID
	 */
	public Integer getPartnerID() {
		return partnerID;
	}

	/**
	 * To set partner ID
	 * @param partnerID
	 */
	public void setPartnerID(Integer partnerID) {
		this.partnerID = partnerID;
	}	
	
	/**
	 * To get maximum count
	 * @return maxCnt
	 */
	public Integer getMaxCnt() {
		return maxCnt;
	}

	/**
	 * to set maximum count
	 * @param maxCnt
	 */
	public void setMaxCnt(Integer maxCnt) {
		this.maxCnt = maxCnt;
	}
	
	public List<ThisLocationRequest> getPartnerList() {
		return partnerList;
	}

	public void setPartnerList(List<ThisLocationRequest> partnerList) {
		this.partnerList = partnerList;
	}
	
	/**
	 * To get search key
	 * @return searchKey
	 */
	public String getSearchKey() {
		return searchKey;
	}

	/**
	 * To set searchKey
	 * @param searchKey
	 */
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	
	/**
	 * to get retailer affiliate image path
	 * @return retAffImg
	 */
	public String getRetAffImg() {
		return retAffImg;
	}

	/**
	 * To set retailer affiliate image path
	 * @param retAffImg
	 */
	public void setRetAffImg(String retAffImg) {
		if(retAffImg != null && !retAffImg.equals(""))	{
			this.retAffImg = retAffImg;
		}
		else	{
			this.retAffImg = ApplicationConstants.IMAGENOTFOUND;
		}
	}	
	
	/**
	 * For user tracking.
	 * @return mainMenuID
	 */
	public Integer getMainMenuID() {
		return mainMenuID;
	}

	/**
	 * For user tracking
	 * @param mainMenuID
	 */
	public void setMainMenuID(Integer mainMenuID) {
		this.mainMenuID = mainMenuID;
	}

	/**
	 * For user tracking.
	 * @return locDetailsID
	 */
	public Integer getLocDetailsID() {
		return locDetailsID;
	}

	/**
	 * For user tracking
	 * @param locDetailsID
	 */
	public void setLocDetailsID(Integer locDetailsID) {
		this.locDetailsID = locDetailsID;
	}
	
	public Boolean getLocOnMap() {
		return locOnMap;
	}

	public void setLocOnMap(Boolean locOnMap) {
		this.locOnMap = locOnMap;
	}
	
	public Integer getModuleID() {
		return moduleID;
	}

	public void setModuleID(Integer moduleID) {
		this.moduleID = moduleID;
	}
	
	public Integer getAlertProdID() {
		return alertProdID;
	}

	public void setAlertProdID(Integer alertProdID) {
		this.alertProdID = alertProdID;
	}
}
