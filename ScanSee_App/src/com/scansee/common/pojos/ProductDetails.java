package com.scansee.common.pojos;

import java.util.List;

import com.scansee.common.constants.ApplicationConstants;
/**
 * The POJO class for ProductDetail.
 * @author shyamsundara_hm
 *
 */

public class ProductDetails extends BaseObject{

	/**
	 * for product mutiple images
	 * 
	 */
	private String productMediaPath;
	
	/**
	 * to check product exists or there not
	 */
	private Integer productIsThere;
	/**
	 * For shopping list response response flag.
	 */
	private String responseFlag;
	/**
	 * for indicating list flag.
	 */
	private String listFlag;
	
	/**
	 * for indicating favorites  flag.
	 */
	private String favoritesFlag;
	
	/** nextPage declared as Integer.
	 * for next page
	 */
	private Integer nextPage;
	/**
	 * for lastVisitedProductNo.
	 */
	private Integer lastVistedProductNo;
	
	/**
	 * for mediaType.
	 */
	private String mediaType;
	
	/**
	 * This is for productDetail list.
	 */
	private List<ProductDetail> productDetail;
	/**
	 * This is for special offer list.
	 *  
	 */
	private List<RetailerDetail> specialOfferlst;
	/**
	 * This method for getting lastVistedProductNo.
	 * @return the lastVistedProductNo
	 */
	
	/**
	 * For product smart search ID
	 */
	private Integer prodSmaSeaID;
	
	public Integer getLastVistedProductNo() {
		return lastVistedProductNo;
	}
	/**This method for setting lastVistedProductNo.
	 * @param lastVistedProductNo the lastVistedProductNo to set
	 */
	public void setLastVistedProductNo(Integer lastVistedProductNo) {
		this.lastVistedProductNo = lastVistedProductNo;
	}
	
	/**
	 * this method for getting productDetails.
	 * @return the productDetail
	 */
	public List<ProductDetail> getProductDetail() {
		return productDetail;
	}
	/**
	 * this method for setting  productDetails.
	 * @param productDetail the productDetail to set
	 */
	public void setProductDetail(List<ProductDetail> productDetail) {
		this.productDetail = productDetail;
	}
	/**
	 * this method for getting mediaType.
	 * @return the mediaType
	 */
	public String getMediaType() {
		return mediaType;
	}
	/**
	 * this method for setting mediaType.
	 * @param mediaType the mediaType to set
	 */
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}
	/**
	 * for getting nextPage. 
	 * @return the nextPage
	 */
	public Integer getNextPage()
	{
		return nextPage;
	}
	/**
	 * for setting nextPage.
	 * @param nextPage the nextPage to set
	 */
	public void setNextPage(Integer nextPage)
	{
		this.nextPage = nextPage;
	}
	
	/**
	 * TO set listFlag.
	 * @param listFlag the listFlag to set
	 */
	public void setListFlag(String listFlag)
	{
		this.listFlag = listFlag;
	}
	/**
	 * To set favoritesFlag.
	 * @param favoritesFlag the favoritesFlag to set
	 */
	public void setFavoritesFlag(String favoritesFlag)
	{
		this.favoritesFlag = favoritesFlag;
	}
	/**
	 * To get listFlag.
	 * @return the listFlag
	 */
	public String getListFlag()
	{
		return listFlag;
	}
	/**
	 * To set favoritesFlag.
	 * @return the favoritesFlag
	 */
	public String getFavoritesFlag()
	{
		return favoritesFlag;
	}
	/**
	 * To get responseFlag.
	 * @return the responseFlag
	 */
	public String getResponseFlag()
	{
		return responseFlag;
	}
	/**
	 * To set responseFlag.
	 * @param responseFlag the responseFlag to set
	 */
	public void setResponseFlag(String responseFlag)
	{
		this.responseFlag = responseFlag;
	}
	public Integer getProductIsThere()
	{
		return productIsThere;
	}
	public void setProductIsThere(Integer productIsThere)
	{
		this.productIsThere = productIsThere;
	}
	public String getProductMediaPath()
	{
		return productMediaPath;
	}
	public void setProductMediaPath(String productMediaPath)
	{
		if(productMediaPath==null)
		{
			this.productMediaPath=ApplicationConstants.NOTAPPLICABLE;
		}else
		{
		this.productMediaPath = productMediaPath;
		}
	}
	
	public List<RetailerDetail> getSpecialOfferlst()
	{
		return specialOfferlst;
	}
	
	public void setSpecialOfferlst(List<RetailerDetail> specialOfferlst)
	{
		this.specialOfferlst = specialOfferlst;
	}
	
	public Integer getProdSmaSeaID() {
		return prodSmaSeaID;
	}
	
	public void setProdSmaSeaID(Integer prodSmaSeaID) {
		this.prodSmaSeaID = prodSmaSeaID;
	}
	
}
