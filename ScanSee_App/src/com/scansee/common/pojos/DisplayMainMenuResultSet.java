package com.scansee.common.pojos;


import java.util.List;

import com.scansee.common.constants.ApplicationConstants;

/**
 * This pojo for capturing mainmenu list.
 * @author shyamsundara_hm
 *
 */


public class DisplayMainMenuResultSet extends BaseObject
{
	/**
	 * for list displayMainmenu.
	 */
	private List<DisplayMainMenu> displayMainmenu;
	
	/**
	 * For RetailGroupID
	 */
	private Integer retGroupID;
	
	/**
	 * For RetailAffiliateID
	 */
	private Integer partnerCount;
	
	private Integer retAffID;
	
	private String retAffName;

	/**
	 * for getting displayMainmenu.
	 * @return displayMainmenu
	 */
	public List<DisplayMainMenu> getDisplayMainmenu()
	{
		return displayMainmenu;
	}
	/**
	 * for setting displayMainmenu.
	 * @param displayMainmenu to be set
	 */

	public void setDisplayMainmenu(List<DisplayMainMenu> displayMainmenu)
	{
		this.displayMainmenu = displayMainmenu;
	}

	/**
	 * To get retailer group ID
	 * @return retGroupID
	 */
	public Integer getRetGroupID() {
		return retGroupID;
	}
	
	/**
	 * To set retailer Affiliate ID
	 * @param retGroupID
	 */
	public void setRetGroupID(Integer retGroupID) {
		this.retGroupID = retGroupID;
	}

	/**
	 * To get retailer affiliate ID
	 * @return retAffID
	 */
	public Integer getPartnerCount() {
		return partnerCount;
	}
	
	/**
	 * To set retailer affiliate ID
	 * @param retAffID
	 */
	public void setPartnerCount(Integer partnerCount) {
		this.partnerCount = partnerCount;
	}
	
	/**
	 * To get Retailer Affiliate ID
	 * @return retAffID
	 */
	public Integer getRetAffID() {
		return retAffID;
	}
	
	/**
	 * To set Retailer Affiliate ID
	 * @param retAffID
	 */
	public void setRetAffID(Integer retAffID) {
		this.retAffID = retAffID;
	}
	
	/**
	 * To get Retailer Affiliate Name
	 * @return retAffName
	 */
	public String getRetAffName() {
		return retAffName;
	}
	
	/**
	 * To set Retailer Affiliate Name
	 * @param retAffName
	 */
	public void setRetAffName(String retAffName) {
		if(retAffName == null || "".equals(retAffName))	{
			this.retAffName = ApplicationConstants.NOTAPPLICABLE;
		}
		else	{
			this.retAffName = retAffName;
		}
	}

}
