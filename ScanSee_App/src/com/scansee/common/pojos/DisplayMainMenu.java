package com.scansee.common.pojos;

import com.scansee.common.constants.ApplicationConstants;

/**
 * this class for displaying main menus.
 * 
 * @author shyamsundara_hm
 */
public class DisplayMainMenu extends BaseObject
{

	/**
	 * for mediaId.
	 */
	private Integer mediaId;
	/**
	 * for mediaName.
	 */
	private String mediaName;
	/**
	 * for moduleId.
	 */
	private Integer moduleId;
	/**
	 * for moduleName.
	 */
	private String moduleName;
	/**
	 * for mediaPath.
	 */
	private String mediaPath;
	/**
	 * for VideoViewed.
	 */
	private String VideoViewed;

	/**
	 * menuImagePath declared as String.
	 */

	private String menuImagePath;

	/**
	 * for main menu video.
	 */
	private Integer videoVeiwed;
	/**
	 * for user id
	 */
	private Integer usrId;
	/**
	 * for latitude
	 */
	private String lat;
	/**
	 * for longitude
	 */
	private String lng;

	/**
	 * to set mediaId.
	 * 
	 * @return mediaId
	 */
	public Integer getMediaId()
	{
		return mediaId;
	}

	/**
	 * for setting mediaId.
	 * 
	 * @param mediaId
	 *            to be set
	 */
	public void setMediaId(Integer mediaId)
	{
		this.mediaId = mediaId;
	}

	/**
	 * for getting mediaName.
	 * 
	 * @return mediaName
	 */
	public String getMediaName()
	{
		return mediaName;
	}

	/**
	 * for setting mediaName.
	 * 
	 * @param mediaName
	 *            to be set
	 */
	public void setMediaName(String mediaName)
	{
		this.mediaName = mediaName;
	}

	/**
	 * for getting moduleId.
	 * 
	 * @return moduleId
	 */
	public Integer getModuleId()
	{
		return moduleId;
	}

	/**
	 * for setting moduleId.
	 * 
	 * @param moduleId
	 *            to be set
	 */
	public void setModuleId(Integer moduleId)
	{
		this.moduleId = moduleId;
	}

	/**
	 * for getting moduleName.
	 * 
	 * @return moduleName
	 */
	public String getModuleName()
	{
		return moduleName;
	}

	/**
	 * for setting moduleName.
	 * 
	 * @param moduleName
	 *            to be set
	 */
	public void setModuleName(String moduleName)
	{
		this.moduleName = moduleName;
	}

	/**
	 * for getting VideoViewed.
	 * 
	 * @return VideoViewed
	 */
	public String getVideoViewed()
	{
		return VideoViewed;
	}

	/**
	 * for setting videoViewed.
	 * 
	 * @param videoViewed
	 *            to be set
	 */
	public void setVideoViewed(String videoViewed)
	{
		VideoViewed = videoViewed;
	}

	/**
	 * for getting mediaPath.
	 * 
	 * @return mediaPath
	 */
	public String getMediaPath()
	{
		return mediaPath;
	}

	/**
	 * to set mediapath.
	 * 
	 * @param mediaPath
	 *            to be set
	 */
	public void setMediaPath(String mediaPath)
	{
		if (null == mediaPath)
		{
			this.menuImagePath = ApplicationConstants.NOIMGPATH;
		}
		else
		{
			this.mediaPath = mediaPath;
		}
	}

	/**
	 * To get menuImagePath.
	 * 
	 * @return the menuImagePath
	 */
	public String getMenuImagePath()
	{
		return menuImagePath;
	}

	/**
	 * To set menuImagePath.
	 * 
	 * @param menuImagePath
	 *            the menuImagePath to set
	 */
	public void setMenuImagePath(String menuImagePath)
	{
		if (null == menuImagePath)
		{
			this.menuImagePath = ApplicationConstants.NOIMGPATH;
		}
		else
		{
			this.menuImagePath = menuImagePath;
		}

	}

	public Integer getVideoVeiwed()
	{
		return videoVeiwed;
	}

	public void setVideoVeiwed(Integer videoVeiwed)
	{
		this.videoVeiwed = videoVeiwed;
	}

	public Integer getUsrId()
	{
		return usrId;
	}

	public void setUsrId(Integer usrId)
	{
		this.usrId = usrId;
	}

	public String getLat()
	{
		return lat;
	}

	public void setLat(String lat)
	{
		this.lat = lat;
	}

	public String getLng()
	{
		return lng;
	}

	public void setLng(String lng)
	{
		this.lng = lng;
	}

}
