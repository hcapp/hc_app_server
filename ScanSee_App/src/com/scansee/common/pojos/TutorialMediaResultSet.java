package com.scansee.common.pojos;

/**
 * The POJO class for TutorialMediaResultSet
 */

import java.util.ArrayList;
import java.util.List;

/**
 * The class has getter and setter methods for TutorialMediaResultSet.
 * 
 * @author shyamsundara_hm
 */
public class TutorialMediaResultSet
{

	/**
	 * Variable sectionContent declared as ArrayList.
	 */

	private ArrayList<SectionContent> sectionContent;
	/**
	 * property for List storing type tutorialMedia.
	 */

	private String screenImgPath;
	
	private List<TutorialMedia> tutorialMedia;
	
	private String welcomeVideo;

	/**
	 * This for getting TutorialMedialist.
	 * 
	 * @return List of type tutorialMedia
	 */

	public List<TutorialMedia> getTutorialMedia()
	{
		return tutorialMedia;
	}

	/**
	 * This is for setting tutorialMedia List.
	 * 
	 * @param tutorialMedia
	 *            the tutorialMedia to set
	 */

	public void setTutorialMedia(List<TutorialMedia> tutorialMedia)
	{
		this.tutorialMedia = tutorialMedia;
	}

	/**
	 * This for getting SectionContent.
	 * 
	 * @return ArrayList of type sectionContent
	 */
	public ArrayList<SectionContent> getSectionContent()
	{
		return sectionContent;
	}

	/**
	 * This is for setting sectionContent.
	 * 
	 * @param sectionContent
	 *            the sectionContent to set
	 */
	public void setSectionContent(ArrayList<SectionContent> sectionContent)
	{
		this.sectionContent = sectionContent;
	}

	/**
	 * @return the screenImgPath
	 */
	public String getScreenImgPath()
	{
		return screenImgPath;
	}

	/**
	 * @param screenImgPath the screenImgPath to set
	 */
	public void setScreenImgPath(String screenImgPath)
	{
		this.screenImgPath = screenImgPath;
	}

	public String getWelcomeVideo() {
		return welcomeVideo;
	}

	public void setWelcomeVideo(String welcomeVideo) {
		this.welcomeVideo = welcomeVideo;
	}

}
