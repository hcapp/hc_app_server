package com.scansee.common.pojos;

/**
 * The POJO for SubCategoryRes.
 * @author saurab_r
 */

public class SubCategoryRes
{

	/**
	 * The subcategoryName property.
	 */

	private String subcategoryName;

	/**
	 * To get subcategoryName.
	 * @return The subcategoryName property.
	 */

	public String getSubcategoryName()
	{
		return subcategoryName;
	}

	/**
	 * To set subcategoryName.
	 * @param subcategoryName
	 *            The subcategoryName property.
	 */

	public void setSubcategoryName(String subcategoryName)
	{
		this.subcategoryName = subcategoryName;
	}
}
