package com.scansee.common.pojos;

/**
 * The POJO for SubCategory.
 * @author sourab_r
 */

public class SubCategory extends BaseObject
{
	/**
	 * The subCategoryId property.
	 */

	private Integer subCategoryId;

	/**
	 * To get subCategoryId.
	 * @return The subCategoryId property.
	 */

	public Integer getSubCategoryId()
	{
		return subCategoryId;
	}

	/**
	 * To set subCategoryId.
	 * @param subCategoryId
	 *            The subCategoryId property.
	 */

	public void setSubCategoryId(Integer subCategoryId)
	{
		this.subCategoryId = subCategoryId;
	}

}
