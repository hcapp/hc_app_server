package com.scansee.common.pojos;

import java.util.List;

/**
 * The CategoryResponse class contains setter and getter methods.
 * @author murali_pnvb
 *
 */
public class CategoryResponse
{
	/**
	 * The categoryRes declared as List.
	 */
	private List<CategoryRes> categoryRes;
    
	/**
	 * To get categoryRes.
	 * @return categoryRes To get
	 */
	public List<CategoryRes> getCategoryRes()
	{
		return categoryRes;
	}
    
	/**
	 * To set categoryRes.
	 * @param categoryRes
	 *        -To set
	 */
	public void setCategoryRes(List<CategoryRes> categoryRes)
	{
		this.categoryRes = categoryRes;
	}

}
