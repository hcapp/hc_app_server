package com.scansee.common.pojos;

/**
 * This class contains setter and getter methods.
 * @author sourab_r
 *
 */
public class CreateOrUpdateFavCategories
{
	
    /**
     * The userId declared as integer.
     */
	private Integer userId;
	
	/**
     * The categoryId declared as integer.
     */
	private Integer categoryId;
	
	/**
     * The subCategoryId declared as integer.
     */
	private Integer subCategoryId;
	
	/**
     * The categories declared as object.
     */
	private Categories categories;

	/**
	 * To get categoryId.
	 * @return categoryId to get
	 */
	public Integer getCategoryId()
	{
		return categoryId;
	}

	/**
	 * To set categoryId.
	 * @param categoryId 
	 *           -To set
	 */
	public void setCategoryId(Integer categoryId)
	{
		this.categoryId = categoryId;
	}

	/**
	 * To get subCategoryId. 
	 * @return subCategoryId To get
	 */
	public Integer getSubCategoryId()
	{
		return subCategoryId;
	}

	/**
	 * To set subCategoryId.
	 * @param subCategoryId 
	 *           -To set
	 */
	public void setSubCategoryId(Integer subCategoryId)
	{
		this.subCategoryId = subCategoryId;
	}


	/**
	 * To get userId.
	 * @return userId to get
	 */
	
    public Integer getUserId()
	{
		return userId;
	}

    /**
	 * To set userId.
	 * @param userId 
	 *           -To set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	/**
	 * To get categories.
	 * @return categories To get
	 */
	public Categories getCategories()
	{
		return categories;
	}

	 /**
	 * To set categories.
	 * @param categories 
	 *           -To set
	 */
	public void setCategories(Categories categories)
	{
		this.categories = categories;
	}




}
