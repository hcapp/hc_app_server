package com.scansee.common.pojos;

import java.util.List;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.util.Utility;

/**
 * The class has getter and setter methods for RetailerDetail fields.
 * 
 * @author manjunatha_gh
 */

public class RetailerDetail extends BaseObject {

	/**
	 * declared for category id
	 */
	private Integer cateId;
	/**
	 * 
	 * declared for category name
	 */
	private String cateName;
	/**
	 * The rowNumber property.
	 */

	private Integer rowNumber;

	/**
	 * The retailerId property.
	 */

	private Integer retailerId;

	/**
	 * The retailerName property.
	 */

	private String retailerName;

	/**
	 * The retailLocationID property.
	 */
	private Integer retailLocationID;

	/**
	 * Variable for Retailer Address1.
	 */
	private String retaileraddress1;

	/**
	 * Variable for Retailer Address2.
	 */

	private String retaileraddress2;

	/**
	 * Variable for Retailer Address3.
	 */
	private String retaileraddress3;

	/**
	 * Variable for Retailer Address4.
	 */
	private String retaileraddress4;

	/**
	 * Variable for City.
	 */
	private String city;

	/**
	 * Variable for state.
	 */
	private String state;

	/**
	 * Variable for postalCode.
	 */
	private String postalCode;
	/**
	 * Variable for distance.
	 */
	private String distance;
	/**
	 * Variable for completeAddress.
	 */
	private String completeAddress;
	/**
	 * Variable for retailAddress.
	 */
	private String retailAddress;
	/**
	 * Variable for retailAddress.
	 */
	private String shareText;

	/**
	 * The list for storing type productDetails.
	 */

	private List<ProductDetail> productDetails;

	/**
	 * The imagePath property.
	 */

	private String logoImagePath;

	/**
	 * The displayed property.
	 */

	private Integer displayed;

	/**
	 * The bannerAdImagePath property.
	 */

	private String bannerAdImagePath;

	/**
	 * The ribbonAdImagePath property.
	 */

	private String ribbonAdImagePath;

	/**
	 * The ribbonAdURL property.
	 */

	private String ribbonAdURL;

	/**
	 * The advertisementID property.
	 */

	private String advertisementID;

	/**
	 * The price declared as string.
	 */
	private String price;

	/**
	 * The salePrice declared as string.
	 */
	private String salePrice;

	/**
	 * for online url
	 */
	private String retailOnlineURL;
	/**
	 * for online BuyURL
	 */
	private String buyURL;

	/**
	 * for sale flag to indicate if retailer has discount or not
	 */
	private Boolean saleFlag;
	/**
	 * To get retailAddress.
	 * 
	 * @return the retailAddress
	 */

	/**
	 * for online ShipmentCost
	 */
	private String shipmentCost;
	/**
	 * for userId
	 */
	private Long userId;

	/**
	 * for qr page id
	 */
	private Long pageID;

	/**
	 * for pagetitle
	 */
	private String pageTitle;
	/**
	 * for retailer image;
	 */
	private String image;
	/**
	 * store short description
	 */
	private String shortDescription;
	/**
	 * store long description.
	 */
	private String longDescription;
	/**
	 * for contact phone
	 */
	private String contactPhone;
	/**
	 * for retailer url
	 */
	private String retailerURL;
	/**
	 * for page link
	 */
	private String pageLink;

	/**
	 * return retailer address
	 * 
	 * @return
	 */
	/**
	 * FOR RetailLocationLatitude
	 */
	private String retLatitude;
	/**
	 * for retailer longitude.
	 */
	private String retLongitude;

	/**
	 * for share special offer info
	 * 
	 * @return
	 */
	private String toEmail;

	private String retailerPageQRURL;

	/**
	 * for ribben ad url
	 */
	private String splashAdID;

	/**
	 * Created for specail offer flag
	 */
	private Boolean specialOffFlag;
	/**
	 * creted for hotdeal flag
	 */
	private Boolean hotDealFlag;
	/**
	 * created for couponFlag;
	 */
	private Boolean clrFlag;

	/**
	 * for pagination
	 */
	/**
	 * The rowNumber property.
	 */

	private Integer lastVisitedNo;
	/**
	 * @return
	 */
	/**
	 * variable declare loyaltyDetails as List.
	 */

	private List<LoyaltyDetail> loyaltyDetails;

	/**
	 * variable declare coupondetails as List.
	 */

	private List<CouponDetails> couponDetails;

	/**
	 * for retailer list
	 * 
	 */
	private List<RetailerDetail> loygrpbyRetlst = null;
	
	/**
	 * For mainMenuID
	 */
	private Integer mainMenuID;
	
	/**
	 * For scanTypeID
	 */
	private Integer scanTypeID;
	
	/**
	 * For retailer list ID
	 */
	private Integer retListID;
	
	/**
	 * For specials list ID
	 */
	private Integer specialsListID;
	
	/**
	 * For category ID in find
	 */
	private Integer findCatID;
	
	/**
	 * for retailer search id
	 */
	private Integer findRetSeaID;
	
	/**
	 * For retailer details ID
	 */
	private Integer retDetID;
	
	/**
	 * For sale list ID
	 */
	private Integer salesListID;
	
	/**
	 * For Online Product Details ID
	 */
	private Integer onProdDetID;

	/**
	 * for startDate.
	 */
	private String startDate;

	/**
	 * for endDate.
	 */
	private String endDate;

	/**
	 * for rules.
	 */
	private String rules;

	/**
	 * for termCondition.
	 */
	private String termCondition;
	
	/**
	 * for click flag.
	 */
	private Boolean click;
	
	/**
	 * For expired
	 */
	private Boolean expired;
	
	/**
	 * For retailer group button image path
	 */
	private String retGroupImg;
	
	/**
	 * @return the expired
	 */
	public Boolean getExpired()
	{
		return expired;
	}

	/**
	 * @param expired the expired to set
	 */
	public void setExpired(Boolean expired)
	{
		this.expired = expired;
	}
	
	/**
	 * To get RetailAddress.
	 */
	public String getRetailAddress() {
		return retailAddress;
	}

	/**
	 * To set RetailAddress.
	 */
	public void setRetailAddress() {
		if (retaileraddress1 != null) {
			completeAddress = retaileraddress1 + ",";
		}
		if (city != null) {
			completeAddress = completeAddress + city + ",";
		}
		if (postalCode != null
				&& !ApplicationConstants.NOTAPPLICABLE.equals(postalCode)) {
			completeAddress = completeAddress + postalCode + ",";
		}
		if (state != null) {
			completeAddress = completeAddress + state;
		}
		this.retailAddress = completeAddress;
	}

	/**
	 * To get completeAddress.
	 * 
	 * @return the completeAddress
	 */
	public String getCompleteAddress() {
		return completeAddress;
	}

	/**
	 * To set completeAddress.
	 * 
	 * @param completeAddress
	 *            the completeAddress to set
	 */
	public void setCompleteAddress(String completeAddress) {

		this.completeAddress = completeAddress;
	}

	/**
	 * To get price value.
	 * 
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * To sety price value.
	 * 
	 * @param price
	 *            the price to set
	 */
	public void setPrice(String price) {
		if (price == null) {
			this.price = ApplicationConstants.NOTAPPLICABLE;
		} else {
			if (!price.contains("$")&& !ApplicationConstants.NOTAPPLICABLE.equals(price)) {

				this.price = Utility.formatDecimalValue(price);
				this.price = "$" + this.price;
			} else {
				this.price = price;
			}
		}
	}

	/**
	 * To get salePrice value.
	 * 
	 * @return the salePrice
	 */
	public String getSalePrice() {
		return salePrice;
	}

	/**
	 * To set salePrice value.
	 * 
	 * @param salePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(String salePrice) {
		if (salePrice == null) {
			this.salePrice = ApplicationConstants.NOTAPPLICABLE;
		} else {
			if (!salePrice.contains("$")
					&& !ApplicationConstants.NOTAPPLICABLE.equals(salePrice)) {
				// salePrice = Utility.formatDecimalValue(salePrice);
				this.salePrice = Utility.formatDecimalValue(salePrice);
				this.salePrice = "$" + this.salePrice;
			} else {
				this.salePrice = salePrice;
			}
		}

	}

	/**
	 * This method return rowNumber value.
	 * 
	 * @return the rowNumber
	 */
	public Integer getRowNumber() {
		return rowNumber;
	}

	/**
	 * This method set the value to rowNumber.
	 * 
	 * @param rowNumber
	 *            the rowNumber to set
	 */
	public void setRowNumber(Integer rowNumber) {
		this.rowNumber = rowNumber;
	}

	/**
	 * This method return retailerId value.
	 * 
	 * @return the retailerId
	 */
	public Integer getRetailerId() {
		return retailerId;
	}

	/**
	 * This method set the value to retailerId.
	 * 
	 * @param retailerId
	 *            the retailerId to set
	 */
	public void setRetailerId(Integer retailerId) {
		this.retailerId = retailerId;
	}

	/**
	 * This method return retailerName value.
	 * 
	 * @return the retailerName
	 */
	public String getRetailerName() {
		return retailerName;
	}

	/**
	 * This method set the value to retailerName.
	 * 
	 * @param retailerName
	 *            the retailerName to set
	 */
	public void setRetailerName(String retailerName) {
		if (null == retailerName || "".equalsIgnoreCase(retailerName)) {
			this.retailerName = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.retailerName = retailerName;
		}
	}

	/**
	 * This method return retailLocationID value.
	 * 
	 * @return the retailLocationID
	 */
	public Integer getRetailLocationID() {
		return retailLocationID;
	}

	/**
	 * This method set the value to retailLocationID.
	 * 
	 * @param retailLocationID
	 *            the retailLocationID to set
	 */
	public void setRetailLocationID(Integer retailLocationID) {

		this.retailLocationID = retailLocationID;
	}

	/**
	 * This method return retaileraddress1 value.
	 * 
	 * @return the retaileraddress1
	 */
	public String getRetaileraddress1() {
		return retaileraddress1;
	}

	/**
	 * This method set the value to retaileraddress1.
	 * 
	 * @param retaileraddress1
	 *            the retaileraddress1 to set
	 */
	public void setRetaileraddress1(String retaileraddress1) {
		if (null == retaileraddress1) {
			this.retaileraddress1 = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.retaileraddress1 = retaileraddress1;
		}
	}

	/**
	 * This method return retaileraddress2 value.
	 * 
	 * @return the retaileraddress2
	 */
	public String getRetaileraddress2() {
		return retaileraddress2;
	}

	/**
	 * This method set the value to retaileraddress2.
	 * 
	 * @param retaileraddress2
	 *            the retaileraddress2 to set
	 */
	public void setRetaileraddress2(String retaileraddress2) {
		if (null == retaileraddress2) {
			this.retaileraddress2 = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.retaileraddress2 = retaileraddress2;
		}
	}

	/**
	 * This method return retaileraddress3 value.
	 * 
	 * @return the retaileraddress3
	 */
	public String getRetaileraddress3() {
		return retaileraddress3;
	}

	/**
	 * This method set the value to retaileraddress3.
	 * 
	 * @param retaileraddress3
	 *            the retaileraddress3 to set
	 */
	public void setRetaileraddress3(String retaileraddress3) {
		if (null == retaileraddress3) {
			this.retaileraddress3 = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.retaileraddress3 = retaileraddress3;
		}
	}

	/**
	 * This method return retaileraddress4 value.
	 * 
	 * @return the retaileraddress4
	 */
	public String getRetaileraddress4() {
		return retaileraddress4;
	}

	/**
	 * This method set the value to retaileraddress4.
	 * 
	 * @param retaileraddress4
	 *            the retaileraddress4 to set
	 */
	public void setRetaileraddress4(String retaileraddress4) {
		if (null == retaileraddress4) {
			this.retaileraddress4 = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.retaileraddress4 = retaileraddress4;
		}
	}

	/**
	 * This method return city value.
	 * 
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * This method set the value to city.
	 * 
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		if(null==city || "".equals(city))
		{
			this.city=ApplicationConstants.NOTAPPLICABLE;
		}else{
		this.city = city;
		}
	}

	/**
	 * This method return state value.
	 * 
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * This method set the value to state.
	 * 
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		if(null== state || "".equals(state))
		{
			this.state = ApplicationConstants.NOTAPPLICABLE;
		}else{
		
		this.state = state;
		}
	}

	/**
	 * This method return postalCode value.
	 * 
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * This method set the value to postalCode.
	 * 
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		if (null == postalCode || "".equals(postalCode)) {
			// this.logoImagePath = ApplicationConstants.NOIMGPATH;
			this.postalCode = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.postalCode = postalCode;
		}

	}

	/**
	 * This method return distance value.
	 * 
	 * @return the distance
	 */
	public String getDistance() {
		return distance;
	}

	/**
	 * This method set the value to distance.
	 * 
	 * @param distance
	 *            the distance to set
	 */
	public void setDistance(String distance) {
		if (null == distance || "".equals(distance)) {
			this.distance = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.distance = Utility.formatDecimalValue(distance) + " mi";
		}
	}

	/**
	 * This method return productDetails value.
	 * 
	 * @return the productDetails
	 */
	public List<ProductDetail> getProductDetails() {
		return productDetails;
	}

	/**
	 * This method set the value to productDetails.
	 * 
	 * @param productDetails
	 *            the productDetails to set
	 */
	public void setProductDetails(List<ProductDetail> productDetails) {
		this.productDetails = productDetails;
	}

	/**
	 * This method return logoImagePath value.
	 * 
	 * @return the logoImagePath
	 */
	public String getLogoImagePath() {
		return logoImagePath;
	}

	/**
	 * This method set the value to logoImagePath.
	 * 
	 * @param logoImagePath
	 *            the logoImagePath to set
	 */
	public void setLogoImagePath(String logoImagePath) {
		if (null == logoImagePath || "".equals(logoImagePath)) {
			// this.logoImagePath = ApplicationConstants.NOIMGPATH;
			this.logoImagePath = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.logoImagePath = logoImagePath;
		}
	}

	/**
	 * This method return displayed value.
	 * 
	 * @return the displayed
	 */
	public Integer getDisplayed() {
		return displayed;
	}

	/**
	 * This method set the value to displayed.
	 * 
	 * @param displayed
	 *            the displayed to set
	 */
	public void setDisplayed(Integer displayed) {
		this.displayed = displayed;
	}

	/**
	 * This method return bannerAdImagePath value.
	 * 
	 * @return the bannerAdImagePath
	 */
	public String getBannerAdImagePath() {
		return bannerAdImagePath;
	}

	/**
	 * This method set the value to bannerAdImagePath.
	 * 
	 * @param bannerAdImagePath
	 *            the bannerAdImagePath to set
	 */
	public void setBannerAdImagePath(String bannerAdImagePath) {
		if (null == bannerAdImagePath || ("").equals(bannerAdImagePath)) {
			this.bannerAdImagePath = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.bannerAdImagePath = bannerAdImagePath;
		}
	}

	/**
	 * This method return ribbonAdImagePath value.
	 * 
	 * @return the ribbonAdImagePath
	 */
	public String getRibbonAdImagePath() {
		return ribbonAdImagePath;
	}

	/**
	 * This method set the value to ribbonAdImagePath.
	 * 
	 * @param ribbonAdImagePath
	 *            the ribbonAdImagePath to set
	 */
	public void setRibbonAdImagePath(String ribbonAdImagePath) {
		if (null == ribbonAdImagePath || ("").equals(ribbonAdImagePath)) {
			this.ribbonAdImagePath = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.ribbonAdImagePath = ribbonAdImagePath;
		}
	}

	/**
	 * This method return ribbonAdURL value.
	 * 
	 * @return the ribbonAdURL
	 */
	public String getRibbonAdURL() {
		return ribbonAdURL;
	}

	/**
	 * This method set the value to ribbonAdURL.
	 * 
	 * @param ribbonAdURL
	 *            the ribbonAdURL to set
	 */
	public void setRibbonAdURL(String ribbonAdURL) {
		if (null == ribbonAdURL) {
			this.ribbonAdURL = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.ribbonAdURL = ribbonAdURL;
		}
	}

	/**
	 * This method return advertisementID value.
	 * 
	 * @return the advertisementID
	 */
	public String getAdvertisementID() {
		return advertisementID;
	}

	/**
	 * This method set the value to advertisementID.
	 * 
	 * @param advertisementID
	 *            the advertisementID to set
	 */
	public void setAdvertisementID(String advertisementID) {
		this.advertisementID = advertisementID;
	}

	public String getRetailOnlineURL() {
		return retailOnlineURL;
	}

	public void setRetailOnlineURL(String retailOnlineURL) {
		if (retailOnlineURL == null || "".equalsIgnoreCase(retailOnlineURL)) {
			this.retailOnlineURL = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.retailOnlineURL = retailOnlineURL;
		}
	}

	public String getBuyURL() {
		return buyURL;
	}

	public void setBuyURL(String buyURL) {
		if (buyURL == null || "".equalsIgnoreCase(buyURL)) {
			this.buyURL = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.buyURL = buyURL;
		}
	}

	public String getShipmentCost() {
		return shipmentCost;
	}

	public void setShipmentCost(String shipmentCost) {
		if (shipmentCost == null || "".equalsIgnoreCase(shipmentCost)) {
			this.shipmentCost = ApplicationConstants.NOTAPPLICABLE;
		} else {
			
			if (!shipmentCost.contains("$")&& !ApplicationConstants.NOTAPPLICABLE.equals(shipmentCost)&& shipmentCost.matches("[0-9.]+")) {
		
				this.shipmentCost = Utility.formatDecimalValue(shipmentCost);
				this.shipmentCost = "$" + this.shipmentCost;
								
			}else{
				this.shipmentCost = shipmentCost;
			}
		}
	}

	public Boolean getSaleFlag() {
		return saleFlag;
	}

	public void setSaleFlag(Boolean saleFlag) {
		this.saleFlag = saleFlag;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		if (pageTitle == null || pageTitle.equals("")) {
			this.pageTitle = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.pageTitle = pageTitle;
		}
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		if (image == null || image.equals("")) {
			this.image = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.image = image;
		}
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		if (shortDescription == null || shortDescription.equals("")) {
			this.shortDescription = ApplicationConstants.NOTAPPLICABLE;
		} else {
			if(shortDescription.contains("<![CDATA["))	{
				this.shortDescription = shortDescription;
			}
			else	{
				this.shortDescription = "<![CDATA[" + shortDescription + "]]>";
			}
		}
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {

		if (longDescription == null || longDescription.equals("")) {
			this.longDescription = ApplicationConstants.NOTAPPLICABLE;
		} else {
			if(shortDescription.contains("<![CDATA["))	{
				this.longDescription = longDescription;
			}
			else	{
				this.longDescription = "<![CDATA[" + longDescription + "]]>";
			}
		}

	}

	public Long getPageID() {
		return pageID;
	}

	public void setPageID(Long pageID) {
		this.pageID = pageID;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		if (contactPhone == null || contactPhone.equals("")) {
			this.contactPhone = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.contactPhone = contactPhone;
		}
	}

	public String getRetailerURL() {
		return retailerURL;
	}

	public void setRetailerURL(String retailerURL) {
		if (retailerURL == null || retailerURL.equals("")) {
			this.retailerURL = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.retailerURL = retailerURL;
		}
	}

	public String getPageLink() {
		return pageLink;
	}

	public void setPageLink(String pageLink) {
		if (pageLink == null || pageLink.equals("")) {
			this.pageLink = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.pageLink = pageLink;
		}
	}

	public String getRetLatitude() {
		return retLatitude;
	}

	public void setRetLatitude(String retLatitude) {
		if (retLatitude == null || retLatitude.equals("")) {
			this.retLatitude = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.retLatitude = retLatitude;
		}
	}

	public String getRetLongitude() {
		return retLongitude;
	}

	public void setRetLongitude(String retLongitude) {
		if (retLongitude == null || retLongitude.equals("")) {
			this.retLongitude = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.retLongitude = retLongitude;
		}
	}

	public String getToEmail() {
		return toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}

	public String getRetailerPageQRURL() {
		return retailerPageQRURL;
	}

	public void setRetailerPageQRURL(String retailerPageQRURL) {
		if (retailerPageQRURL == null || ("").equals(retailerPageQRURL)) {
			this.retailerPageQRURL = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.retailerPageQRURL = retailerPageQRURL;
		}
	}

	public String getSplashAdID() {
		return splashAdID;
	}

	public void setSplashAdID(String splashAdID) {
		if (splashAdID == null || ("").equals(splashAdID)) {
			this.splashAdID = ApplicationConstants.NOTAPPLICABLE;
		} else {
			this.splashAdID = splashAdID;
		}

	}

	public Boolean getSpecialOffFlag() {
		return specialOffFlag;
	}

	public void setSpecialOffFlag(Boolean specialOffFlag) {
		this.specialOffFlag = specialOffFlag;
	}

	public Boolean getHotDealFlag() {
		return hotDealFlag;
	}

	public void setHotDealFlag(Boolean hotDealFlag) {
		this.hotDealFlag = hotDealFlag;
	}

	public Boolean getClrFlag() {
		return clrFlag;
	}

	public void setClrFlag(Boolean clrFlag) {
		this.clrFlag = clrFlag;
	}

	public Integer getLastVisitedNo() {
		return lastVisitedNo;
	}

	public void setLastVisitedNo(Integer lastVisitedNo) {
		this.lastVisitedNo = lastVisitedNo;
	}

	public List<LoyaltyDetail> getLoyaltyDetails() {
		return loyaltyDetails;
	}

	public void setLoyaltyDetails(List<LoyaltyDetail> loyaltyDetails) {
		this.loyaltyDetails = loyaltyDetails;
	}

	public List<CouponDetails> getCouponDetails() {
		return couponDetails;
	}

	public void setCouponDetails(List<CouponDetails> couponDetails) {
		this.couponDetails = couponDetails;
	}

	public Integer getCateId() {
		return cateId;
	}

	public void setCateId(Integer cateId) {
		this.cateId = cateId;
	}

	public String getCateName() {
		return cateName;
	}

	public void setCateName(String cateName) {
		this.cateName = cateName;
	}

	/**
	 * @return the shareText
	 */
	public String getShareText()
	{
		return shareText;
	}

	/**
	 * @param shareText the shareText to set
	 */
	public void setShareText(String shareText)
	{
		this.shareText = shareText;
	}

	/**
	 * To get mainMenuID
	 * @return mainMenuID
	 */
	public Integer getMainMenuID() {
		return mainMenuID;
	}

	/**
	 * To set mainMenuID
	 * @param mainMenuID
	 */
	public void setMainMenuID(Integer mainMenuID) {
		this.mainMenuID = mainMenuID;
	}

	/**
	 * To get scanTypeID
	 * @return scanTypeID
	 */
	public Integer getScanTypeID() {
		return scanTypeID;
	}

	/**
	 * To set scanTypeID
	 * @param scanTypeID
	 */
	public void setScanTypeID(Integer scanTypeID) {
		this.scanTypeID = scanTypeID;
	}
	
	/**
	 * To get retListID
	 * @return retListID
	 */
	public Integer getRetListID() {
		return retListID;
	}

	/**
	 * To set retListID
	 * @param retListID
	 */
	public void setRetListID(Integer retListID) {
		this.retListID = retListID;
	}
	
	/**
	 * To get specialsListID
	 * @return specialsListID
	 */
	public Integer getSpecialsListID() {
		return specialsListID;
	}

	/**
	 * To set specialsListID
	 * @param specialsListID
	 */
	public void setSpecialsListID(Integer specialsListID) {
		this.specialsListID = specialsListID;
	}
	
	public Integer getFindCatID() {
		return findCatID;
	}

	public void setFindCatID(Integer findCatID) {
		this.findCatID = findCatID;
	}

	public Integer getFindRetSeaID() {
		return findRetSeaID;
	}

	public void setFindRetSeaID(Integer findRetSeaID) {
		this.findRetSeaID = findRetSeaID;
	}
	
	public Integer getRetDetID() {
		return retDetID;
	}

	public void setRetDetID(Integer retDetID) {
		this.retDetID = retDetID;
	}
	
	public Integer getSalesListID() {
		return salesListID;
	}

	public void setSalesListID(Integer salesListID) {
		this.salesListID = salesListID;
	}
	
	public Integer getOnProdDetID() {
		return onProdDetID;
	}

	public void setOnProdDetID(Integer onProdDetID) {
		this.onProdDetID = onProdDetID;
	}

	/**
	 * @return the click
	 */
	public Boolean getClick()
	{
		return click;
	}

	/**
	 * @param click the click to set
	 */
	public void setClick(Boolean click)
	{
		this.click = click;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate()
	{
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate)
	{
		if (startDate != null) {
			this.startDate = startDate;
			this.startDate = Utility.convertDBdate(startDate);
		} else {
			this.startDate = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate()
	{
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate)
	{
		if (endDate != null) {
			this.endDate = startDate;
			this.endDate = Utility.convertDBdate(endDate);
		} else {
			this.endDate = ApplicationConstants.NOTAPPLICABLE;
		}
	}

	/**
	 * @return the rules
	 */
	public String getRules()
	{
		return rules;
	}

	/**
	 * @param rules the rules to set
	 */
	public void setRules(String rules)
	{
		this.rules = rules;
	}

	/**
	 * @return the termCondition
	 */
	public String getTermCondition()
	{
		return termCondition;
	}

	/**
	 * @param termCondition the termCondition to set
	 */
	public void setTermCondition(String termCondition)
	{
		this.termCondition = termCondition;
	}
	
	public String getRetGroupImg() {
		return retGroupImg;
	}
	
	public void setRetGroupImg(String retGroupImg) {
		this.retGroupImg = retGroupImg;
	}
}
