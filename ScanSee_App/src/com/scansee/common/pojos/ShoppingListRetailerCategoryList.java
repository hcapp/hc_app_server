package com.scansee.common.pojos;

import java.util.List;

/**
 * This class for handling Master Shopping list retailer and category list.
 * 
 * @author shyamsundara_hm
 */

public class ShoppingListRetailerCategoryList
{
	/**
	 * for pagination next page
	 */
	private Integer nextPage;
	/**
	 * for pagination maxCount
	 */
	private Integer maxCount;

	/**
	 * for list containing category list.
	 */
	private List<Category> categoryInfolst;

	/**
	 * for list containing retailers and category list.
	 */
	private List<RetailerInfo> retailerInfolst;

	/**
	 * to get retailerInfolst.
	 * 
	 * @return the retailerInfolst
	 */
	public List<RetailerInfo> getRetailerInfolst()
	{
		return retailerInfolst;
	}

	/**
	 * to set retailerInfolst.
	 * 
	 * @param retailerInfolst
	 *            the retailerInfolst to set
	 */
	public void setRetailerInfolst(List<RetailerInfo> retailerInfolst)
	{
		this.retailerInfolst = retailerInfolst;
	}

	/**
	 * to get categoryInfolst.
	 * 
	 * @return the categoryInfolst
	 */
	public List<Category> getCategoryInfolst()
	{
		return categoryInfolst;
	}

	/**
	 * to set categoryInfolst.
	 * 
	 * @param categoryInfolst
	 *            the categoryInfolst to set
	 */
	public void setCategoryInfolst(List<Category> categoryInfolst)
	{
		this.categoryInfolst = categoryInfolst;
	}

	public Integer getNextPage()
	{
		return nextPage;
	}

	public void setNextPage(Integer nextPage)
	{
		this.nextPage = nextPage;
	}

	/**
	 * @return the maxCount
	 */
	public Integer getMaxCount()
	{
		return maxCount;
	}

	/**
	 * @param maxCount the maxCount to set
	 */
	public void setMaxCount(Integer maxCount)
	{
		this.maxCount = maxCount;
	}

}
