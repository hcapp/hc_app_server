package com.scansee.common.pojos;

import com.scansee.common.constants.ApplicationConstants;

/**
 * The POJO class for HotDealsListRequest.
 * 
 * @author dileepa_cc.
 */
public class MyAccountDetails extends BaseObject
{
	/**
	 * Variable currentBalance declared as String.
	 */
	private String currentBalance;
	/**
	 * Variable couponsCur declared as String.
	 */
	private String couponsCur;
	/**
	 * Variable rebateCur declared as Integer.
	 */
	private String rebateCur;
	/**
	 * Variable fieldAgentCur declared as String.
	 */
	private String fieldAgentCur;
	/**
	 * Variable totalLifetime declared as String.
	 */
	private String totalLifetime;
	/**
	 * Variable couponsTot declared as String.
	 */
	private String couponsTot;
	/**
	 * Variable rebateTot declared as String.
	 */
	private String rebateTot;
	/**
	 * Variable userId fieldAgentTot as String.
	 */
	private String fieldAgentTot;
	/**
	 * Variable userId redeemSavingURL as String.
	 */
	private String redeemSavingURL;

	/**
	 * Gets the value of the redeemSavingURL property.
	 * 
	 * @return the redeemSavingURL
	 */
	public String getRedeemSavingURL()
	{
		return redeemSavingURL;
	}

	/**
	 * Sets the value of the redeemSavingURL property.
	 * 
	 * @param redeemSavingURL
	 *            the redeemSavingURL to set
	 */
	public void setRedeemSavingURL(String redeemSavingURL)
	{
		this.redeemSavingURL = redeemSavingURL;
	}

	/**
	 * Gets the value of the currentBalance property.
	 * 
	 * @return the currentBalance
	 */

	public String getCurrentBalance()
	{
		return currentBalance;
	}

	/**
	 * Sets the value of the currentBalance property.
	 * 
	 * @param currentBalance
	 *            as of type String.
	 */
	public void setCurrentBalance(String currentBalance)
	{
		this.currentBalance = currentBalance;
	}

	/**
	 * Gets the value of the isFaveCategory property.
	 * 
	 * @return the isFaveCategory
	 */

	public String getTotalLifetime()
	{
		return totalLifetime;
	}

	/**
	 * Sets the value of the totalLifetime property.
	 * 
	 * @param totalLifetime
	 *            as of type String.
	 */
	public void setTotalLifetime(String totalLifetime)
	{
		this.totalLifetime = totalLifetime;
	}

	/**
	 * Gets the value of the couponsCur property.
	 * 
	 * @return the couponsCur
	 */

	public String getCouponsCur()
	{
		return couponsCur;
	}

	/**
	 * Sets the value of the couponsCur property.
	 * 
	 * @param couponsCur
	 *            as of type String.
	 */
	public void setCouponsCur(String couponsCur)
	{

		if (null == couponsCur)
		{
			this.couponsCur = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.couponsCur = couponsCur;
		}

	}

	/**
	 * Gets the value of the rebateCur property.
	 * 
	 * @return the rebateCur
	 */

	public String getRebateCur()
	{
		return rebateCur;
	}

	/**
	 * Sets the value of the rebateCur property.
	 * 
	 * @param rebateCur
	 *            as of type String.
	 */
	public void setRebateCur(String rebateCur)
	{

		if (null == rebateCur)
		{

			this.rebateCur = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.rebateCur = rebateCur;
		}

	}

	/**
	 * Gets the value of the fieldAgentCur property.
	 * 
	 * @return the fieldAgentCur
	 */

	public String getFieldAgentCur()
	{
		return fieldAgentCur;
	}

	/**
	 * Sets the value of the fieldAgentCur property.
	 * 
	 * @param fieldAgentCur
	 *            as of type String.
	 */
	public void setFieldAgentCur(String fieldAgentCur)
	{

		if (null == fieldAgentCur)
		{
			this.fieldAgentCur = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.fieldAgentCur = fieldAgentCur;
		}

	}

	/**
	 * Gets the value of the couponsTot property.
	 * 
	 * @return the couponsTot
	 */

	public String getCouponsTot()
	{
		return couponsTot;
	}

	/**
	 * Sets the value of the couponsTot property.
	 * 
	 * @param couponsTot
	 *            as of type String.
	 */
	public void setCouponsTot(String couponsTot)
	{

		if (null == couponsTot)
		{
			this.couponsTot = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.couponsTot = couponsTot;
		}

	}

	/**
	 * Gets the value of the rebateTot property.
	 * 
	 * @return the rebateTot
	 */

	public String getRebateTot()
	{
		return rebateTot;
	}

	/**
	 * Sets the value of the rebateTot property.
	 * 
	 * @param rebateTot
	 *            as of type String.
	 */
	public void setRebateTot(String rebateTot)
	{
		if (null == rebateTot)
		{
			this.rebateTot = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.rebateTot = rebateTot;
		}

	}

	/**
	 * Gets the value of the fieldAgentTot property.
	 * 
	 * @return the fieldAgentTot
	 */

	public String getFieldAgentTot()
	{
		return fieldAgentTot;
	}

	/**
	 * Sets the value of the fieldAgentTot property.
	 * 
	 * @param fieldAgentTot
	 *            as of type String.
	 */
	public void setFieldAgentTot(String fieldAgentTot)
	{
		if (null == fieldAgentTot)
		{
			this.fieldAgentTot = ApplicationConstants.NOTAPPLICABLE;
		}
		else
		{
			this.fieldAgentTot = fieldAgentTot;
		}

	}
	/*
	 * public String getLoyaltyProgramTot() { return loyaltyProgramTot; } public
	 * void setLoyaltyProgramTot(String loyaltyProgramTot) {
	 * if(null==loyaltyProgramTot){ this.loyaltyProgramTot="N/A"; }
	 * this.loyaltyProgramTot = loyaltyProgramTot; }
	 */

}
