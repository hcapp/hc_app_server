package com.scansee.common.pojos;

/**
 * this class for handling user payout type information.
 * 
 * @author shyamsundara_hm
 */
public class PayoutType extends BaseObject
{

	/**
	 * for payoutTypeID.
	 */
	private Integer payoutTypeID;
	/**
	 * for payoutTypeName.
	 */
	private String payoutTypeName;

	/**
	 * this method for getting payoutTypeID.
	 * 
	 * @return the payoutTypeID
	 */
	public Integer getPayoutTypeID()
	{
		return payoutTypeID;
	}

	/**
	 * this method for setting payoutTypeID.
	 * 
	 * @param payoutTypeID
	 *            the payoutTypeID to set
	 */
	public void setPayoutTypeID(Integer payoutTypeID)
	{
		this.payoutTypeID = payoutTypeID;
	}

	/**
	 * this method for getting payoutTypeName.
	 * 
	 * @return the payoutTypeName
	 */
	public String getPayoutTypeName()
	{
		return payoutTypeName;
	}

	/**
	 * this method for setting payoutTypeName.
	 * 
	 * @param payoutTypeName
	 *            the payoutTypeName to set
	 */
	public void setPayoutTypeName(String payoutTypeName)
	{
		this.payoutTypeName = payoutTypeName;
	}
}
