package com.scansee.common.pojos;


import java.util.List;

/**
 * The CreateOrUpdateCategories contains setter and getter methods.
 * @author sourab_r
 *
 */
public class CreateOrUpdateCategories 

        
{
	/**
	 * userId declared as integer.
	 */
	private Integer userId;
	
	/**
	 * category declared as List.
	 */
	private List<Category> category;
	/**
	 * To get userId.
	 * @return userId to get
	 */
	public Integer getUserId()
	{
		return userId;
	}
    
	/**
	 * To set userId.
	 * @param userId
	 *        -To set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
    
	/**
	 * To set category.
	 * @return category To get
	 */
	
	public List<Category> getCategory() {
		return category;
	}
    
	/**
	 * To set category.
	 * @param category
	 *        -To set.
	 */
	public void setCategory(List<Category> category) {
		this.category = category;
	}

	

}
