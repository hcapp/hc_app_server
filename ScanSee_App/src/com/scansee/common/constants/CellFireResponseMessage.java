package com.scansee.common.constants;

public class CellFireResponseMessage
{

	public static final String SUCCESS = "Success";

	public static final String UPDATEUSERINFOSUCCES = "Updated Successfully";

	public static final String FAILURE = "Failure";

	public static final String WRONGZIPCODE = "Wrong zip code";

	public static final String WRONGEMAILFORMAT = "Wrong email format";

	public static final String WRONGPHONEFORMAT = "Wrong phone format";

	public static final String INAVALIDAGE = "Not qualified year of birth, must be 13 years of age";

	public static final String SUCCESSFULASSOCIATION = "Card has been added successfully";

	public static final String DIFFERENTMERCHANT = "Card has been added to a different merchant";

	public static final String WRONGCARDNUMBER = "Card # is wrong";

	public static final String ADDINGCARDFAILED = "Adding card failed";

	public static final String REMOVECARDFAILED = "Removing card failed";

	public static final String SUCCESSFULREMOVAL = "Card has been removed successfully";
	
	public static final String CLIPSUCCESS = "Coupon has been clipped successfully";
	
	public static final String CLIPFAILED = "Clip failed";

	public static final String INVALIDLOYALTYID = "Invalid Loyalty Id";

	public static final String UNAUTHORIZEDACCESS = "Unauthorized access";

	public static final String CLIPPEDBEFORE = "Clipped before";

	public static final String REDEEMEDBEFORE = "Reddemed before(same coupon in same cycle)";

	public static final String LATESTCONTENT = "We are updating our database.Please try after some time.";
	
	public static final String CARDNOTREG = "Please add loyalty card";
	
	public static final String FIRSTTIMECLIP = "Coupon clipped \n \n Please wait %s minutes before redeeming them at the store.\n \n Continue clipping more Coupons.";

}
