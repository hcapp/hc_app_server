package com.scansee.common.constants;

/**
 * The class having all constants which are used in the application.
 * 
 * @author sowjanya_d
 */
public class ApplicationConstants
{
	/**
	 * NoHotDealsProductDetailFoundText declared as String for displaying
	 * message in HotDeals.
	 */
	public static final String NOHOTDEALSPRODUCTDETAILFOUNDTEXT = "Hot deal  detail not found";
	/**
	 * SUCCESS declared as String for displaying message in SUCCESS.
	 */
	public static final String SUCCESS = "SUCCESS";

	/**
	 * FAILURE declared as String for displaying message in FAILURE.
	 */
	public static final String FAILURE = "FAILURE";
	/**
	 * ResponseXml declared as String for displaying message errorCode and
	 * errorResponse in the form of xml.
	 */
	public static final String RESPONSEXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><error>10001<errorCode></errorCode><errorResponse></errorResponse></error>";
	/**
	 * inValidRequest declared as String for displaying message errorCode and
	 * errorResponse in the form of xml.
	 */
	public static final String INVALIDREQUEST = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><error>10002<errorCode></errorCode><errorResponse>Invalid Request.</errorResponse></error>";
	/**
	 * InsuffientData declared as String for getting error code.
	 */
	public static final String INSUFFICIENTDATA = "10007";
	/**
	 * SuccessCode declared as String for getting success response code.
	 */
	public static final String SUCCESSCODE = "10000";
	/**
	 * SuccessResponseText declared as String for getting success response text.
	 */
	public static final String SUCCESSRESPONSETEXT = "SUCCESS";
	/**
	 * DataAccessExceptionCode declared as String for getting error code.
	 */
	public static final String DATAACCESSEXCEPTIONCODE = "10008";
	/**
	 * TechnicalProblemErroCode declared as String for getting error code.
	 */
	public static final String TECHNICALPROBLEMERRORCODE = "10001";
	/**
	 * TechnicalProblemErrorText declared as String for getting error text.
	 */
	public static final String TECHNICALPROBLEMERRORTEXT = "There is a technical problem, please try after some time.";
	/**
	 * NoDeviceIdFound declared as String for showing this device id not found.
	 */
	public static final String NODEVICEIDFOUND = "DeviceID not found";
	/**
	 * InvalidRequestErroCode declared as String for getting error code.
	 */
	public static final String INVALIDREQUESTERRORCODE = "10002";
	/**
	 * InvalidRequestErrorText declared as String for getting error text.
	 */
	public static final String INVALIDREQUESTERRORTEXT = "Invalid Request.";
	/**
	 * InvalidUserNameORPassword declared as String for getting error code.
	 */
	public static final String INVALIDUSERNAMEORPASSWORD = "10003";
	/**
	 * InvalidUserNameORPasswordText declared as String for getting error text.
	 */
	public static final String INVALIDUSERNAMEORPASSWORDTEXT = "Invalid User/Password.";
	/**
	 * DuplicateUser declared as String for getting response code.
	 */
	public static final String DUPLICATEUSER = "10004";
	/**
	 * DuplicateUserText declared as String for getting response text.
	 */
	public static final String DUPLICATEUSERTEXT = "User Exists Already.";
	/**
	 * NoRecordsFound declared as String for getting response code.
	 */
	public static final String NORECORDSFOUND = "10005";
	/**
	 * NoRecordsFoundText declared as String for getting response text.
	 */
	public static final String NORECORDSFOUNDTEXT = "No Records Found.";
	/**
	 * NoRetailerFoundText declared as String for getting response text.
	 */
	public static final String NORETAILERFOUNDTEXT = "No Retailer Found.";
	/**
	 * NoProductFoundText declared as String for getting response text.
	 */
	public static final String NOPRODUCTFOUNDTEXT = "No Products Found.";
	/**
	 * NoCategoryFoundText declared as String for getting response text.
	 */
	public static final String NOCATEGORYFOUNDTEXT = "No Category Found.";
	/**
	 * ElementName declared as String for getting userId .
	 */
	public static final String ELEMENTNAME = "userId";
	/**
	 * NoShoppinglistProductText declared as String for getting response text.
	 */
	public static final String NOSHOPPINGLISTPRODUCTTEXT = "No Products  Found in the Shopping list.";
	/**
	 * NoShoppingCartProductText declared as String for getting response text.
	 */
	public static final String NOSHOPPINGCARDPRODUCTTEXT = "No Products  Found in the Shopping Cart.";
	/**
	 * NoShoppingBasketProductText declared as String for getting response text.
	 */
	public static final String NOSHOPPINGBASKETPRODUCTTEXT = "No Products  Found in the Shopping Cart.";
	/**
	 * NoFindNearByRetailerText declared as String for getting response text.
	 */
	public static final String NOFINDNEARBYRETAILERTEXT = "No Near By Retailer Found.";
	/**
	 * NoHotDealsFoundText declared as String for getting response text.
	 */
	public static final String NOHOTDEALSFOUNDTEXT = "No HotDeals Found ";
	/**
	 * NoUserNotes declared as String for getting response text.
	 */
	public static final String NOUSERNOTES = "User Notes are not added";
	/**
	 * NoWishlistProductText declared as String for getting response text.
	 */
	public static final String NOWISHLISTPRODUCTTEXT = "No Products  Found in the Wish list.";
	/**
	 * MethodStart declared as String for logger messages.
	 */
	public static final String METHODSTART = "In side method >>> ";
	/**
	 * MethodEnd declared as String for logger messages.
	 */
	public static final String METHODEND = " Exiting method >>> ";
	/**
	 * ExceptionOccurred declared as String for logger messages.
	 */
	public static final String EXCEPTIONOCCURRED = "Exception Occurred in  >>> ";
	/**
	 * ErrorOccurred declared as String for logger messages.
	 */
	public static final String ERROROCCURRED = "Error Occurred in  >>> ";
	/**
	 * NoEmailID declared as String for email id validation message.
	 */
	public static final String NOEMAILID = " EmailID Field should not be Empty.";
	/**
	 * NoPasswd declared as String for password validation message.
	 */
	public static final String NOPASSWORD = " Password Field should not be Empty.";
	/**
	 * NoDeviceId declared as String for device id validation message.
	 */
	public static final String NODEVICEID = " DeviceId Field should not be Empty.";
	/**
	 * DBErrorCode declared as Integer for assigning db error code.
	 */
	public static final Integer DBERRORCODE = 0;
	/**
	 * DBErrorText declared as Integer for assigning db error text.
	 */
	public static final String DBERRORTEXT = null;
	/**
	 * NoFieldAgent declared as String for FieldAgent validation message.
	 */
	public static final String NOFIELDAGENT = " FieldAgent Field should not be Empty.";
	/**
	 * NoFirstUseComplete declared as String for FirstUseComplete validation
	 * message.
	 */
	public static final String NOFIRSTUSECOMPLETE = " FirstUseComplete Field should not be Empty.";
	/**
	 * NoDob declared as String for Date Of Birth validation message.
	 */
	public static final String NODOB = " Date Of Birth Field should not be Empty.";
	/**
	 * NoDobValidation declared as String for Date Of Birth Field validation
	 * message .
	 */
	public static final String NODOBVALIDATION = " Date Of Birth Field should be proper date value.";
	/**
	 * InvalidEmailAddess declared as String for invalid email address error
	 * code.
	 */
	public static final String INVALIDEMAILADDRESS = "100010";
	/**
	 * InvalidEmailAddessText declared as String for invalid email address error
	 * text.
	 */
	public static final String INVALIDEMAILADDRESSTEXT = "Invalid Email Address";
	/**
	 * InvalidUserIdText declared as String for invalid userid text.
	 */
	public static final String INVALIDUSERIDTEXT = "Invalid UserId";
	/**
	 * PaymentIntervalID declared as String for null check of PaymentIntervalID
	 * paramenter validation message .
	 */
	public static final String PAYMENTINTERVALID = "PaymentIntervalID paramenter should not be null";
	/**
	 * PaymentTypeID declared as String for null check of PaymentTypeID
	 * paramenter validation message .
	 */
	public static final String PAYMENTTYPEID = "PaymentTypeID paramenter should not be null";
	/**
	 * PayoutTypeID declared as String for null check of PayoutTypeID paramenter
	 * validation message .
	 */
	public static final String PAYOUTTYPEID = "PayoutTypeID paramenter should not be null";
	/**
	 * DefaultPayout declared as String for null check of DefaultPayout
	 * paramenter validation message .
	 */
	public static final String DEFAULTPAYOUT = "DefaultPayout paramenter should not be null";
	/**
	 * This constant for retrieving Database error message.
	 */
	public static final String ERRORMESSAGE = "ErrorMessage";
	/**
	 * This constant for retrieving Database error code.
	 */
	public static final String ERRORNUMBER = "ErrorNumber";
	/**
	 * fetchNearBy declared as String for getting response text.
	 */
	public static final String FETCHNEARBY = "No Find Near by Retailers ";
	/**
	 * fetchNearByText declared as String for getting response text.
	 */
	public static final String FETCHNEARBYTEXT = "Either turn  on gps or provide zip code";
	/**
	 * NoUserID declared as String for getting response text.
	 */
	public static final String NOUSERID = "UserID is not available in the request.";
	/**
	 * NoRatings declared as String for getting response text.
	 */
	public static final String NORATINGS = "No Rating founds for the Product";
	/**
	 * userID declared as String for UserID text.
	 */
	public static final String USERID = "UserID";

	/**
	 * notApplicable declared as String for N/A text.
	 */
	public static final String NOTAPPLICABLE = "N/A";
	/**
	 * textXml declared as String for text/xml text.
	 */
	public static final String TEXTXML = "text/xml";
	/**
	 * NoMedia declared for Product media response.
	 */
	public static final String NOMEDIA = "No Media  founds for  this Product";
	/**
	 * ResponseLog declared for SHOWING Response XML returned MSG.
	 */
	public static final String RESPONSELOG = "Response XML returned {} ";
	/**
	 * REQUESTLOG declared for SHOWING Request XML received MSG.
	 */
	public static final String REQUESTLOG = "Request XML received {} ";
	/**
	 * DBSTATUS declared for database out variable.
	 */
	public static final String DBSTATUS = "Status";
	/**
	 * PAYMENTINFO declared for payouttypeid validation.
	 */
	public static final String PAYMENTINFO = "PaymentIntervalID/PaymentTypeID/PayoutTypeID paramenter should not be null";
	/**
	 * DBSTATUS declared for database out variable.
	 */
	public static final String PRODUCTSHAREINFO = "Prouduct Details not found for this product.";
	/**
	 * DBSTATUS declared for database out variable.
	 */
	public static final String MSLSCREENNAME = "Shopping List - Product List";
	/**
	 * This constant is used for This Location Retailer Pagination.
	 */
	public static final String THISLOCATIONRETAILERSCREEN = "This Location - Retailer List";
	/**
	 * This constant is used for Scan Hostory Pagination.
	 */
	public static final String SCANHISTORYDISPLAY = "Scan Now - Scan History";
	/**
	 * This constant is used for This Location Product Pagination.
	 */
	public static final String THISLOCATIONPRODUCTSSCREEN = "This Location - Product List";
	/**
	 * This constant is used for Hot deals search pagination.
	 */
	public static final String HOTDEALSSEARCHSCREEN = "Hot Deals - Hot Deals List";
	/**
	 * Variable for External API response format.
	 */
	public static final String EXTERNALAPIFORMAT = "xml";
	/**
	 * FOR settings & preferences prefered retailers pagination...
	 */
	public static final String MANAGESETTINGSPREFERREDRETAILERS = "Manage Settings - Preferred Retailers";
	/**
	 * for login status.
	 */
	public static final String LOGINSTATUS = "loginStatus";

	/**
	 * for UserID name validation message.
	 */
	public static final String UserID = "Mandatory field UserId is missing in the request";
	/**
	 * for first name validation message.
	 */
	public static final String NOFIRSTNAME = "Mandatory field First Name is missing in the request";

	/**
	 * for first name validation message.
	 */
	public static final String EMAILID = "Mandatory field Email Id is missing in the request";
	/**
	 * for last name validation message.
	 */
	public static final String NOLASTNAME = "Mandatory field Last Name is missing in the request";
	/**
	 * for No address validation message.
	 */
	public static final String NOADDRESS = "Mandatory field Address is missing in the request";
	/**
	 * for No imagepath validation message.
	 */
	public static final String NOIMGPATH = "images/imageNotFound.jpeg";
	/**
	 * for scanhistory pagination.
	 */
	public static final String SCAHHISTORYPAGINATION = "Scan Now - Scan History";
	/**
	 * for this location latitude longitude validation message.
	 */
	public static final String THISLOCATIONLATITUDELONGITUDE = "No Latitude ,Longitude and Postalcode found.";
	/**
	 * NOBODY declared as String for email id validation message.
	 */
	public static final String NOBODY = " Message Description Field should not be Empty.";
	/**
	 * EMAIL_PATTERN declared as String for email id validation.
	 */
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/**
	 * NOSUBJECT declared as String for email id validation message.
	 */
	public static final String NOSUBJECT = " Subject Field should not be Empty.";
	/**
	 * for empty string.
	 */
	public static final String EMPTYSTRING = " ";
	/**
	 * CONFIGURATIONTYPE declared as String for Email Decline Notification.
	 */
	public static final String CONFIGURATIONTYPE = "Decline Email";
	/**
	 * constructor for ApplicationConstants.
	 */
	public static final String THISLOCATIONPOSTALCODE = "No Postalcode  found.";
	/**
	 * constructor for ApplicationConstants.
	 */
	public static final String LATLONGNOTFOUND = "NO Latitude,Longitude Found.";
	/**
	 * for accountflag.
	 */
	public static final String ACCOUNTFLAG = "accountFlag";
	/**
	 * for no content out variable.
	 */
	public static final String FIRSTUSESTATICINFO = "No Content to Display";
	/**
	 * REMOVE declared as String for getting success response text.
	 */
	public static final String REMOVE = "DELETED";
	/**
	 * UPDATE declared as String for getting success response text.
	 */
	public static final String UPDATE = "UPDATED";
	/**
	 * SAVEUSERINFORMATION declared as String for getting success response text.
	 */
	public static final String SAVEUSERINFORMATION = "User information data added";
	/**
	 * USERPREFERENCEDATA declared as String for getting success response text.
	 */
	public static final String USERPREFERENCEDATA = "User preferences details added";
	/**
	 * SETUSERFAVCATEGORIES declared as String for getting success response
	 * text.
	 */
	public static final String SETUSERFAVCATEGORIES = "Preferred favourite categories are updated";
	/**
	 * SETPREFFEREDRETAILERS declared as String for getting success response
	 * text.
	 */
	public static final String SETPREFFEREDRETAILERS = "Preferred retailers added";
	/**
	 * PASSWORDUPDATED declared as String for getting success response text.
	 */
	public static final String PASSWORDUPDATED = "Password updated";
	/**
	 * ADDPRODUCTMAINTOSHOPPINGLIST declared as String for getting success
	 * response text.
	 */
	public static final String ADDPRODUCTMAINTOSHOPPINGLIST = "Added to Favorites";
	/**
	 * ADDPRODUCTSHOPPINGLISTFORDEBUG declared as String for getting success
	 * response text.
	 */
	public static final String ADDPRODUCTSHOPPINGLISTFORDEBUG = "Added to List";
	/**
	 * ADDUNASSIGNEDPRODUCT declared as String for getting success response
	 * text.
	 */
	public static final String ADDUNASSIGNEDPRODUCT = "Unassign product added";
	/**
	 * ADDREMOVESBPRODUCTS declared as String for getting success response text.
	 */
	public static final String ADDREMOVESBPRODUCTS = "Shopping basket product added/removed";
	/**
	 * ADDCOUPON declared as String for getting success response text.
	 */
	public static final String ADDCOUPON = "Coupon added";
	/**
	 * ADDLOYALTY declared as String for getting success response text.
	 */
	public static final String ADDLOYALTY = "Loyalty added";
	/**
	 * ADDREBATE declared as String for getting success response text.
	 */
	public static final String ADDREBATE = "Rebate added";
	/**
	 * ADDREMOVETODAYSLPRODUCTS declared as String for getting success response
	 * text.
	 */
	public static final String ADDREMOVETODAYSLPRODUCTS = "Today Shopping List added/remove";
	/**
	 * ADDTSLPRODUCTS declared as String for getting success response text.
	 */
	public static final String ADDTODAYSLPRODUCTS = "Product added to List";
	/**
	 * ADDUSERNOTES declared as String for getting success response text.
	 */
	public static final String ADDUSERNOTES = "User Notes Updated";
	/**
	 * SHAREPRODUCTINFO declared as String for getting success response text.
	 */
	public static final String SHAREPRODUCTINFO = "Email sent successfully.";
	/**
	 * PROCESSFIRSTUSE declared as String for getting success response text.
	 */
	public static final String PROCESSFIRSTUSE = "First use process is success";
	/**
	 * VALIDUSER declared as String for getting success response text.
	 */
	public static final String VALIDUSER = "Valid user";
	/**
	 * PUSHNOTIFYALERT declared as String for getting success response text.
	 */
	public static final String PUSHNOTIFYALERT = "Notify alert pushed";
	/**
	 * USERMEDIAINFOADDED declared as String for getting success response text.
	 */
	public static final String USERMEDIAINFOADDED = "User media info added";
	/**
	 * FORGOTPASSWORD declared as String for getting success response text.
	 */
	public static final String FORGOTPASSWORD = "Password sent";
	/**
	 * EMAILNOTIFICATION declared as String for getting success response text.
	 */
	public static final String EMAILNOTIFICATION = "Email sent successfully.";
	/**
	 * ADVERTISMENTHIT declared as String for getting success response text.
	 */
	public static final String ADVERTISMENTHIT = "Advertisment hit added";
	/**
	 * WISHLISTPRODUCTADDED declared as String for getting success response
	 * text.
	 */
	public static final String WISHLISTPRODUCTADDED = "Added to WishList";
	/**
	 * ACTIVETYPE declared as String for Email Decline Notification.
	 */
	public static final Integer ACTIVETYPE = 1;
	/**
	 * for adding unassigned product.
	 */
	public static final String ADDINGUNASSINGEDPRODUCT = "Added to list";
	/**
	 * Result declared for database out variable.
	 */
	public static final String FIRSTUSESCREENNAME = "Tutorial_Screen";
	/**
	 * FORGOTEMAILCONFIGURATIONTYPE declared as String for Email Decline
	 * Notification.
	 */
	public static final String FORGOTEMAILCONFIGURATIONTYPE = "Forgot Password";
	/**
	 * deleting product.
	 */
	public static final String DELETEPRODUCTTEXT = "Deleted";
	/**
	 * forgotPassword .
	 */
	public static final String FORGOTPASSWORDTEXT = "Password sent";
	/**
	 * saveUserPaymentPreference .
	 */
	public static final String SAVEUSERPAYMENTPREFERENCETEXT = "User Payment information saved";
	/**
	 * for adding unassigned product .
	 */
	public static final String ADDUNASSINGNEDPRODUCT = "User Payment information saved";
	/**
	 * ConfigurationType = 'Ticker' .
	 */
	public static final String FIRSTUSESCREENTICKER = "Ticker";
	/**
	 * for First use static screen content image.
	 */
	public static final String FIRSTUSESCREENWELCOMEIMAGE = "Welcome Image";
	/**
	 * for First use static screen content video.
	 */
	public static final String FIRSTUSESCREENWELCOMEVIDEO = "Welcome Video";
	/**
	 * for First use static screen content.
	 */
	public static final String FIRSTUSESCREENSTATICSCREENCONTENT = "Static Screen Content";
	/**
	 * for Hotdeals no category.
	 */
	public static final String HOTDEALSCATEGORYNOTFOUND = "Hot Deals Category Not Found";
	/**
	 * for invalid zipcode ..
	 */
	public static final String NOZIPCODE = "Zipcode should not be empty";
	/**
	 * for invalid zipcode.
	 */
	public static final String INVALIDZIPCODE = "Invalid Zipcode";
	/**
	 * for hotdeals updatation.
	 */
	public static final String HOTDEALSUPDATETEXT = "Updated Successfully";
	/**
	 * Can be used for all insufficient requests.
	 */
	public static final String INSUFFICENTTEXT = "Insufficient request";
	/**
	 * for Find near by .
	 */
	public static final String FINDNEARBYMODULENAME = "findNearBy";
	/**
	 * for findOnlineStores.
	 */
	public static final String FINDONLINESTOESMODULENAME = "FindOnlineStores";
	/**
	 * for No online stores.
	 */
	public static final String NOONLINESTORES = "No Online Stores";
	/**
	 * for Email id Validation.
	 */
	public static final String VALIDEMAILIDTEXT = "Enter Valid Email ID";
	/**
	 * for first name Validation.
	 */
	public static final String INVALIDFIRSTNAME = "First Name should not contain Special charactes.";
	/**
	 * for last name Validation.
	 */
	public static final String INVALIDLASTNAME = "Last Name should not contain Special charactes.";
	/**
	 * for last name Validation.
	 */
	public static final String INVALIDCITYNAME = "City Name should not contain Special charactes.";
	/**
	 * for last name Validation.
	 */
	public static final String INVALIDSTATENAME = "State Name should not contain Special charactes.";
	/**
	 * for address1 Validation.
	 */
	public static final String INVALIDADDRESS1 = "Address should not contain Special charactes.";
	/**
	 * for city Validation.
	 */
	public static final String INVALIDCITY = "City name should not contain Special charactes.";
	/**
	 * for state Validation.
	 */
	public static final String INVALIDSTATE = "State name should not contain Special charactes.";
	/**
	 * for postalcode Validation.
	 */
	public static final String INVALIDPOSTALCODE = "PostalCode should not contain Special charactes.";
	/**
	 * for mobile no Validation.
	 */
	public static final String INVALIDMOBILENOTEXT = "Mobile Number should not contain Special charactes.";
	/**
	 * for No of children Validation.
	 */
	public static final String INVALIDNOOFCHILDRENTEXT = "Number of Children field should not contain Special charactes.";
	/**
	 * for scan now search product pagination..
	 */
	public static final String SCANNOWSEARCHPRODPAGINATION = "General - Search Product Name";
	/**
	 * for external api failure.
	 */
	public static final int EXTERNALAPIFAILURE = 11002;
	/**
	 * for scan now search product pagination..
	 */
	public static final String WISHLISTHISTORYTEXT = "No Product Found in WishList History";
	/**
	 * NoWishlistProductText declared as String for getting response text.
	 */
	public static final String NOHOTDEALS = "No HotDeal Found in the Product.";
	/**
	 * for stores.
	 */
	public static final String STORES = " Stores";
	/**
	 * for share product text.
	 */
	public static final String SHAREPRODUCTTEXT = "Great find at ";
	/**
	 * for share product text.
	 */
	public static final String SHAREPRODUCTEXTFROMSCANSEE = " ScanSee!";
	/**
	 * for title media text.
	 */
	public static final String SHAREPRODUCTMEDIATITLETEXT = "Take a look at this!";
	/**
	 * NOWISHLISTPRODUCTCOUPONTEXT declared as String for getting response text.
	 */
	public static final String NOWISHLISTPRODUCTCOUPONTEXT = "No Coupon  Found";
	/**
	 * Declared For adding shopping list history product to list and favorites.
	 */
	public static final String SLHISTORYADDEDTOLISTANDFAVORITES = "Added to List and Favorites";
	/**
	 * Declared For adding shopping list history product to list and favorites.
	 */
	public static final String SLHISTORYPRODUCTEXISTINLIST = "Some item(s) weren't added because they were already in List";
	/**
	 * Declared For adding shopping list history product to list and favorites.
	 */
	public static final String SLHISTORYPRODUCTEXISTINFAVORITES = "Some item(s) weren't added because they were already in Favorites";
	/**
	 * Declared For adding shopping list history product to list and favorites.
	 * if product already exists we are sending this message.
	 */
	public static final String SLHISTORYPRODUCTEXISTINLISTANDFAVORITES = "Some item(s) weren't added because they were already in Favorites and List";
	/**
	 * Declared For adding shopping list history product to list and favorites.
	 */
	public static final String SLHISTORYPRODADDEDTOLISTFAVORITESTEXT = "Added to List and Favorites";
	/**
	 * THIS Location product search pagination screen name.
	 */
	public static final String THISLOCATIONPRODUCTSEARCHSCREENNAME = "General - Search Product Name";
	/**
	 * Declared LOCATEONMAPTEXT as String.
	 */
	public static final String LOCATEONMAPTEXT = "Unable to Locate the Zipcode on Map";
	/**
	 * Declared email for first use.
	 */
	public static final String EMAIL = "Email";

	/**
	 * Declared USERNAME for first use.
	 */
	public static final String USERNAME = "UserName";
	/**
	 * Declared password for first use.
	 */
	public static final String PASSWORD = "Password";
	/**
	 * Declared status for first use.
	 */
	public static final String STATUS = "Status";
	/**
	 * Declared ScreenName for first use.
	 */
	public static final String SCREENNAME = "ScreenName";

	/**
	 * Declared CLRSCREENNAME for my gallery.
	 */
	public static final String CLRSCREENNAME = "CLR Screen";

	/**
	 * Declared LOWERLIMIT for pagination.
	 */
	public static final String LOWERLIMIT = "LowerLimit";

	/**
	 * Declared EMPTY STRING.
	 */
	public static final String EMPTYSTR = "";
	/**
	 * Declared Comma.
	 */
	public static final String COMMA = ",";

	/**
	 * Declared For adding shopping list history product to list and favorites.
	 * if product already exists we are sending this message.
	 */
	public static final String SLHSTADDEDTOLISTNOTFAVORITES = "Added to List and some item(s) weren't added to Favorites  because they were already in Favorites";

	/**
	 * Declared For adding shopping list history product to list and favorites.
	 * if product already exists we are sending this message.
	 */
	public static final String SLHSTADDEDTOFAVORITESNOTLIST = "Added to Favorites and some item(s) weren't added to List  because they were already in List";
	/**
	 * Declared PRODUCTHEADER as String for App Configuration.
	 */
	public static final String PRODUCTHEADER = "ProductHeader";
	/**
	 * Declared PRODUCTIMAGE as String for App Configuration.
	 */
	public static final String PRODUCTIMAGE = "ProductImage";
	/**
	 * Declared NEARBYHEADER as String for App Configuration.
	 */
	public static final String NEARBYHEADER = "NearbyHeader";
	/**
	 * Declared NEARBYIMAGE as String for App Configuration.
	 */
	public static final String NEARBYIMAGE = "NearbyImage";
	/**
	 * Declared ONLINEHEADER as String for App Configuration.
	 */
	public static final String ONLINEHEADER = "OnlineHeader";
	/**
	 * Declared ONLINEIMAGE as String for App Configuration.
	 */
	public static final String ONLINEIMAGE = "OnlineImage";
	/**
	 * Declared REVIEWHEADER as String for App Configuration.
	 */
	public static final String REVIEWHEADER = "ReviewHeader";
	/**
	 * Declared REVIEWIMAGE as String for App Configuration.
	 */
	public static final String REVIEWIMAGE = "ReviewImage";
	/**
	 * Declared COUPONHEADER as String for App Configuration.
	 */
	public static final String COUPONHEADER = "CouponHeader";
	/**
	 * Declared COUPONIMAGE as String for App Configuration.
	 */
	public static final String COUPONIMAGE = "CouponImage";

	/**
	 * Declared GETDIRHEADER as String for App Configuration.
	 */
	public static final String GETDIRHEADER = "GetDirectionHeader";
	/**
	 * Declared GETDIRIMAGE as String for App Configuration.
	 */
	public static final String GETDIRIMAGE = "GetDirectionImage";
	/**
	 * Declared BROWSEWEBSITEHEADER as String for App Configuration.
	 */
	public static final String BROWSEWEBSITEHEADER = "BrowseWebsiteHeader";
	/**
	 * Declared BROWSEWEBSITEIMAGE as String for App Configuration.
	 */
	public static final String BROWSEWEBSITEIMAGE = "BrowseWebsiteImage";
	/**
	 * Declared BROWSEWEBSITEIMAGE as String for App Configuration.
	 */
	public static final String SLCLRSCREENNAME = "CLR Screen";

	/**
	 * Declared CONFIGURATIONTYPESTOCK as String for App Configuration.
	 */
	public static final String CONFIGURATIONTYPESTOCK = "Stock";

	/**
	 * Declared CONFIGURATIONTYPESTOCK as String for App Configuration.
	 */
	public static final String CONFIGURATIONTYPESHAREURL = "ShareURL";
	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String EMAILSHAREURL = "EmailShareURL";

	/**
	 * Declared TUTORIALSCREENIMAGE as String for App Configuration.
	 */
	public static final String TUTORIALSCREENIMAGE = "TutorialScreenImage";

	/**
	 * Declared password for first use.
	 */
	public static final String FACEBOOK = "FaceBookAuthenticatedUser";

	/**
	 * for Find near by .
	 */
	public static final String FINDMODULENAME = "Find";

	/**
	 * for Find near by .
	 */
	public static final String USERDEFAULTRADIUS = "Please set prefered radius";
	/**
	 * /** Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String EMAILCONFIG = "Email";

	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String FORGOTPWDCONFIG = "Forgot Password";

	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String SMTPHOST = "SMTP_Host";

	/**
	 * Declared EMAILSHAREURL as String for App Configuration.
	 */
	public static final String SMTPPORT = "SMTP_Port";
	/**
	 * for wish list add product.
	 */
	public static final String WISHLISTADDPRODUCTEXISTS = "Product already exists in WishList";
	/**
	 * Declared COUPONHEADER as String for App Configuration.
	 */
	public static final String DISCOUNTHEADER = "DiscountHeader";
	/**
	 * Declared COUPONIMAGE as String for App Configuration.
	 */
	public static final String DISCOUNTIMAGE = "DiscountImage";
	/**
	 * for redeem coupon.
	 */
	public static final String COUPONREDEEMRESPONSETEXT = "Coupon Redeemed.";
	/**
	 * for redeem rebate.
	 */
	public static final String REBATEREDEEMRESPONSETEXT = "Rebate Redeemed.";
	/**
	 * for redeem rebate.
	 */
	public static final String LOYALTYREDEEMRESPONSETEXT = "Loyalty Redeemed.";

	/**
	 * for redeem rebate.
	 */
	public static final String USEREMAILIDNOTEXIST = "Please enter your email Id in user information screen to get the password.";

	public static final String USEREMAILIDNOTEXISTTOSHARE = "Please Enter the From email Address below, to share the Item";

	public static final String SHAREPRODUCTINFONOTEXIST = "Product details not exist for this product.";

	public static final String USEREMAILIDNOTEXIST4 = "Please enter your email Id in user information screen to get the password.";

	/**
	 * FOR sharing coupon by email subject
	 */
	public static final String SHARECOUPONBYEMAILSUBJECT = "Coupon Information Shared Via ScanSee";
	/**
	 * FOR sharing hotdeal by email subject
	 */
	public static final String SHAREHOTDEALBYEMAILSUBJECT = "HotDeal Information Shared Via ScanSee";
	/**
	 * FOR share hot deal details not found.
	 */
	public static final String SHAREHOTDEALBYEMAILDETAILSNOTFOUND = "HotDeal Details not exist for this HotDeal Id.";
	/**
	 * FOR share hot deal details not found.
	 */
	public static final String SHARECOUPONBYEMAILDETAILSNOTFOUND = "Coupon Details not exist for this Coupon Id.";

	/**
	 * DuplicateUserText declared as String for getting response text.
	 */
	public static final String DUPLICATEPRODUCTTEXT = "Product already exists in ShoppingList.";

	/**
	 * for push notification details..
	 */
	/**
	 * for apple gateway.sandbox.push.apple.com test server ip
	 */
	public static final String HOST = "17.172.238.209";
	/***
	 * for apple gateway.sandbox.push.apple.com port
	 */
	public static final int PORT = 2195;
	/***
	 * for apple gateway.sandbox.push.apple.com Badge
	 */
	public static final int BADGE = 1111;
	/***
	 * for push notification certificate.
	 */
	public static final String PUSHNOTIFICATIONCERTIFICATE = "D:\\New_Certificate\\Certificates.p12";
	/***
	 * for push notification certificate password.
	 */
	public static final String CERTIFICATIONPASSWORD = "span@1234";
	/***
	 * for push notification success text.
	 */
	public static final String PUSHNOTIFICATIONSUCCESSTEXT = "Push notification sent successfully.";
	/***
	 * for duplicate product text code.
	 */
	public static final String DUPLICATEPRODUCTCODE = "10010";
	/**
	 * for constructor.
	 */

	public static final String SLHISTORYSCREENNAME = "Shopping List History";
	/**
	 * for constructor.
	 */
	public static final String WLHISTORYSCREENNAME = "Wish List History";
	/**
	 * for constructor.
	 */
	public static final String PUSHNOTIFREGISTRATION = "TokenID/DeviceID Exists Already.";

	/**
	 * User Favourite Categories screen name
	 */
	public static final String USERFAVCATEGORIES = "User Favourite Categories";
	/**
	 * for hotdeal no favorite category set
	 */
	public static final String HDNOFAVORITECATEGORYSET = "00000";
	/**
	 * for clr redeem response text.
	 */
	public static final String CLRALLREADYREDEEMRESPONSETEXT = "Already Redeemed.";

	/**
	 * FOR sharing Loyalty by email subject
	 */
	public static final String SHARELOYALTYBYEMAILSUBJECT = "Loyalty Information Shared Via ScanSee";
	/**
	 * FOR sharing Loyalty by email subject
	 */
	public static final String SHAREREBATEBYEMAILSUBJECT = "Rebate Information Shared Via ScanSee";
	/**
	 * Declared For adding product to today shopping list . if product already
	 * exists we are sending this message.
	 */
	public static final String SLADDPRODTLEXISTS = "Some item(s) weren't added to List  because they were already in List";
	/***
	 * for some of products r added or not added for today shopping list.
	 */
	public static final String DUPLICATETSLPRODUCTCODE = "10011";

	/**
	 * SUCCESS declared as String for displaying message in SUCCESS.
	 */
	public static final String USEREMAILUPDATE = "Email id updated";

	/**
	 * For image not found issue
	 */
	public static final String IMAGENOTFOUND = "http://app.scansee.net/Images/imageNotFound.png";

	/**
	 * PROCESSFIRSTUSE declared as String for getting success response text.
	 */
	public static final String AFTERREGISTRATIONTEXT = "ScanSee does not share or sell your private information to anyone.  Ever.  However, we do want to know about you to help us provide relevant and timely information that will be of interest to you and exclude information we know you won't like.";

	/**
	 * For user zip code exists
	 */
	public static final String ZIPCODEEXISTS = "true";

	/**
	 * For user zip code not exists
	 */
	public static final String ZIPCODENOTEXISTS = "false";

	/**
	 * For user zip code not exists
	 */
	public static final String ZIPCODEUPDATED = "Zipcode updated Successfully.";

	/**
	 * For user zip code not exists
	 */
	public static final String USERRETAILERSALENOTIFICATION = "Retailer Sale Notification Saved Successfully.";

	/**
	 * find retailer list pagination
	 */
	public static final String FINDSCREENNAME = "Find - Retailer List";

	/**
	 * find retailer list pagination
	 */
	public static final String FINDAPIPATNERNAME = "ScanSee";

	/**
	 * FOR sharing hotdeal by email subject
	 */
	public static final String SHARESPECIALOFFBYEMAILSUBJECT = "Retailer Special Offer Information Shared Via ScanSee";
	/**
	 * Created for scan product not found.
	 */
	public static final String SCANPRODNOTFOUND = "We could not recognize this scan, please search by typing in your request";

	/**
	 * created for latest version download text.
	 */
	public static final String LATESTAPPVERSIONDOWNLOADTEXT = "Latest version of ScanSee App available, please download";

	/**
	 * ElementName declared as String for getting updateVersionFlag .
	 */
	public static final String UPDATEVERSIONFLAG = "updateVersionFlag";

	/**
	 * ElementName declared as String for getting updateVersionURL .
	 */
	public static final String LATESTVERSIONURL = "updateVersionURL";
	/**
	 * ElementName declared as String for getting updateVersionFlag .
	 */
	public static final String LATESTVERSIONFLAG = "latestVerFlag";

	/**
	 * SuccessCode declared as String for getting success response code.
	 */
	public static final String LATESTVERSIONDOWNLOADCODE = "10012";

	/**
	 * SuccessResponseText declared as String for getting success response text.
	 */
	public static final String UPDATEAPPVERSIONTEXT = "User App Version Updated.";

	/**
	 * SCHEMANAME declared as String for database schema name.
	 */
	public static final String SCHEMANAME = "Version10";
	/**
	 * ElementName declared as String for getting updateVersionFlag .
	 */
	public static final String ADDDEVICETOUSER = "addDeviceToUser";

	/**
	 * SuccessResponseText declared as String for getting success response text.
	 */
	public static final String UPDATEUSERDEVICEIDTEXT = "User DeviceID Updated.";

	/**
	 * find retailer list pagination
	 */
	public static final String SMARTSEARCHSCREENNAME = "Smart Search Results";

	/**
	 * Declared CONFIGURATIONDEFAULTRADIUS as String for App Configuration.
	 */
	public static final String CONFIGURATIONDEFAULTRADIUS = "DefaultRadius";

	/**
	 * Declared DEFAULTRADIUSSCREEN as String for App Configuration.
	 */
	public static final String DEFAULTRADIUSSCREEN = "DefaultRadius";
	/**
	 * For No coupon found text.
	 */
	public static final String COUPONNOTFOUNDTEXT = "No Coupons Clipped, Visit All Coupons to Add";
	/**
	 * For No coupon found text.
	 */
	public static final String LOYALTYNOTFOUNDTEXT = "No Loyalty Clipped, Visit All Coupons to Add";
	/**
	 * for coupon screen name
	 */
	public static final String ALLCOUPSCREENNAME = "All Coupons";
	/**
	 * for user push notification and radius add or update.
	 */

	public static final String USERSETTINGSUPDATETEXT = "User Setting Added/Updated.";
	/**
	 * Declared RETAILERHOTDEALS for hot deal in this location. public static
	 * final String USERSETTINGSUPDATETEXT = "User Setting Added/Updated."; /**
	 * Declared RETAILERHOTDEALS for hot deal in this location.
	 */
	public static final String RETAILERHOTDEALS = "RetailerHotDeals";
	/**
	 * for Invalid user name.
	 */

	/**
	 * for Invalid user name.
	 */
	public static final String INVALIDUSERNAMETEXT = "User Not Found";
	/**
	 * for Invalid user name.
	 */
	public static final String INVALIDPASSWORDTEXT = "Invalid Password";

	public static String LOYALTY = "Loyalty";

	public static String CELLFIRE = "CellFire";

	public static String APIKEY = "Api-Key";
	
	public static String TLC = "tlc";

	public static String CELLFIREPARTNERID = "partnerId";
	/**
	 * Created for geocode api url , to fetch postalcode from user latitude and
	 * longitude
	 */
	public static final String GEOCODEURL = "http://maps.google.com/maps/api/geocode/json?";
	
	/**
	 * NoProductFoundText declared as String for getting response text.
	 */
	public static final String NOMERCHANTFOUNDTEXT = "No Merchants Found.";
	
	/**
	 * a varaible for code to notify if card has not been registered.
	 */
	
	public static final String CARDNOTREGISTERED = "10011";
	
	public static final String LOYALTYCARDINFO="Coupons you save to your %s %s are automatically applied when you use your card at checkout!";

	/**
	 * for card number
	 */
	
	public static final String INVALIDCARDNUMBERTEXT="Invalid Card Number.";
	/**
	 * a varaible for code to notify if card has not been registered.
	 */
	
	public static final String FISRTTIMECLIP="10013";

	/**
	 * NoHotDealsFoundText declared as String for getting response text.
	 */
	public static final String NOPREFERREDCATESETTEXT = "Please set Preferred categories to get the Hot Deals ";
	
	/**
	 * a varaible for code to notify if card has not been registered.
	 */
	
	public static final String CLSTARTDATETEXT="now";
	/**
	 * for constructor.
	 */
	
	public static final String CELLFIREBATCHPROCESSSTATUS="CellFire Batch Process Running";
	
	/**
	 * for Invalid user name.
	 */
	public static final String INVALIDUSRTEXT= "Invalid User";
	
	/**
	 * For User Tracking
	 */
	public static final String MAINMENUID = "MainMenuID";
	
	/**
	 * For user tracking
	 */
	public static final String RETAILERLISTID = "RetailerListID";
	
	/**
	 * For user tracking
	 */
	public static final String PRODUCTLISTID = "ProductListID";
	
	/**
	 * For user tracking
	 */
	public static final String MODULEID = "ModuleID";
	
	/**
	 * For user tracking
	 */
	public static final String PRODUCTSMARTSEARCHID = "ProductSmartSearchID";
	
	/**
	 * For user tracking
	 */
	public static final String SEARCHKEYWORD = "SearchKeyword";
	
	/**
	 * For user tracking
	 */
	public static final String DMAID = "DMAId";
	
	/**
	 * For user tracking
	 */
	public static final String SCANTYPESID = "ScanTypesID";
	
	/**
	 * For user tracking
	 */
	public static final String SCANTYPEID = "ScanTypeID";
	
	/**
	 * For user tracking
	 */
	public static final String HOTDEALLISTID = "HotDealListID";
	
	/**
	 * For user tracking
	 */
	public static final String LOCATIONDETAILID = "LocationDetailID";
	
	/**
	 * For user tracking
	 */
	public static final String LOCATIONDETAILSID = "LocationDetailsID";
	
	/**
	 * For user tracking
	 */
	public static final String FINDCATEGORYID = "FindCategoryID";
	
	/**
	 * For user tracking
	 */
	public static final String FINDRETAILERSEARCHID = "FindRetailerSearchID";
	
	/**
	 * For user tracking
	 */
	public static final String SPECIALLISTID = "SpecialListID";
	
	/**
	 * For user tracking
	 */
	public static final String LOCATEONMAP = "Locateonmap";
	
	/**
	 * Declared SHARE_APPSITE_SUBJECT as String for App Configuration.
	 * Note : AppSite� = In Java, use Unicode 16-bit character � = \u2122
	 */
	public static final String SHARE_APPSITE_SUBJECT = "AppSite\u2122 Shared Via ScanSee";
//	public static final String SHARE_APPSITE_SUBJECT = "AppSite\u00AE Shared Via ScanSee";
	
	public static final String STRING_ZERO = "0";
	
	/**
	 * DuplicateEmailText declared as String for getting response text.
	 */
	public static final String DUPLICATEEMAILTEXT = "Email Exists Already.";
	
	/**
	 * Declared TUTORIALSCREENIMAGE as String for App Configuration.
	 */
	public static final String CONFIGURATION_OF_SERVER = "Configuration of server";
	
	/**
	 * Declared EMAILIDNOTFOUND as String for App Configuration.
	 */
	public static final String EMAILIDNOTFOUND = "EmailID Not Found";
	/**
	 * Declared APPSITE_HEADING as String for App Configuration.
	 */
	public static final String APPSITE_HEADING = "I found this AppSite\u2122 @ScanSee and thought you might be interested:";
	/**
	 * Declared WISHLISTSEARCHSCREEN as String.
	 */
	public static final String WISHLISTSEARCHSCREEN = "Wish List - Pagination";
	/**
	 * Declared MASTERSHOPLISTPAGINATIOM as String.
	 */
	public static final String MASTERSHOPLISTPAGINATIOM = "Master Shopping list - Pagination";
	/**
	 * Declared TODAYSHOPLISTPAGINATIOM as String.
	 */
	public static final String TODAYSHOPLISTPAGINATIOM = "Today Shopping list - Pagination";
	/**
	 * INSUFFICENTQTY declared as String for getting error msg.
	 */
	public static final String INSUFFICENTQTY = "This promotion is no more available!";
	/**
	 * INSUFFICIENTQTY declared as String for getting error code.
	 */
	public static final String INSUFFICIENTQTY = "10009";
	/**
	 * INSUFFICENTQTY declared as String for getting error msg.
	 */
	public static final String POWEREDBY = "Google";
	/**
	 * ALREADYREGISTERED declared as String for getting response text.
	 */
	public static final String ALREADYREGISTERED = "You have already registered.";
	
	private ApplicationConstants()
	{
	}
}