package com.scansee.common.constants;

/**
 * The class having all constants which are used in the application.
 * 
 * @author team
 */
public class ScanSeeURLPath
{

	// Base URL path parameters
	/**
	 * wishListBaseURl declared as String for wishlist module.
	 */
	public static final String WISHLISTBASEURL = "/wishlist";
	/**
	 * manageSettingsBaseURl declared as String for managesettings module.
	 */
	public static final String MANAGESETTINGSBASEURL = "/managesettings";
	/**
	 * myAccountBaseURL declared as String for myaccount module.
	 */
	public static final String MYACCOUNTBASEURL = "/myaccount";
	/**
	 * firstUseBaseURL declared as String for firstUse module.
	 */
	public static final String FIRSTUSEBASEURL = "/firstUse";
	/**
	 * thisLocationBaseURL declared as String for thisLocation module.
	 */
	public static final String THISLOCATIONBASEURL = "/thisLocation";
	/**
	 * mediaURLPath declared as String .
	 */
	public static final String MEDIAURLPATH = "http://122.181.128.148:8080/";

	// Functional path parameters
	/**
	 * pushnotifyalert declared as String for showing popup to the user.
	 */
	public static final String PUSHNOTIFYALERT = "/pushnotifyalert";
	/**
	 * displaymainmenu declared as String for displaying main menu.
	 */
	public static final String DISPLAYMAINMENU = "/displaymainmenu";
	/**
	 * saveusermedia declared as String for adding user media.
	 */
	public static final String SAVEUSERMEDIA = "/saveusermedia";
	/**
	 * fetchtutorialmedia declared as String for fetching tutorial media inf.
	 */
	public static final String FETCHTUTORIALMEDIA = "/fetchtutorialmedia";

	// Functional path parameters for My Account Module
	/**
	 * myaccountdetails declared as String for getting myaccount details.
	 */
	public static final String MYACCOUNTDETAILS = "/myaccountdetails";
	/**
	 * forgotpassword declared as String for forgotpassword.
	 */
	public static final String FORGOTPASSWORD = "/forgotpassword";
	/**
	 * fetchuserpaymentinfo declared as String for fetching user payment
	 * information.
	 */
	public static final String FETCHUSERPAYMENTINFO = "/fetchuserpaymentinfo";
	/**
	 * saveuserpaymentpreference declared as String for saving user payment
	 * preference information.
	 */
	public static final String SAVEUSERPAYMENTPREFERENCE = "/saveuserpaymentpreference";

	/**
	 * fetchuserpaymentinfo declared as String for fetching user payment
	 * information.
	 */
	public static final String CHECKACCNTINFO = "/checkaccntinfo";

	// Functional path parameters for Wish List Module
	/**
	 * fetchproductdetails declared as String for getting wishlist product
	 * information .
	 */
	public static final String FETCHPRODUCTDETAILS = "/fetchproductdetails";
	/**
	 * deletewishlistitem declared as String for deleting wishlist product.
	 */
	public static final String DELETEWISHITEM = "/deletewishlistitem";
	/**
	 * getwishlistitems declared as String for getting wishlist items details.
	 */
	public static final String GETWISHLISTITEMS = "/getwishlistitems";
	/**
	 * get wish list product items declared as String for getting searched wish
	 * list products.
	 */
	public static final String GETWISHLISTPRODUCTITEMS = "/getwishlistproductitems";
	/**
	 * add unassigned prod declared as String for adding unassigned wish list
	 * product.
	 */
	public static final String ADDUNASSIGNEDPROD = "/addunassignedprod";
	/**
	 * add wishlist prod declared as String for adding searched wish list
	 * product.
	 */
	public static final String ADDWISHLISTPROD = "/addwishlistprod";
	/**
	 * fetchuserinfo declared as String for fetching user information in the
	 * settings.
	 */
	public static final String FETCHUSERINFO = "/fetchuserinfo";
	/**
	 * insertuserinfo declared as String for adding user information in the
	 * settings.
	 */
	public static final String INSERTUSERINFO = "/insertuserinfo";
	/**
	 * getRetailersInfo declared as String for getting Retailers information.
	 */
	public static final String GETRETAILERSINFO = "/getRetailersInfo";

	/**
	 * getRetailersInfo declared as String for getting Retailers information.
	 */
	public static final String FINDNEARBYPRODUCTS = "/findnearbyproducts";
	/**
	 * getCategoryInfo declared as String for getting Category information.
	 */
	public static final String GETCATEGORYINFO = "/getCategoryInfo";
	/**
	 * getProducts declared as String for getting product information.
	 */

	public static final String GETPRODUCTS = "/getProducts";
	/**
	 * getFavCatergories declared as String for getting Favorite Catergories .
	 */
	public static final String GETFAVCATERGORIES = "/getFavCatergories";
	/**
	 * getOtherCatergories declared as String for getting other categories.
	 */
	public static final String GETOTHERCATERGORIES = "/getOtherCatergories";
	/**
	 * saveuserpreference declared as String for adding user preference
	 * information.
	 */
	public static final String SAVEUSERPREFERENCE = "/savesettings";
	/**
	 * getuserpreference declared as String for getting user preference
	 * information.
	 */
	public static final String GETUSERPREFERENCE = "/getuserpreference";
	/**
	 * setfavcategories declared as String for adding Favorite Catergories .
	 */
	public static final String SETFAVCATEGORIES = "/setfavcategories";
	/**
	 * setpreferedretailers declared as String for adding preferred retailers.
	 */
	public static final String SETPREFEREDRETAILERS = "/setpreferedretailers";
	/**
	 * getfavcategories declared as String for getting favorite categories.
	 */
	public static final String GETFAVCATEGORIES = "/getfavcategories";
	/**
	 * getpreferedretailers declared as String for getting preferred retailers.
	 */
	public static final String GETPREFEREDRETAILERS = "/getpreferedretailers";

	/**
	 * for change password.
	 */
	public static final String CHANGEPASSWORD = "/changepassword";

	// FirstUse functional path parameters...
	/**
	 * For user Authentication.
	 */
	public static final String AUTHENTICATE = "/authenticate";
	/**
	 * Save user varaible.
	 */
	public static final String SAVEUSER = "saveUser";

	// HOTDEALS MODULE functional path parameters...
	/**
	 * This is the Base URL for HotDeals module.
	 */
	public static final String HOTDEALBASEURL = "/hotDeals";
	/**
	 * This is for fetch GETHOTDEALPRODS products..
	 */
	public static final String GETHOTDEALPRODS = "/getHotDealProds";

	/**
	 * This is for getting GETHOTDEALPRODINFO complete information...
	 */
	public static final String GETHOTDEALPRODINFO = "/getHdProdInfo";

	/**
	 * This is for removing hot deal product...
	 */
	public static final String REMOVEHDPROD = "/removeHdProd";
	/**
	 * this is for Getting user favorite category...
	 */
	public static final String GETHDCATEGORY = "/gethdcategory";

	// SCANNOW MODULE functional path parameters...
	/**
	 * Base url for scan now module...
	 */
	public static final String SCANNOEBASEURL = "/scanNow";

	/**
	 * Getting product barcode information..
	 */
	public static final String GETPRODFORBARCODE = "/getProdForBarCode";
	/**
	 * Getting.
	 */
	public static final String GETPRODFORNAME = "/getProdForName";
	/**
	 * Getting SDK information...
	 */

	public static final String GETSDK = "/getSDK";

	/**
	 * URL Path for ScanHistory.
	 */
	public static final String GETSCANHISTORY = "/getscanhistory";

	// Shopping List URL mapping

	/**
	 * Base URL for SL.
	 */
	public static final String SHOPPINGLIST = "/shoppingList";
	/**
	 * Base URL for SL.
	 */
	public static final String GETSHOPPINGLIST = "/getShoppingListItems";

	/**
	 * Base URL for SL.
	 */
	public static final String GETPRODUCTSINFO = "/getproductsinfo";

	/**
	 * Base URL for SL.
	 */
	public static final String ADDPRODUCTTOSL = "/addproducttosl";

	/**
	 * Base URL for SL.
	 */
	public static final String DELETESLPRODUCT = "/deleteslproduct";

	/**
	 * Base URL for SL.
	 */
	public static final String GETPRODUCTINFO = "/getproductinfo";

	/**
	 * Base URL for SL.
	 */
	public static final String GETSCPRODUCTS = "/getscproducts";

	/**
	 * Base URL for SL.
	 */
	public static final String ADDUNASSIGNEDPRODSL = "/addunassignedProd";

	/**
	 * Base URL for SL.
	 */
	public static final String FINDNEARBYRETAILERS = "/findNearByRetailers";

	/**
	 * Base URL for SL.
	 */
	public static final String ADDREMBASKET = "/addrembasket";

	/**
	 * Base URL for SL.
	 */
	public static final String GETSBPRODUCTS = "/getsbproducts";

	/**
	 * Base URL for SL.
	 */
	public static final String ADDCOUPON = "/addCoupon";

	/**
	 * Base URL for SL.
	 */
	public static final String REMOVECOUPON = "/removeCoupon";

	/**
	 * Base URL for SL.
	 */
	public static final String ADDLOYALTY = "/addLoyalty";

	/**
	 * Base URL for SL.
	 */
	public static final String REMOVELOYALTY = "/removeLoyalty";

	/**
	 * Base URL for SL.
	 */
	public static final String ADDREBATE = "/addRebate";

	/**
	 * Base URL for SL.
	 */
	public static final String REMOVEREBATE = "/removeRebate";

	/**
	 * Base URL for SL.
	 */
	public static final String GETCOUPONINFO = "/getCouponInfo";

	/**
	 * Base URL for SL.
	 */
	public static final String GETWISHLISTCOUPONINFO = "/getwlcouponinfo";

	/**
	 * Base URL for SL.
	 */
	public static final String GETLOYALTYINFO = "/getLoyaltyInfo";

	/**
	 * Base URL for SL.
	 */
	public static final String GETREBATEINFO = "/getRebateInfo";

	/**
	 * Base URL for SL.
	 */
	public static final String ADDREMCART = "/addremcart";

	/**
	 * Base URL for SL.
	 */
	public static final String GETSHOPPLISTANDCARTPROD = "/getshoppListandCartProd";

	/**
	 * Base URL for SL.
	 */
	public static final String ADDUSERNOTES = "/addusernotes";

	/**
	 * Base URL for SL.
	 */
	public static final String DELETEUSERNOTES = "/deleteusernotes";

	/**
	 * Base URL for SL.
	 */
	public static final String GETUSERNOTES = "/getusernotes";

	/**
	 * Base URL for SL.
	 */
	public static final String FINDNEARBY = "/findNearBy";

	/**
	 * Base URL for SL.
	 */
	public static final String FINDNEARBYLOWESTPRICE = "/findNearByLowestPrice";

	/**
	 * Base URL for SL.
	 */
	public static final String CHECKOUT = "/checkout";

	/**
	 * Base URL for SL.
	 */
	public static final String SHAREPRODUCTINFO = "/shareProductInfo";

	/**
	 * Base URL for SL.
	 */
	public static final String MEDIAINFO = "/mediainfo";

	/**
	 * URL for Rate review module.
	 */
	public static final String RATEREVIEW = "/ratereview";

	/**
	 * URL for rating.
	 */
	public static final String FETCHUSERRATING = "/fetchuserrating";

	/**
	 * Save user Rating.
	 */
	public static final String SAVEUSERRATING = "/saveuserrating";

	/**
	 * Save user shareProductRetailerInfo.
	 */
	public static final String SHAREPRODUCTRETAILERINFO = "/shareproductretailerinfo";

	/**
	 * Save user Rating.
	 */
	public static final String SHAREPRODRATEREVIEW = "/shareProdComment";

	/**
	 * For User Reviews.
	 */
	public static final String GETPRODUCTREVIEWS = "/getproductreviews";

	/**
	 * for MSL Retatiler and Category list.
	 */
	public static final String GETSLRETCATLIST = "/getslretailercategorylst";

	/**
	 * for displaying MSL product list according categories.
	 */
	public static final String GETMSLPRODUCTS = "/getmslproducts";

	/**
	 * for displaying Firstuse Welcome screen text.
	 */
	public static final String SCREENTEXTINFO = "/screentextinfo";

	/**
	 * for displaying Firstuse Welcome screen privacy.
	 */
	public static final String FETCHWELCOMESCREENPRIVACY = "/getwelcomescreenprivacy";

	/**
	 * for displaying Firstuse Welcome screen terms of use.
	 */
	public static final String FETCHWELCOMESCREENTERMSOFUSE = "/getwelcomescreentermsofuse";

	/**
	 * for displaying Firstuse Welcome screen terms of use.
	 */
	public static final String FETCHWELCOMESCREENDECLINE = "/getwelcomescreentermsofuse";

	/**
	 * for displaying shopping list product clr information.
	 */
	public static final String FETCHPRODUCTCLRINFO = "/fetchproductclrinfo";
	/**
	 * for online stores baseline.
	 */

	public static final String ONLINESTORES = "/findOnlineStores";

	/**
	 * for displaying Product summary info page accross all modules.
	 */
	public static final String GETPRODUCTSUMMARY = "/getproductsummary";

	/**
	 * for displaying user longitude and latitude.
	 */
	public static final String FETCHUSERLOCATIONPOINTS = "/fetchuserlocationpoints";

	/**
	 * for displaying shopping list product clr information.
	 */
	public static final String FETCHUSERPOSTALCODE = "/fetchuserpostalcode";

	/**
	 * Get lat and long values.
	 */
	public static final String FETCHLATLONG = "/fetchlatlong";

	/**
	 * for displaying MSL product list according categories.
	 */
	public static final String GETTSLPRODUCTS = "/gettslproducts";

	/**
	 * for displaying All countries.
	 */
	public static final String FETCHALLCOUNTRIES = "/fetchallcountries";

	/**
	 * fetch all states.
	 */
	public static final String FETCHALLSTATES = "/fetchallstates";

	/**
	 * fetch all cities.
	 */
	public static final String FETCHALLCITIES = "/fetchallcities";

	/**
	 * fetch all states and cities.
	 */
	public static final String FETCHALLSTATESANDCITIES = "/fetchallstatesandcities";

	/**
	 * emaildeclaimnotification declared as String for saving email.
	 */
	public static final String EMAILDECLAIMNOTIFICATION = "/emaildeclaimnotification";

	/**
	 * Base URL for SL.
	 */
	public static final String ADDTSLPRODUCTSEARCH = "/addtslbysearch";

	/**
	 * Base URL for CLRGallery.
	 */
	public static final String CLRGALLERY = "/clrgallery";

	/**
	 * for displaying MSL product list according categories.
	 */
	public static final String GETMSLCATEGORYPRODUCTS = "/getmslcategoryproducts";

	/**
	 * GETSTOREDETAILS declared as String for getting Store details.
	 */
	public static final String GETSTOREDETAILS = "/getstoredetails";

	/**
	 * for displaying MSL product list according categories.
	 */
	public static final String GETHOTDEALFORPRODUCT = "/getprodhotdeal";

	/**
	 * for displaying Wish list History.
	 */
	public static final String GETWISHLISTHISTORY = "/getwishlisthistory";

	/**
	 * for displaying coupon details.
	 */
	public static final String FETCHCOUPONDETAILS = "/fetchcoupondetails";

	/**
	 * for displaying Product Attribute details.
	 */
	public static final String FETCHPROUDCTATTRIBUTES = "/fetchproudctattributes";

	/**
	 * for displaying wish list Product coupon details.
	 */
	public static final String FETCHWISHLISTCOUPONINFO = "/fetchwishlistcouponinfo";

	/**
	 * Base URL for SL.
	 */
	public static final String FETCHSLHISTORY = "/fetchslhistory";

	/**
	 * Base URL for adding product from shopping list history to list or
	 * favorites or both
	 */
	public static final String ADDSLHISTORYPRODUCT = "/addslhistoryproduct";

	/**
	 * Base URL for MYGALLERY.
	 */
	public static final String MYGALLERY = "/mygallery";

	/**
	 * for MyGallery information.
	 */
	public static final String GETMYGALLERYINFO = "/getMyGalleryInfo";

	public static final String UPLOADIMAGE = "/uploadimage";

	/**
	 * for MyGallery All type information.
	 */
	public static final String GETMYGALLERYALLTYPEINFO = "/getMyGalleryAllTypeInfo";
	/**
	 * for MyGallery All type information.
	 */
	public static final String GETCLRDETAILS = "/getclrdetails";

	/**
	 * for displaying Contacts us details.
	 */
	public static final String FETCHCONTACTUSDETAILS = "/fetchcontactusdetails";

	/**
	 * Save facebook user varaible.
	 */
	public static final String SAVEFACEBOOKUSER = "savefacebookuser";

	/**
	 * Getting.
	 */
	public static final String GETSEARCHPRODFORNAME = "/getSearchProdForName";

	public static final String FINDURL = "/find";

	public static final String FINDCATSEARCH = "/findcatsearch";

	/**
	 * this is for Getting user favorite category...
	 */
	public static final String GETCATEGORY = "/getcategory";
	/**
	 * A private constructor.
	 */
	/**
	 * fetch all states and cities based on state.
	 */
	public static final String FETCHCATEGORIZEDCITIES = "/fetchcategorizedcities";
	/**
	 * for checking user zip code.
	 */
	public static final String CHECKUSERZIPCODE = "/checkuserzipcode";

	/**
	 * for sharing hotdeal and coupon information.
	 */
	public static final String SHAREHOTDEALINFO = "/sharehotdealinfo";

	/**
	 * for MyGallery redeem coupon information.
	 */
	public static final String USERREDEEMCOUPON = "/userredeemcoupon";
	/**
	 * for MyGallery redeem loyalty information.
	 */
	public static final String USERREDEEMLOYALTY = "/userredeemloyalty";
	/**
	 * for MyGallery redeem rebate information.
	 */
	public static final String USERREDEEMREBATE = "/userredeemrebate";

	/**
	 * fetch all states and cities based on state.
	 */
	public static final String GETHOTDEALPOPULATIONCENTERS = "/gethdpopulationcenters";

	/**
	 * Base URL for SL.
	 */
	public static final String SHARECLRBYEMAIL = "/shareclrbyemail";

	/**
	 * Base URL for SL.
	 */
	public static final String SHAREHOTDEALBYEMAIL = "/sharehotdealbyemail";

	/**
	 * fetch all states and cities based on state.
	 */
	public static final String SEARCHHDPOPULATIONCENTERS = "/searchhdpopcenters";

	/**
	 * for displaying Firstuse email static text.
	 */
	public static final String GENERALSCREENTEXTINFO = "/generalscreentextinfo";

	/**
	 * for MyGallery delete coupon information.
	 */
	public static final String DELETEUSEDCOUPON = "/deleteusedcoupon";

	/**
	 * for MyGallery delete loyalty information.
	 */
	public static final String DELETEUSEDLOYALTY = "/deleteusedloyalty";

	/**
	 * for MyGallery delete rebate information.
	 */
	public static final String DELETEUSEDREBATE = "/deleteusedrebate";

	public static final String FINDLOCATIONDETAILS = "/findlocdetails";

	/**
	 * for push notification class url
	 */
	public static final String PUSHNOTIFICATIONURL = "/pushnotification";
	/**
	 * for send push notification method
	 */
	public static final String SENDWLPUSHNOTIFICATION = "/sendwlpushnotification";

	/**
	 * Save facebook user varaible.
	 */
	public static final String REGISTERPUSHNOTIFICATION = "registerpushnotification";

	/**
	 * for sharing clr information
	 */
	public static final String SHARECLRINFO = "/shareclrinfo";

	/**
	 * Getting.
	 */
	public static final String GETSMARTSEARCHPRODUCTS = "/getsmartseaprods";

	/**
	 * for displaying shopping list product clr information.
	 */
	public static final String FETCHPRODDETAILS = "/fetchproddetails";

	/**
	 * to save user mail id .
	 */
	public static final String SAVEUSEREMID = "/saveuseremid";
	/**
	 * to save user mail id .
	 */
	public static final String FETCHUNIVSTATES = "/getunivstates";
	/**
	 * to save user mail id .
	 */
	public static final String FETCHUNIVERSITIES = "/getuniversities";

	/**
	 * to get radius details .
	 */
	public static final String GETRADIUSINFO = "/getradiusinfo";

	/**
	 * Get lat and long values.
	 */
	public static final String UPDATESUSRZIPCODE = "/updateusrzipcode";

	/**
	 * Get lat and long values.
	 */
	public static final String CHECKUSRZIPCODE = "/checkusrzipcode";

	/**
	 * FOR saving user retailer sale hit.
	 */
	public static final String SAVEUSRRETSALEHIT = "/saveusrretsalehit";

	/**
	 * for retailer store information
	 */
	public static final String GETRETSTOREINFO = "/getretstoreinfo";

	/**
	 * for retailer summary information
	 */
	public static final String GETRETSUMMARY = "/getretsummary";

	/**
	 * for retailer special offer details
	 */
	public static final String GETSPECIALOFFINFO = "/getspecialoffinfo";

	/**
	 * for get coupon product information.
	 */
	public static final String GETCLRPRODINFO = "/getclrprodinfo";

	/**
	 * Base URL for share special offer info.
	 */
	public static final String SHARESPECIALOFF = "/sharespecialoff";

	/**
	 * deletewishlistitem declared as String for deleting wishlist product.
	 */
	public static final String DELETEWLHISTORYITEM = "/deletewlhistory";

	/**
	 * deletewishlistitem declared as String for deleting wishlist product.
	 */
	public static final String DELETESCANHISTORYITEM = "/deletescanhistory";

	/**
	 * updateappver declared as String for deleting wishlist product.
	 */
	public static final String UPDATEAPPVERSION = "/updateappver";

	/**
	 * updateappver declared as String for deleting wishlist product.
	 */
	public static final String ADDDEVICETOUSER = "/adddevicetouser";

	/**
	 * fetch all cities.
	 */
	public static final String GETPOSTALCODES = "/getpostalcodes";
	/**
	 * For find location search..
	 */
	public static final String FINDRETSEARCH = "/findretsearh";

	/**
	 * For scan now smart search count
	 */
	public static final String GETSMARTSEARCHCOUNT = "/getsmartseacount";

	/**
	 * For smart search product list.
	 */
	public static final String GETSMARTSEARCHPRODS = "/getsearchprodlst";
	/**
	 * For Searching category in database.
	 */
	public static final String FINDSSCATEGORY = "/findsscatsearch";

	/**
	 * For Searching retailer in database.
	 */
	public static final String FINDSSRETSEARCH = "/findssretsearch";

	public static final String MANAGELOYALTYCARD = "/manageloyaltycard";

	public static final String CELLFIREUPDATEUSERINFO = "/updateuserinfo";

	public static final String CELLFIREADDLOYALTYCARD = "/addloyaltycard";

	public static final String CELLFIREREMOVELOYALTYCARD = "/removeloyaltycard";

	public static final String CELLGETSAVEDLOYALTYCARDS = "/getsavedloyaltycards";

	/**
	 * for retailer summary information
	 */
	public static final String GETRETSPEOFFINFO = "/getspedealslist";

	/**
	 * for retailer summary information
	 */
	public static final String GETRETCLRINFO = "/getretclrinfo";
	/**
	 * for retailer summary information
	 */
	public static final String GETRETHOTDEALS = "/getrethotdeals";

	/**
	 * for retailer special offers, hotdeals and clr display
	 */
	public static final String GETRETSPHOTCLRINFO = "/getretspcialoffs";
	      
	      public static final String GETMERCHANTS = "/getmerchants";
	      
	      public static final String GETCARDDETAIL = "/getcarddetail";

	/**
	 * for retailer special offers, hotdeals and clr display
	 */
	public static final String GETSPDEALINFO = "/getspdealinfo";

	/**
	 * For retriving category list.
	 */
	public static final String RETRIVECATEGORY = "/retrivecategory";
	/**
	 * For coupon retailer list
	 */
	public static final String GETCOUPRETLST = "/getcouretlst";

	/**
	 * For loyalty retailer list
	 */
	public static final String GETLOYRETLST = "/getloyretlst";
	
	/**
	 * to get radius details .
	 */
	public static final String GETUSRRAD = "/getusrrad";

	/**
	 * to get austin retailers
	 */
	public static final String GETRETBYGROUPID =  "/getretbygroupid";
	
	/**
	 * to get go local retailers
	 */
	public static final String GETRETFORPARTNER =  "/getretforpartner";

	/**
	 * To get partners
	 */
	public static final String GETPARTNERS = "/getpartners";
	
	/**
	 * To get categories for partner retailers
	 */
	public static final String GETCATFORPARTNER = "/getcatforpartner";
	
	/**
	 * To get categories for group retailers
	 */
	public static final String GETCATFORGROUP = "/getcatforgroup";
	
		/**
	 * To share the app site by email
	 */
	public static final String SHAREAPPSITEEMAIL = "shareappsiteemail";
	

	/**
	 * SHAREAPPSITE declared as String constant variable.
	 */
	public static final String SHAREAPPSITE = "/shareappsite";
	
	/**
	 * For user tracking. For main menu.
	 */
	public static final String UTMODULEDISPLAY = "/utmoduledisplay";
	
	/**
	 * For user tracking. For module count
	 */
	public static final String UTMODULESELECT = "/utmoduleselect";
	
	/**
	 * For user tracking. For thisLocation count
	 */
	public static final String UTLOCATIONDETAILS = "/utlocationdetails";
	
	/**
	 * For user tracking. For retailer summary screen click
	 */
	public static final String UTRETSUMCLICK = "/utretsumclick";

	/**
	 * For user tracking. For retailer summary screen count
	 */
	public static final String UTRETSPEOFFCLICK = "/utretspeoffclick";
	
	/**
	 * For user tracking.
	 */
	public static final String UTGETHOTDEALCLICK = "/utgethotdealclick";
	
	/**
	 * For user tracking.
	 */
	public static final String UTPRODSMARTSEA = "/utprodsmartsea";
	
	/**
	 * For user tracking.
	 */
	public static final String UTGOOGLERET = "/utgoogleret";
	
	/**
	 * For user tracking.
	 */
	public static final String UTPRODSMASEA = "/utprodsmasea";
	/**
	 * GET_DEFAULT_PRODUCT_LIST  declared as String constant.
	 */
	public static final String GET_DEFAULT_PRODUCT_LIST = "/getProducts";
	
	/**
	 * For getting scan type
	 */
	public static final String UTGETSCANTYPE = "/utgetscantype";
	
	/**
	 * For  product media list ID
	 */
	public static final String UTPMCLICK = "/utpmclick";
	
	/**
	 * For  online store
	 */
	public static final String UTONSTOCLICK = "/utonstoclick";
	
	/**
	 * For  online store
	 */
	public static final String UTSHARETYPE = "/utsharetype";
	
	/**
	 * UTGETMAINMENID  declared as String constant.
	 */
	public static final String UTGETMAINMENID = "/utgetmainmenuid";
	/**
	 * GETSTARTEDIMAGES  declared as String constant.
	 */
	public static final String GETSTARTEDIMAGES = "/getstartedimages";
	/**
	 * GIVEAWAYUSERREGIST declared as String constant.
	 */
	public static final String GIVEAWAYUSERREGIST = "/giveawayuserregistration";
	/**
	 * GIVEAWAYPAGE declared as String constant.
	 */
	public static final String GIVEAWAYPAGE = "/giveawaypage";
	
	/**
	 * To get scan see data in find category search
	 */
	public static final String FINDSCANSEECATSEARCH = "/findscanseecatsearch";
	
	/**
	 * To get scan see data in find category search
	 */
	public static final String FINDGOOGLECATSEARCH = "/findgooglecatsearch";
	
	/**
	 * GETHDPRODSBYCATEGORY declared as String constant.
	 */
	public static final String GETHDPRODSBYCATEGORY = "/getHDProdsByCategory";
	
	/**
	 * FINDGOOGLERETSEARCH declared as String constant.
	 */
	public static final String FINDGOOGLERETSEARCH = "/findgoogleretsearch";
	/**
	 * FINDSCANSEERETSEARCH declared as String constant.
	 */
	public static final String FINDSCANSEERETSEARCH = "/findscanseeretsearch";
	
	/**
	 * HOTDEALCLAIM declared as string, To claim hot deal.
	 */
	public static final String HOTDEALCLAIM = "/hotdealclaim";
	
	/**
	 * HOTDEALREDEEM declared as string, To redeem hot deal.
	 */
	public static final String HOTDEALREDEEM = "/hotdealredeem";
	
	/**
	 * GETALLCOUPBYLOC declared as string
	 */
	public static final String GETALLCOUPBYLOC = "/getallcoupbyloc";
	
	/**
	 * ALLCOUPBYPROD declared as string
	 */
	public static final String GETALLCOUPBYPROD = "/getallcoupbyprod";
	
	/**
	 * COUPLOCPOPCENT declared as string
	 */
	public static final String COUPLOCPOPCENT = "/couplocpopcent";
	
	/**
	 * ALLCOUPBYPROD declared as string
	 */
	public static final String COUPPRODPOPCENT = "/coupprodpopcent";
	
	/**
	 *  COUPLOCBUSCAT declared as string coupon location business category
	 */
	public static final String COUPLOCBUSCAT = "/couplocbuscat";
	
	/**
	 *  RETFORBUSCAT declared as string retailer for business category
	 */
	public static final String RETFORBUSCAT = "/retforbuscat";
	
	/**
	 * COUPPRODCAT declared as string for coupon product category
	 */
	public static final String COUPPRODCAT = "/coupprodcat";
	
	/**
	 * for GETCOUPONDETAILS declared as String constant.
	 */
	public static final String GETCOUPONDETAILS = "/getcoupondetails";
	/**
	 * for GETCOUPONDETAILS declared as String constant.
	 */
	public static final String GETCOUPONPRODUCTLOCATIONLIST = "/getcouponproductorloclist";
	
	public static final String GALLCOUPBULOC = "/gallcoupbyloc";
	
	public static final String GALLCOUPBYPROD = "/gallcoupbyprod";
	
	public static final String GALLHDBYLOC = "/gallhdbyloc";
	
	public static final String GALLHDBYPROD = "/gallhdbyprod";
	
	protected ScanSeeURLPath()
	{

	}

}
