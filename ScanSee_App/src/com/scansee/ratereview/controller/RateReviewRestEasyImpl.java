package com.scansee.ratereview.controller;

import static com.scansee.common.util.Utility.formResponseXml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.servicefactory.ServiceFactory;
import com.scansee.ratereview.service.RateReviewService;

/**
 * The RateReviewRestEasyImpl has methods to accept RateReviewRestEasy
 * requests.These methods are called on the Client request. The method is
 * selected based on the URL Path and the type of request(GET,POST). It invokes
 * the appropriate method in Service layer.
 * 
 * @author shyamsundara_hm
 */

public class RateReviewRestEasyImpl implements RateReviewRestEasy
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(RateReviewRestEasyImpl.class);
	/**
	 * For setting logger .
	 */

	private static final boolean ISDEBUGENABLED = LOG.isDebugEnabled();

	/**
	 * This is a RestEasyImplimentation WebService method for get the user
	 * product rating list. Calls method in service layer. accepts a GET
	 * request, MIME type is text/xml.
	 * 
	 * @param userId
	 *            input request for which needed to fetch user product rating
	 *            list.
	 * @param productId
	 *            input request for which needed to fetch user product rating
	 *            list. If userId or proudctId is null then it is invalid
	 *            request.
	 * @return XML containing user product rating list information in the
	 *         response.
	 */
	@Override
	public String fecthUserProductRating(Integer userId, Integer productId)
	{
		final String methodName = "fecthUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved  request  parameters UserID {} and ProductID", userId, productId);
		}
		String response = null;
		final RateReviewService rateReviewService = ServiceFactory.getRateReviewService();
		try
		{
			response = rateReviewService.fecthUserProductRating(userId, productId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved Response : " + userId);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasyImplimentation WebService method for saving user
	 * product rating Calls method in service layer. accepts a POST request,
	 * MIME type is text/xml
	 * 
	 * @param xml
	 *            as request parameter. If userId or productId is null then it
	 *            is invalid request.
	 * @return XML containing success or failure response
	 */

	@Override
	public String saveUserProductRating(String xml)
	{
		final String methodName = "fecthUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved request xml ", xml);
		}
		String response = null;
		final RateReviewService rateReviewService = ServiceFactory.getRateReviewService();
		try
		{
			response = rateReviewService.saveUserProductRating(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved Response --> " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasyImplimentation WebService method for product review
	 * comments Calls method in service layer. accepts a GET request, MIME type
	 * is text/xml
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param productId
	 *            as request parameter If userId or productId is null then it is
	 *            invalid request.
	 * @return XML containing success or failure response
	 */

	public String shareProdRateReview(Integer userId, Integer productId)
	{
		final String methodName = "shareProdRateReview";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved request User ID {} and ProductID", userId, productId);
		}
		String response = null;
		final RateReviewService rateReviewService = ServiceFactory.getRateReviewService();
		try
		{
			response = rateReviewService.shareProductReview(userId, productId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved Response is" + userId);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching product reviews and
	 * user ratings.Method Type:GET.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return XML Containing Product Reviews and User Ratings.
	 */
	@Override
	public String getProductReviews(Integer userId, Integer productId)
	{
		final String methodName = "getProductReviews";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved request User ID {} and ProductID", userId, productId);
		}
		String response = null;
		final RateReviewService rateReviewService = ServiceFactory.getRateReviewService();
		try
		{
			if (null != userId && null != productId)
			{
				response = rateReviewService.getProductReviews(userId, productId);
			}
			else
			{
				response = formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
			}

		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved Response " + userId);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method used to share product information
	 * from This Location and other modules. Retailer Information is also shared
	 * if product is shared from This Location module.
	 * 
	 * @param xml
	 *            contains information about the Product to be shared.
	 * @return XML in the response containig product and retailerc information.
	 */
	@Override
	public String shareProductRetailerInfo(String xml)
	{
		final String methodName = "shareProductRetailerInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved request xml ", xml);
		}
		String response = null;
		final RateReviewService shoppingListService = ServiceFactory.getRateReviewService();
		try
		{
			response = shoppingListService.shareProductRetailerInfo(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved Response " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method used to share product information
	 * via Email.Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the Product to be shared and
	 *            Recipients.
	 * @return XML success or failure as response.
	 */
	@Override
	public String shareProductInfo(String xml)
	{

		final String methodName = "shareProductInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final RateReviewService rateReviewService = ServiceFactory.getRateReviewService();
		try
		{
			response = rateReviewService.shareProductInfo(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}
		LOG.debug("Response returned " + response);
		return response;
	}
	/**
	 * This is a RestEasy WebService Method used to share hot deal or coupon information
	 * 
	 * @param xml
	 *            contains information about the hot deal or coupon to be shared.
	 * @return XML in the response containing hot deal or coupon information.
	 */
	@Override
	public String shareHotDealInfo(Integer hotdealId,Integer userId)
	{
		final String methodName = "shareHotDealCouponInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved request hotdeal Id"+ hotdealId + "userId :"+ userId);
		}
		String response = null;
		final RateReviewService rateReviewService = ServiceFactory.getRateReviewService();
		try
		{
			response = rateReviewService.shareHotdealInfo(hotdealId,userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved Response " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method used to share coupon information through mail
	 * 
	 * @param xml
	 *            contains information about the hot deal or coupon to be shared.
	 * @return XML in the response containing hot deal or coupon information.
	 */
	@Override
	public String shareCLRbyEmail(String xml)
	{
		final String methodName = "shareCLRbyEmail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final RateReviewService rateReviewService = ServiceFactory.getRateReviewService();
		try
		{
			response = rateReviewService.shareCLRbyEmail(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}
		LOG.debug("Response returned " + response);
		return response;
	}
	/**
	 * This is a RestEasy WebService Method used to share hot deal or coupon information
	 * 
	 * @param xml
	 *            contains information about the hot deal or coupon to be shared.
	 * @return XML in the response containing hot deal or coupon information.
	 */
	@Override
	public String shareHotdealByEmail(String xml)
	{
		final String methodName = "shareHotdealByEmail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final RateReviewService rateReviewService = ServiceFactory.getRateReviewService();
		try
		{
			response = rateReviewService.shareHotdealbyEmail(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}
		LOG.debug("Response returned " + response);
		return response;
	}
	/**
	 * This is a RestEasy WebService Method used to share coupon,rebate and loyalty information
	 * 
	 * @param xml
	 *            contains information about the coupon,rebate and loyalty to be shared.
	 * @return XML in the response containing coupon,rebate and loyalty.
	 */
	
	@Override
	public String shareCLRInfo(Integer userId,Integer couponId, Integer loyaltyId, Integer rebateId)
	{
		final String methodName = "shareCLRInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved request userId :" + userId +"couponId :"+ couponId + "loyaltyId :"+loyaltyId + "rebateId :" + rebateId);
		}
		String response = null;
		final RateReviewService rateReviewService = ServiceFactory.getRateReviewService();
		try
		{
			response = rateReviewService.shareCLRInfo(userId,couponId,loyaltyId,rebateId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved Response " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	/**
	 * This is a RestEasy WebService Method used to share retailer special offer information
	 * via Email.Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the special offer to be shared and
	 *            Recipients.
	 * @return XML success or failure as response.
	 */
	@Override
	public String shareSpecialOffInfoByEmail(String xml)
	{
		final String methodName = "shareSpecialOffInfoByEmail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final RateReviewService rateReviewService = ServiceFactory.getRateReviewService();
		try
		{
			response = rateReviewService.shareSpecialOffByEmail(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}
		LOG.debug("Response returned " + response);
		return response;
	}
	
	/**
	 * This is a RestEasy WebService Method used when the user clicks Share, 
	 * they will be prompted to share via different options Text/Twitter/Facebook  will send wording,
	 *  retailer name, and link to the AppSite�  information.
	 * 
	 * @param xml contains information about the special offer to be shared and Recipients.
	 * @return XML success or failure as response.
	 */
	@Override
	public String shareAppsite(String xml)
	{
		final String methodName = "shareAppsite";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final RateReviewService rateReviewService = ServiceFactory.getRateReviewService();
		try
		{
			response = rateReviewService.shareAppsite(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}
		LOG.debug("Response returned " + response);
		return response;
	}
	/**
	 * This is a RestEasy WebService Method used to share app site via email.
	 * Method Type:POST.
	 * 
	 * @param xml
	 *            contains recipient email ID, share type, user ID, retailer ID and retailer location ID.
	 *            
	 * @return XML success or failure as response.
	 */
	@Override
	public String shareAppSiteByEmail(String xml) {
		final String methodName = "shareAppSiteByEmail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final RateReviewService rateReviewService = ServiceFactory.getRateReviewService();
		try
		{
			response = rateReviewService.shareAppSiteByEmail(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}
		LOG.debug("Response returned " + response);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method to update share type
	 * Method Type: POST
	 * 
	 * @param xml
	 * @return No return parameter
	 */
	@Override
	public void userTrackingShareTypeUpdate(String xml) {
		final String methodName = "userTrackingShareTypeUpdate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final RateReviewService rateReviewService = ServiceFactory.getRateReviewService();
		try
		{
			response = rateReviewService.userTrackingShareTypeUpdate(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}
		LOG.debug("Response returned " + response);
	}

}
