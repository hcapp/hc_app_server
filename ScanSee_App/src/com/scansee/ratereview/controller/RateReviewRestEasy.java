package com.scansee.ratereview.controller;

import static com.scansee.common.constants.ScanSeeURLPath.FETCHUSERRATING;
import static com.scansee.common.constants.ScanSeeURLPath.GETPRODUCTREVIEWS;
import static com.scansee.common.constants.ScanSeeURLPath.RATEREVIEW;
import static com.scansee.common.constants.ScanSeeURLPath.SAVEUSERRATING;
import static com.scansee.common.constants.ScanSeeURLPath.SHAREHOTDEALINFO;
import static com.scansee.common.constants.ScanSeeURLPath.SHAREPRODRATEREVIEW;
import static com.scansee.common.constants.ScanSeeURLPath.SHAREPRODUCTINFO;
import static com.scansee.common.constants.ScanSeeURLPath.SHAREPRODUCTRETAILERINFO;
import static com.scansee.common.constants.ScanSeeURLPath.SHARECLRBYEMAIL;
import static com.scansee.common.constants.ScanSeeURLPath.SHAREHOTDEALBYEMAIL;
import static com.scansee.common.constants.ScanSeeURLPath.SHARECLRINFO;
import static com.scansee.common.constants.ScanSeeURLPath.SHARESPECIALOFF;
import static com.scansee.common.constants.ScanSeeURLPath.SHAREAPPSITE;
import static com.scansee.common.constants.ScanSeeURLPath.SHAREAPPSITEEMAIL;
import static com.scansee.common.constants.ScanSeeURLPath.UTSHARETYPE;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.inject.ImplementedBy;

/**
 * The RateReviewRestEasy Interface. {@link ImplementedBy}
 * {@link RateReviewRestEasyImpl}
 * 
 * @author shyamsundara_hm
 */

@Path(RATEREVIEW)
public interface RateReviewRestEasy
{

	/**
	 * This is a RestEasy WebService Method for fetching User product rating for
	 * the given userId,productId. Method GET
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param productId
	 *            as request parameter. If userId or productId is null then it
	 *            is invalid request.
	 * @return XML containing productId,current rating and average rating
	 */
	@GET
	@Path(FETCHUSERRATING)
	@Produces("text/xml")
	String fecthUserProductRating(@QueryParam("userId") Integer userId, @QueryParam("productId") Integer productId);

	/**
	 * This is a RestEasy WebService Method for saving User product rating for
	 * the given userId,productId. Method POST
	 * 
	 * @param xml
	 *            as request parameter. If userId or productId is null then it
	 *            is invalid request.
	 * @return XML containing success or failure response
	 */
	@POST
	@Path(SAVEUSERRATING)
	@Produces("text/xml")
	@Consumes("text/xml")
	String saveUserProductRating(String xml);

	/**
	 * This is a RestEasy WebService Method for sharing product review comments
	 * for the given userId,productId. Method GET
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param productId
	 *            as request parameter. If userId or productId is null then it
	 *            is invalid request.
	 * @return XML containing product description,url and price.
	 */
	@GET
	@Path(SHAREPRODRATEREVIEW)
	@Produces("text/xml")
	String shareProdRateReview(@QueryParam("userId") Integer userId, @QueryParam("productId") Integer productId);

	/**
	 * This is a RestEasy WebService Method for fetching product reviews and
	 * user ratings. Method Type:GET
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return XML Containing Product Reviews and User Ratings.
	 */
	@GET
	@Path(GETPRODUCTREVIEWS)
	@Produces("text/xml;charset=UTF-8")
	String getProductReviews(@QueryParam("userId") Integer userId, @QueryParam("productId") Integer productId);

	/**
	 * This is a RestEasy WebService Method used to share product information
	 * from This Location and other modules. Retailer Information is also shared
	 * if product is shared from This Location module.
	 * 
	 * @param xml
	 *            contains information about the Product to be shared.
	 * @return XML in the response containig product and retailerc information.
	 */
	@POST
	@Path(SHAREPRODUCTRETAILERINFO)
	@Produces("text/xml;charset=UTF-8")
	String shareProductRetailerInfo(String xml);

	/**
	 * This is a RestEasy WebService Method used to share product information
	 * via Email.Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the Product to be shared and
	 *            Recipients.
	 * @return XML success or failure as response.
	 */
	@POST
	@Path(SHAREPRODUCTINFO)
	@Produces("text/xml;charset=UTF-8")
	String shareProductInfo(String xml);
	
	/**
	 * This is a RestEasy WebService Method used to share hot deal or  coupon information
	 * from  modules. 
	 * 	 * 
	 * @param xml
	 *            contains information about the hot deal or coupon to be shared.
	 * @return XML in the response containing hot deal or coupon information.
	 */
	@GET
	@Path(SHAREHOTDEALINFO)
	@Produces("text/xml;charset=UTF-8")
	String shareHotDealInfo(@QueryParam("hotdealId") Integer hotdealId,@QueryParam("userId") Integer userId);

	/**
	 * This is a RestEasy WebService Method used to share coupon information
	 * via Email.Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the coupon to be shared and
	 *            Recipients.
	 * @return XML success or failure as response.
	 */
	@POST
	@Path(SHARECLRBYEMAIL)
	@Produces("text/xml;charset=UTF-8")
	String shareCLRbyEmail(String xml);
	/**
	 * This is a RestEasy WebService Method used to share hotdeal information
	 * via Email.Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the hotdeal to be shared and
	 *            Recipients.
	 * @return XML success or failure as response.
	 */
	@POST
	@Path(SHAREHOTDEALBYEMAIL)
	@Produces("text/xml;charset=UTF-8")
	String shareHotdealByEmail(String xml);
	
	/**
	 * This is a RestEasy WebService Method used to share coupon,loyalty and rebate information
	 * from  modules. 
	 * 	 * 
	 * @param couponid
	 *            contains information about coupon to be shared.
	 * @param loyaltyid
	 * 			contains information about loyalty to be shared
	 * @param rebateid
	 * 				contains information about rebate to be shared
	 * 	
	 * @return XML in the response containing hot deal or coupon information.
	 */
	@GET
	@Path(SHARECLRINFO)
	@Produces("text/xml;charset=UTF-8")
	String shareCLRInfo(@QueryParam("userId") Integer userId,@QueryParam("couponid") Integer couponId, @QueryParam("loyaltyid") Integer loyaltyId,@QueryParam("rebateid") Integer rebateId);
	
	
	/**
	 * This is a RestEasy WebService Method used to share retailer special offer information
	 * via Email.Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the special offer to be shared and
	 *            Recipients.
	 * @return XML success or failure as response.
	 */
	@POST
	@Path(SHARESPECIALOFF)
	@Produces("text/xml;charset=UTF-8")
	String shareSpecialOffInfoByEmail(String xml);
	
	/**
	 * This is a RestEasy WebService Method used to share app site via email.
	 * Method Type:POST.
	 * 
	 * @param xml
	 *            contains destination email, share type, user ID, retailer ID and retailer location ID
	 *            
	 * @return XML success or failure as response.
	 */
	@POST
	@Path(SHAREAPPSITEEMAIL)
	@Consumes("text/xml;charset=UTF-8")
	@Produces("text/xml")
	String shareAppSiteByEmail(String xml);
	
	/**
	 * This is a RestEasy WebService Method used to share retailer special offer information
	 * via Email.Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the special offer to be shared and
	 *            Recipients.
	 * @return XML conmtaining retailer details.
	 */
	@POST
	@Path(SHAREAPPSITE)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml;charset=UTF-8")
	String shareAppsite(String xml);
	
	/**
	 * This is a RestEasy WebService Method to update share type
	 * Method Type: POST
	 * 
	 * @param xml
	 * @return NULL
	 */
	@POST
	@Path(UTSHARETYPE)
	@Consumes("text/xml")
	void userTrackingShareTypeUpdate(String xml);
}
