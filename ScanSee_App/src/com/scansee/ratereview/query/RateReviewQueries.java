package com.scansee.ratereview.query;

/**
 * This class for handling RateReview queries.
 * @author shyamsundara_hm
 *
 */
public class RateReviewQueries
{
	
	/**
     * for fetching product short description and retail price.
     */
	public static final String  GETSHAREPRODUCTINFO = "SELECT ProductShortDescription prodShortDesc,SuggestedRetailPrice prodSugPrice FROM Product WHERE  ProductID=?";

	/**
	 * For fetching product review details.
	 */
	public static final String  GETPRODUCTREVIEWS = "SELECT ProductReviewsID productReviewsID,ProductID productID,ReviewURL reviewURL,ReviewComments reviewComments FROM ProductReviews WHERE  ProductID=?";

	/**
	 * constructor for RateReviewQueries.
	 */
     protected RateReviewQueries()
     {
	 
     }
}
