package com.scansee.ratereview.service;

import static com.scansee.common.util.Utility.formResponseXml;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.email.EmailComponent;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.helper.XstreamParserHelper;
import com.scansee.common.pojos.AppConfiguration;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.LoyaltyDetail;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductRatingReview;
import com.scansee.common.pojos.ProductReview;
import com.scansee.common.pojos.RebateDetail;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.ShareProductInfo;
import com.scansee.common.pojos.ShareProductInfoEmailLink;
import com.scansee.common.pojos.ShareProductRetailerInfo;
import com.scansee.common.pojos.UserRatingInfo;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.common.util.Utility;
import com.scansee.ratereview.dao.RateReviewDAO;
import com.scansee.shoppinglist.dao.ShoppingListDAO;

/**
 * This class is implementation of RateReviewService methods.
 * 
 * @author shyamsundara_hm
 */
public class RateReviewServiceImpl implements RateReviewService
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(RateReviewServiceImpl.class);

	/**
	 * Instance variable for Shopping list DAO instance.
	 */

	private RateReviewDAO rateReviewDao;

	/**
	 * Instance variable for Shopping list DAO instance.
	 */

	private ShoppingListDAO shoppingListDao;

	/**
	 * sets the ShoppingListDAO DAO.
	 * 
	 * @param rateReviewDao
	 *            The instance for RateReviewDAO
	 */
	public void setRateReviewDao(RateReviewDAO rateReviewDao)
	{
		this.rateReviewDao = rateReviewDao;
	}

	/**
	 * sets the ShoppingListDAO DAO.
	 * 
	 * @param shoppingListDao
	 *            The instance for ShoppingListDAO
	 */
	public void setShoppingListDao(ShoppingListDAO shoppingListDao)
	{
		this.shoppingListDao = shoppingListDao;
	}

	/**
	 * This is a service implementation method for get the user product rating
	 * information. Calls method in dao layer. accepts a GET request, MIME type
	 * is text/xml.
	 * 
	 * @param userId
	 *            input request needed to fetch user product rating list.
	 * @param productId
	 *            input request needed to fetch user product rating list. If
	 *            userId or proudctId is null then it is invalid request.
	 * @return XML containing user product rating list information in the
	 *         response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String fecthUserProductRating(Integer userId, Integer productId) throws ScanSeeException
	{
		final String methodName = "fecthUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (userId == null || productId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			final UserRatingInfo userRatingInfo = rateReviewDao.fecthUserProductRating(userId, productId);
			if (null != userRatingInfo)
			{
				response = XstreamParserHelper.produceXMlFromObject(userRatingInfo);
			}
			else
			{
				LOG.info("No records found for the userid {}", userId);
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORATINGS);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a service implementation method for saving user product rating
	 * information. Calls method in dao layer. accepts a POST request, MIME type
	 * is text/xml.
	 * 
	 * @param xml
	 *            input request for which needed to save user product rating. If
	 *            userId or proudctId is null then it is invalid request.
	 * @return XML containing success or failure in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String saveUserProductRating(String xml) throws ScanSeeException
	{
		final String methodName = "saveUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final UserRatingInfo userRatingInfo = (UserRatingInfo) parser.parseXmlToObject(xml);
		if (userRatingInfo.getUserId() == null || userRatingInfo.getProductId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			response = rateReviewDao.saveUserProductRating(userRatingInfo);
			if (null != response && response.equals(ApplicationConstants.SUCCESS))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SUCCESSRESPONSETEXT);
			}
			else
			{
				response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method for shareProductReview.
	 * 
	 * @param userId
	 *            the request parameter.
	 * @param productId
	 *            as the request parameter.
	 * @return response xml with product description,url and price information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String shareProductReview(Integer userId, Integer productId) throws ScanSeeException
	{
		final String methodName = "fecthUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (userId == null || productId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			final UserRatingInfo userRatingInfo = rateReviewDao.getShareProductInfo(userId, productId);
			if (null != userRatingInfo)
			{
				response = XstreamParserHelper.produceXMlFromObject(userRatingInfo);
			}
			else
			{
				LOG.info("No records found for the userid {}", userId);
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.PRODUCTSHAREINFO);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The Service Method for fetching product reviews and user ratings.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return XML String Containing Product Reviews and User Ratings.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getProductReviews(Integer userId, Integer productId) throws ScanSeeException
	{
		final String methodName = "getProductReviews";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (userId == null || productId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			final ProductRatingReview productRatingReview = new ProductRatingReview();
			final ArrayList<ProductReview> productReviewslist = rateReviewDao.getProductReviews(userId, productId);
			final UserRatingInfo userRatingInfo = rateReviewDao.fecthUserProductRating(userId, productId);

			if (null != productReviewslist && !productReviewslist.isEmpty())
			{
				productRatingReview.setProductReviews(productReviewslist);
				productRatingReview.setTotalReviews(String.valueOf(productReviewslist.size()));

			}
			else
			{
				productRatingReview.setTotalReviews("0");
			}

			if (null != userRatingInfo)
			{

				productRatingReview.setUserRatingInfo(userRatingInfo);
				response = XstreamParserHelper.produceXMlFromObject(productRatingReview);

			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. This Method fetches product and retailer
	 * information.
	 * 
	 * @param xml
	 *            contains information to fetch product and retailer details.
	 * @return String XML with Product and Retailer Details.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	@Override
	public String shareProductRetailerInfo(String xml) throws ScanSeeException
	{
		final String methodName = "shareProductRetailerInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		RetailerDetail retailerDetail = null;
		ShareProductRetailerInfo shareProductRetailerInfo = null;
		ShareProductInfoEmailLink shareProductInfoEmailLink = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final UserRatingInfo shareProductInfo = (UserRatingInfo) parser.parseXmlToObject(xml);

		if (shareProductInfo.getUserId() == null || shareProductInfo.getProductId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			final ProductDetail productDetail = rateReviewDao.getProductInfo(shareProductInfo);
			if (1 == shareProductInfo.getIsFromThisLocation())
			{
				if (null == shareProductInfo.getRetailerId())
				{
					LOG.info("Validation failed RetailerId is not available");
					return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
				}
				if (null == shareProductInfo.getRetailerLocationId())
				{
					LOG.info("Validation failed RetailerLocationId() is not available");
					return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
				}

				retailerDetail = rateReviewDao.getRetailerInfoWithSalePrice(shareProductInfo.getRetailerId(),
						shareProductInfo.getRetailerLocationId(), Integer.valueOf(shareProductInfo.getProductId()));

			}

			if (null != productDetail)
			{

				shareProductRetailerInfo = new ShareProductRetailerInfo();
				shareProductInfoEmailLink = new ShareProductInfoEmailLink();
				shareProductInfoEmailLink.setEmailTemplateURL(productDetail.getEmailTemplateURL());
				shareProductInfoEmailLink.setImagePath(productDetail.getImagePath());
				shareProductInfoEmailLink.setProductName(productDetail.getProductName());
				shareProductRetailerInfo.setTitleText(ApplicationConstants.SHAREPRODUCTTEXT + ApplicationConstants.SHAREPRODUCTEXTFROMSCANSEE);
				shareProductRetailerInfo.setTitleText2(ApplicationConstants.SHAREPRODUCTMEDIATITLETEXT);
				shareProductRetailerInfo.setProductDetail(shareProductInfoEmailLink);

			}

			if (null != retailerDetail)
			{

				final RetailerDetail retailerDetails = new RetailerDetail();
				retailerDetails.setCompleteAddress(retailerDetail.getRetaileraddress1() + "," + retailerDetail.getCity() + ","
						+ retailerDetail.getState() + "," + retailerDetail.getPostalCode());
				retailerDetails.setRetailerName(retailerDetail.getRetailerName());
				retailerDetails.setSalePrice(retailerDetail.getSalePrice());
				shareProductRetailerInfo.setTitleText(ApplicationConstants.SHAREPRODUCTTEXT + retailerDetail.getRetailerName()
						+ ApplicationConstants.SHAREPRODUCTEXTFROMSCANSEE);
				shareProductRetailerInfo.setRetailerDetail(retailerDetails);

			}

			if (null != shareProductRetailerInfo)
			{
				response = XstreamParserHelper.produceXMlFromObject(shareProductRetailerInfo);
			}
			else
			{
				response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}

		return response;
	}

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Sends email to recipients with product information.
	 * 
	 * @param xml
	 *            contains email id, product to be shared.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String shareProductInfo(String xml) throws ScanSeeException
	{

		final String methodName = " shareProductInfo of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String productInfo = null;
		String userEmailId = null;
		String toAddress = null;
		String[] toMailArray = null;
		String fromAddress = null;
		String subject = "Product information shared from Scansee";
		String msgBody = null;
		RetailerDetail retailerDetail = null;
		String shareURL = null;
		ArrayList<AppConfiguration> stockInfoList = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ShareProductInfo shareProductInfoObj = (ShareProductInfo) streamHelper.parseXmlToObject(xml);
		boolean valFlag = true;
		if (null != shareProductInfoObj)
		{
			if (null == shareProductInfoObj.getUserId())
			{
				LOG.info("Validation failed User Id is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
			}
			else if (null == shareProductInfoObj.getProductId())
			{
				LOG.info("Validation failed Product Id is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			}
			else if (null == shareProductInfoObj.getToEmail())
			{
				LOG.info("Validation failed To Email is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			}
			else if (1 == shareProductInfoObj.getIsFromThisLocation())
			{

				if (null == shareProductInfoObj.getRetailerId())
				{
					LOG.info("Validation failed RetailerId is not available");
					valFlag = false;
					response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
				}
				else if (null == shareProductInfoObj.getRetailerLocationId())
				{
					LOG.info("Validation failed RetailerLocationId() is not available");
					valFlag = false;
					response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
				}

				retailerDetail = rateReviewDao.getRetailerInfoWithSalePrice(shareProductInfoObj.getRetailerId(),
						shareProductInfoObj.getRetailerLocationId(), shareProductInfoObj.getProductId());
			}
		}
		if (valFlag && null != shareProductInfoObj)
		{
			final int userId = shareProductInfoObj.getUserId();
			stockInfoList = shoppingListDao.getAppConfig(ApplicationConstants.CONFIGURATIONTYPESHAREURL);

			for (int i = 0; i < stockInfoList.size(); i++)
			{
				if (stockInfoList.get(i).getScreenName().equals(ApplicationConstants.EMAILSHAREURL))
				{
					shareURL = stockInfoList.get(i).getScreenContent();
				}

			}

			productInfo = rateReviewDao.getProductInfo(shareProductInfoObj, retailerDetail, userId, shareURL);
			if (productInfo == null)
			{
				LOG.info("Validation failed To Email is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.SHAREPRODUCTINFONOTEXIST);
				return response;
			}
			userEmailId = rateReviewDao.getUserInfo(shareProductInfoObj);
			if (userEmailId == null)
			{
				LOG.info("Validation failed To Email is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.USEREMAILIDNOTEXISTTOSHARE);
				return response;
			}
			toAddress = shareProductInfoObj.getToEmail();

			try
			{
				emailConf = shoppingListDao.getAppConfig(ApplicationConstants.EMAILCONFIG);

				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
					{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
					{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}

				subject = "Product Information Shared Via ScanSee";
				msgBody = productInfo;
				fromAddress = userEmailId;
				final String delimiter = ";";
				//toAddress=toAddress.replaceAll("\\s+", "");
				toMailArray = toAddress.split(delimiter);
				if (null != smtpHost || null != smtpPort)
				{
					EmailComponent.multipleUsersmailingComponent(fromAddress, toMailArray, subject, msgBody, smtpHost, smtpPort);

				}

				LOG.info("Mail delivered to:" + toAddress);
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SHAREPRODUCTINFO);
			}
			catch (MessagingException exception)
			{
				LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
				response = Utility.formResponseXml(ApplicationConstants.INVALIDEMAILADDRESS, ApplicationConstants.INVALIDEMAILADDRESSTEXT);
			}

		}
		else
		{
			if (shareProductInfoObj == null)
			{
				response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. This Method fetches hot deal or coupon information.
	 * 
	 * @param xml
	 *            contains information to fetch hot deal or coupon details.
	 * @return String XML with hot deal or coupon Details.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	@Override
	public String shareHotdealInfo(Integer hotdealId, Integer userId) throws ScanSeeException
	{
		final String methodName = "shareHotdealInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		List<HotDealsDetails> hotDealsDetailslst = null;

		if (userId == null && hotdealId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			hotDealsDetailslst = rateReviewDao.getShareHotdealInfo(hotdealId);
			if (!hotDealsDetailslst.isEmpty() && hotDealsDetailslst != null)
			{
				for(HotDealsDetails hdDetails : hotDealsDetailslst)	{
					hdDetails.setIsDateFormated(false);
					hdDetails.sethDStartDate(hdDetails.gethDStartDate());
					hdDetails.sethDEndDate(hdDetails.gethDEndDate());
					hdDetails.setIsDateFormated(null);
				}
				
				hotDealsDetailslst.get(0).setTitleText(ApplicationConstants.SHAREPRODUCTTEXT + ApplicationConstants.SHAREPRODUCTEXTFROMSCANSEE);
				hotDealsDetailslst.get(0).setTitleText2(ApplicationConstants.SHAREPRODUCTMEDIATITLETEXT);
				response = XstreamParserHelper.produceXMlFromObject(hotDealsDetailslst);
				response = response.replaceAll("<list>", "<ShareHotdealInfo>");
				response = response.replaceAll("</list>", "</ShareHotdealInfo>");
			}
			else
			{
				LOG.info("No records found for the userid {}", hotdealId);
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}

		return response;
	}

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. This Method fetches hot deal or coupon information.
	 * 
	 * @param xml
	 *            contains information to fetch coupon details.
	 * @return String XML with coupon Details.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	@Override
	public String shareCLRbyEmail(String xml) throws ScanSeeException
	{
		final String methodName = " shareCLRbyEmail of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String productInfo = null;
		String userEmailId = null;
		String toAddress = null;
		String[] toMailArray = null;
		String fromAddress = null;
		String subject = null;
		String msgBody = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ShareProductInfo shareProductInfoObj = (ShareProductInfo) streamHelper.parseXmlToObject(xml);
		boolean valFlag = true;
		if (null != shareProductInfoObj)
		{
			if (null == shareProductInfoObj.getUserId())
			{
				LOG.info("Validation failed User Id is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
			}
			else if (null == shareProductInfoObj.getCouponId() && null == shareProductInfoObj.getLoyaltyId()
					&& null == shareProductInfoObj.getRebateId())
			{
				LOG.info("Validation failed Product Id is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			}
			else if (null == shareProductInfoObj.getToEmail())
			{
				LOG.info("Validation failed To Email is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			}

			/*
			 * if (valFlag && null != shareProductInfoObj) { final int userId =
			 * shareProductInfoObj.getUserId(); stockInfoList =
			 * shoppingListDao.getAppConfig
			 * (ApplicationConstants.CONFIGURATIONTYPESHAREURL); for (int i = 0;
			 * i < stockInfoList.size(); i++) { if
			 * (stockInfoList.get(i).getScreenName
			 * ().equals(ApplicationConstants.EMAILSHAREURL)) { shareURL =
			 * stockInfoList.get(i).getScreenContent(); } }
			 */
			final int userId = shareProductInfoObj.getUserId();

			if (null != shareProductInfoObj.getCouponId())
			{
				productInfo = rateReviewDao.getCouponShareThruEmail(shareProductInfoObj);
				subject = ApplicationConstants.SHARECOUPONBYEMAILSUBJECT;

			}
			else if (null != shareProductInfoObj.getLoyaltyId())
			{
				productInfo = rateReviewDao.getLoyaltyShareThruEmail(shareProductInfoObj);
				subject = ApplicationConstants.SHARELOYALTYBYEMAILSUBJECT;
			}
			else
			{
				productInfo = rateReviewDao.getRebateShareThruEmail(shareProductInfoObj);
				subject = ApplicationConstants.SHAREREBATEBYEMAILSUBJECT;
			}
			if (productInfo == null)
			{
				LOG.info("CLR details is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NORECORDSFOUNDTEXT);
				return response;
			}
			userEmailId = rateReviewDao.getUserInfo(shareProductInfoObj);
			if (userEmailId == null)
			{
				LOG.info("Validation failed To Email is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.USEREMAILIDNOTEXISTTOSHARE);
				return response;
			}
			toAddress = shareProductInfoObj.getToEmail();

			try
			{
				emailConf = shoppingListDao.getAppConfig(ApplicationConstants.EMAILCONFIG);

				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
					{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
					{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}

				msgBody = productInfo;
				fromAddress = userEmailId;
				final String delimiter = ";";
				toMailArray = toAddress.split(delimiter);
				if (null != smtpHost || null != smtpPort)
				{
					EmailComponent.multipleUsersmailingComponent(fromAddress, toMailArray, subject, msgBody, smtpHost, smtpPort);

				}

				LOG.info("Mail delivered to:" + toAddress);
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SHAREPRODUCTINFO);
			}
			catch (MessagingException exception)
			{
				LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
				response = Utility.formResponseXml(ApplicationConstants.INVALIDEMAILADDRESS, ApplicationConstants.INVALIDEMAILADDRESSTEXT);
			}

		}
		else
		{
			if (shareProductInfoObj == null)
			{
				response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. This Method fetches hot deal information.
	 * 
	 * @param xml
	 *            contains information to fetch hot deal
	 * @return String XML with coupon Details.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	@Override
	public String shareHotdealbyEmail(String xml) throws ScanSeeException
	{
		final String methodName = " shareHotdealbyEmail of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String productInfo = null;
		String userEmailId = null;
		String toAddress = null;
		String[] toMailArray = null;
		String fromAddress = null;
		String subject = null;
		String msgBody = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ShareProductInfo shareProductInfoObj = (ShareProductInfo) streamHelper.parseXmlToObject(xml);
		boolean valFlag = true;
		if (null != shareProductInfoObj)
		{
			if (null == shareProductInfoObj.getUserId())
			{
				LOG.info("Validation failed User Id is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
			}
			else if (null == shareProductInfoObj.getHotdealId())
			{
				LOG.info("Validation failed hotdeal  details is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.SHAREHOTDEALBYEMAILDETAILSNOTFOUND);
			}
			else if (null == shareProductInfoObj.getToEmail())
			{
				LOG.info("Validation failed To Email is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			}

			/*
			 * if (valFlag && null != shareProductInfoObj) { final int userId =
			 * shareProductInfoObj.getUserId(); stockInfoList =
			 * shoppingListDao.getAppConfig
			 * (ApplicationConstants.CONFIGURATIONTYPESHAREURL); for (int i = 0;
			 * i < stockInfoList.size(); i++) { if
			 * (stockInfoList.get(i).getScreenName
			 * ().equals(ApplicationConstants.EMAILSHAREURL)) { shareURL =
			 * stockInfoList.get(i).getScreenContent(); } }
			 */
			final int userId = shareProductInfoObj.getUserId();
			productInfo = rateReviewDao.getHotdealShareThruEmail(shareProductInfoObj);
			if (productInfo == null)
			{
				LOG.info("HotDeal  details is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
				return response;
			}
			userEmailId = rateReviewDao.getUserInfo(shareProductInfoObj);
			if (userEmailId == null)
			{
				LOG.info("Validation failed To Email is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.USEREMAILIDNOTEXISTTOSHARE);
				return response;
			}

			toAddress = shareProductInfoObj.getToEmail();

			try
			{
				emailConf = shoppingListDao.getAppConfig(ApplicationConstants.EMAILCONFIG);

				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
					{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
					{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}

				subject = ApplicationConstants.SHAREHOTDEALBYEMAILSUBJECT;
				msgBody = productInfo;
				fromAddress = userEmailId;
				final String delimiter = ";";
				toMailArray = toAddress.split(delimiter);
				if (null != smtpHost || null != smtpPort)
				{
					EmailComponent.multipleUsersmailingComponent(fromAddress, toMailArray, subject, msgBody, smtpHost, smtpPort);

				}

				LOG.info("Mail delivered to:" + toAddress);
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SHAREPRODUCTINFO);
			}
			catch (MessagingException exception)
			{
				LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
				response = Utility.formResponseXml(ApplicationConstants.INVALIDEMAILADDRESS, ApplicationConstants.INVALIDEMAILADDRESSTEXT);
			}

		}
		else
		{
			if (shareProductInfoObj == null)
			{
				response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. This Method fetches coupon ,loyalty and rebate
	 * details information.
	 * 
	 * @param xml
	 *            contains information to fetch coupon ,loyalty and rebate
	 *            details.
	 * @return String XML with coupon ,loyalty and rebate details.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	@Override
	public String shareCLRInfo(Integer userId, Integer couponId, Integer loyaltyId, Integer rebateId) throws ScanSeeException
	{
		final String methodName = "shareHotdealCouponInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		List<CouponDetails> couponDetaillst = null;
		List<LoyaltyDetail> loyaltyDetailslst = null;
		List<RebateDetail> rebateDetailslst = null;

		if (userId == null && couponId == null && loyaltyId == null && rebateId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else if (couponId != null)
		{
			couponDetaillst = rateReviewDao.getShareCouponInfo(couponId);
			if (!couponDetaillst.isEmpty() && couponDetaillst != null)
			{

				couponDetaillst.get(0).setTitleText(ApplicationConstants.SHAREPRODUCTTEXT + ApplicationConstants.SHAREPRODUCTEXTFROMSCANSEE);
				couponDetaillst.get(0).setTitleText2(ApplicationConstants.SHAREPRODUCTMEDIATITLETEXT);
				response = XstreamParserHelper.produceXMlFromObject(couponDetaillst);
				response = response.replaceAll("<list>", "<ShareCLRInfo>");
				response = response.replaceAll("</list>", "</ShareCLRInfo>");
			}
			else
			{
				LOG.info("No records found for the userid {}", couponId);
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}

		}
		else if (loyaltyId != null)
		{
			loyaltyDetailslst = rateReviewDao.getShareLoyaltyInfo(loyaltyId);
			if (!loyaltyDetailslst.isEmpty() && loyaltyDetailslst != null)
			{

				loyaltyDetailslst.get(0).setTitleText(ApplicationConstants.SHAREPRODUCTTEXT + ApplicationConstants.SHAREPRODUCTEXTFROMSCANSEE);
				loyaltyDetailslst.get(0).setTitleText2(ApplicationConstants.SHAREPRODUCTMEDIATITLETEXT);
				response = XstreamParserHelper.produceXMlFromObject(loyaltyDetailslst);
				response = response.replaceAll("<list>", "<ShareCLRInfo>");
				response = response.replaceAll("</list>", "</ShareCLRInfo>");
			}
			else
			{
				LOG.info("No records found for the userid {}", loyaltyId);
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		else
		{
			rebateDetailslst = rateReviewDao.getShareRebateInfo(rebateId);
			if (!rebateDetailslst.isEmpty() && rebateDetailslst != null)
			{

				rebateDetailslst.get(0).setTitleText(ApplicationConstants.SHAREPRODUCTTEXT + ApplicationConstants.SHAREPRODUCTEXTFROMSCANSEE);
				rebateDetailslst.get(0).setTitleText2(ApplicationConstants.SHAREPRODUCTMEDIATITLETEXT);
				response = XstreamParserHelper.produceXMlFromObject(rebateDetailslst);
				response = response.replaceAll("<list>", "<ShareCLRInfo>");
				response = response.replaceAll("</list>", "</ShareCLRInfo>");
			}
			else
			{
				LOG.info("No records found for the userid {}", rebateId);
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}

		return response;
	}

	/**
	 * This is a RestEasy WebService Method used to share retailer special offer
	 * information via Email.Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the special offer to be shared and
	 *            Recipients.
	 * @return XML success or failure as response.
	 */
	@Override
	public String shareSpecialOffByEmail(String xml) throws ScanSeeException
	{
		final String methodName = " shareSpecialOffByEmail of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String productInfo = null;
		String userEmailId = null;
		String toAddress = null;
		String[] toMailArray = null;
		String fromAddress = null;
		String subject = null;
		String msgBody = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		// final RetailerDetail retailerDetailObj = (RetailerDetail)
		// streamHelper.parseXmlToObject(xml);
		final ShareProductInfo retailerDetailObj = (ShareProductInfo) streamHelper.parseXmlToObject(xml);
		boolean valFlag = true;
		if (null != retailerDetailObj)
		{
			if (null == retailerDetailObj.getUserId())
			{
				LOG.info("Validation failed User Id is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
			}
			else if (null == retailerDetailObj.getPageId())
			{
				LOG.info("Validation failed hotdeal  details is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.SHAREHOTDEALBYEMAILDETAILSNOTFOUND);
			}
			else if (null == retailerDetailObj.getToEmail())
			{
				LOG.info("Validation failed To Email is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			}
			else if (null == retailerDetailObj.getRetailerId())
			{
				LOG.info("Validation failed due to Retailer id  is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			}

			final Integer userId = retailerDetailObj.getUserId();

			productInfo = rateReviewDao.getSpecialOffShareThruEmail(retailerDetailObj);
			if (productInfo == null)
			{
				LOG.info("HotDeal  details is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
				return response;
			}
			userEmailId = rateReviewDao.fetchUserInfo(userId);
			if (userEmailId == null)
			{
				LOG.info("Validation failed To Email is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.USEREMAILIDNOTEXISTTOSHARE);
				return response;
			}

			toAddress = retailerDetailObj.getToEmail();

			try
			{
				emailConf = shoppingListDao.getAppConfig(ApplicationConstants.EMAILCONFIG);

				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
					{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
					{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}

				subject = ApplicationConstants.SHARESPECIALOFFBYEMAILSUBJECT;
				msgBody = productInfo;
				fromAddress = userEmailId;
				final String delimiter = ";";
				toMailArray = toAddress.split(delimiter);
				if (null != smtpHost || null != smtpPort)
				{
					EmailComponent.multipleUsersmailingComponent(fromAddress, toMailArray, subject, msgBody, smtpHost, smtpPort);

				}

				LOG.info("Mail delivered to:" + toAddress);
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SHAREPRODUCTINFO);
			}
			catch (MessagingException exception)
			{
				LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
				response = Utility.formResponseXml(ApplicationConstants.INVALIDEMAILADDRESS, ApplicationConstants.INVALIDEMAILADDRESSTEXT);
			}

		}
		else
		{
			if (retailerDetailObj == null)
			{
				response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	
	
	/**
	 * This is a RestEasy WebService Method used when the user clicks Share, 
	 * they will be prompted to share via different options Text/Twitter/Facebook  will send wording,
	 * retailer name, and link to the AppSite� information.
	 * 
	 * @param xml contains shareType, userId, retailerId, retailerLocationId to be shared.
	 * @return XML success or failure as response.
	 */
	@Override
	public String shareAppsite(String xml) throws ScanSeeException
	{
		final String methodName = " shareAppsite";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		List<RetailersDetails> arRetailerList = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		// final RetailerDetail retailerDetailObj = (RetailerDetail)
		// streamHelper.parseXmlToObject(xml);
		final ShareProductInfo objRetailerDetail = (ShareProductInfo) streamHelper.parseXmlToObject(xml);
		boolean valFlag = true;
		if (null != objRetailerDetail)
		{
			if (null == objRetailerDetail.getUserId()) {
				LOG.info("Validation failed User Id is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
			} else if (null == objRetailerDetail.getRetailerId()) {
				LOG.info("Validation failed due to Retailer id is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			} else if (null == objRetailerDetail.getRetailerLocationId()) {
				LOG.info("Validation failed Retailer Location Id details is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.SHAREHOTDEALBYEMAILDETAILSNOTFOUND);
			} else if (null == objRetailerDetail.getShareType()) {
				LOG.info("Validation failed to share type details is not available");
				valFlag = false;
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			}
			
			if (valFlag == true) {
				arRetailerList = rateReviewDao.getShareAppsite(objRetailerDetail);
				if (!arRetailerList.isEmpty() && arRetailerList != null) {
					response = XstreamParserHelper.produceXMlFromObject(arRetailerList);
					response = response.replaceAll("<list>", "");
					response = response.replaceAll("</list>", "");
				} else {
					LOG.info("No records found.");
					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
				}
			}
		} else {
			if (objRetailerDetail == null)
			{
				response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method validates the input parameters and returns validation error if validation fails.
	 * Shares app site information via email. It also parse input xml to object using XStream parser.
	 * 
	 * @param xml
	 * 			contains recipient email ID, share type, user ID, retailer ID and retailer location ID.
	 * 
	 * @return String XML with success or failure.
	 * 
	 * @throws ScanSeeException
	 * 				 throws if exception occurs.
	 */
	@Override
	public String shareAppSiteByEmail(String xml) throws ScanSeeException {
		final String methodName = "shareAppSiteByEmail";
		LOG.info("Method name: " + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		ShareProductInfo objShareProductInfo = (ShareProductInfo) parser.parseXmlToObject(xml);
		
		String userEmailId = null;
		String toMail = null;
		String[] toMailArray = null;
		String subject = null;
		String msgBody = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;
		
		if (objShareProductInfo.getUserId() == null || "".equals(objShareProductInfo.getUserId())) {
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		} else if (objShareProductInfo.getToEmail() == null || "".equals(objShareProductInfo.getToEmail())) {
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOEMAILID);
		} else if (objShareProductInfo.getShareType() == null || "".equals(objShareProductInfo.getShareType())) {
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		} else {
			
			userEmailId = rateReviewDao.fetchUserInfo(objShareProductInfo.getUserId());
			if(userEmailId == null || userEmailId.trim().equals(""))	{
				LOG.info("Validation failed To Email is not available");
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.USEREMAILIDNOTEXISTTOSHARE);
				return response;
			}
			msgBody = rateReviewDao.shareAppSiteByEmail(objShareProductInfo);
			
			if (msgBody == null) {
				return response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			} 
			
//			userEmailId = rateReviewDao.fetchUserInfo(objShareProductInfo.getUserId());
			toMail = objShareProductInfo.getToEmail();
			final String delimiter = ";";
			toMailArray = toMail.split(delimiter);
			try
			{
				emailConf = shoppingListDao.getAppConfig(ApplicationConstants.EMAILCONFIG);
				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))	{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))	{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}
				subject = ApplicationConstants.SHARE_APPSITE_SUBJECT;

				if (null != smtpHost || null != smtpPort)	{
					//EmailComponent.mailingComponent(userEmailId, toMail, subject, msgBody, smtpHost, smtpPort);
					EmailComponent.multipleUsersmailingComponent(userEmailId, toMailArray, subject, msgBody, smtpHost, smtpPort);
				}
				LOG.info("Mail delivered to:" + toMailArray);
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SHAREPRODUCTINFO);
			}
			catch (MessagingException exception)
			{
				LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
				response = Utility.formResponseXml(ApplicationConstants.INVALIDEMAILADDRESS, ApplicationConstants.INVALIDEMAILADDRESSTEXT);
			}
		}
		return response;
	}

	/**
	 * Method validates input xml and convert xml to object using xstream parser.
	 * 
	 * @param xml
	 * @return SUCCESS or FAILURE string
	 * @throws ScanSeeException
	 */
	@Override
	public String userTrackingShareTypeUpdate(String xml) throws ScanSeeException {
		final String methodName = "userTrackingShareTypeUpdate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final UserTrackingData objUserTrackingData = (UserTrackingData) parser.parseXmlToObject(xml);
		List<UserTrackingData> shareTypesList = null;
		if (objUserTrackingData.getMainMenuID() == null || objUserTrackingData.getShrTypNam() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			Integer shrTypID = null;
			shareTypesList = new ArrayList<UserTrackingData>();
			shareTypesList = rateReviewDao.getShareTypes();
			
			for(int i = 0; i < shareTypesList.size(); i++)	{
				if(objUserTrackingData.getShrTypNam().equalsIgnoreCase(shareTypesList.get(i).getShrTypNam()))	{
					shrTypID = shareTypesList.get(i).getShrTypID();
				}
			}
			objUserTrackingData.setShrTypID(shrTypID);
			
			response = rateReviewDao.updateShareType(objUserTrackingData);
			
			if(!ApplicationConstants.SUCCESSRESPONSETEXT.equalsIgnoreCase(response))	{
				response = ApplicationConstants.FAILURE;
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}
