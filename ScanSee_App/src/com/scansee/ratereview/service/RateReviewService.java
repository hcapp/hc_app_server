package com.scansee.ratereview.service;

import com.scansee.common.exception.ScanSeeException;

/**
 * This interface for Rate Review methods, which are implemented by Rate Review
 * implementation class.
 * 
 * @author shyamsundara_hm
 */
public interface RateReviewService
{
	/**
	 * This method for fetching user product rating information.
	 * 
	 * @param userId
	 *            as the request parameter.
	 * @param productId
	 *            as the request parameter.
	 * @return xml containing user rating,average rating information.
	 * @throws ScanSeeException
	 *             for exception
	 */

	String fecthUserProductRating(Integer userId, Integer productId) throws ScanSeeException;

	/**
	 * This method for Saving user product rating information.
	 * 
	 * @param xml
	 *            as the request parameter.It contains userId,productId and
	 *            rating information.
	 * @return xml containing success or failure.
	 * @throws ScanSeeException
	 *             for exception.
	 */

	String saveUserProductRating(String xml) throws ScanSeeException;

	/**
	 * Sharing product rate,description with users.
	 * 
	 * @param userId
	 *            as the request parameter
	 * @param productId
	 *            as the request parameter
	 * @return xml containing product description,url and price information.
	 * @throws ScanSeeException
	 *             for any exception.
	 */
	String shareProductReview(Integer userId, Integer productId) throws ScanSeeException;

	/**
	 * The Service Method for fetching product reviews and user ratings.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return XML String Containing Product Reviews and User Ratings.
	 * @throws ScanSeeException
	 *             for exception
	 */
	String getProductReviews(Integer userId, Integer productId) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. This Method fetches product and retailer
	 * information.
	 * 
	 * @param xml
	 *            contains information to fetch product and retailer details.
	 * @return String XML with Product and Retailer Details.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String shareProductRetailerInfo(String xml) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Sends email to recipients with product information.
	 * 
	 * @param xml
	 *            contains email id, product to be shared.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String shareProductInfo(String xml) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. This Method fetches hot deal information.
	 * 
	 * @param xml
	 *            contains information to fetch hot deal details.
	 * @return String XML with hot deal Details.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String shareHotdealInfo(Integer userId, Integer hotdealId) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Sends email to recipients with coupon information.
	 * 
	 * @param xml
	 *            contains email id, product to be shared.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String shareCLRbyEmail(String xml) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Sends email to recipients with hot deal information.
	 * 
	 * @param xml
	 *            contains email id, hot deal to be shared.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String shareHotdealbyEmail(String xml) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. This Method fetches coupon,loyalty and rebate
	 * information.
	 * 
	 * @param xml
	 *            contains information to fetch hot deal details.
	 * @return String XML with hot deal Details.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String shareCLRInfo(Integer userId, Integer couponId, Integer loyaltyId, Integer rebateId) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Sends email to recipients with retailer special
	 * offer information.
	 * 
	 * @param xml
	 *            contains email id,special offers to be shared.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String shareSpecialOffByEmail(String xml) throws ScanSeeException;

	
	/**
	 * This method validates the input parameters and returns validation error if validation fails.
	 * Shares app site information via email. It also parse input xml to object using XStream parser.
	 * 
	 * @param xml
	 * 			contains recipient email ID, share type, user ID, retailer ID and retailer location ID.
	 * 
	 * @return String XML with success or failure.
	 * 
	 * @throws ScanSeeException
	 * 				 throws if exception occurs.
	 */
	String shareAppSiteByEmail(String xml) throws ScanSeeException;

	
	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. This method shareType, userId, retailerId, retailerLocationId information.
	 * 
	 * @param xml contains qrUrl,retailerAddress,retImage,shareText to be shared.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException throws if exception occurs.
	 */
	String shareAppsite(String xml) throws ScanSeeException;

	/**
	 * Method validates input xml and convert xml to object using xstream parser.
	 * 
	 * @param xml
	 * @return SUCCESS or FAILURE string
	 * @throws ScanSeeException
	 */
	String userTrackingShareTypeUpdate(String xml) throws ScanSeeException;
	


}
