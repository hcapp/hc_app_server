package com.scansee.ratereview.dao;

import java.util.ArrayList;
import java.util.List;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.LoyaltyDetail;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductReview;
import com.scansee.common.pojos.RebateDetail;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.ShareProductInfo;
import com.scansee.common.pojos.UserRatingInfo;
import com.scansee.common.pojos.UserTrackingData;

/**
 * This interface is used for RateReview module.It has methods which are
 * implemented by RateReviewDAOImpl.
 * 
 * @author shyamsundara_hm
 */
public interface RateReviewDAO
{

	/**
	 * This method fetches user product rating information.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param productId
	 *            as request parameter.
	 * @return xml containing user,average product ratings.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	UserRatingInfo fecthUserProductRating(Integer userId, Integer productId) throws ScanSeeException;

	/**
	 * This method for saving user product rating information.
	 * 
	 * @param userRatingInfo
	 *            as the request parameter.
	 * @return xml containing success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String saveUserProductRating(UserRatingInfo userRatingInfo) throws ScanSeeException;

	/**
	 * The method fetches the product Details.
	 * 
	 * @param userId
	 *            as the request parameter.
	 * @param productId
	 *            as the request parameter.
	 * @return UserRatingInfo based on success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	UserRatingInfo getShareProductInfo(Integer userId, Integer productId) throws ScanSeeException;

	/**
	 * The DAO method for fetching product reviews.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return ProductReview list.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ArrayList<ProductReview> getProductReviews(Integer userId, Integer productId) throws ScanSeeException;

	/**
	 * The DAO method for fetching product information.
	 * 
	 * @param shareProductInfo
	 *            as parameter containing userId and productId.
	 * @return ProductDetail object.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetail getProductInfo(UserRatingInfo shareProductInfo) throws ScanSeeException;

	/**
	 * The DAO method for fetching Retailer information.
	 * 
	 * @param retailerId
	 *            as parameter.
	 * @param retailerLocationId
	 *            as parameter
	 * @return RetailerDetail object.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	RetailerDetail getRetailerInfo(Integer retailerId, Integer retailerLocationId) throws ScanSeeException;

	/**
	 * The DAo method for fetching product information.
	 * 
	 * @param shareProductInfo
	 *            as request parameter
	 * @param retailerDetail
	 *            as request parameter
	 * @param userId
	 *            as request parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getProductInfo(ShareProductInfo shareProductInfo, RetailerDetail retailerDetail, int userId, String shareURL) throws ScanSeeException;

	/**
	 * The DAO method for fetching user information for sending mail to share.
	 * product information
	 * 
	 * @param shareProductInfo
	 *            as paraemeter user
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String getUserInfo(ShareProductInfo shareProductInfo) throws ScanSeeException;

	/**
	 * The DAO method for fetching Retailer information.
	 * 
	 * @param retailerId
	 *            as parameter.
	 * @param retailerLocationId
	 *            as parameter
	 * @param productId
	 *            as parameter
	 * @return RetailerDetail object.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	RetailerDetail getRetailerInfoWithSalePrice(Integer retailerId, Integer retailerLocationId, Integer productId) throws ScanSeeException;

	/**
	 * The DAO method for fetching share coupon information
	 * 
	 * @param couponId
	 *            as parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<CouponDetails> getShareCouponInfo(Integer couponId) throws ScanSeeException;

	/**
	 * The DAO method for fetching share hot deal information
	 * 
	 * @param hotdealId
	 *            as parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<HotDealsDetails> getShareHotdealInfo(Integer hotdealId) throws ScanSeeException;

	/**
	 * The DAo method for fetching product information.
	 * 
	 * @param shareProductInfo
	 *            as request parameter
	 * @param retailerDetail
	 *            as request parameter
	 * @param userId
	 *            as request parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getCouponShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeException;

	/**
	 * The DAo method for fetching product information.
	 * 
	 * @param shareProductInfo
	 *            as request parameter
	 * @param retailerDetail
	 *            as request parameter
	 * @param userId
	 *            as request parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getHotdealShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeException;

	/**
	 * The DAO method for fetching share loyalty information
	 * 
	 * @param loyaltyId
	 *            as parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<LoyaltyDetail> getShareLoyaltyInfo(Integer loyaltyId) throws ScanSeeException;

	/**
	 * The DAO method for fetching share rebate information
	 * 
	 * @param loyaltyId
	 *            as parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	List<RebateDetail> getShareRebateInfo(Integer rebateId) throws ScanSeeException;

	/**
	 * The DAo method for fetching product information.
	 * 
	 * @param shareProductInfo
	 *            as request parameter
	 * @param retailerDetail
	 *            as request parameter
	 * @param userId
	 *            as request parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getLoyaltyShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeException;

	/**
	 * The DAo method for fetching product information.
	 * 
	 * @param shareProductInfo
	 *            as request parameter
	 * @param retailerDetail
	 *            as request parameter
	 * @param userId
	 *            as request parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getRebateShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeException;

	/**
	 * The DAo method for fetching special offer information.
	 * 
	 * @param retailerDetailObj
	 *            as request parameter contains retailer page id,retailer id
	 *            ,location id and user id
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getSpecialOffShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeException;
	

	/**
	 * The DAO method for fetching user information for sending mail to share.
	 * product information
	 * 
	 * @param shareProductInfo
	 *            as paraemeter user
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	 String fetchUserInfo(Integer userId) throws ScanSeeException;

	 
	 /**
	  * The DAO method for adding share app site by email details to database.
	  * 
	  * @param objShareProductInfo
	  * 
	  * @return String message.
	  * 
	  * @throws ScanSeeException
	  */
	 String shareAppSiteByEmail(ShareProductInfo objShareProductInfo) throws ScanSeeException;

	 
		/**
		 * The DAO method  when the user clicks Share, they will be prompted to share via different options 
		 * Text/Twitter/Facebook  will send wording, retailer name, and link to the AppSite� information.
		 * 
		 * @param retailerDetailObj
		 *            as request parameter contains retailer page id,retailer id
		 *            ,location id and user id
		 * @return The XML as the response.
		 * @throws ScanSeeException
		 *             The exceptions are caught and a ScanSee Exception defined for
		 *             the application is thrown which is caught in the Controller
		 *             layer.
		 */
	 List<RetailersDetails> getShareAppsite(ShareProductInfo objShareProduct) throws ScanSeeException;
	 
	 /**
	  * The DAO method to get share types with its ID and name from database.
	  * 
	  * @return list containing shareTypeID and shareTypeName
	  * 
	  * @throws ScanSeeException
	  */
	 List<UserTrackingData> getShareTypes() throws ScanSeeException;
	 
	 /**
	  * The DAO method to update share type details to the database 
	  * @param objUserTrackingData
	  * @return SUCCESS or FAILURE string
	  * @throws ScanSeeException
	  */
	 String updateShareType(UserTrackingData objUserTrackingData) throws ScanSeeException;
}
