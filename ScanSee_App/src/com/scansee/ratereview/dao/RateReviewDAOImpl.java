package com.scansee.ratereview.dao;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AppConfiguration;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.LoyaltyDetail;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductReview;
import com.scansee.common.pojos.RebateDetail;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.ShareProductInfo;
import com.scansee.common.pojos.UserRatingInfo;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.common.util.EncryptDecryptPwd;
import com.scansee.common.util.Utility;
import com.scansee.ratereview.query.RateReviewQueries;
import com.scansee.shoppinglist.query.ShoppingListQueries;

/**
 * This is implementation class for RateReviewDAO. This class has methods for
 * RateReview Module. The methods of this class are called from the RateReview
 * Service layer.
 * 
 * @author shyamsundara_hm
 */
public class RateReviewDAOImpl implements RateReviewDAO
{

	/**
	 * Getting the Logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(RateReviewDAOImpl.class);
	/**
	 * To set ParameterizedBeanPropertyRowMapper to map POJO.
	 */

	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * for Jdbc connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * This method for to get data source from xml.
	 * 
	 * @param dataSource
	 *            as input parameter.
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * This method fetches User product ratings.
	 * 
	 * @param userId
	 *            as query parameter
	 * @param productId
	 *            as query parameter
	 * @return UserRatingInfo object
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("unchecked")
	public UserRatingInfo fecthUserProductRating(Integer userId, Integer productId) throws ScanSeeException
	{
		final String methodName = "fecthUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		UserRatingInfo userRatingInfo = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetUserRatings");
			simpleJdbcCall.returningResultSet("userRatingInfo", new BeanPropertyRowMapper<UserRatingInfo>(UserRatingInfo.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("prProductID", productId);
			ratingParameters.addValue("prUserID", userId);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final List<UserRatingInfo> userRatingInfoList = (List<UserRatingInfo>) resultFromProcedure.get("userRatingInfo");
					if (null != userRatingInfoList && !userRatingInfoList.isEmpty())
					{
						userRatingInfo = userRatingInfoList.get(0);

						final String userRating = userRatingInfo.getCurrentRating();
						final String avgRating = userRatingInfo.getAvgRating();
						final String noOfUserrated = userRatingInfo.getNoOfUsersRated();
						if (null == userRating)
						{
							userRatingInfo.setCurrentRating("0");
						}
						if (null == avgRating)
						{
							userRatingInfo.setAvgRating("0");
						}
						if (null == noOfUserrated)
						{
							userRatingInfo.setNoOfUsersRated("0");
						}

					}
					else
					{
						userRatingInfo = new UserRatingInfo();
						userRatingInfo.setCurrentRating("0");
						userRatingInfo.setAvgRating("0");
						userRatingInfo.setNoOfUsersRated("0");
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

					LOG.error("Error occurred in usp_GetUserRatings Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return userRatingInfo;
	}

	/**
	 * This method fetches User product ratings.
	 * 
	 * @param userRatingInfo
	 *            as request parameter containing userId,produtId and rating
	 *            information
	 * @return xml containing success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String saveUserProductRating(UserRatingInfo userRatingInfo) throws ScanSeeException
	{
		final String methodName = "saveUserProductRating";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_SaveUserRatings");
			simpleJdbcCall.returningResultSet("userRatingInfo", new BeanPropertyRowMapper<UserRatingInfo>(UserRatingInfo.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("prProductID", userRatingInfo.getProductId());
			ratingParameters.addValue("prUserID", userRatingInfo.getUserId());
			ratingParameters.addValue("prRating", userRatingInfo.getCurrentRating());
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					response = ApplicationConstants.SUCCESS;
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

					LOG.error("Error occurred in usp_SaveUserRatings Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method fetches share product review comments information.
	 * 
	 * @param userId
	 *            as request parameter
	 * @param productId
	 *            as request parameter
	 * @return UserRatingInfo containing product description,url and price.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public UserRatingInfo getShareProductInfo(Integer userId, Integer productId) throws ScanSeeException
	{
		final String methodName = "getShareProductInfo in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<UserRatingInfo> userRatingInfolst = null;
		UserRatingInfo userRatingInfoObj = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			userRatingInfolst = simpleJdbcTemplate.query(RateReviewQueries.GETSHAREPRODUCTINFO, new BeanPropertyRowMapper<UserRatingInfo>(
					UserRatingInfo.class), productId);
			if (userRatingInfolst != null && !userRatingInfolst.isEmpty())
			{
				userRatingInfoObj = new UserRatingInfo();
				userRatingInfoObj = userRatingInfolst.get(0);
				userRatingInfoObj.setProdStaticText("I found this for you and thought you might be interested :");
				userRatingInfoObj.setScanseeURL("www.ScanSee.com");
			}

		}

		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return userRatingInfoObj;
	}

	/**
	 * The DAO method for fetching product reviews.
	 * 
	 * @param userId
	 *            as a request parameter
	 * @param productId
	 *            as a request parameter
	 * @return ProductReview list.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public ArrayList<ProductReview> getProductReviews(Integer userId, Integer productId) throws ScanSeeException
	{
		final String methodName = "getProductReviews in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductReview> productReviewslist = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			productReviewslist = (ArrayList<ProductReview>) simpleJdbcTemplate.query(RateReviewQueries.GETPRODUCTREVIEWS,
					new BeanPropertyRowMapper<ProductReview>(ProductReview.class), productId);

		}

		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productReviewslist;
	}

	/**
	 * The method for fetching product information.
	 * 
	 * @param shareProductInfo
	 *            as paraemeter user
	 * @return ProductDetail object.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public ProductDetail getProductInfo(UserRatingInfo shareProductInfo) throws ScanSeeException
	{
		final String methodName = "getProductInfo in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail productDetail = null;
		String productKey = null;
		String shareURL = null;
		ArrayList<AppConfiguration> stockInfoList = null;
		final String productId = shareProductInfo.getProductId();
		final String decryptedUserId = shareProductInfo.getUserId();
		List <ProductDetail> arProdDetailList  = null;
		Map<String, Object> resultFromProcedure = null;
		stockInfoList = getAppConfig(ApplicationConstants.CONFIGURATIONTYPESHAREURL);
		for (int i = 0; i < stockInfoList.size(); i++)
		{
			if (stockInfoList.get(i).getScreenName().equals(ApplicationConstants.EMAILSHAREURL))
			{
				shareURL = stockInfoList.get(i).getScreenContent();
			}
		}
		// "http://122.181.128.148:8080/ScanSee-Email/product/showContent.do?productKey="
		// + productId
		final String key = decryptedUserId + "/" + productId;
		EncryptDecryptPwd enryptDecryptpwd = null;
		// stockInfoList =
		// shoppingListDao.getAppConfig(ApplicationConstants.CONFIGURATIONTYPESHAREURL);
		// http://122.181.128.148:8080/ScanSee-Email/product/showContent.do?productKey="
		// + productId;
		try
		{
			enryptDecryptpwd = new EncryptDecryptPwd();
			productKey = enryptDecryptpwd.encrypt(key);
			shareURL = shareURL + "productKey=" + productKey;
			
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_productshare");
			simpleJdbcCall.returningResultSet("getProductInfo", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
			final MapSqlParameterSource objProdInfoParam = new MapSqlParameterSource();
			objProdInfoParam.addValue("ProductID", shareProductInfo.getProductId());
			resultFromProcedure = simpleJdbcCall.execute(objProdInfoParam);
			arProdDetailList = (List<ProductDetail>)resultFromProcedure.get("getProductInfo");
			if (null != resultFromProcedure) {
				productDetail = new ProductDetail();
				if (null == resultFromProcedure.get("ErrorNumber")) {
					if (null != arProdDetailList && !arProdDetailList.isEmpty()) {
						productDetail = (ProductDetail)arProdDetailList.get(0);
						productDetail.setEmailTemplateURL(shareURL);
					} 
				} else {
						final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
						final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
						LOG.error("Error occurred in usp_productshare Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
						throw new ScanSeeException(errorMsg);
					}
				}
		}
		catch (InvalidKeyException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		catch (BadPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetail;
	}

	/**
	 * The method for fetching Retailer information.
	 * 
	 * @param retailerId
	 *            as parameter.
	 * @param retailerLocationId
	 *            as parameter
	 * @return RetailerDetail object.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public RetailerDetail getRetailerInfo(Integer retailerId, Integer retailerLocationId) throws ScanSeeException
	{
		final String methodName = "getRetailerInfo in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailerDetail retailerDetail = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			retailerDetail = simpleJdbcTemplate.queryForObject(ShoppingListQueries.FETCHRETAILERINFO, new BeanPropertyRowMapper<RetailerDetail>(
					RetailerDetail.class), retailerId, retailerLocationId);

		}

		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerDetail;
	}

	/**
	 * This method for fetching retailer information with sale price.
	 * 
	 * @param retailerId
	 *            - as request parameter
	 * @param retailerLocationId
	 *            - as request parameter
	 * @param productId
	 *            - as request parameter
	 * @return xml containing retailer details with sale price
	 * @throws ScanSeeException
	 *             for any exception
	 */
	@Override
	public RetailerDetail getRetailerInfoWithSalePrice(Integer retailerId, Integer retailerLocationId, Integer productId) throws ScanSeeException
	{
		final String methodName = "getRetailerInfoWithSalePrice in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		RetailerDetail retailerDetail = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			retailerDetail = simpleJdbcTemplate.queryForObject(ShoppingListQueries.FETCHRETAILERINFOWITHRETAILERINFO,
					new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class), retailerId, retailerLocationId, productId);

		}
		catch (EmptyResultDataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			retailerDetail = null;
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return retailerDetail;
	}

	/**
	 * The method for fetching product information.
	 * 
	 * @param shareProductInfo
	 *            as parameter
	 * @param retailerDetail
	 *            as request parameter.
	 * @param userId
	 *            as request parameter.
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getProductInfo(ShareProductInfo shareProductInfo, RetailerDetail retailerDetail, int userId, String shareURL)
			throws ScanSeeException
	{

		final String methodName = "getProductInfo in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetaillst = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ShareProductDetails");
			simpleJdbcCall.returningResultSet("productDetails", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();

			fetchProductDetailsParameters.addValue("ProductID", shareProductInfo.getProductId());

			fetchProductDetailsParameters.addValue("ErrorNumber", null);
			fetchProductDetailsParameters.addValue("ErrorMessage", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetaillst = (List<ProductDetail>) resultFromProcedure.get("productDetails");

					if (productDetaillst != null && !productDetaillst.isEmpty())
					{

						response = Utility.formProductInfoHTML(productDetaillst, retailerDetail, userId, shareURL);
					}
				}
			}
		}
		catch (InvalidKeyException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (BadPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method for fetching user information for sending mail to share
	 * product information.
	 * 
	 * @param shareProductInfo
	 *            as request parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getUserInfo(ShareProductInfo shareProductInfo) throws ScanSeeException
	{
		final String methodName = "getUserInfo in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			response = this.jdbcTemplate.queryForObject(ShoppingListQueries.FETCHUSEREMAILID, new Object[] { shareProductInfo.getUserId() },
					String.class);

		}

		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method fetches share coupon details.
	 * 
	 * @param couponId
	 *            as request parameter
	 * @return xml containing coupon name and URL.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CouponDetails> getShareCouponInfo(Integer couponId) throws ScanSeeException
	{
		final String methodName = "getShareCouponInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> couponDetailslst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_CouponShare");
			simpleJdbcCall.returningResultSet("shareCouponInfo", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("CouponID", couponId);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					couponDetailslst = (List<CouponDetails>) resultFromProcedure.get("shareCouponInfo");

					/*
					 * if(!couponDetailslst.isEmpty() && couponDetailslst!=null)
					 * { if(couponDetailslst.get(0).getCouponImagePath() ==null)
					 * {
					 * couponDetailslst.get(0).setCouponImagePath(couponImagePath
					 * ) } }
					 */

					/*
					 * if (couponDetailslst != null &&
					 * !couponDetailslst.isEmpty()) { final Boolean
					 * couponExpired = (Boolean)
					 * resultFromProcedure.get("CouponExpired"); if
					 * (couponExpired) {
					 * couponDetailslst.get(0).setCouponExpired(1); } else {
					 * couponDetailslst.get(0).setCouponExpired(0); } }
					 */
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_CouponShare Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return couponDetailslst;
	}

	/**
	 * This method fetches share hot deal details.
	 * 
	 * @param hotdealId
	 *            as request parameter
	 * @return xml containing hot deal name and URL.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<HotDealsDetails> getShareHotdealInfo(Integer hotdealId) throws ScanSeeException
	{
		final String methodName = "getShareHotdealInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsDetails> hotDealsDetailsLst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_HotDealShare");
			simpleJdbcCall.returningResultSet("shareHotdealInfo", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("HotDealID", hotdealId);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					hotDealsDetailsLst = (List<HotDealsDetails>) resultFromProcedure.get("shareHotdealInfo");
					/*
					 * if (hotDealsDetailsLst != null &&
					 * !hotDealsDetailsLst.isEmpty()) { final Boolean
					 * dealExpired = (Boolean)
					 * resultFromProcedure.get("HotDealExpired"); if
					 * (dealExpired) {
					 * hotDealsDetailsLst.get(0).setHotDealExpired(1); } else {
					 * hotDealsDetailsLst.get(0).setHotDealExpired(0); } }
					 */
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_HotDealShare Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsDetailsLst;
	}

	/**
	 * This method fetches share coupon details through mail.
	 * 
	 * @param hotdealId
	 *            as request parameter
	 * @return xml containing hot deal name and URL.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String getCouponShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeException
	{
		final String methodName = "getShareCouponInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> couponDetailslst = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_CouponShare");
			simpleJdbcCall.returningResultSet("shareCouponInfo", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("CouponID", shareProductInfo.getCouponId());
			// ratingParameters.addValue("CLR", shareProductInfo.getClrFlag());
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					couponDetailslst = (List<CouponDetails>) resultFromProcedure.get("shareCouponInfo");
					if (!couponDetailslst.isEmpty() && couponDetailslst != null)
					{

						response = Utility.formCouponInfoHTML(couponDetailslst);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_HotDealShare Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (InvalidKeyException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (BadPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method fetches share hot deal details through mail.
	 * 
	 * @param hotdealId
	 *            as request parameter
	 * @return xml containing hot deal name and URL.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getHotdealShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeException
	{
		final String methodName = "getHotdealShareThruEmail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsDetails> hotDealsDetailsLst = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_HotDealShare");
			simpleJdbcCall.returningResultSet("shareHotdealInfo", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("HotDealID", shareProductInfo.getHotdealId());
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					hotDealsDetailsLst = (List<HotDealsDetails>) resultFromProcedure.get("shareHotdealInfo");
					if (hotDealsDetailsLst != null && !hotDealsDetailsLst.isEmpty())
					{
						response = Utility.formHotdealInfoHTML(hotDealsDetailsLst);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_HotDealShare Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (InvalidKeyException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (BadPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method fetches loyalty details to share
	 * 
	 * @param loyaltyId
	 *            as request parameter
	 * @return xml containing loyalty name and details.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<LoyaltyDetail> getShareLoyaltyInfo(Integer loyaltyId) throws ScanSeeException
	{
		final String methodName = "getShareLoyaltyInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<LoyaltyDetail> loyaltyDetaillst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_LoyaltyShare");
			simpleJdbcCall.returningResultSet("shareLoyaltyInfo", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("LoyaltyID", loyaltyId);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					loyaltyDetaillst = (List<LoyaltyDetail>) resultFromProcedure.get("shareLoyaltyInfo");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_LoyaltyShare Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return loyaltyDetaillst;
	}

	/**
	 * This method fetches Rebate details to share
	 * 
	 * @param rebateId
	 *            as request parameter
	 * @return xml containing rebateId name and details.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RebateDetail> getShareRebateInfo(Integer rebateId) throws ScanSeeException
	{
		final String methodName = "getShareRebateInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RebateDetail> rebateDetaillst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_RebateShare");
			simpleJdbcCall.returningResultSet("shareRebateInfo", new BeanPropertyRowMapper<RebateDetail>(RebateDetail.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("rebateId", rebateId);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					rebateDetaillst = (List<RebateDetail>) resultFromProcedure.get("shareRebateInfo");

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_RebateShare Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return rebateDetaillst;
	}

	/**
	 * This method fetches share coupon details through mail.
	 * 
	 * @param hotdealId
	 *            as request parameter
	 * @return xml containing hot deal name and URL.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String getRebateShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeException
	{
		final String methodName = "getShareRebateInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RebateDetail> rebateDetaillst = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_RebateShare");
			simpleJdbcCall.returningResultSet("shareRebateInfo", new BeanPropertyRowMapper<RebateDetail>(RebateDetail.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("rebateId", shareProductInfo.getRebateId());
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					rebateDetaillst = (List<RebateDetail>) resultFromProcedure.get("shareRebateInfo");
					if (!rebateDetaillst.isEmpty() && rebateDetaillst != null)
					{
						response = Utility.formRebateInfoHTML(rebateDetaillst);
					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_HotDealShare Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}

		catch (InvalidKeyException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (BadPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method fetches share coupon details through mail.
	 * 
	 * @param hotdealId
	 *            as request parameter
	 * @return xml containing hot deal name and URL.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String getLoyaltyShareThruEmail(ShareProductInfo shareProductInfo) throws ScanSeeException
	{
		final String methodName = "getShareLoyaltyInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<LoyaltyDetail> loyaltyDetaillst = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_LoyaltyShare");
			simpleJdbcCall.returningResultSet("shareLoyaltyInfo", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("LoyaltyID", shareProductInfo.getLoyaltyId());
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					loyaltyDetaillst = (List<LoyaltyDetail>) resultFromProcedure.get("shareLoyaltyInfo");
					if (!loyaltyDetaillst.isEmpty() && loyaltyDetaillst != null)
					{
						response = Utility.formLoyaltyInfoHTML(loyaltyDetaillst);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_LoyaltyShare Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}

		}
		catch (InvalidKeyException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (BadPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method Fetching App Configuration details.
	 * 
	 * @return ArrayList<AppConfiguration> .
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeException
	{

		final String methodName = "getStockInfo";

		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent");
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));

			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_GetScreenContent Store Procedure with error number: {} " + errorNum + " and error message: {}"
						+ errorMsg);
				throw new ScanSeeException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e);
		}
		return appConfigurationList;
	}

	/**
	 * This method fetches share hot deal details through mail.
	 * 
	 * @param hotdealId
	 *            as request parameter
	 * @return xml containing hot deal name and URL.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getSpecialOffShareThruEmail(ShareProductInfo retailerDetailObj) throws ScanSeeException
	{
		final String methodName = "getHotdealShareThruEmail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailersDetails> specialOfflst = null;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WebDisplayRetailerCustomPageURL");
			simpleJdbcCall.returningResultSet("shareSpecialOffInfo", new BeanPropertyRowMapper<RetailersDetails>(RetailersDetails.class));

			final MapSqlParameterSource ratingParameters = new MapSqlParameterSource();
			ratingParameters.addValue("UserID", retailerDetailObj.getUserId());
			ratingParameters.addValue("RetailID", retailerDetailObj.getRetailerId());
			ratingParameters.addValue("PageID", retailerDetailObj.getPageId());
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			ratingParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(ratingParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					specialOfflst = (List<RetailersDetails>) resultFromProcedure.get("shareSpecialOffInfo");
					if (!specialOfflst.isEmpty() && specialOfflst != null)
					{
						response = Utility.formSpecailOffInfoHtml(specialOfflst);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_WebDisplayRetailerCustomPageURL Store Procedure error number: {} " + errorNum
							+ " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (InvalidKeyException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (BadPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method for fetching user information for sending mail to share
	 * product information.
	 * 
	 * @param shareProductInfo
	 *            as request parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String fetchUserInfo(Integer userId) throws ScanSeeException
	{
		final String methodName = "getUserInfo in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			response = this.jdbcTemplate.queryForObject(ShoppingListQueries.FETCHUSEREMAILID, new Object[] { userId }, String.class);

		}

		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = ApplicationConstants.FAILURE;
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}


	 /**
	  * The DAO method for adding share app site by email details to database.
	  * 
	  * @param objShareProductInfo
	  * 
	  * @return The XML as the response.
	  * 
	  * @throws ScanSeeException
	  */
	@Override
	public String shareAppSiteByEmail(ShareProductInfo objShareProductInfo) throws ScanSeeException {
		final String methodName = "shareAppSiteByEmail in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		List<RetailersDetails> arRetailersDetailsList = null;
		
		try	{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ShareAppsite");
			simpleJdbcCall.returningResultSet("shareByEmail", new BeanPropertyRowMapper<RetailersDetails>(RetailersDetails.class));
			final MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue(ApplicationConstants.USERID, objShareProductInfo.getUserId());
			parameters.addValue("RetailID", objShareProductInfo.getRetailerId());
			parameters.addValue("RetailLocationID", objShareProductInfo.getRetailerLocationId());
			parameters.addValue("ShareType", objShareProductInfo.getShareType());
			//parameters.addValue(ApplicationConstants.MAINMENUID, objShareProductInfo.getMainMenuID());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(parameters);
			
			if(null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))	{
				arRetailersDetailsList = (List<RetailersDetails>) resultFromProcedure.get("shareByEmail");
				if(arRetailersDetailsList != null && !arRetailersDetailsList.isEmpty())	{
						arRetailersDetailsList.get(0).setShareText(ApplicationConstants.APPSITE_HEADING);
					if (null == arRetailersDetailsList.get(0).getRetImage()) {
						arRetailersDetailsList.get(0).setRetImage(ApplicationConstants.IMAGENOTFOUND);
					}
					response = Utility.formShareAppInfoHTML(arRetailersDetailsList);
				}
			}
			else	{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_ShareAppsite Store Procedure error number: {} " + errorNum
						+ " and error message: {}" + errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		}
		catch (InvalidKeyException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);

		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		catch (BadPaddingException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		return response;
	}


	/**
	 * The DAOImpl method  when the user clicks Share, they will be prompted to share via different options 
	 * Text/Twitter/Facebook  will send wording, retailer name, and link to the AppSite� information.
	 * 
	 * @param objShareProduct
	 *            as request parameter
	 * @return xml containing rebateId name and details.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RetailersDetails> getShareAppsite(ShareProductInfo objShareProduct) throws ScanSeeException
	{
		final String methodName = "getShareAppsite";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailersDetails> arRetailersDetailsList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ShareAppsite");
			simpleJdbcCall.returningResultSet("retailersDetails", new BeanPropertyRowMapper<RetailersDetails>(RetailersDetails.class));

			final MapSqlParameterSource shareParam = new MapSqlParameterSource();
			shareParam.addValue("UserID", objShareProduct.getUserId());
			shareParam.addValue("RetailID", objShareProduct.getRetailerId());
			shareParam.addValue("RetailLocationID", objShareProduct.getRetailerLocationId());
			shareParam.addValue("ShareType", objShareProduct.getShareType());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(shareParam);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					arRetailersDetailsList = (List<RetailersDetails>) resultFromProcedure.get("retailersDetails");
					//strShareText = (String)resultFromProcedure.get("ShareText");
					if(!arRetailersDetailsList.isEmpty() && arRetailersDetailsList != null)	{
						arRetailersDetailsList.get(0).setShareText(ApplicationConstants.APPSITE_HEADING);
						
						if (null == arRetailersDetailsList.get(0).getRetImage()) {
							arRetailersDetailsList.get(0).setRetImage(ApplicationConstants.IMAGENOTFOUND);
						}
					}
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_ShareAppsite Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arRetailersDetailsList;
	}

	 /**
	  * The DAO method to fetch types of share  
	  * @param objUserTrackingData
	  * @return share types name and its ID
	  * @throws ScanSeeException
	  */
	@Override
	public List<UserTrackingData> getShareTypes() throws ScanSeeException {
		final String methodName = "gerShareTypes";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<UserTrackingData> objShareTypesList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserTrackingShareTypeDisplay");
			simpleJdbcCall.returningResultSet("shareTypes", new BeanPropertyRowMapper<UserTrackingData>(UserTrackingData.class));

			final MapSqlParameterSource shareParam = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(shareParam);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					objShareTypesList = (List<UserTrackingData>) resultFromProcedure.get("shareTypes");
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_UserTrackingShareTypeDisplay Store Procedure error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return objShareTypesList;
	}

	 /**
	  * The DAO method to update share type details to the database 
	  * @param objUserTrackingData
	  * @return SUCCESS or FAILURE string
	  * @throws ScanSeeException
	  */
	@Override
	public String updateShareType(UserTrackingData objUserTrackingData) throws ScanSeeException {
		final String methodName = "updateShareType";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String strSPName = null;
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			final MapSqlParameterSource shareParam = new MapSqlParameterSource();
			
			if(objUserTrackingData.getProdID() != null)	{
				simpleJdbcCall.withProcedureName("usp_UserTrackingProductShare");
				shareParam.addValue("ProductID", objUserTrackingData.getProdID());
				strSPName = "usp_UserTrackingProductShare";
			}
			else if(objUserTrackingData.getHotDealID() != null)	{
				simpleJdbcCall.withProcedureName("usp_UserTrackingHotDealShare");
				shareParam.addValue("ProductHotDealID", objUserTrackingData.getHotDealID());
				strSPName = "usp_UserTrackingHotDealShare";
			}
			else if(objUserTrackingData.getRetLocID() != null)	{
				simpleJdbcCall.withProcedureName("usp_UserTrackingShareAppsite");
				shareParam.addValue("RetailLocationID", objUserTrackingData.getRetLocID());
				strSPName = "usp_UserTrackingShareAppsite";
			}
			else if(objUserTrackingData.getCoupID() != null)	{
				simpleJdbcCall.withProcedureName("usp_UserTrackingCouponShare");
				shareParam.addValue("CouponID", objUserTrackingData.getCoupID());
				strSPName = "usp_UserTrackingCouponShare";
			}
			else if(objUserTrackingData.getLoyID() != null)	{
				simpleJdbcCall.withProcedureName("usp_UserTrackingLoyaltyShare");
				shareParam.addValue("LoyaltyID", objUserTrackingData.getLoyID());
				strSPName = "usp_UserTrackingLoyaltyShare";
			}
			else if(objUserTrackingData.getRebID() != null)	{
				simpleJdbcCall.withProcedureName("usp_UserTrackingRebateShare");
				shareParam.addValue("RebateID", objUserTrackingData.getRebID());
				strSPName = "usp_UserTrackingRebateShare";
			}

			shareParam.addValue(ApplicationConstants.MAINMENUID, objUserTrackingData.getMainMenuID());
			shareParam.addValue("TargetAddress", objUserTrackingData.getTarAddr());
			shareParam.addValue("ShareTypeID", objUserTrackingData.getShrTypID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(shareParam);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					response = ApplicationConstants.SUCCESSRESPONSETEXT;
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in Store Procedure "+ strSPName +" error number: {} " + errorNum + " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		
		return response;
	}

}
