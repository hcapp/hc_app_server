package com.scansee.scannow.controller;



import static com.scansee.common.constants.ScanSeeURLPath.GETSDK;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.scansee.common.constants.ScanSeeURLPath;

/**
 * The ScanNowRestEasy Interface. {@link ImplementedBy}
 * {@link ScanNowRestEasyImpl}
 * 
 * @author shyamsundara_hm
 */
@Path(ScanSeeURLPath.SCANNOEBASEURL)
public interface ScanNowRestEasy
{

	/**
	 * This is a RestEasy WebService Method for fetching product information for
	 * the given bar code,scan type,user id. Method Type:POST
	 * 
	 * @param xml
	 *            contains bar code,scan type and user id needed to fetch
	 *            product information. If bar code ,scan type and user id is
	 *            null then it is invalid request.
	 * @return XML containing product information in the response.
	 */

	@POST
	@Path(ScanSeeURLPath.GETPRODFORBARCODE)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String scanProductForBarCode(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching product information for
	 * the given search key,user id. Method Type:POST
	 * 
	 * @param xml
	 *            contains search key and user id needed to fetch product
	 *            information. If search key and user id is null then it is
	 *            invalid request.
	 * @return XML containing product information in the response.
	 */

	@POST
	@Path(ScanSeeURLPath.GETPRODFORNAME)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String scanProductForName(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching SDK information. Method
	 * Type:GET
	 * 
	 * @return XML containing SDK information in the response.
	 */

	@GET
	@Path(GETSDK)
	String getSDK();

	/**
	 * This is a RestEasy WebService Method for fetching product information for
	 * the given search key,user id. Method Type:POST
	 * 
	 * @param userId
	 *            as request parameter in which scan history to be fetched.
	 * @param lastVisitedRecord
	 *            as request parameter for pagination.
	 * @return XML containing scan history information in the response.
	 */

	@GET
	@Path(ScanSeeURLPath.GETSCANHISTORY)
	@Produces("text/xml;charset=UTF-8")
	String getScanHistory(@QueryParam("userId") Integer userId, @QueryParam("lastVisitedRecord") Integer lastVisitedRecord);
	
	
	/**
	 * This is a RestEasy WebService Method for fetching product information for
	 * the given search key,user id. Method Type:POST
	 * 
	 * @param xml
	 *            contains search key and user id needed to fetch product
	 *            information. If search key and user id is null then it is
	 *            invalid request.
	 * @return XML containing product information in the response.
	 */

	@POST
	@Path(ScanSeeURLPath.GETSEARCHPRODFORNAME)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String searchAddProductForName(String xml);
	
	/**
	 * This is a RestEasy WebService Method for displaying  smart search products by category wise 
	 * the given search key,user id. Method Type:POST
	 * 
	 * @param xml
	 *            contains search key and user id needed to fetch product
	 *            information. If search key and user id is null then it is
	 *            invalid request.
	 * @return XML containing product information in the response.
	 */

	@POST
	@Path(ScanSeeURLPath.GETSMARTSEARCHPRODUCTS)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String smartSearchProduct(String xml);

	/**
	 * This is a RestEasy WebService Method to delete wishlist items.
	 * 
	 * @param xml
	 *            for which wish list Product to be delete.
	 * @return XML based on success or failure
	 */

	@POST
	@Path(ScanSeeURLPath.DELETESCANHISTORYITEM)
	@Produces("text/xml")
	@Consumes("text/xml")
	String deleteScanHistoryItem(String xml);
	
	/**
	 * This is a RestEasy WebService Method for smart search for fetching product information for
	 * the given search key,user id. Method Type:POST
	 * 
	 * @param xml
	 *            contains search key and user id needed to fetch product
	 *            information. If search key and user id is null then it is
	 *            invalid request.
	 * @return XML containing product information in the response.
	 */

	@POST
	@Path(ScanSeeURLPath.GETSMARTSEARCHCOUNT)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getSmartSearchCount(String xml);
	
	/**
	 * This is a RestEasy WebService Method for smart search for fetching product information for
	 * the given search key,user id. Method Type:POST
	 * 
	 * @param xml
	 *            contains search key and user id needed to fetch product
	 *            information. If search key and user id is null then it is
	 *            invalid request.
	 * @return XML containing product information in the response.
	 */

	@POST
	@Path(ScanSeeURLPath.GETSMARTSEARCHPRODS)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getSmartSearchProdlst(String xml);
	
	/**
	 * This is a RestEasy WebService Method for fetching product information for
	 * the default user id and to monitor the reponse time using PingDom. Method Type:GET
	 * 
	 * @param searchKey  as product name search key
	 * @return XML containing product information in the response.
	 */
	@GET
	@Path(ScanSeeURLPath.GET_DEFAULT_PRODUCT_LIST)
	@Produces("text/xml;charset=UTF-8")
	String getProductInfo(@QueryParam("searchkey") String searchkey);
	
//	/**
//	 * This is a RestEasy WebService Method for fetching scan type
//	 * Method Type:GET
//	 * 
//	 * @param No parameter
//	 * @return XML containing ScanType and ScanTypeID
//	 */
//	@GET
//	@Path(ScanSeeURLPath.UTGETSCANTYPE)
//	@Produces("text/xml;charset=UTF-8")
//	String userTrackingGetScanType();
	
	/**
	 * This is a RestEasy WebService Method for fetching MainMenuID from Module ID.
	 * Method Type:GET
	 * 
	 * @param No parameter
	 * @return XML containing MainMenuID
	 */
	@POST
	@Path(ScanSeeURLPath.UTGETMAINMENID)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String userTrackingGetMainMenuID(String xml);
	
	/**
	 * This is a RestEasy WebService Method for give away user registration. 
	 * Method Type:POST.
	 * @param xml
	 *            contains QRRetailerCustomPageID and user id needed.
	 * @return XML containing the success or failure in the response.
	 */
	@POST
	@Path(ScanSeeURLPath.GIVEAWAYUSERREGIST)
	@Produces("text/xml")
	@Consumes("text/xml")
	String giveAwayUserRegistration(String xml);
	
	
	/**
	 * This is a RestEasy WebService Method for display giveAway page details to User. 
	 * Method Type:POST.
	 * @param xml
	 *            contains QRRetailerCustomPageID, mainMenuID and scanTypeID needed.
	 * @return XML containing the success or failure in the response.
	 */
	@POST
	@Path(ScanSeeURLPath.GIVEAWAYPAGE)
	@Produces("text/xml")
	@Consumes("text/xml")
	String giveAwayPageDetails(String xml);
}