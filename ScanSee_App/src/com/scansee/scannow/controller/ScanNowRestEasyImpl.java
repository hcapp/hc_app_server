package com.scansee.scannow.controller;

import static com.scansee.common.util.Utility.formResponseXml;

import javax.ws.rs.QueryParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.servicefactory.ServiceFactory;
import com.scansee.common.util.Utility;
import com.scansee.scannow.service.ScanNowService;

/**
 * The ScanNowRestEasy has methods to accept ScanNowRestEasy requests.These
 * methods are called on the Client request. The method is selected based on the
 * URL Path and the type of request(GET,POST). It invokes the appropriate method
 * in Service layer.
 * 
 * @author shyamsundara_hm
 */
public class ScanNowRestEasyImpl implements ScanNowRestEasy
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ScanNowRestEasyImpl.class.getName());

	/**
	 * The RestEasyImpl method for get product information. Calls method in
	 * service layer. accepts a POST request, MIME type is text/xml.
	 * 
	 * @param xml
	 *            input request contains bar code,user id.
	 * @return returns response XML based on Success or Error.
	 */
	public String scanProductForBarCode(String xml)
	{
		final String methodName = "scanProductForBarCode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request from Client:" + xml);
		}

		String responseXml = null;
		final ScanNowService scanNowService = ServiceFactory.getScanNowService();
		try
		{
			responseXml = scanNowService.scanBarCodeProduct(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;

	}

	/**
	 * The RestEasyImpl method for get product information. Calls method in
	 * service layer. accepts a POST request, MIME type is text/xml.
	 * 
	 * @param xml
	 *            input request contains search key and user id.
	 * @return returns response XML based on Success or Error.
	 */

	public String scanProductForName(String xml)
	{

		final String methodName = "scanProductForName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request from Client:" + xml);
		}
		final ScanNowService scanNowService = ServiceFactory.getScanNowService();
		try
		{
			responseXml = scanNowService.scanForProdName(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * The RestEasyImpl method for get SDK information. Calls method in service
	 * layer. accepts a GET request, MIME type is text/xml.
	 * 
	 * @return returns response XML based on Success or Error.
	 */

	@Override
	public String getSDK()
	{
		final String methodName = "getSDK";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Inside getSDK method:No input parameter");
		}

		final ScanNowService scanNowService = ServiceFactory.getScanNowService();
		try
		{
			responseXml = scanNowService.getSDK();
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response:" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * This is a RestEasyImplementation WebService Method for fetching product
	 * information for the given user id. Method Type:GET
	 * 
	 * @param userId
	 *            as request parameter in which scan history to be fetched.
	 * @param lastVisitedRecord
	 *            as request parameter for pagination.
	 * @return XML containing scan history information in the response.
	 */

	public String getScanHistory(Integer userId, Integer lastVisitedRecord)
	{
		final String methodName = "getScanHistory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (LOG.isDebugEnabled())
		{
			LOG.debug("client Requested :" + "userID" + userId + "lastVisitedRecord" + lastVisitedRecord);
		}
		final ScanNowService scanNowService = ServiceFactory.getScanNowService();
		try
		{
			responseXml = scanNowService.getScanHistory(userId, lastVisitedRecord);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response:" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}
	
	/**
	 * The RestEasyImpl method for get product information. Calls method in
	 * service layer. accepts a POST request, MIME type is text/xml.
	 * 
	 * @param xml
	 *            input request contains search key and user id.
	 * @return returns response XML based on Success or Error.
	 */

	public String searchAddProductForName(String xml)
	{

		final String methodName = "scanProductForName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request from Client:" + xml);
		}
		final ScanNowService scanNowService = ServiceFactory.getScanNowService();
		try
		{
			responseXml = scanNowService.seacrhAddForProdName(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}
	/**
	 * The RestEasyImpl method for get product information. Calls method in
	 * service layer. accepts a POST request, MIME type is text/xml.
	 * 
	 * @param xml
	 *            input request contains search key and user id.
	 * @return returns response XML based on Success or Error.
	 */
	@Override
	public String smartSearchProduct(String xml)
	{
		final String methodName = "smartSearchProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request from Client:" + xml);
		}
		final ScanNowService scanNowService = ServiceFactory.getScanNowService();
		try
		{
			responseXml = scanNowService.smartSearchProducts(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	@Override
	public String deleteScanHistoryItem(String xml)
	{
		final String methodName = "deleteScanHistoryItem";
		String responseXml = null;
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Request XML" + xml);
		}

		final ScanNowService scanNowService = ServiceFactory.getScanNowService();

		try
		{
			responseXml = scanNowService.deleteScanHistoryItem(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);

			responseXml = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned" + responseXml);
		}

		return responseXml;
	}
	/**
	 * The service method For fetching Smart Search product count information based on search key
	 * and user Id.
	 * 
	 * @param xml
	 *            as request contains user Id and search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String getSmartSearchCount(String xml)
	{
		final String methodName = "getSmartSearchCount";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request from Client:" + xml);
		}
		final ScanNowService scanNowService = ServiceFactory.getScanNowService();
		try
		{
			responseXml = scanNowService.getSmartSearchCount(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}
	 /**
	 * The service method For displaying  smart search product list information based on search key
	 * and user Id.
	 * 
	 * @param xml
	 *            as request contains user Id and search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getSmartSearchProdlst(String xml)
	{
		final String methodName = "getSmartSearchProdlst";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request from Client:" + xml);
		}
		final ScanNowService scanNowService = ServiceFactory.getScanNowService();
		try
		{
			responseXml = scanNowService.getSmartSearchProdlst(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}
	
	@Override
	public	String getProductInfo(@QueryParam("searchkey") String searchkey)
	{
		final String methodName = "getProductInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request from Client:" + searchkey);
		}
		final ScanNowService scanNowService = ServiceFactory.getScanNowService();
		try
		{
			responseXml = scanNowService.getProductInfo(searchkey);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * This is a RestEasy WebService Method for fetching MainMenuID
	 * Method Type:GET
	 * 
	 * @param No parameter
	 * @return XML containing ScanType and ScanTypeID
	 */
	@Override
	public String userTrackingGetMainMenuID(String xml) {
		final String methodName = "userTrackingGetMainMenuID";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXml = null;
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request from Client: " + xml);
		}
		final ScanNowService scanNowService = ServiceFactory.getScanNowService();
		try
		{
			responseXml = scanNowService.userTrackingGetMainMenuID(xml);
		}
		catch (ScanSeeException exception)
		{
			responseXml = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client is :" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * This is a RestEasyImpl WebService Method for give away user registration. 
	 * Method Type:POST.
	 * @param xml
	 *            contains QRRetailerCustomPageID and user id needed.
	 * @return XML containing the success or failure in the response.
	 */
	public String giveAwayUserRegistration(String xml)
	{
		final String methodName = "giveAwayUserRegistration";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponseXML = null;
		final ScanNowService scanNowService = ServiceFactory.getScanNowService();
		try {
			strResponseXML = scanNowService.giveAwayUserRegistration(xml);
		} catch (ScanSeeException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			strResponseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponseXML;
	}
	
	/**
	 * This is a RestEasyImpl WebService Method for display giveAway page details to User. 
	 * Method Type:POST.
	 * @param xml
	 *            contains QRRetailerCustomPageID, mainMenuID and scanTypeID needed.
	 * @return XML containing the success or failure in the response.
	 */
	public String giveAwayPageDetails(String xml)
	{
		final String methodName = "giveAwayPageDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponseXML = null;
		final ScanNowService scanNowService = ServiceFactory.getScanNowService();
		try {
			strResponseXML = scanNowService.giveAwayPageDetails(xml);
		} catch (ScanSeeException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			strResponseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponseXML;
	}
}
