package com.scansee.scannow.query;

/**
 * This class contains set of queries used by Scan now module.
 * 
 * @author manjunatha_gh
 */
public class ScanNowQueries
{

	/**
	 * Variable getSDKQuery for displaying BarcodeSDKName based on Active.
	 */

	public static final String SDKQUERY = "Select BarcodeSDKName  from BarcodeSDKUsage where Active=?";

	/**
	 * constructor for ScanNowQueries.
	 */
	private ScanNowQueries()
	{

	}

}
