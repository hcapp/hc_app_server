package com.scansee.scannow.service;

import com.scansee.common.exception.ScanSeeException;

/**
 * This interface for ScanNow methods, which are implemented by ScanNow
 * implementation class.
 * 
 * @author shyamsundara_hm
 */
public interface ScanNowService
{
	/**
	 * The service method For fetching product information based on bar code or
	 * scan code.
	 * 
	 * @param xml
	 *            as request
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String scanBarCodeProduct(String xml) throws ScanSeeException;

	/**
	 * The service method For fetching product information based on search key
	 * and user Id.
	 * 
	 * @param xml
	 *            as request contains user Id and search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String scanForProdName(String xml) throws ScanSeeException;

	/**
	 * The service method For fetching SDK information.
	 * 
	 * @return xml contains SDK information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getSDK() throws ScanSeeException;

	/**
	 * The service method For fetching scan history information based on user
	 * Id.
	 * 
	 * @param userId
	 *            request user
	 * @param lastVisitedRecord
	 *            for to fetch next set of records.
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getScanHistory(Integer userId, Integer lastVisitedRecord) throws ScanSeeException;
	
	
	/**
	 * The service method For fetching product information based on search key
	 * and user Id.
	 * 
	 * @param xml
	 *            as request contains user Id and search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String seacrhAddForProdName(String xml) throws ScanSeeException;
	/**
	 * The service method For fetching product information based on search key
	 * and user Id.
	 * 
	 * @param xml
	 *            as request contains user Id and search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String smartSearchProducts(String xml) throws ScanSeeException;
	
	/**
	 * The method calls JAXB Helper class method and parses the XML. The result
	 * is passed to Wish List DAO method.
	 * 
	 * @param xml
	 *            the Input XML.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String deleteScanHistoryItem(String xml) throws ScanSeeException;
	/**
	 * The service method For fetching Smart Search product count information based on search key
	 * and user Id.
	 * 
	 * @param xml
	 *            as request contains user Id and search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	 String getSmartSearchCount(String xml)throws ScanSeeException;
	 
	 /**
		 * The service method For displaying  smart search product list information based on search key
		 * and user Id.
		 * 
		 * @param xml
		 *            as request contains user Id and search key
		 * @return xml based on success or failure
		 * @throws ScanSeeException
		 *             The exceptions are caught and a ScanSee Exception defined for
		 *             the application is thrown which is caught in the Controller
		 *             layer.
		 */
	String getSmartSearchProdlst(String xml)throws ScanSeeException;
	
	/**
	 * The service method For fetching product information based on search key for monitoring response time	 
	 * 
	 * @param xml
	 *            as request contains search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getProductInfo(String searchKey) throws ScanSeeException;
	
	/**
	 * This is a RestEasy WebService Method for fetching MainMenuID
	 * Method Type:GET
	 * 
	 * @return XML containing MainMenuID
	 * @throws ScanSeeException
	 */
	String userTrackingGetMainMenuID(String xml) throws ScanSeeException;
	
	/**
     * The service method for give away user registration. 
	 * Method Type:POST.
	 * @param xml
	 *            contains QRRetailerCustomPageID and user id needed.
	 * @return XML containing the success or failure in the response.
     */
	String giveAwayUserRegistration(String xml) throws ScanSeeException;
	
	/**
     * The service method for display giveAway page details to User. 
	 * Method Type:POST.
	 * @param xml
	 *            contains QRRetailerCustomPageID, mainMenuID and scanTypeID needed.
	 * @return XML containing the success or failure in the response.
     */
	String giveAwayPageDetails(String xml) throws ScanSeeException;
}
