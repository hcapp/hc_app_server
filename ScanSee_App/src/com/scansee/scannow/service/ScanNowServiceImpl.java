package com.scansee.scannow.service;

import static com.scansee.common.util.Utility.isEmptyOrNullString;

import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.helper.XstreamParserHelper;
import com.scansee.common.pojos.AuthenticateUser;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.SDKInfo;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.common.pojos.WishListHistoryDetails;
import com.scansee.common.util.Utility;
import com.scansee.firstuse.dao.FirstUseDAO;
import com.scansee.scannow.dao.ScanNowDAO;

/**
 * This class is implementation of ScanNowService..
 * 
 * @author shyamsundara_hm
 */

public class ScanNowServiceImpl implements ScanNowService
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ScanNowServiceImpl.class);

	/**
	 * Instance variable for Manage Settings DAO instance.
	 */
	private ScanNowDAO scanNowDao;
	
	/**
	 * Variable type for FirstUse
	 */
	private FirstUseDAO firstUseDao;

	/**
	 * sets the ScanNowDAO DAO.
	 * 
	 * @param scanNowDAO
	 *            The instance for ScanNowDAO
	 */

	public void setScanNowDao(ScanNowDAO scanNowDAO)
	{
		this.scanNowDao = scanNowDAO;
	}

	/**
	 * Setter method for FirstUseDAO.
	 * 
	 * @param firstUseDAO
	 *            the object of type FirstUseDAO
	 */
	public void setFirstUseDao(FirstUseDAO firstUseDao)
	{
		this.firstUseDao = firstUseDao;
	}
	
	/**
	 * The service implementation method for fetching product information by
	 * passing barcode. Calls the XStreams helper class methods for parsing.
	 * 
	 * @param xml
	 *            the request Xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String scanBarCodeProduct(String xml) throws ScanSeeException
	{
		final String methodName = "scanBarCodeProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		ProductDetail productDetail = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetail scanDetail = (ProductDetail) parser.parseXmlToObject(xml);
		if (isEmptyOrNullString(scanDetail.getBarCode()) || scanDetail.getUserId() == null || isEmptyOrNullString(scanDetail.getScanType()))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			productDetail = scanNowDao.scanBarCodeProduct(scanDetail);
			if (null != productDetail)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetail);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.SCANPRODNOTFOUND);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service implementation method for fetching product information by
	 * passing search key. Calls the XStreams helper class methods for parsing.
	 * 
	 * @param xml
	 *            the request Xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String scanForProdName(String xml) throws ScanSeeException
	{
		final String methodName = "scanForProdName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetail seacrhDetail = (ProductDetail) parser.parseXmlToObject(xml);
		if (null == seacrhDetail.getUserId() || isEmptyOrNullString(seacrhDetail.getSearchkey()))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{	
			final ProductDetails productDetails = scanNowDao.scanForProductName(seacrhDetail);
			if (null != productDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetails);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service implementation method for fetching SDK information. Calls the
	 * XStreams helper class methods for parsing.
	 * 
	 * @return response xml contains SDK information
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String getSDK() throws ScanSeeException
	{

		final String methodName = "getSDK";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		
		final SDKInfo sdkInfo = scanNowDao.getSDK();

		if (null != sdkInfo) 
		{
			response = XstreamParserHelper.produceXMlFromObject(sdkInfo);
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * The service implementation method for fetching Scan History information
	 * by passing user id. Calls the XStreams helper class methods for parsing.
	 * 
	 * @param userId
	 *            request user
	 * @param lastVisitedRecord
	 *            for to fetch next set of records.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("static-access")
	@Override
	public String getScanHistory(Integer userId, Integer lastVisitedRecord) throws ScanSeeException
	{
		final String methodName = "getScanHistory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		List<ProductDetail> productDetailslst = null;
		WishListHistoryDetails scanNowHistoryDetailsObj = null;
		if (null == userId)
		{

			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			productDetailslst = scanNowDao.getScanHistory(userId, lastVisitedRecord);
			if (null != productDetailslst && !productDetailslst.isEmpty())
			{
				final ScanNowHelper scanNowHelperObj = new ScanNowHelper();
				scanNowHistoryDetailsObj = scanNowHelperObj.getScanNowHistory(productDetailslst);
				scanNowHistoryDetailsObj.setNextPage(productDetailslst.get(0).getNextPage());
				if (null != scanNowHistoryDetailsObj)
				{

					response = XstreamParserHelper.produceXMlFromObject(scanNowHistoryDetailsObj);
					response = response.replaceAll("<WishListResultSet>", "<ScanHistoryInfo>");
					response = response.replaceAll("</WishListResultSet>", "</ScanHistoryInfo>");
					response = response.replaceAll("<productDetail>", "<ProductDetails>");
					response = response.replaceAll("</productDetail>", "</ProductDetails>");
					response = response.replaceAll("<WishListHistoryDetails>", "<ScanHistory>");
					response = response.replaceAll("</WishListHistoryDetails>", "</ScanHistory>");
				}
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service implementation method for fetching product information by
	 * passing search key. Calls the XStreams helper class methods for parsing.
	 * 
	 * @param xml
	 *            the request Xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String seacrhAddForProdName(String xml) throws ScanSeeException
	{
		final String methodName = "scanForProdName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetail seacrhDetail = (ProductDetail) parser.parseXmlToObject(xml);
		if (null == seacrhDetail.getUserId() || isEmptyOrNullString(seacrhDetail.getSearchkey()))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			final ProductDetails productDetails = scanNowDao.seacrhAddForProductName(seacrhDetail);
			if (null != productDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetails);
				response = StringEscapeUtils.unescapeXml(response);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service implementation method for fetching product information by
	 * passing search key. Calls the XStreams helper class methods for parsing.
	 * 
	 * @param xml
	 *            the request Xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String smartSearchProducts(String xml) throws ScanSeeException
	{
		final String methodName = "smartSearchProducts";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetail seacrhDetail = (ProductDetail) parser.parseXmlToObject(xml);
		if (null == seacrhDetail.getUserId() || isEmptyOrNullString(seacrhDetail.getSearchkey()))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			final ProductDetails productDetails = scanNowDao.smartSearchProducts(seacrhDetail);
			if (null != productDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetails);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String deleteScanHistoryItem(String xml) throws ScanSeeException
	{
		final String methodName = "deleteScanHistoryItem";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ProductDetail productDetail = (ProductDetail) streamHelper.parseXmlToObject(xml);

		if (productDetail.getScanHistoryID() == null || productDetail.getUserId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{

			response = scanNowDao.deleteScanHistoryItem(productDetail);

			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				LOG.info("Wish List product deleted with the UserId:" + productDetail.getUserId() + "and UserProductID"
						+ productDetail.getUserProductId());
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.REMOVE);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method For fetching Smart Search product count information
	 * based on search key and user Id.
	 * 
	 * @param xml
	 *            as request contains user Id and search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getSmartSearchCount(String xml) throws ScanSeeException
	{

		final String methodName = "getSmartSearchCount";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetail seacrhDetail = (ProductDetail) parser.parseXmlToObject(xml);
		if (null == seacrhDetail.getParCatId() || isEmptyOrNullString(seacrhDetail.getSearchkey()))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			//For user tracking
			String prodSearch = seacrhDetail.getSearchkey();
			Integer mainMenuID = seacrhDetail.getMainMenuID();
			Integer prodSmaSeaID = scanNowDao.userTrackingProductSmartSearch(prodSearch, mainMenuID);
			seacrhDetail.setProdSmaSeaID(prodSmaSeaID);
			
			final ProductDetails productDetails = scanNowDao.getSmartSearchCount(seacrhDetail);
			if (null != productDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetails);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method For displaying smart search product list information
	 * based on search key and user Id.
	 * 
	 * @param xml
	 *            as request contains user Id and search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getSmartSearchProdlst(String xml) throws ScanSeeException
	{
		final String methodName = "getSmartSearchProdlst";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetail seacrhDetail = (ProductDetail) parser.parseXmlToObject(xml);
		if (null == seacrhDetail.getParCatId() || isEmptyOrNullString(seacrhDetail.getSearchkey()))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			final ProductDetails productDetails = scanNowDao.getSmartSearchProdlst(seacrhDetail);
			if (null != productDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetails);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	
	@Override
	public String getProductInfo(String searchKey) throws ScanSeeException
	{
		final String methodName = "getProductInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		
		if (isEmptyOrNullString(searchKey))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			ProductDetail productDetail = new ProductDetail();
			int userId = 3982;
			productDetail.setSearchkey(searchKey);			
			productDetail.setUserId(userId);			
			final ProductDetails productDetails = scanNowDao.smartSearchProducts(productDetail);
			if (null != productDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetails);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching scan type
	 * Method Type:GET
	 * 
	 * @return XML containing MainMenuID
	 * @throws ScanSeeException
	 */
	@Override
	public String userTrackingGetMainMenuID(String xml) throws ScanSeeException {
		final String methodName = "userTrackingGetScanType";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final UserTrackingData objUserTrackingData = (UserTrackingData) parser.parseXmlToObject(xml);
		UserTrackingData objMainMenuID = null;
		
		if(objUserTrackingData.getUserID() == null || objUserTrackingData.getModuleID() == null)	{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		} else	{
			Integer mainMenuID = null;
			mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);

			objMainMenuID = new UserTrackingData();
			objMainMenuID.setMainMenuID(mainMenuID);
			response = XstreamParserHelper.produceXMlFromObject(objMainMenuID);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	
	/**
     * The serviceImpl method for give away user registration. 
	 * Method Type:POST.
	 * @param xml
	 *            contains QRRetailerCustomPageID and user id needed.
	 * @return XML containing the success or failure in the response.
     */
	public String giveAwayUserRegistration(String xml)throws ScanSeeException
	{
		final String methodName = "giveAwayUserRegistration";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		AuthenticateUser objAuthentUser =  new AuthenticateUser();
		final AuthenticateUser objAuthUser = (AuthenticateUser) streamHelper.parseXmlToObject(xml);
		if (objAuthUser.getQrRetCustPageID() == null || objAuthUser.getUserId() == null)
		{
			strResponse = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		} else {
			objAuthentUser = scanNowDao.giveAwayUserRegistration(objAuthUser.getUserId(), objAuthUser.getQrRetCustPageID());
			
			if (objAuthentUser.getIsExistingEmail()) {
				strResponse = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.EMAILIDNOTFOUND);
			} else if (objAuthentUser.isExistingUser()) {
				strResponse = Utility.formResponseXml(ApplicationConstants.DUPLICATEUSER, ApplicationConstants.ALREADYREGISTERED);
			} else if (objAuthentUser.isSufficientQty()) {
				strResponse = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTQTY, ApplicationConstants.INSUFFICENTQTY);
			}else {
				strResponse = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SUCCESSRESPONSETEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}
	
	/**
     * The serviceImpl method for display giveAway page details to User. 
	 * Method Type:POST.
	 * @param xml
	 *            contains QRRetailerCustomPageID, mainMenuID and scanTypeID needed.
	 * @return XML containing the success or failure in the response.
     */
	public String giveAwayPageDetails(String xml)throws ScanSeeException
	{
		final String methodName = "giveAwayPageDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		RetailerDetail objRetDetail =  new RetailerDetail();
		final UserTrackingData objUserTrack = (UserTrackingData) streamHelper.parseXmlToObject(xml);
		if (objUserTrack.getQrRetCustPageID() == null || objUserTrack.getMainMenuID() == null || objUserTrack.getScanTypeID() == null)
		{
			strResponse = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		} else {
			objRetDetail = scanNowDao.giveAwayPageDetails(objUserTrack.getQrRetCustPageID(), objUserTrack.getMainMenuID(), objUserTrack.getScanTypeID());
			if (objRetDetail.getExpired() != null) {
				strResponse = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			} else if (objRetDetail.getExpired() == null  ) {
				strResponse = XstreamParserHelper.produceXMlFromObject(objRetDetail);
			} else {
				strResponse = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}
}
