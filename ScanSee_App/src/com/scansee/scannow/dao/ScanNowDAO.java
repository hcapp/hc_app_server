package com.scansee.scannow.dao;

import java.util.List;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AuthenticateUser;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.SDKInfo;

/**
 * This interface is used for ScanNow module.It has methods which are
 * implemented by ScanNowDAOImpl.
 * 
 * @author shyamsundara_hm
 */
public interface ScanNowDAO
{

	/**
	 * The DAO method fetches the product information by passing user id,bar
	 * code,scan type.
	 * 
	 * @param scanDetail
	 *            Instance of ProductDetail contains user id,bar code,scan
	 *            type,scan Date.
	 * @return ProductDetail information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetail scanBarCodeProduct(ProductDetail scanDetail) throws ScanSeeException;

	/**
	 * The DAO method fetches the product information based on search key.
	 * 
	 * @param seacrhDetail
	 *            Instance of ProductDetail.
	 * @return ProductDetails.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetails scanForProductName(ProductDetail seacrhDetail) throws ScanSeeException;

	/**
	 * The DAO method fetches SDK Details.
	 * 
	 * @return ProductDetails.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	SDKInfo getSDK() throws ScanSeeException;

	/**
	 * This DAO method returns the Scan history of the requested user.
	 * 
	 * @param userId
	 *            request user
	 * @param lastVisitedRecord
	 *            for to fetch next set of records.
	 * @return responseXml
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	List<ProductDetail> getScanHistory(Integer userId, Integer lastVisitedRecord) throws ScanSeeException;

	/**
	 * The DAO method fetches the product information based on search key.
	 * 
	 * @param seacrhDetail
	 *            Instance of ProductDetail.
	 * @return ProductDetails.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetails seacrhAddForProductName(ProductDetail seacrhDetail) throws ScanSeeException;

	/**
	 * The DAO method fetches the product information based on search key.
	 * 
	 * @param seacrhDetail
	 *            Instance of ProductDetail.
	 * @return ProductDetails.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetails smartSearchProducts(ProductDetail seacrhDetail) throws ScanSeeException;

	/**
	 * The method for deleting wish list item.
	 * 
	 * @param productDetail
	 *            request parameter
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String deleteScanHistoryItem(ProductDetail productDetail) throws ScanSeeException;

	/**
	 * The doa method For fetching Smart Search product count information based
	 * on search key and user Id.
	 * 
	 * @param xml
	 *            as request contains user Id and search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetails getSmartSearchCount(ProductDetail productDetail) throws ScanSeeException;

	/**
	 * The dao method For displaying smart search product list information based
	 * on search key and user Id.
	 * 
	 * @param xml
	 *            as request contains user Id and search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetails getSmartSearchProdlst(ProductDetail productDetail) throws ScanSeeException;
	
	/**
	 * The dao method fetches product smart search ID form database.
	 * 
	 * @param xml
	 *            as request contains and ProdSearch and mainMenuID
	 * @return prodSmaSeaID
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	Integer userTrackingProductSmartSearch(String prodSearch, Integer mainMenuID) throws ScanSeeException;
	
	/**
	 * The dao method fetches scan typr and its ID.
	 * 
	 * @return ProductDetails object containing scanType and scanTypeID
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	
	/**
     * The DAO method for give away user registration. 
	 * @param input parameter QRRetailerCustomPageID and userID.
	 * @return AuthenticateUser.
     */
	AuthenticateUser giveAwayUserRegistration(Integer iUserId, Integer iQrRetCustPageID) throws ScanSeeException;
	
	/**
     * The DAO method for display giveAway page details to User.
	 * @param input parameter QRRetailerCustomPageID, mainMenuID and scanTypeID.
	 * @return RetailerDetail.
     */
	RetailerDetail giveAwayPageDetails(Integer iQrRetCustPageID, Integer iManiMenuID, Integer iScanTypeID) throws ScanSeeException;
}
