package com.scansee.scannow.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AuthenticateUser;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.SDKInfo;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.common.util.Utility;
import com.scansee.scannow.query.ScanNowQueries;

/**
 * This is implementation class for ScanNowDAO. This class has methods for Scan
 * Now Module. The methods of this class are called from the ScanNowDAO Service
 * layer.
 * 
 * @author shyamsundara_hm
 */
public class ScanNowDAOImpl implements ScanNowDAO
{
	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ScanNowDAOImpl.class);
	/**
	 * for Jdbc connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */
	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To get the data source from xml..
	 * 
	 * @param dataSource
	 *            used for DB access.
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * This DAOImpl method is used to fetch product information based on Bar
	 * code ,scan type.
	 * 
	 * @param scanDetail
	 *            contains bar code,scan type,user id,Date.
	 * @return productDetails product information based on bar code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ProductDetail scanBarCodeProduct(ProductDetail scanDetail) throws ScanSeeException
	{
		final String methodName = "scanBarCodeProduct";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetail productDetails = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ScanProduct");
			simpleJdbcCall.returningResultSet("productslist", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", scanDetail.getUserId());
			scanQueryParams.addValue("ScanCode", scanDetail.getBarCode());
			scanQueryParams.addValue("DeviceID", scanDetail.getDeviceId());
			scanQueryParams.addValue("ScanLongitude", scanDetail.getScanLatitude());
			scanQueryParams.addValue("ScanLatitude", scanDetail.getScanLongitude());
			scanQueryParams.addValue("ScanType", scanDetail.getScanType());
			scanQueryParams.addValue("FieldAgentRequest", 0);
			scanQueryParams.addValue("Date", Utility.getFormattedDate());
			//For user tracking
			scanQueryParams.addValue(ApplicationConstants.MAINMENUID, scanDetail.getMainMenuID());
			scanQueryParams.addValue(ApplicationConstants.SCANTYPESID, scanDetail.getScanTypeID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetailList = (List<ProductDetail>) resultFromProcedure.get("productslist");
					if (null != productDetailList && !productDetailList.isEmpty())
					{
						LOG.info("Product found for the search");
						productDetails = productDetailList.get(0);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_ScanProduct Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in scanBarCodeProduct", exception);
			throw new ScanSeeException(exception);
		}
		catch (ParseException exception)
		{
			LOG.error("Exception occurred in scanBarCodeProduct ", exception);
			throw new ScanSeeException(exception);
		}
		return productDetails;
	}

	/**
	 * This DAOImpl method is used to fetch product information based on search
	 * key.
	 * 
	 * @param searchDetail
	 *            contains search key,user id.
	 * @return productDetails product information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	@SuppressWarnings({ "unchecked" })
	public ProductDetails scanForProductName(ProductDetail searchDetail) throws ScanSeeException
	{

		final String methodName = "scanForProductName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetails productDetails = null;

		try
		{
			if (null == searchDetail.getLastVistedProductNo())
			{
				searchDetail.setLastVistedProductNo(0);

			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_SearchProductPagination");
			simpleJdbcCall.returningResultSet("products", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", searchDetail.getUserId());
			scanQueryParams.addValue("ProdSearch", searchDetail.getSearchkey());
			scanQueryParams.addValue("LowerLimit", searchDetail.getLastVistedProductNo());
			scanQueryParams.addValue("ScreenName", ApplicationConstants.SCANNOWSEARCHPRODPAGINATION);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("products");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{
						LOG.info("Products found for the search");
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetailList);

						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");

						// This code for Pagination. Set the NextPage flag to
						// first row of list
						if (nextpage != null)
						{
							if (nextpage)
							{
								productDetails.setNextPage(1);
							}
							else
							{
								productDetails.setNextPage(0);

							}
						}
					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_SearchProduct Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in scanForProductName", exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;

	}

	/**
	 * This DAOImpl method is used to fetch SDK information.
	 * 
	 * @return Xml contains SDK information
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public SDKInfo getSDK() throws ScanSeeException
	{
		final String methodName = "getSDK";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		SDKInfo sdkInfo = null;
		String sdk = null;

		/*
		 * Query to be changed after the change in table structure.
		 */

		simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
		try
		{
			sdk = this.jdbcTemplate.queryForObject(ScanNowQueries.SDKQUERY, new Object[] { Integer.valueOf(1) }, String.class);

			if (sdk != null)
			{
				sdkInfo = new SDKInfo();
				sdkInfo.setSdkInfo(sdk);
			}

		}

		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in getSDK() method", exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return sdkInfo;
	}

	/**
	 * This DAOImpl method is used to fetch Scan History information.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param lastVisitedRecord
	 *            as request parameter. for to fetch next set of records.
	 * @return Xml contains scan history information
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */

	@SuppressWarnings("unchecked")
	@Override
	public List<ProductDetail> getScanHistory(Integer userId, Integer lastVisitedRecord) throws ScanSeeException

	{
		final String methodName = "getScanHistory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductDetail> productDetailList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ScanHistoryDisplayPagination");
			simpleJdbcCall.returningResultSet("scanhistory", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", userId);
			scanQueryParams.addValue("LowerLimit", lastVisitedRecord);
			scanQueryParams.addValue("ScreenName", ApplicationConstants.SCAHHISTORYPAGINATION);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					productDetailList = (ArrayList<ProductDetail>) resultFromProcedure.get("scanhistory");

					if (!productDetailList.isEmpty() && productDetailList != null)
					{
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");

						// This code for Pagination. Set the NextPage flag to
						// first row of list
						if (nextpage != null)
						{
							if (nextpage)
							{
								productDetailList.get(0).setNextPage(1);
							}
							else
							{
								productDetailList.get(0).setNextPage(0);

							}

						}
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_ScanHistoryDisplayPagination Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}
		}
		catch (EmptyResultDataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			return null;
		}

		catch (DataAccessException exception)
		{
			LOG.error("Exception Occurred in getScanHistory", exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetailList;
	}

	/**
	 * This DAOImpl method is used to fetch product information based on search
	 * key.
	 * 
	 * @param searchDetail
	 *            contains search key,user id.
	 * @return productDetails product information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	@SuppressWarnings({ "unchecked" })
	public ProductDetails seacrhAddForProductName(ProductDetail searchDetail) throws ScanSeeException
	{

		final String methodName = "scanForProductName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetails productDetails = null;

		try
		{
			if (null == searchDetail.getLastVistedProductNo())
			{
				searchDetail.setLastVistedProductNo(0);

			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_SearchProductPagination");
			simpleJdbcCall.returningResultSet("products", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", searchDetail.getUserId());
			scanQueryParams.addValue("ProdSearch", searchDetail.getSearchkey());
			scanQueryParams.addValue("LowerLimit", searchDetail.getLastVistedProductNo());
			scanQueryParams.addValue("ScreenName", "General - Search Add  Product Name");

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("products");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{
						LOG.info("Products found for the search");
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetailList);

						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");

						// This code for Pagination. Set the NextPage flag to
						// first row of list
						if (nextpage != null)
						{
							if (nextpage)
							{
								productDetails.setNextPage(1);
							}
							else
							{
								productDetails.setNextPage(0);

							}
						}
					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_SearchProduct Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in scanForProductName", exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;

	}

	/**
	 * This DAOImpl method is used to fetch product information based on search
	 * key.
	 * 
	 * @param searchDetail
	 *            contains search key,user id.
	 * @return productDetails product information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer
	 */
	@Override
	public ProductDetails smartSearchProducts(ProductDetail searchDetail) throws ScanSeeException
	{
		final String methodName = "scanForProductName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetails productDetails = null;

		try
		{
			if (null == searchDetail.getLastVistedProductNo())
			{
				searchDetail.setLastVistedProductNo(0);

			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ProductSmartSearch");
			simpleJdbcCall.returningResultSet("smartsearchproducts", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			// scanQueryParams.addValue("UserId", searchDetail.getUserId());
			scanQueryParams.addValue("ProdSearch", searchDetail.getSearchkey());
			/*
			 * scanQueryParams.addValue("LowerLimit",
			 * searchDetail.getLastVistedProductNo());
			 * scanQueryParams.addValue("ScreenName",
			 * "General - Search Add  Product Name");
			 */

			//for user tracking
			scanQueryParams.addValue(ApplicationConstants.MAINMENUID, searchDetail.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("smartsearchproducts");
			Integer prodSmaSeaID = (Integer) resultFromProcedure.get("ProductSmartSearchID");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{
						LOG.info("Products found for the search");
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetailList);
						productDetails.setProdSmaSeaID(prodSmaSeaID);

						/*
						 * final Boolean nextpage = (Boolean)
						 * resultFromProcedure.get("NxtPageFlag"); // This code
						 * for Pagination. Set the NextPage flag to // first row
						 * of list if(nextpage !=null) { if (nextpage) {
						 * productDetails.setNextPage(1); } else {
						 * productDetails.setNextPage(0); } }
						 */
					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_ProductSmartSearch Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in scanForProductName", exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;

	}

	@Override
	public String deleteScanHistoryItem(ProductDetail productDetail) throws ScanSeeException
	{
		final String methodName = "deleteScanHistoryItem";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Integer result = 1;

		simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
		simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
		simpleJdbcCall.withProcedureName("usp_ScanHistorytDelete");
		MapSqlParameterSource wishListDeleteParameters;
		try
		{
			wishListDeleteParameters = new MapSqlParameterSource();
			wishListDeleteParameters.addValue("ScanHistoryID", productDetail.getScanHistoryID());
			wishListDeleteParameters.addValue(ApplicationConstants.USERID, productDetail.getUserId());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(wishListDeleteParameters);
			result = (Integer) resultFromProcedure.get(ApplicationConstants.DBSTATUS);
			if (result == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				response = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsgis = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info(ApplicationConstants.ERROROCCURRED + "in usp_ScanHistorytDelete" + errorNum + "errorMsgis.." + errorMsgis);
				throw new ScanSeeException(errorMsgis);

			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * The doa method For fetching Smart Search product count information based
	 * on search key and user Id.
	 * 
	 * @param xml
	 *            as request contains user Id and search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public ProductDetails getSmartSearchCount(ProductDetail productDetail) throws ScanSeeException
	{
		final String methodName = "getSmartSearchCount";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetails productDetails = null;

		try
		{
			if (null == productDetail.getLastVistedProductNo())
			{
				productDetail.setLastVistedProductNo(0);

			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ProductSmartSearchResults");
			simpleJdbcCall.returningResultSet("smartsearchcount", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("ProdSearch", productDetail.getSearchkey());
			scanQueryParams.addValue("ParentCategoryID", productDetail.getParCatId());
			scanQueryParams.addValue("LowerLimit", productDetail.getLastVistedProductNo());
			scanQueryParams.addValue("ScreenName", ApplicationConstants.SMARTSEARCHSCREENNAME);
			//for user tracking
			scanQueryParams.addValue(ApplicationConstants.MAINMENUID, productDetail.getMainMenuID());
			scanQueryParams.addValue(ApplicationConstants.PRODUCTSMARTSEARCHID, productDetail.getProdSmaSeaID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("smartsearchcount");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{
						LOG.info("Products found for the search");
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetailList);

						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						if (nextpage != null)
						{
							if (nextpage)
							{
								productDetails.setNextPage(1);
							}
							else
							{
								productDetails.setNextPage(0);
							}
						}

					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_ProductSmartSearchResults Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in scanForProductName", exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * The dao method For displaying smart search product list information based
	 * on search key and user Id.
	 * 
	 * @param xml
	 *            as request contains user Id and search key
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public ProductDetails getSmartSearchProdlst(ProductDetail productDetail) throws ScanSeeException
	{
		final String methodName = "getSmartSearchProdlst";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetails productDetails = null;

		try
		{
			if (null == productDetail.getLastVistedProductNo())
			{
				productDetail.setLastVistedProductNo(0);

			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ProductSmartSearchProductList");
			simpleJdbcCall.returningResultSet("smartsearchcount", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("ProdSearch", productDetail.getSearchkey());
			scanQueryParams.addValue("ParentCategoryID", productDetail.getParCatId());
			scanQueryParams.addValue("LowerLimit", productDetail.getLastVistedProductNo());
			scanQueryParams.addValue("ScreenName", ApplicationConstants.SMARTSEARCHSCREENNAME);
			//for user tracking
//			scanQueryParams.addValue(ApplicationConstants.PARENTCATEGORYID, productDetail.getParCatId());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("smartsearchcount");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{
						LOG.info("Products found for the search");
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetailList);

						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						if (nextpage != null)
						{
							if (nextpage)
							{
								productDetails.setNextPage(1);
							}
							else
							{
								productDetails.setNextPage(0);
							}
						}

					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_ProductSmartSearchProductList Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in scanForProductName", exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * The dao method For adding smart search data to database for User tracking
	 *  
	 * @param xml
	 *            as request contains mainMenuID and search key
	 * @return productSmartSearchID
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public Integer userTrackingProductSmartSearch(String prodSearch, Integer mainMenuID) throws ScanSeeException {
		final String methodName = "getSmartSearchProdlst";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Integer prodSmaSeaID = null;
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserTrackingProductSmartSearch");
			simpleJdbcCall.returningResultSet("smartsearchcount", new BeanPropertyRowMapper<UserTrackingData>(UserTrackingData.class));

			final MapSqlParameterSource productSearch = new MapSqlParameterSource();
			productSearch.addValue("ProdSearch", prodSearch);
			productSearch.addValue(ApplicationConstants.MAINMENUID, mainMenuID);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productSearch);
			

			if (null == resultFromProcedure.get("ErrorNumber")) {
				prodSmaSeaID = (Integer) resultFromProcedure.get("ProductSmartSearchID");
			} 
			else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.error("Error occurred in usp_UserTrackingProductSmartSearch Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}	
		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in userTrackingProductSmartSearch", exception);
			throw new ScanSeeException(exception);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		
		return prodSmaSeaID;
	}

	/**
	 * The dao method fetches scan typr and its ID.
	 * 
	 * @return ProductDetails object containing scanType and scanTypeID
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
//	@SuppressWarnings("unchecked")
//	@Override
//	public ProductDetails userTrackingGetScanType() throws ScanSeeException {
//		final String methodName = "userTrackingGetScanType";
//		LOG.info(ApplicationConstants.METHODSTART + methodName);
//		List<ProductDetail> objProductDetailsList = null;
//		ProductDetails objProductDetails = null;
//		
//		try
//		{
//			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
//			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
//			simpleJdbcCall.withProcedureName("usp_UserTrackingScanTypeDisplay");
//			simpleJdbcCall.returningResultSet("scanType", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
//
//			final MapSqlParameterSource productSearch = new MapSqlParameterSource();
//
//			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productSearch);
//			
//			if (null == resultFromProcedure.get("ErrorNumber")) {
//				objProductDetailsList = (List<ProductDetail>) resultFromProcedure.get("scanType");
//				objProductDetails = new ProductDetails();
//				objProductDetails.setProductDetail(objProductDetailsList);
//			} 
//			else {
//				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
//				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
//				LOG.error("Error occurred in usp_UserTrackingProductSmartSearch Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
//				throw new ScanSeeException(errorMsg);
//			}	
//		}
//		catch (DataAccessException exception)
//		{
//			LOG.error("Exception occurred in userTrackingProductSmartSearch", exception);
//			throw new ScanSeeException(exception);
//		}
//		LOG.info(ApplicationConstants.METHODEND + methodName);
//		
//		return objProductDetails;
//	}
	
	
	/**
     * The DAOImpl method for give away user registration. 
	 * @param input parameter QRRetailerCustomPageID and userId.
	 * @return AuthenticateUser.
     */
	@Override
	public AuthenticateUser giveAwayUserRegistration(Integer iUserId, Integer iQrRetCustPageID) throws ScanSeeException {
		final String methodName = "giveAwayUserRegistration";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Boolean bEmailID = false;
		Boolean bDupUserID = false;
		Boolean bSuffQty = false;
		Integer iStatus = 0;
		AuthenticateUser objAuthUser = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GiveAwayUserRegistration");
			final MapSqlParameterSource objGiveAwayRegParam = new MapSqlParameterSource();
			objGiveAwayRegParam.addValue(ApplicationConstants.USERID, iUserId);
			objGiveAwayRegParam.addValue("QRRetailerCustomPageID", iQrRetCustPageID);
			resultFromProcedure = simpleJdbcCall.execute(objGiveAwayRegParam);
			
			if (null != resultFromProcedure) {
				objAuthUser = new AuthenticateUser();
				bEmailID = (Boolean) resultFromProcedure.get("NoEmailID");
				bDupUserID = (Boolean) resultFromProcedure.get("DuplicateUserID");
				bSuffQty = (Boolean) resultFromProcedure.get("InsufficientQty");
				iStatus = (Integer) resultFromProcedure.get("Status");
					objAuthUser.setExistingUser(bDupUserID);
					objAuthUser.setIsExistingEmail(bEmailID);
					objAuthUser.setSufficientQty(bSuffQty);
					objAuthUser.setStatus(iStatus);
			}
		} catch (DataAccessException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,exception);
			throw new ScanSeeException(exception.getMessage());
		}
		return objAuthUser;
	}
	
	/**
     * The DAOImpl method for display giveAway page details to User.
	 * @param input parameter QRRetailerCustomPageID, mainMenuID and scanTypeID .
	 * @return RetailerDetail.
     */
	@Override
	public RetailerDetail giveAwayPageDetails(Integer iQrRetCustPageID, Integer iManiMenuID, Integer iScanTypeID) throws ScanSeeException {
		final String methodName = "giveAwayPageDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer iStatus = 0;
		Boolean bExpire = false;
		RetailerDetail objRetDetail = null;
		List <RetailerDetail> arretDetailList  = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GiveAwaypagedetails");
			simpleJdbcCall.returningResultSet("giveAwayPage", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource objGiveAwayInfoParam = new MapSqlParameterSource();
			objGiveAwayInfoParam.addValue("QRRetailerCustomPageID", iQrRetCustPageID);
			objGiveAwayInfoParam.addValue("MainMenuID", iManiMenuID);
			objGiveAwayInfoParam.addValue("ScanTypeID", iScanTypeID);
			resultFromProcedure = simpleJdbcCall.execute(objGiveAwayInfoParam);
			arretDetailList = (List<RetailerDetail>)resultFromProcedure.get("giveAwayPage");
			if (null != resultFromProcedure) {
				iStatus = (Integer) resultFromProcedure.get("Status");
				bExpire = (Boolean) resultFromProcedure.get("Expired");
				objRetDetail = new RetailerDetail();
				if (null == resultFromProcedure.get("ErrorNumber")) {
					if (null != arretDetailList && !arretDetailList.isEmpty()) {
						objRetDetail = (RetailerDetail)arretDetailList.get(0);
					} else if (bExpire == true) {
					objRetDetail.setExpired(bExpire);
					}
				} else {
						final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
						final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
						LOG.error("Error occurred in usp_GiveAwaypagedetails Store Procedure error number: {} and error message: {}", errorNum,
								errorMsg);
						throw new ScanSeeException(errorMsg);
					}
				}
		} catch (DataAccessException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,exception);
			throw new ScanSeeException(exception.getMessage());
		}
		return objRetDetail;
	}
}
