package com.scansee.wishlist.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.scansee.common.constants.ScanSeeURLPath;

/**
 * This is a RestEasy WebService Interface for capturing wish list requests.
 * 
 * @author shyamsundara_hm
 */
@Path(ScanSeeURLPath.WISHLISTBASEURL)
public interface WishListRestEasy
{

	/**
	 * This is a RestEasy WebService Method for fetching Wish list for the.
	 * given ProductId and UserId .Method Type:GET
	 * 
	 * @param productId
	 *            for which Product information to be fetched.
	 * @param userId
	 *            for which Product information to be fetched.
	 * @return XML containing wish list product information in the response.
	 */

	@GET
	@Path(ScanSeeURLPath.FETCHPRODUCTDETAILS)
	@Produces("text/xml;charset=UTF-8")
	String fetchProductDetails(@QueryParam("productId") Integer productId, @QueryParam("userId") Integer userId);

	/**
	 * This is a RestEasy WebService Method to delete wishlist items.
	 * 
	 * @param xml
	 *            for which wish list Product to be delete.
	 * @return XML based on success or failure
	 */

	@POST
	@Path(ScanSeeURLPath.DELETEWISHITEM)
	@Produces("text/xml")
	@Consumes("text/xml")
	String deleteWishListItem(String xml);

	/**
	 * the GET method which fetches wishlist items info.
	 * 
	 * @param userId
	 *            the UserId in the request.
	 * @param zipcode
	 *            the zipcode in the request. 
	 * @param latitude
	 *            the latitude in the request.  
	 * @param longitude
	 *           as request parameter                   
	 * @param radius
	 *            the radius in the request.                                  
	 * @return the XML in response.
	 */
	@POST
	@Path("/getWishListItems")
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getWishListItems(String xml);

	/**
	 * the GET method which fetches product info.
	 * 
	 * @param userId
	 *            of the user
	 * @param prodSearchKey
	 *            are in the request.
	 * @return the XML in response.
	 */
	@GET
	@Path("/getWishListProductItems")
	@Produces("text/xml;charset=UTF-8")
	String getWishListProductItems(@QueryParam("USERID") Integer userId, @QueryParam("searchKey") String prodSearchKey);

	/**
	 * This method is used to add the unassigned product to wish list.
	 * 
	 * @param xml
	 *            the XML in the request
	 * @return response: return the whether added is successful or not.
	 */
	@POST
	@Path("/addunassignedProd")
	@Produces("text/xml")
	@Consumes("text/xml")
	String addUnassignedProd(String xml);

	/**
	 * This method is used to add the unassigned product to wish list.
	 * 
	 * @param xml
	 *            the XML in the request
	 * @return response: return the whether added is successful or not.
	 */
	@POST
	@Path("/addWishListProd")
	@Produces("text/xml")
	@Consumes("text/xml")
	String addWishListProd(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching Wish list Hot Deal
	 * information for the. given UserId .Method Type:GET
	 * 
	 * @param userId 
	 *            as request parameter
	 * @param productId 
	 *             as request parameter
	 * @param zipcode
	 *           as request parameter
	 * @param latitude
	 *           as request parameter 
	 * @param longitude
	 *           as request parameter             
	 * @param radius
	 *            as request parameter             
	 * @return The XML in response.
	 */

	@POST
	@Path(ScanSeeURLPath.GETHOTDEALFORPRODUCT)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getHotDealInfoForProduct(String xml);

	/**
	 * the GET method which fetches wishList Store info.
	 * 
	 * @param userId
	 *            of the user
	 * @param productId
	 *            are in the request.
	 * @param zipcode
	 *           as request parameter
	 * @param latitude
	 *           as request parameter 
	 * @param longitude
	 *           as request parameter           
	 * @param radius
	 *            as request parameter           
	 * @return returns response XML based on Success or Error.
	 */

	@POST
	@Path(ScanSeeURLPath.GETSTOREDETAILS)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getWishListStoreDetails(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching Wish list History for
	 * the. given UserId .Method Type:GET
	 * 
	 * @param userId
	 *            for which wish list history information to be fetched.
	 * @return XML containing wish list history information in the response.
	 */

	@GET
	@Path(ScanSeeURLPath.GETWISHLISTHISTORY)
	@Produces("text/xml;charset=UTF-8")
	String fetchWishListHistory(@QueryParam("userId") Integer userId, @QueryParam("lowerlimit") Integer lowerLimit);

	/**
	 * the GET method which fetches wishList Product Attributes info.
	 * 
	 * @param userId
	 *            of the user
	 * @param productId
	 *            are in the request.
	 * @return returns response XML based on Success or Error.
	 */

	@GET
	@Path(ScanSeeURLPath.FETCHPROUDCTATTRIBUTES)
	@Produces("text/xml;charset=UTF-8")
	String getProductAttributes(@QueryParam("userId") Integer userId, @QueryParam("productId") Integer productId);

	
	/**
	 * the GET method which fetches coupon info.
	 * 
	 * @param userId
	 *            of the user
	 * @param productId
	 *            are in the request.
	 * @param zipcode
	 *           as request parameter
	 * @param latitude
	 *           as request parameter 
	 * @param longitude
	 *           as request parameter                  
	 * @param radius
	 *            as request parameter               
	 * @return returns response XML based on Success or Error.
	 */

	@POST
	@Path(ScanSeeURLPath.FETCHWISHLISTCOUPONINFO)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String fetchCouponInfo(String xml);
	
	/**
	 * the GET method which fetches wishList Product Attributes info.
	 * 
	 * @param userId
	 *            of the user
	 * @return returns response XML based on Success or Error.
	 */

	@GET
	@Path(ScanSeeURLPath.CHECKUSERZIPCODE)
	@Produces("text/xml;charset=UTF-8")
	String checkUserZipcode(@QueryParam("userId") Integer userId);
	
	/**
	 * This is a RestEasy WebService Method to delete wishlist items.
	 * 
	 * @param xml
	 *            for which wish list Product to be delete.
	 * @return XML based on success or failure
	 */

	@POST
	@Path(ScanSeeURLPath.DELETEWLHISTORYITEM)
	@Produces("text/xml")
	@Consumes("text/xml")
	String deleteWLHistoryItem(String xml);
}
