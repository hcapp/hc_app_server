package com.scansee.wishlist.controller;

import static com.scansee.common.util.Utility.formResponseXml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.servicefactory.ServiceFactory;
import com.scansee.wishlist.service.WishListService;

/**
 * The class accepts the web service requests for WishListModule.
 * 
 * @author dileepa_cc
 */
public class WishListRestEasyImpl implements WishListRestEasy
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(WishListRestEasyImpl.class);

	/**
	 * The variable for return XML.
	 */

	private String responseXML = "";

	/**
	 * This is a RestEasy WebService method for fetching wish list product
	 * information. Calls method in service layer. accepts a GET request in mime
	 * type text/plain.
	 * 
	 * @param productId
	 *            input request for which wish list product information to be
	 *            fetched.
	 * @param userId
	 *            input request for which wish list product information to be
	 *            fetched.
	 * @return returns response XML based on Success or Error.
	 */

	@Override
	public String fetchProductDetails(Integer productId, Integer userId)
	{

		final String methodName = "fetchProductDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Data: productId  " + productId + "User Id:" + userId);
		}

		final WishListService wishListService = ServiceFactory.getWishListService();

		try
		{
			responseXML = wishListService.fetchProductDetails(productId, userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned to Client" + responseXML);
		}

		return responseXML;
	}

	/**
	 * The method for Deleting wish list item. Calls method in service layer.
	 * accepts a POST request in mime type text/xml.
	 * 
	 * @param xml
	 *            input request
	 * @return returns response XML based on Success or Error.
	 */

	@Override
	public String deleteWishListItem(String xml)
	{

		final String methodName = "deleteWishListItem";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Request XML" + xml);
		}

		final WishListService wishListService = ServiceFactory.getWishListService();

		try
		{
			responseXML = wishListService.deleteWishListItem(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);

			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned" + responseXML);
		}

		return responseXML;
	}

	/**
	 * The method for displaying wish list products . Calls method in service
	 * layer. accepts a GET request in mime type text/plain.
	 * 
	 * @param userId
	 *            input request
	 * @param zipcode
	 *            input request
	 * @param latitude
	 *            input request
	 * @param longitude
	 *            input request
	 * @param radius
	 *            input request
	 * @return returns response XML based on Success or Error.
	 */

	public String getWishListItems(String xml)
	{
		final String methodName = "getWishListItems";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Data: " + xml);
		}

		String response = null;
		final WishListService wishListService = ServiceFactory.getWishListService();
		try
		{

			response = wishListService.getWishListItems(xml);

		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned" + responseXML);
		}

		return response;
	}

	/**
	 * The method for displaying wish list products . Calls method in service
	 * layer. accepts a GET request in MIME type text/plain.
	 * 
	 * @param userId
	 *            of the user
	 * @param productSearchkey
	 *            are the input requests
	 * @return returns response XML based on Success or Error.
	 */

	public String getWishListProductItems(Integer userId, String productSearchkey)
	{
		final String methodName = "getWishListProductItems";

		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (LOG.isDebugEnabled())
		{

			LOG.debug("Recieved request parameters: userId:" + userId + "productSearchkey:" + productSearchkey);
		}
		String response = null;
		final WishListService wishListService = ServiceFactory.getWishListService();
		try
		{
			response = wishListService.getWishListProductItems(userId, productSearchkey);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned to Client is :" + responseXML);
		}

		return response;
	}

	/**
	 * The method for adding unassigned product to wish list. Calls method in
	 * service layer. accepts a POST request in MIME type text/XML.
	 * 
	 * @param xml
	 *            input request
	 * @return returns response XML based on Success or Error.
	 */
	public String addUnassignedProd(String xml)
	{
		final String methodName = "addUnassignedProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (LOG.isDebugEnabled())
		{

			LOG.debug("Recieved Xml:" + xml);

		}

		String response = null;
		final WishListService wishListService = ServiceFactory.getWishListService();

		try
		{
			response = wishListService.addUnassigedProd(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned:" + responseXML);
		}

		return response;
	}

	/**
	 * The method for adding searched product to wish list. Calls method in
	 * service layer. accepts a POST request in MIME type text/XML.
	 * 
	 * @param xml
	 *            input request
	 * @return returns response XML based on Success or Error.
	 */
	public String addWishListProd(String xml)
	{
		final String methodName = "addWishListProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Xml:" + xml);
		}

		String response = null;
		final WishListService wishListService = ServiceFactory.getWishListService();

		try
		{
			response = wishListService.addWishListProd(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned:" + response);
		}

		return response;
	}

	/**
	 * the GET method which fetches wishList hot deal info. Calls method in *
	 * service layer. accepts a POST request in MIME type text/XML.
	 * 
	 * @param userId
	 *            of the user
	 * @param productId
	 *            are in the request.
	 * @param zipcode
	 *            input request
	 * @param latitude
	 *            input request
	 * @param longitude
	 *            input request
	 * @param radius
	 *            input request
	 * @return returns response XML based on Success or Error.
	 */

	@Override
	public String getHotDealInfoForProduct(String xml)
	{
		final String methodName = "getHotDealInfoForProduct";

		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (LOG.isDebugEnabled())
		{

			LOG.debug("Recieved Data: " + xml);
		}
		String response = null;
		final WishListService wishListService = ServiceFactory.getWishListService();
		try
		{
			response = wishListService.getHotDealInfoForProduct(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned in the Client is :" + responseXML);
		}

		return response;

	}

	/**
	 * the GET method which fetches wishList Store info.
	 * 
	 * @param userId
	 *            of the user
	 * @param productId
	 *            are in the request.
	 * @param zipcode
	 *            input request
	 * @param latitude
	 *            input request
	 * @param longitude
	 *            input request
	 * @param radius
	 *            input request
	 * @return returns response XML based on Success or Error.
	 */

	public String getWishListStoreDetails(String xml)
	{

		final String methodName = "getStoreDetails";

		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (LOG.isDebugEnabled())
		{

			LOG.debug("Recieved Data: " + xml);
		}
		String response = null;
		final WishListService wishListService = ServiceFactory.getWishListService();
		try
		{
			response = wishListService.getAllWishListStoreDetails(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned int the Client is :" + responseXML);
		}

		return response;

	}

	/**
	 * The method for displaying wish list history products . Calls method in
	 * service layer. accepts a GET request in mime type text/plain.
	 * 
	 * @param userId
	 *            as request parameter
	 * @param lowerlimit
	 *            as request parameter
	 * @return returns response XML based on Success or Error.
	 */

	@Override
	public String fetchWishListHistory(Integer userId, Integer lowerlimit)
	{
		final String methodName = "fetchWishListHistory";

		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (LOG.isDebugEnabled())
		{

			LOG.debug("Recieved Data : userId:" + userId);
		}
		String response = null;
		final WishListService wishListService = ServiceFactory.getWishListService();
		try
		{
			response = wishListService.fetchWishListHistoryDetails(userId, lowerlimit);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned in the Client is :" + responseXML);
		}

		return response;

	}

	/**
	 * the GET method which fetches Product Attributes information.
	 * 
	 * @param userId
	 *            of the user
	 * @param productId
	 *            are in the request.
	 * @return returns response XML based on Success or Error.
	 */
	@Override
	public String getProductAttributes(Integer userId, Integer productId)
	{
		final String methodName = "getProductAttributes";

		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (LOG.isDebugEnabled())
		{

			LOG.debug("Recieved Data: userId:" + userId + "productId:" + productId);
		}
		String response = null;
		final WishListService wishListService = ServiceFactory.getWishListService();
		try
		{
			response = wishListService.fetchProductAttributes(userId, productId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned Client is  :" + responseXML);
		}

		return response;

	}

	/**
	 * This method used to display product coupon details.
	 * 
	 * @param userId
	 *            of the user
	 * @param productId
	 *            are in the request.
	 * @param zipcode
	 *            input request
	 * @param latitude
	 *            input request
	 * @param longitude
	 *            input request
	 * @param radius
	 *            input request
	 * @return response -As String
	 */
	@Override
	public String fetchCouponInfo(String xml)
	{
		final String methodName = "fetchCouponInfo";

		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (LOG.isDebugEnabled())
		{

			LOG.debug("Recieved Data: " + xml);
		}
		String response = null;
		final WishListService wishListService = ServiceFactory.getWishListService();
		try
		{
			response = wishListService.fetchCouponDetails(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned Client is :" + responseXML);
		}

		return response;

	}

	/**
	 * This is a RestEasy WebService method for checking user Zip code exists or
	 * not. If exists it returns zipcode. Calls method in service layer. accepts
	 * a GET request in mime type text/plain.
	 * 
	 * @param userId
	 *            input request for which user zip code to be fetched.
	 * @return returns response XML containing user zip code.
	 */
	@Override
	public String checkUserZipcode(Integer userId)
	{
		final String methodName = "checkUserZipcode";

		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (LOG.isDebugEnabled())
		{

			LOG.debug("Recieved Data: userId:" + userId);
		}
		String response = null;
		final WishListService wishListService = ServiceFactory.getWishListService();
		try
		{
			response = wishListService.checkUserZipcode(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned Client is :" + responseXML);
		}

		return response;

	}

	@Override
	public String deleteWLHistoryItem(String xml)
	{
		final String methodName = "deleteWLHistoryItem";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Request XML" + xml);
		}

		final WishListService wishListService = ServiceFactory.getWishListService();

		try
		{
			responseXML = wishListService.deleteWLHistoryItem(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);

			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned" + responseXML);
		}

		return responseXML;
	}

}
