package com.scansee.wishlist.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.ProductAttributes;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.ProductDetailsRequest;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.ThisLocationRequest;
import com.scansee.common.pojos.WishListResultSet;
import com.scansee.common.util.Utility;

/**
 * This class for fetching wish list details.
 */
public class WishListDAOImpl implements WishListDAO
{

	/**
	 * Getting the Logger Instance.
	 */

	private static final Logger log = LoggerFactory.getLogger(WishListDAOImpl.class.getName());

	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */

	@SuppressWarnings("unused")
	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * for Jdbc connection.
	 */
	private JdbcTemplate jdbcTemplate;

	/**
	 * To call stored procedue.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * This method for to get data source from xml.
	 * 
	 * @param dataSource
	 *            datasource name
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * This method for deleting wish list item.
	 * 
	 * @param productDetail
	 *            request parameter
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String deleteWishListItem(ProductDetail productDetail) throws ScanSeeException
	{

		final String methodName = "deleteWishListItem";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Integer result = 1;

		simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
		simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
		simpleJdbcCall.withProcedureName("usp_WishListDelete");
		SqlParameterSource wishListDeleteParameters;
		try
		{
			wishListDeleteParameters = new MapSqlParameterSource().addValue("UserProductID", productDetail.getUserProductId())
					.addValue(ApplicationConstants.USERID, productDetail.getUserId()).addValue("WishListRemoveDate", Utility.getFormattedDate());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(wishListDeleteParameters);
			result = (Integer) resultFromProcedure.get(ApplicationConstants.DBSTATUS);
			if (result == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				response = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsgis = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "in usp_WishListDelete" + errorNum + "errorMsgis.." + errorMsgis);

				throw new ScanSeeException(errorMsgis);

			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		catch (ParseException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method fetches WishList Products.
	 * 
	 * @param objThisLocationRequest request parameter.
	 * @param strApplnConstant as request parameter.
	 * @return Success or Failure depending upon the result.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<ProductDetail> getWishListItems(ThisLocationRequest objThisLocRequest, String strApplnConstant)
			throws ScanSeeException
	{
		final String methodName = "getWishListItems";
		log.info(ApplicationConstants.METHODSTART + methodName);
		WishListResultSet wishListProds = null;
		ArrayList<ProductDetail> productDetails = null;
		Boolean bNextPage = false;
		Integer iMaxCount = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WishListDisplay");
			simpleJdbcCall.returningResultSet("wistListsItems", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource fetchProdDetailsParam = new MapSqlParameterSource();
			fetchProdDetailsParam.addValue(ApplicationConstants.USERID, objThisLocRequest.getUserId());
			fetchProdDetailsParam.addValue("ZipCode", objThisLocRequest.getZipcode());
			fetchProdDetailsParam.addValue("Latitude", objThisLocRequest.getLatitude());
			fetchProdDetailsParam.addValue("Longitude", objThisLocRequest.getLongitude());
			fetchProdDetailsParam.addValue("Radius", objThisLocRequest.getRadius());
			
			//For user tracking
			fetchProdDetailsParam.addValue(ApplicationConstants.MAINMENUID, objThisLocRequest.getMainMenuID());
			/* For pagination */
			fetchProdDetailsParam.addValue("ScreenName", strApplnConstant);
			fetchProdDetailsParam.addValue("LowerLimit", objThisLocRequest.getLastVisitedRecord());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProdDetailsParam);
			productDetails = (ArrayList<ProductDetail>) resultFromProcedure.get("wistListsItems");
			
			if (null != productDetails && !productDetails.isEmpty()) {
				iMaxCount = (Integer) resultFromProcedure.get("MaxCnt");
				bNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
				wishListProds = new WishListResultSet();
				wishListProds.setProductDetail(productDetails);
				productDetails.get(0).setMaxCount(iMaxCount);
				if (bNextPage== true) {
					productDetails.get(0).setNextPage(1);
				} else {
					productDetails.get(0).setNextPage(0);
				}
			}
		} catch (DataAccessException e) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * The method fetches WishList searched Products.
	 * 
	 * @param userId
	 *            of the user
	 * @param productSearchkey
	 *            are the request parameters
	 * @return Success or Failure depending upon the result.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public WishListResultSet getWishListProductItems(int userId, String productSearchkey) throws ScanSeeException
	{
		final String methodName = "getWishListProductItems";
		log.info(ApplicationConstants.METHODSTART + methodName);

		WishListResultSet wishListProdItems = null;
		ArrayList<ProductDetail> productDetails = null;
		try
		{
			/*
			 * simpleJdbcCall = new
			 * SimpleJdbcCall(jdbcTemplate).withProcedureName
			 * ("usp_MasterShoppingListSearch"
			 * ).returningResultSet("productDetails",
			 * ParameterizedBeanPropertyRowMapper
			 * .newInstance(ProductDetail.class));
			 */
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListSearch");
			simpleJdbcCall.returningResultSet("productDetails", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue("UserId", userId);
			fetchProductDetailsParameters.addValue("ProdSearch", productSearchkey);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			productDetails = (ArrayList<ProductDetail>) resultFromProcedure.get("productDetails");

			if (null != productDetails && !productDetails.isEmpty())
			{
				wishListProdItems = new WishListResultSet();
				wishListProdItems.setProductDetail(productDetails);
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return wishListProdItems;
	}

	/**
	 * The method to add unassigned product to WishList.
	 * 
	 * @param productDetailsRequest
	 *            is the request parameter
	 * @return Success or Failure depending upon the result.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */
	@Override
	public String addUnassignedPro(ProductDetailsRequest productDetailsRequest) throws ScanSeeException
	{
		final String methodName = "addUnassignedPro";
		String response;
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer result = 1;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WishListUnassignedProducts");
			final MapSqlParameterSource addUnassignedProParams = new MapSqlParameterSource();
			addUnassignedProParams.addValue(ApplicationConstants.USERID, productDetailsRequest.getUserId());
			addUnassignedProParams.addValue("UnassignedProductName", productDetailsRequest.getProductName());
			addUnassignedProParams.addValue("UnassignedProductDescription", productDetailsRequest.getProductDescription());
			addUnassignedProParams.addValue("WishListAddDate", Utility.getFormattedDate());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(addUnassignedProParams);
			result = (Integer) resultFromProcedure.get("Status");
			if (result == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

				log.info(ApplicationConstants.ERROROCCURRED + "in usp_WishListUnassignedProducts" + errorNum + "errorMsg" + errorMsg);
				response = ApplicationConstants.FAILURE;
				throw new ScanSeeException(errorMsg);

			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e.getMessage());
		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method to add searched product to WishList.
	 * 
	 * @param productDetailsRequest
	 *            is the request parameter
	 * @return Success or Failure depending upon the result.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */
	@Override
	public String addWishListProd(ProductDetailsRequest productDetailsRequest) throws ScanSeeException
	{
		final String methodName = "addWishListProd";
		log.info(ApplicationConstants.METHODSTART + methodName);

		MapSqlParameterSource scanQueryParams = null;

		String responseFromProc = null;
		Integer fromProc = null;
		Integer productExits = null;

		try
		{
			final List<ProductDetail> productDetails = productDetailsRequest.getProductDetails();

			if (null != productDetails && !productDetails.isEmpty())
			{

				if (null == productDetailsRequest.getIsWishlstHistory())
				{
					final String idString = Utility.getCommaSepartedValues(productDetails);

					simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
					simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
					simpleJdbcCall.withProcedureName("usp_WishListSearchAdd");
					simpleJdbcCall.returningResultSet("userProdId", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
					scanQueryParams = new MapSqlParameterSource();
					scanQueryParams.addValue(ApplicationConstants.USERID, productDetailsRequest.getUserId());
					scanQueryParams.addValue("ProductID", idString);
					scanQueryParams.addValue("WishListAddDate", Utility.getFormattedDate());
					scanQueryParams.addValue(ApplicationConstants.MAINMENUID, productDetailsRequest.getMainMenuID());

				}
				else
				{
					final String idString = Utility.getCommaSepartedUserProductID(productDetails);
					simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
					simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
					simpleJdbcCall.withProcedureName("usp_WishlistAddFromHistory");
					simpleJdbcCall.returningResultSet("userProdId", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
					scanQueryParams = new MapSqlParameterSource();
					scanQueryParams.addValue("UserID", productDetailsRequest.getUserId());
					scanQueryParams.addValue("userproductid", idString);
					scanQueryParams.addValue("WishListAddDate", Utility.getFormattedDate());
					scanQueryParams.addValue(ApplicationConstants.MAINMENUID, productDetailsRequest.getMainMenuID());
				}

				final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

				fromProc = (Integer) resultFromProcedure.get("Status");

				if (fromProc == 0)
				{
					responseFromProc = ApplicationConstants.SUCCESS;
					productExits = (Integer) resultFromProcedure.get("ProductExists");
					if (productExits != null)
					{
						if (productExits == 1)
						{
							responseFromProc = ApplicationConstants.WISHLISTADDPRODUCTEXISTS;
						}
						else
						{
							responseFromProc = ApplicationConstants.SUCCESS;
						}
					}
				}
				else
				{
					responseFromProc = ApplicationConstants.FAILURE;
				}
				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in addWishListProd method ..errorNum is..." + errorNum + "errorMsg..is " + errorMsg);
				}
			}
		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e.getMessage());
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

	/**
	 * The method for getting Hot Deal information.
	 * 
	 * @param userId
	 *            are the request parameters.
	 * @param productId
	 *            are the request parameters.
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 */
	@SuppressWarnings({ "unchecked", "null" })
	@Override
	public List<HotDealsDetails> getHotDealInfoForProduct(ThisLocationRequest objThisLocationRequest) throws ScanSeeException
	{
		final String methodName = "getHotDealInfoForProduct";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsDetails> hotDetailList = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WishListProductHotDealInformation");
			simpleJdbcCall.returningResultSet("hotDeals", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", objThisLocationRequest.getUserId());
			scanQueryParams.addValue("ProductId", objThisLocationRequest.getProductId());
			scanQueryParams.addValue("ZipCode", objThisLocationRequest.getZipcode());
			scanQueryParams.addValue("Latitude", objThisLocationRequest.getLatitude());
			scanQueryParams.addValue("Longitude", objThisLocationRequest.getLongitude());
			scanQueryParams.addValue("Radius", objThisLocationRequest.getRadius());
			
			//For user tracking
			scanQueryParams.addValue(ApplicationConstants.MAINMENUID, objThisLocationRequest.getMainMenuID());
			scanQueryParams.addValue("AlertedProductID", objThisLocationRequest.getAlertProdID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					hotDetailList = (List<HotDealsDetails>) resultFromProcedure.get("hotDeals");
					/*
					 * if (null != hotDetailList && !hotDetailList.isEmpty()) {
					 * log.info("Product found for the search"); objHotDeal =
					 * hotDetailList.get(0); }
					 */
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "in usp_WishListProductHotDealInformation" + errorNum + "errorMsg is " + errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDetailList;
	}

	/**
	 * The method for getting all wishList store information.
	 * 
	 * @param userId
	 *            are the request parameters.
	 * @param productId
	 *            are the request parameters.
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 */

	@SuppressWarnings("unchecked")
	public RetailersDetails getStoreDetail(ThisLocationRequest objThisLocationRequest)
			throws ScanSeeException
	{

		final String methodName = "getStoreDetail in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		RetailersDetails retailersDetailsObj = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WishListRetailerInfo");
			simpleJdbcCall.returningResultSet("StoreDetails", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource storetParameters = new MapSqlParameterSource();
			storetParameters.addValue("UserID", objThisLocationRequest.getUserId());
			storetParameters.addValue("ProductID", objThisLocationRequest.getProductId());
			storetParameters.addValue("Latitude", objThisLocationRequest.getLatitude());
			storetParameters.addValue("Longitude", objThisLocationRequest.getLongitude());
			storetParameters.addValue("ZipCode", objThisLocationRequest.getZipcode());
			storetParameters.addValue("Radius", objThisLocationRequest.getRadius());
			//For user tracking
			storetParameters.addValue(ApplicationConstants.MAINMENUID, objThisLocationRequest.getMainMenuID());
			storetParameters.addValue("AlertedProductID", objThisLocationRequest.getAlertProdID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(storetParameters);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final List<RetailerDetail> storeDetaillst = (List<RetailerDetail>) resultFromProcedure.get("StoreDetails");

					if (null != storeDetaillst && !storeDetaillst.isEmpty())
					{
						retailersDetailsObj = new RetailersDetails();
						retailersDetailsObj.setRetailerDetail(storeDetaillst);
					}

				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					log.error("Error occurred in getProductDetail Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getProductDetail", exception);
			return null;
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return retailersDetailsObj;
	}

	/**
	 * The method fetches WishList history Products.
	 * 
	 * @param userId
	 *            as the request parameter
	 * @param lowerlimit
	 *            as the request parameter
	 * @return Success or Failure depending upon the result.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<ProductDetail> getWishListHistoryDetails(Integer userId, Integer lowerlimit) throws ScanSeeException
	{
		final String methodName = "getWishListHistoryDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ProductDetail> productDetails = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WishListHistoryDisplay");
			simpleJdbcCall.returningResultSet("wistListItems", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource wishlisthistoryparams = new MapSqlParameterSource();
			wishlisthistoryparams.addValue(ApplicationConstants.USERID, userId);
			wishlisthistoryparams.addValue("LowerLimit", lowerlimit);
			wishlisthistoryparams.addValue("ScreenName", ApplicationConstants.WLHISTORYSCREENNAME);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(wishlisthistoryparams);
			productDetails = (ArrayList<ProductDetail>) resultFromProcedure.get("wistListItems");

			if (!productDetails.isEmpty() && productDetails != null)
			{
				final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
				if (nextpage != null)
				{
					if (nextpage)
					{
						productDetails.get(0).setNextPage(1);
					}
					else
					{
						productDetails.get(0).setNextPage(0);
					}
				}
			}
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in addWishListProd method ..errorNum ..." + errorNum + "errorMsg is" + errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * The method for getting product attributes information.
	 * 
	 * @param userId
	 *            are the request parameters.
	 * @param productId
	 *            are the request parameters.
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 */

	@SuppressWarnings({ "unchecked", "null" })
	@Override
	public ProductDetail fetchProductAttributeDetails(Integer userId, Integer productId) throws ScanSeeException
	{
		final String methodName = "fetchProductAttributeDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ProductDetail> productDetailslst = null;
		ProductDetail productDetailObj = null;

		ProductDetails productImageObj = null;

		final List<ProductDetails> productImageslst = new ArrayList<ProductDetails>();
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WishListProductAttributes");
			simpleJdbcCall.returningResultSet("productdetails", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("productid", productId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetailslst = (ArrayList<ProductDetail>) resultFromProcedure.get("productdetails");

				}
				if (null != productDetailslst && !productDetailslst.isEmpty())
				{
					productDetailObj = new ProductDetail();
					for (int i = 0; i < productDetailslst.size(); i++)
					{
						productImageObj = new ProductDetails();
						productImageObj.setProductMediaPath(productDetailslst.get(i).getProductMediaPath());
						productImageslst.add(productImageObj);

					}

					productDetailObj.setProductId(productDetailslst.get(0).getProductId());
					productDetailObj.setProductName(productDetailslst.get(0).getProductName());
					productDetailObj.setProductImagePath(productDetailslst.get(0).getProductImagePath());
					productDetailObj.setProductName(productDetailslst.get(0).getProductName());
					productDetailObj.setProductShortDescription(productDetailslst.get(0).getProductShortDescription());
					productDetailObj.setProductLongDescription(productDetailslst.get(0).getProductLongDescription());
					productDetailObj.setVideoFlag(productDetailslst.get(0).getVideoFlag());
					productDetailObj.setAudioFlag(productDetailslst.get(0).getAudioFlag());
					productDetailObj.setFileFlag(productDetailslst.get(0).getFileFlag());
					productDetailObj.setModelNumber(productDetailslst.get(0).getModelNumber());
					productDetailObj.setWarrantyServiceInfo(productDetailslst.get(0).getWarrantyServiceInfo());
					productDetailObj.setProductImageslst(productImageslst);
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "in usp_WishListProductHotDealInformation" + errorNum + "errorMsg.." + errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e.getMessage());
		}

		ProductAttributes productAttributesObj = null;
		final List<ProductAttributes> productAttributeslst = new ArrayList<ProductAttributes>();
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WishlistProductAttributeDisplay");
			simpleJdbcCall.returningResultSet("productAttributes", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("productid", productId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetailslst = (ArrayList<ProductDetail>) resultFromProcedure.get("productAttributes");

				}
				if (null != productDetailslst && !productDetailslst.isEmpty())
				{

					for (int i = 0; i < productDetailslst.size(); i++)
					{
						productAttributesObj = new ProductAttributes();
						productAttributesObj.setAttributeName(productDetailslst.get(i).getAttributeName());
						productAttributesObj.setDisplayValue(productDetailslst.get(i).getDisplayValue());
						productAttributeslst.add(productAttributesObj);

					}

					productDetailObj.setProductAttributeslst(productAttributeslst);
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "in usp_WishListProductHotDealInformation" + errorNum + "errorMsg.." + errorMsg);
				throw new ScanSeeException(errorMsg);

			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e.getMessage());
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetailObj;
	}

	/**
	 * This method fetch the coupon details from the database.
	 * 
	 * @param productId
	 *            as request parameter
	 * @param userId
	 *            as request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return couponDetailslst -coupon deatils from database
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<CouponDetails> fetchCouponDetails(ThisLocationRequest objThisLocationRequest) throws ScanSeeException
	{
		final String methodName = "fetchCouponDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<CouponDetails> couponDetailslst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WishListCouponDisplay");
			simpleJdbcCall.returningResultSet("wistListCoupons", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue(ApplicationConstants.USERID, objThisLocationRequest.getUserId());
			fetchProductDetailsParameters.addValue("ProductID", objThisLocationRequest.getProductId());
			fetchProductDetailsParameters.addValue("ZipCode", objThisLocationRequest.getZipcode());
			fetchProductDetailsParameters.addValue("Latitude", objThisLocationRequest.getLatitude());
			fetchProductDetailsParameters.addValue("Longitude", objThisLocationRequest.getLongitude());
			fetchProductDetailsParameters.addValue("Radius", objThisLocationRequest.getRadius());
			
			//For user tracking
			fetchProductDetailsParameters.addValue(ApplicationConstants.MAINMENUID, objThisLocationRequest.getMainMenuID());
			fetchProductDetailsParameters.addValue("AlertedProductID", objThisLocationRequest.getAlertProdID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			couponDetailslst = (ArrayList<CouponDetails>) resultFromProcedure.get("wistListCoupons");

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in fetchCouponDetails method in WishListDao ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return couponDetailslst;
	}

	/**
	 * The method for getting user zipcode.
	 * 
	 * @param userId
	 *            are the request parameters.
	 * @return The XML with user zipcode information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public WishListResultSet checkUserZipcode(Integer userId) throws ScanSeeException
	{
		final String methodName = "getWishListHistoryDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);
		WishListResultSet wishListResultSetObj = null;
		ArrayList<WishListResultSet> wishListResultSetlist = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WishListDisplayUserZipCodeCheck");
			simpleJdbcCall.returningResultSet("userZipcodelist", new BeanPropertyRowMapper<WishListResultSet>(WishListResultSet.class));

			final SqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource().addValue(ApplicationConstants.USERID, userId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			wishListResultSetlist = (ArrayList<WishListResultSet>) resultFromProcedure.get("userZipcodelist");

			if (null != wishListResultSetlist && !wishListResultSetlist.isEmpty())
			{
				wishListResultSetObj = new WishListResultSet();
				wishListResultSetObj.setZipcode(wishListResultSetlist.get(0).getZipcode());
				wishListResultSetObj.setZipCodeExists(wishListResultSetlist.get(0).getZipCodeExists());
			}

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in addWishListProd method ..errorNum..." + errorNum + "errorMsg is" + errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return wishListResultSetObj;
	}

	@Override
	public String deleteWLHistoryItem(ProductDetail productDetail) throws ScanSeeException
	{
		final String methodName = "deleteWishListItem";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Integer result = 1;

		simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
		simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
		simpleJdbcCall.withProcedureName("usp_WishListHistorytDelete");
		MapSqlParameterSource wishListDeleteParameters;
		try
		{
			wishListDeleteParameters = new MapSqlParameterSource();
			wishListDeleteParameters.addValue("UserProductID", productDetail.getUserProductId());
			wishListDeleteParameters.addValue(ApplicationConstants.USERID, productDetail.getUserId());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(wishListDeleteParameters);
			result = (Integer) resultFromProcedure.get(ApplicationConstants.DBSTATUS);
			if (result == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				response = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsgis = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "in usp_WishListDelete" + errorNum + "errorMsgis.." + errorMsgis);

				throw new ScanSeeException(errorMsgis);

			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}
