package com.scansee.wishlist.dao;

import java.util.ArrayList;
import java.util.List;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetailsRequest;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.ThisLocationRequest;
import com.scansee.common.pojos.WishListResultSet;

/**
 * The Interface for WishListDAO. ImplementedBy {@link WishListDAOImpl}
 * 
 * @author sowjanya_d
 */
public interface WishListDAO
{

	/**
	 * The method for deleting wish list item.
	 * 
	 * @param productDetail
	 *            request parameter
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String deleteWishListItem(ProductDetail productDetail) throws ScanSeeException;

	/**
	 * The method for getting WishList products.
	 * 
	 * @param objThisLocationRequest request parameter.
	 * @param strApplnConstant as request parameter.
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ArrayList<ProductDetail> getWishListItems(ThisLocationRequest objThisLocationRequest, String strApplnConstant)
			throws ScanSeeException;

	/**
	 * The method for getting WishList searched products.
	 * 
	 * @param userId
	 *            of user
	 * @param prodSearchKey
	 *            are the request parameters.
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 */
	WishListResultSet getWishListProductItems(int userId, String prodSearchKey) throws ScanSeeException;

	/**
	 * The method for adding unassigned product to WishList.
	 * 
	 * @param productDetailsRequest
	 *            is the request parameter.
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 */
	String addUnassignedPro(ProductDetailsRequest productDetailsRequest) throws ScanSeeException;

	/**
	 * The method for adding searched WishList product.
	 * 
	 * @param productDetailsRequest
	 *            is the request parameter.
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 */
	// ProductDetails addWishListProd(ProductDetailsRequest
	// productDetailsRequest)throws ScanSeeException;
	String addWishListProd(ProductDetailsRequest productDetailsRequest) throws ScanSeeException;

	/**
	 * The method for getting hot deal information.
	 * 
	 * @param userId
	 *            are the request parameters.
	 * @param productId
	 *            are the request parameters.
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 */

	List<HotDealsDetails> getHotDealInfoForProduct(ThisLocationRequest objThisLocationRequest) throws ScanSeeException;

	/**
	 * The method for getting all wishList store information.
	 * 
	 * @param userId
	 *            are the request parameters.
	 * @param productId
	 *            are the request parameters.
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 */
	RetailersDetails getStoreDetail(ThisLocationRequest objThisLocationRequest) throws ScanSeeException;

	/**
	 * The method for getting WishList history details.
	 * 
	 * @param userId
	 *            request parameter
	 * @param lowerlimit
	 *            as request parameter
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ArrayList<ProductDetail> getWishListHistoryDetails(Integer userId, Integer lowerlimit) throws ScanSeeException;

	/**
	 * The method for getting Product Attribute information.
	 * 
	 * @param userId
	 *            are the request parameters.
	 * @param productId
	 *            are the request parameters.
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 */
	ProductDetail fetchProductAttributeDetails(Integer userId, Integer productId) throws ScanSeeException;

	/**
	 * The method for getting all wishList store information.
	 * 
	 * @param userId
	 *            are the request parameters.
	 * @param productId
	 *            are the request parameters.
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 */
	ArrayList<CouponDetails> fetchCouponDetails(ThisLocationRequest objThisLocationRequest)
			throws ScanSeeException;

	/**
	 * The method for getting user zipcode information.
	 * 
	 * @param userId
	 *            of user
	 * @return The XML with fetched information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @throws ScanSeeException
	 */
	WishListResultSet checkUserZipcode(Integer userId) throws ScanSeeException;
	
	/**
	 * The method for deleting wish list item.
	 * 
	 * @param productDetail
	 *            request parameter
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String deleteWLHistoryItem(ProductDetail productDetail) throws ScanSeeException;
}
