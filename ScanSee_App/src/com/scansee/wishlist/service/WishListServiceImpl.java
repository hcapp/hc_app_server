package com.scansee.wishlist.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.helper.BaseDAO;
import com.scansee.common.helper.XstreamParserHelper;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetailsRequest;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.ThisLocationRequest;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.common.pojos.WishListHistoryDetails;
import com.scansee.common.pojos.WishListProducts;
import com.scansee.common.pojos.WishListResultSet;
import com.scansee.common.util.Utility;
import com.scansee.firstuse.dao.FirstUseDAO;
import com.scansee.wishlist.dao.WishListDAO;

/**
 * This class is used to display user wish list items depending on retailers.
 * 
 * @author sowjanya_d
 */

public class WishListServiceImpl implements WishListService
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(WishListServiceImpl.class);
	/**
	 * Instance variable for Wish list DAO instance.
	 */
	/**
	 * Instance variable for Wish List DAO instance.
	 */
	private WishListDAO wishListDao;

	/**
	 * Instance variable for Base DAO instance.
	 */
	private BaseDAO baseDao;

	/**
	 * Variable type for FirstUse
	 */
	private FirstUseDAO firstUseDao;
	
	/**
	 * sets the Base DAO.
	 * 
	 * @param baseDao
	 *            The instance for BaseDAO
	 */

	public void setBaseDao(BaseDAO baseDao)
	{
		this.baseDao = baseDao;
	}

	/**
	 * sets the Wish List DAO.
	 * 
	 * @param wishListDao
	 *            The instance for WishListDAO
	 */

	public void setWishListDao(WishListDAO wishListDao)
	{
		this.wishListDao = wishListDao;
	}
	
	/**
	 * Setter method for FirstUseDAO.
	 * 
	 * @param firstUseDAO
	 *            the object of type FirstUseDAO
	 */
	public void setFirstUseDao(FirstUseDAO firstUseDao)
	{
		this.firstUseDao = firstUseDao;
	}

	/**
	 * the method fetches Wish List Product information.
	 * 
	 * @param productId
	 *            as request parameter
	 * @param userId
	 *            as request parameter.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String fetchProductDetails(Integer productId, Integer userId) throws ScanSeeException
	{

		final String methodName = "fetchProductDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final int retailId = 0;
		if (productId == null || userId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{
			
			final ProductDetail productDetail = baseDao.getProductDetail(userId, productId, retailId, null, null, null, null);

			if (null != productDetail)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetail);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method calls XStream Helper class method and parses the XML. The
	 * result is passed to Wish List DAO method.
	 * 
	 * @param xml
	 *            the Input XML.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String deleteWishListItem(String xml) throws ScanSeeException
	{

		final String methodName = "deleteWishListItem";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ProductDetail productDetail = (ProductDetail) streamHelper.parseXmlToObject(xml);

		if (productDetail.getUserProductId() == null || productDetail.getUserId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{

			response = wishListDao.deleteWishListItem(productDetail);

			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				LOG.info("Wish List product deleted with the UserId:" + productDetail.getUserId() + "and UserProductID"
						+ productDetail.getUserProductId());
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.REMOVE);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method calls XStream Helper class method and parses the XML. The
	 * result is passed to WishListDAO method
	 * 
	 * @param xml
	 *            the Input XML.
	 * @return returns the an XML response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("static-access")
	@Override
	public String getWishListItems(String xml) throws ScanSeeException
	{
		final String methodName = "getWishListItems";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		ArrayList<ProductDetail> arProdDetailList = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		Integer iNextPage = null;
		Integer iMaxCount = null;
		final ThisLocationRequest objThisLocationRequest = (ThisLocationRequest) streamHelper.parseXmlToObject(xml);
		if (objThisLocationRequest.getUserId() == null 
				/*|| ((objThisLocationRequest.getLatitude() == null) && (objThisLocationRequest.getZipcode() == null) )
				|| ((objThisLocationRequest.getLongitude() == null ) && (objThisLocationRequest.getZipcode() == null))*/
				)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		} else {
			//For user tracking. To get mainMenuID from moduleID
			Integer mainMenuID = null;
			if (objThisLocationRequest.getMainMenuID() == null) {
				UserTrackingData objUserTrackingData = new UserTrackingData();
				objUserTrackingData.setUserID(objThisLocationRequest.getUserId());
				objUserTrackingData.setModuleID(objThisLocationRequest.getModuleID());
				objUserTrackingData.setLatitude(objThisLocationRequest.getLatitude());
				objUserTrackingData.setLongitude(objThisLocationRequest.getLongitude());
				objUserTrackingData.setPostalCode(objThisLocationRequest.getZipcode());
				mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);
				objThisLocationRequest.setMainMenuID(mainMenuID);
			} else {
				mainMenuID = objThisLocationRequest.getMainMenuID();
			}
			arProdDetailList = wishListDao.getWishListItems(objThisLocationRequest, ApplicationConstants.WISHLISTSEARCHSCREEN);

			WishListProducts wishLisProduct = null;
			if (null != arProdDetailList && !arProdDetailList.isEmpty())
			{
				iNextPage = arProdDetailList.get(0).getNextPage();
				iMaxCount = arProdDetailList.get(0).getMaxCount();
				final WishListHelper wishlisthelperObj = new WishListHelper();
				
				wishLisProduct = wishlisthelperObj.getWishListDetails(arProdDetailList);
				wishLisProduct.setMainMenuID(mainMenuID);
				wishLisProduct.setNextPage(iNextPage);
				wishLisProduct.setMaxCount(iMaxCount);
			}

			if (null != wishLisProduct)
			{
				response = XstreamParserHelper.produceXMlFromObject(wishLisProduct);
				response = response.replaceAll("<productInfo>", "<ProductInfo>");
				response = response.replaceAll("</productInfo>", "</ProductInfo>");
				response = response.replaceAll("<alertProducts>", "<AlertedProducts>");
				response = response.replaceAll("</alertProducts>", "</AlertedProducts>");
				response = response.replaceAll("<productDetail>", "<ProductDetails>");
				response = response.replaceAll("</productDetail>", "</ProductDetails>");
			} else {
				String strMMId = null;
				if(null != mainMenuID)	{
					strMMId = mainMenuID.toString();
				} else	{
					strMMId = ApplicationConstants.STRING_ZERO;
				}
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND,	ApplicationConstants.NOPRODUCTFOUNDTEXT, "mainMenuID", strMMId);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method calls XStream Helper class method and parses the XML. The
	 * result is passed to WishListDAO method
	 * 
	 * @param userId
	 *            The userId in the request.
	 * @param productSearchkey
	 *            are the request parameter.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getWishListProductItems(Integer userId, String productSearchkey) throws ScanSeeException
	{
		final String methodName = "getWishListProductItems";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		WishListResultSet wishListResultSet;

		if (userId == null || Utility.isEmptyOrNullString(productSearchkey))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{

			wishListResultSet = wishListDao.getWishListProductItems(userId, productSearchkey);

			if (null != wishListResultSet)
			{
				response = XstreamParserHelper.produceXMlFromObject(wishListResultSet);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOWISHLISTPRODUCTTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method calls XStream Helper class method and parses the XML. The
	 * result is passed to WishListDAO method
	 * 
	 * @param xml
	 *            .The input XML.
	 * @return returns unassigned product added SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @return
	 */
	@Override
	public String addUnassigedProd(String xml) throws ScanSeeException
	{
		final String methodName = "addUnassigedProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ProductDetailsRequest productDetailsRequest = (ProductDetailsRequest) streamHelper.parseXmlToObject(xml);
		if (productDetailsRequest.getProductName() == null || productDetailsRequest.getUserId() == null
				|| productDetailsRequest.getProductDescription() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			response = wishListDao.addUnassignedPro(productDetailsRequest);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.ADDUNASSIGNEDPRODUCT);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * The method calls XStream Helper class method and parses the XML. The
	 * result is passed to WishListDAO method
	 * 
	 * @param xml
	 *            as the request.
	 * @return returns searched wishList product added SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String addWishListProd(String xml) throws ScanSeeException
	{

		final String methodName = "addWishListProd";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;

		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ProductDetailsRequest productDetailsRequest = (ProductDetailsRequest) streamHelper.parseXmlToObject(xml);

		if (productDetailsRequest.getUserId() == null)
		{

			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else if (!productDetailsRequest.getProductDetails().isEmpty() && productDetailsRequest.getProductDetails() != null)
		{

			for (int i = 0; i < productDetailsRequest.getProductDetails().size(); i++)
			{

				if (productDetailsRequest.getProductDetails().get(i).getProductId() == null)

				{
					response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
					return response;
				}
			}

			response = wishListDao.addWishListProd(productDetailsRequest);
			if (response != null)
			{
				if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
				{

					response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.WISHLISTPRODUCTADDED);
				}
				else if (ApplicationConstants.WISHLISTADDPRODUCTEXISTS.equalsIgnoreCase(response))
				{

					response = Utility.formResponseXml(ApplicationConstants.DUPLICATEPRODUCTCODE, ApplicationConstants.WISHLISTADDPRODUCTEXISTS);
				}
				else
				{
					response = Utility
							.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
				}
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * To get hot deals information for the product.
	 * 
	 * @param userId
	 *            as request parameter
	 * @param productId
	 *            as request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return response as success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getHotDealInfoForProduct(String xml) throws ScanSeeException
	{
		final String methodName = "getWishListProductItems";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		List<HotDealsDetails> hotDealsDetailslst = null;

		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ThisLocationRequest objThisLocationRequest = (ThisLocationRequest) streamHelper.parseXmlToObject(xml);
		
		if (objThisLocationRequest.getUserId() == null || objThisLocationRequest.getProductId() == null || Utility.isEmpty(objThisLocationRequest.getZipcode()))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			hotDealsDetailslst = wishListDao.getHotDealInfoForProduct(objThisLocationRequest);
			
			if (null != hotDealsDetailslst && !hotDealsDetailslst.isEmpty())
			{
				//Changing date format to display
				for(HotDealsDetails hdDetails : hotDealsDetailslst)	{
					hdDetails.setIsDateFormated(false);
					hdDetails.sethDStartDate(hdDetails.gethDStartDate());
					hdDetails.sethDEndDate(hdDetails.gethDEndDate());
				}
				
				response = XstreamParserHelper.produceXMlFromObject(hotDealsDetailslst);

				response = response.replaceAll("<list>", "<WishLstProdHotDeals>");
				response = response.replaceAll("</list>", "</WishLstProdHotDeals>");
				// if user views the product hot deal information need to update
				// the push notification flag.
				final String updateResposne = baseDao.upDatePushNotFlag(objThisLocationRequest.getProductId(), objThisLocationRequest.getUserId());
				if (null != updateResposne && ApplicationConstants.FAILURE.equals(updateResposne))
				{
					// in case update failure. sending failure message
					response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
				}
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * the method fetches all the WishList store information.
	 * 
	 * @param userId
	 *            as the request.
	 * @param productId
	 *            as request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String getAllWishListStoreDetails(String xml) throws ScanSeeException
	{
		final String methodName = "getAllStoreDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ThisLocationRequest objThisLocationRequest = (ThisLocationRequest) streamHelper.parseXmlToObject(xml);

		if (objThisLocationRequest.getProductId() == null || objThisLocationRequest.getUserId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{

			final RetailersDetails wishListRetailDetail = wishListDao.getStoreDetail(objThisLocationRequest);

			if (null != wishListRetailDetail)
			{
				response = XstreamParserHelper.produceXMlFromObject(wishListRetailDetail);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}

			final String updateResposne = baseDao.upDatePushNotFlag(objThisLocationRequest.getProductId(), objThisLocationRequest.getUserId());
			if (null != updateResposne && ApplicationConstants.FAILURE.equals(updateResposne))
			{
				// in case update failure. sending failure message
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This method for fetching wish list history details based on user.
	 * 
	 * @param userId
	 *            The userId in the request.
	 * @param lowerlimit
	 *            as input request parameter
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("static-access")
	@Override
	public String fetchWishListHistoryDetails(Integer userId, Integer lowerlimit) throws ScanSeeException
	{
		final String methodName = "fetchWishListHistoryDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		ArrayList<ProductDetail> productDetails = null;
		WishListHistoryDetails wishListHistoryDetails = null;

		if (userId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{

			if (lowerlimit == null)
			{
				lowerlimit = 0;
			}
			productDetails = wishListDao.getWishListHistoryDetails(userId, lowerlimit);

			if (null != productDetails && !productDetails.isEmpty())
			{
				final WishListHelper wishlisthelperObj = new WishListHelper();

				wishListHistoryDetails = wishlisthelperObj.getWishListHistory(productDetails);
				wishListHistoryDetails.setNextPage(productDetails.get(0).getNextPage());

			}

			if (null != wishListHistoryDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(wishListHistoryDetails);
				response = response.replaceAll("<WishListResultSet>", "<ProductHistoryInfo>");
				response = response.replaceAll("</WishListResultSet>", "</ProductHistoryInfo>");
				response = response.replaceAll("<productDetail>", "<ProductDetails>");
				response = response.replaceAll("</productDetail>", "</ProductDetails>");
				response=StringEscapeUtils.unescapeXml(response);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method fetches product attributes information.
	 * 
	 * @param userId
	 *            as the request.
	 * @param productId
	 *            as request parameter
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String fetchProductAttributes(Integer userId, Integer productId) throws ScanSeeException
	{
		final String methodName = "fetchProductAttributes";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		ProductDetail productDetailsObj = null;

		if (userId == null || productId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{
			productDetailsObj = wishListDao.fetchProductAttributeDetails(userId, productId);

			if (null != productDetailsObj)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetailsObj);

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * To fetch the coupon details from the database.
	 * 
	 * @param userId
	 *            as request parameter
	 * @param productId
	 *            as request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String fetchCouponDetails(String xml) throws ScanSeeException
	{
		final String methodName = "getWishListItems";
		LOG.info("In Service..." + ApplicationConstants.METHODSTART + methodName);
		String response = null;
		ArrayList<CouponDetails> couponDetailslst = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ThisLocationRequest objThisLocationRequest = (ThisLocationRequest) streamHelper.parseXmlToObject(xml);
		
		if (objThisLocationRequest.getUserId() == null || objThisLocationRequest.getProductId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			if("".equals(objThisLocationRequest.getZipcode()))
			{
				objThisLocationRequest.setZipcode(null);
			}
			
			couponDetailslst = wishListDao.fetchCouponDetails(objThisLocationRequest);

			if (couponDetailslst != null && !couponDetailslst.isEmpty())
			{
				response = XstreamParserHelper.produceXMlFromObject(couponDetailslst);

				response = response.replaceAll("<list>", "<CouponInfo>");
				response = response.replaceAll("</list>", "</CouponInfo>");

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method used to fetch user zipcode. The method calls XStream Helper
	 * class method and parses the XML. The result is passed to WishListDAO
	 * method
	 * 
	 * @param userId
	 *            The userId in the request.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String checkUserZipcode(Integer userId) throws ScanSeeException
	{
		final String methodName = "checkUserZipcode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		WishListResultSet wishListResultSetObj = null;
		String response = null;
		if (userId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);

		}
		else
		{
			wishListResultSetObj = wishListDao.checkUserZipcode(userId);

			if (null != wishListResultSetObj)
			{
				response = XstreamParserHelper.produceXMlFromObject(wishListResultSetObj);

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String deleteWLHistoryItem(String xml) throws ScanSeeException
	{
		final String methodName = "deleteWishListItem";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ProductDetail productDetail = (ProductDetail) streamHelper.parseXmlToObject(xml);

		if (productDetail.getUserProductId() == null || productDetail.getUserId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{

			response = wishListDao.deleteWLHistoryItem(productDetail);

			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				LOG.info("Wish List product deleted with the UserId:" + productDetail.getUserId() + "and UserProductID"
						+ productDetail.getUserProductId());
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.REMOVE);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}
