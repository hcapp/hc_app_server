package com.scansee.wishlist.service;

import com.google.inject.ImplementedBy;
import com.scansee.common.exception.ScanSeeException;

/**
 * The WishListService Interface. {@link ImplementedBy}
 * {@link WishListServiceImpl}
 * 
 * @author dileepa_cc
 */
public interface WishListService
{

	/**
	 * This method fetches wish list product information.
	 * 
	 * @param productId
	 *            the request parameter
	 * @param userId
	 *            the request parameter.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String fetchProductDetails(Integer productId, Integer userId) throws ScanSeeException;

	/**
	 * The method calls JAXB Helper class method and parses the XML. The result
	 * is passed to Wish List DAO method.
	 * 
	 * @param xml
	 *            the Input XML.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String deleteWishListItem(String xml) throws ScanSeeException;

	/**
	 * For displaying Wish List Items based on userId.
	 * 
	 * @param userId
	 *            as request
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return XML
	 * @throws ScanSeeException
	 *             for exception
	 */
	String getWishListItems(String xml) throws ScanSeeException;

	/**
	 * For displaying Wish List searched products based on userId and
	 * prodSearchKey.
	 * 
	 * @param userId
	 *            of the user
	 * @param prodSearchKey
	 *            as request
	 * @return XML
	 * @throws ScanSeeException
	 *             for exception
	 */
	String getWishListProductItems(Integer userId, String prodSearchKey) throws ScanSeeException;

	/**
	 * To add unassigned product to Wish List.
	 * 
	 * @param xml
	 *            as request
	 * @return added success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */
	String addUnassigedProd(String xml) throws ScanSeeException;

	/**
	 * To add searched product to Wish List.
	 * 
	 * @param xml
	 *            as request
	 * @return added success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */
	String addWishListProd(String xml) throws ScanSeeException;

	/**
	 * To get hotdeals information for the product.
	 * 
	 * @param userId
	 *            As request parameter
	 * @param productId
	 *            As request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getHotDealInfoForProduct(String xml) throws ScanSeeException;

	/**
	 * the method fetches all the WishList store information.
	 * 
	 * @param userId
	 *            as the request.
	 * @param productId
	 *            as request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String getAllWishListStoreDetails(String xml) throws ScanSeeException;

	/**
	 * For displaying Wish List History details based on userId.
	 * 
	 * @param userId
	 *            as request
	 * @param lowerLimit
	 *            as request parameter * @return XML
	 * @throws ScanSeeException
	 *             for exception
	 */
	String fetchWishListHistoryDetails(Integer userId, Integer lowerLimit) throws ScanSeeException;

	/**
	 * the method fetches product Attributes information.
	 * 
	 * @param userId
	 *            as the request.
	 * @param productId
	 *            as request parameter
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String fetchProductAttributes(Integer userId, Integer productId) throws ScanSeeException;

	/**
	 * For displaying Wish List Items based on userId.
	 * 
	 * @param userId
	 *            as request
	 * @param productId
	 *            as request parameter
	 * @param zipcode
	 *            as request parameter
	 * @param latitude
	 *            as request parameter
	 * @param longitude
	 *            as request parameter
	 * @param radius
	 *            as request parameter
	 * @return XML
	 * @throws ScanSeeException
	 *             for exception
	 */
	String fetchCouponDetails(String xml)
			throws ScanSeeException;

	/**
	 * This method fetches user location zip code.
	 * 
	 * @param userId
	 *            the request parameter.
	 * @return returns the an XML containing user zipcode.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String checkUserZipcode(Integer userId) throws ScanSeeException;
	
	/**
	 * The method calls JAXB Helper class method and parses the XML. The result
	 * is passed to Wish List DAO method.
	 * 
	 * @param xml
	 *            the Input XML.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String deleteWLHistoryItem(String xml) throws ScanSeeException;

}
