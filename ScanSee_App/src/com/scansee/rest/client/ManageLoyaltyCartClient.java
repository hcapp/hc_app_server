//package com.scansee.rest.client;

import javax.ws.rs.core.MediaType;
import org.jboss.resteasy.client.ClientRequest;

public class ManageLoyaltyCartClient
{

		public static void main(String args[])
		{
			addloyaltycard();
			//removeloyaltycard();
		}

		static void addloyaltycard()
		{
		//	ScanSee1.4.0/manageloyaltycard/removeloyaltycard 
			final String getMyGalleryInfo = "http://localhost:8080/ScanSee1.4.0/manageloyaltycard/addloyaltycard";
			final ClientRequest request = new ClientRequest(getMyGalleryInfo);
			
			//"?><AddRemoveCLR><userId>3222</userId><loyaltyDealId>472</loyaltyDealId><merchantId>714</merchantId></AddRemoveCLR>
			final String addloyaltycard="<CFRequest><retailId>632</retailId><merchantId>444</merchantId><cardNumber><![CDATA[" + "sdfjs^^dfz%%%s00@@@df" + "]]></cardNumber><referenceId>3280</referenceId></CFRequest>";

			request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, addloyaltycard);
			request.getHeaders();
			try
			{
				final String response = request.postTarget(String.class);
				System.out.println("response " + response);
			}
			catch (Exception e)
			{
				System.out.println("exception in scansee rest easy client..");
			}

		}
		
		static void removeloyaltycard()
		{
		//	ScanSee1.4.0/manageloyaltycard/removeloyaltycard 
			final String getMyGalleryInfo = "http://localhost:8080/ScanSee1.4.0/manageloyaltycard/removeloyaltycard";
			final ClientRequest request = new ClientRequest(getMyGalleryInfo);
			//<CFRequest><retailId>708</retailId><merchantId>517</merchantId><cardNumber>706000004457</cardNumber><referenceId>3280</referenceId></CFRequest>
			final String removeloyaltycard="<CFRequest><retailId>708</retailId><merchantId>517</merchantId><cardNumber>706000004457</cardNumber><referenceId>3280</referenceId></CFRequest>";

			request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, removeloyaltycard);
			request.getHeaders();
			try
			{
				final String response = request.postTarget(String.class);
				System.out.println("response " + response);
			}
			catch (Exception e)
			{
				System.out.println("exception in scansee rest easy client..");
			}

		}
}
