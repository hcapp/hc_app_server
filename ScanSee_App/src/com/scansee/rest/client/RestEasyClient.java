/*package com.scansee.rest.client;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;

public class RestEasyClient
{
	
	 * //String Responseis=null; //String getWishListItems =
	 * "http://localhost:8080/ScanSee/wishlist/getWishListItems?USERID=1";
	 * Responseis=
	 * "<WishListResultSet><productDetail><ProductDetail><userProductId>112</userProductId><productId>13</productId><productName>Nestle Active Life Water</productName><userId>1</userId></ProductDetail><ProductDetail><userProductId>113</userProductId><productId>14</productId><productName>Nestle Right Water</productName><userId>1</userId></ProductDetail><ProductDetail><userProductId>134</userProductId><productId>0</productId><productName>NonAvailableProdcut</productName><userId>1</userId></ProductDetail></productDetail></WishListResultSet>"
	 * ; String fetchproductdetails=
	 * "http://localhost:8080/ScanSee/wishlist/fetchproductdetails?productId=1";
	 * Responseis=
	 * "<ProductDetail><productId>1</productId><productName>Froot Loops</productName><productShortDescription>Kellogg's Fruit Loops 17 oz package</productShortDescription><productLongDescription>Kelloggs Fruit Loops 17 oz package</productLongDescription><manufacturersName>Kellogg's</manufacturersName><imagePath>images/frootloops.jpg</imagePath><modelNumber>N/A</modelNumber><suggestedRetailPrice>N/A</suggestedRetailPrice><productWeight>17.0</productWeight><productExpDate>2011-12-31 00:00:00.0</productExpDate><weightUnits>Ounces</weightUnits><productVideoUrl>images/MVI_1808.avi</productVideoUrl></ProductDetail>"
	 * ; StringaddUnassignedProduct=
	 * ": http://serverIP:8080/ScanSee/shoppingList/addunassignedProd ";
	 

	
	 * String requestXml="<ProductDetailsRequest>" + "<userId></userId>" +
	 * "<productName></productName>" +
	 * "<productDescription></productDescription>" + "</ProductDetailsRequest>";
	 

	String requestXml = "<WishListResultSet>" + "<userProductId>14</userProductId>" + "<userId>1</userId>" + "</WishListResultSet>";

	// http://serverIP:8080/ScanSee/wishlist/deletewishlistitem

	public static void main(String[] args)
	{
		String urlSaveUser = "http://localhost:8080/ScanSee/firstUse/saveUser";
		String auth = "http://localhost:8080/ScanSee/firstUse/authenticate";
		String thisLoc = "http://localhost:8080/ScanSee/thisLocation/getRetailersInfo";
		String productDetails = "http://localhost:8080/ScanSee/thisLocation/getProducts";
		String cateDetails = "http://localhost:8080/ScanSee/thisLocation/getCategoryInfo";
		String addUnass = "http://10.11.204.10:8080/ScanSee/shoppingList/addunassignedProd";
		String addSl = "http://localhost:8080/ScanSee/shoppingList/addproducttosl";
		String deletesl = "http://10.11.204.10:8080/ScanSee/shoppingList/deleteslproduct";
		String addDelSC = "http://10.11.204.10:8080/ScanSee/shoppingList/addremcart";
		String addDelSB = "http://10.11.204.10:8080/ScanSee/shoppingList/addrembasket";
		String shareProd = "http://localhost:8080/ScanSee/shoppingList/shareProductInfo";
		String fetchproductdetails = "http://localhost:8080/ScanSee/wishlist/fetchproductdetails";
		String deletewishlistitem = "http://localhost:8080/ScanSee/wishlist/deletewishlistitem";
		String wishDetails = "http://localhost:8080/ScanSee/wishlist/getwishlistitems";
		String wishProductDetails = "http://localhost:8080/ScanSee/wishlist/getwishlistproductitems";
		String addUnassWishList = "http://localhost:8080/ScanSee/shoppingList/addunassignedProd";
		String addExistProdToWishList = "http://localhost:8080/ScanSee/wishlist/addwishlistprod";
		String updatePushNotifyVal = "http://localhost:8080/ScanSee/firstuse/pushnotifyalert";
		String fetchUserInfo = "http://localhost:8080/ScanSee/managesettings/fetchuserinfo";
		String insertUserInfo = "http://localhost:8080/ScanSee/managesettings/insertuserinfo";

		String saveUserInfo = "http://localhost:8080/ScanSee/firstUse/saveUser";
		String ScanUrl = "http://localhost:8080/ScanSee/scanNow/getProdForBarCode";
		String SUrl = "http://localhost:8080/ScanSee/scanNow/getProdForName";
		
		String fetchAccountDetails = "http://localhost:8080/ScanSee/myaccount/fetchpaymentinfo";

		

		String register = "http://localhost:8080/ScanSee/firstUse/saveusermedia";

		String pushnotify = "http://localhost:8080/ScanSee/firstUse/pushnotifyalert?USERID=1&pushNotify=yes";
		// ClientRequest request = new ClientRequest(register);

		String hotdeals1 = "http://localhost:8080/ScanSee/hotDeals/removeHdProd";
		StringBuilder sb = new StringBuilder();
		sb.append("<AuthenticateUser>");
		sb.append("<email>ramanan_k@spanservices.com</email>");
		sb.append("<password>A</password>");
		sb.append("</AuthenticateUser>");

		
		
		
		
		*//**
		 * Shopping List Module Interactions
		 *//*
		
		// Add Product To Master Shopping List
		String addproducttoshoplist = "http://localhost:8080/ScanSee/shoppingList/addproducttosl";
		String addproducttoshoplistInputXml = "<AddShoppingList>" + "<userId>1</userId>" + "<productDetails>" + "<ProductDetail>"
				+ "<productId>10</productId>" + "<productName>Sunsilk Shampoo</productName>" + "</ProductDetail>" + "<ProductDetail>"
				+ "<productId>13</productId>" + "<productName>Dove Shampoo</productName>" + "</ProductDetail>" + "</productDetails>"
				+ "</AddShoppingList>";

		// Add Unassigned Product
		String addunassignedproduct = "http://localhost:8080/ScanSee/shoppingList/addunassignedProd";
		String addunassignedproductInputXml = "<ProductDetailsRequest>" + "<userId>109</userId>" + "<productName>NonAvailableProdcut</productName>"
				+ "<productDescription>testing the unassigned</productDescription>" + "</ProductDetailsRequest>";

		// Add/Remove Today's Shopping List Products
		String addremcart = "http://localhost:8080/ScanSee/shoppingList/addremcart";
		String addremcartInputXml = "<AddShoppingCart>" 
			+ "<userId>186</userId>" + "<productDetails>" 
			+ "<ProductDetail>"
				+ "<userProductId>150</userProductId>" +
						"<productId>14</productId>" +
						"	<productName>Sunsilk Shampoo</productName>" +
						"</ProductDetail>" 
			+  "</productDetails>"+ "<isaddToTSL>true</isaddToTSL>" +
				 "</AddShoppingCart>";
		
//"<isaddToTSL>false</isaddToTSL>" +
		// Add/Remove Basket
		String addrembasket = "http://localhost:8080/ScanSee/shoppingList/addrembasket";
		String addrembasketInputXml = "<AddRemoveSBProducts>" + "<userId>1</userId>" + "<cartProducts>" + "<ProductDetail>"
				+ "<userProductId>111</userProductId>" + "</ProductDetail>" + "<ProductDetail>" + "<userProductId>113</userProductId>"
				+ "</ProductDetail>" + "</cartProducts>" + "<basketProducts>" + "<ProductDetail>" + "<userProductId>112</userProductId>"
				+ "</ProductDetail>" + "</basketProducts>" + "</AddRemoveSBProducts>";

		// Get Master Shopping List Products
		String getShoppingListItems = "http://localhost:8080/ScanSee/shoppingList/getShoppingListItems?USERID=1";

		// Fetch Today's Shopping List Products
		String getscproducts = "http://localhost:8080/ScanSee/shoppingList/getscproducts?userId=1";

		// Fetch Shopping Cart Products
		String getsbproducts = "http://localhost:8080/ScanSee/shoppingList/getsbproducts?userId=1";

		// Delete MasterShopping List Product
		String deleteslproduct = "http://localhost:8080/ScanSee/shoppingList/deleteslproduct";
		String deleteslproductInputXml = "<AddShoppingCart>" + "<userId>1</userId>" + "<productDetails>" + "<ProductDetail>"
				+ "<userProductId>111</userProductId>" + "</ProductDetail>" + "<ProductDetail>" + "<userProductId>112</userProductId>"
				+ "</ProductDetail>" + "</productDetails>" + "</AddShoppingCart>";

		//String saveuserpreference = "http://localhost:8080/ScanSee/managesettings/savesettings";
		String requestXml = "<ProductDetailsRequest>" + "<userId>2</userId>" + "<productName>NewProduct</productName>"
				+ "<productDescription>NewProductDesc</productDescription>" + "</ProductDetailsRequest>";
		String addUnassignedProduct = "http://localhost:8080/ScanSee/shoppingList/addunassignedProd";
		String requestXml2 = "<HotDealsListRequest>" + "<userId>1</userId>" + "<hotDealId>2</hotDealId>" + "<hDInterested>1</hDInterested>"
				+ "</HotDealsListRequest>";
		requestXml = "<WishListResultSet>" + "<userProductId>112</userProductId>" + "<userId>1</userId>" + "</WishListResultSet>";

		String removeWLitem = "http://localhost:8080/ScanSee/wishlist/deletewishlistitem";
		

		

		
		
		*//**
		 * End of Shopping List Interactions
		 *//*
		
	
		*//**
		 * MY Account Interactions
		 *//*
		// Fetch Myaccount Details
		String fetchMyAccountDetails = "http://localhost:8080/ScanSee/myaccount/myaccountdetails";
		String fetchMyAccountDetailsInputXml = "<AuthenticateUser>" + "<userId>2</userId>" + "<password>A</password>" + "</AuthenticateUser>";

		// Forgot Password
		String forgotPassword = "http://localhost:8080/ScanSee/myaccount/forgotpassword";

		
		//To fetch User Payment Preferences info
		String fetchuserpaymentinfo="http://localhost:8080/ScanSee/myaccount/fetchuserpaymentinfo?userdId=1";
		
		//To save User Paymnet Preferences info
		String saveuserpaymentpreference = "http://localhost:8080/ScanSee/myaccount/savesettings";
		String saveuserpaymentpreferenceinputXml = "<UserPreference>" + "<userID>4</userID>" + "<defaultPayout>5000.55</defaultPayout>"
		+ "<useDefaultAddress>1</useDefaultAddress>" + "<payAddress1>address 1111</payAddress1>" + "<payAddress2>address 2222</payAddress2>"
		+ "<payCity>Bangalore</payCity>" + "<payState>Karanata</payState>" + "<payPostalCode>56454554</payPostalCode>"
		+ "<provisionalPayoutActive>1</provisionalPayoutActive>" + "<provisionalPayoutStartDate>2011-06-06 00:00:00</provisionalPayoutStartDate>"
		+ "<provisionalPayoutEndDate>2011-10-12 00:00:00</provisionalPayoutEndDate>" + "<paymentIntervalID>2</paymentIntervalID>"
		+ "<paymentTypeID>1</paymentTypeID>" + "<payoutTypeID>1</payoutTypeID>" + "</UserPreference>";
		
		String input = "<UserPreference>" + "<userID>185</userID>" + "<localeRadius>200</localeRadius>" + "<scannerSilent>1</scannerSilent>"
		+ "<displayCoupons>0</displayCoupons>" + "<displayRebates>0</displayRebates>" + "<displayLoyaltyRewards>1</displayLoyaltyRewards>"
		+ "<savingsActivated>1</savingsActivated>" + "<sleepStatus>1</sleepStatus>" 
		+ "<dateModified>2011-10-12 02:32:23</dateModified>" 
		+ "</UserPreference>";
		*//**
		 * End MY Account Interactions
		 *//*
		
		String saveuserpreference = "http://10.11.202.76:8080/ScanSee/managesettings/savesettings";
		ClientRequest request = new ClientRequest(saveuserpreference);
		
		
		
		// adding shopping list
		
		 * String inputXml = "<AddShoppingList>" + "<userId>1</userId>" +
		 * "<productDetails>" + "<ProductDetail>" + "<productId>8</productId>" +
		 * "<productName>Froot Loops </productName>" +
		 * "<userProductId>27</userProductId>"+ "</ProductDetail>" +
		 * "</productDetails>" + "</AddShoppingList>"; String
		 * inputXml="<RemoveWishListResultSet>"+
		 * "<userProductId>13</userProductId>" + "<userId>1</userId>"+
		 * "</RemoveWishListResultSet>";
		 

		
		 * inputXml="<HotDealsListRequest>"+ "<userId>1</userId>"+
		 * "<showType>null</showType>"+ "<sortType>c</sortType>"+
		 * "<searchItem>null</searchItem>"+
		 * "<lastVisitedProductNo>0</lastVisitedProductNo>"+
		 * "</HotDealsListRequest>"; inputXml="<HotDealsListRequest>"+
		 * "<userId>1</userId>"+ "<hotDealId>2</hotDealId>"+
		 * "<hDInterested>0</hDInterested>"+ "</HotDealsListRequest>"
		 

		
		 * String inputXml = "<ProductDetailsRequest>" + "<userId>10</userId>" +
		 * "<productName>NonAvailableProdcut</productName>" +
		 * "<productDescription>testing the unassigned</productDescription>" +
		 * "</ProductDetailsRequest>";
		 

		
		 * String inputXml = "<ProductDetailsRequest>" + "<userId>1</userId>" +
		 * "<productDetails>" + "<ProductDetail>" + "<productId>10</productId>"
		 * + "</ProductDetail>" + "<ProductDetail>" +
		 * "<productId>13</productId>" + "</ProductDetail>" +
		 * "</productDetails>" + "</ProductDetailsRequest>";
		 
		
		 * String inputXml = "<ShareProductInfo>"+
		 * "<toEmail>dileepa_cc@spanservices.com</toEmail>"+
		 * "<productId>1</productId>"+ "<userId>20</userId>"+
		 * "</ShareProductInfo>";
		 
		
		 String inputXml = "<UserRegistrationInfo>"+ //
		 "<userName>sowjanya</userName>"+ "<userId>12</userId>"+
		 "<firstName>span</firstName>"+ "<lastName>devarasetty</lastName>" +
		 "<address1>sreenivas nagar</address1>"+
		 "<address2>10thmain</address2>"+ "<address3>poiu1</address3>"+
		 "<city>Bangalore1</city>"+ "<state>Karnatak1a</state>"+
		 "<postalCode>5600218</postalCode>"+ "<countryID>2</countryID>"+
		  "<gender>1</gender>"+ "<relationShip>Married</relationShip>"+
		  "<incomeRangeID>5</incomeRangeID>"+
		  "<mobileNumber>9866200100</mobileNumber>"+
		  "<email>banglore@spanservices.com</email>"+
		 "<password>scanseee</password>" + "<fieldAgent>0</fieldAgent>" +
		  "</UserRegistrationInfo>"; 
		  inputXml= "<UserRegistrationInfo>" +
		  "<email>test1@test1.com</email>" + "<password>p</password>" +
		  "<deviceID>7</deviceID>" + "<userLongitude>3</userLongitude>" +
		 "<userLatitude>2</userLatitude>" + "</UserRegistrationInfo>";
		  inputXml="<UserMediaInfo>" + "<userId>1</userId>" +
		  "<mediaId>5</mediaId>" + "</UserMediaInfo>"; String xmlReg =
		  "<UserRegistrationInfo>" + "<email>Hubli@spanservices.com</email>" +
		  "<password>scanseee</password>" + "<deviceID>666666</deviceID>" +
		  "<userLongitude>29.99</userLongitude>"+
		  "<userLatitude>52.22</userLatitude>"+ "</UserRegistrationInfo>" ;
		 
		// String authXml = "<AuthenticateUser>" + "<userId>2</userId>" +
		// "<password>A</password>" + "</AuthenticateUser>";
		// adding items from sl to sc
		/*
		 * inputXml = "<AddShoppingCart>" + "<userId>1</userId>" +
		 * "<productDetails>" + "<ProductDetail>" +
		 * "<userProductId>31</userProductId>"+ "<productId>111</productId>" +
		 * "<productName>Sunsilk Shampoo</productName>" + "</ProductDetail>" +
		 * "</productDetails>" + "</AddShoppingCart>" ;
		 

		// adding / remove sc to sb
		
		 * String inputXml = "<AddRemoveSBProducts>" + "<userId>14</userId>" +
		 * "<cartProducts>" + "<ProductDetail>" + "<productId>1</productId>" +
		 * "<productName>P1</productName>" + "</ProductDetail>" +
		 * "</cartProducts>" + "</AddRemoveSBProducts>";
		 

		
		 * String inputXml =
		 * "<?xml version=\"1.0\" encoding=\"UTF-8\"?><UpdateUserInfo xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
		 * + "<userName>murali_pnvb18781</userName><password>a</password>" +
		 * "<firstName>Nagavenkata</firstName><lastName>Balamurali</lastName>" +
		 * "<address1>Bangalore 294 </address1><address2>Tr Nagar</address2><address3>ctbed</address3>"
		 * +
		 * "<state>Karnataka</state><city>Bangalore</city><postalCode>560028</postalCode>"
		 * +
		 * "<mobileNumber>9886398481</mobileNumber><email>murali_pnvb@spanservices.com</email>"
		 * + "<fieldAgent>0</fieldAgent>" + "</UpdateUserInfo>";
		 

		
		 * String inputXml =
		 * "<?xml version=\"1.0\" encoding=\"UTF-8\"?><UserRegistrationInfo>"+
		 * "<password>A</password>" +
		 * "<firstName>Nagavenkata</firstName><lastName>Balamurali</lastName>" +
		 * "<address1>Bangalore 294 </address1><address2>Tr Nagar</address2><address3>ctbed</address3>"
		 * +
		 * "<state>Karnataka</state><city>Bangalore</city><postalCode>560028</postalCode>"
		 * +
		 * "<mobileNumber>9886398481</mobileNumber><email>milestone1demo@span.com</email>"
		 * + "<fieldAgent>0</fieldAgent>" + "<demoGrapyDetails>" +
		 * "<gender>0</gender>" + "<dob>1967-08-13</dob>" +
		 * "<incomeRangeID>1</incomeRangeID>" +
		 * "<maritalStatus>klkl</maritalStatus>" + "</demoGrapyDetails>"+
		 * "<userRetailPreferences>"+ "<UserRetailPreferences>"+
		 * "<retailerId>1</retailerId>"+ "</UserRetailPreferences>"+
		 * "<UserRetailPreferences>"+ "<retailerId>2</retailerId>"+
		 * "</UserRetailPreferences>"+ "</userRetailPreferences>"+
		 * "</UserRegistrationInfo>";
		 

		
		String thisinputXml ="<ThisLocationRequest>" +
		 "<preferredRadius>25</preferredRadius>" +
		 "<zipcode>10001</zipcode>" + " <userId>2</userId>" +
		 "<gpsEnabled>true</gpsEnabled>" + "</ThisLocationRequest>" ; 
		 inputXml = "<CategoryRequest>" + "<userId>1</userId>" +
		 * "<retailerId>1</retailerId>" + "</CategoryRequest>"; inputXml =
		 * "<ProductDetailsRequest>" + "<categoryId>1</categoryId> " +
		 * "<userId>35</userId>" +
		 * "<lastVisitedProductNo>0</lastVisitedProductNo>"+
		 * "</ProductDetailsRequest>";
		 

		
		 * String inputXml = "<ProductDetailsRequest>" + "<userId>14</userId>" +
		 * "<productName>NonAvailableProdcut</productName>" +
		 * "<productDescription>testing the unassigned</productDescription>" +
		 * "</ProductDetailsRequest>";
		 

		// String xmltext = sb.toString();

		// request.accept("application/xml").body(MediaType.APPLICATION_XML_TYPE,
		// xmltext);

		// request.accept("text/plain").body(MediaType.TEXT_PLAIN_TYPE, 1);
		
		 * int productid=1;
		 * request.accept("text/plain").pathParameter("PRODUCTID", productid);
		 
		
		String scanNow = "<ProductDetail>" +
				"<userId>2</userId>" +
				"<barCode>3800ff0391200</barCode>" +
				"<deviceId>33</deviceId>" +
				"<scanLatitude>66.99</scanLatitude>" +
				"<scanLongitude>88.00</scanLongitude>" +
				"</ProductDetail>";
		String searchNow = "<ProductDetail>" +
		"<userId>2</userId>" +
		"<searchkey>kel</searchkey>" +
		"<deviceId>33</deviceId>" +
		"<scanLatitude>66.99</scanLatitude>" +
		"<scanLongitude>88.00</scanLongitude>" +
		"</ProductDetail>";
		
		String inputXml = "<UserRegistrationInfo>"+ //
		 "<firstName>span</firstName>"+ "<lastName>devarasetty</lastName>" +
		 "<address1>sreenivas nagar</address1>"+
		 "<address2>10thmain</address2>"+ "<address3>poiu1</address3>"+
		 "<city>Bangalore1</city>"+ "<state>Karnatak1a</state>"+
		 "<postalCode>5600218</postalCode>"+ "<countryID>2</countryID>"+
		  "<gender>1</gender>"+ "<relationShip>Married</relationShip>"+
		  "<incomeRangeID>5</incomeRangeID>"+
		  "<mobileNumber>9866200100</mobileNumber>" +
		  "<deviceID>33333</deviceID>"+
		  "<email>lavanya@spanservices.com</email>" +
		  "<userLongitude>22.22</userLongitude>"+
		  "<userLatitude>22.22</userLatitude>"+
		 "<password>la</password>" + "<fieldAgent>0</fieldAgent>" +
		  "</UserRegistrationInfo>"; 
		request.accept("text/xml").body(MediaType.TEXT_XML, input);
		// request.accept("text/plain").body(MediaType.TEXT_PLAIN_TYPE,
		// sb.toString());
		
		
		
		
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			// ClientResponse<String> responseObj = request.post();
			// System.out.println("response " + responseObj.getStatus());
			// ClientResponse<String> responseObj = request.get();
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // get response and automatically unmarshall to a string.

		// or

	}

}
*/