//package com.scansee.rest.client;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;


public class FirstUseClient
{

	
	public static void main(String[] args)
	{
		
			executeFirstUse();
		
	}

	
	public static void executeFirstUse()
	{
		//getStartedImageInfo();
		testLogin();
		//testAdddevicetouser();
//		testUpdateAppVersion();
		//forgotPassword();
//		testRegisterUser();
	//	testPushNotification();
		//emailDeclaimNotification();
//	testSavefacebookuser();
	//testRegisterUser();
		
	//testFind();
	//	testPushNotification();
	//testRegisterUser();
		//testPushNotification();
		//testPushNotification();
	//	testFind();
	//	testUpdateAppVersion();
		
	//	testAdddevicetouser();
	//	testVersionCheck();
		// emailDeclaimNotification();
		
//		diaplayMainMenu();
//		testUserTrackingModuleDisplay();
//		testUserTrackingModuleClick();
		
//		contactScanSeeByEmail();
	}

	
	public static void testLogin()
	{
	// */
		final String logon = "http://localhost:8080/ScanSee2.4/firstUse/authenticate";
//		final String logon = "https://app.scansee.net/ScanSee2.3/firstUse/authenticate";
		final ClientRequest request = new ClientRequest(logon);
		final StringBuilder sb = new StringBuilder();
		sb.append("<AuthenticateUser>");
		sb.append("<email><![CDATA[spanqa_app1]]></email>");
		sb.append("<password><![CDATA[123456]]></password>");    
		sb.append("<deviceID>fjhfjfgf</deviceID>");
		sb.append("<appVersion>2.4</appVersion>");
		sb.append("</AuthenticateUser>");
		request.accept("text/xml").body(MediaType.TEXT_XML, sb.toString());
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in Login"+ response);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	public static void testRegisterUser()
	{
		/*UserRegistrationInfo><userName>" +
		"<![CDATA[userregissue]]></userName>" +
		"<password><![CDATA[123456]]></password>" +
		"<deviceID>B5B6B973-B105-50CC-84D5-DE5D65F4A91F</deviceID>" +
		"<appVersion>1.2.2</appVersion>" +
		"</UserRegistrationInfo>
*/
		
		final String logon = "http://localhost:8080/ScanSee2.1/firstUse/saveUser";
		final ClientRequest request = new ClientRequest(logon);
		final String inputXml= "<UserRegistrationInfo>" +"<email><![CDATA[kumar_dodda@spanservices.com]]></email>"+
		 "<password>123456</password>" +"<appVersion>2.0</appVersion>" +
		 "<deviceID>7</deviceID>"+ 
		  "</UserRegistrationInfo>";
		
		String req = "<UserRegistrationInfo>"
				+ "<email><![CDATA[abc@gmail.com]]></email>"
				+ "<password><![CDATA[123456]]></password>"
				+ "<deviceID>B412E39F-593A-4B84-BDC9-415A815B07C0</deviceID>"
				+ "<appVersion>2.1</appVersion>" 
					+ "</UserRegistrationInfo>";

		request.accept("text/xml").body(MediaType.TEXT_XML, req);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in fetchAccountDetails" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static void testSavefacebookuser()
	{
		
		final String logon = "http://localhost:8080/ScanSee1.3.0/firstUse/savefacebookuser";
		final ClientRequest request = new ClientRequest(logon);
		final String inputXml= "<UserRegistrationInfo>" +
		  "<email>ssdf</email>" + "<login>faceBook</login>" +
		  "<deviceID>7</deviceID>"
		   + "</UserRegistrationInfo>";

		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in fetchAccountDetails" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	
	public static void emailDeclaimNotification()
	{
		final String logon = "http://localhost:8080/ScanSee1.3.0/firstUse/emaildeclaimnotification";
		final ClientRequest request = new ClientRequest(logon);
		String inputXml= "<AuthenticateUser>" +
		  "<fromAddress>shyamsundara_hm@spanservices.com</fromAddress>" +
		   "<msgBody>hi</msgBody>" +
		  "</AuthenticateUser>";

		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in testEmails" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	

	
	public static void testSaveUserMedia()
	{
		final String logon = "http://localhost:8080/ScanSeeQA/firstUse/saveusermedia";
		final ClientRequest request = new ClientRequest(logon);
		String inputXml= "<UserMediaInfo>" +
		  "<userId>2</userId>" + 
		  "<mediaId>4</mediaId>" +
		  "</UserMediaInfo>";

		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in save user media" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void testFind()
	{
		//final String logon = "http://localhost:8080/ScanSee/find/findlocdetails";
		final String logon = "http://localhost:8080/ScanSee/find/findlocser";
		final ClientRequest request = new ClientRequest(logon);
		/*String inputXml= "<ServiceSerachRequest>" +
		"<userId>2</userId>"+
		  "<reference>trtCmRgAAAAo2AflR4h-o_AuZ92Mvx46ujUPAAYnmXQDuSzBcNwn62VDxm4q9v_UGuB3R7RkoSxuFQAztCIj0AwkcWZX3StWAVQDheylsZo9bt4Zzkawt4NZiK-6-UO0mgdta66j9JiEhBH_lwxoNIKgyrLpL29ItE1GhQgYRFaXmM6rqreJgex22lktMAkSw</reference>" + 
		  "<language>english</language>" +
		  "</ServiceSerachRequest>";*/
		//request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);/
		String inputXml="<ServiceSerachRequest><userId>2</userId><lat>32.790439</lat><lng>-96.80439</lng><type>bar</type></ServiceSerachRequest>";
request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);
		
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in save user media" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}                                
	}
	public static void testPushNotification()
	{
		

		final String logon = "http://localhost:8080/ScanSee/firstUse/registerpushnotification";
		final ClientRequest request = new ClientRequest(logon);
		final String inputXml= "<UserRegistrationInfo>" +
		  "<userTokenID><![CDATA[1c9b822ef34c36970407eb0f855c3b626349052f4a373933b8f8a56c78bee6899638f]]></userTokenID>"
		+"<deviceID>8eeb6587fbab3fcaeac0abaa67d16d13dfd33ddcdc7d</deviceID>"+"</UserRegistrationInfo>";

		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in fetchAccountDetails" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void testUpdateAppVersion()
	{
		final String logon = "http://10.11.202.76:8080/ScanSee2.0/firstUse/updateappver";
		final ClientRequest request = new ClientRequest(logon);
		final String inputXml= "<UserRegistrationInfo>" +
		 "<appVersion>1.3.0</appVersion>" +"<userId>2</userId>"+
		  "<deviceID>7</deviceID>" 
		 + "</UserRegistrationInfo>";

		
		
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in fetchAccountDetails" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void testAdddevicetouser()
	{
		final String logon = "http://localhost:8080/ScanSee/firstUse/adddevicetouser";
		final ClientRequest request = new ClientRequest(logon);
		final String inputXml= "<UserRegistrationInfo>" +
		 "<appVersion>1.3.0</appVersion>" +"<userId>2</userId>"+
		  "<deviceID>aa5B6B973-B105-50CC-84D5-DE5D65F4A91h</deviceID>" 
		 + "</UserRegistrationInfo>";

		
		
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in fetchAccountDetails" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void testVersionCheck()
	{
//		><AuthenticateUser><deviceID>78b44d2204eefbe3a0dd13f251c71ac8ae219968</deviceID><appVersion>1.3.0</appVersion></AuthenticateUser>

		final String logon = "http://10.11.202.76:8080/VersionScanSeeCheck/firstUse/versioncheck";
		// final String logon =
		// "http://localhost:8080/ScanSee/firstUse/authenticate";
		final ClientRequest request = new ClientRequest(logon);
		final StringBuilder sb = new StringBuilder();
		sb.append("<AuthenticateUser>");
		sb.append("<appVersion>1.3.0</appVersion>");
		sb.append("<deviceID>78b44d2204eefbe3a0dd13f251c71ac8ae219968</deviceID>");
		sb.append("</AuthenticateUser>");
		request.accept("text/xml").body(MediaType.TEXT_XML, sb.toString());
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in Login" + response);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void diaplayMainMenu()
	{
		final String logon = "http://localhost:8080/ScanSee/firstUse/displaymainmenu";
		final ClientRequest request = new ClientRequest(logon);
		final String inputXml= "<DisplayMainMenu>" 
							+ "<usrId>2114</usrId>"
//							+ "<lat>30.45206</lat>" 
//							+ "<lng>-97.768787</lng>" 
							  + "</DisplayMainMenu>";

		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in diaplayMainMenu" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void testUserTrackingModuleDisplay()	{
		final String logon = "http://localhost:8080/ScanSee/firstUse/utmoduledisplay";
		final ClientRequest request = new ClientRequest(logon);

		try
		{
			final String response = request.getTarget(String.class);
			System.out.println("response in testUserTrackingModuleDisplay" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void testUserTrackingModuleClick()	{
		final String logon = "http://localhost:8080/ScanSee/firstUse/utmoduleselect";
		final ClientRequest request = new ClientRequest(logon);
		final String inputXml= "<UserTrackingData>"
							+ "<userID>1</userID>"
							+ "<latitude>31</latitude>"
							+ "<longitude>78</longitude>"
							+ "<moduleID>1</moduleID>"
							+ "<postalCode>45454</postalCode>"
							+ "</UserTrackingData>";
		
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in testUserTrackingModuleClick" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	

	public static void getStartedImageInfo() {
		final String strImages = "http://localhost:8080/ScanSee/firstUse/getstartedimages?userId=1";
		final ClientRequest objClRequest = new ClientRequest(strImages);
		try {
			final String strResponse = objClRequest.getTarget(String.class);
			System.out.println("response in getStartedImageInfo --> " + strResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void forgotPassword()	{
		final String strFGotPwd = "http://localhost:8080/ScanSee/firstUse/forgotpassword?username=kumar_dodda@spanservices.com";
		final ClientRequest objClRequest = new ClientRequest(strFGotPwd);
		try {
			final String strResponse = objClRequest.getTarget(String.class);
			System.out.println("response in forgotPassword --> " + strResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void contactScanSeeByEmail()	{
		final String logon = "http://localhost:8080/ScanSee2.3/firstUse/contactscansee";
		final ClientRequest request = new ClientRequest(logon);
		final String inputXml= "<UserRegistrationInfo>"
								+ "<userId>1</userId>"
								+ "<userName>Ajit nadig</userName>"
								+ "<email>temp@spanservices.com</email>"
								+ "<subject><![CDATA[To test Contact ScanSee]]></subject>"
								+ "<message><![CDATA[Hi,<br/>  This Line contain dfg<br/>    4 space<br/>   3 space<br/>  2 space<br/> 1 space<br/><br/>Thanks<br/>Signature]]></message>"
								+ "<postalCode>78729</postalCode>"
							 + "</UserRegistrationInfo>";
		
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in contactScanSeeByEmail :" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
