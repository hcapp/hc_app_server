//package com.scansee.rest.client;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;

public class MyGalleryRestEasyClient
{
	public static void main(String args[])
	{
//		getMyGalleryInfo();
		 //testAddRebate();
		 //testAddLoyalty();
		//testAddCoupon();
	//	redeemLoyaltyDetails();
//redeemRebateDetails();
	//	redeemLoyaltyDetails();
//	redeemCouponDetails();
	//deleteUsedCoupon();
	//deleteUsedLoyalty();
	//	deleteUsedRebate();

//		getAllCouponsByLocation();
//		getAllCouponsByProduct();
//		getCoupPopulationCentresForLoc();
//		getCoupPopulationCentresForProd();
		
//		getCoupLocationBusinessCategory();
//		getRetailerForBusinessCategory();
//		getCoupProductCategory();
		
		
//		getGalleryCouponsByLocation();
//		getGalleryCouponsByProduct();
//		getGalleryHotDealByLocation();
		getGalleryHotDealByProduct();
	}

	static void getMyGalleryInfo()
	{
		/*@Userid = 2,
		@RetailerID = 0,
		@SearchKey = NULL,
		@CategoryIDs = 11,
		@LowerLimit = 0,
		@ScreenName = N'clr screen',*/
		
		final String getMyGalleryInfo = "http://localhost:8080/ScanSee/mygallery/getMyGalleryInfo";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<CLRDetails>" 
										+ "<userId>3351</userId>" 
										+ "<type>Allloys</type>" 
										+ "<searchKey>a</searchKey>" 
										+ "<retailerId>0</retailerId>" 
										+ "<categoryID>0</categoryID>" 
										+ "<lowerLimit>0</lowerLimit>"
										+ "<moduleID>7</moduleID>"
									+ "</CLRDetails>";
		//<CLRDetails><userId>2114</userId><searchKey>a</searchKey><categoryID>79</categoryID><type>Myloys</type><lowerLimit>0</lowerLimit></CLRDetails>
		//<CLRDetails><userId>2114</userId><type>Usedcoups</type><lowerLimit>0</lowerLimit></CLRDetails><retailerId>0</retailerId>
//<CLRDetails><userId>3054</userId><type>Myloys</type><lowerLimit>0</lowerLimit></CLRDetails>

		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void testAddCoupon()
	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/addCoupon";
		String inputXml = "<AddRemoveCLR>" + "<couponId>23</couponId>" + "<userId>4</userId>" + "</AddRemoveCLR>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	static void testAddLoyalty()
	{
		String URL = "http://10.11.202.76:8080/ScanSee/shoppingList/addLoyalty";
		String inputXml = "<AddRemoveCLR>" + "<loyaltyDealId>23</loyaltyDealId>" + "<userId>4</userId>" 
				+ "</AddRemoveCLR>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	static void testAddRebate()
	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/addRebate";
		String inputXml = "<AddRemoveCLR>" + "<userId>4</userId>" + "<rebateId>2</rebateId>"  + "</AddRemoveCLR>";

		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	

	static void getCouponLoyaltyRebateDetails()
	{

		final String getMyGalleryInfo = "http://localhost:8080/ScanSee/mygallery/fetchcoupondetails";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<CLRDetails>" + "<userId>111</userId>" + "<couponId>1</couponId>" + "<rebateId>null</rebateId>"
				+ "<loyaltyId>null</loyaltyId>" + "</CLRDetails>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}

	static void getCouponInfo()
	{
		final String getHdProdInfo = "http://localhost:8080/ScanSee/mygallery/getCouponInfo?userId=2&productId=1&retailId=1";
		final ClientRequest request = new ClientRequest(getHdProdInfo);
		try
		{
			final String responseObj = request.getTarget(String.class);
			System.out.println("response in getCouponInfo" + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	static void getLoyaltyInfo()
	{
		final String getHdProdInfo = "http://localhost:8080/ScanSee/mygallery/getLoyaltyInfo?userId=1&hotDealId=81714";
		final ClientRequest request = new ClientRequest(getHdProdInfo);
		try
		{
			final String responseObj = request.getTarget(String.class);
			System.out.println("response in getMyGalleryInfo" + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	static void getRebateInfo()
	{
		final String getHdProdInfo = "http://localhost:8080/ScanSee/mygallery/getRebateInfo?userId=1&hotDealId=81714";
		final ClientRequest request = new ClientRequest(getHdProdInfo);
		try
		{
			final String responseObj = request.getTarget(String.class);
			System.out.println("response in getMyGalleryInfo" + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	static void redeemCouponDetails()
	{

		final String getMyGalleryInfo = "http://localhost:8080/ScanSee/mygallery/userredeemcoupon";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<CouponDetails>" + "<userId>265</userId>" + "<couponId>67</couponId>"  + "</CouponDetails>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	static void redeemLoyaltyDetails()
	{
		
		/*<LoyaltyDetail><userId>265</userId><loyaltyDealId>5</loyaltyDealId></LoyaltyDetail> 
		 url =http://10.11.201.26:8080/ScanSee/mygallery/userredeemloyalty 
*/

		final String getMyGalleryInfo = "http://localhost:8080/ScanSee/mygallery/userredeemloyalty";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<LoyaltyDetail>" + "<userId>265</userId>" + "<loyaltyDealId>5</loyaltyDealId>"  + "</LoyaltyDetail>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	static void redeemRebateDetails()
	{
	///	<RebateDetail><userId>3</userId><rebateId>4</rebateId></RebateDetail>
		final String getMyGalleryInfo = "http://localhost:8080/ScanSee/mygallery/userredeemrebate";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<RebateDetail>" + "<userId>265</userId>" + "<rebateId>2</rebateId>"  + "</RebateDetail>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	static void deleteUsedRebate()
	{
	///	<RebateDetail><userId>3</userId><rebateId>4</rebateId></RebateDetail>
		final String getMyGalleryInfo = "http://localhost:8080/ScanSee1.4.0/mygallery/deleteusedrebate";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<RebateDetail>" + "<userId>265</userId>" + "<userRebateGalleryID>42</userRebateGalleryID>"  + "</RebateDetail>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	static void deleteUsedLoyalty()
	{

		final String getMyGalleryInfo = "http://localhost:8080/ScanSee1.4.0/mygallery/deleteusedloyalty";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<LoyaltyDetail>" + "<userId>265</userId>" + "<userLoyaltyGalleryID>75</userLoyaltyGalleryID>"  + "</LoyaltyDetail>";
		
	//	"?><CouponDetails><userId>265</userId><userLoyaltyGalleryID>75</userLoyaltyGalleryID></CouponDetails>

		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	static void deleteUsedCoupon()
	{
//CouponDetails><userId>3351</userId><userCouponGalleryID>66055</userCouponGalleryID></CouponDetails>
		final String getMyGalleryInfo = "http://localhost:8080/ScanSee1.4.0/mygallery/deleteusedcoupon";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<CouponDetails>" + "<userId>3351</userId>" + "<userCouponGalleryID>1910</userCouponGalleryID>"  + "</CouponDetails>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void getAllCouponsByLocation()
	{
		final String getMyGalleryInfo = "http://10.11.202.76:8080/ScanSee2.2/mygallery/getallcoupbyloc";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<ProductDetailsRequest>" 
										+ "<userId>3881</userId>" 
//										+ "<searchKey></searchKey>"
										+ "<postalcode>71656</postalcode>"  
//										+ "<latitude>33.5892</latitude>"
//										+ "<longitude>-91.8088</longitude>"
										+ "<retailID>0</retailID>"
										+ "<busCatIDs>0</busCatIDs>"
										+ "<lowerLimit>0</lowerLimit>"
//										+ "<popCentId></popCentId>"
//										+ "<mainMenuID>45</mainMenuID>"
										+ "<moduleID>7</moduleID>"
									+ "</ProductDetailsRequest>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
//	<ProductDetailsRequest>
//	<userId>10319</userId>
//	<moduleID>7</moduleID>
//	<retailID>0</retailID>
//	<catIDs>0</catIDs>
//	<lowerLimit>0</lowerLimit>
//	<popCentId>0</popCentId>
//	</ProductDetailsRequest>
//	http://199.36.142.83:8080/ScanSee2.2/mygallery/getallcoupbyprod

	
	static void getAllCouponsByProduct()
	{
		final String getMyGalleryInfo = "http://10.11.202.76:8080/ScanSee2.2/mygallery/getallcoupbyprod";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<ProductDetailsRequest>" 
										+ "<userId>10319</userId>" 
//										+ "<searchKey></searchKey>"
//										+ "<postalcode>75243</postalcode>"  
//										+ "<latitude>32.910347</latitude>"
//										+ "<longitude>-96.728472</longitude>"
										+ "<catIDs>0</catIDs>"
										+ "<lowerLimit>0</lowerLimit>"
										+ "<popCentId>0</popCentId>"
//										+ "<mainMenuID>25</mainMenuID>"
										+ "<moduleID>7</moduleID>"
									+ "</ProductDetailsRequest>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void getCoupPopulationCentresForLoc()
	{
		final String getMyGalleryInfo = "http://localhost:8080/ScanSee2.2/mygallery/couplocpopcent?userId=12";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);

		try
		{
			final String response = request.getTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void getCoupPopulationCentresForProd()
	{
		final String getMyGalleryInfo = "http://localhost:8080/ScanSee2.2/mygallery/coupprodpopcent?userId=1";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);

		try
		{
			final String response = request.getTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void getCoupLocationBusinessCategory()
	{
		final String getMyGalleryInfo = "http://localhost:8080/ScanSee2.2/mygallery/couplocbuscat";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);

		final String getMyGalInfXml = "<ProductDetailsRequest>" 
										+ "<userId>3878</userId>" 
//										+ "<postalcode>75243</postalcode>"  
//										+ "<latitude>32.910347</latitude>"
//										+ "<longitude>-96.728472</longitude>"
//										+ "<popCentId>0</popCentId>"
									+ "</ProductDetailsRequest>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
	
	static void getRetailerForBusinessCategory()
	{
		final String getMyGalleryInfo = "http://localhost:8080/ScanSee/mygallery/retforbuscat";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);

		final String getMyGalInfXml = "<ProductDetailsRequest>" 
										+ "<userId>3878</userId>" 
										+ "<postalcode>75243</postalcode>"  
										+ "<latitude>32.910347</latitude>"
										+ "<longitude>-96.728472</longitude>"
										+ "<popCentId>0</popCentId>"
										+ "<busCatIDs></busCatIDs>"
									+ "</ProductDetailsRequest>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
	
	static void getCoupProductCategory()
	{
		final String getMyGalleryInfo = "http://localhost:8080/ScanSee/mygallery/coupprodcat";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);

		final String getMyGalInfXml = "<ProductDetailsRequest>" 
										+ "<userId>3878</userId>" 
										+ "<postalcode>75243</postalcode>"  
										+ "<latitude>32.910347</latitude>"
										+ "<longitude>-96.728472</longitude>"
										+ "<popCentId>0</popCentId>"
									+ "</ProductDetailsRequest>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
	
	static void getGalleryCouponsByLocation()
	{
		final String getMyGalleryInfo = "https://app.scansee.net/ScanSee2.2/mygallery/gallcoupbyloc";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<ProductDetailsRequest>" 
										+ "<userId>8700</userId>" 
										+ "<searchKey>Coupon</searchKey>"
										+ "<lowerLimit>0</lowerLimit>"
										+ "<mainMenuID>45</mainMenuID>"
										+ "<type>Claimed</type>"
//										+ "<moduleID>13</moduleID>"
									+ "</ProductDetailsRequest>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void getGalleryCouponsByProduct()
	{
		final String getMyGalleryInfo = "https://app.scansee.net/ScanSee2.2/mygallery/gallcoupbyprod";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<ProductDetailsRequest>" 
										+ "<userId>8700</userId>" 
//										+ "<searchKey>coupon</searchKey>"
										+ "<lowerLimit>0</lowerLimit>"
										+ "<type>Claimed</type>"
										+ "<mainMenuID>45</mainMenuID>"
//										+ "<moduleID>13<moduleID>"
									+ "</ProductDetailsRequest>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	
	static void getGalleryHotDealByLocation()
	{
		final String getMyGalleryInfo = "http://localhost:8080/ScanSee2.2/mygallery/gallhdbyloc";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<ProductDetailsRequest>" 
										+ "<userId>1</userId>" 
//										+ "<searchKey></searchKey>"
										+ "<lowerLimit>0</lowerLimit>"
										+ "<mainMenuID>45</mainMenuID>"
										+ "<type>Gallery</type>"
									+ "</ProductDetailsRequest>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void getGalleryHotDealByProduct()
	{
		final String getMyGalleryInfo = "http://199.36.142.83:8080/ScanSee2.4/mygallery/gallhdbyprod";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<ProductDetailsRequest>" 
										+ "<userId>12076</userId>" 
//										+ "<searchKey></searchKey>"
										+ "<lowerLimit>0</lowerLimit>"
//										+ "<mainMenuID>45</mainMenuID>"
										+ "<type>Claimed</type>"
									+ "</ProductDetailsRequest>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}

}
