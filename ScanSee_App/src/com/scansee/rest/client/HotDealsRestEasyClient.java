//package com.scansee.rest.client;

import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;

import com.scansee.common.constants.ApplicationConstants;

public class HotDealsRestEasyClient
{

	
	private HotDealsRestEasyClient()
	{

	}

	
	public static void main(String[] args)
	{
		hotDeals();
//		getHotDeallist();
		//getHotDealProdByCategory();
	}

	
	public static void hotDeals()
	{
//		System.out.println("=================Before getHotDeallist===================\n");
//		getHdProdInfo();
		//parseSpecialCharacters();
		getHotDeallist();
		//System.out.println("\n=================After getHotDeallist===================\n");
		
//		System.out.println("=================Before getHdProdInfo===================\n");
//		getHdProdInfo();
//		System.out.println("\n=================After getHdProdInfo===================\n");
//
//		System.out.println("=================Before removeHdProd===================\n");
		//removeHdProd();
//		System.out.println("\n=================After removeHdProd===================\n");
//
//		System.out.println("=================Before addHotDealToShoppingList===================\n");
//		addHotDealToShoppingList();
//		System.out.println("\n=================After addHotDealToShoppingList===================\n");
//
//		System.out.println("=================Before addHotDealToWishList===================\n");
//		addHotDealToWishList();
//		System.out.println("\n=================After addHotDealToWishList===================\n");
		
//		userTrackingGetHotDealClick();
//		getProdHotGDal();
//		hotDealClaim();
//		hotDealRedeem();
	}

	static void parseSpecialCharacters()
	{
		String specialLstw[]={"*","@",",","#","$","%","^","&","*","(",")","_","+","=","{","}","[","]",":",";","-","<",">","?" };
		ArrayList<String> specialLst=new ArrayList<String>();
		
		//{"*@#$,%&*()_+={}[]:;-<>? };
		//specialLst={"*","@",",","#","$","%","^","&","*","(",")","_","+","=","{","}","[","]",":",";","-","<",">","?" };
		specialLst.add("*");
		specialLst.add("@");
		specialLst.add(",");
		specialLst.add("#");
		specialLst.add("$");
		specialLst.add("%");
		specialLst.add("^");
		specialLst.add("&");
		specialLst.add("(");
		specialLst.add(")");
		specialLst.add("_");
		specialLst.add("-");
		specialLst.add("+");
		specialLst.add("=");
		specialLst.add("{");
		specialLst.add("}");
		specialLst.add("[");
		specialLst.add("]");
		specialLst.add(":");
		specialLst.add(";");
		specialLst.add("<");
		specialLst.add(">");
		specialLst.add("/");
		specialLst.add("?");
		specialLst.add("|");
		specialLst.add("!");
		specialLst.add(".");
		specialLst.add(",");
		
		
		for (String string : specialLst)
		{
			System.out.println("array contents are \n"+string);
			
		}
		//",',<,>,/,?,}";
	}
	
	static void getHotDeallist()
	{

		final String getHotDeallist2 = "http://199.36.142.83:8080/ScanSee2.4/hotDeals/getHotDealProds";
//		final String getHotDeallist2 = "http://localhost:8080/ScanSee2.3/hotDeals/getHotDealProds";
		final ClientRequest request = new ClientRequest(getHotDeallist2);

		final String getHotDeallistXml2 = "<HotDealsListRequest>" 
											+ "<userId>7693</userId>"
											+ "<category>0</category>"
											//+ "<searchItem><![CDATA[a]]></searchItem>"
//											+ "<latitude>30.331562</latitude>"
//											+ "<longitude>-97.700394</longitude>"
											+ "<zipCode>78751</zipCode>"
											+ "<lastVisitedProductNo>0</lastVisitedProductNo>"
//											+ "<populationCentreID>1</populationCentreID>"
//											+ "<mainMenuID>11</mainMenuID>"
											+ "<moduleID>4</moduleID>"
//											+ "<radius>20</radius>"
										+ "</HotDealsListRequest>";
		
//		final String getHotDeallistXml = "<HotDealsListRequest>" + "<userId>2</userId>" +"<moduleID>1</moduleID>"+"<category>0</category>"+"<latitude>40.714224</latitude>" +"<longitude>-73.961452</longitude>"
//				+	 "<populationCentreID>45</populationCentreID><lastVisitedProductNo>0</lastVisitedProductNo></HotDealsListRequest>" ;

		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", getHotDeallistXml2);
		request.getHeaders();
		try
		{
			long strtTime = new Date().getTime();
			final String response = request.postTarget(String.class);
			long endTime = new Date().getTime();
			System.out.println(endTime - strtTime);
			System.out.println("response for getHotDeallist" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	
	static void getHdProdInfo()
	{
		final String getHdProdInfo = "http://localhost:8080/ScanSee2.3/hotDeals/getHdProdInfo?userId=2&hotDealId=1461475&hotDealListID=522526";
//		final String getHdProdInfo = "http://199.36.142.83:8080";
		final ClientRequest request = new ClientRequest(getHdProdInfo);
		try
		{
			final String responseObj = request.getTarget(String.class);
			System.out.println("response in getHdProdInfo" + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	
	static void removeHdProd()
	{
		
		final String removeHdProd = "http://localhost:8080/ScanSee2.2/hotDeals/removeHdProd";
		final ClientRequest request = new ClientRequest(removeHdProd);
		final String removeHdProdXml = "<HotDealsListRequest>" + "<userId>1</userId>" + "<hotDealId>81773</hotDealId>" + "<hDInterested>0</hDInterested>"
				+ "</HotDealsListRequest>";

		request.accept(ApplicationConstants.TEXTXML).body(MediaType.TEXT_XML, removeHdProdXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in removeHdProd" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	
	static void getHdFaviCate()
	{
		final String getHdFaviCate = "http://localhost:8080/ScanSee/hotDeals/getHdFaviCate?userId=1";
		final ClientRequest request = new ClientRequest(getHdFaviCate);
		try
		{
			final String responseObj = request.getTarget(String.class);
			System.out.println("response in getHdFaviCate " + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	static void addHotDealToShoppingList()
	{
		final String addHotDealToShoppingList = "http://localhost:8080/ScanSee/shoppingList/addproducttosl";
		final ClientRequest request = new ClientRequest(addHotDealToShoppingList);
		final String addHotDealToShoppingListXml = "<AddShoppingList>" + "<userId>1</userId>" + "<productDetails>" + "<ProductDetail>"
				+ "<productId>81715</productId>" + "<productName>Sunsilk Shampoo</productName>" + "</ProductDetail>" + "</productDetails>"
				+ "</AddShoppingList>";
		request.accept(ApplicationConstants.TEXTXML).body(MediaType.TEXT_XML, addHotDealToShoppingListXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in  addHotDealToShoppingList" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	
	static void addHotDealToWishList()
	{
		final String addHotDealToWishList = "http://localhost:8080/ScanSee/wishlist/addWishListProd";
		final ClientRequest request = new ClientRequest(addHotDealToWishList);
		final String addHotDealToWishListXml = "<ProductDetailsRequest>" + "<userId>2</userId>" + "<productDetails>" + "<ProductDetail>"
				+ "<productId>3</productId>" + "</ProductDetail>" + "<ProductDetail>" + "<productId>7</productId>" + "</ProductDetail>"
				+ "</productDetails>" + "</ProductDetailsRequest>";
		request.accept(ApplicationConstants.TEXTXML).body(MediaType.TEXT_XML, addHotDealToWishListXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in addHotDealToWishList" + response);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	static void userTrackingGetHotDealClick()
	{
		final String addHotDealToWishList = "http://localhost:8080/ScanSee/wishlist/utgethotdealclick?userId=1&hotDealId=1&hotDealListID=1";
		final ClientRequest request = new ClientRequest(addHotDealToWishList);
		
		try
		{
			final String response = request.getTarget(String.class);
			System.out.println("response in userTrackingGetHotDealClick" + response);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}

	static void getHotDealProdByCategory()
	{
		final String getHotDeallist2 = "http://localhost:8080/ScanSee/hotDeals/getHDProdsByCategory";
		final ClientRequest request = new ClientRequest(getHotDeallist2);      
		final String strInputXML = "<HotDealsListRequest>" 
											//+ "<userId>2367</userId>"
											//+ "<category>0</category>"
											//+ "<searchItem><![CDATA[a]]></searchItem>"
											//+ "<latitude>30.331562</latitude>"
											//+ "<longitude>-97.700394</longitude>"
											//+ "<zipCode>78752</zipCode>"
											//+ "<radius>0</radius>"
										//	+ "<populationCentreID>0</populationCentreID>"
											
											+ "<userId>2542</userId>"
											+ "<category>0</category>"
											//+ "<searchItem><![CDATA[a]]></searchItem>"
											//+ "<latitude>30.331562</latitude>"
											//+ "<longitude>-97.700394</longitude>"
											+ "<zipCode>78752</zipCode>"
											+ "<radius>0</radius>"
										//	+ "<populationCentreID>0</populationCentreID>"
											+ "</HotDealsListRequest>";
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", strInputXML);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("Request for getHotDealProdByCategory" + strInputXML);
			System.out.println("response for getHotDealProdByCategory" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	static void getProdHotGDal()
	{
		final String addHotDealToWishList = "http://localhost:8080/ScanSee/hotDeals/getprodhotdeal";
		final ClientRequest request = new ClientRequest(addHotDealToWishList);
		final String addHotDealToWishListXml = "<ThisLocationRequest>"
												+ "<userId>7718</userId>"
												+ "<zipcode>59721</zipcode>"
												+ "<productId>6302896</productId>"
												+ "<mainMenuID>7366</mainMenuID>"
												+ "<alertProdID>1997</alertProdID>"
											+ "</ThisLocationRequest>";
		request.accept(ApplicationConstants.TEXTXML).body(MediaType.TEXT_XML, addHotDealToWishListXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in addHotDealToWishList" + response);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	static void hotDealClaim()
	{
		final String addHotDealToWishList = "http://localhost:8080/ScanSee2.2/hotDeals/hotdealclaim";
		final ClientRequest request = new ClientRequest(addHotDealToWishList);
		final String addHotDealToWishListXml = "<HotDealsListRequest>"
												+ "<userId>15</userId>"
												+ "<hotDealId>6467032</hotDealId>"
												+ "<mainMenuID>11</mainMenuID>"
											+ "</HotDealsListRequest>";
		request.accept(ApplicationConstants.TEXTXML).body(MediaType.TEXT_XML, addHotDealToWishListXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in hotDealClaim" + response);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	static void hotDealRedeem()
	{
		final String addHotDealToWishList = "http://localhost:8080/ScanSee2.2/hotDeals/hotdealredeem";
		final ClientRequest request = new ClientRequest(addHotDealToWishList);
		final String addHotDealToWishListXml = "<HotDealsListRequest>"
												+ "<hotDealId>6467186</hotDealId>"
												+ "<userId>2639</userId>"
												+ "<mainMenuID>4174</mainMenuID>"
											+ "</HotDealsListRequest>";
		request.accept(ApplicationConstants.TEXTXML).body(MediaType.TEXT_XML, addHotDealToWishListXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in hotDealRedeem" + response);
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}
}
