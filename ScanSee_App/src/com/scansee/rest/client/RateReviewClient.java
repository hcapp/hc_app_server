//package com.scansee.rest.client;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;

public class RateReviewClient
{

	public static void main(String[] args)
	{
		RateReviewClient ii = new RateReviewClient();
		ii.executeRateReviewTestCases();
	}

	public void executeRateReviewTestCases()
	{
		// testFecthUserProductRating("2", "122");
	//	 testSaveUserProductRating();
		// testSharespecialoffByEmail();
	//testShareProductRetailerInfo();
		testGetShareProductInfo();
		//testGetShareCouponthruEmailInfo();
	//testGetShareProductInfo();
	// testGetShareCouponthruEmailInfo();
		
//		testGetShareHotdealthruEmailInfo();
		
		//testGetShareProductInfo();
	//testGetShareProductInfo();
	//	testGetShareHotdealthruEmailInfo();
	//	testGetShareCouponthruEmailInfo();
//		testGetShareCouponthruEmailInfo();
//		userTrackingShareTypeUpdate();
//		testShareAppSite();
//		testShareAppSiteByEmail();
//		testShareProductRetailerInfo();
	}

	void testFecthUserProductRating(String userId, String productId)
	{
		String URL = "http://localhost:8080/ScanSee/ratereview/fetchuserrating?userId=" + userId + "&productId=" + productId;
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testSaveUserProductRating()
	{
		String URL = "http://localhost:8080/ScanSee/ratereview/saveuserrating";
//		<UserRatingInfo><userId>2850</userId><productId>3345944</productId><currentRating>0</currentRating></UserRatingInfo>
		String addproducttoshoplistInputXml = "<UserRatingInfo>" + "<userId>2850</userId>" + "<productId>3345944</productId>"
				+ "<currentRating>7</currentRating>" + "</UserRatingInfo>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, addproducttoshoplistInputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testShareProductRetailerInfo()
	{
		String URL = "https://app.scansee.net/ScanSee2.0/ratereview/shareproductretailerinfo";
		//String URL = "http://10.122.61.146:8080/ScanSee2.0/ratereview/shareproductretailerinfo";
		String strInputXml = "<UserRatingInfo>" + "<userId>2</userId>" + "<isFromThisLocation>1</isFromThisLocation>"
				+ "<retailerId>31</retailerId>" + "<productId>1</productId>" + "<retailerLocationId>65</retailerLocationId>"
				+ "</UserRatingInfo>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", strInputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testGetShareProductInfo()
	{
	/*	<?xml version="1.0" encoding="UTF-8"?><ShareProductInfo><toEmail><![CDATA[sandeep_sr@spanservices.com]]>
		</toEmail><isFromThisLocation>0</isFromThisLocation><productId>170686</productId><userId>2</userId></ShareProductInfo>*/
		
		/*4568
		4569
		4570
		4571
		4572l	//	shareURL="http://localhost:8080/ScanSee-Email/product/showContent.do?";
		4573
		4574*/
		
		String URL = "http://localhost:8080/ScanSee2.3/ratereview/shareProductInfo";
		String inputXml = "<ShareProductInfo>" 
							+ "<toEmail>dhruvanath_mm@spanservices.com</toEmail>"
							+ "<isFromThisLocation>0</isFromThisLocation>" 
							+ "<productId>4568</productId>"
							+ "<userId>2</userId>" 
						+ "</ShareProductInfo>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response); 
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testGetShareCouponthruEmailInfo()
	{
		String URL = "http://localhost:8080/ScanSee1.4.0/ratereview/shareclrbyemail";
		String inputXml = "<ShareProductInfo><toEmail>shyamsundara_hm@spanservices.com</toEmail>"
				+ "<loyaltyId>489</loyaltyId>" + "<userId>3329</userId></ShareProductInfo> ";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testGetShareHotdealthruEmailInfo()
	{
		String URL = "http://localhost:8080/ScanSee2.3/ratereview/sharehotdealbyemail";
		String inputXml = "<ShareProductInfo>" 
							+ "<toEmail>chaya_bs@spanservices.com</toEmail>"
							+ "<hotdealId>6466536</hotdealId>" 
							+ "<userId>2</userId>" 
						+ "</ShareProductInfo>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	void testSharespecialoffByEmail()
	{
		String URL = "http://localhost:8080/ScanSee/ratereview/sharespecialoff";
		String inputXml = "<ShareProductInfo><toEmail>shyamsundara_hm@spanservices.com</toEmail>"
				+ "<retailerId>31</retailerId><pageId>5</pageId>" + "<userId>552</userId></ShareProductInfo>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	void userTrackingShareTypeUpdate()	{
		
		String URL = "http://localhost:8080/ScanSee/ratereview/utsharetype";
		String inputXml = "<UserTrackingData>"
							+ "<mainMenuID>1</mainMenuID>"
							+ "<shrTypNam>Facebook</shrTypNam>"
							+ "<tarAddr>asdf</tarAddr>"
							+ "<prodID>6</prodID>"
//							+ "<coupID></coupID>"
//							+ "<loyID></loyID>"
//							+ "<rebID></rebID>"
//							+ "<retLocID></retLocID>"
//							+ "<hotDealID></hotDealID>"
						+ "</UserTrackingData>";
		
		String shareTypes = "Facebook,Twitter,Email,Text";
		
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	void testShareAppSite()
	{
		String URL = "http://localhost:8080/ScanSee/ratereview/shareappsite";
		ClientRequest request = new ClientRequest(URL);

		String inputXml = "<ShareProductInfo>"
							+ "<userId>2</userId><retailerId>1685</retailerId>" 
							+ "<retailerLocationId>93874</retailerLocationId><shareType>Facebook</shareType>"
						    + "</ShareProductInfo>";

		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8",	inputXml);
		request.getHeaders();
		try {
			String response = request.postTarget(String.class);
			//response = response.replaceAll("AppSite�", "AppSite™");
			System.out.println("response " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	void testShareAppSiteByEmail()
	{
		String URL = "http://10.11.202.76:8080/ScanSee2.3/ratereview/shareappsiteemail";
		
		String inputXml = "<ShareProductInfo>" 
							+ "<toEmail>dhruvanath_mm@spanservices.com</toEmail>"
							+ "<shareType>email</shareType>" 
							+ "<retailerId>17</retailerId>" 
							+ "<retailerLocationId>21072</retailerLocationId>" 
							+ "<userId>2</userId>" 
						+ "</ShareProductInfo>";
		
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
