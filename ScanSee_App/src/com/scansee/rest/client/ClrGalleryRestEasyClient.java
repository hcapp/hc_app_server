//package com.scansee.rest.client;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;

public class ClrGalleryRestEasyClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClrGalleryRestEasyClient obj=new ClrGalleryRestEasyClient();
		obj.testCouponDetails();
//		obj.testRebateDetails();
//		obj.testLoyaltyDetails();
//		obj.testLoyaltyDetails();
//		obj.testGetCLRProdInfo();
		
	}

	 void testCouponDetails()
	{
		String URL = "http://localhost:8080/ScanSee2.1/clrgallery/getclrdetails";
		String onlinestorexml = "<CLRDetails>" 
								+ "<userId>2</userId>" 
								+ "<couponId>66001</couponId>"
								+ "<couponListID>1</couponListID>" 
//								+ "<loyaltyListID>1</loyaltyListID>"
							+ "</CLRDetails>";
			
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", onlinestorexml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	 void testRebateDetails()
		{
			String URL = "http://localhost:8080/ScanSee1.5.1/clrgallery/getclrdetails";
			String onlinestorexml = "<CLRDetails>" + "<userId>2</userId>" + "<rebateId>1</rebateId>" +"</CLRDetails>";
						
			ClientRequest request = new ClientRequest(URL);
			request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", onlinestorexml);
			request.getHeaders();
			try
			{
				String response = request.postTarget(String.class);
				System.out.println("response " + response);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	 void testLoyaltyDetails()
		{
			String URL = "http://localhost:8080/ScanSee/clrgallery/getclrdetails";
			String onlinestorexml = "<CLRDetails>" 
										+ "<userId>2114</userId>" 
										+ "<loyaltyDealID>471</loyaltyDealID>" 
										+ "<couponListID>1</couponListID>" 
										+ "<loyaltyListID>1</loyaltyListID>"
									+ "</CLRDetails>";
			
			// <CLRDetails><userId>2114</userId><loyaltyDealID>4</loyaltyDealID></CLRDetails>

			

			ClientRequest request = new ClientRequest(URL);
			request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", onlinestorexml);
			request.getHeaders();
			try
			{
				String response = request.postTarget(String.class);
				System.out.println("response " + response);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	 
	 void testGetCLRProdInfo()
		{
			String URL = "http://localhost:8080/ScanSee/clrgallery/getclrprodinfo";
		String loyaltyXml = "<CLRDetails>" 
								+ "<userId>2</userId>" 
//								+ "<loyaltyDealID>55</loyaltyDealID>" 
								+ "<couponId>66195</couponId>"
								+ "<couponListID>2</couponListID>"
//								+ "<loyaltyListID>1</loyaltyListID>"
//								+ "<rebateListID>1</rebateListID>"
								+ "<mainMenuID>1</mainMenuID>"
							+"</CLRDetails>";
		String couponXml = "<CLRDetails>" + "<userId>2797</userId>" + "<couponId>65700</couponId>" +"</CLRDetails>";
			
	
//<CLRDetails><userId>3298</userId><loyaltyDealID>55</loyaltyDealID></CLRDetails>
			ClientRequest request = new ClientRequest(URL);
			request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", loyaltyXml);
			request.getHeaders();
			try
			{
				String response = request.postTarget(String.class);
				System.out.println("response " + response);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	 
	 static void promotionCouponDetails()
		{
			final String getMyGalleryInfo = "http://localhost:8080/ScanSee/clrgallery/getcoupondetails";
			final ClientRequest request = new ClientRequest(getMyGalleryInfo);
			final String getMyGalInfXml = "<CLRDetails>" + "<userId>25</userId>" + "<couponId>66441</couponId>"  + "<couponListID>13975</couponListID>" + "</CLRDetails>";
			request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
			request.getHeaders();
			try
			{
				final String response = request.postTarget(String.class);
				System.out.println("response " + response);
			} catch (Exception e) {
				System.out.println("exception in scansee rest easy client..");
			}
		}
		
		
		static void promotionCouponLocationDetails()
		{
			final String getMyGalleryInfo = "http://localhost:8080/ScanSee/clrgallery/getcouponproductorloclist";
			final ClientRequest request = new ClientRequest(getMyGalleryInfo);
			final String getMyGalInfXml = "<CLRDetails>" + "<userId>25</userId>" + "<couponId>66441</couponId>" 
										+ "<couponListID>13975</couponListID>" + "<mainMenuID>25</mainMenuID>"  + "<type>location</type>"  +"</CLRDetails>";
			request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
			request.getHeaders();
			try
			{
				final String response = request.postTarget(String.class);
				System.out.println("response " + response);
			} catch (Exception e) {
				System.out.println("exception in scansee rest easy client..");
			}
		}
}
