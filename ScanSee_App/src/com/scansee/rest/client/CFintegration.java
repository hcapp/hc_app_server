//package com.scansee.rest.client;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;

public class CFintegration
{
	public static void main(String[] args)
	{
		addLoyalty();
	}
	
	public static void getMerchants(){
		
	}
	
	public static void addLoyalty(){
		final String loyaltyInfo = "<CFRequest><retailId>1221</retailId><merchantId>444</merchantId><cardNumber>706000004462</cardNumber><referenceId>2059</referenceId></CFRequest>";
		String url = "http://localhost:8080/ScanSee/manageloyaltycard/addloyaltycard";
		final ClientRequest request = new ClientRequest(url);
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, loyaltyInfo);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
	
	static void testAddLoyalty()
	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/addLoyalty";
		String inputXml = "<AddRemoveCLR>" + "<loyaltyDealId>2697</loyaltyDealId>" + "<userId>2059</userId> <merchantId>1221</merchantId>" + "<productId>1</productId>"
				+ "</AddRemoveCLR>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
