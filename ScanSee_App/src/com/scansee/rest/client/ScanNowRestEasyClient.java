//package com.scansee.rest.client;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;

/**
 * This class for sending client request and response xmls.
 * 
 * @author shyamsundara_hm
 */
public class ScanNowRestEasyClient
{
	/**
	 * constructor for ScanNowRestEasyClient.
	 */
	public ScanNowRestEasyClient()
	{

	}

	/**
	 * Main method.
	 * 
	 * @param args
	 *            as the request.
	 */
	public static void main(String[] args)
	{

		scanRequests();
		//userTrackingGetMainMenuID();
		//giveAwayReg();
//		giveAwayPageDetails();

	}

	/**
	 * This method contains all the scansee requests.
	 */
	public static void scanRequests()
	{
//		System.out.println("=================Before getProdForBarCode===================\n");
//		getProdForBarCode();
		// System.out.println("=================After getProdForBarCode===================\n");
		// System.out.println("=================Before scanForProductName===================\n");
	//	scanForProductName();
		// System.out.println("=================After scanForProductName===================\n");
		// System.out.println("=================Before getsdk===================\n");
//		 getsdk();
		// System.out.println("=================After getsdk===================\n");
		// System.out.println("=================Before getscanhistory===================\n");
	//	getscanhistory();
		// System.out.println("=================After getscanhistory===================\n");
		//scanForProductName();
		//getSearchProdForName();
	//getSearchProdForName();
	//	scanForProductName();
	//scanForProductName();
	//	scanForProductName();
		//deletefromScanHistory();
		
//		getsmartsearchproducts();
		getsmartsearchCount();
	//	getsmartsearchproducts();
//		getsmartsearchProductlst();
		
		//scanForProductName();
		
	//	getsmartsearchCount();
		
	//	getProductInfo();
//		getSmartSearchProducts1();
//		userTrackingGetMainMenuID();
		//giveAwayReg();
		//giveAwayPageDetails();
	}

	/**
	 * This method for getting product for barcode.
	 */
	static void getProdForBarCode()
	{
		
		
	/*	<ProductDetail>
		<deviceId>a87b23bcafd520b2423db93589d6d236050083ee</deviceId>
		<userId>2860</userId>
		<barCode><![CDATA[65756744]]></barCode>
		<scanType><![CDATA[EAN 8]]></scanType>
	</ProductDetail>
*/
	//	<ProductDetail><deviceId>123</deviceId><userId>1</userId><barCode><![CDATA[30001817]]></barCode><scanType><![CDATA[EAN 8]]></scanType></ProductDetail>
		final String getProdForBarCode = "http://localhost:8080/ScanSee/scanNow/getProdForBarCode";
		final ClientRequest request = new ClientRequest(getProdForBarCode);
//"<scanType>UPC-11</scanType><lat>39.080332</lat>
	//	<lng>-94.780593</lng>
		final String getProdForBarCodeXml = "<ProductDetail>" 
												+ "<deviceId>a87b23bcafd520b2423db93589d6d236050083ee</deviceId>" 
												+ "<barCode><![CDATA[010343876200]]></barCode>" 
												+ "<scanType><![CDATA[UPC-A]]></scanType>"
												+ "<userId>2</userId>" 
												+ "<mainMenuID>214</mainMenuID>"
												+ "<scanTypeID>1</scanTypeID>"
											+ "</ProductDetail>";
		//"<scanLatitude>22.00</scanLatitude>" + "<scanLongitude>33.99</scanLongitude>"
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProdForBarCodeXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}

	/**
	 * This method for scan for product name.
	 */
	static void scanForProductName()
	{
		
		/*<ProductDetailsRequest>
		<userId>3</userId>
		<productId>3606185</productId>
	</ProductDetailsRequest>

		*/
		//final String getProdForName = "https://app.scansee.net/ScanSee1.5/scanNow/getProdForName";
		final String getProdForName2 = "http://localhost:8080/ScanSee1.5/scanNow/getProdForName";
		final ClientRequest request = new ClientRequest(getProdForName2);
		// String getProdForBarCodeXml2 =
		// "<ProductDetail><scanLongitude>75.4321</scanLongitude><scanLatitude>75.4321</scanLatitude><fieldAgentRequest>22</fieldAgentRequest><userId>2</userId><searchkey>sa</searchkey></ProductDetail>";
	//	<ProductDetail><userId>3054</userId><searchkey><![CDATA[coolpix]]></searchkey><lastVistedProductNo>0</lastVistedProductNo></ProductDetail>
	
		
		final String getProdForBarCodeXml = "<ProductDetail>"
			+ "<searchkey><![CDATA[fish]]></searchkey>"+
			"<userId>2823</userId>"
			+ 
			"<lastVistedProductNo>0</lastVistedProductNo>"+
			"</ProductDetail>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProdForBarCodeXml);
		request.getHeaders();
		try
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			long strtTime = new Date().getTime();
			String response = request.postTarget(String.class);
			long endtime = new Date().getTime();
			System.out.println((endtime - strtTime));
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}

	/**
	 * this method for getting sdk information..
	 */
	static void getsdk()
	{
		final String getsdk = "http://localhost:8080/ScanSee/scanNow/getSDK?userId=1&moduleID=3";
		final ClientRequest request = new ClientRequest(getsdk);
		// request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML,
		// getProdForBarCodeXml2);
		try
		{
			final String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}

	/**
	 * This method for getting scan history.
	 */
	static void getscanhistory()
	{
		final String getscanhistory = "http://localhost:8080/ScanSee/scanNow/getscanhistory?userId=772&lastVisitedRecord=0";
		final ClientRequest request = new ClientRequest(getscanhistory);
		try
		{
			final String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	
	/**
	 * This method for getting product for barcode.
	 */
	static void getSearchProdForName()
	{
		
		/*ProductDetail><userId>2</userId><searchkey><![CDATA[a]]></searchkey></ProductDetail>
		url = http://localhost:8080/ScanSee/scanNow/getSearchProdForName
		https://app.scansee.net/ScanSee/thisLocation
*/		final String getProdForBarCode ="https://app.scansee.net/ScanSee/scanNow/getProdForName";
		final ClientRequest request = new ClientRequest(getProdForBarCode);
//"<scanType>UPC-11</scanType>"
		//<ProductDetails><userId>2</userId><searchkey><![CDATA[a]]></searchkey></ProductDetails>
		final String getProdForBarCodeXml = "<ProductDetail>" + "<userId>2</userId>" +
		"<searchkey><![CDATA[XX]]></searchkey>" + "</ProductDetail>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProdForBarCodeXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void deletefromScanHistory()
	{

		String deleteWishList = "http://localhost:8080/ScanSee1.2.2/scanNow/deletescanhistory";
		ClientRequest request = new ClientRequest(deleteWishList);
		  String inputXml = "<ProductDetail>  <scanHistoryID>418</scanHistoryID>  <userId>2</userId></ProductDetail>";
		  request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		  		  		  
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("Delete Wish List response : " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	/**
	 * 
	 * This method for smart search 
	 *  for product name.
	 */
	static void getsmartsearchproducts()
	{
				
		final String getProdForName = "http://localhost:8080/ScanSee/scanNow/getsmartseaprods";
		final ClientRequest request = new ClientRequest(getProdForName);
				
		final String getProdForBarCodeXml = "<ProductDetail>"
												+ "<searchkey><![CDATA[coolpix]]></searchkey>"
												+ "<userId>2</userId>"
												+ "<mainMenuID>1</mainMenuID>"
											+ "</ProductDetail>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProdForBarCodeXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	/**
	 * 
	 * This method for smart search 
	 *  for product name.
	 */
	static void getsmartsearchCount()
	{
//		final String getProdForName = "http://199.36.142.83:8080/ScanSee2.2/scanNow/getsmartseacount";
		final String getProdForName = "https://app.scansee.net/ScanSee2.4/scanNow/getsmartseacount";
		final ClientRequest request = new ClientRequest(getProdForName);
					
//		<ProductDetail>
//		<searchkey><![CDATA[grapes]]></searchkey>
//		<parCatId><![CDATA[0]]></parCatId>
//		<lastVistedProductNo>0</lastVistedProductNo>
//		<mainMenuID>9525</mainMenuID>
//		</ProductDetail>
		
		String strBu = "baby food";
//		String keys [] = {"v", "i", "s", "i", "o", "n"};
//		StringBuilder strBu = new StringBuilder("tele");
//		for(int i = 0; i < keys.length; i++){
//			strBu.append(keys[i]);
			try
			{
				final String getProdForBarCodeXml = "<ProductDetail>"
					+ "<searchkey><![CDATA[" + strBu + "]]></searchkey>"
					+ "<parCatId>0</parCatId>" 
					+ "<lastVistedProductNo>0</lastVistedProductNo>"
					+ "<mainMenuID>45</mainMenuID>"
//					+ "<prodSmaSeaID>1</prodSmaSeaID>"
						+ "</ProductDetail>";
				request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProdForBarCodeXml);
				request.getHeaders();
				long strtTime = new Date().getTime();
				String response = request.postTarget(String.class);
				long endtime = new Date().getTime();
				System.out.println((endtime - strtTime));
				System.out.println("response " + response);
			}
			catch (Exception e)
			{
				System.out.println("exception in scansee rest easy client..");
			}
//		}
		

	}
	
	/**
	 * 
	 * This method for smart search 
	 *  for product name.
	 */
	static void getsmartsearchProductlst()
	{
		final String getProdForName = "https://app.scansee.net/ScanSee2.2/scanNow/getsearchprodlst";
		final ClientRequest request = new ClientRequest(getProdForName);
				
		
//		<ProductDetail>
//			<searchkey><![CDATA[Baby Food]]></searchkey>
//			<parCatId>4816112</parCatId>
//			<lastVistedProductNo>0</lastVistedProductNo>
//		</ProductDetail> 

		final String getProdForBarCodeXml = "<ProductDetail>"
												+ "<searchkey><![CDATA[Baby Food]]></searchkey>"
												+ "<parCatId>0</parCatId>" 
												+ "<lastVistedProductNo>0</lastVistedProductNo>"
											+"</ProductDetail>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProdForBarCodeXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	
	
	/**
	 * This method for product information based on the search key
	 */
	static void getProductInfo()
	{
		final String getscanhistory = "http://localhost:8080/ScanSee/scanNow/getProducts?searchkey=cookie";
		final ClientRequest request = new ClientRequest(getscanhistory);
		try
		{
			final String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
	
	static void getSmartSearchProducts1()
	{

		String getSmartSearchProducts = "http://10.11.202.76:8080/ScanSee2.3/scanNow/getsmartseaprods";
		//String getSmartSearchProducts = "http://localhost:8080/ScanSee/scanNow/getsmartsearchproducts";
		ClientRequest request = new ClientRequest(getSmartSearchProducts);
		String inputXml = "<ProductDetail><userId>3342</userId><searchkey><![CDATA[harry potter]]></searchkey><mainMenuID>9525</mainMenuID></ProductDetail>";
		request.body(MediaType.TEXT_XML, inputXml);
		  		  		  
		request.getHeaders();
		try
		{
			long strtTime = new Date().getTime();
			String response = request.postTarget(String.class);
			long endTime = new Date().getTime();
			System.out.println(endTime - strtTime);
			System.out.println("getSmartSearchProducts : " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	static void userTrackingGetMainMenuID()
	{

		String url = "http://localhost:8080/ScanSee/scanNow/utgetmainmenuid";
		
		ClientRequest request = new ClientRequest(url);
		String inputXml = "<UserTrackingData><userID>2</userID><moduleID>3</moduleID><postalCode><![CDATA[78752]]></postalCode></UserTrackingData>";
		
		request.body(MediaType.TEXT_XML, inputXml);  		  		  
		request.getHeaders();
		try
		{
			long strtTime = new Date().getTime();
			String response = request.postTarget(String.class);
			long endTime = new Date().getTime();
			System.out.println(endTime - strtTime);
			System.out.println("userTrackingGetMainMenuID : " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	
	public static void giveAwayReg() {
		final String strGiveReg = "http://localhost:8080/ScanSee/scanNow/giveawayuserregistration";
		ClientRequest request = new ClientRequest(strGiveReg);
		String strInputXml = "<AuthenticateUser><userId>2</userId><qrRetCustPageID>3</qrRetCustPageID></AuthenticateUser>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, strInputXml);
		request.getHeaders();
		try {
			final String response = request.postTarget(String.class);
			System.out.println("response giveAwayReg --->" + response);
		} catch (Exception e) {
			System.out.println("exception in response giveAwayReg");
		}
	}
	
	public static void giveAwayPageDetails() {
		final String strGiveReg = "http://localhost:8080/ScanSee/scanNow/giveawaypage";
		ClientRequest request = new ClientRequest(strGiveReg);
		String strInputXml = "<UserTrackingData><mainMenuID>100</mainMenuID><scanTypeID>50</scanTypeID><qrRetCustPageID>3</qrRetCustPageID></UserTrackingData>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, strInputXml);
		request.getHeaders();
		try {
			final String response = request.postTarget(String.class);
			System.out.println("response giveAwayReg --->" + response);
		} catch (Exception e) {
			System.out.println("exception in response giveAwayPageDetails");
		}
	}
}
