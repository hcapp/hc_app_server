//package com.scansee.rest.client;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;

import com.scansee.common.constants.ApplicationConstants;

public class ManageSettingsRestEasyClient
{

	static final String serverIp = "122.181.128.148:8008";

	
	private ManageSettingsRestEasyClient()
	{

	}

	
	public static void main(String[] args)
	{
		manageSettings();
//		testInsertUserInfo();
//		getUserSettings();

	}

	
	public static void manageSettings()
	{
		//testsetUserFavCategories();
//		testInsertUserInfo();
//		testChangePassword();
//		testsaveSettings();
//		testsetUserFavCategories();
		testFetchUserInfo();
//		testGetPrefCategory();
//		getpreferedretailers();
//		testsetPreferredRetailers();
//		testsetuserSettings();
//		getUserSettings();
	
	}

	static void testFetchUserInfo()
	{
		String URL = "http://localhost:8080/ScanSee/managesettings/fetchuserinfo?USERID=1";
//		String URL = "http://199.36.142.83/ScanSee2.1/managesettings/fetchuserinfo?USERID=2800";
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	static void testGetPrefCategory()
	{
		String URL = "http://localhost:8080/ScanSee/managesettings/getfavcategories?userId=1";
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	static void getpreferedretailers()
	{
		final String getpreferedretailers = "http://122.181.128.148:8080/ScanSee/managesettings/getpreferedretailers";
		String link = "http://localhost:8080/ScanSee/managesettings/getpreferedretailers";
		final ClientRequest request = new ClientRequest(link);
		final String getpreferedretailersXml = "<RetailerInfoReq>" + "<userID>1</userID>" + "<lastVisitedRecord>100</lastVisitedRecord>"
				+ "</RetailerInfoReq>";

		request.accept(ApplicationConstants.TEXTXML).body(MediaType.TEXT_XML, getpreferedretailersXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response for getpreferedretailers" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	static void testInsertUserInfo()
	{
		final String insertUserInfo = "http://localhost:8080/ScanSee2.1/managesettings/insertuserinfo";

		final ClientRequest request = new ClientRequest(insertUserInfo);

		final String saveUserInfo2= "<UserRegistrationInfo>" 
									+ "<userId>1</userId>" 
									+ "<firstName>sf</firstName>"
									+ "<lastName>fd</lastName>"
									+ "<postalCode>00651></postalCode>" 
									+ "<gender>1</gender>" 
									+ "<mobileNumber>080</mobileNumber>"
									+ "<deviceID>87878</deviceID>"
									+ "<email><![CDATA[sandeep_sr@spanservices.com]]></email>" 
									+ "<universityIDs>1</universityIDs>" 
								+ "</UserRegistrationInfo>";
		
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", saveUserInfo2);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response for testInsertUserInfo-------->" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	static void testsaveSettings()
	{

		String saveSettings = "http://localhost:8080/ScanSee/managesettings/savesettings";
		final ClientRequest request = new ClientRequest(saveSettings);
		final String saveSettingsInfo = "<UserPreference>" + "<userID>2</userID>" +
		"<localeRadius>99</localeRadius>"
				+ "<scannerSilent>1</scannerSilent>" + "<displayCoupons>1</displayCoupons>" 
				+ "<displayRebates>0</displayRebates>"
				+ "<displayLoyaltyRewards>1</displayLoyaltyRewards>" 
				+ "<savingsActivated>1</savingsActivated>" 
				+ "<sleepStatus>1</sleepStatus>"
				+ "</UserPreference>";

		request.accept(ApplicationConstants.TEXTXML).body(MediaType.TEXT_XML, saveSettingsInfo);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response for saveSettings-------->" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	static void testfetchsettings()
	{
		final String getpreferedretailers = "http://localhost:8080/ScanSee/managesettings/fetchsettings?userId=2";
		final ClientRequest request = new ClientRequest(getpreferedretailers);

		try
		{
			String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	static void testsetUserFavCategories()
	{

		String setUserFavCategories = "http://localhost:8080/ScanSee/managesettings/setfavcategories";
		final ClientRequest request = new ClientRequest(setUserFavCategories);
		final String setUserFavCategoriesInfo = "<Categories><userId>2805</userId><Category>" +
				"<categoryId>0</categoryId></Category><Category><categoryId>49</categoryId></Category><Category><categoryId>50</categoryId></Category>" +
				"<Category><categoryId>51</categoryId></Category><Category>	<categoryId>52</categoryId></Category><Category><categoryId>123</categoryId>" +
				"</Category><Category><categoryId>57</categoryId></Category><Category><categoryId>56</categoryId></Category><Category><categoryId>53</categoryId>" +
				"</Category><Category><categoryId>55</categoryId></Category><Category><categoryId>54</categoryId></Category><Category><categoryId>1</categoryId></Category><Category>" +
				"<categoryId>159</categoryId></Category><Category><categoryId>234</categoryId></Category><Category><categoryId>236</categoryId></Category>" +
				"<Category><categoryId>235</categoryId></Category><Category><categoryId>161</categoryId></Category><Category><categoryId>160" +
				"</categoryId></Category><Category>	<categoryId>158</categoryId></Category><Category><categoryId>37</categoryId></Category>" +
				"<Category><categoryId>120</categoryId></Category><Category><categoryId>264</categoryId></Category><Category>" +
				"<categoryId>119</categoryId></Category><Category><categoryId>163</categoryId></Category><Category><categoryId>118</categoryId>" +
				"</Category><Category><categoryId>265</categoryId></Category><Category><categoryId>11</categoryId></Category><Category>" +
				"<categoryId>3</categoryId></Category></Categories>";

		String info = "<Categories><userId>1</userId><Category>"
			+ "<categoryId>115</categoryId></Category>"
			+ "<Category><categoryId>114</categoryId></Category>"
			+ "<Category><categoryId>111</categoryId>"
			+ "</Category><Category><categoryId>113</categoryId></Category>"
			+ "</Categories>";

		/*<Categories>
		<userId>163</userId>
		<Category>
			<categoryId>109</categoryId>
		</Category>
		<Category>
			<categoryId>110</categoryId>
		</Category>
		<Category>
			<categoryId>111</categoryId>
		</Category>
		<Category>
			<categoryId>113</categoryId>
		</Category>
	</Categories>*/
		request.accept(ApplicationConstants.TEXTXML).body(MediaType.TEXT_XML, info);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response for testsetUserFavCategories-------->" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	static void testsetPreferredRetailers()
	{

		String setPreferredRetailers = "http://localhost:8080/ScanSee/managesettings/setpreferedretailers";
		final ClientRequest request = new ClientRequest(setPreferredRetailers);
		final String setPreferredRetailersInfo = "<RetailerDetails>" + "<userId>1</userId>" + "<RetailerDetail>" + "<retailerId>4</retailerId>"
				+ "</RetailerDetail>" + "<RetailerDetail>" + "<retailerId>8</retailerId>" + "</RetailerDetail>" + "</RetailerDetails>";

		String temp = "<RetailerDetails>"
			+ "<userId>1</userId>"
			+ "<RetailerDetail><retailerId>214</retailerId></RetailerDetail>"
			+ "<RetailerDetail><retailerId>155</retailerId></RetailerDetail>"
			+ "</RetailerDetails>";
		
		request.accept(ApplicationConstants.TEXTXML).body(MediaType.TEXT_XML, temp);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response for testsetPreferredRetailers-------->" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	static void testChangePassword()
	{
		final String insertUserInfo = "http://localhost:8080/ScanSee/managesettings/changepassword";
		final ClientRequest request = new ClientRequest(insertUserInfo);
		final String saveUserInfo = "<UserRegistrationInfo><userId>188</userId>" + "<password>A</password>" + "</UserRegistrationInfo>";

		request.accept(ApplicationConstants.TEXTXML).body(MediaType.TEXT_XML, saveUserInfo);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response for testChangePassword-------->" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	static void testsetuserSettings()
	{
		final String insertUserInfo = "http://localhost:8080/ScanSee1.4.0/managesettings/setusersettings";
		final ClientRequest request = new ClientRequest(insertUserInfo);
		final String saveUserInfo = "<UserSettings>"
						+ "<userId>2646</userId>"
						+ "<localeRadius>10</localeRadius>"
						+ "<pushNotify>true</pushNotify>"
						+ "</UserSettings>";

		request.accept(ApplicationConstants.TEXTXML).body(MediaType.TEXT_XML, saveUserInfo);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response for testChangePassword-------->" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	static void getUserSettings()
	{
		String URL = "http://localhost:8080/ScanSee2.1/managesettings/getusersettings";
		ClientRequest request = new ClientRequest(URL);

		final String saveUserInfo = "<AuthenticateUser>" 
										+ "<userId>2451</userId>"
										+ "<deviceID>cb089da08a3f4798eeb875eeaf31910d9d7cd137</deviceID>"
									+ "</AuthenticateUser>";

		request.accept(ApplicationConstants.TEXTXML).body(MediaType.TEXT_XML, saveUserInfo);
		request.getHeaders();
		try {
			final String response = request.postTarget(String.class);
			System.out.println("response for testChangePassword-------->" + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
