//package com.scansee.rest.client;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;

public class WishListRestClient
{
	
	public static String URL_fecthProductDetails="http://localhost:8080/ScanSee/wishlist/fetchproductdetails";
	
	public static String URL_fecthWithListItems="http://localhost:8080/ScanSee/wishlist/getWishListItems";
	
	public static String URL_fetchWishListProductItems  ="http://localhost:8080/ScanSee/wishlist/getWishListProductItems?USERID=1&searchKey=Green";
	
	
	public static void main(String args[])
	{
		
	
		//addWishListProd();
//		deletefromWishList();
	//	addunassignedProd();
		//executeWishListMethods();
		//getMyGalleryInfo();
		getprodhotdeal();
		//addunassignedProd();
//		getWishListItems();
//		fetchCouponInfo();
//		getHotDealInfoForProduct();
//		getWishListStoreDetails();
//		addWishListProd();
		//getMyGalleryInfo();
//		getWishListItems();
	}

	
	
	
	public static void executeWishListMethods()
	{
		System.out.println("************Execution starts for addWishListProd functionality**************\n");
		
		addWishListProd();
		
		System.out.println("************Execution ends for deletefromWishList functionality**************\n");
		
		System.out.println("************Execution starts for deletefromWishList functionality**************\n");
		
		
		deletefromWishList();
		
		System.out.println("************Execution ends for deletefromWishList functionality**************\n");
		
		
		System.out.println("************Execution starts for addunassignedProd functionality**************\n");

		
		addunassignedProd();
		
		
		System.out.println("************Execution ends for addunassignedProd functionality**************\n");
		
		
	}

	static void addWishListProd()
	{
		/*<ProductDetailsRequest>
		<userId>4</userId>
		<productDetails>
			<ProductDetail>
				<productId>1145731</productId>
				<productName>00714-0 Hardskin touch Case for iPod touch</productName>
			</ProductDetail>
		</productDetails>
	</ProductDetailsRequest>*/

		
		/*ProductDetailsRequest><userId>4</userId><productDetails>
		<ProductDetail><productId>42766</productId><productName>Office 2010 Home and Business 
		- 32/64-bit</productName></ProductDetail></productDetails></ProductDetailsRequest>*/
		
		String addWishListProd = "http://localhost:8080/ScanSee/wishlist/addWishListProd";
		ClientRequest request = new ClientRequest(addWishListProd);
		  String inputXml = "<ProductDetailsRequest>" 
			  					+ "<userId>3</userId>" 
			  					+ "<productDetails>" 
			  						+ "<ProductDetail>" 
			  							+ "<productId>4569</productId>" 
//			  							+ "<productName><![CDATA[water |~| in Others]]></productName>" 
			  						+ "</ProductDetail>" 
			  					+ "</productDetails>" 
			  					+ "<mainMenuID>1</mainMenuID>"
			  			  + "</ProductDetailsRequest>" ;
		 

			request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("Add Wish List response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}


	
	static void getprodhotdeal()
	{
		String addWishListProd = "http://localhost:8080/ScanSeeVM1/wishlist/getprodhotdeal";
		ClientRequest request = new ClientRequest(addWishListProd);
		
		String inputXml = "<ThisLocationRequest><userId>3342</userId><productId>4546</productId><mainMenuID>9905</mainMenuID><alertProdID>1642</alertProdID></ThisLocationRequest>";
		
/*		String inputXml = "<ThisLocationRequest>" 
								+ "<userId>10415</userId>" 
								+ "<zipcode>75007</zipcode>" 
								+ "<productId>273298</productId>" 
								+ "<mainMenuID>4491</mainMenuID>" 
								+ "<alertProdID>664</alertProdID>" 
							+ "</ThisLocationRequest>";
*/
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("Add Wish List response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	static void deletefromWishList()
	{

		String deleteWishList = "http://localhost:8080/ScanSee1.4.0/wishlist/deletewlhistory";
		ClientRequest request = new ClientRequest(deleteWishList);
		  String inputXml = "<ProductDetail>  <userProductId>13</userProductId>  <userId>2</userId></ProductDetail>";
	
		  		  
		  
		  request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("Delete Wish List response : " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	
	
	
	static void addunassignedProd()
	{

		String addunassignedProd= "http://localhost:8080/ScanSee1.4.0/wishlist/addunassignedProd";
		ClientRequest request = new ClientRequest(addunassignedProd);
		
		  String inputXml = "<ProductDetailsRequest>" +
		  "<userId>69</userId>" +
	     	       "<productName>NonAvailableProdcut22</productName>" +
	       "<productDescription>testing the unassigned22</productDescription>" +
	        "</ProductDetailsRequest>";

		  request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("AddunassignedProd response : " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	
	}
	static void getMyGalleryInfo()
	{

		final String getMyGalleryInfo = "http://199.36.142.82:8080/ScanSee/mygallery/getMyGalleryInfo";
		final ClientRequest request = new ClientRequest(getMyGalleryInfo);
		final String getMyGalInfXml = "<CLRDetails>" + "<userId>4</userId>" + "<type>All</type>" + "<lowerLimit>0</lowerLimit>" +"<clrC>0</clrC>" 
		+ "<clrR>0</clrR>"
		+ "<clrL>0</clrL>"+ "</CLRDetails>";
		
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getMyGalInfXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}

	static void getWishListItems()	{
		final String url = "http://localhost:8080/ScanSee/wishlist/getWishListItems";
		final ClientRequest request = new ClientRequest(url);
		
		String data = "<ThisLocationRequest>" 
						+ "<userId>2</userId>" 
						+ "<zipcode>78752</zipcode>" 
						//+ "<latitude>111</latitude>" 
						//+ "<longitude></longitude>" 
//						+ "<radius></radius>" 
						+ "<lastVisitedRecord>0</lastVisitedRecord>"
						+ "<moduleID>5</moduleID>" 
					+ "</ThisLocationRequest>";
		
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, data);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
	
	static void fetchCouponInfo()	{
		final String url = "http://localhost:8080/ScanSee/wishlist/fetchwishlistcouponinfo";
		final ClientRequest request = new ClientRequest(url);
		
		String data = "<ThisLocationRequest>" 
						+ "<userId>2</userId>"
						+ "<productId>1</productId>"
//						+ "<zipcode></zipcode>" 
//						+ "<latitude></latitude>" 
//						+ "<longitude></longitude>" 
//						+ "<radius>5</radius>" 
						+ "<mainMenuID>1</mainMenuID>" 
						+ "<alertProdID>50</alertProdID>"
					+ "</ThisLocationRequest>";
		
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, data);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
	
	static void getHotDealInfoForProduct()	{
		final String url = "http://localhost:8080/ScanSee/wishlist/getprodhotdeal";
		final ClientRequest request = new ClientRequest(url);
		
		String data = "<ThisLocationRequest>" 
						+ "<userId>2</userId>" 
						+ "<productId>6</productId>"
						+ "<zipcode>75243</zipcode>" 
//						+ "<latitude></latitude>" 
//						+ "<longitude></longitude>" 
//						+ "<radius></radius>" 
						+ "<mainMenuID>379</mainMenuID>" 
						+ "<alertProdID>125</alertProdID>"
					+ "</ThisLocationRequest>";
		
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, data);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
	
	static void getWishListStoreDetails()	{
		final String url = "http://localhost:8080/ScanSee/wishlist/getstoredetails";
		final ClientRequest request = new ClientRequest(url);
		
		String data = "<ThisLocationRequest>" 
						+ "<userId>2</userId>" 
						+ "<productId>1</productId>"
//						+ "<zipcode></zipcode>" 
//						+ "<latitude></latitude>" 
//						+ "<longitude></longitude>" 
//						+ "<radius>5</radius>" 
						+ "<moduleID>1</moduleID>" 
						+ "<alertProdID>50</alertProdID>"
					+ "</ThisLocationRequest>";
		
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, data);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
}
	
	
	
	
	
	
	


