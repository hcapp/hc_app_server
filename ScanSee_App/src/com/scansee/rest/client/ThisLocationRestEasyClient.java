//package com.scansee.rest.client;

import java.util.Date;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;

/**
 * This class contains all this location requests..
 * 
 * @author shyamsundara_hm
 */
public class ThisLocationRestEasyClient
{

	/**
	 * constructor for ThisLocationResteasyclient.
	 */
	public ThisLocationRestEasyClient()
	{

	}

	/**
	 * Main method.
	 * 
	 * @param args
	 *            as the request.
	 */
	public static void main(String[] args)
	{


		scanRequests();

	}

	/**
	 * This method contains all the Thislocation requests.
	 */
	public static void scanRequests()
	{
		// System.out.println("=================Before getRetailersInfo===================\n");
		getRetailersInfo();
//		getProducts();
//		 testFindnearbyproducts();
		// System.out.println("=================After getRetailersInfo===================\n");

		// System.out.println("=================After getProducts===================\n");
		// System.out.println("=================Before advertisementhit===================\n");
		// advertisementhit();
		// System.out.println("=================After advertisementhit===================\n");

		// for save user sale notification
		// saveUserRetSaleNotification();
//	fetchRetailerStoreDetails();

//		getretsummary();
//	getretSpecialOfferDetails();
		
	//	saveUserRetSaleNotification();
		
	//	getRetailerSpecialOffInfo();
		//getretSpecialOfferDetails();
	//	getretSpecialOfferDetails();
//	getretHotDeals();
//	testGetRetailerCLRInfo();
		//testGetRetailerCLRInfo();
		
//	getSpecialOfferHotdealCLRflags();
//		testGetSpecialDealDetails();
		//getretSpecialOfferDetails();
		//fetchRetailerStoreDetails();
		//getretsummary();
//	getretHotDeals();
//	getRetailersByGroupID();
//		getRetailersForPartner();
//		testGetPartners();
//		getCategoriesForGroupRetailers();
//		getCategoriesForPartners();
//		testUserTrackingLocationDetails(); 
//		userTrackingRetailerSummaryClick(); 
//		userTrackingRetSpeOffersClick(); 
		//userTrackingGetHotDealClick();
//		getProducts();
	}

	/**
	 * This method for getting product for barcode.
	 */
	static void getRetailersInfo()
	{
//		final String getRetailersInfo = "http://10.11.202.76:8080/ScanSee2.1/thisLocation/getRetailersInfo";
		final String getRetailersInfo = "https://app.scansee.net/ScanSee2.3/thisLocation/getRetailersInfo";
		final ClientRequest request = new ClientRequest(getRetailersInfo);
		// If GPS is off..<lat>18.00529</lat><lng>-66.61231</lng>
		
		final String getRetailersInfo1Xml = "<ThisLocationRequest>" 
												+ "<preferredRadius>20</preferredRadius>" 
												+ "<zipcode>78725</zipcode>"
//												+ "<latitude>42.8102</latitude>"
//												+ "<longitude>-73.9507</longitude>"
												+ "<userId>7693</userId>" 
												+ "<gpsEnabled>false</gpsEnabled>" 
												+ "<lastVisitedRecord>0</lastVisitedRecord>" 
//												+ "<mainMenuID>315</mainMenuID>"
												+ "<moduleID>1</moduleID>"
												+ "<locOnMap>false</locOnMap>"
											+ "</ThisLocationRequest>";

		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getRetailersInfo1Xml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}

	static void getCategoryInfo()
	{
		
		final String getCategoryInfo = "http://localhost:8080/ScanSee1.4.0/thisLocation/getCategoryInfo";
		final ClientRequest request = new ClientRequest(getCategoryInfo);
		// If GPS is off..
		final String getCategoryInfoXml = "<CategoryRequest><retailerId>550</retailerId><retailerLocationId>91357</retailerLocationId></CategoryRequest>";


		// request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML,
		// getCategoryInfoXml);
				request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}

	static void getProducts()
	{
		
		final String getProducts = "http://localhost:8080/ScanSee/thisLocation/getProducts";
		
		final ClientRequest request = new ClientRequest(getProducts);
			
		final String getProductsXML2 = "<ProductDetailsRequest>"
										+ "<userId>2</userId>"
										+ "<retailLocationID>92952</retailLocationID>"
										+ "<lastVisitedProductNo>0</lastVisitedProductNo>"
										+ "<retailID>31</retailID>"
										+ "<retListID>2</retListID>"
										+"<mainMenuID>1</mainMenuID>"
									+ "</ProductDetailsRequest>";
		
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProductsXML2);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}

	static void advertisementhit()
	{
		final String advertisementhit = "http://localhost:8080/ScanSee/thisLocation/advertisementhit";
		final ClientRequest request = new ClientRequest(advertisementhit);
		// If GPS is off..
		final String advertisementhitXml = "<AdvertisementHitReq>" + "<userId>2</userId>" + "<advertisementId>1</advertisementId>"
				+ "</AdvertisementHitReq>";

		request.accept("text/xml").body(MediaType.TEXT_XML, advertisementhitXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}

	static void testFindnearbyproducts()
	{
		

		final String advertisementhit = "http://localhost:8080/ScanSee/thisLocation/findnearbyproducts";
		final ClientRequest request = new ClientRequest(advertisementhit);
		// If GPS is off..
		final String advertisementhitXml = "<ProductDetail>" + "<userId>2</userId>" + "<lastVistedProductNo>0</lastVistedProductNo>"
				+ "<zipcode>75244</zipcode>" + "<searchkey>a</searchkey>" + "</ProductDetail>";

		/*
		 * <ProductDetail> <userId>265</userId>
		 * <searchkey><![CDATA[a]]></searchkey> <gpsEnabled>false</gpsEnabled>
		 * <zipcode>75244</zipcode> <lastVistedProductNo>0</lastVistedProductNo>
		 * </ProductDetail>
		 */

		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, advertisementhitXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}

	static void saveUserRetSaleNotification()
	{
		final String saveUsrRetSaleNot = "http://10.11.202.76:8080/ScanSee/thisLocation/saveusrretsalehit";
		final ClientRequest request = new ClientRequest(saveUsrRetSaleNot);

		String saveUsrRetSaleNotxml = "<RetailerDetail><userId>2</userId><retailLocationID>92827</retailLocationID><retailerId>970</retailerId></RetailerDetail>";

		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, saveUsrRetSaleNotxml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}

	static void fetchRetailerStoreDetails()
	{
		final String saveUsrRetSaleNot = "http://localhost:8080/ScanSee/thisLocation/getretstoreinfo";
		final ClientRequest request = new ClientRequest(saveUsrRetSaleNot);

		String getretstoreinfoxml = "<RetailerDetail><userId>2</userId>" 
										+ "<retailLocationID>2</retailLocationID>"
										+ "<retailerId>31</retailerId>" 
										+ "<pageID>3</pageID>" 
										+ "<mainMenuID>25</mainMenuID>"
										+ "<scanTypeID>1</scanTypeID>"  
									+ "</RetailerDetail>";

		request.accept("text/xml").body(MediaType.TEXT_XML, getretstoreinfoxml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}

	static void getretsummary()
	{
		final String saveUsrRetSaleNot = "http://199.36.142.83:8080/ScanSee2.2/thisLocation/getretsummary";
		final ClientRequest request = new ClientRequest(saveUsrRetSaleNot);

	
		String getretstoreinfoxml = "<RetailerDetail>"
										+ "<userId>7693</userId>"
										+ "<postalCode>59721</postalCode>" 
										+ "<retailLocationID>1279180</retailLocationID>"
										+ "<retailerId>78638</retailerId>"
										+ "<mainMenuID>3301</mainMenuID>"
										+ "<scanTypeID>0</scanTypeID>"
										+ "<retListID>27788</retListID>"
									+ "</RetailerDetail>";

		request.accept("text/xml").body(MediaType.TEXT_XML, getretstoreinfoxml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
	
	static void getretSpecialOfferDetails()
	{
		final String saveUsrRetSaleNot = "http://localhost:8080/ScanSee2.3/thisLocation/getspedealslist";
		final ClientRequest request = new ClientRequest(saveUsrRetSaleNot);

		String getretstoreinfoxml = "<RetailerDetail>"
										+ "<userId>1</userId>" 
										+ "<retailLocationID>92618</retailLocationID>"
										+ "<retailerId>1046</retailerId>" 
//										+ "<retListID>5886</retListID>"
									+ "</RetailerDetail>";

		request.accept("text/xml").body(MediaType.TEXT_XML, getretstoreinfoxml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
	
	static void getRetailerSpecialOffInfo()
	{

		final String getProducts = "http://localhost:8080/ScanSee1.4.0/thisLocation/getretspeoffinfo";
		
		final ClientRequest request = new ClientRequest(getProducts);
			
		final String getProductsXML2 = "<RetailerDetail><userId>2998</userId>"
				+ "<retailLocationID>6128</retailLocationID><retailerId>31</retailerId></RetailerDetail>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProductsXML2);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}

	static void testGetRetailerCLRInfo()
	{

		final String getProducts = "http://199.36.142.83:8080/ScanSee2.1/thisLocation/getretclrinfo";
		
		final ClientRequest request = new ClientRequest(getProducts);
			
		final String getProductsXML2 = "<RetailerDetail>"
										+"<userId>25</userId>"
										+ "<retailLocationID>2017250</retailLocationID>"
										+ "<lastVisitedNo>0</lastVisitedNo>"
										+ "<retailerId>1067060</retailerId>"
										+ "<mainMenuID>1</mainMenuID>"
										+ "<retListID>2</retListID>"
									+ "</RetailerDetail>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProductsXML2);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void getSpecialOfferHotdealCLRflags()
	{
		/*703
		RetailLocationID = 91895*/

		final String getProducts = "http://199.36.142.83:8080/ScanSee2.2/thisLocation/getretspcialoffs";
		
		final ClientRequest request = new ClientRequest(getProducts);
			
		final String getProductsXML2 = "<ProductDetailsRequest><userId>7693</userId>"
				+ "<retailLocationID>1279180</retailLocationID><retailID>78638</retailID></ProductDetailsRequest>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProductsXML2);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void getretHotDeals()
	{
		final String saveUsrRetSaleNot = "http://199.36.142.83:8080/ScanSee2.2/thisLocation/getrethotdeals";
		final ClientRequest request = new ClientRequest(saveUsrRetSaleNot);
// retaillocationID 13:06 
	
	//<RetailerDetail><userId>3054</userId><retailerId>696</retailerId><retailLocationID>91875</retailLocationID><lastVisitedNo>0</lastVisitedNo></RetailerDetail>HTTP
		String getretstoreinfoxml = "<RetailerDetail>"
				+ "<userId>7693</userId>"
				+ "<retailerId>78638</retailerId>"
				+ "<retailLocationID>1279180</retailLocationID>"
				+ "<lastVisitedNo>0</lastVisitedNo>"
				+ "<retListID>27788</retListID>"
//				+ "<mainMenuID>1</mainMenuID>"
				+ "</RetailerDetail>";

		request.accept("text/xml").body(MediaType.TEXT_XML, getretstoreinfoxml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
	
	static void testGetSpecialDealDetails()
	{

		final String getProducts = "http://localhost:8080/ScanSee/thisLocation/getspdealinfo";
		
		final ClientRequest request = new ClientRequest(getProducts);
			
		final String getProductsXML2 = "<RetailerDetail>"
											+ "<pageID>2</pageID>"
											+ "<retailLocationID>6128</retailLocationID>"
											+ "<retailerId>31</retailerId>"
										+ "</RetailerDetail>";
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProductsXML2);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void getRetailersByGroupID()
	{
		final String getRetailersInfo = "http://localhost:8080/ScanSee/thisLocation/getretbygroupid";
		final ClientRequest request = new ClientRequest(getRetailersInfo);
//		
//		final String getRetailersInfo1Xml = "<ThisLocationRequest>" 
//										+ "<zipcode>78729</zipcode>"
//										+ "<userId>1</userId>" 
//										+ "<gpsEnabled>false</gpsEnabled>" 
//										+ "<lastVisitedRecord>0</lastVisitedRecord>" 
//										+ "<retGroupID>1</retGroupID>"
//										+ "<catID>0</catID>"
////										+ "<searchKey>bella</searchKey>"
////										+ "<longitude>-121.485833</longitude>"
////										+ "<latitude>45.799444</latitude>"
//										+ "<moduleID>1</moduleID>"
//										  + "</ThisLocationRequest>";
		
		
		final String getRetailersInfo1Xml = "<ThisLocationRequest>" 
			+ "<zipcode>78752</zipcode>"
			+ "<userId>2121</userId>" 
			+ "<gpsEnabled>false</gpsEnabled>" 
			+ "<lastVisitedRecord>0</lastVisitedRecord>" 
			+ "<retGroupID>1</retGroupID>"
			+ "<catID>0</catID>"
//			+ "<searchKey>bella</searchKey>"
//			+ "<longitude>-121.485833</longitude>"
//			+ "<latitude>45.799444</latitude>"
//			+ "<locOnMap>false</locOnMap>"
//			+ "<moduleID>1</moduleID>"
			+ "<mainMenuID>25</mainMenuID>"
			  + "</ThisLocationRequest>";

		final String getRetailersInfo2Xml = "<ThisLocationRequest>" + "<preferredRadius>20</preferredRadius>" + "<longitude>-73.9974</longitude>"
				+ "<latitude>40.7507</latitude>" + "<userId>1</userId>" + "<gpsEnabled>true</gpsEnabled>" + " </ThisLocationRequest>";

		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getRetailersInfo1Xml);
		request.getHeaders();
		try
		{
			long strtTime = new Date().getTime();
			final String response = request.postTarget(String.class);
			long endtime = new Date().getTime();
			System.out.println((endtime - strtTime));
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void getRetailersForPartner()
	{
		final String getRetailersInfo = "http://localhost:8080/ScanSee/thisLocation/getretforpartner";
		final ClientRequest request = new ClientRequest(getRetailersInfo);
		
		final String getRetailersInfo1Xml = "<ThisLocationRequest>" 
										+ "<zipcode>78729</zipcode>"
										+ "<userId>3</userId>" 
										+ "<gpsEnabled>false</gpsEnabled>" 
										+ "<lastVisitedRecord>0</lastVisitedRecord>" 
										+ "<retAffID>1</retAffID>"
										+ "<catID>0</catID>"
//										+ "<searchKey>bella</searchKey>"
										+ "<moduleID>1</moduleID>"
										  + "</ThisLocationRequest>";
		
		String xml = "<ThisLocationRequest>"
					+ "<userId>3</userId>"
					+ "<gpsEnabled>true</gpsEnabled>"
					+ "<lastVisitedRecord>0</lastVisitedRecord>"
					+ "<retAffID>1</retAffID>"
					+ "<longitude>-121.485833</longitude>"
					+ "<latitude>45.799444</latitude>"
					+ "<catID>0</catID>"
//					+ "<searchKey></searchKey>"
					+ "<moduleID>1</moduleID>"
					+ "</ThisLocationRequest>";

		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getRetailersInfo1Xml);
		request.getHeaders();
		try
		{
			long strtTime = new Date().getTime();
			final String response = request.postTarget(String.class);
			long endtime = new Date().getTime();
			System.out.println((endtime - strtTime));
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}

	static void testGetPartners()
	{

		final String url = "http://10.11.202.76:8080/ScanSee1.5.1/thisLocation/getpartners";
		
		final ClientRequest request = new ClientRequest(url);
			
		try
		{
			final String response = request.getTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
	
	static void getCategoriesForGroupRetailers()
	{

		final String url = "http://localhost:8080/ScanSee/thisLocation/getcatforgroup?retGroupID=1";
		
		final ClientRequest request = new ClientRequest(url);
			
		try
		{
			final String response = request.getTarget(String.class);
			System.out.println("getCategoriesForGroupRetailers " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void getCategoriesForPartners()
	{

		final String url = "http://localhost:8080/ScanSee1.5.1/thisLocation/getcatforpartner?retAffID=1";
		
		final ClientRequest request = new ClientRequest(url);
			
		try
		{
			final String response = request.getTarget(String.class);
			System.out.println("getCategoriesForPartners " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}

	static void testUserTrackingLocationDetails() 
	{

		final String getProducts = "http://localhost:8080/ScanSee/thisLocation/utlocationdetails";
		
		final ClientRequest request = new ClientRequest(getProducts);
			
		final String getProductsXML2 = "<UserTrackingData>"
							+ "<locateOnMap>false</locateOnMap>"
							+ "<latitude>30.45</latitude>"
							+ "<longitude>-97.773</longitude>"
							+ "<mainMenuID>1</mainMenuID>"
							+ "<postalCode>78729</postalCode>"
							+ "</UserTrackingData>";
		
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProductsXML2);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void userTrackingRetailerSummaryClick() 
	{
//		final String getProducts = "http://localhost:8080/ScanSee/thisLocation/utretsumclick";
		final String getProducts = "http://10.11.202.76:8080/ScanSee1.5.3/thisLocation/utretsumclick";
		
		final ClientRequest request = new ClientRequest(getProducts);
			
		final String getProductsXML2 = "<UserTrackingData>"
							+ "<retDetailsID>137</retDetailsID>"
							+ "<anythingPageListID>126</anythingPageListID>"
							+ "<retListID>3434</retListID>"
							+ "<banADClick>false</banADClick>"
							+ "<callStoreClick>false</callStoreClick>"
							+ "<browseWebClick>false</browseWebClick>"
							+ "<getDirClick>false</getDirClick>"
							+ "<anythingPageClick>true</anythingPageClick>"
								+ "</UserTrackingData>";
		
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProductsXML2);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void userTrackingRetSpeOffersClick()
	{
		final String getProducts = "http://localhost:8080/ScanSee/thisLocation/utretspeoffclick";
		
		final ClientRequest request = new ClientRequest(getProducts);
			
		final String getProductsXML2 = "<UserTrackingData>"
							+ "<userID>1</userID>"
							+ "<retListID>1</retListID>"
							+ "<speListID>1</speListID>"
							+ "<speOffClick>true</speOffClick>"
								+ "</UserTrackingData>";
		
		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProductsXML2);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}

	}
	
	static void userTrackingGetHotDealClick(){
		final String getProducts = "http://10.11.202.76:8080/ScanSee1.5.3/hotDeals/utgethotdealclick?userId=3342&hotDealId=1450890&hotDealListID=1506";
		
		final ClientRequest request = new ClientRequest(getProducts);
			
		final String getProductsXML2 = "<UserTrackingData>"
							+ "<userID>3342</userID>"
							+ "<hotDealID>1450890</hotDealID>"
							+ "<hotDealListID>1506</hotDealListID>"
								+ "</UserTrackingData>";
		
//		request.accept("text/xml;charset=UTF-8").body(MediaType.TEXT_XML, getProductsXML2);
//		request.getHeaders();
		try
		{
			final String response = request.getTarget(String.class);
			System.out.println("response in userTrackingGetHotDealClick" + response);
		}
		catch (Exception e)
		{
			System.out.println("exception in scansee rest easy client..");
		}
	}
	
}
