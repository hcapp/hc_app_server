//package com.scansee.rest.client;

import java.util.Date;

import org.jboss.resteasy.client.ClientRequest;

public class FindRestEasyClient
{

	public static void main(String[] args)
	{

		executeFind();

	}

	public static void executeFind()
	{
		//testfindgooretsearh();
		
//	testfindcatsearch();
//	testfindretsearh();
//	testfindcatsearch();
//		testGetcategory();
//		 testFindlocdetails();
		// testfindretsearh();
//		 userTrackingGoogleRetailers();
//	testfindretsearh();
	//testfindcatsearch();
	//testGetcategory();
	//testFindlocdetails();
	//userTrackingGoogleRetailers();
	
//	findCatSearchForSSData();
		testfindGoogleCatSearch();
//		findRetSearchForSSData();
//		testfindGoogleRetSearch();
	}

	public static void testfindcatsearch()
	{
	
		String logon = "http://localhost:8080/ScanSee/find/findcatsearch";
//		String logon = "http://localhost:8080/ScanSee/find/findcatsearch";
//		String logon = "https://app.scansee.net/ScanSee1.5.1/find/findcatsearch";
	 	
		final ClientRequest request = new ClientRequest(logon);
	
		String inputXml = "<ServiceSerachRequest>"
							+ "<userId>3343</userId>" 
							+ "<lat>32.910347</lat>" 
							+ "<lng>-96.728472</lng>" 
							+ "<type>Grocery</type>" 
							+ "<lastVisitedNo>0</lastVisitedNo>"
							//For testing find implementation to separate scansee and google data
//							+ "<reqData>google</reqData>"
						+ "</ServiceSerachRequest>";
		
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);

		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in save user media" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	
	public static void testfindretsearh()
	{
	
		 String logon = "http://localhost:8080/ScanSee/find/findretsearh";
		final ClientRequest request = new ClientRequest(logon);
	
	//<ServiceSerachRequest><userId>3341</userId><lat>33.5892</lat><lng>-91.8088</lng><keyword>baby food</keyword></ServiceSerachRequest>	
		
		String inputXml = "<ServiceSerachRequest>" 
							+ "<userId>2</userId>" 
							+ "<lat>32.9312475</lat><lng>-96.84046</lng>" 
							+ "<keyword>test</keyword>" 
							+ "<mainMenuID>1</mainMenuID>"
						+ "</ServiceSerachRequest>";
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);

		request.getHeaders();
		try
		{
			long strtTime = new Date().getTime();
			final String response = request.postTarget(String.class);
			long endtime = new Date().getTime();
			System.out.println((endtime - strtTime));
			System.out.println("response in save user media" + response);
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static void testGetcategory()
	{
		final String logon = "http://199.36.142.83:8080/ScanSee2.4/find/getcategory";
		String inputXml = " <UserTrackingData>"+
							"<userID>7693</userID>"+
							"<moduleID>2</moduleID>"+
							//"<postalCode>78729</postalCode>"+
							"<longitude>77.576300</longitude><latitude>12.946500</latitude>"+
							"</UserTrackingData>";
		final ClientRequest request = new ClientRequest(logon);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in testGetcategory -->" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void testFindlocdetails()
	{

		final String findlocdetails = "http://localhost:8080/ScanSee1.4.0/find/findlocdetails";
		final ClientRequest request = new ClientRequest(findlocdetails);
		final String inputXml = "<ServiceSerachRequest>"
				+ "<userId>2</userId><reference>CmRgAAAAo2AflR4h-o_AuZ92Mvx46ujUPAAYnmXQDuSzBcNwn62VDxm4q9v_UGuB3R7RkoSxuFQAztCIj0AwkcWZX3StWAVQDheylsZo9bt4Zzkawt4NZiK-6-UO0mgdta66j9JiEhBH_lwxoNIKgyrLpL29ItE1GhQgYRFaXmM6rqreJgex22lktMAkSw"
				+ "</reference></ServiceSerachRequest>";

		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in findlocdetails" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	public static void testfindsscategory()
	{
	
		 String logon = "http://localhost:8080/ScanSee1.3.0/find/findsscatsearch";

		final ClientRequest request = new ClientRequest(logon);
	
		String inputXml = "<ServiceSerachRequest><userId>4335</userId><lat>33.014600</lat><lng>-96.893700</lng><type>Grocery</type>"
				+"<lastVisitedNo>0</lastVisitedNo></ServiceSerachRequest>" ;
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);

		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in save user media" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void testfindssretsearch()
	{
	
		 String logon = "http://localhost:8080/ScanSee1.3.0/find/findssretsearch";

		final ClientRequest request = new ClientRequest(logon);
	
		String inputXml = "<ServiceSerachRequest><userId>4335</userId><lat>33.014600</lat><lng>-96.893700</lng><keyword>dodda</keyword>"
				+ "<lastVisitedNo>0</lastVisitedNo></ServiceSerachRequest>";
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);

		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in save user media" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void userTrackingGoogleRetailers()
	{
	
		 String logon = "http://localhost:8080/ScanSee/find/utgoogleret";

		final ClientRequest request = new ClientRequest(logon);
	
		String inputXml = "<RetailerDetail>"
							+ "<mainMenuID>1</mainMenuID>"
							+ "<findCatID>1</findCatID>"
							+ "<retailerName>aasd</retailerName>"
							+ "<completeAddress>adssad</completeAddress>"
							+ "<distance>10.5</distance>"
							+ "<findRetSeaID>2</findRetSeaID>"
						+ "</RetailerDetail>";
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);

		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in save user media" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void findCatSearchForSSData()	
	{
		
//		String logon = "http://10.11.202.76:8080/ScanSee2.1/find/findscanseecatsearch";
		
		String logon = "http://10.11.202.76:8080/ScanSee2.3/find/findscanseecatsearch";
	 	
		final ClientRequest request = new ClientRequest(logon);
	
		String inputXml = "<ServiceSerachRequest>"
							+ "<userId>1</userId>" 
							+ "<lat>32.910347</lat>" 
							+ "<lng>-96.728472</lng>" 
							+ "<type>Arts & Culture</type>" 
							+ "<lastVisitedNo>0</lastVisitedNo>"
						+ "</ServiceSerachRequest>";
		
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);

		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response : " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void testfindGoogleCatSearch()
	{
	
//		String logon = "https://app.scansee.net/ScanSee2.4/find/findgooglecatsearch";
		final String logon = "http://199.36.142.83:8080/ScanSee2.4/find/findgooglecatsearch";
//		final String logon = "http://localhost:8080/ScanSee2.4/find/findgooglecatsearch";
	 	
		final ClientRequest request = new ClientRequest(logon);
		
		String inputXml = "<ServiceSerachRequest>"
							+ "<userId>7693</userId>" 
							+ "<lat>30.331562</lat>" 
							+ "<lng>-97.700394</lng>" 
							+ "<type>Hotels/Motels/Resorts</type>" 
//							+"<pagetoken>CmRZAAAANFNfx33VvCaGy0wrUARVfORQ70IVrn8oA01qAhvfFJYVX6Ly-Zuub_SPV-jaKHjPj_0SvVpGtVL-8_z3X3lc7gTYrKkURsKD96edpsRvXapdxVke2tvoNATDJL-LfAOfEhAsmB8S7lZ9IHZez0ir1DbqGhROFud2AaZIXhESythsHURkBpzDxw</pagetoken>"
						+ "</ServiceSerachRequest>";
		
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);

		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in save user media" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void findRetSearchForSSData()	
	{
		
		String logon = "http://localhost:8080/ScanSee2.2/find/findscanseeretsearch";
	 	
		final ClientRequest request = new ClientRequest(logon);
	
		String inputXml = "<ServiceSerachRequest>"
							+ "<userId>7693</userId>" 
							+ "<lat>45.894071</lat>" 
							+ "<lng>-111.780946</lng>" 
							+ "<keyword>LONG</keyword>" 
							+ "<lastVisitedNo>0</lastVisitedNo>"
						+ "</ServiceSerachRequest>";
		
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);

		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response : " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void testfindGoogleRetSearch()
	{
		
		String logon = "http://localhost:8080/ScanSee/find/findgoogleretsearch";
	 	
		final ClientRequest request = new ClientRequest(logon);
		
		String inputXml = "<ServiceSerachRequest>"
							+ "<userId>1</userId>" 
							+ "<lat>32.910347</lat>" 
							+ "<lng>-96.728472</lng>" 
							+ "<keyword>pizza</keyword>" 
							+"<pagetoken>ClRQAAAAKeIjjsLel-y-Ovtxgof5Bi8le5hgoI-LPASEAIRG9SQKksCyc5iC_UTmbentRuM_eEx7MjfPkJnE7mS3ILa-b04dpp05GHtz71XFQoqwF6YSEKnUJ2d5wHE8vIIGWVQOrNoaFFJ-W7YvTLUryVtEy2CmdK8jSHSM</pagetoken>"
						+ "</ServiceSerachRequest>";
		
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", inputXml);

		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in save user media" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
