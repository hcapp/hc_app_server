//package com.scansee.rest.client;

import javax.ws.rs.core.MediaType;
import org.jboss.resteasy.client.ClientRequest;


public class MyAccountRestEasyClient
{
	
	private MyAccountRestEasyClient()
	{

	}

	
	public static void main(String[] args)
	{
	//	myAccount();
		forgotPassword();
	}
	
	public static void myAccount()
	{
		System.out.println("=================Before fetchAccountDetails===================\n");
	//	fetchAccountDetails();
		System.out.println("\n=================After fetchAccountDetails===================\n");

		System.out.println("=================Before forgotPassword===================\n");
		forgotPassword();
		System.out.println("\n=================After forgotPassword===================\n");

		System.out.println("=================Before fetchUserPaymentInfo===================\n");
		//fetchUserPaymentInfo();
		System.out.println("\n=================After fetchUserPaymentInfo===================\n");

		System.out.println("=================Before saveUserPaymentPreference===================\n");
	//	saveUserPaymentPreference();
		System.out.println("\n=================After saveUserPaymentPreference===================\n");
	}

	static void fetchAccountDetails()
	{

		final String myAccountDetails = "http://localhost:8080/ScanSee/myaccount/myaccountdetails";
		final ClientRequest request = new ClientRequest(myAccountDetails);
		final String myAccountDetailsXml = "<?xml version='1.0' encoding='UTF-8'?>" + "<AuthenticateUser>" + "<userId>316</userId>"
				+ "<password>123456</password>" + "</AuthenticateUser>";

		request.accept("text/xml").body(MediaType.TEXT_XML, myAccountDetailsXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in fetchAccountDetails" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	static void forgotPassword()
	{
		final String forgotPassword = "http://localhost:8080/ScanSee/myaccount/forgotpassword?userId=3";
		final ClientRequest request = new ClientRequest(forgotPassword);
		try
		{
			final String responseObj = request.getTarget(String.class);
			System.out.println("response in forgotPassword" + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	static void fetchUserPaymentInfo()
	{
		final String fetchUserPaymentInfo = "http://localhost:9090/ScanSee/myaccount/fetchuserpaymentinfo?userId=2";
		final ClientRequest request = new ClientRequest(fetchUserPaymentInfo);
		try
		{
			final String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	static void saveUserPaymentPreference()
	{
		final String saveUserPaymentPreference = "http://localhost:9090/ScanSee/myaccount/saveuserpaymentpreference";
		final ClientRequest request = new ClientRequest(saveUserPaymentPreference);
		final String saveuserpaymentpreferenceinputXml = "<UserPreference>" + "<userID>3</userID>" + "<defaultPayout>1000.55</defaultPayout>"
				+ "<useDefaultAddress>1</useDefaultAddress>" + "<payAddress1>address 1111</payAddress1>" + "<payAddress2>address 2222</payAddress2>"
				+ "<payCity>Bangalore</payCity>" + "<payState>Karanata</payState>" + "<payPostalCode>56454554</payPostalCode>"
				+ "<provisionalPayoutActive>1</provisionalPayoutActive>"
				+ "<provisionalPayoutStartDate>2011-06-06 00:00:00</provisionalPayoutStartDate>"
				+ "<provisionalPayoutEndDate>2011-10-12 00:00:00</provisionalPayoutEndDate>" + "<selPaymentIntervalID>1</selPaymentIntervalID>"
				+ "<selPaymentTypeID>1</selPaymentTypeID>" + "<selPayoutTypeID>1</selPayoutTypeID>" + "</UserPreference>";
		
		final String saveuserpaymentpreferenceinputXml2 = "<UserPreference>" + "<userID>4</userID>" + "<defaultPayout>1000.55</defaultPayout>"
		+ "<provisionalPayoutActive>1</provisionalPayoutActive>"
		+ "<provisionalPayoutStartDate>2011-06-06 00:00:00</provisionalPayoutStartDate>"
		+ "<provisionalPayoutEndDate>2011-10-12 00:00:00</provisionalPayoutEndDate>" + "<selPaymentIntervalID>1</selPaymentIntervalID>"
		+ "<selPaymentTypeID>1</selPaymentTypeID>" + "<selPayoutTypeID>1</selPayoutTypeID>" + "</UserPreference>";

		request.accept("text/xml").body(MediaType.TEXT_XML, saveuserpaymentpreferenceinputXml);
		request.getHeaders();
		try
		{
			final String response = request.postTarget(String.class);
			System.out.println("response in saveUserPaymentPreference" + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
