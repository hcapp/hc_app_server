//package com.scansee.rest.client;

import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;

public class ShoppingListTestCases implements Runnable
{
	public static void main(String[] args)
	{
		ShoppingListTestCases shopList = new ShoppingListTestCases();
		shopList.executeShoppingListTestCases();
		
//		testAddLoyalty();
	}

	public void executeShoppingListTestCases()
	{		
		
	//	testDeleteProductFromSL();
	//testCLRDetails();
//		testOnlineStores();
		// testGetMainShoppingListItems("2");
		// testShareProductInfo();
	///	testDeleteProductFromSL();
//		 testAddProductToSL();
		// testremoveRebate();
		// testAddRemoveTodaySLProducts();
	//getmslproducts();
//		 gettslproducts();
		// testDeleteProductFromSL();
	//	testAddCoupon();
		// testRemoveCoupon();
	//	 testAddLoyalty();
		// testLoyaltyRemove();
		// testAddRebate();
		//testAddUnassignedProd();
	//	testAddUserNotes();
//		 testGetShareProductInfo();

//	gettProductsSummary();
		//testCLRDetails();
//		testAddtslbysearch();
	//	testAddProductToSL();
		//testAddtslbysearch();
		//testAddtslbysearch();
	//	testAddRemoveTodaySLProducts();
//		gettProductsSummary();
		//testaddslhistoryproduct();
	//testGetShareProductInfo();
	
		//testAddCoupon();
		//testOnlineStores();
		//testOnlineStores();
	//	testAddUnassignedProd();
	//	Url = http://122.181.128.152:8080/ScanSee/shoppingList/addremcart

	//	RequestStr = <AddShoppingCart><userId>9</userId><isaddToTSL>true</isaddToTSL><productDetails><ProductDetail><userProductId>415</userProductId><productId>129366</productId><productName>010-10723-12 AC Adapter</productName></ProductDetail></productDetails></AddShoppingCart>
	//	http://localhost:8080/ScanSee1.4.0/shoppingList/getproductinfo?userId=2&searchKey=fsdaf
		
//		findNearBy();
//		findNearByLowestPrice();
//		testGetMediaInfo();
	//userTrackingOnlineStoreClick();
		//findNearBy();
	//	getLoyaltyInfo();
		getProductsSummary();
//		testGetShoppingBasketProd();
		//testGetMasterShoppingListCategoryProduct();
//		testAddRemoveSBProducts();
	}
	
	void testGetMasterShoppingListCategoryProduct()
	{
		String URL = "http://199.36.142.83:8080/ScanSee2.1/shoppingList/getmslcategoryproducts?userId=10338&lowLimit=0";
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj + Calendar.getInstance().getTimeInMillis());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	void testGetMainShoppingListItems(String userId)
	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/getShoppingListItems?USERID=" + 3361;
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj + Calendar.getInstance().getTimeInMillis());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testAddProductToSL()
	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/addproducttosl";
		String addproducttoshoplistInputXml = "<AddShoppingList>" 
												+ "<userId>2</userId>" 
												+ "<productDetails>" 
													+ "<ProductDetail>" 
														+ "<productId>4570</productId>"
														+ "<productName>Sunsilk Shampoo</productName>" 
													+ "</ProductDetail>" 
												+ "</productDetails>" 
											+ "</AddShoppingList>";
		
		/*<AddShoppingList>
		<userId>162</userId>
		<productDetails>
			<ProductDetail>
				<productId>6</productId>
				<productName>Sunsilk Shampoo</productName>
			</ProductDetail>
		</productDetails>
	</AddShoppingList>*/

		
		
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, addproducttoshoplistInputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testAddtslbysearch()
	{
		
		/*<AddSLHistoryItems>
	      <userId>1</userId>
		<addTo>List</addTo>
		<productDetails>
			<ProductDetail>
			<userProductId>312</userProductId>
			<productId>247770</productId>
			</ProductDetail>
		</productDetails>
	</AddSLHistoryItems>*/

		String URL = "http://localhost:8080/ScanSee/shoppingList/addtslbysearch";
		
		String addproducttoshoplistInputXml = "<AddSLHistoryItems>" 
												+ "<userId>2</userId>" 
												+ "<addTo>List</addTo>"
												+ "<productDetails>" 
													+ "<ProductDetail>" 
														+ "<productId>4568</productId>"
													+ "</ProductDetail>" 
												+ "</productDetails>"
												+ "<mainMenuID>55</mainMenuID>"
											+ "</AddSLHistoryItems>";
		

		String addproducttoshoplistInputXml2 =null;
			/*"<AddShoppingList><userId>3</userId><productDetails><ProductDetail><productId>2590848</productId><productName><![CDATA[Rayovac Rechargeable Powerpack]]>" +
			"</productName><productDetails><ProductDetail>
					<productId>75631</productId>
						<productName><![CDATA[""L"" Workstation Chairmat]]></productName>
					</ProductDetail>
				</productDetails>
	</AddShoppingList>*/
			
		
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, addproducttoshoplistInputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	void testDeleteProductFromSL()
	{
		String URL = "http://localhost:8080/ScanSee1.4.0/shoppingList/deleteslproduct";
		String deleteslproductInputXml = "<AddShoppingCart>" + "<userId>1</userId>" + "<productDetails>" + "<ProductDetail>"
				+ "<userProductId>12</userProductId>" + "</ProductDetail>" + "<ProductDetail>" + "<userProductId>13</userProductId>"
				+ "</ProductDetail>" + "</productDetails>" + "</AddShoppingCart>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, deleteslproductInputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testAddUnassignedProd()
	{
	//	"?><ProductDetailsRequest><userId>265</userId><productName>business</productName><addedTo>L</addedTo><productDescription></productDescription></ProductDetailsRequest> 14 
		String URL = "http://localhost:8080/ScanSee1.4.0/shoppingList/addunassignedProd";
		String addunassignedproductInputXml = "<ProductDetailsRequest>" + "<userId>2</userId>" + "<productName>AAAA</productName>"+
			"<addedTo>F</addedTo>"+"<productDescription>pppppppw</productDescription>"	+ "</ProductDetailsRequest>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", addunassignedproductInputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testGetShoppingBasketProd()
	{
		String URL = "http://localhost:8080/ScanSee2.3/shoppingList/getsbproducts";
		String InputXml = "<UserTrackingData>"+"<userID>2</userID>" 
		+ "<moduleID>2</moduleID>"
		//+ "<longitude>77.578300</longitude><latitude>12.947900</latitude>"
		+"<lastVisited>0</lastVisited>"
		+ "<zipcode>75243</zipcode>" 
		+"</UserTrackingData>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", InputXml);
		request.getHeaders();
		try
		{
			String responseObj = request.postTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testAddRemoveSBProducts()
	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/addrembasket";
		
		String addrembasketInputXml = 
		"<AddRemoveSBProducts>" 
			+ "<userId>2</userId>" 
/*			+ "<basketProducts>" 
				+ "<ProductDetail>"
					+ "<userProductId>12</userProductId>" 
				+ "</ProductDetail>" 
				+ "<ProductDetail>" 
					+ "<userProductId>13</userProductId>"
				+ "</ProductDetail>" + 
			"</basketProducts>" */
			+ "<cartProducts>" 
				+ "<ProductDetail>" 
					+ "<userProductId>14</userProductId>"
				+ "</ProductDetail>" 
				+ "<ProductDetail>" 
					+ "<userProductId>14</userProductId>"
				+ "</ProductDetail>"
			+ "</cartProducts>" 
		+ "</AddRemoveSBProducts>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, addrembasketInputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testAddRemoveTodaySLProducts()
	{
		//RequestStr = <AddShoppingCart><userId>9</userId><isaddToTSL>true</isaddToTSL><productDetails><ProductDetail><userProductId>415</userProductId>
		//<productId>129366</productId><productName>010-10723-12 AC Adapter</productName></ProductDetail></productDetails></AddShoppingCart>
		String URL = "http://localhost:8080/ScanSee1.4.0/shoppingList/addremcart";
		String addremcartInputXml = "<AddShoppingCart>" + "<userId>2</userId>" +"<isaddToTSL>true</isaddToTSL> <productDetails> <ProductDetail>"
				+ "<userProductId>8</userProductId>" + "</ProductDetail>"  +"<ProductDetail>"
				+ "<userProductId>13</userProductId>" + "</ProductDetail>" + "</productDetails>" + "</AddShoppingCart>";

		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, addremcartInputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testGetTodaysNMasterSL(String userId)
	{

		String URL = "http://localhost:8080/ScanSee/shoppingList/getshoppListandCartProd?userId=" + userId;
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testAddUserNotes()
	{
		String URL = "http://10.11.202.76:8080/ScanSee1.4.0/shoppingList/addusernotes";
		String addremcartInputXml = "<UserNotesDetails>" + "<userId>2</userId>" + "<userNotes><![CDATA[           ]]></userNotes>"
				+ "</UserNotesDetails>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, addremcartInputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testDeleteUserNotes(String userId)
	{
		
		String URL = "http://localhost:8080/ScanSee/shoppingList/deleteusernotes?userId=" + userId;
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testRetrieveUserNotes(String userId)
	{

		String URL = "http://localhost:8080/ScanSee/shoppingList/getusernotes?userId=" + userId;
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testShareProductInfo()
	{
		String URL = "http://10.11.202.76:8080/ScanSee/shoppingList/shareProductInfo";
		String inputXml = "<ShareProductInfo>" + "<toEmail>manjunatha_gh@spanservices.com</toEmail>"
				+ "<toEmail>dileepa_cc@spanservices.com</toEmail>" + "<productId>1</productId>" + "<userId>2</userId>" + "</ShareProductInfo>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testGetMediaInfo()
	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/mediainfo?userId=2&productID=2&mediaType=video&prodListID=1";
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testAddCoupon()
	{
		String URL = "http://localhost:8080/ScanSee1.4.0/shoppingList/addCoupon";
		String inputXml = "<AddRemoveCLR>" + "<couponId>2</couponId>" + "<userId>2</userId>" + "</AddRemoveCLR>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testRemoveCoupon()
	{
		String URL = "http://localhost:8080/ScanSee1.4.0/shoppingList/removeCoupon";
		String inputXml = "<AddRemoveCLR>" + "<couponId>2</couponId>" + "<userId>2</userId>" + "</AddRemoveCLR>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	static void testAddLoyalty()
	{
		String URL = "http://localhost:8080/ScanSee1.4.0/shoppingList/addLoyalty";
		//"?><AddRemoveCLR><userId>3222</userId><loyaltyDealId>472</loyaltyDealId><merchantId>714</merchantId></AddRemoveCLR>
		String inputXml = "<AddRemoveCLR>" + "<loyaltyDealId>481</loyaltyDealId>" + "<userId>3222</userId> <merchantId>632</merchantId>"
				+ "</AddRemoveCLR>";
		//"?><AddRemoveCLR><userId>3298</userId><loyaltyDealId>21</loyaltyDealId><merchantId>714</merchantId></AddRemoveCLR>
		//"?><AddRemoveCLR><userId>3065</userId><loyaltyDealId>21</loyaltyDealId><merchantId>714</merchantId></AddRemoveCLR>

		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testAddRebate()
	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/addRebate";
		String inputXml = "<AddRemoveCLR>" + "<userId>2</userId>" + "<rebateId>2</rebateId>" + "<productId>1</productId>" + "</AddRemoveCLR>";

		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testremoveRebate()
	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/removeRebate";
		String inputXml = "<AddRemoveCLR>" + "<userId>2</userId>" + "<rebateId>2</rebateId>" + "</AddRemoveCLR>";

		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testLoyaltyRemove()
	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/removeLoyalty";
		String inputXml = "<AddRemoveCLR>" + "<loyaltyDealId>2</loyaltyDealId>" + "<userId>2</userId>" + "</AddRemoveCLR>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body(MediaType.TEXT_XML, inputXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void getTodaySLProducts(String userId)
	{

		String URL = "http://localhost:8080/ScanSee/shoppingList/getscproducts?userId=" + userId;
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String responseObj = request.getTarget(String.class);
			System.out.println("response " + responseObj);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void getmslproducts()
	{
		String URL = "http://localhost:8080/ScanSee1.4.0/shoppingList/getmslproducts";
		String getMslprodsXml = "<ProductDetailsRequest>" + "<userId>2</userId>" + "<userRetailPreferenceID>14</userRetailPreferenceID>"
				+ "<retailID>0</retailID>" + "<parentCategoryID>0</parentCategoryID>" + "<lastVisitedProductNo>0</lastVisitedProductNo>"
				+ "</ProductDetailsRequest>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", getMslprodsXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void gettslproducts()
	{
		String URL = "http://localhost:8080/ScanSeeQA/shoppingList/gettslproducts";
		
		
		String getMslprodsXml = "<ProductDetailsRequest>" + "<userId>2</userId>" + "<userRetailPreferenceID>14</userRetailPreferenceID>"
				+ "<retailID>0</retailID>" + "<parentCategoryID>0</parentCategoryID>" + "<lastVisitedProductNo>0</lastVisitedProductNo>"
				+ "</ProductDetailsRequest>";

		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", getMslprodsXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	void getProductsSummary()
	{
		//<ProductDetailsRequest><userId>4522</userId><productId>4758503</productId><postalcode>75244</postalcode></ProductDetailsRequest>
	String URL2 = "http://199.36.142.83:8080/ScanSee2.3/shoppingList/getproductsummary";
//	String URL = "http://localhost:8080/ScanSee1.5/shoppingList/getproductsummary";
		
		
		Date d=new Date();
		//System.out.println("before call :"+d.getTime());
		//ProductDetailsRequest><userId>2114</userId><productId>8884</productId><postalcode>12121</postalcode></ProductDetailsRequest>
		String getMslprodsXml = "<ProductDetailsRequest>"
									+ "<userId>1813</userId>"
									+ "<productId>1</productId>"
									+ "<postalcode>00733</postalcode>"
									+ "<retailID>1067084</retailID>"
									+ "<saleListID>1986</saleListID>"
									+ "<prodListID>1</prodListID>"
									+ "<mainMenuID>2</mainMenuID>"
									+ "<scanTypeID>0</scanTypeID>"
								+ "</ProductDetailsRequest>";
	
		 String getMslprodsXml2 = "<ProductDetailsRequest><userId>10319</userId><productId>5411325</productId><longitude>-122.406400</longitude><latitude>37.785800</latitude><mainMenuID>5186</mainMenuID><scanTypeID>0</scanTypeID><prodListID>89059</prodListID></ProductDetailsRequest>";
		 
		ClientRequest request = new ClientRequest(URL2);

		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", getMslprodsXml2);
		
		request.getHeaders();
		try
		{
		
			long strtTime = new Date().getTime();
			String response = request.postTarget(String.class);
			long endtime = new Date().getTime();
			System.out.println((endtime - strtTime));
			System.out.println("response " + response);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testGetShareProductInfo()
	{
		String URL = "http://localhost:8080/ScanSee1.3.0/shoppingList/getproductsummary";
		String inputXml = "<toEmail>dileepa_cc@spanservices.com</toEmail>" + "<productId>1</productId>" + "<userId>2</userId>";
		String getMslprodsXml = "<ProductDetailsRequest><userId>2</userId><productId>59843</productId><postalcode>12121</postalcode>" +
		"</ProductDetailsRequest>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml").body("text/xml;charset=UTF-8", getMslprodsXml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testOnlineStores()
	{
		String URL = "http://10.11.202.76:8080/ScanSee1.4.0/shoppingList/getproductsummary";
	//	https://app.scansee.net6360840,6381423
		String onlinestorexml ="<ProductDetailsRequest><userId>2</userId><productId>4569</productId>" +
		"</ProductDetailsRequest>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", onlinestorexml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	void testCLRDetails()
	{
		String URL = "http://localhost:8080/ScanSee1.5.3/shoppingList/fetchproductclrinfo";
		String onlinestorexml = "<CLRDetails>" 
								+ "<userId>2</userId>" 
								+ "<productId>17</productId>"
								+ "<mainMenuID>1</mainMenuID>"
							   + "</CLRDetails>";
		/*<CLRDetails>
		<userId>2</userId>
		<productId>17</productId>
		<retailerId>0</retailerId>
		<lowerLimit>0</lowerLimit>
		<clrC>1</clrC>
		<clrR>0</clrR>
		<clrL>0</clrL>
	</CLRDetails>
*/
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", onlinestorexml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	void testaddslhistoryproduct()
	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/addslhistoryproduct";
		
		String addslhistoryproductxml = "<AddSLHistoryItems><userId>3</userId><addTo>List</addTo>" +
				"<productDetails><ProductDetail><userProductId>87</userProductId>" +
				"<productId>3564</productId></ProductDetail>" +
				"</productDetails></AddSLHistoryItems>";

		

		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", addslhistoryproductxml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	void findNearBy()	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/findNearBy?userID=2&productId=2&latitude=18.2297&longitude=-66.0458&postalCode=78729&radius=5&mainMenuID=1";
		
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String response = request.getTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	void findNearByLowestPrice()	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/findNearByLowestPrice?userID=2&productId=2&latitude=18.2297&longitude=-66.0458&postalCode=78729&radius=5&mainMenuID=1";
		
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String response = request.getTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	void userTrackingProdMediaClick()	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/utpmclick?pmListID=1";
		
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String response = request.getTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	void userTrackingOnlineStoreClick()	{
		//String URL = "http://localhost:8080/ScanSee/shoppingList/utonstoclick?onProdDetID=1";
		String URL = "http://localhost:8080/ScanSee/shoppingList/utonstoclick";
		String addslhistoryproductxml = "<AddSLHistoryItems><productID>3447548</productID><retailerName>Jewelry Adviser</retailerName>" +
		"<mainMenuID>100</mainMenuID></AddSLHistoryItems>";
		ClientRequest request = new ClientRequest(URL);
		request.accept("text/xml;charset=UTF-8").body("text/xml;charset=UTF-8", addslhistoryproductxml);
		request.getHeaders();
		try
		{
			String response = request.postTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
																
	void getLoyaltyInfo()	{
		String URL = "http://localhost:8080/ScanSee/shoppingList/getLoyaltyInfo?userId=2&productId=581147&retailId=1212&mainMenuID=1";
		
		ClientRequest request = new ClientRequest(URL);
		try
		{
			String response = request.getTarget(String.class);
			System.out.println("response " + response);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	@Override
	public void run()
	{
		// TODO Auto-generated method stub

	}
}
