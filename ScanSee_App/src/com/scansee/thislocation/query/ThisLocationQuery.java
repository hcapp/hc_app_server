package com.scansee.thislocation.query;

/**
 * The class has queries to be used in the ThisLocation module.
 * 
 *@author shyamsundara_hm
 */

public class ThisLocationQuery
{

	/**
	 * Query for fetchCategoryDetails.
	 */

	public static final String FETCHCATEGORYDETAILSQUERY = " SELECT RetailID, RC.CategoryID categoryID, C.ParentCategoryID parentCategoryID"
			+ ", C.ParentCategoryName parentCategoryName,"
			+ " C.ParentCategoryDescription  parentCategoryDescription, C.SubCategoryID subCategoryID, C.SubCategoryName subCategoryName, "
			+ " C.SubCategoryDescription  subCategoryDescription "
			+ " FROM RetailCategory RC INNER JOIN Category C ON C.CategoryID = RC.CategoryID WHERE RetailID = ? order by C.ParentCategoryName,C.SubCategoryName";

	/**
	 * Query for fetchProductDetails.
	 */

	public static final String FETCHPRODUCTDETAILSQUERY = "SELECT P.ProductName productName,P.ProductID productId, M.ManufName , P.ModelNumber ,"
			+ " P.ProductDescription productDescription , P.ProductExpirationDate, P.ProductImagePath, P.SuggestedRetailPrice"
			+ ", P.Weight, P.WeightUnits FROM Category C INNER JOIN ProductCategory PC ON PC.CategoryID = C.CategoryID"
			+ " INNER JOIN Product P ON P.ProductID = PC.ProductID INNER JOIN Manufacturer M ON M.ManufacturerID = P.ManufacturerID"
			+ " WHERE C.ParentCategoryID =? ";

	/**
	 * Query for fetching FavCategoryDetails.
	 */

	public static final String FETCHFAVCATEGORYDETAILSQUERY = "SELECT UC.UserID   , UC.CategoryID   , C.ParentCategoryID    , C.ParentCategoryName,C.SubCategoryID , C.SubCategoryName  FROM UserCategory UC  INNER JOIN Category C ON C.CategoryID = UC.CategoryID   INNER JOIN RetailCategory RC ON RC.CategoryID = C.CategoryID WHERE UC.UserID = ?   AND RC.RetailID = ?";

	/**
	 * Query for fetching OtherCategoriesDetails.
	 */

	public static final String FETCHOTHERCATEGORIESDETAILSQUERY = "SELECT RC.RetailID , RC.CategoryID  , C.ParentCategoryID  , C.ParentCategoryName, C.SubCategoryID , C.SubCategoryName FROM RetailCategory RC   INNER JOIN Category C ON RC.CategoryID = C.CategoryID WHERE RetailID = ?    AND C.CategoryID NOT IN (SELECT CategoryID FROM UserCategory WHERE UserID = ?)";

	/**
	 * Query for saving User AdvertisemetHit in database.
	 */

	public static final  String SAVEADVERTISEMETHITQUERY = "insert into UserAdvertisementHit (UserID, RetailLocationAdvertisementID,DateViewed) values (?, ?,?)";

	/**
	 * Query for fetching user latitude and longitude from Database.
	 */

	public static final  String FETCHUSERLOCATIONPOINTSQUERY = "SELECT Latitude latitude, Longitude longitude,PostalCode postalCode FROM GeoPosition WHERE PostalCode = (SELECT PostalCode FROM Users WHERE UserID =?)";

	/**
	 * For fetching user postal code from Database.
	 */
	public static final  String FETCHUSERPOSTALCODEQUERY="SELECT PostalCode 	FROM GeoPosition WHERE Latitude = ? AND Longitude = ?";
	
	/**
	 * For fetching Lat and Long for the given zipcode.
	 */
	public static final  String FETCHLATLONG="SELECT Latitude, Longitude FROM GeoPosition WHERE PostalCode = ?";

	
	/**
	 * This is for ThisLocationQuery class constructor.
	 */
	private ThisLocationQuery()
	{

	}

}
