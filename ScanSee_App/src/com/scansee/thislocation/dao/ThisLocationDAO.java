package com.scansee.thislocation.dao;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.ImplementedBy;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AdvertisementHitReq;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.CategoryDetail;
import com.scansee.common.pojos.CategoryInfo;
import com.scansee.common.pojos.CategoryRequest;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.ProductDetailsRequest;
import com.scansee.common.pojos.RetailerCreatedPages;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.ThisLocationRequest;
import com.scansee.common.pojos.UserTrackingData;

/**
 * The ThisLocationDAO Interface. {@link ImplementedBy} ThisLocationDAOImpl
 * 
 * @author shyamsundara_hm
 */
public interface ThisLocationDAO
{

	/**
	 * The method fetches the Retailer Details from the database for the given
	 * search parameters(Zipcode or Latitude and longitude).
	 * 
	 * @param thisLocationRequest
	 *            Instance of ThisLocationRequest.
	 * @param screenName
	 *            screenName to be used for Pagination size.
	 * @return RetailersDetails returns RetailerDetails object container List of
	 *         retailers.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer.
	 */

	RetailersDetails fetchRetailerDetails(ThisLocationRequest thisLocationRequest, String screenName) throws ScanSeeException;

	/**
	 * The DAO method fetches the Category Details from the database for the
	 * given Retailer Id.
	 * 
	 * @param categoryDetail
	 *            Instance of CategoryRequest.
	 * @return CategoryDetail returns list of Categories.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	CategoryDetail fetchCategoryDetails(CategoryRequest categoryDetail) throws ScanSeeException;

	/**
	 * The method fetches the Product Details.
	 * 
	 * @param productDetailsRequest
	 *            Instance of ProductDetailsRequest.
	 * @param screenName
	 *            screenName parameter constant used for Pagination.
	 * @return ProductDetails returns list of Products.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetails fetchProductDetails(ProductDetailsRequest productDetailsRequest, String screenName) throws ScanSeeException;

	/**
	 * Method declartion of fetchFavCategoryDetail.
	 * 
	 * @param userID
	 *            The userID in the request.
	 * @param retailID
	 *            The retailID in the request.
	 * @return favCategoryDetails info.
	 * @throws ScanSeeException
	 *             The exeption defined for the application.
	 */
	CategoryDetail fetchFavCategoryDetails(Integer userID, Integer retailID) throws ScanSeeException;

	/**
	 * Method declartion of fetchOtherCategoryDetail.
	 * 
	 * @param userID
	 *            The userID in the request.
	 * @param retailID
	 *            The retailID in the request.
	 * @return fetchOtherCategoryDetail info.
	 * @throws ScanSeeException
	 *             The exeption defined for the application.
	 */
	CategoryDetail fetchOtherCategoriesDetails(Integer userID, Integer retailID) throws ScanSeeException;

	/**
	 * Method saving user AdvertisementHit in DB.
	 * 
	 * @param userID
	 *            advertisementHitReq The xml with userId and advertisementID.
	 * @return fetchOtherCategoryDetail info.
	 * @throws ScanSeeException
	 * @throws ScanSeeException
	 *             The exeption defined for the application.
	 */

	/**
	 * Method saving user AdvertisementHit in DB.
	 * 
	 * @param advertisementHitReq
	 *            The XML with userId and advertisementID.
	 * @return String Success if DB updation Successful else failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String saveAdvertisementHit(AdvertisementHitReq advertisementHitReq) throws ScanSeeException;

	/**
	 * This method fetches Location details from the database for the given
	 * user.
	 * 
	 * @param userID
	 *            The userID in the request.
	 * @return ThisLocationRequest info contains Latitude and Longitude.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ThisLocationRequest fetchUserLocationDetails(Integer userID) throws ScanSeeException;

	/**
	 * This method is used to fetch the Postal code for the given latitude and
	 * longitude from the Database.
	 * 
	 * @param latitude
	 *            The latitude in the request.
	 * @param longitude
	 *            The longitude in the request.
	 * @return ThisLocationRequest Contains Zipcode information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ThisLocationRequest fetchUserPostalcode(Double latitude, Double longitude) throws ScanSeeException;

	/**
	 * This method is used to fetch the Latitude and Longitude for the given
	 * Zipcode.
	 * 
	 * @param zipcode
	 *            Zipcode for getting Lat and Long.
	 * @return ThisLocationRequest Container lat and long.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ThisLocationRequest fetchLatLong(Long zipcode) throws ScanSeeException;

	/**
	 * This method is used to fetch near by products for given product details.
	 * 
	 * @param searchDetails
	 *            -As request parameter
	 * @return ProductDetails Contains categoryID,sale price etc.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	ProductDetails findNearByProducts(ProductDetail searchDetails) throws ScanSeeException;

	/**
	 * Method declaration of get radius details.
	 * 
	 * @param userID
	 *            The userID in the request.
	 * @param retailID
	 *            The retailID in the request.
	 * @return favCategoryDetails info.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	ArrayList<ThisLocationRequest> getRediusDetails() throws ScanSeeException;

	/**
	 * This method for update user zip code information .
	 * 
	 * @param userId
	 * @param zipcode
	 * @return String with success or failure resposne.
	 * @throws ScanSeeException
	 *             The exeption defined for the application.
	 */
	String updateZipcode(Long userId, String zipcode) throws ScanSeeException;

	/**
	 * This method for check user zip code information .
	 * 
	 * @param userId
	 * @return String with success or failure resposne.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	String checkUsrZipcode(Long userId) throws ScanSeeException;

	/**
	 * This Rest Easy method for recording User retailer sale notification.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	String saveUsrRetNotification(RetailerDetail retailerDetailObj) throws ScanSeeException;

	/**
	 * This Rest Easy method for fetching retailer store details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	List<RetailerDetail> fetchRetailerStoreDetails(RetailerDetail retailerDetailObj) throws ScanSeeException;

	/**
	 * This Rest Easy method for fetching special offer details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */
	List<RetailerDetail> fetchSpecialOffers(RetailerDetail specialOfferRequest) throws ScanSeeException;

	/**
	 * This Rest Easy method for fetching retailer summary details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */
	List<RetailerDetail> getRetailerSummary(RetailerDetail retailerDetailObj) throws ScanSeeException;

	/**
	 * This Rest Easy method for fetching retailer created pages details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	List<RetailerCreatedPages> getRetCreatePages(RetailerDetail retailerDetailObj) throws ScanSeeException;
	/**
	 * This Rest Easy method for fetching special offer,hot deals and coupon,loyalty and rebate discounts based on retailer and location id.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	ProductDetails fetchSpecialOffersHotDealDiscounts(ProductDetailsRequest productDetailsRequest) throws ScanSeeException;
	
	CLRDetails getRetailerCLRDetails(RetailerDetail retailerDetail) throws ScanSeeException;
	/**
	 * This DAO method for fetching retailer location hot deals.
	 * 
	 * @param xml
	 *            as the request object containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response object or exception.
	 */
	List<HotDealsDetails> getRetailerHotDeals(RetailerDetail retailerDetailObj) throws ScanSeeException;
	
	List<RetailerDetail> fetchSpecialDealsDetails(RetailerDetail retailerDetail)throws ScanSeeException;
	
	/**
	 * The method fetches the Retailer Details from the database for the given City
	 * 
	 * @param thisLocationRequest
	 *            Instance of ThisLocationRequest.
	 * @param screenName
	 *            screenName to be used for Pagination size.
	 * @return RetailersDetails returns RetailerDetails object container List of
	 *         retailers.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer.
	 */
	RetailersDetails getRetailersByGroupID(ThisLocationRequest thisLocationRequest, String screenName) throws ScanSeeException;
	
	/**
	 * The method fetches the Retailer Details from the database for the given GoLocal.
	 * 
	 * @param thisLocationRequest
	 *            Instance of ThisLocationRequest.
	 * @param screenName
	 *            screenName to be used for Pagination size.
	 * @return RetailersDetails returns RetailerDetails object container List of
	 *         retailers.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer.
	 */
	RetailersDetails getRetailersForPartner(ThisLocationRequest thisLocationRequest, String screenName) throws ScanSeeException;
	
	/**
	 * The method fetches the partners from the database for the given location.
	 * 
	 * @param thisLocationRequest
	 *            Instance of ThisLocationRequest.
	 * @return returns ThisLocationRequest object container List of partners.
	 * 
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer.
	 */
	ThisLocationRequest getPartners(Integer retGroupID) throws ScanSeeException;
	
	/**
	 * The method fetches the categories for group retailers from the database for the given location.
	 * 
	 * @param retGroupID.
	 * 
	 * @return returns CategoryInfo object container List of categories.
	 * 
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer.
	 */
	List<CategoryInfo> getCategoriesForGroupRetailers(Integer retGroupID) throws ScanSeeException;
	
	/**
	 * The method fetches the categories for partner retailers from the database for the given location.
	 * 
	 * @param retAffID.
	 * 
	 * @return returns CategoryInfo object container List of categories.
	 * 
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer.
	 */
	List<CategoryInfo> getCategoriesForPartners (Integer retAffID) throws ScanSeeException;
	
	
	/**
	 * For user tracking.
	 * To be called whenever user taps on any of the item in the Retailer summary screen. 
	 * @param userTrackingData
	 * @return xml response containing SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 */
	String userTrackingRetailerSummaryClick(UserTrackingData userTrackingData) throws ScanSeeException;
	
	/**
	 * For user tracking.
	 * To be called when the user taps on any special offers.
	 * 
	 * @param UserTrackingData object
	 * @return SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 */
	String userTrackingRetSpeOffersClick(UserTrackingData userTrackingData) throws ScanSeeException;
	
}
