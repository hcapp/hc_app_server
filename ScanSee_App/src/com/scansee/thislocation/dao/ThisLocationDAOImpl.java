package com.scansee.thislocation.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AdvertisementHitReq;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.CategoryDetail;
import com.scansee.common.pojos.CategoryInfo;
import com.scansee.common.pojos.CategoryRequest;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.LoyaltyDetail;
import com.scansee.common.pojos.MainCategoryDetail;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.ProductDetailsRequest;
import com.scansee.common.pojos.RebateDetail;
import com.scansee.common.pojos.RetailerCreatedPages;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.SubCategoryDetail;
import com.scansee.common.pojos.ThisLocationRequest;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.common.util.Utility;
import com.scansee.thislocation.query.ThisLocationQuery;

/**
 * This is implementation class for ThisLocatoinDAO. This class has methods for
 * ThisLocation Module. The methods of this class are called from the
 * ThisLocatoinDAO Service layer.
 * 
 * @author shyamsundara_hm
 */

public class ThisLocationDAOImpl implements ThisLocationDAO
{
	/**
	 * Getting the logger instance.
	 */

	private static final Logger log = LoggerFactory.getLogger(ThisLocationDAOImpl.class.getName());

	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;

	/**
	 * To set ParameterizedBeanPropertyRowMapper to map POJOs.
	 */
	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */

	/**
	 * To call stored procedue.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To get the datasource.
	 * 
	 * @param dataSource
	 *            The data source for Spring JDBC connection.
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.setResultsMapCaseInsensitive(true);
	}

	/**
	 * The method fetches the Retailer Details from the database for the given
	 * search parameters(Zipcode or Latitude and longitude).
	 * 
	 * @param thisLocationRequest
	 *            Instance of ThisLocationRequest.
	 * @param screenName
	 *            screenName to be used for Pagination size.
	 * @return RetailersDetails returns RetailerDetails object container List of
	 *         retailers.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer.
	 */
	@SuppressWarnings("unchecked")
	public RetailersDetails fetchRetailerDetails(ThisLocationRequest thisLocationRequest, String screenName) throws ScanSeeException
	{
		final String methodName = "fetchRetailerDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailerDetail> retailerList = null;
		RetailersDetails details = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FetchRetailerListPagination");
			simpleJdbcCall.returningResultSet("retailers", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));

			final MapSqlParameterSource fetchRetailerDetailsParameters = new MapSqlParameterSource();

			fetchRetailerDetailsParameters.addValue(ApplicationConstants.USERID, thisLocationRequest.getUserId());
			fetchRetailerDetailsParameters.addValue("Radius", thisLocationRequest.getPreferredRadius());
			fetchRetailerDetailsParameters.addValue("Longitude", thisLocationRequest.getLongitude());
			fetchRetailerDetailsParameters.addValue("Latitude", thisLocationRequest.getLatitude());
			fetchRetailerDetailsParameters.addValue("ZipCode", thisLocationRequest.getZipcode());
			fetchRetailerDetailsParameters.addValue("LowerLimit", thisLocationRequest.getLastVisitedRecord());
			fetchRetailerDetailsParameters.addValue("ScreenName", screenName);
			//For user tracking
			fetchRetailerDetailsParameters.addValue(ApplicationConstants.LOCATEONMAP, thisLocationRequest.getLocOnMap());
			fetchRetailerDetailsParameters.addValue(ApplicationConstants.MAINMENUID, thisLocationRequest.getMainMenuID());
			
			// below param are output params from SP.
			fetchRetailerDetailsParameters.addValue("ErrorNumber", null);
			fetchRetailerDetailsParameters.addValue("ErrorMessage", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRetailerDetailsParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					
					retailerList = (List<RetailerDetail>) resultFromProcedure.get("retailers");
					details = new RetailersDetails();
					if (null != retailerList && !retailerList.isEmpty())
					{
					
						details.setRetailerDetail(retailerList);
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						if (nextpage != null)
						{
							if (nextpage)
							{
								details.setNextPage(1);
							}
							else
							{
								details.setNextPage(0);
							}
						}
						
						
					}
					else
					{
						log.info("No Retailers found ");
//						return details;
					}
					
					final Boolean cityExperience = (Boolean) resultFromProcedure.get("CityExperience");
					final Integer retailGroupID = (Integer) resultFromProcedure.get("RetailGroupID");
					final Integer retailAffiliateID = (Integer) resultFromProcedure.get("RetailAffiliateID");
					final Integer maxCnt = (Integer) resultFromProcedure.get("MaxCnt");
					final Integer retAffCount = (Integer) resultFromProcedure.get("RetailAffiliateCount");
					final String retAffName = (String) resultFromProcedure.get("RetailAffiliateName");
					final String retGroupImg = (String) resultFromProcedure.get("RetailGroupButtonImagePath");
					
					if (cityExperience != null)
					{
						if (cityExperience)
						{
							details.setCityExp(true);
						}
						else
						{
							details.setCityExp(false);
						}
						
						if(null != retailGroupID)	{
							details.setRetGroupID(retailGroupID);
						} else	{
							details.setRetGroupID(0);
						}
						
						if(null != retailAffiliateID)	{
							details.setRetAffID(retailAffiliateID);
						} else	{
							details.setRetAffID(0);
						}
						
						details.setMaxCnt(maxCnt);
						details.setRetAffName(retAffName);
						details.setRetAffCount(retAffCount);
						details.setRetGroupImg(retGroupImg);
					}
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in usp_FetchRetailerList Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return details;
	}

	/**
	 * The method fetches the Product Details.
	 * 
	 * @param productDetailsRequest
	 *            Instance of ProductDetailsRequest.
	 * @param screenName
	 *            screenName parameter constant used for Pagination.
	 * @return ProductDetails returns list of Products.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public ProductDetails fetchProductDetails(ProductDetailsRequest productDetailsRequest, String screenName) throws ScanSeeException
	{

		final String methodName = "fetchProductDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetails productDetails = null;
		List<ProductDetail> productDetailLst = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FetchProductListPagination");
			simpleJdbcCall.returningResultSet("productDetails", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue("UserID", productDetailsRequest.getUserId());
			fetchProductDetailsParameters.addValue("RetailLocationID", productDetailsRequest.getRetailLocationID());
			// need to be implemented
			fetchProductDetailsParameters.addValue("ScreenName", screenName);
			if(productDetailsRequest.getLastVisitedProductNo()==null)
			{
				productDetailsRequest.setLastVisitedProductNo(0);
			}
			fetchProductDetailsParameters.addValue("LowerLimit", productDetailsRequest.getLastVisitedProductNo());
			//For user tracking
			fetchProductDetailsParameters.addValue(ApplicationConstants.RETAILERLISTID, productDetailsRequest.getRetListID());
			fetchProductDetailsParameters.addValue(ApplicationConstants.MAINMENUID, productDetailsRequest.getMainMenuID());
			// below param are output params from SP.
			fetchProductDetailsParameters.addValue("ErrorNumber", null);
			fetchProductDetailsParameters.addValue("ErrorMessage", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetailLst = (List<ProductDetail>) resultFromProcedure.get("productDetails");
					if (null != productDetailLst && !productDetailLst.isEmpty())
					{
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetailLst);
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						if (nextpage != null)
						{
							if (nextpage)
							{
								productDetails.setNextPage(1);

							}
							else
							{
								productDetails.setNextPage(0);

							}
						}
					}
					else
					{
						log.info("No Products found for the search");
						return productDetails;
					}
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in usp_FetchProductList Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}

		}
		catch (DataAccessException exception)
		{

			log.error("Exception occurred in scanBarCodeProduct", exception);
			throw new ScanSeeException(exception);

		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * The DAO method fetches the Category Details from the database for the
	 * given Retailer Id.
	 * 
	 * @param categoryRequestDetail
	 *            Instance of CategoryRequest.
	 * @return CategoryDetail returns list of Categories.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public CategoryDetail fetchCategoryDetails(CategoryRequest categoryRequestDetail) throws ScanSeeException
	{
		final String methodName = "fetchCategoryDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<CategoryDetail> categoryDetailLst = null;
		int preMainCategorId = 0;
		List<SubCategoryDetail> subList = null;
		final List<MainCategoryDetail> mainList = new ArrayList<MainCategoryDetail>();
		MainCategoryDetail mainCategoryDetail = null;
		CategoryDetail categoryDetailResp = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ThislocationCategoryList");
			simpleJdbcCall.returningResultSet("categoryDetails", new BeanPropertyRowMapper<CategoryDetail>(CategoryDetail.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();

			fetchProductDetailsParameters.addValue("RetailID", categoryRequestDetail.getRetailerId());
			fetchProductDetailsParameters.addValue("Retaillocationid", categoryRequestDetail.getRetailerLocationId());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					categoryDetailLst = (List<CategoryDetail>) resultFromProcedure.get("categoryDetails");
					// Need to test

					if (!categoryDetailLst.isEmpty())
					{
						CategoryDetail categoryDetail = null;
						int currentCatId = 0;
						for (Iterator<CategoryDetail> iterator = categoryDetailLst.iterator(); iterator.hasNext();)
						{

							categoryDetail = iterator.next();
							currentCatId = categoryDetail.getParentCategoryID();
							if (preMainCategorId == 0)
							{
								preMainCategorId = categoryDetail.getParentCategoryID();
								mainCategoryDetail = new MainCategoryDetail();
								mainCategoryDetail.setMainCategoryId(preMainCategorId);
								mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());
								subList = new ArrayList<SubCategoryDetail>();
							}
							if (currentCatId != preMainCategorId)
							{

								mainCategoryDetail.setSubCategoryDetailLst(subList);
								mainList.add(mainCategoryDetail);
								mainCategoryDetail = new MainCategoryDetail();
								preMainCategorId = categoryDetail.getParentCategoryID();
								mainCategoryDetail.setMainCategoryId(preMainCategorId);
								mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());
								subList = new ArrayList<SubCategoryDetail>();
								final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
								subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
								subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
								subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
								subList.add(subCategoryDetail);

							}
							else
							{
								final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
								subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
								subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
								subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
								subList.add(subCategoryDetail);
							}
						}

						mainCategoryDetail.setSubCategoryDetailLst(subList);
						mainList.add(mainCategoryDetail);
					}
					if (null != mainList && !mainList.isEmpty())
					{
						categoryDetailResp = new CategoryDetail();
						categoryDetailResp.setMainCategoryDetaillst(mainList);
					}
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in usp_ThislocationCategoryList Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}

		}
		catch (DataAccessException e)
		{

			log.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return categoryDetailResp;
	}

	/**
	 * Method fetches User's Favourite Categories Info from DB.
	 * 
	 * @param userId
	 *            the userId in the request.
	 * @param retailId
	 *            the retailID int he request.
	 * @return favCategoryDetails Favourite Categories Info.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public CategoryDetail fetchFavCategoryDetails(Integer userId, Integer retailId) throws ScanSeeException
	{
		final String methodName = "fetchCategoryDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<CategoryDetail> categoryDetailLst = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);

			categoryDetailLst = simpleJdbcTemplate.query(ThisLocationQuery.FETCHFAVCATEGORYDETAILSQUERY, new BeanPropertyRowMapper<CategoryDetail>(
					CategoryDetail.class), userId, retailId);

		}
		catch (DataAccessException e)
		{

			log.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());

		}

		int preMainCategorId = 0;
		List<SubCategoryDetail> subList = null;
		final List<MainCategoryDetail> mainList = new ArrayList<MainCategoryDetail>();
		MainCategoryDetail mainCategoryDetail = null;
		CategoryDetail categoryDetailResp = null;
		CategoryDetail categoryDetail = null;
		int currentCatId = 0;
		for (final Iterator<CategoryDetail> iterator = categoryDetailLst.iterator(); iterator.hasNext();)
		{
			categoryDetail = iterator.next();
			currentCatId = categoryDetail.getParentCategoryID();
			if (preMainCategorId == 0)
			{
				preMainCategorId = categoryDetail.getParentCategoryID();
				mainCategoryDetail = new MainCategoryDetail();
				mainCategoryDetail.setMainCategoryId(preMainCategorId);
				mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());
				subList = new ArrayList<SubCategoryDetail>();
			}
			if (currentCatId != preMainCategorId)
			{
				mainCategoryDetail.setSubCategoryDetailLst(subList);
				mainList.add(mainCategoryDetail);
				subList = new ArrayList<SubCategoryDetail>();
				final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
				subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
				subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
				subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
				subList.add(subCategoryDetail);
				mainCategoryDetail = new MainCategoryDetail();
				mainCategoryDetail.setMainCategoryId(currentCatId);
				mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());
				preMainCategorId = categoryDetail.getParentCategoryID();
			}
			else
			{
				final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
				subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
				subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
				subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
				subList.add(subCategoryDetail);
			}
		}

		if (null != mainList && !mainList.isEmpty())
		{
			categoryDetailResp = new CategoryDetail();
			categoryDetailResp.setMainCategoryDetaillst(mainList);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return categoryDetailResp;
	}

	/**
	 * Method fetches User's OtherCategories Info from DB.
	 * 
	 * @return favCategoryDetails List of Favourite Categories Info.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 * @param userId
	 *            The userId in the request.
	 * @param retailId
	 *            The retailId in the request.
	 */

	@Override
	public CategoryDetail fetchOtherCategoriesDetails(Integer userId, Integer retailId) throws ScanSeeException
	{
		final String methodName = "fetchOtherCategoryDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<CategoryDetail> categoryDetailLst = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);

			categoryDetailLst = simpleJdbcTemplate.query(ThisLocationQuery.FETCHOTHERCATEGORIESDETAILSQUERY,
					new BeanPropertyRowMapper<CategoryDetail>(CategoryDetail.class), retailId, userId);
		}
		catch (DataAccessException e)
		{

			log.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());

		}

		int preMainCategorId = 0;
		List<SubCategoryDetail> subList = null;
		final List<MainCategoryDetail> mainList = new ArrayList<MainCategoryDetail>();
		MainCategoryDetail mainCategoryDetail = null;
		CategoryDetail categoryDetailResp = null;
		CategoryDetail categoryDetail = null;
		int currentCatId = 0;
		for (final Iterator<CategoryDetail> iterator = categoryDetailLst.iterator(); iterator.hasNext();)
		{
			categoryDetail = iterator.next();
			currentCatId = categoryDetail.getParentCategoryID();
			if (preMainCategorId == 0)
			{
				preMainCategorId = categoryDetail.getParentCategoryID();
				mainCategoryDetail = new MainCategoryDetail();
				mainCategoryDetail.setMainCategoryId(preMainCategorId);
				mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());
				subList = new ArrayList<SubCategoryDetail>();
			}
			if (currentCatId != preMainCategorId)
			{
				mainCategoryDetail.setSubCategoryDetailLst(subList);
				mainList.add(mainCategoryDetail);
				subList = new ArrayList<SubCategoryDetail>();
				final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
				subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
				subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
				subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
				subList.add(subCategoryDetail);
				mainCategoryDetail = new MainCategoryDetail();
				mainCategoryDetail.setMainCategoryId(currentCatId);
				mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());
				preMainCategorId = categoryDetail.getParentCategoryID();
			}
			else
			{
				final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
				subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
				subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
				subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
				subList.add(subCategoryDetail);
			}
		}

		if (null != mainList && !mainList.isEmpty())
		{
			categoryDetailResp = new CategoryDetail();
			categoryDetailResp.setMainCategoryDetaillst(mainList);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return categoryDetailResp;
	}

	/**
	 * Method saving user AdvertisementHit in DB.
	 * 
	 * @param advertisementHitReq
	 *            The XML with userId and advertisementID.
	 * @return String Success if DB updation Successful else failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String saveAdvertisementHit(AdvertisementHitReq advertisementHitReq) throws ScanSeeException
	{

		final String methodName = "saveAdvertisementHit";
		if (log.isDebugEnabled())
		{
			log.debug(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + advertisementHitReq.getUserId());
		}

		String responseFromProc = null;
		Integer fromProc = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ThisLocationUserAdvertisementHit");
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", advertisementHitReq.getUserId());
			scanQueryParams.addValue("RetailLocationAdvertisementID", advertisementHitReq.getAdvertisementId());
			scanQueryParams.addValue("Date", Utility.getFormattedDate());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;

			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
			}
			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_ThisLocationUserAdvertisementHit ..errorNum..." + errorNum + "errorMsg.."
						+ errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in removeHDProduct", exception);
			throw new ScanSeeException(exception);
		}
		catch (ParseException exception)
		{
			log.error("Exception occurred in removeHDProduct", exception);
			throw new ScanSeeException(exception);

		}
		return responseFromProc;
	}

	/**
	 * This method fetches Location details from the database for the given
	 * user.
	 * 
	 * @param userID
	 *            The userID in the request.
	 * @return ThisLocationRequest info contains Latitude and Longitude.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public ThisLocationRequest fetchUserLocationDetails(Integer userID) throws ScanSeeException
	{
		final String methodName = "fetchUserLocationDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<ThisLocationRequest> thisLocationRequestLst = null;
		ThisLocationRequest thisLocationRequestObj = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			thisLocationRequestLst = simpleJdbcTemplate.query(ThisLocationQuery.FETCHUSERLOCATIONPOINTSQUERY,
					new BeanPropertyRowMapper<ThisLocationRequest>(ThisLocationRequest.class), userID);

			if (!thisLocationRequestLst.isEmpty() && thisLocationRequestLst != null)
			{
				thisLocationRequestObj = new ThisLocationRequest();
				thisLocationRequestObj.setLatitude(thisLocationRequestLst.get(0).getLatitude());
				thisLocationRequestObj.setLongitude(thisLocationRequestLst.get(0).getLongitude());
				thisLocationRequestObj.setPostalCode(thisLocationRequestLst.get(0).getPostalCode());
			}
		}
		catch (DataAccessException exception)
		{

			log.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());

		}
		return thisLocationRequestObj;
	}

	/**
	 * This method is used to fetch the Postal code for the given latitude and
	 * longitude from the Database.
	 * 
	 * @param latitude
	 *            The latitude in the request.
	 * @param longitude
	 *            The longitude in the request.
	 * @return ThisLocationRequest Contains Zipcode information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public ThisLocationRequest fetchUserPostalcode(Double latitude, Double longitude) throws ScanSeeException
	{
		final String methodName = "fetchUserPostalcode in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<ThisLocationRequest> thisLocationRequestLst = null;
		ThisLocationRequest thisLocationRequestObj = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			thisLocationRequestLst = simpleJdbcTemplate.query(ThisLocationQuery.FETCHUSERPOSTALCODEQUERY,
					new BeanPropertyRowMapper<ThisLocationRequest>(ThisLocationRequest.class), latitude, longitude);

			if (!thisLocationRequestLst.isEmpty() && thisLocationRequestLst != null)
			{
				thisLocationRequestObj = new ThisLocationRequest();
				thisLocationRequestObj.setPostalCode(thisLocationRequestLst.get(0).getPostalCode());
			}
		}
		catch (DataAccessException exception)
		{

			log.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());

		}
		return thisLocationRequestObj;
	}

	/**
	 * This method is used to fetch the Latitude and Longitude for the given
	 * Zipcode.
	 * 
	 * @param zipcode
	 *            Zipcode for getting Lat and Long.
	 * @return ThisLocationRequest Container lat and long.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public ThisLocationRequest fetchLatLong(Long zipcode) throws ScanSeeException
	{
		final String methodName = "fetchLatLong in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<ThisLocationRequest> thisLocationRequestLst = null;
		ThisLocationRequest thisLocationRequestObj = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			thisLocationRequestLst = simpleJdbcTemplate.query(ThisLocationQuery.FETCHLATLONG, new BeanPropertyRowMapper<ThisLocationRequest>(
					ThisLocationRequest.class), zipcode);

			if (!thisLocationRequestLst.isEmpty() && thisLocationRequestLst != null)
			{
				thisLocationRequestObj = new ThisLocationRequest();
				thisLocationRequestObj = thisLocationRequestLst.get(0);
			}
		}
		catch (DataAccessException exception)
		{

			log.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());

		}
		return thisLocationRequestObj;
	}

	/**
	 * This method is used to fetch product details for the given details.
	 * 
	 * @param searchDetails
	 *            - Details to get product details.
	 * @return ProductDetails contains sale price catageryid etc.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ProductDetails findNearByProducts(ProductDetail searchDetails) throws ScanSeeException
	{
		final String methodName = "findNearByProducts";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList;
		ProductDetails productDetails = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ThisLocationFindNearByProductPagination");
			simpleJdbcCall.returningResultSet("products", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", searchDetails.getUserId());
			scanQueryParams.addValue("Latitude", searchDetails.getScanLatitude());
			scanQueryParams.addValue("Longitude", searchDetails.getScanLongitude());
			scanQueryParams.addValue("ZipCode", searchDetails.getZipcode());
			scanQueryParams.addValue("Radius", searchDetails.getRadius());
			scanQueryParams.addValue("ProdSearch", searchDetails.getSearchkey());
			// scanQueryParams.addValue("RetailLocationID",
			// searchDetails.getRetailLocationID());
			scanQueryParams.addValue("LowerLimit", searchDetails.getLastVistedProductNo());
			scanQueryParams.addValue("ScreenName", ApplicationConstants.THISLOCATIONPRODUCTSEARCHSCREENNAME);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("products");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != productDetailList && !productDetailList.isEmpty())
					{
						log.info("Products found for the search");
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetailList);
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						if (nextpage != null)
						{
							if (nextpage)
							{
								productDetails.setNextPage(1);

							}
							else
							{
								productDetails.setNextPage(0);

							}
						}
					}
					else
					{
						log.info("Products found for the search");
						return productDetails;
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_SearchProduct Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in scanForProductName", exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	@Override
	public ArrayList<ThisLocationRequest> getRediusDetails() throws ScanSeeException
	{
		final String methodName = "getRediusDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ThisLocationRequest> thislocatonRediuslst;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FetchRadius");
			simpleJdbcCall.returningResultSet("radiuslst", new BeanPropertyRowMapper<ThisLocationRequest>(ThisLocationRequest.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			if (null == resultFromProcedure.get("ErrorNumber"))
			{
				thislocatonRediuslst = (ArrayList<ThisLocationRequest>) resultFromProcedure.get("radiuslst");

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_SearchProduct Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in scanForProductName", exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return thislocatonRediuslst;
	}

	/**
	 * This method for update user zip code information .
	 * 
	 * @param userId
	 * @param zipcode
	 * @return String with success or failure resposne.
	 * @throws ScanSeeException
	 *             The exeption defined for the application.
	 */
	@Override
	public String updateZipcode(Long userId, String zipcode) throws ScanSeeException
	{
		final String methodName = "saveUserEmailId";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc;
		Map<String, Object> resultFromProcedure = null;
		String response = null;
		Boolean validZipcode=null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserPostalcodeUpdation");
			final MapSqlParameterSource changePasswordParameters = new MapSqlParameterSource();
			changePasswordParameters.addValue("UserID", userId);
			changePasswordParameters.addValue("PostalCode", zipcode);
			resultFromProcedure = simpleJdbcCall.execute(changePasswordParameters);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				//response = ApplicationConstants.SUCCESS;
				validZipcode = (Boolean) resultFromProcedure.get("InvalidPostalCode");
				if(validZipcode != null)
				{
					if(validZipcode)
					{
						response=ApplicationConstants.INVALIDZIPCODE;
					}else
					{
						response=ApplicationConstants.ZIPCODEUPDATED;
					}
				}
				
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in saveUserEmailId method ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in saveUserEmailId", ApplicationConstants.DATAACCESSEXCEPTIONCODE, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String checkUsrZipcode(Long userId) throws ScanSeeException
	{
		final String methodName = "checkUsrZipcode";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc;
		Boolean postalCodeExists;
		Map<String, Object> resultFromProcedure = null;
		String response = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserPostalcodeCheck");
			final MapSqlParameterSource changePasswordParameters = new MapSqlParameterSource();
			changePasswordParameters.addValue("UserID", userId);
			resultFromProcedure = simpleJdbcCall.execute(changePasswordParameters);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{

				postalCodeExists = (Boolean) resultFromProcedure.get("PostalCodeExists");
				if (postalCodeExists != null)
				{
					if (postalCodeExists == true)

						response = ApplicationConstants.ZIPCODEEXISTS;
					else
					{
						response = ApplicationConstants.ZIPCODENOTEXISTS;
					}
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in saveUserEmailId method ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in saveUserEmailId", ApplicationConstants.DATAACCESSEXCEPTIONCODE, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This Rest Easy method for recording User retailer sale notification.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */

	@Override
	public String saveUsrRetNotification(RetailerDetail retailerDetailObj) throws ScanSeeException
	{
		final String methodName = "saveUsrRetNotification";
		if (log.isDebugEnabled())
		{
			log.debug(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + retailerDetailObj.getUserId());
		}

		String responseFromProc = null;
		Integer fromProc = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_QRUserRetailerSaleNotification");
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", retailerDetailObj.getUserId());
			scanQueryParams.addValue("RetailID", retailerDetailObj.getRetailerId());
			scanQueryParams.addValue("RetailLocationID", retailerDetailObj.getRetailLocationID());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;

			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
			}
			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_QRUserRetailerSaleNotification ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in saveUsrRetNotification", exception);
			throw new ScanSeeException(exception);
		}

		return responseFromProc;
	}

	/**
	 * This Rest Easy method for fetching retailer store details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id and page id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	@Override
	public List<RetailerDetail> fetchRetailerStoreDetails(RetailerDetail retailerDetailObj) throws ScanSeeException
	{
		final String methodName = "fetchRetailerStoreDetails";
		if (log.isDebugEnabled())
		{
			log.debug(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + retailerDetailObj.getUserId());
		}

		String responseFromProc = null;
		Integer fromProc = null;
		List<RetailerDetail> retailStoreslst = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_QRRetailerCreatedPagesDetails");
			simpleJdbcCall.returningResultSet("retailerStoreDetails", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", retailerDetailObj.getUserId());
			scanQueryParams.addValue("RetailID", retailerDetailObj.getRetailerId());
			scanQueryParams.addValue("RetailLocationID", retailerDetailObj.getRetailLocationID());
			scanQueryParams.addValue("PageID", retailerDetailObj.getPageID());
			//For user tracking
			scanQueryParams.addValue(ApplicationConstants.MAINMENUID, retailerDetailObj.getMainMenuID());
			scanQueryParams.addValue(ApplicationConstants.SCANTYPEID, retailerDetailObj.getScanTypeID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			retailStoreslst = (List<RetailerDetail>) resultFromProcedure.get("retailerStoreDetails");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					/*
					 * if (null != retailStoreslst &&
					 * !retailStoreslst.isEmpty()) {
					 * log.info("Products found for the search"); final Boolean
					 * nextpage = (Boolean)
					 * resultFromProcedure.get("NxtPageFlag"); if (nextpage !=
					 * null) { if (nextpage) { productDetails.setNextPage(1); }
					 * else { productDetails.setNextPage(0); } }
					 */

				}

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_QRRetailerCreatedPagesDetails ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchRetailerStoreDetails", exception);
			throw new ScanSeeException(exception);
		}

		return retailStoreslst;
	}

	/**
	 * This Rest Easy method for fetching special offer details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */
	@Override
	public List<RetailerDetail> fetchSpecialOffers(RetailerDetail specialOfferRequest) throws ScanSeeException
	{
		final String methodName = "fetchSpecialOffers";

		String responseFromProc = null;
		Integer fromProc = null;
		ProductDetails productDetailsObj = null;
		List<RetailerDetail> specialOfferlst = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_RetailerLocationSpecialOffersDisplay");
			simpleJdbcCall.returningResultSet("retailerStoreDetails", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", specialOfferRequest.getUserId());
			scanQueryParams.addValue("RetailID", specialOfferRequest.getRetailerId());
			scanQueryParams.addValue("RetailLocationID", specialOfferRequest.getRetailLocationID());

			if (null == specialOfferRequest.getLastVisitedNo())
			{
				specialOfferRequest.setLastVisitedNo(0);
			}

			scanQueryParams.addValue("LowerLimit", specialOfferRequest.getLastVisitedNo());
			scanQueryParams.addValue("ScreenName", ApplicationConstants.THISLOCATIONPRODUCTSSCREEN);
			//For user tracking
			scanQueryParams.addValue(ApplicationConstants.RETAILERLISTID, specialOfferRequest.getRetListID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			specialOfferlst = (List<RetailerDetail>) resultFromProcedure.get("retailerStoreDetails");
			if (null != resultFromProcedure)
			{

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_RetailerLocationSpecialOffersDisplay ..errorNum..." + errorNum + "errorMsg.."
						+ errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchRetailerStoreDetails", exception);
			throw new ScanSeeException(exception);
		}

		return specialOfferlst;
	}

	/**
	 * This Rest Easy method for fetching retailer summary details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */
	@Override
	public List<RetailerDetail> getRetailerSummary(RetailerDetail retailerDetailObj) throws ScanSeeException
	{
		final String methodName = "getRetailerSummary";
		if (log.isDebugEnabled())
		{
			log.debug(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + retailerDetailObj.getUserId());
		}

		String responseFromProc = null;
		Integer fromProc = null;
		List<RetailerDetail> retailSummarylst = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_DisplayRetailLocationInfo");
			simpleJdbcCall.returningResultSet("retailerSummaryDetails", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", retailerDetailObj.getUserId());
			scanQueryParams.addValue("RetailID", retailerDetailObj.getRetailerId());
			scanQueryParams.addValue("RetailLocationID", retailerDetailObj.getRetailLocationID());
			scanQueryParams.addValue("Latitude", retailerDetailObj.getRetLatitude());
			scanQueryParams.addValue("Longitude", retailerDetailObj.getRetLongitude());
			scanQueryParams.addValue("PostalCode", retailerDetailObj.getPostalCode());
			//For user tracking
			scanQueryParams.addValue(ApplicationConstants.MAINMENUID, retailerDetailObj.getMainMenuID());
			scanQueryParams.addValue(ApplicationConstants.SCANTYPEID, retailerDetailObj.getScanTypeID());
			scanQueryParams.addValue(ApplicationConstants.RETAILERLISTID, retailerDetailObj.getRetListID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			retailSummarylst = (List<RetailerDetail>) resultFromProcedure.get("retailerSummaryDetails");

			if (null == resultFromProcedure.get("ErrorNumber"))
			{

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_DisplayRetailLocationInfo ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchRetailerStoreDetails", exception);
			throw new ScanSeeException(exception);
		}

		return retailSummarylst;
	}

	/**
	 * This Rest Easy method for fetching retailer Special offer details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */
	@Override
	public List<RetailerCreatedPages> getRetCreatePages(RetailerDetail retailerDetailObj) throws ScanSeeException
	{
		final String methodName = "getRetCreatePages";
		if (log.isDebugEnabled())
		{
			log.debug(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + retailerDetailObj.getUserId());
		}

		String responseFromProc = null;
		Integer fromProc = null;
		List<RetailerCreatedPages> retCreatedPageslst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_QRDisplayRetailerCreatedPages");
			simpleJdbcCall.returningResultSet("retCreatedPages", new BeanPropertyRowMapper<RetailerCreatedPages>(RetailerCreatedPages.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", retailerDetailObj.getUserId());
			scanQueryParams.addValue("RetailID", retailerDetailObj.getRetailerId());
			scanQueryParams.addValue("RetailLocationID", retailerDetailObj.getRetailLocationID());
			//For User tracking
			scanQueryParams.addValue(ApplicationConstants.MAINMENUID, retailerDetailObj.getMainMenuID());
			scanQueryParams.addValue(ApplicationConstants.SCANTYPEID, retailerDetailObj.getScanTypeID());
			scanQueryParams.addValue(ApplicationConstants.RETAILERLISTID, retailerDetailObj.getRetListID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			retCreatedPageslst = (List<RetailerCreatedPages>) resultFromProcedure.get("retCreatedPages");

			if (null == resultFromProcedure.get("ErrorNumber"))
			{

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_QRDisplayRetailerCreatedPages ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchRetailerStoreDetails", exception);
			throw new ScanSeeException(exception);
		}

		return retCreatedPageslst;
	}

	/**
	 * This Rest Easy method for fetching special offer details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */
	@Override
	public ProductDetails fetchSpecialOffersHotDealDiscounts(ProductDetailsRequest productDetailsRequest) throws ScanSeeException
	{
		final String methodName = "fetchSpecialOffersHotDealDiscounts";

		String responseFromProc = null;
		Integer fromProc = null;
		ProductDetails productDetailsObj = null;
		List<RetailerDetail> specialOfferlst = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_RetailLocationSpecialDealsDisplay");
			simpleJdbcCall.returningResultSet("specialofferdiscounts", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", productDetailsRequest.getUserId());
			scanQueryParams.addValue("RetailID", productDetailsRequest.getRetailID());
			scanQueryParams.addValue("RetailLocationID", productDetailsRequest.getRetailLocationID());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			specialOfferlst = (List<RetailerDetail>) resultFromProcedure.get("specialofferdiscounts");
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					if (null != specialOfferlst && !specialOfferlst.isEmpty())
					{
						productDetailsObj = new ProductDetails();
						productDetailsObj.setSpecialOfferlst(specialOfferlst);
					}

				}

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_RetailLocationSpecialDealsDisplay ..errorNum..." + errorNum + "errorMsg.."
						+ errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchRetailerStoreDetails", exception);
			throw new ScanSeeException(exception);
		}

		return productDetailsObj;
	}

	@Override
	public CLRDetails getRetailerCLRDetails(RetailerDetail retailerDetail) throws ScanSeeException
	{
		final String methodName = "getRetailerCLRDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;
		/**
		 * This variables needed to check nextPage is 1 or 0 for pagination.
		 */
		boolean isCouponNextPage = false;
		boolean isLoyaltyNextPage = false;
		boolean isRebateNextPage = false;

		List<RebateDetail> rebateDetailList = null;
		List<LoyaltyDetail> loyaltyDetailList = null;
		List<CouponDetails> couponDetailList = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_RetailLocationCouponsDisplay");

			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue("UserID", retailerDetail.getUserId());
			fetchCouponParameters.addValue("RetailID", retailerDetail.getRetailerId());
			fetchCouponParameters.addValue("RetailLocationID", retailerDetail.getRetailLocationID());
			if (retailerDetail.getLastVisitedNo() == null)
			{
				retailerDetail.setLastVisitedNo(0);
			}
			fetchCouponParameters.addValue("LowerLimit", retailerDetail.getLastVisitedNo());
			fetchCouponParameters.addValue("ScreenName", ApplicationConstants.SLCLRSCREENNAME);
			//For user tracking
			fetchCouponParameters.addValue(ApplicationConstants.MAINMENUID, retailerDetail.getMainMenuID());
			fetchCouponParameters.addValue(ApplicationConstants.RETAILERLISTID, retailerDetail.getRetListID());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get("ErrorNumber"))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_RetailLocationCouponsDisplay method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");
					if (null != couponDetailList && !couponDetailList.isEmpty())
					{
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						if (nextpage != null)
						{
							if (nextpage)
							{
								isCouponNextPage = true;
							}
						}
					}
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_RetailLocationLoyaltyDealDisplay");
			simpleJdbcCall.returningResultSet("LoyaltyDetails", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final MapSqlParameterSource fetchLoyaltyParameters = new MapSqlParameterSource();
			fetchLoyaltyParameters.addValue("UserID", retailerDetail.getUserId());
			fetchLoyaltyParameters.addValue("RetailID", retailerDetail.getRetailerId());
			fetchLoyaltyParameters.addValue("RetailLocationID", retailerDetail.getRetailLocationID());
			fetchLoyaltyParameters.addValue("LowerLimit", retailerDetail.getLastVisitedNo());
			fetchLoyaltyParameters.addValue("ScreenName", ApplicationConstants.SLCLRSCREENNAME);
			//For user tracking
			fetchLoyaltyParameters.addValue(ApplicationConstants.MAINMENUID, retailerDetail.getMainMenuID());
			fetchLoyaltyParameters.addValue(ApplicationConstants.RETAILERLISTID, retailerDetail.getRetListID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_RetailLocationLoyaltyDealDisplay method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else
			{
				loyaltyDetailList = (List<LoyaltyDetail>) resultFromProcedure.get("LoyaltyDetails");
				if (null != loyaltyDetailList && !loyaltyDetailList.isEmpty())
				{
					final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					if (nextpage != null)
					{
						if (nextpage)
						{
							isRebateNextPage = true;
						}
					}

				}
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_RetailLocationRebateDisplay");
			simpleJdbcCall.returningResultSet("RebateDetails", new BeanPropertyRowMapper<RebateDetail>(RebateDetail.class));

			final MapSqlParameterSource fetchRebateParameters = new MapSqlParameterSource();
			fetchRebateParameters.addValue("UserID", retailerDetail.getUserId());
			fetchRebateParameters.addValue("RetailID", retailerDetail.getRetailerId());
			fetchRebateParameters.addValue("RetailLocationID", retailerDetail.getRetailLocationID());
			fetchRebateParameters.addValue("LowerLimit", retailerDetail.getLastVisitedNo());
			fetchRebateParameters.addValue("ScreenName", ApplicationConstants.SLCLRSCREENNAME);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRebateParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_RetailLocationRebateDisplay  ..errorNum.{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);

			}
			else
			{
				rebateDetailList = (List<RebateDetail>) resultFromProcedure.get("RebateDetails");
				if (null != rebateDetailList && !rebateDetailList.isEmpty())
				{
					final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					if (nextpage != null)
					{
						if (nextpage)
						{
							isLoyaltyNextPage = true;
						}
					}

				}

			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		if (couponDetailList != null || !couponDetailList.isEmpty() || rebateDetailList != null || !rebateDetailList.isEmpty()
				|| loyaltyDetailList != null || !loyaltyDetailList.isEmpty())
		{
			clrDetatilsObj = new CLRDetails();
			if (isCouponNextPage == true || isRebateNextPage == true || isRebateNextPage == true)
			{
				clrDetatilsObj.setNextPageFlag(1);
			}
			if (isCouponNextPage == true)
			{
				clrDetatilsObj.setClrC(1);
			}
			else
			{
				clrDetatilsObj.setClrC(0);
			}
			if (isLoyaltyNextPage == true)
			{
				clrDetatilsObj.setClrL(1);
			}
			else
			{
				clrDetatilsObj.setClrL(0);
			}
			if (isLoyaltyNextPage == true)
			{
				clrDetatilsObj.setClrR(1);
			}
			else
			{
				clrDetatilsObj.setClrR(0);
			}
			if (couponDetailList != null && !couponDetailList.isEmpty())
			{
				clrDetatilsObj.setIsCouponthere(true);
				clrDetatilsObj.setCouponDetails(couponDetailList);
			}
			else
			{
				clrDetatilsObj.setIsCouponthere(false);
			}
			if (rebateDetailList != null && !rebateDetailList.isEmpty())
			{
				clrDetatilsObj.setIsRebatethere(true);
				clrDetatilsObj.setRebateDetails(rebateDetailList);
			}
			else
			{
				clrDetatilsObj.setIsRebatethere(false);
			}
			if (loyaltyDetailList != null && !loyaltyDetailList.isEmpty())
			{
				clrDetatilsObj.setIsLoyaltythere(true);
				clrDetatilsObj.setLoyaltyDetails(loyaltyDetailList);
			}
			else
			{
				clrDetatilsObj.setIsLoyaltythere(false);
			}

		}
		return clrDetatilsObj;
	}

	/**
	 * This DAO method for fetching retailer location hot deals.
	 * 
	 * @param xml
	 *            as the request object containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response object or exception.
	 */
	@Override
	public List<HotDealsDetails> getRetailerHotDeals(RetailerDetail retailerDetailObj) throws ScanSeeException
	{
		final String methodName = "getRetailerHotDeals";
		if (log.isDebugEnabled())
		{
			log.debug(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + retailerDetailObj.getUserId());
		}

		List<HotDealsDetails> hotDealsDetailslst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_RetailLocationHotDealsDisplay");
			simpleJdbcCall.returningResultSet("hotDeals", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", retailerDetailObj.getUserId());
			scanQueryParams.addValue("RetailID", retailerDetailObj.getRetailerId());
			scanQueryParams.addValue("RetailLocationID", retailerDetailObj.getRetailLocationID());
			scanQueryParams.addValue("ScreenName", ApplicationConstants.RETAILERHOTDEALS);

			if (retailerDetailObj.getLastVisitedNo() == null)
			{
				retailerDetailObj.setLastVisitedNo(0);
			}

			scanQueryParams.addValue("LowerLimit", retailerDetailObj.getLastVisitedNo());
			//For user tracking
			scanQueryParams.addValue(ApplicationConstants.RETAILERLISTID, retailerDetailObj.getRetListID());
			scanQueryParams.addValue(ApplicationConstants.MAINMENUID, retailerDetailObj.getMainMenuID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			hotDealsDetailslst = (List<HotDealsDetails>) resultFromProcedure.get("hotDeals");

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_RetailLocationHotDealsDisplay ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getRetailerHotDeals", exception);
			throw new ScanSeeException(exception);
		}
		return hotDealsDetailslst;
	}

	@Override
	public List<RetailerDetail> fetchSpecialDealsDetails(RetailerDetail specialOfferRequest) throws ScanSeeException
	{
		final String methodName = "fetchSpecialOffers";

		String responseFromProc = null;
		Integer fromProc = null;
		ProductDetails productDetailsObj = null;
		List<RetailerDetail> specialOfferlst = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_RetailerLocationSpecialOffersDetails");
			simpleJdbcCall.returningResultSet("retailerStoreDetails", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("PageID", specialOfferRequest.getPageID());
			scanQueryParams.addValue("RetailID", specialOfferRequest.getRetailerId());
			scanQueryParams.addValue("RetailLocationID", specialOfferRequest.getRetailLocationID());

			if (null == specialOfferRequest.getLastVisitedNo())
			{
				specialOfferRequest.setLastVisitedNo(0);
			}

			scanQueryParams.addValue("LowerLimit", specialOfferRequest.getLastVisitedNo());
			scanQueryParams.addValue("ScreenName", ApplicationConstants.THISLOCATIONPRODUCTSSCREEN);
			//For user tracking
			scanQueryParams.addValue(ApplicationConstants.RETAILERLISTID, specialOfferRequest.getRetListID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			specialOfferlst = (List<RetailerDetail>) resultFromProcedure.get("retailerStoreDetails");
			if (null != resultFromProcedure)
			{

			}

			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + "usp_RetailerLocationSpecialOffersDetails ..errorNum..." + errorNum + "errorMsg.."
						+ errorMsg);
				responseFromProc = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchRetailerStoreDetails", exception);
			throw new ScanSeeException(exception);
		}

		return specialOfferlst;
	}

	/**
	 * The method fetches the Retailer Details from the database for the given City
	 * 
	 * @param thisLocationRequest
	 *            Instance of ThisLocationRequest.
	 * @param screenName
	 *            screenName to be used for Pagination size.
	 * @return RetailersDetails returns RetailerDetails object container List of
	 *         retailers.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer.
	 */
	@Override
	public RetailersDetails getRetailersByGroupID(ThisLocationRequest thisLocationRequest, String screenName) throws ScanSeeException {
		final String methodName = "getRetailersByGroupID in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailerDetail> retailerList = null;
		RetailersDetails details = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FetchGroupRetailerList");
			simpleJdbcCall.returningResultSet("retailers", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));

			final MapSqlParameterSource fetchRetailerDetailsParameters = new MapSqlParameterSource();

			fetchRetailerDetailsParameters.addValue(ApplicationConstants.USERID, thisLocationRequest.getUserId());
			fetchRetailerDetailsParameters.addValue("RetailGroupID", thisLocationRequest.getRetGroupID());
			fetchRetailerDetailsParameters.addValue("Longitude", thisLocationRequest.getLongitude());
			fetchRetailerDetailsParameters.addValue("Latitude", thisLocationRequest.getLatitude());
			fetchRetailerDetailsParameters.addValue("ZipCode", thisLocationRequest.getZipcode());
			fetchRetailerDetailsParameters.addValue("LowerLimit", thisLocationRequest.getLastVisitedRecord());
			fetchRetailerDetailsParameters.addValue("ScreenName", screenName);
			fetchRetailerDetailsParameters.addValue("CategoryID", thisLocationRequest.getCatID());
			fetchRetailerDetailsParameters.addValue("SearchKey", thisLocationRequest.getSearchKey());
			//For user tracking
			fetchRetailerDetailsParameters.addValue(ApplicationConstants.MAINMENUID, thisLocationRequest.getMainMenuID());
//			fetchRetailerDetailsParameters.addValue(ApplicationConstants.LOCATIONDETAILSID, thisLocationRequest.getLocDetailsID());
			// below param are output params from SP.
			fetchRetailerDetailsParameters.addValue("ErrorNumber", null);
			fetchRetailerDetailsParameters.addValue("ErrorMessage", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRetailerDetailsParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					details = new RetailersDetails();
					retailerList = (List<RetailerDetail>) resultFromProcedure.get("retailers");
					String retGroupImg = (String) resultFromProcedure.get("RetailGroupButtonImagePath");
					details.setRetGroupImg(retGroupImg);
					if (null != retailerList && !retailerList.isEmpty())
					{
						details.setRetailerDetail(retailerList);
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						final Integer retailAffiliateID = (Integer) resultFromProcedure.get("RetailAffiliateID");
						final Integer maxCnt = (Integer) resultFromProcedure.get("MaxCnt");
						final Integer retAffCount = (Integer) resultFromProcedure.get("RetailAffiliateCount");
						final String retAffName = (String) resultFromProcedure.get("RetailAffiliateName");
						
						if (nextpage != null)
						{
							if (nextpage)
							{
								details.setNextPage(1);
							}
							else
							{
								details.setNextPage(0);
							}
						}
						
						if(null != retailAffiliateID)	{
							details.setRetAffID(retailAffiliateID);
						} else	{
							details.setRetAffID(0);
						}
						
						details.setMaxCnt(maxCnt);
						details.setRetAffCount(retAffCount);
						details.setRetAffName(retAffName);
					}
					else
					{
						log.info("No Retailers found ");
						return details;
					}
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in usp_FetchGroupRetailerList Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return details;
	}

	/**
	 * The method fetches the Retailer Details from the database for the given Partner.
	 * 
	 * @param thisLocationRequest
	 *            Instance of ThisLocationRequest.
	 * @param screenName
	 *            screenName to be used for Pagination size.
	 * @return RetailersDetails returns RetailerDetails object container List of
	 *         retailers.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer.
	 */
	@Override
	public RetailersDetails getRetailersForPartner(ThisLocationRequest thisLocationRequest, String screenName) throws ScanSeeException {
		final String methodName = "getRetailersForPartner in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailerDetail> retailerList = null;
		RetailersDetails details = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FetchAffiliateRetailerList");
			simpleJdbcCall.returningResultSet("retailers", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));

			final MapSqlParameterSource fetchRetailerDetailsParameters = new MapSqlParameterSource();

			fetchRetailerDetailsParameters.addValue(ApplicationConstants.USERID, thisLocationRequest.getUserId());
			fetchRetailerDetailsParameters.addValue("RetailAffiliateID", thisLocationRequest.getRetAffID());
			fetchRetailerDetailsParameters.addValue("Longitude", thisLocationRequest.getLongitude());
			fetchRetailerDetailsParameters.addValue("Latitude", thisLocationRequest.getLatitude());
			fetchRetailerDetailsParameters.addValue("ZipCode", thisLocationRequest.getZipcode());
			fetchRetailerDetailsParameters.addValue("LowerLimit", thisLocationRequest.getLastVisitedRecord());
			fetchRetailerDetailsParameters.addValue("ScreenName", screenName);
			fetchRetailerDetailsParameters.addValue("CategoryID", thisLocationRequest.getCatID());
			fetchRetailerDetailsParameters.addValue("PartnerID", thisLocationRequest.getPartnerID());
			fetchRetailerDetailsParameters.addValue("SearchKey", thisLocationRequest.getSearchKey());
			//For user tracking
			fetchRetailerDetailsParameters.addValue(ApplicationConstants.MAINMENUID, thisLocationRequest.getMainMenuID());

			// below param are output params from SP.
			fetchRetailerDetailsParameters.addValue("ErrorNumber", null);
			fetchRetailerDetailsParameters.addValue("ErrorMessage", null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRetailerDetailsParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					retailerList = (List<RetailerDetail>) resultFromProcedure.get("retailers");
					if (null != retailerList && !retailerList.isEmpty())
					{
						details = new RetailersDetails();
						details.setRetailerDetail(retailerList);
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						final Integer maxCnt = (Integer) resultFromProcedure.get("MaxCnt");
						final String retGroupImg = (String) resultFromProcedure.get("RetailGroupButtonImagePath");
						
						if (nextpage != null)
						{
							if (nextpage)
							{
								details.setNextPage(1);
							}
							else
							{
								details.setNextPage(0);
							}
						}
						details.setMaxCnt(maxCnt);
						details.setRetGroupImg(retGroupImg);
					}
					else
					{
						log.info("No Retailers found ");
						return details;
					}
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in getRetailersForGoLocalAffiliate Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return details;
	}

	/**
	 * The method fetches the Partners for Austin experience from the database.
	 * 
	 * @param N/A
	 * 
	 * @return {@link ThisLocationRequest} returns ThisLocationRequest object container List of
	 *         partners.
	 *         
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer.
	 */
	@Override
	public ThisLocationRequest getPartners(Integer retGroupID) throws ScanSeeException {
		final String methodName = "getPartners in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ThisLocationRequest> partnerList = null;
		ThisLocationRequest partnerDetails = null;
		
		try	{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FetchAffiliates");
			simpleJdbcCall.returningResultSet("partners", new BeanPropertyRowMapper<ThisLocationRequest>(ThisLocationRequest.class));

			final MapSqlParameterSource partners = new MapSqlParameterSource();
			partners.addValue("RetailGroupID", retGroupID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(partners);
			
			if(null != resultFromProcedure)	{
				if(null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))	{
					partnerList = (List<ThisLocationRequest>)resultFromProcedure.get("partners");
					Integer maxCnt = (Integer) resultFromProcedure.get("MaxCnt");
					if(partnerList != null || !partnerList.isEmpty()) {
						partnerDetails = new ThisLocationRequest();
						partnerDetails.setPartnerList(partnerList);
						partnerDetails.setMaxCnt(maxCnt);
					}
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in usp_FetchAffiliates Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());

		}
		return partnerDetails;
	}
	
	/**
	 * The method fetches the categories belonging to group retailers from the database.
	 * 
	 * @param retGroupID.
	 * 
	 * @return returns CategoryInfo object container List of categories.
	 * 
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer.
	 */
	@Override
	public List<CategoryInfo> getCategoriesForGroupRetailers(Integer retGroupID) throws ScanSeeException {
		final String methodName = "getCategoriesForGroupRetailers in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CategoryInfo> categoryInfoList = null;
		try	{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FetchGroupRetaierCategories");
			simpleJdbcCall.returningResultSet("affiliateRetailers", new BeanPropertyRowMapper<CategoryInfo>(CategoryInfo.class));

			final MapSqlParameterSource fetchRetailerDetailsParameters = new MapSqlParameterSource();
			fetchRetailerDetailsParameters.addValue("RetailGroupID", retGroupID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRetailerDetailsParameters);		
			if(null != resultFromProcedure)	{
				if(null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))	{
					categoryInfoList = (List<CategoryInfo>) resultFromProcedure.get("affiliateRetailers");
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in usp_FetchAffiliateRetailerCategories Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());

		}
		return categoryInfoList;
	}

	/**
	 * The method fetches the categories belonging to partner retailers from the database.
	 * 
	 * @param retAffID.
	 * 
	 * @return returns CategoryInfo object container List of categories.
	 * 
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application will be thrown which is caught in the
	 *             Controller layer.
	 */
	@Override
	public List<CategoryInfo> getCategoriesForPartners(Integer retAffID) throws ScanSeeException {
		final String methodName = "getCategoriesForPartners in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CategoryInfo> categoryInfoList = null;
		try	{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FetchAffiliateRetailerCategories");
			simpleJdbcCall.returningResultSet("affiliateRetailers", new BeanPropertyRowMapper<CategoryInfo>(CategoryInfo.class));

			final MapSqlParameterSource fetchRetailerDetailsParameters = new MapSqlParameterSource();
			fetchRetailerDetailsParameters.addValue("RetailAffiliateID", retAffID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRetailerDetailsParameters);		
			if(null != resultFromProcedure)	{
				if(null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))	{
					categoryInfoList = (List<CategoryInfo>) resultFromProcedure.get("affiliateRetailers");
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in usp_FetchAffiliateRetailerCategories Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());

		}
		return categoryInfoList;
	}


	/**
	 * For user tracking.
	 * 
	 * @param userTrackingData
	 * @return xml response containing SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 */
	@Override
	public String userTrackingRetailerSummaryClick(UserTrackingData userTrackingData) throws ScanSeeException {
		final String methodName = "userTrackingLocationDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Boolean bValue = false;
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserTrackingRetailerSummaryClicks");
			simpleJdbcCall.returningResultSet("retailers", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));

			final MapSqlParameterSource userTrackLocationDetails = new MapSqlParameterSource();

			userTrackLocationDetails.addValue("RetailerDetailsID", userTrackingData.getRetDetailsID());
			userTrackLocationDetails.addValue("AnythingPageListID", userTrackingData.getAnythingPageListID());
			userTrackLocationDetails.addValue(ApplicationConstants.RETAILERLISTID, userTrackingData.getRetListID());
			if (userTrackingData.getBanADClick() == null) {
				userTrackingData.setBanADClick(bValue);
			}
			userTrackLocationDetails.addValue("BannerADClick", userTrackingData.getBanADClick());
			if (userTrackingData.getCallStoreClick() == null) {
				userTrackingData.setCallStoreClick(bValue);
			}
			userTrackLocationDetails.addValue("CallStoreClick", userTrackingData.getCallStoreClick());
			if (userTrackingData.getBrowseWebClick() == null) {
				userTrackingData.setBrowseWebClick(bValue);
			}
			userTrackLocationDetails.addValue("BrowseWebsiteClick", userTrackingData.getBrowseWebClick());
			if (userTrackingData.getGetDirClick() == null) {
				userTrackingData.setGetDirClick(bValue);
			}
			userTrackLocationDetails.addValue("GetDirectionsClick", userTrackingData.getGetDirClick());
			if (userTrackingData.getAnythingPageClick() == null) {
				userTrackingData.setAnythingPageClick(bValue);
			}
			userTrackLocationDetails.addValue("AnythingPageClick", userTrackingData.getAnythingPageClick());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(userTrackLocationDetails);
			
			Integer status = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) {
					if (status == 0) {
						response = ApplicationConstants.SUCCESSRESPONSETEXT;
					} 
					else {
						response = ApplicationConstants.FAILURE;
					}
				}
				else	{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in usp_UserTrackingRetailerSummaryClicks Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
			
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());
		}
		
		log.info(ApplicationConstants.METHODEND + methodName);
		
		return response;
	}

	/**
	 * For user tracking.
	 * 
	 * @param UserTrackingData object
	 * @return SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 */
	@Override
	public String userTrackingRetSpeOffersClick(UserTrackingData userTrackingData) throws ScanSeeException {
		final String methodName = "userTrackingRetSpeOffersClick in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserTrackingRetailerLocationSpecialOffersClicks");

			final MapSqlParameterSource userTrackLocationDetails = new MapSqlParameterSource();

			userTrackLocationDetails.addValue(ApplicationConstants.USERID, userTrackingData.getUserID());
			userTrackLocationDetails.addValue(ApplicationConstants.RETAILERLISTID, userTrackingData.getRetListID());
			userTrackLocationDetails.addValue("SpecialsListID", userTrackingData.getSpeListID());
			userTrackLocationDetails.addValue("SpecialOfferClick", userTrackingData.getSpeOffClick());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(userTrackLocationDetails);
			
			Integer status = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER)) {
					if (status == 0) {
						response = ApplicationConstants.SUCCESSRESPONSETEXT;
					} 
					else {
						response = ApplicationConstants.FAILURE;
					}
				}
				else	{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in userTrackingRetSpeOffersClick Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
			
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());
		}
		
		log.info(ApplicationConstants.METHODEND + methodName);
		
		return response;
	}

}