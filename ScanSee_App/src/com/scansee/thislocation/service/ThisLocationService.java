package com.scansee.thislocation.service;

import com.scansee.common.exception.ScanSeeException;

/**
 * The ThisLocationService interface. The methods declared are called from the
 * ThisLocation Controller Layer. The methods declared contain call to the
 * ThisLocation DAO layer. implemented by {@link ThisLocationServiceImpl}
 * 
 * @author shyamsundara_hm
 */

public interface ThisLocationService
{

	/**
	 * The service method for searching retailers. Calls the XStreams helper to
	 * parse the given request XML and validates the required fields.
	 * 
	 * @param xml
	 *            - the input request contains search parameters like zipcode or
	 *            location attributes.
	 * @return response XML as a String containing Retailers list or No
	 *         retailers found message.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getRetailersInfoForLocation(String xml) throws ScanSeeException;

	/**
	 * The service method for fetching the category list for the specified
	 * Retailer id. Calls the XStreams helper for parsing request xml.It
	 * validates the require parameters and if validation fails return
	 * insufficient data.
	 * 
	 * @param xml
	 *            - the request xml container retailer id.
	 * @return response as String XML containing Category list or No category
	 *         found message.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getCategoryInfo(String xml) throws ScanSeeException;

	/**
	 * The Service method for fetching Products list for the given Category and
	 * Retailer location ID. Calls the XStreams helper for parsing the request
	 * XML. Validates the request XML. if validation succeeds then it will call
	 * DAO.
	 * 
	 * @param xml
	 *            - the request XML containingCategory and Retailer location ID.
	 * @return response XML as String with Product list or No Product found
	 *         message.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getProductsInfo(String xml) throws ScanSeeException;

	/**
	 * This method for getting favorite category information of user.
	 * 
	 * @param userID
	 *            - The userID in the request.
	 * @param retailID
	 *            - The retailID in the request.
	 * @return xml with Favourite Category Info.
	 * @throws ScanSeeException
	 *             The exeption defined for the application.
	 */

	String getFavCategoryInfo(Integer userID, Integer retailID) throws ScanSeeException;

	/**
	 * This method for getting other category information of user.
	 * 
	 * @param userID
	 *            - The userID in the request.
	 * @param retailID
	 *            -The retailID in the request.
	 * @return xml with Favourite Category Info.
	 * @throws ScanSeeException
	 *             The exeption defined for the application.
	 */
	String getOtherCategoriesInfo(Integer userID, Integer retailID) throws ScanSeeException;

	/**
	 * Method declaration for method for saving advertisement Hit.
	 * 
	 * @param xml
	 *            - The request xml with userID and advertisemendID.
	 * @throws ScanSeeException
	 *             - for Database exception.
	 * @return xml based on success or failure.
	 */

	String saveAdvertisementHit(String xml) throws ScanSeeException;

	/**
	 * This is Service method for fetching user's Latitude, Longitude info.This
	 * method validates user id and calls DAO method.
	 * 
	 * @param userID
	 *            - whose location attributes need to be fetched.
	 * @return returns response XML Containing User's Location attributes or No
	 *         records found message.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String getUserLocationPoints(Integer userID) throws ScanSeeException;

	/**
	 * THis is service method for fetching postal code from given Latitude,
	 * Longitude.
	 * 
	 * @param latitude
	 *            as request parameter containing latitude value.
	 * @param longitude
	 *            as request parameter containing longitude value
	 * @return returns response XML Containing Zipcode or No records found
	 *         message.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String getUserPostalcode(Double latitude, Double longitude) throws ScanSeeException;

	/**
	 * This is Rest Easy Webservice for getting Latitude and Longitude for the
	 * given Zipcode.
	 * 
	 * @param zipcode
	 *            - as request parameter for Zipcode.
	 * @return returns response XML Containing User's Location attributes if
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer. exception it will return error message XML.
	 */
	String getLatLong(Long zipcode) throws ScanSeeException;

	/**
	 * This is Rest Easy Webservice for fetching product deatils for the given
	 * userid and retailLocationID.
	 * 
	 * @param xml
	 *            -As request parameter
	 * @return String
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer. exception it will return error message XML.
	 */
	String findNearByProducts(String xml) throws ScanSeeException;

	/**
	 * This method for getting radius information .
	 * 
	 * @return xml with radius information.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	String getRediusInfo() throws ScanSeeException;

	/**
	 * This method for update user zip code information .
	 * 
	 * @param userId
	 * @param zipcode
	 * @return String with success or failure resposne.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	String updateZipcode(Long userId, String zipcode) throws ScanSeeException;

	/**
	 * This method for check user zip code information .
	 * 
	 * @param userId
	 * @return String with success or failure resposne.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	String checkUsrZipcode(Long userId) throws ScanSeeException;

	/**
	 * This Rest Easy method for recording User retailer sale notification.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	String saveUsrRetNotification(String xml) throws ScanSeeException;

	/**
	 * This Rest Easy method for fetching retailer store information.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	String fetchRetailerStoreInfo(String xml) throws ScanSeeException;

	/**
	 * This Rest Easy method for fetching retailer summary information.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */
	String getRetailerSummary(String xml) throws ScanSeeException;
	/**
	 * This Rest Easy method for fetching retailer special offer  information.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */
	String getRetailerSpecialOffDetails(String xml)throws ScanSeeException;

	 String getRetailerCLRDetails(String xml)throws ScanSeeException;
	 
	 /**
		 * This Service Layer method for fetching retailer location hot deal information.
		 * 
		 * @param xml
		 *            as the request XML containing user info and Retailer
		 *            id,retailer location id info.
		 * @return returns response XML As String Containing hot deals or if
		 *         exception it will return error message XML.
		 */
		String getRetailerHotDeals(String xml) throws ScanSeeException;
		 /**
		 * This Service Layer method for fetching retailer location special offer ,hot deal and coupon,rebate and loyalty information.
		 * 
		 * @param xml
		 *            as the request XML containing user info and Retailer
		 *            id,retailer location id info.
		 * @return returns response XML As String Containing hot deals or if
		 *         exception it will return error message XML.
		 */
		String getRetailerSpeHotDealsCLRDetails(String xml)throws ScanSeeException;
		
		String fetchSpecialDealsDetails(String xml)throws ScanSeeException;
		
		/**
		 * The service method for searching retailers by city. Calls the XStreams helper to
		 * parse the given request XML and validates the required fields.
		 * 
		 * @param xml
		 *            - the input request contains search parameters like city name, zipcode or
		 *            location attributes.
		 * @return response XML as a String containing Retailers list or No
		 *         retailers found message.
		 * @throws ScanSeeException
		 *             The exceptions are caught and a ScanSee Exception defined for
		 *             the application is thrown which is caught in the Controller
		 *             layer.
		 */
		String getRetailersByGroupID(String xml) throws ScanSeeException;
		
		/**
		 * The service method for searching retailers for Go Local. Calls the XStreams helper to
		 * parse the given request XML and validates the required fields.
		 * 
		 * @param xml
		 *            - the input request contains search parameters like city name, zipcode or
		 *            location attributes.
		 * @return response XML as a String containing Retailers list or No
		 *         retailers found message.
		 * @throws ScanSeeException
		 *             The exceptions are caught and a ScanSee Exception defined for
		 *             the application is thrown which is caught in the Controller
		 *             layer.
		 */
		String getRetailersForPartner(String xml) throws ScanSeeException;
		
		/**
		 * The service method to get partners. Calls the XStreams helper to
		 * parse the given request XML and validates the required fields.
		 * 
		 * @param xml
		 * @return response XML as a string containing partners
		 * @throws ScanSeeException
		 */
		String getPartners(Integer retGroupID) throws ScanSeeException;
		
		/**
		 * The service method to get categories for group retailers. It also validates the required fields.
		 * 
		 * @param retGroupID
		 * @return response XML as a string containing categories
		 * @throws ScanSeeException
		 */
		String getCategoriesForGroupRetailers(Integer retGroupID) throws ScanSeeException;
		
		/**
		 * The service method to get categories for partner retailers. It also validates the required fields.
		 * 
		 * @param retAffID
		 * @return response XML as a string containing categories
		 * @throws ScanSeeException
		 */
		String getCategoriesForPartners(Integer retAffID) throws ScanSeeException;
		
		/**
		 * For user tracking retailer summary clicks.
		 * To be called whenever user taps on any of the item in the Retailer summary screen.
		 * 
		 * @param xml
		 * @return xml containing SUCCESS or FAILURE response
		 * @throws ScanSeeException
		 */
		String userTrackingRetailerSummaryClick(String xml) throws ScanSeeException;
		
		/**
		 * For user tracking.
		 * To be called when the user taps on any special offers.
		 * 
		 * @param xml
		 * @return xml response SUCCESS or FAILURE
		 * @throws ScanSeeException
		 */
		String userTrackingRetSpeOffersClick(String xml) throws ScanSeeException;

}
