package com.scansee.thislocation.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.helper.XstreamParserHelper;
import com.scansee.common.pojos.AdvertisementHitReq;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.CategoryDetail;
import com.scansee.common.pojos.CategoryInfo;
import com.scansee.common.pojos.CategoryRequest;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.ProductDetailsRequest;
import com.scansee.common.pojos.RetailerCreatedPages;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.ThisLocationRequest;
import com.scansee.common.pojos.ThisLocationRetailerInfo;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.common.util.Utility;
import com.scansee.firstuse.dao.FirstUseDAO;
import com.scansee.thislocation.dao.ThisLocationDAO;

/**
 * The class implements ThisLocationService interface and has methods for
 * getRetailersInfoForLocation,getProductsInfo,getCategoryInfo The methods of
 * this class are called from the ThisLocation Controller Layer. The methods
 * contain call to the ThisLocation DAO layer.
 * 
 * @author shyamsundara_hm
 */

public class ThisLocationServiceImpl implements ThisLocationService
{

	/**
	 * Getting the logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ThisLocationServiceImpl.class);

	/**
	 * The variable of type ThisLocationDAO.
	 */

	private ThisLocationDAO thisLocationDao;
	
	/**
	 * Variable type for FirstUse
	 */
	private FirstUseDAO firstUseDao;

	/**
	 * Setter method for thisLocationDao.
	 * 
	 * @param thisLocationDao
	 *            the Variable of type ThisLocationDAO
	 */

	public void setThisLocationDao(ThisLocationDAO thisLocationDao)
	{
		this.thisLocationDao = thisLocationDao;
	}
	
	/**
	 * Setter method for FirstUseDAO.
	 * 
	 * @param firstUseDAO
	 *            the object of type FirstUseDAO
	 */
	public void setFirstUseDao(FirstUseDAO firstUseDao)
	{
		this.firstUseDao = firstUseDao;
	}

	/**
	 * The service method for searching retailers. Calls the XStreams helper to
	 * parse the given request XML and validates the required fields.if
	 * validation succeeds then it will call DAO.
	 * 
	 * @param xml
	 *            the input request contains search parameters like zipcode or
	 *            location attributes.
	 * @return response XML as a String containing Retailers list or No
	 *         retailers found message.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String getRetailersInfoForLocation(String xml) throws ScanSeeException
	{
		final String methodName = "getRetailersInfoForLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Boolean gpsEnabled;
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		String completeAddress;
		final ThisLocationRequest thisLocationRequest = (ThisLocationRequest) streamHelper.parseXmlToObject(xml);
		if (thisLocationRequest.getUserId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
			return response;
		}

		gpsEnabled = thisLocationRequest.isGpsEnabled();

		if (!gpsEnabled)
		{
			thisLocationRequest.setLatitude(null);
			thisLocationRequest.setLongitude(null);
		}
		if (null == thisLocationRequest.getLastVisitedRecord())
		{
			// if this property is zero
			thisLocationRequest.setLastVisitedRecord(0);
		}
		
		//For user tracking
		Integer mainMenuID = null;
		if (thisLocationRequest.getMainMenuID() == null) {
			UserTrackingData objUserTrackingData = new UserTrackingData();
			objUserTrackingData.setUserID(thisLocationRequest.getUserId());
			objUserTrackingData.setModuleID(thisLocationRequest.getModuleID());
			objUserTrackingData.setLatitude(thisLocationRequest.getLatitude());
			objUserTrackingData.setLongitude(thisLocationRequest.getLongitude());
			objUserTrackingData.setPostalCode(thisLocationRequest.getZipcode());
			mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);
			thisLocationRequest.setMainMenuID(mainMenuID);
		}
		else 	{
			mainMenuID = thisLocationRequest.getMainMenuID();
		}
		
		final RetailersDetails retailsDetails = thisLocationDao.fetchRetailerDetails(thisLocationRequest, ApplicationConstants.THISLOCATIONRETAILERSCREEN);

//		retailsDetails.setMainMenuID(mainMenuID);
		
		List<ThisLocationRetailerInfo> thisLocationRetailerInfoList = null;

		if (null != retailsDetails)
		{
			final List<RetailerDetail> retailerDetail = retailsDetails.getRetailerDetail();
			if (!retailerDetail.isEmpty())
			{
				thisLocationRetailerInfoList = new ArrayList<ThisLocationRetailerInfo>();
				for (int i = 0; i < retailerDetail.size(); i++)
				{
					final ThisLocationRetailerInfo thisLocationRetailerInfo = new ThisLocationRetailerInfo();
					completeAddress = new String();
					if (null != retailerDetail.get(i).getRetaileraddress1()
							&& !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getRetaileraddress1())
							&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getRetaileraddress1()))
					{
						completeAddress = retailerDetail.get(i).getRetaileraddress1() + ApplicationConstants.COMMA;
					}
					if (null != retailerDetail.get(i).getCity() && !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getCity())
							&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getCity()))
					{
						completeAddress = completeAddress + retailerDetail.get(i).getCity() + ApplicationConstants.COMMA;
					}
					if (null != retailerDetail.get(i).getPostalCode()
							&& !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getPostalCode())
							&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getPostalCode()))
					{
						completeAddress = completeAddress + retailerDetail.get(i).getPostalCode() + ApplicationConstants.COMMA;
					}
					if (null != retailerDetail.get(i).getState() && !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getState())
							&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getState()))
					{
						completeAddress = completeAddress + retailerDetail.get(i).getState();
					}

					thisLocationRetailerInfo.setRowNumber(retailerDetail.get(i).getRowNumber());
					thisLocationRetailerInfo.setRetailerId(retailerDetail.get(i).getRetailerId());
					thisLocationRetailerInfo.setRetailerName(retailerDetail.get(i).getRetailerName());
					thisLocationRetailerInfo.setRetailLocationID(retailerDetail.get(i).getRetailLocationID());
					thisLocationRetailerInfo.setRetailAddress(completeAddress);
					thisLocationRetailerInfo.setDistance(retailerDetail.get(i).getDistance());
					thisLocationRetailerInfo.setLogoImagePath(retailerDetail.get(i).getLogoImagePath());
					thisLocationRetailerInfo.setBannerAdImagePath(retailerDetail.get(i).getBannerAdImagePath());
					thisLocationRetailerInfo.setRibbonAdImagePath(retailerDetail.get(i).getRibbonAdImagePath());
					thisLocationRetailerInfo.setRibbonAdURL(retailerDetail.get(i).getRibbonAdURL());
					thisLocationRetailerInfo.setSaleFlag(retailerDetail.get(i).getSaleFlag());
					thisLocationRetailerInfo.setSplashAdID(retailerDetail.get(i).getSplashAdID());
					thisLocationRetailerInfo.setRetListID(retailerDetail.get(i).getRetListID());
					thisLocationRetailerInfo.setRetGroupImg(retailerDetail.get(i).getRetGroupImg());

					thisLocationRetailerInfoList.add(thisLocationRetailerInfo);
				}

			}
			
			if(thisLocationRetailerInfoList !=null && !thisLocationRetailerInfoList.isEmpty())
			{
				retailsDetails.setMainMenuID(mainMenuID);
				retailsDetails.setRetailerDetail(null);
				retailsDetails.setThisLocationRetailerInfo(thisLocationRetailerInfoList);
				response = XstreamParserHelper.produceXMlFromObject(retailsDetails);
			}
			else {
			Boolean cityExpFlag = retailsDetails.getCityExp();
			Integer retGroupID = retailsDetails.getRetGroupID();
			Integer retAffID = retailsDetails.getRetAffID();
			String retAffName = retailsDetails.getRetAffName();
			Integer retAffCount = retailsDetails.getRetAffCount();
			String retGroupImg = retailsDetails.getRetGroupImg();
			
			String strMMId = null;
			if (null != mainMenuID) {
				strMMId = mainMenuID.toString();
			} else {
				strMMId = ApplicationConstants.STRING_ZERO;  
			}
				response = Utility.formResponseXml(
						ApplicationConstants.NORECORDSFOUND,
						ApplicationConstants.NORECORDSFOUNDTEXT, "cityExp",
						String.valueOf(cityExpFlag), "retGroupID",
						String.valueOf(retGroupID), "retAffID",
						String.valueOf(retAffID), "retAffName", retAffName,
						"retAffCount", String.valueOf(retAffCount),
						"mainMenuID", strMMId, "retGroupImg", retGroupImg);
			}
		}
		/*else
		{
			
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}*/

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The Service method for fetching Products list for the given Category and
	 * Retailer location ID. Calls the XStreams helper for parsing the request
	 * XML. Validates the request XML. if validation succeeds then it will call
	 * DAO.
	 * 
	 * @param xml
	 *            the request xml containingCategory and Retailer location ID.
	 * @return response XML as String with Product list or No Product found
	 *         message.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String getProductsInfo(String xml) throws ScanSeeException
	{
		final String methodName = "getProductsInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ProductDetailsRequest productDetailsRequest = (ProductDetailsRequest) streamHelper.parseXmlToObject(xml);

		if (null == productDetailsRequest.getRetailLocationID() || null == productDetailsRequest.getRetailID())
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			final ProductDetails productDetails = thisLocationDao.fetchProductDetails(productDetailsRequest,
					ApplicationConstants.THISLOCATIONPRODUCTSSCREEN);

			if (productDetails != null)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetails);

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * The service method for fetching the category list for the specified
	 * Retailer id. Calls the XStreams helper for parsing request xml.It
	 * validates the require parameters and if validation fails return
	 * insufficient data message.if validation succeeds then it will call DAO.
	 * 
	 * @param xml
	 *            the request xml container retailer id.
	 * @return response as String XML containing Category list or No category
	 *         found message.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String getCategoryInfo(String xml) throws ScanSeeException
	{
		final String methodName = "getCategoryInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final CategoryRequest categoryDetail = (CategoryRequest) streamHelper.parseXmlToObject(xml);

		if (null == categoryDetail.getRetailerId())

		{
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			final CategoryDetail categoryDetails = thisLocationDao.fetchCategoryDetails(categoryDetail);
			if (null != categoryDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(categoryDetails);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOCATEGORYFOUNDTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * Method for getting getting user FavCategoryInfo.
	 * 
	 * @param userID
	 *            The userID in the request.
	 * @param retailID
	 *            The retailID in the request.
	 * @return favCategoryDetails List of Favourite Categories Info.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public String getFavCategoryInfo(Integer userID, Integer retailID) throws ScanSeeException
	{
		final String methodName = "getFavCategoryInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		if (null == userID || null == retailID)
		{

			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{

			final CategoryDetail favCategoryDetails = thisLocationDao.fetchFavCategoryDetails(userID, retailID);

			if (null != favCategoryDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(favCategoryDetails);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOCATEGORYFOUNDTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * Method for getting OtherCategories.
	 * 
	 * @param userID
	 *            The userID in the request.
	 * @param retailID
	 *            The retailID in the request.
	 * @return favCategoryDetails List of Favourite Categories Info.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public String getOtherCategoriesInfo(Integer userID, Integer retailID) throws ScanSeeException
	{
		final String methodName = "getOtherCategoriesInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;

		if (null == userID || null == retailID)
		{

			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{

			final CategoryDetail otherCategoriesDetails = thisLocationDao.fetchOtherCategoriesDetails(userID, retailID);

			if (null != otherCategoriesDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(otherCategoriesDetails);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOCATEGORYFOUNDTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The Service Method for saving AdvertisementHit.Calls the XStreams helper
	 * for parsing the request XML.Validates the input request and if success it
	 * will call DAO methods else it will return insufficient data message.
	 * 
	 * @param xml
	 *            The XML input with advertisementRequest.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 * @return response XML with success or error code.
	 */

	@Override
	public String saveAdvertisementHit(String xml) throws ScanSeeException
	{
		final String methodName = "addAdvertisementHit";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AdvertisementHitReq advertisementHitReq = (AdvertisementHitReq) streamHelper.parseXmlToObject(xml);

		if (null == advertisementHitReq.getAdvertisementId() || null == advertisementHitReq.getUserId())

		{
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUEST, ApplicationConstants.INVALIDREQUEST);

		}
		else
		{
			response = thisLocationDao.saveAdvertisementHit(advertisementHitReq);

			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.ADVERTISMENTHIT);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is Service method for fetching user's Latitude, Longitude info.This
	 * method validates user id and calls DAO method.
	 * 
	 * @param userID
	 *            whose location attributes need to be fetched.
	 * @return returns response XML Containing User's Location attributes or No
	 *         records found message.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getUserLocationPoints(Integer userID) throws ScanSeeException
	{
		final String methodName = "getUserLocationPoints";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		if (null == userID)
		{
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			final ThisLocationRequest thisLocationRequestObj = thisLocationDao.fetchUserLocationDetails(userID);

			if (null != thisLocationRequestObj)
			{
				response = XstreamParserHelper.produceXMlFromObject(thisLocationRequestObj);
				response = response.replaceAll("<ThisLocationRequest>", "<UserLocationPoints>");
				response = response.replaceAll("</ThisLocationRequest>", "</UserLocationPoints>");

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.THISLOCATIONLATITUDELONGITUDE);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * THis is service method for fetching postal code from given Latitude,
	 * Longitude.
	 * 
	 * @param latitude
	 *            as request parameter containing latitude value.
	 * @param longitude
	 *            as request parameter containing longitude value
	 * @return returns response XML Containing Zipcode or No records found
	 *         message.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getUserPostalcode(Double latitude, Double longitude) throws ScanSeeException
	{
		final String methodName = "getUserLocationPoints";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		if (null == latitude || null == longitude)
		{
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			final ThisLocationRequest thisLocationRequestObj = thisLocationDao.fetchUserPostalcode(latitude, longitude);

			if (null != thisLocationRequestObj)
			{
				response = XstreamParserHelper.produceXMlFromObject(thisLocationRequestObj);
				response = response.replaceAll("<ThisLocationRequest>", "<UserLocationPoints>");
				response = response.replaceAll("</ThisLocationRequest>", "</UserLocationPoints>");

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.THISLOCATIONPOSTALCODE);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is Rest Easy Webservice for getting Latitude and Longitude for the
	 * given Zipcode.
	 * 
	 * @param zipcode
	 *            as request parameter for Zipcode.
	 * @return returns response XML Containing User's Location attributes if
	 *         exception it will return error message XML.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getLatLong(Long zipcode) throws ScanSeeException
	{
		final String methodName = "getLatLong";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		if (null == zipcode)
		{
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			final ThisLocationRequest thisLocationRequestObj = thisLocationDao.fetchLatLong(zipcode);

			if (null != thisLocationRequestObj)
			{
				final HashMap<String, String> latAndLongMap = new HashMap<String, String>();
				latAndLongMap.put("Latitude", String.valueOf(thisLocationRequestObj.getLatitude()));
				latAndLongMap.put("Longitude", String.valueOf(thisLocationRequestObj.getLongitude()));
				response = Utility.formResponseXml("response", latAndLongMap);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.LOCATEONMAPTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is Rest Easy Webservice for fetching the product details for the
	 * given userid and retailLocationID.
	 * 
	 * @param xml
	 *            -as request parameter * @return returns response XML
	 *            Containing User's Location attributes if exception it will
	 *            return error message XML.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String findNearByProducts(String xml) throws ScanSeeException
	{
		final String methodName = "scanForProdName";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Boolean gpsEnabled;
		final XstreamParserHelper parser = new XstreamParserHelper();
		final ProductDetail seacrhDetails = (ProductDetail) parser.parseXmlToObject(xml);
		if (null == seacrhDetails.getUserId() || Utility.isEmptyOrNullString(seacrhDetails.getSearchkey()))

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			if (seacrhDetails.getGpsEnabled() != null)
			{
				gpsEnabled = seacrhDetails.getGpsEnabled();

				if (gpsEnabled == false)
				{
					seacrhDetails.setScanLatitude(null);
					seacrhDetails.setScanLongitude(null);
				}
			}

			if (seacrhDetails.getLastVistedProductNo() == null)
			{
				// if this property is zero

				seacrhDetails.setLastVistedProductNo(0);
			}
			final ProductDetails productDetails = thisLocationDao.findNearByProducts(seacrhDetails);
			if (null != productDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetails);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is Rest Easy Webservice for getting radius informtion
	 * 
	 * @return returns response XML Containing radius if exception it will
	 *         return error message XML.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getRediusInfo() throws ScanSeeException
	{
		final String methodName = "getRediusInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		ArrayList<ThisLocationRequest> thislocationRediuslst;
		thislocationRediuslst = thisLocationDao.getRediusDetails();

		if (thislocationRediuslst != null && !thislocationRediuslst.isEmpty())
		{
			response = XstreamParserHelper.produceXMlFromObject(thislocationRediuslst);
			response = response.replaceAll("<list>", "<RediusInfo>");
			response = response.replaceAll("</list>", "</RediusInfo>");
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method for update user zip code information .
	 * 
	 * @param userId
	 * @param zipcode
	 * @return String with success or failure resposne.
	 * @throws ScanSeeException
	 *             The exeption defined for the application.
	 */
	@Override
	public String updateZipcode(Long userId, String zipcode) throws ScanSeeException
	{
		final String methodName = "getRediusInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		response = thisLocationDao.updateZipcode(userId, zipcode);

		if (ApplicationConstants.ZIPCODEUPDATED.equalsIgnoreCase(response))
		{
			response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.ZIPCODEUPDATED);
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.INVALIDZIPCODE);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method for check user zip code information .
	 * 
	 * @param userId
	 * @return String with success or failure resposne.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	@Override
	public String checkUsrZipcode(Long userId) throws ScanSeeException
	{
		final String methodName = "checkUsrZipcode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		response = thisLocationDao.checkUsrZipcode(userId);

		final Map<String, String> zipcodeMap = new HashMap<String, String>();
		zipcodeMap.put("repsonseCode", ApplicationConstants.SUCCESSCODE);
		zipcodeMap.put("zipcodeExists", response);

		final String finalResponse = Utility.formResponseXml("response", zipcodeMap);

		/*
		 * if (ApplicationConstants.ZIPCODEEXISTS.equalsIgnoreCase(response)) {
		 * response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE,
		 * ApplicationConstants.ZIPCODEEXISTS); } else
		 * if(ApplicationConstants.ZIPCODENOTEXISTS.equalsIgnoreCase(response))
		 * { response =
		 * Utility.formResponseXml(ApplicationConstants.SUCCESSCODE,
		 * ApplicationConstants.ZIPCODENOTEXISTS); }else{ response =
		 * Utility.formResponseXml
		 * (ApplicationConstants.TECHNICALPROBLEMERRORCODE,
		 * ApplicationConstants.TECHNICALPROBLEMERRORTEXT); }
		 */
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return finalResponse;
	}

	@Override
	public String saveUsrRetNotification(String xml) throws ScanSeeException
	{
		final String methodName = "saveUsrRetNotification";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final RetailerDetail retailerDetailObj = (RetailerDetail) streamHelper.parseXmlToObject(xml);

		if (null == retailerDetailObj.getRetailerId() || null == retailerDetailObj.getUserId() || null == retailerDetailObj.getRetailLocationID())

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);

		}
		else
		{
			response = thisLocationDao.saveUsrRetNotification(retailerDetailObj);

			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.USERRETAILERSALENOTIFICATION);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method for check user zip code information .
	 * 
	 * @param userId
	 * @return String with success or failure resposne.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	@Override
	public String fetchRetailerStoreInfo(String xml) throws ScanSeeException
	{
		final String methodName = "fetchRetailerStoreInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final RetailerDetail retailerDetailObj = (RetailerDetail) streamHelper.parseXmlToObject(xml);
		List<RetailerDetail> retailStorelst = null;
		if (null == retailerDetailObj.getRetailerId() || null == retailerDetailObj.getUserId() || null == retailerDetailObj.getRetailLocationID())

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);

		}
		else
		{
			retailStorelst = thisLocationDao.fetchRetailerStoreDetails(retailerDetailObj);

			if (retailStorelst != null && !retailStorelst.isEmpty())
			{
				response = XstreamParserHelper.produceXMlFromObject(retailStorelst);
				response = response.replaceAll("<list>", "<RetailerStoreInfo>");
				response = response.replaceAll("</list>", "</RetailerStoreInfo>");
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This Rest Easy method for fetching retailer summary information.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or if
	 *         exception it will return error message XML.
	 */
	@Override
	public String getRetailerSummary(String xml) throws ScanSeeException
	{
		final String methodName = "getRetailerSummary";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final RetailerDetail retailerDetailObj = (RetailerDetail) streamHelper.parseXmlToObject(xml);
		List<RetailerDetail> retailSummarylst = null;
		List<RetailerCreatedPages> retailCreatedPageslst = null;
		RetailersDetails retailerSummaryObj = null;
		if (null == retailerDetailObj.getRetailerId() || null == retailerDetailObj.getUserId() || null == retailerDetailObj.getRetailLocationID())

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);

		}
		else
		{
			retailSummarylst = thisLocationDao.getRetailerSummary(retailerDetailObj);

			if (retailSummarylst != null && !retailSummarylst.isEmpty())
			{
				retailerSummaryObj = new RetailersDetails();
				retailerSummaryObj.setRetailerDetail(retailSummarylst);
				retailCreatedPageslst = thisLocationDao.getRetCreatePages(retailerDetailObj);

				if (retailCreatedPageslst != null && !retailCreatedPageslst.isEmpty())
				{
					retailerSummaryObj.setRetailerCreatedPagesList(retailCreatedPageslst);

				}

			}
			if (retailerSummaryObj != null)
			{

				response = XstreamParserHelper.produceXMlFromObject(retailerSummaryObj);
				/*
				 * response = response.replaceAll("<list>",
				 * "<RetailerSummary>"); response =
				 * response.replaceAll("</list>", "</RetailerSummary>");
				 */
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String getRetailerSpecialOffDetails(String xml) throws ScanSeeException
	{
		final String methodName = "getRetailerSpecialOffDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		List<RetailerDetail> specialOfferlst = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final RetailerDetail specialOfferRequest = (RetailerDetail) streamHelper.parseXmlToObject(xml);
		if (null == specialOfferRequest.getRetailerId() || null == specialOfferRequest.getUserId()
				|| null == specialOfferRequest.getRetailLocationID())

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);

		}
		else
		{
			specialOfferlst = thisLocationDao.fetchSpecialOffers(specialOfferRequest);

			if (specialOfferlst != null && !specialOfferlst.isEmpty())
			{
				response = XstreamParserHelper.produceXMlFromObject(specialOfferlst);
				response = response.replaceAll("<list>", "<RetailerSpeOfflist>");
				response = response.replaceAll("</list>", "</RetailerSpeOfflist>");
			}

			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String getRetailerCLRDetails(String xml) throws ScanSeeException
	{
		final String methodName = "getRetailerCLRDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		CLRDetails clrDetailsObj = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final RetailerDetail specialOfferRequest = (RetailerDetail) streamHelper.parseXmlToObject(xml);
		if (null == specialOfferRequest.getRetailerId() || null == specialOfferRequest.getUserId()
				|| null == specialOfferRequest.getRetailLocationID())

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);

		}
		else
		{
			clrDetailsObj = thisLocationDao.getRetailerCLRDetails(specialOfferRequest);

			if (clrDetailsObj != null)
			{
				response = XstreamParserHelper.produceXMlFromObject(clrDetailsObj);
			}

			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This Service Layer method for fetching retailer location hot deal
	 * information.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing hot deals or if
	 *         exception it will return error message XML.
	 */
	@Override
	public String getRetailerHotDeals(String xml) throws ScanSeeException
	{
		final String methodName = "getRetailerHotDeals";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final RetailerDetail retailerDetailObj = (RetailerDetail) streamHelper.parseXmlToObject(xml);
		List<HotDealsDetails> hotDealsDetailsList = null;

		if (null == retailerDetailObj.getRetailerId() || null == retailerDetailObj.getUserId() || null == retailerDetailObj.getRetailLocationID())
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			hotDealsDetailsList = thisLocationDao.getRetailerHotDeals(retailerDetailObj);

			if (hotDealsDetailsList != null && !hotDealsDetailsList.isEmpty())
			{
				response = XstreamParserHelper.produceXMlFromObject(hotDealsDetailsList);
				response = response.replaceAll("<list>", "<RetailerHotDealslist>");
				response = response.replaceAll("</list>", "</RetailerHotDealslist>");
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String getRetailerSpeHotDealsCLRDetails(String xml) throws ScanSeeException
	{
		final String methodName = "getRetailerSpeHotDealsCLRDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ProductDetailsRequest productDetailsRequest = (ProductDetailsRequest) streamHelper.parseXmlToObject(xml);

		if (null == productDetailsRequest.getRetailLocationID() || null == productDetailsRequest.getRetailID())
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{

			/**
			 * retailer special offers list.
			 */
			ProductDetails productDetailsObj = null;
			productDetailsObj = thisLocationDao.fetchSpecialOffersHotDealDiscounts(productDetailsRequest);
			if (productDetailsObj != null)
			{

				response = XstreamParserHelper.produceXMlFromObject(productDetailsObj);

				response = response.replaceAll("<RetailerDetail>", "<SpecialOffer>");
				response = response.replaceAll("</RetailerDetail>", "</SpecialOffer>");
				response = response.replaceAll("<ProductDetails>", "<SpecialOffersList>");
				response = response.replaceAll("</ProductDetails>", "</SpecialOffersList>");

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	@Override
	public String fetchSpecialDealsDetails(String xml) throws ScanSeeException
	{
		final String methodName = "getRetailerSpecialOffDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		List<RetailerDetail> specialOfferlst = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final RetailerDetail specialOfferRequest = (RetailerDetail) streamHelper.parseXmlToObject(xml);
		if (null == specialOfferRequest.getRetailerId() || null == specialOfferRequest.getPageID()
				|| null == specialOfferRequest.getRetailLocationID())

		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);

		}
		else
		{
			specialOfferlst = thisLocationDao.fetchSpecialDealsDetails(specialOfferRequest);

			if (specialOfferlst != null && !specialOfferlst.isEmpty())
			{
				response = XstreamParserHelper.produceXMlFromObject(specialOfferlst);
				response = response.replaceAll("<list>", "<SpecialDealDetails>");
				response = response.replaceAll("</list>", "</SpecialDealDetails>");
			}

			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method for searching retailers by Group ID. Calls the XStreams helper to
	 * parse the given request XML and validates the required fields.
	 * 
	 * @param xml
	 *            - the input request contains search parameters like city name, zipcode or
	 *            location attributes.
	 * @return response XML as a String containing Retailers list or No
	 *         retailers found message.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getRetailersByGroupID(String xml) throws ScanSeeException {
		final String methodName = "getRetailersByGroupID";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Boolean gpsEnabled;
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		String completeAddress;
		final ThisLocationRequest thisLocationRequest = (ThisLocationRequest) streamHelper.parseXmlToObject(xml);
		if (thisLocationRequest.getUserId() == null || thisLocationRequest.getRetGroupID() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
			return response;
		}

		if(thisLocationRequest.isGpsEnabled() != null)	{
			gpsEnabled = thisLocationRequest.isGpsEnabled();
		} else	{
			gpsEnabled = false;
		}

		if (!gpsEnabled)
		{
			thisLocationRequest.setLatitude(null);
			thisLocationRequest.setLongitude(null);
		}
		if (null == thisLocationRequest.getLastVisitedRecord())
		{
			// if this property is zero
			thisLocationRequest.setLastVisitedRecord(0);
		}
		
		//For user tracking
		Integer mainMenuID = null;
		if (thisLocationRequest.getMainMenuID() == null) {
			UserTrackingData objUserTrackingData = new UserTrackingData();
			objUserTrackingData.setUserID(thisLocationRequest.getUserId());
			objUserTrackingData.setModuleID(thisLocationRequest.getModuleID());
			objUserTrackingData.setLatitude(thisLocationRequest.getLatitude());
			objUserTrackingData.setLongitude(thisLocationRequest.getLongitude());
			objUserTrackingData.setPostalCode(thisLocationRequest.getZipcode());
			mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);
			thisLocationRequest.setMainMenuID(mainMenuID);
		}
		else 	{
			mainMenuID = thisLocationRequest.getMainMenuID();
		}
		
		final RetailersDetails retailsDetails = thisLocationDao.getRetailersByGroupID(thisLocationRequest, ApplicationConstants.THISLOCATIONRETAILERSCREEN);
		
//		String retGroupImg = retailsDetails.getRetGroupImg();
//		retailsDetails.setRetGroupImg(null);
		
//		retailsDetails.setMainMenuID(mainMenuID);

		List<ThisLocationRetailerInfo> thisLocationRetailerInfoList = null;

		if (null != retailsDetails)
		{
			final List<RetailerDetail> retailerDetail = retailsDetails.getRetailerDetail();
			if (!retailerDetail.isEmpty())
			{
				thisLocationRetailerInfoList = new ArrayList<ThisLocationRetailerInfo>();
				for (int i = 0; i < retailerDetail.size(); i++)
				{
					final ThisLocationRetailerInfo thisLocationRetailerInfo = new ThisLocationRetailerInfo();
					completeAddress = new String();
					if (null != retailerDetail.get(i).getRetaileraddress1()
							&& !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getRetaileraddress1())
							&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getRetaileraddress1()))
					{
						completeAddress = retailerDetail.get(i).getRetaileraddress1() + ApplicationConstants.COMMA;
					}
					if (null != retailerDetail.get(i).getCity() && !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getCity())
							&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getCity()))
					{
						completeAddress = completeAddress + retailerDetail.get(i).getCity() + ApplicationConstants.COMMA;
					}
					if (null != retailerDetail.get(i).getPostalCode()
							&& !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getPostalCode())
							&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getPostalCode()))
					{
						completeAddress = completeAddress + retailerDetail.get(i).getPostalCode() + ApplicationConstants.COMMA;
					}
					if (null != retailerDetail.get(i).getState() && !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getState())
							&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getState()))
					{
						completeAddress = completeAddress + retailerDetail.get(i).getState();
					}

					thisLocationRetailerInfo.setRowNumber(retailerDetail.get(i).getRowNumber());
					thisLocationRetailerInfo.setRetailerId(retailerDetail.get(i).getRetailerId());
					thisLocationRetailerInfo.setRetailerName(retailerDetail.get(i).getRetailerName());
					thisLocationRetailerInfo.setRetailLocationID(retailerDetail.get(i).getRetailLocationID());
					thisLocationRetailerInfo.setRetailAddress(completeAddress);
					thisLocationRetailerInfo.setDistance(retailerDetail.get(i).getDistance());
					thisLocationRetailerInfo.setLogoImagePath(retailerDetail.get(i).getLogoImagePath());
					thisLocationRetailerInfo.setBannerAdImagePath(retailerDetail.get(i).getBannerAdImagePath());
					thisLocationRetailerInfo.setRibbonAdImagePath(retailerDetail.get(i).getRibbonAdImagePath());
					thisLocationRetailerInfo.setRibbonAdURL(retailerDetail.get(i).getRibbonAdURL());
					thisLocationRetailerInfo.setSaleFlag(retailerDetail.get(i).getSaleFlag());
					thisLocationRetailerInfo.setSplashAdID(retailerDetail.get(i).getSplashAdID());
					thisLocationRetailerInfo.setRetListID(retailerDetail.get(i).getRetListID());
					
					thisLocationRetailerInfo.setLatitude(retailerDetail.get(i).getRetLatitude());
					thisLocationRetailerInfo.setLongitude(retailerDetail.get(i).getRetLongitude());
					thisLocationRetailerInfo.setRetGroupImg(retailerDetail.get(i).getRetGroupImg());

					thisLocationRetailerInfoList.add(thisLocationRetailerInfo);
				}

			}
//			retailsDetails.setRetailerDetail(null);
//			retailsDetails.setThisLocationRetailerInfo(thisLocationRetailerInfoList);
//			response = XstreamParserHelper.produceXMlFromObject(retailsDetails);
			if(thisLocationRetailerInfoList !=null && !thisLocationRetailerInfoList.isEmpty())
			{
				retailsDetails.setMainMenuID(mainMenuID);
				retailsDetails.setRetailerDetail(null);
				retailsDetails.setThisLocationRetailerInfo(thisLocationRetailerInfoList);
				response = XstreamParserHelper.produceXMlFromObject(retailsDetails);
			}
			else {
				Integer retAffID = retailsDetails.getRetAffID();
				String retAffName = retailsDetails.getRetAffName();
				Integer retAffCount = retailsDetails.getRetAffCount();
				String retGroupImg = retailsDetails.getRetGroupImg();
				String mmID = null;
				if(mainMenuID != null){
					mmID = mainMenuID.toString();
				} else{
					mmID = ApplicationConstants.STRING_ZERO;
				}
				if(retAffName != null)	{
				response = Utility.formResponseXml(
						ApplicationConstants.NORECORDSFOUND,
						ApplicationConstants.NORECORDSFOUNDTEXT, "retAffID",
						String.valueOf(retAffID), "retAffName", retAffName, "retAffCount", String.valueOf(retAffCount), "mainMenuID", mmID, "retGroupImg", retGroupImg);
				}
				else	{
					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND,	ApplicationConstants.NORECORDSFOUNDTEXT, "mainMenuID", mmID, "retGroupImg", retGroupImg);
				}
			}
		}
		else
		{
			String strMMId = null;
			if(mainMenuID != null){
				strMMId = mainMenuID.toString();
			} else{
				strMMId = ApplicationConstants.STRING_ZERO;
			}
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND,	ApplicationConstants.NORECORDSFOUNDTEXT, "mainMenuID", strMMId);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method for searching retailers for partners. Calls the XStreams helper to
	 * parse the given request XML and validates the required fields.
	 * 
	 * @param xml
	 *            - the input request contains search parameters like city name, zipcode or
	 *            location attributes.
	 * @return response XML as a String containing Retailers list or No
	 *         retailers found message.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getRetailersForPartner(String xml) throws ScanSeeException {
		final String methodName = "getRetailersForPartner";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Boolean gpsEnabled;
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		String completeAddress;
		final ThisLocationRequest thisLocationRequest = (ThisLocationRequest) streamHelper.parseXmlToObject(xml);
		if (thisLocationRequest.getUserId() == null || thisLocationRequest.getRetAffID() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
			return response;
		}

		if(thisLocationRequest.isGpsEnabled() != null)	{
			gpsEnabled = thisLocationRequest.isGpsEnabled();
		} else	{
			gpsEnabled = false;
		}

		if (!gpsEnabled)
		{
			thisLocationRequest.setLatitude(null);
			thisLocationRequest.setLongitude(null);
		}
		if (null == thisLocationRequest.getLastVisitedRecord())
		{
			// if this property is zero
			thisLocationRequest.setLastVisitedRecord(0);
		}
		
		//For user tracking
		Integer mainMenuID = null;
		if (thisLocationRequest.getMainMenuID() == null) {
			UserTrackingData objUserTrackingData = new UserTrackingData();
			objUserTrackingData.setUserID(thisLocationRequest.getUserId());
			objUserTrackingData.setModuleID(thisLocationRequest.getModuleID());
			objUserTrackingData.setLatitude(thisLocationRequest.getLatitude());
			objUserTrackingData.setLongitude(thisLocationRequest.getLongitude());
			objUserTrackingData.setPostalCode(thisLocationRequest.getZipcode());
			mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);
			thisLocationRequest.setMainMenuID(mainMenuID);
		}
		else 	{
			mainMenuID = thisLocationRequest.getMainMenuID();
		}
		
		final RetailersDetails retailsDetails = thisLocationDao.getRetailersForPartner(thisLocationRequest, ApplicationConstants.THISLOCATIONRETAILERSCREEN);

//		retailsDetails.setMainMenuID(mainMenuID);
		List<ThisLocationRetailerInfo> thisLocationRetailerInfoList = null;

		if (null != retailsDetails)
		{
			final List<RetailerDetail> retailerDetail = retailsDetails.getRetailerDetail();
			if (!retailerDetail.isEmpty())
			{
				thisLocationRetailerInfoList = new ArrayList<ThisLocationRetailerInfo>();
				for (int i = 0; i < retailerDetail.size(); i++)
				{
					final ThisLocationRetailerInfo thisLocationRetailerInfo = new ThisLocationRetailerInfo();
					completeAddress = new String();
					if (null != retailerDetail.get(i).getRetaileraddress1()
							&& !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getRetaileraddress1())
							&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getRetaileraddress1()))
					{
						completeAddress = retailerDetail.get(i).getRetaileraddress1() + ApplicationConstants.COMMA;
					}
					if (null != retailerDetail.get(i).getCity() && !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getCity())
							&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getCity()))
					{
						completeAddress = completeAddress + retailerDetail.get(i).getCity() + ApplicationConstants.COMMA;
					}
					if (null != retailerDetail.get(i).getPostalCode()
							&& !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getPostalCode())
							&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getPostalCode()))
					{
						completeAddress = completeAddress + retailerDetail.get(i).getPostalCode() + ApplicationConstants.COMMA;
					}
					if (null != retailerDetail.get(i).getState() && !ApplicationConstants.NOTAPPLICABLE.equals(retailerDetail.get(i).getState())
							&& !ApplicationConstants.EMPTYSTR.equals(retailerDetail.get(i).getState()))
					{
						completeAddress = completeAddress + retailerDetail.get(i).getState();
					}

					thisLocationRetailerInfo.setRowNumber(retailerDetail.get(i).getRowNumber());
					thisLocationRetailerInfo.setRetailerId(retailerDetail.get(i).getRetailerId());
					thisLocationRetailerInfo.setRetailerName(retailerDetail.get(i).getRetailerName());
					thisLocationRetailerInfo.setRetailLocationID(retailerDetail.get(i).getRetailLocationID());
					thisLocationRetailerInfo.setRetailAddress(completeAddress);
					thisLocationRetailerInfo.setDistance(retailerDetail.get(i).getDistance());
					thisLocationRetailerInfo.setLogoImagePath(retailerDetail.get(i).getLogoImagePath());
					thisLocationRetailerInfo.setBannerAdImagePath(retailerDetail.get(i).getBannerAdImagePath());
					thisLocationRetailerInfo.setRibbonAdImagePath(retailerDetail.get(i).getRibbonAdImagePath());
					thisLocationRetailerInfo.setRibbonAdURL(retailerDetail.get(i).getRibbonAdURL());
					thisLocationRetailerInfo.setSaleFlag(retailerDetail.get(i).getSaleFlag());
					thisLocationRetailerInfo.setSplashAdID(retailerDetail.get(i).getSplashAdID());
					thisLocationRetailerInfo.setRetListID(retailerDetail.get(i).getRetListID());
					
					thisLocationRetailerInfo.setLatitude(retailerDetail.get(i).getRetLatitude());
					thisLocationRetailerInfo.setLongitude(retailerDetail.get(i).getRetLongitude());
					thisLocationRetailerInfo.setRetGroupImg(retailerDetail.get(i).getRetGroupImg());

					thisLocationRetailerInfoList.add(thisLocationRetailerInfo);
				}

			}
			retailsDetails.setMainMenuID(mainMenuID);
			retailsDetails.setRetailerDetail(null);
			retailsDetails.setThisLocationRetailerInfo(thisLocationRetailerInfoList);
			response = XstreamParserHelper.produceXMlFromObject(retailsDetails);
		}
		else
		{
			String strMMId = null;
			if(mainMenuID != null){
				strMMId = mainMenuID.toString();
			} else{
				strMMId = ApplicationConstants.STRING_ZERO;
			}
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND,	ApplicationConstants.NORECORDSFOUNDTEXT, "mainMenuID", strMMId);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method to get Partners. Calls the XStreams helper to
	 * parse the given request XML and validates the required fields.
	 * 
	 * @param No input parameters
	 * 
	 * @return response XML as a String containing partners list
	 * 
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getPartners(Integer retGroupID) throws ScanSeeException {
		final String methodName = "getPartners";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		ThisLocationRequest partnerList;
	
		partnerList = thisLocationDao.getPartners(retGroupID);

		if (partnerList != null)	{
			response = XstreamParserHelper.produceXMlFromObject(partnerList);
		}
		else	{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method to get categories for partner retailers. It also validates the required fields.
	 * 
	 * @param retAffID
	 * 
	 * @return response XML as a string containing categories
	 * 
	 * @throws ScanSeeException
	 */
	@Override
	public String getCategoriesForPartners(Integer retAffID) throws ScanSeeException {
		final String methodName = "getCategoriesForPartners";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		List<CategoryInfo> categoryInfoList = null;
		
		if (retAffID == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);

		}
		else
		{
			categoryInfoList = thisLocationDao.getCategoriesForPartners(retAffID);

			if (categoryInfoList != null && !categoryInfoList.isEmpty())
			{
				response = XstreamParserHelper.produceXMlFromObject(categoryInfoList);
				response = response.replaceAll("<list>", "<Category>");
				response = response.replaceAll("</list>", "</Category>");
			}

			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOCATEGORYFOUNDTEXT);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method to get categories for group retailers. It also validates the required fields.
	 * 
	 * @param retGroupID
	 * 
	 * @return response XML as a string containing categories
	 * 
	 * @throws ScanSeeException
	 */
	@Override
	public String getCategoriesForGroupRetailers(Integer retGroupID) throws ScanSeeException {
		final String methodName = "getCategoriesForGroupRetailers";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		List<CategoryInfo> categoryInfoList = null;
		if (retGroupID == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			categoryInfoList= thisLocationDao.getCategoriesForGroupRetailers(retGroupID);

			if (categoryInfoList != null && !categoryInfoList.isEmpty())
			{
				response = XstreamParserHelper.produceXMlFromObject(categoryInfoList);
				response = response.replaceAll("<list>", "<Category>");
				response = response.replaceAll("</list>", "</Category>");
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOCATEGORYFOUNDTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	

	/**
	 * For user tracking retailer summary clicks. Called whenever user taps item in the Retailer summary screen.
	 * Calls the XStreams helper to parse the given request XML and validates the required fields
	 * Method Type: POST
	 * 
	 * @param xml
	 * @return xml containing SUCCESS or FAILURE response
	 * @throws ScanSeeException
	 */
	@Override
	public String userTrackingRetailerSummaryClick(String xml) throws ScanSeeException {
		final String methodName = "userTrackingRetailerSummaryClick";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserTrackingData userTrackingData = (UserTrackingData) streamHelper.parseXmlToObject(xml);
		if (userTrackingData.getRetListID() == null || userTrackingData.getRetDetailsID() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			response = thisLocationDao.userTrackingRetailerSummaryClick(userTrackingData);
			
			if(response == ApplicationConstants.SUCCESSRESPONSETEXT )	{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SUCCESSRESPONSETEXT);
			}
			else	{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.FAILURE);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * For user tracking. Called when the user taps on any special offers.
	 * Calls the XStreams helper to parse the given request XML and validates the required fields.
	 * 
	 * @param xml
	 * @return
	 * @throws ScanSeeException
	 */
	@Override
	public String userTrackingRetSpeOffersClick(String xml) throws ScanSeeException {
		final String methodName = "userTrackingRetailerSummaryClick";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserTrackingData userTrackingData = (UserTrackingData) streamHelper.parseXmlToObject(xml);
		if (userTrackingData.getUserID() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			response = thisLocationDao.userTrackingRetSpeOffersClick(userTrackingData);
			
			if(response == ApplicationConstants.SUCCESSRESPONSETEXT)	{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SUCCESSRESPONSETEXT);
			}
			else	{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.FAILURE);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}
