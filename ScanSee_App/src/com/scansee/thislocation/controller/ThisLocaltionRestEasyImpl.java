package com.scansee.thislocation.controller;

import static com.scansee.common.util.Utility.formResponseXml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.servicefactory.ServiceFactory;
import com.scansee.thislocation.service.ThisLocationService;

/**
 * The class has methods to accept ThisLocation module requests.These methods
 * are called on the Client request. The method is selected based on the URL
 * Path and the type of request(GET,POST). It invokes the appropriate method in
 * Service layer.
 * 
 * @author shyamsundara_hm
 */

public class ThisLocaltionRestEasyImpl implements ThisLocaltionRestEasy
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ThisLocaltionRestEasyImpl.class);

	/**
	 * This is RestEasy webservice method for searching the retailer for the
	 * given zip code or location attributes(Latitude or longitude).Method Type:
	 * POST.
	 * 
	 * @param xml
	 *            the input request contains search parameters like zipcode or
	 *            location attributes.
	 * @return response XML as String containing Retailers list.
	 */

	public String getRetailersInfoForLocation(String xml)
	{
		final String methodName = "getRetailersInfoForLocation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug(" Retailer Search Request :" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getRetailersInfoForLocation(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return responseXML;

	}

	/**
	 * This is Rest Easy webservice method for fetching Categories for specified
	 * retailers in the request. Method Type:POST.
	 * 
	 * @param xml
	 *            request XML contains Retailer ID.
	 * @return returns response XML as String containing Category list or if
	 *         exception it will return error message XML.
	 */

	public String getCategoryInfo(String xml)
	{
		final String methodName = "getCategoryInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request xml Category:" + xml);
		}

		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getCategoryInfo(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to client-->:" + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return responseXML;

	}

	/**
	 * This is Rest Easy webservice method for fetching Products information for
	 * the specified Category and Retailer location ID. Method: POST
	 * 
	 * @param xml
	 *            input request contains Category and Retailer location ID.
	 * @return returns response XML As String Containing Product List or if
	 *         exception it will return error message XML.
	 */

	public String getProductsInfo(String xml)
	{

		final String methodName = "getProductsInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Xml:" + xml);
		}

		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getProductsInfo(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to client:" + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXML;

	}

	/**
	 * Method for fetching userFavourite Categories.
	 * 
	 * @param userID
	 *            The userID in request.
	 * @param retailID
	 *            The retailID in request.
	 * @return responseXML The XML with Other Category Details.
	 */

	@Override
	public String getFavCategoryInfo(Integer userID, Integer retailID)
	{

		final String methodName = "getFavCategoryInfo in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved  UserID:" + userID + "  retailID:" + retailID);
		}

		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getFavCategoryInfo(userID, retailID);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned response to Client is :" + responseXML);
		}

		return responseXML;

	}

	/**
	 * Method for fetching Other Categories.
	 * 
	 * @param userID
	 *            The userID in request.
	 * @param retailID
	 *            The retailID in request.
	 * @return responseXML The XML with Other Category Details.
	 */

	@Override
	public String getOtherCategoriesInfo(Integer userID, Integer retailID)
	{

		final String methodName = "getOtherCategoriesInfo in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Data userID:" + userID + "  retailID:" + retailID);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getOtherCategoriesInfo(userID, retailID);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response:" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This Rest Easy method for recording User Advertisement Hit.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            advertisement info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */

	@Override
	public String userAdvertisementHit(String xml)
	{
		final String methodName = "userAdvertisementHit in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved request " + xml);
		}

		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();

		try
		{
			responseXML = thisLocationService.saveAdvertisementHit(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response:" + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXML;
	}

	/**
	 * This Rest Easy method for fetching user's Latitude, Longitude info.
	 * 
	 * @param userID
	 *            whose location attributes need to be fetched.
	 * @return returns response XML Containing User's Location attributes if
	 *         exception it will return error message XML.
	 */
	@Override
	public String getUserLocationPoints(Integer userID)
	{
		final String methodName = "getUserLocationPoints";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Data userID:" + userID);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getUserLocationPoints(userID);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client is :" + responseXML);
		}
		return responseXML;
	}

	/**
	 * THis is RestEasy web service method for fetching postal code from given
	 * Latitude, Longitude.
	 * 
	 * @param latitude
	 *            as request parameter containing latitude value.
	 * @param longitude
	 *            as request parameter containing longitude value
	 * @return returns response XML Containing User's Location attributes if
	 *         exception it will return error message XML.
	 */
	@Override
	public String getUserPostalcode(Double latitude, Double longitude)
	{
		final String methodName = "getUserPostalcode in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Data latitude:" + latitude + "longitude" + longitude);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getUserPostalcode(latitude, longitude);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned response to Client  is:" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is Rest Easy Webservice for getting Latitude and Longitude for the
	 * given Zipcode.
	 * 
	 * @param zipcode
	 *            as request parameter for Zipcode.
	 * @param userId
	 *            as request parameter for USerid
	 * @return returns response XML Containing User's Location attributes if
	 *         exception it will return error message XML.
	 */
	@Override
	public String getLatLong(Long zipcode, Long userId)
	{
		final String methodName = "getLatLong";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Recieved Data zipcode:" + zipcode);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getLatLong(zipcode);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client  :" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is Rest Easy Webservice for finding near by products for the given
	 * retailLocationID and userid.
	 * 
	 * @param xml
	 *            As request parameter.
	 * @return returns response XML Containing User's Location attributes if
	 *         exception it will return error message XML.
	 */
	@Override
	public String findNearByProducts(String xml)
	{

		final String methodName = "findNearByProducts";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request Recieved:" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.findNearByProducts(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client  :" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is Rest Easy Web service for displaying radius information.
	 * 
	 * @return returns response XML Containing radius information if exception
	 *         it will return error message XML.
	 */
	@Override
	public String getRadius()
	{
		final String methodName = "getRadius";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("No Query Parameters");
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getRediusInfo();
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client  :" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This method for update user zip code information .
	 * 
	 * @param userId
	 * @param zipcode
	 * @return String with success or failure resposne.
	 * @throws ScanSeeException
	 *             The exeption defined for the application.
	 */
	@Override
	public String updateZipcode(Long userId, String zipcode)
	{
		final String methodName = "updateZipcode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("No Query Parameters");
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.updateZipcode(userId, zipcode);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client  :" + responseXML);
		}
		return responseXML;
	}

	@Override
	public String checkUsrZipcode(Long userId)
	{
		final String methodName = "checkUsrZipcode";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("No Query Parameters");
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.checkUsrZipcode(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client  :" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This Rest Easy method for recording User retailer sale notification.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	@Override
	public String saveUsrRetSaleNotification(String xml)
	{
		final String methodName = "saveUserRetailerSaleNotification";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request Recieved:" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.saveUsrRetNotification(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client  :" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This Rest Easy method for fetching retailer store details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */

	@Override
	public String fetchRetailerStoreDetails(String xml)
	{
		final String methodName = "fetchRetailerStoreDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request Recieved:" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.fetchRetailerStoreInfo(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client  :" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This Rest Easy method for fetching retailer summary details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or
	 *         exception it will return error message XML.
	 */
	@Override
	public String getRetailerSummary(String xml)
	{
		final String methodName = "getRetailerSummary";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request Recieved:" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getRetailerSummary(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client  :" + responseXML);
		}
		return responseXML;
	}

	@Override
	public String getRetailerSpecialOffDetails(String xml)
	{
		final String methodName = "getRetailerSpecialOffDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request Recieved:" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getRetailerSpecialOffDetails(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client  :" + responseXML);
		}
		return responseXML;
	}

	@Override
	public String getRetailerCLRDetails(String xml)
	{
		final String methodName = "getRetailerCLRDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request Recieved:" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getRetailerCLRDetails(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client  :" + responseXML);
		}
		return responseXML;
	}
	/**
	 * This Rest Easy method for fetching retailer location hot deals.
	 * 
	 * Type: POST
	 * @param xml
	 *            as the request XML containing userId, retaileID, retailerLocationID info
	 *            and lowerLimit
	 * @return returns response XML As String Containing hot deal list or
	 *         exception it will return error message XML.
	 */
	@Override
	public String getRetailerHotDeals(String xml)
	{
		final String methodName = "getRetailerHotDeals";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request Recieved:" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getRetailerHotDeals(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client  :" + responseXML);
		}
		return responseXML;
	}
/**
 * This service is used to get retailer special offer , hotdeals and coupon,loyalty and rebate information.
 * 
 */
	@Override
	public String getRetailerSpeHotDealsCLRDetails(String xml)
	{
		final String methodName = "getRetailerSpeHotDealsCLRDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request Recieved:" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getRetailerSpeHotDealsCLRDetails(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client  :" + responseXML);
		}
		return responseXML;
	}
	@Override
	public String fetchSpecialOffDetails(String xml) {
		final String methodName = "fetchSpecialOffDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request Recieved:" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.fetchSpecialDealsDetails(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response to Client  :" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is RestEasy webservice method for searching retailer for the given city.
	 * Method Type: POST.
	 * 
	 * @param xml
	 *            the input request contains search parameters like zipcode or
	 *            location attributes, retailerGroupID.
	 * @return response XML as String containing Retailers list.
	 */
	@Override
	public String getRetailersByGroupID(String xml) {
		final String methodName = "getRetailersByGroupID";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug(" Retailer Search Request :" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getRetailersByGroupID(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return responseXML;
	}

	/**
	 * This is RestEasy webservice method for searching the retailer by go local affiliate.
	 * Method Type: POST.
	 * 
	 * @param xml
	 *            the input request contains search parameters like city name, zipcode or
	 *            location attributes, retailerAffiliateID.
	 * @return response XML as String containing Retailers list.
	 */
	@Override
	public String getRetailersForPartner(String xml) {
		final String methodName = "getRetailersForPartner";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug(" Retailer Search Request :" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getRetailersForPartner(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return responseXML;
	}

	/**
	 * This is restEasy webservice method to get partners for Austin Experience
	 * Method Type: GET
	 * 
	 * @return response XML as String containing partner list
	 */
	@Override
	public String getPartners(Integer retGroupID) {
		final String methodName = "getPartners";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug(" Partner Search Request : No request param");
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getPartners(retGroupID);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return responseXML;
	}
	
	/**
	 * This is restEasy webservice method to get categories for Specified group retailers.
	 * Method Type: GET
	 * 
	 * @param xml
	 * 
	 * @return response XML as String containing partner list
	 */
	@Override
	public String getCategoriesForGroupRetailers(Integer retGroupID) {
		final String methodName = "getCategoriesForGroupRetailers";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug(" Received request RetailerGroupID :" + retGroupID);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getCategoriesForGroupRetailers(retGroupID);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return responseXML;
	}

	/**
	 * This is restEasy webservice method to get categories for Specified partner retailers.
	 * Method Type: GET
	 * 
	 * @param xml
	 * 
	 * @return response XML as String containing partner list
	 */
	@Override
	public String getCategoriesForPartners(Integer retAffID) {
		final String methodName = "getCategoriesForPartners";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug(" Received request RetailerAffiliateID :" + retAffID);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.getCategoriesForPartners(retAffID);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return responseXML;
	}		


	/**
	 * This is RestEasy webservice method for user tracking retailer summary clicks.
	 * To be called whenever user taps on any of the item in the Retailer summary screen.
	 * 
	 * @param xml
	 * @return xml response SUCCESS or FAILURE
	 */
	@Override
	public void userTrackingRetailerSummaryClick(String xml) {
		final String methodName = "userTrackingRetailerSummaryClick";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug(" Retailer Search Request :" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.userTrackingRetailerSummaryClick(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response :" + responseXML);
		}

	}

	/**
	 * This is RestEasy webservice method for user tracking.
	 * To be called when the user taps on any special offers.
	 * Method Type: POST
	 * 
	 * @param xml
	 * 
	 * @return 
	 */
	@Override
	public void userTrackingRetSpeOffersClick(String xml) {
		final String methodName = "userTrackingRetSpeOffersClick";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug(" Retailer Search Request :" + xml);
		}
		String responseXML = null;
		final ThisLocationService thisLocationService = ServiceFactory.getThisLocationService();
		try
		{
			responseXML = thisLocationService.userTrackingRetSpeOffersClick(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response  client:" + responseXML);
		}

	}

}
