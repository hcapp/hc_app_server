package com.scansee.thislocation.controller;

import static com.scansee.common.constants.ScanSeeURLPath.CHECKUSRZIPCODE;
import static com.scansee.common.constants.ScanSeeURLPath.FETCHLATLONG;
import static com.scansee.common.constants.ScanSeeURLPath.FETCHUSERLOCATIONPOINTS;
import static com.scansee.common.constants.ScanSeeURLPath.FETCHUSERPOSTALCODE;
import static com.scansee.common.constants.ScanSeeURLPath.FINDNEARBYPRODUCTS;
import static com.scansee.common.constants.ScanSeeURLPath.GETCATEGORYINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETFAVCATERGORIES;
import static com.scansee.common.constants.ScanSeeURLPath.GETOTHERCATERGORIES;
import static com.scansee.common.constants.ScanSeeURLPath.GETPARTNERS;
import static com.scansee.common.constants.ScanSeeURLPath.GETPRODUCTS;
import static com.scansee.common.constants.ScanSeeURLPath.GETRADIUSINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETRETAILERSINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETRETBYGROUPID;
import static com.scansee.common.constants.ScanSeeURLPath.GETRETCLRINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETRETHOTDEALS;
import static com.scansee.common.constants.ScanSeeURLPath.GETRETSPEOFFINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETRETSPHOTCLRINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETRETSTOREINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETRETSUMMARY;
import static com.scansee.common.constants.ScanSeeURLPath.GETSPDEALINFO;
import static com.scansee.common.constants.ScanSeeURLPath.SAVEUSRRETSALEHIT;
import static com.scansee.common.constants.ScanSeeURLPath.THISLOCATIONBASEURL;
import static com.scansee.common.constants.ScanSeeURLPath.UPDATESUSRZIPCODE;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.scansee.common.constants.ScanSeeURLPath;

/**
 * The interface for ThisLocation RestEasy implementation. ImplementedBy
 * {@link ThisLocaltionRestEasyImpl}
 * 
 * @author shyamsundara_hm
 */

@Path(THISLOCATIONBASEURL)
public interface ThisLocaltionRestEasy
{

	/**
	 * This is RestEasy webservice method for searching the retailer for the
	 * given zip code or location attributes(Latitude or longitude).Method Type:
	 * POST.
	 * 
	 * @param xml
	 *            the input request contains search parameters like zipcode or
	 *            location attributes.
	 * @return response XML as String containing Retailers list.
	 */

	@POST
	@Path(GETRETAILERSINFO)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getRetailersInfoForLocation(String xml);

	/**
	 * This is Rest Easy webservice method for fetching Categories for specified
	 * retailers in the request. Method Type:POST.
	 * 
	 * @param xml
	 *            request XML contains Retailer ID.
	 * @return returns response XML as String containing Category list or if
	 *         exception it will return error message XML.
	 */

	@POST
	@Path(GETCATEGORYINFO)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getCategoryInfo(String xml);

	/**
	 * This is Rest Easy webservice method for fetching Products information for
	 * the specified Category and Retailer location ID. Method: POST
	 * 
	 * @param xml
	 *            input request contains Category and Retailer location ID.
	 * @return returns response XML As String Containing Product List or if
	 *         exception it will return error message XML.
	 */

	@POST
	@Path(GETPRODUCTS)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getProductsInfo(String xml);

	/**
	 * method declaration for get fetching Favourite category info. the method
	 * has its implementation in the ThisLocationRestEasy Class.
	 * 
	 * @param userID
	 *            as request parameter
	 * @param retailID
	 *            as request parameter
	 * @return returns response XML based on Success or Error.
	 */
	@GET
	@Path(GETFAVCATERGORIES)
	@Produces("text/xml")
	String getFavCategoryInfo(@QueryParam("userID") Integer userID, @QueryParam("retailID") Integer retailID);

	/**
	 * method declaration for get fetching Favourite category info. the method
	 * has its implementation in the ThisLocationRestEasy Class.
	 * 
	 * @param userID
	 *            as request parameter
	 * @param retailID
	 *            as request parameter
	 * @return returns response XML based on Success or Error.
	 */
	@GET
	@Path(GETOTHERCATERGORIES)
	@Produces("text/xml")
	String getOtherCategoriesInfo(@QueryParam("userID") Integer userID, @QueryParam("retailID") Integer retailID);

	/**
	 * This Rest Easy method for recording User Advertisement Hit.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            advertisement info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */

	@POST
	@Path("advertisementhit")
	@Consumes("text/xml")
	@Produces("text/xml")
	String userAdvertisementHit(String xml);

	/**
	 * This Rest Easy method for fetching user's Latitude, Longitude info.
	 * 
	 * @param userID
	 *            whose location attributes need to be fetched.
	 * @return returns response XML Containing User's Location attributes if
	 *         exception it will return error message XML.
	 */
	@GET
	@Path(FETCHUSERLOCATIONPOINTS)
	@Produces("text/xml")
	String getUserLocationPoints(@QueryParam("userID") Integer userID);

	/**
	 * THis is RestEasy web service method for fetching postal code from given
	 * Latitude, Longitude.
	 * 
	 * @param latitude
	 *            as request parameter containing latitude value.
	 * @param longitude
	 *            as request parameter containing longitude value
	 * @return returns response XML Containing User's Location attributes if
	 *         exception it will return error message XML.
	 */
	@GET
	@Path(FETCHUSERPOSTALCODE)
	@Produces("text/xml")
	String getUserPostalcode(@QueryParam("latitude") Double latitude, @QueryParam("longitude") Double longitude);

	/**
	 * This is Rest Easy Webservice for getting Latitude and Longitude for the
	 * given Zipcode.
	 * 
	 * @param zipcode
	 *            as request parameter for Zipcode.
	 * @param userId
	 *            as request parameter for USerid
	 * @return returns response XML Containing User's Location attributes if
	 *         exception it will return error message XML.
	 */

	@GET
	@Path(FETCHLATLONG)
	@Produces("text/xml")
	String getLatLong(@QueryParam("zipcode") Long zipcode, @QueryParam("userId") Long userId);

	/**
	 * This is Rest Easy Web service for finding near by products for the given
	 * retailLocationID and userid.
	 * 
	 * @param xml
	 *            as input parameter
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */
	@POST
	@Path(FINDNEARBYPRODUCTS)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String findNearByProducts(String xml);

	/**
	 *   This is Rest Easy Web service for getting radius details from database.
	 * 
	 * @return returns response XML Containing radius details if exception it
	 *         will return error message XML.
	 */

	@GET
	@Path(GETRADIUSINFO)
	@Produces("text/xml")
	String getRadius();

	/**
	 * This is Rest Easy Webservice for getting Latitude and Longitude for the
	 * given Zipcode.
	 * 
	 * @param zipcode
	 *            as request parameter for Zipcode.
	 * @param userId
	 *            as request parameter for USerid
	 * @return returns response XML Containing User's Location attributes if
	 *         exception it will return error message XML.
	 */

	@GET
	@Path(UPDATESUSRZIPCODE)
	@Produces("text/xml")
	String updateZipcode(@QueryParam("userId") Long userId, @QueryParam("zipcode") String zipcode);

	/**
	 * This Rest Easy method for fetching user's Latitude, Longitude info.
	 * 
	 * @param userID
	 *            whose location attributes need to be fetched.
	 * @return returns response XML Containing User's Location attributes if
	 *         exception it will return error message XML.
	 */
	@GET
	@Path(CHECKUSRZIPCODE)
	@Produces("text/xml")
	String checkUsrZipcode(@QueryParam("userId") Long userID);

	/**
	 * This Rest Easy method for recording User retailer sale notification.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */

	@POST
	@Path(SAVEUSRRETSALEHIT)
	@Consumes("text/xml")
	@Produces("text/xml")
	String saveUsrRetSaleNotification(String xml);

	/**
	 * This Rest Easy method for fetching Retailer store details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id,userId info.
	 * @return returns response XML As String Containing Success message or if
	 *         exception it will return error message XML.
	 */

	@POST
	@Path(GETRETSTOREINFO)
	@Consumes("text/xml")
	@Produces("text/xml")
	String fetchRetailerStoreDetails(String xml);

	/**
	 * This Rest Easy method for fetching retailer summary details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or
	 *         exception it will return error message XML.
	 */

	@POST
	@Path(GETRETSUMMARY)
	@Consumes("text/xml")
	@Produces("text/xml")
	String getRetailerSummary(String xml);

	/**
	 * This Rest Easy method for fetching retailer summary details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or
	 *         exception it will return error message XML.
	 */

	@POST
	@Path(GETRETSPEOFFINFO)
	@Consumes("text/xml")
	@Produces("text/xml")
	String getRetailerSpecialOffDetails(String xml);

	/**
	 * This Rest Easy method for fetching retailer summary details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or
	 *         exception it will return error message XML.
	 */

	@POST
	@Path(GETRETCLRINFO)
	@Consumes("text/xml")
	@Produces("text/xml")
	String getRetailerCLRDetails(String xml);

	/**
	 * This Rest Easy method for fetching retailer location hot deals.
	 * 
	 * @param xml
	 *            as the request XML containing userId, retaileID,
	 *            retailerLocationID info and lowerLimit
	 * @return returns response XML As String Containing hot deal list or
	 *         exception it will return error message XML.
	 */
	@POST
	@Path(GETRETHOTDEALS)
	@Consumes("text/xml")
	@Produces("text/xml")
	String getRetailerHotDeals(String xml);
	
	/**
	 * This Rest Easy method for fetching retailer summary details.
	 * 
	 * @param xml
	 *            as the request XML containing user info and Retailer
	 *            id,retailer location id info.
	 * @return returns response XML As String Containing retailer details or
	 *         exception it will return error message XML.
	 */

	@POST
	@Path(GETRETSPHOTCLRINFO)
	@Consumes("text/xml")
	@Produces("text/xml")
	String getRetailerSpeHotDealsCLRDetails(String xml);
	
	/**
	 * This service is used display special offer details.
	 * @param xml containing special deal information
	 * @return xml 
	 * 	containing special deal details.
	 */
	
	@POST
	@Path(GETSPDEALINFO)
	@Consumes("text/xml")
	@Produces("text/xml")
	String fetchSpecialOffDetails(String xml);

	/**
	 * This is RestEasy webservice method for searching the retailer by city/groupID for austin experience.
	 * Method Type: POST.
	 * 
	 * @param xml
	 *            the input request contains search parameters like city name, zipcode or
	 *            location attributes.
	 * @return response XML as String containing Retailers list.
	 */
	@POST
	@Path(GETRETBYGROUPID)
	@Consumes("text/xml")
	@Produces("text/xml")
	String getRetailersByGroupID(String xml);
	
	/**
	 * This is RestEasy webservice method for searching the retailer by go local affiliate.
	 * Method Type: POST.
	 * 
	 * @param xml
	 *            the input request contains search parameters like city name, zipcode or
	 *            location attributes.
	 * @return response XML as String containing Retailers list.
	 */
	@POST
	@Path(ScanSeeURLPath.GETRETFORPARTNER)
	@Consumes("text/xml")
	@Produces("text/xml")
	String getRetailersForPartner(String xml);
	
	/**
	 * This is restEasy webservice method to get partners for Austin Experience
	 * Method Type: GET
	 * 
	 * @param xml
	 * 
	 * @return response XML as String containing partner list
	 */
	@GET
	@Path(GETPARTNERS)
	@Produces("text/xml")
	String getPartners(@QueryParam("retGroupID") Integer retGroupID);
	
	/**
	 * This is restEasy webservice method to get categories for Specified group retailers.
	 * Method Type: GET
	 * 
	 * @param xml
	 * 
	 * @return response XML as String containing partner list
	 */
	@GET
	@Path(ScanSeeURLPath.GETCATFORGROUP)
	@Produces("text/xml")
	String getCategoriesForGroupRetailers(@QueryParam("retGroupID") Integer retGroupID);
	
	/**
	 * This is restEasy webservice method to get categories for Specified partner retailers.
	 * Method Type: GET
	 * 
	 * @param xml
	 * 
	 * @return response XML as String containing partner list
	 */
	@GET
	@Path(ScanSeeURLPath.GETCATFORPARTNER)
	@Produces("text/xml")
	String getCategoriesForPartners(@QueryParam("retAffID") Integer retAffID);
	
	/**
	 * This is RestEasy web service method for user tracking retailer summary clicks.
	 * To be called whenever user taps on any of the item in the Retailer summary screen.
	 * Method Type: POST
	 * 
	 * @param xml
	 * @return xml response containing SUCCESS or FAILURE.
	 */
	@POST
	@Path(ScanSeeURLPath.UTRETSUMCLICK)
	@Consumes("text/xml")
	@Produces("text/xml")
	void userTrackingRetailerSummaryClick(String xml);
	
	/**
	 * This is RestEasy web service method for user tracking.
	 * To be called when the user taps on any special offers.
	 * Method Type: POST
	 * 
	 * @param xml
	 * 
	 * @return xml response as SUCCESS OR FAILURE
	 */
	@POST
	@Path(ScanSeeURLPath.UTRETSPEOFFCLICK)
	@Consumes("text/xml")
	@Produces("text/xml")
	void userTrackingRetSpeOffersClick(String xml);
	
}