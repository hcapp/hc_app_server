
/**
 * QBridgeServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package com.scansee.cellfire.webservice;

    /**
     *  QBridgeServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class QBridgeServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public QBridgeServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public QBridgeServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for clipCouponWithLoyaltyCard method
            * override this method for handling normal response from clipCouponWithLoyaltyCard operation
            */
           public void receiveResultclipCouponWithLoyaltyCard(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.ClipCouponWithLoyaltyCardResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from clipCouponWithLoyaltyCard operation
           */
            public void receiveErrorclipCouponWithLoyaltyCard(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addLoyaltyCard method
            * override this method for handling normal response from addLoyaltyCard operation
            */
           public void receiveResultaddLoyaltyCard(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.AddLoyaltyCardResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addLoyaltyCard operation
           */
            public void receiveErroraddLoyaltyCard(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getMerchantsAndOffers method
            * override this method for handling normal response from getMerchantsAndOffers operation
            */
           public void receiveResultgetMerchantsAndOffers(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.GetMerchantsAndOffersResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getMerchantsAndOffers operation
           */
            public void receiveErrorgetMerchantsAndOffers(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for removeLoyaltyCardFromUser method
            * override this method for handling normal response from removeLoyaltyCardFromUser operation
            */
           public void receiveResultremoveLoyaltyCardFromUser(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.RemoveLoyaltyCardFromUserResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from removeLoyaltyCardFromUser operation
           */
            public void receiveErrorremoveLoyaltyCardFromUser(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getLocalStoreLocationList method
            * override this method for handling normal response from getLocalStoreLocationList operation
            */
           public void receiveResultgetLocalStoreLocationList(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.GetLocalStoreLocationListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getLocalStoreLocationList operation
           */
            public void receiveErrorgetLocalStoreLocationList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getClippedCouponList method
            * override this method for handling normal response from getClippedCouponList operation
            */
           public void receiveResultgetClippedCouponList(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.GetClippedCouponListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getClippedCouponList operation
           */
            public void receiveErrorgetClippedCouponList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateUserInfo method
            * override this method for handling normal response from updateUserInfo operation
            */
           public void receiveResultupdateUserInfo(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.UpdateUserInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateUserInfo operation
           */
            public void receiveErrorupdateUserInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrentContentVersion method
            * override this method for handling normal response from getCurrentContentVersion operation
            */
           public void receiveResultgetCurrentContentVersion(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.GetCurrentContentVersionResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrentContentVersion operation
           */
            public void receiveErrorgetCurrentContentVersion(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getActiveCoupons method
            * override this method for handling normal response from getActiveCoupons operation
            */
           public void receiveResultgetActiveCoupons(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.GetActiveCouponsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getActiveCoupons operation
           */
            public void receiveErrorgetActiveCoupons(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCouponDetailList method
            * override this method for handling normal response from getCouponDetailList operation
            */
           public void receiveResultgetCouponDetailList(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.GetCouponDetailListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCouponDetailList operation
           */
            public void receiveErrorgetCouponDetailList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFullStoreLocaltionUrl method
            * override this method for handling normal response from getFullStoreLocaltionUrl operation
            */
           public void receiveResultgetFullStoreLocaltionUrl(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.GetFullStoreLocaltionUrlResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFullStoreLocaltionUrl operation
           */
            public void receiveErrorgetFullStoreLocaltionUrl(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for reportClipActivity method
            * override this method for handling normal response from reportClipActivity operation
            */
           public void receiveResultreportClipActivity(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.ReportClipActivityResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from reportClipActivity operation
           */
            public void receiveErrorreportClipActivity(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getLocalStoreLocationList2 method
            * override this method for handling normal response from getLocalStoreLocationList2 operation
            */
           public void receiveResultgetLocalStoreLocationList2(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.GetLocalStoreLocationList2Response result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getLocalStoreLocationList2 operation
           */
            public void receiveErrorgetLocalStoreLocationList2(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSavedLoyaltyCards method
            * override this method for handling normal response from getSavedLoyaltyCards operation
            */
           public void receiveResultgetSavedLoyaltyCards(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.GetSavedLoyaltyCardsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSavedLoyaltyCards operation
           */
            public void receiveErrorgetSavedLoyaltyCards(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getLocalStoreLocationList1 method
            * override this method for handling normal response from getLocalStoreLocationList1 operation
            */
           public void receiveResultgetLocalStoreLocationList1(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.GetLocalStoreLocationList1Response result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getLocalStoreLocationList1 operation
           */
            public void receiveErrorgetLocalStoreLocationList1(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCouponByUPC method
            * override this method for handling normal response from getCouponByUPC operation
            */
           public void receiveResultgetCouponByUPC(
                    com.scansee.cellfire.webservice.QBridgeServiceStub.GetCouponByUPCResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCouponByUPC operation
           */
            public void receiveErrorgetCouponByUPC(java.lang.Exception e) {
            }
                


    }
    