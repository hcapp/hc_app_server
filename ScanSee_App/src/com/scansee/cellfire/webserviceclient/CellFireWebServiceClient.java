package com.scansee.cellfire.webserviceclient;

import java.rmi.RemoteException;
import java.util.ArrayList;

import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.log4j.Logger;

import com.scansee.cellfire.webservice.QBridgeServiceStub;
import com.scansee.cellfire.webservice.QBridgeServiceStub.AddLoyaltyCard;
import com.scansee.cellfire.webservice.QBridgeServiceStub.AddLoyaltyCardResponse;
import com.scansee.cellfire.webservice.QBridgeServiceStub.ArrayOfInfoValue;
import com.scansee.cellfire.webservice.QBridgeServiceStub.ArrayOfInt;
import com.scansee.cellfire.webservice.QBridgeServiceStub.ArrayOfLoyaltyCard;
import com.scansee.cellfire.webservice.QBridgeServiceStub.ArrayOfQClipResponse;
import com.scansee.cellfire.webservice.QBridgeServiceStub.ArrayOfQCouponLite;
import com.scansee.cellfire.webservice.QBridgeServiceStub.ClipCouponWithLoyaltyCard;
import com.scansee.cellfire.webservice.QBridgeServiceStub.ClipCouponWithLoyaltyCardResponse;
import com.scansee.cellfire.webservice.QBridgeServiceStub.GetClippedCouponList;
import com.scansee.cellfire.webservice.QBridgeServiceStub.GetClippedCouponListResponse;
import com.scansee.cellfire.webservice.QBridgeServiceStub.GetCouponByUPC;
import com.scansee.cellfire.webservice.QBridgeServiceStub.GetCouponByUPCResponse;
import com.scansee.cellfire.webservice.QBridgeServiceStub.GetCouponDetailList;
import com.scansee.cellfire.webservice.QBridgeServiceStub.GetCouponDetailListResponse;
import com.scansee.cellfire.webservice.QBridgeServiceStub.GetCurrentContentVersion;
import com.scansee.cellfire.webservice.QBridgeServiceStub.GetCurrentContentVersionResponse;
import com.scansee.cellfire.webservice.QBridgeServiceStub.GetMerchantsAndOffers;
import com.scansee.cellfire.webservice.QBridgeServiceStub.GetMerchantsAndOffersResponse;
import com.scansee.cellfire.webservice.QBridgeServiceStub.GetSavedLoyaltyCards;
import com.scansee.cellfire.webservice.QBridgeServiceStub.GetSavedLoyaltyCardsResponse;
import com.scansee.cellfire.webservice.QBridgeServiceStub.InfoValue;
import com.scansee.cellfire.webservice.QBridgeServiceStub.LoyaltyCard;
import com.scansee.cellfire.webservice.QBridgeServiceStub.QClipRequests;
import com.scansee.cellfire.webservice.QBridgeServiceStub.QClipResponse;
import com.scansee.cellfire.webservice.QBridgeServiceStub.QCouponDetail;
import com.scansee.cellfire.webservice.QBridgeServiceStub.QCouponLite;
import com.scansee.cellfire.webservice.QBridgeServiceStub.QpnContent;
import com.scansee.cellfire.webservice.QBridgeServiceStub.RemoveLoyaltyCardFromUser;
import com.scansee.cellfire.webservice.QBridgeServiceStub.RemoveLoyaltyCardFromUserResponse;
import com.scansee.cellfire.webservice.QBridgeServiceStub.UpdateUserInfo;
import com.scansee.cellfire.webservice.QBridgeServiceStub.UpdateUserInfoResponse;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.CellFireRequest;

@SuppressWarnings("rawtypes")
public class CellFireWebServiceClient
{
	/**
	 * To get logger instance.
	 */
	private static final Logger LOG = Logger.getLogger(CellFireWebServiceClient.class);

	/**
	 * Variable used for Cellfire Api Key parameter name.
	 */
	private String apiKeyParameterName;

	/**
	 * Variable used for Cellfire Api Key parameter name.
	 */
	private String apiKeyParameterValue;
	/**
	 * Variable used for Cellfire tracking code parameter name.
	 */
	private String trackingCodeParameterName;
	/**
	 * Variable used for Cellfire tracking code parameter name.
	 */
	private String trackingCodeParameterValue;

	public CellFireWebServiceClient(String apiKeyParameterName, String apiKeyParameterValue, String trackingCodeParameterName,
			String trackingCodeParameterValue)
	{
		this.apiKeyParameterName = apiKeyParameterName;
		this.apiKeyParameterValue = apiKeyParameterValue;
		this.trackingCodeParameterName = trackingCodeParameterName;
		this.trackingCodeParameterValue = trackingCodeParameterValue;
	}

	public UpdateUserInfoResponse updateUserInfo(String referenceId, String partnerID, InfoValue[] values) throws ScanSeeException
	{
		LOG.info("CellFireWebServiceClient.class::::Inside updateUserInfo method ");

		UpdateUserInfoResponse userInfoResponse = null;

		try
		{
			QBridgeServiceStub stub = new QBridgeServiceStub();
			UpdateUserInfo updateUserInfo = new UpdateUserInfo();
			ArrayOfInfoValue arrayOfInfoValue = new ArrayOfInfoValue();
			ServiceClient serviceClient = stub._getServiceClient();
			Options options = serviceClient.getOptions();
			ArrayList customHttpHeaderList = getCustomHTTPHeaders();
			options.setProperty(HTTPConstants.HTTP_HEADERS, customHttpHeaderList);
			options.setProperty(HTTPConstants.CHUNKED, false);
			updateUserInfo.setIn0(referenceId);
			updateUserInfo.setIn1(partnerID);
			if (null != values)
			{
				arrayOfInfoValue.setInfoValue(values);
			}

			updateUserInfo.setIn2(arrayOfInfoValue);
			serviceClient.setOptions(options);
			LOG.info("Invoking updateUserInfo web service");
			userInfoResponse = stub.updateUserInfo(updateUserInfo);
		}
		catch (RemoteException e)
		{
			LOG.error("Exception Occured in CellFireWebServiceClient.class::Method:updateUserInfo" + e);
			throw new ScanSeeException(e);
		}

		LOG.info("CellFireWebServiceClient.class::::Exiting updateUserInfo method ");

		return userInfoResponse;

	}

	public AddLoyaltyCardResponse addLoyaltyCard(CellFireRequest cellFireRequest,String partnerId) throws ScanSeeException
	{

		LOG.info("CellFireWebServiceClient.class::::Inside addLoyaltyCard method ");
		AddLoyaltyCardResponse addLoyaltyCardResponse = null;
		try
		{
			QBridgeServiceStub stub = new QBridgeServiceStub();
			AddLoyaltyCard addLoyaltyCardRequest = new AddLoyaltyCard();
			ServiceClient serviceClient = stub._getServiceClient();
			Options options = serviceClient.getOptions();

			ArrayList customHttpHeaderList = getCustomHTTPHeaders();
			options.setProperty(HTTPConstants.HTTP_HEADERS, customHttpHeaderList);
			options.setProperty(HTTPConstants.CHUNKED, false);
			addLoyaltyCardRequest.setIn0(cellFireRequest.getReferenceId());
			addLoyaltyCardRequest.setIn1(partnerId);
			addLoyaltyCardRequest.setIn2(cellFireRequest.getCardNumber());
			addLoyaltyCardRequest.setIn3(Integer.parseInt(cellFireRequest.getMerchantId()));
			serviceClient.setOptions(options);
			addLoyaltyCardResponse = stub.addLoyaltyCard(addLoyaltyCardRequest);

		}
		catch (RemoteException e)
		{
			LOG.error("Exception Occured in CellFireWebServiceClient.class::Method:addLoyaltyCard" + e);
			throw new ScanSeeException(e);
		}

		LOG.info("CellFireWebServiceClient.class::::Exiting addLoyaltyCard method ");

		return addLoyaltyCardResponse;
	}

	public RemoveLoyaltyCardFromUserResponse removeLoyaltyCardFromUser(String referenceId, String partnerID, int merchantId) throws ScanSeeException
	{

		LOG.info("CellFireWebServiceClient.class::::Inside removeLoyaltyCardFromUser method ");
		RemoveLoyaltyCardFromUserResponse removeLoyaltyCardFromUserResponse = null;

		try
		{
			QBridgeServiceStub stub = new QBridgeServiceStub();
			RemoveLoyaltyCardFromUser removeLoyaltyCardFromUserRequest = new RemoveLoyaltyCardFromUser();
			ServiceClient serviceClient = stub._getServiceClient();
			Options options = serviceClient.getOptions();
			ArrayList customHttpHeaderList = getCustomHTTPHeaders();
			options.setProperty(HTTPConstants.HTTP_HEADERS, customHttpHeaderList);
			options.setProperty(HTTPConstants.CHUNKED, false);
			removeLoyaltyCardFromUserRequest.setIn0(referenceId);
			removeLoyaltyCardFromUserRequest.setIn1(partnerID);
			removeLoyaltyCardFromUserRequest.setIn2(merchantId);
			serviceClient.setOptions(options);
			removeLoyaltyCardFromUserResponse = stub.removeLoyaltyCardFromUser(removeLoyaltyCardFromUserRequest);
		}
		catch (RemoteException e)
		{
			LOG.error("Exception Occured in CellFireWebServiceClient.class::Method:removeLoyaltyCardFromUser" + e);
			throw new ScanSeeException(e);
		}

		LOG.info("CellFireWebServiceClient.class::::Exiting removeLoyaltyCardFromUser method ");
		return removeLoyaltyCardFromUserResponse;
	}

	public LoyaltyCard[] getSavedLoyaltyCards(String referenceId, String partnerID) throws ScanSeeException
	{

		LOG.info("CellFireWebServiceClient.class::::Inside getSavedLoyaltyCards method ");

		GetSavedLoyaltyCardsResponse getSavedLoyaltyCardsResponse = null;

		ArrayOfLoyaltyCard arrayOfLoyaltyCard = null;
		LoyaltyCard[] loyaltyCardsArray = null;

		try
		{
			QBridgeServiceStub stub = new QBridgeServiceStub();
			GetSavedLoyaltyCards getSavedLoyaltyCardsRequest = new GetSavedLoyaltyCards();
			ServiceClient serviceClient = stub._getServiceClient();
			Options options = serviceClient.getOptions();
			ArrayList customHttpHeaderList = getCustomHTTPHeaders();
			options.setProperty(HTTPConstants.HTTP_HEADERS, customHttpHeaderList);
			options.setProperty(HTTPConstants.CHUNKED, false);
			getSavedLoyaltyCardsRequest.setIn0(referenceId);
			getSavedLoyaltyCardsRequest.setIn1(partnerID);
			serviceClient.setOptions(options);
			getSavedLoyaltyCardsResponse = stub.getSavedLoyaltyCards(getSavedLoyaltyCardsRequest);
			arrayOfLoyaltyCard = getSavedLoyaltyCardsResponse.getOut();
			loyaltyCardsArray = arrayOfLoyaltyCard.getLoyaltyCard();
		}
		catch (RemoteException e)
		{
			LOG.error("Exception Occured in CellFireWebServiceClient.class::Method:getSavedLoyaltyCards" + e);
			throw new ScanSeeException(e);
		}

		LOG.info("CellFireWebServiceClient.class::::Exiting getSavedLoyaltyCards method ");
		return loyaltyCardsArray;

	}

	public QClipResponse[] clipCouponWithLoyaltyCard(QClipRequests clipRequests, long version) throws ScanSeeException
	{
		LOG.info("CellFireWebServiceClient.class::::Inside clipCouponWithLoyaltyCard method ");

		ClipCouponWithLoyaltyCardResponse getSavedLoyaltyCardsResponse = null;
		ArrayOfQClipResponse arrayOfQClipResponse = null;
		QClipResponse[] clipResponsesArray = null;

		try
		{
			QBridgeServiceStub stub = new QBridgeServiceStub();
			ClipCouponWithLoyaltyCard clipCouponWithLoyaltyCardRequest = new ClipCouponWithLoyaltyCard();
			ServiceClient serviceClient = stub._getServiceClient();
			Options options = serviceClient.getOptions();
			ArrayList customHttpHeaderList = getCustomHTTPHeaders();
			options.setProperty(HTTPConstants.HTTP_HEADERS, customHttpHeaderList);
			options.setProperty(HTTPConstants.CHUNKED, false);
			clipCouponWithLoyaltyCardRequest.setIn0(clipRequests);
			clipCouponWithLoyaltyCardRequest.setIn1(version);

			serviceClient.setOptions(options);
			getSavedLoyaltyCardsResponse = stub.clipCouponWithLoyaltyCard(clipCouponWithLoyaltyCardRequest);
			arrayOfQClipResponse = getSavedLoyaltyCardsResponse.getOut();
			clipResponsesArray = arrayOfQClipResponse.getQClipResponse();
		}
		catch (RemoteException e)
		{
			LOG.error("Exception Occured in CellFireWebServiceClient.class::Method:clipCouponWithLoyaltyCard" + e);
			throw new ScanSeeException(e);
		}

		LOG.info("CellFireWebServiceClient.class::::Exiting clipCouponWithLoyaltyCard method ");
		return clipResponsesArray;

	}

	public QCouponLite[] getClippedCouponList(String referenceId, String partnerID) throws ScanSeeException
	{

		LOG.info("CellFireWebServiceClient.class::::Inside getClippedCouponList method ");

		GetClippedCouponListResponse getClippedCouponListResponse = null;
		ArrayOfQCouponLite arrayOfQCouponLite = null;
		QCouponLite[] couponLitesArray = null;

		try
		{
			QBridgeServiceStub stub = new QBridgeServiceStub();
			GetClippedCouponList getClippedCouponListRequest = new GetClippedCouponList();

			ServiceClient serviceClient = stub._getServiceClient();
			Options options = serviceClient.getOptions();
			ArrayList customHttpHeaderList = getCustomHTTPHeaders();
			options.setProperty(HTTPConstants.HTTP_HEADERS, customHttpHeaderList);
			options.setProperty(HTTPConstants.CHUNKED, false);
			getClippedCouponListRequest.setIn0(referenceId);
			getClippedCouponListRequest.setIn1(partnerID);

			serviceClient.setOptions(options);
			getClippedCouponListResponse = stub.getClippedCouponList(getClippedCouponListRequest);
			arrayOfQCouponLite = getClippedCouponListResponse.getOut();
			couponLitesArray = arrayOfQCouponLite.getQCouponLite();
		}
		catch (RemoteException e)
		{
			LOG.error("Exception Occured in CellFireWebServiceClient.class::Method:getClippedCouponList" + e);
			throw new ScanSeeException(e);
		}

		LOG.info("CellFireWebServiceClient.class::::Exiting getClippedCouponList method ");
		return couponLitesArray;

	}



	public QpnContent getMerchantsAndOffers() throws ScanSeeException
	{
		LOG.info("CellFireWebServiceClient.class::::Inside getMerchantsAndOffers method ");
		QpnContent qpnContent = null;
		GetMerchantsAndOffersResponse respMerOff = null;
		try
		{
			QBridgeServiceStub stub = new QBridgeServiceStub();
			GetMerchantsAndOffers paramMerOff = new GetMerchantsAndOffers();
			ServiceClient serviceClient = stub._getServiceClient();
			Options options = serviceClient.getOptions();
			ArrayList customHttpHeaderList = getCustomHTTPHeaders();
			options.setProperty(HTTPConstants.HTTP_HEADERS, customHttpHeaderList);
			options.setProperty(HTTPConstants.CHUNKED, false);
			serviceClient.setOptions(options);
			LOG.info("Invoking getMerchantsAndOffers web service");
			respMerOff = stub.getMerchantsAndOffers(paramMerOff);
			qpnContent = respMerOff.getOut();
		}
		catch (RemoteException e)
		{
			LOG.error("Exception Occured in CellFireWebServiceClient.class::Method:getMerchantsAndOffers" + e);
			throw new ScanSeeException(e);
		}

		LOG.info("CellFireWebServiceClient.class::::Exiting getMerchantsAndOffers method ");
		return qpnContent;

	}

	public QCouponLite[] getCouponByUPC(String upc, int merchantId) throws ScanSeeException
	{
		LOG.info("CellFireWebServiceClient.class::::Inside getCouponByUPC method ");
		GetCouponByUPCResponse couponByUPCResponse = null;
		QCouponLite[] couponLiteArray = null;
		try
		{
			QBridgeServiceStub stub = new QBridgeServiceStub();
			GetCouponByUPC getCouponByUPC = new GetCouponByUPC();
			getCouponByUPC.setIn0(upc);
			getCouponByUPC.setIn1(merchantId);
			ServiceClient serviceClient = stub._getServiceClient();
			Options options = serviceClient.getOptions();
			ArrayList customHttpHeaderList = getCustomHTTPHeaders();
			options.setProperty(HTTPConstants.HTTP_HEADERS, customHttpHeaderList);
			options.setProperty(HTTPConstants.CHUNKED, false);
			serviceClient.setOptions(options);
			couponByUPCResponse = stub.getCouponByUPC(getCouponByUPC);
			couponLiteArray = couponByUPCResponse.getOut().getQCouponLite();
		}
		catch (RemoteException e)
		{
			LOG.error("Exception Occured in CellFireWebServiceClient.class::Method:getCouponByUPC" + e);
			throw new ScanSeeException(e);
		}

		LOG.info("CellFireWebServiceClient.class::::Exiting getCouponByUPC method ");
		return couponLiteArray;
	}

	public QCouponDetail[] getCouponDetailList(int[] couponId) throws ScanSeeException
	{
		LOG.info("CellFireWebServiceClient.class::::Inside getCouponByUPC method ");
		GetCouponDetailListResponse couponDetailListResponse = null;
		QCouponDetail[] couponDetailArray = null;
		try
		{
			QBridgeServiceStub stub = new QBridgeServiceStub();
			GetCouponDetailList couponDetailList = new GetCouponDetailList();
			ArrayOfInt arrayOfInt = new ArrayOfInt();
			arrayOfInt.set_int(couponId);
			couponDetailList.setIn0(arrayOfInt);
			ServiceClient serviceClient = stub._getServiceClient();
			Options options = serviceClient.getOptions();
			ArrayList customHttpHeaderList = getCustomHTTPHeaders();
			options.setProperty(HTTPConstants.HTTP_HEADERS, customHttpHeaderList);
			options.setProperty(HTTPConstants.CHUNKED, false);
			serviceClient.setOptions(options);
			couponDetailListResponse = stub.getCouponDetailList(couponDetailList);
			couponDetailArray = couponDetailListResponse.getOut().getQCouponDetail();
		}
		catch (RemoteException e)
		{
			LOG.error("Exception Occured in CellFireWebServiceClient.class::Method:getCouponByUPC" + e);
			throw new ScanSeeException(e);
		}
		LOG.info("CellFireWebServiceClient.class::::Exiting getCouponByUPC method ");
		return couponDetailArray;
	}

	public Long getCurrentContentVersion() throws ScanSeeException
	{
		LOG.info("CellFireWebServiceClient.class::::Inside getCurrentContentVersion method ");
		GetCurrentContentVersionResponse contentVersionResponse = null;
		Long contentVersion = null;
		try
		{
			QBridgeServiceStub stub = new QBridgeServiceStub();
			GetCurrentContentVersion currentContentVersion = new GetCurrentContentVersion();
			ServiceClient serviceClient = stub._getServiceClient();
			Options options = serviceClient.getOptions();
			ArrayList customHttpHeaderList = getCustomHTTPHeaders();
			options.setProperty(HTTPConstants.HTTP_HEADERS, customHttpHeaderList);
			options.setProperty(HTTPConstants.CHUNKED, false);
			serviceClient.setOptions(options);
			contentVersionResponse = stub.getCurrentContentVersion(currentContentVersion);
			contentVersion = contentVersionResponse.getOut();
		}
		catch (RemoteException e)
		{
			LOG.error("Exception Occured in CellFireWebServiceClient.class::Method:getCouponByUPC" + e);
			throw new ScanSeeException(e);
		}
		LOG.info("CellFireWebServiceClient.class::::Exiting getCurrentContentVersion method ");
		return contentVersion;
	}

	@SuppressWarnings("unchecked")
	public ArrayList getCustomHTTPHeaders()
	{

		LOG.info("CellFireWebServiceClient.class::::Inside getCustomHTTPHeaders method ");
		ArrayList customHTTPHeaderlist = new ArrayList();
		org.apache.commons.httpclient.Header header = new org.apache.commons.httpclient.Header();
		header.setName(apiKeyParameterName);
		header.setValue(apiKeyParameterValue);
		customHTTPHeaderlist.add(header);

		org.apache.commons.httpclient.Header header1 = new org.apache.commons.httpclient.Header();
		header1.setName(trackingCodeParameterName);
		header1.setValue(trackingCodeParameterValue);
		customHTTPHeaderlist.add(header1);
		LOG.info("CellFireWebServiceClient.class::::Exiting getCustomHTTPHeaders method ");
		return customHTTPHeaderlist;
	}

}
