package com.scansee.shoppinglist.controller;

import static com.scansee.common.util.Utility.formResponseXml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.servicefactory.ServiceFactory;
import com.scansee.shoppinglist.service.ShoppingListService;
import com.thoughtworks.xstream.alias.ClassMapper.Null;

/**
 * The ShoppingListRestEasy class has methods to accept requests for shopping
 * list module from client. The method is selected based on the URL Path and the
 * type of request(GET,POST). It invokes the appropriate method in Service
 * layer.
 * 
 * @author manjunatha_gh.
 */

public class ShoppingListRestEasyImpl implements ShoppingListRestEasy
{
	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ShoppingListRestEasyImpl.class);
	/**
	 * debugger flag for logging.
	 */
	private static final boolean ISDEBUGENABLED = LOG.isDebugEnabled();

	/**
	 * This is a RestEasy WebService Method for fetching main shopping list
	 * items for the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which main shopping list items need to be fetched.If
	 *            userId is null then it is invalid request.
	 * @return XML containing main shopping list items in the response.
	 */
	public String getMainShoppingListItems(Integer userId)
	{

		final String methodName = "getMainShoppingListItems of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved request User ID is " + userId);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getMainShoppingListItems(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching product information for
	 * the given userId and product search key. Method Type:GET.
	 * 
	 * @param userId
	 *            for which products information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @param prodSearchKey
	 *            for which products information need to be fetched.If
	 *            prodSearchKey is null then it is invalid request.
	 * @return XML containing products information in the response.
	 */
	public String getProductsInfo(Integer userId, String prodSearchKey)
	{
		final String methodName = "getProductsInfo of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved request parameters userId" + userId + "prodSeachkey is " + prodSearchKey);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getProductsForShopping(userId, prodSearchKey);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + " { } ", response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This is a RestEasy WebService Method for adding product to MSL. Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be add product to MSL
	 *            products .
	 * @return response: return the whether updated is successful or not.
	 */
	public String addToMainShoppingList(String xml)
	{
		final String methodName = "addToMainShoppingList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.addProductMainToShopList(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + " {} ", response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This is a RestEasy WebService Method for deleting product from SL. Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be deleted product from
	 *            SL.
	 * @return response: return the whether updated is successful or not.
	 */
	public String deleteMainShoppingList(String xml)
	{
		final String methodName = "deleteMainShoppingList of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.dleteProductMainShopList(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching product information for
	 * the given userId,productId and retailId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which product information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @param productId
	 *            for which product information need to be fetched.If productId
	 *            is null then it is invalid request.
	 * @param retailId
	 *            for which product information need to be fetched.If retailId
	 *            is null then it is invalid request.
	 * @return XML containing product information in the response.
	 */
	public String getProductInfo(Integer userId, Integer productId, Integer retailId, Integer saleListID, Integer prodListID, Integer mainMenuID, Integer scanID)
	{

		final String methodName = "getProductInfo of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request Recieved  parameters are userId is {} and product Id {}", userId, productId);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getProductsInfo(userId, productId, retailId, saleListID, prodListID, mainMenuID, scanID);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching today shopping list for
	 * the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which today shopping list need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return XML containing product information in the response.
	 */

	public String getTodaySL(Integer userId)
	{
		final String methodName = "getTodaySL of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request Recieved  userId" + userId);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getTodaySLProducts(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for adding unassigned product to SL.
	 * Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be add unassigned product
	 *            to SL.
	 * @return response: return the whether updated is successful or not.
	 */
	public String addUnassignedProd(String xml)
	{
		final String methodName = "addUnassignedProd of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request Recieved " + xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.addUnassigedProd(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching the products from
	 * shopping basket for the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which the products from shopping basket need to be
	 *            fetched.If userId is null then it is invalid request.
	 * @return String response as a string contains all shopping basket
	 *         products. if no products found No records found code will be
	 *         returned.
	 */
	public String getShoppingBasketProducts(Integer userId)
	{
		final String methodName = "getShoppingBasketProducts of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved userId" + userId);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getShoppingBasketProds(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for finding near by retailers.
	 * Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be find near by
	 *            retailers.
	 * @return XML containing near by retailers as response.
	 */
	public String findNearByRetailers(String xml)
	{
		final String methodName = "findNearByRetailers of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.findNearByRetailers(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for adds/removes products to/from
	 * shopping basket. Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be adds/removes products
	 *            to/from shopping basket.
	 * @return response: return the whether updated is successful or not.
	 */
	public String addRemoveSBProducts(String xml)
	{
		final String methodName = "addRemoveSBProducts of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.addRemoveSBProducts(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching the products from
	 * shopping basket for the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which the products from shopping basket need to be
	 *            fetched.If userId is null then it is invalid request.
	 * @return String response as a string contains all shopping basket
	 *         products. if no products found No records found code will be
	 *         returned.
	 */
	public String getShoppingBasketProd(String xml)
	{
		final String methodName = "getShoppingBasketProd of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			//LOG.debug("Request recieved userId " + userId);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getShopBasketProducts(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for the Adding a Coupon(to the
	 * Coupon Gallery). Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the Coupon.
	 * @return String response as Success or failure.
	 */
	public String couponAdd(String xml)
	{
		final String methodName = "couponAdd of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.couponAdd(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for the removing a Coupon(from the
	 * Coupon Gallery). Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the Coupon.
	 * @return String response as Success or failure.
	 */
	public String couponRemove(String xml)
	{
		final String methodName = "couponRemove of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.removeCoupon(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for adding Loyalty (to the Loyalty
	 * Gallery). Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the coupons.
	 * @return String as as Success or failure.
	 */

	public String loyaltyAdd(String xml)
	{
		final String methodName = "loyaltyAdd of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.addLoyalty(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for removing Loyalty (from the
	 * Loyalty Gallery). Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the loyalty.
	 * @return String as as Success or failure.
	 */
	public String loyaltyRemove(String xml)
	{
		final String methodName = "loyaltyRemove of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.removeLoyalty(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for adding Rebate (to the Rebate
	 * Gallery). Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the loyalty.
	 * @return String as as Success or failure.
	 */
	public String rebateAdd(String xml)
	{
		final String methodName = "rebateAdd of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.addRebate(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for removing Rebate (from the Rebate
	 * Gallery). Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the rebates.
	 * @return String as as Success or failure.
	 */
	public String rebateRemove(String xml)
	{
		final String methodName = "rebateRemove of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.removeRebate(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching the Coupon Info. Method
	 * Type:POST.
	 * 
	 * @param userId
	 *            used to for Fetching coupon information.
	 * @param productId
	 *            used to for Fetching coupon information.
	 * @param retailId
	 *            used to for Fetching coupon information.
	 * @return String response coupons will be returned.
	 */

	public String getCouponInfo(Integer userId, Integer productId, Integer retailId, Integer mainMenuID)
	{
		final String methodName = "getCouponInfo of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("parameters received  userId" + userId + "ProductId" + productId + "retail Id" + retailId);
		}
		String responseXml = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			responseXml = shoppingListService.getCouponInfo(userId, productId, retailId, mainMenuID);
		}
		catch (ScanSeeException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			responseXml = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * This is a RestEasy WebService Method for fetching the Loyalty Info.
	 * Method Type:POST.
	 * 
	 * @param userId
	 *            used to for Fetching loyalty information.
	 * @param productId
	 *            used to for Fetching loyalty information.
	 * @param retailId
	 *            used to for Fetching loyalty information.
	 * @return String response loyalty will be returned.
	 */
	public String getLoyaltyInfo(Integer userId, Integer productId, Integer retailId, Integer mainMenuID)
	{
		final String methodName = "getLoyaltyInfo of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved parameters are userId" + userId + "ProductId" + productId + "retailId" + retailId);
		}
		String responseXml = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			responseXml = shoppingListService.getLoyaltyInfo(userId, productId, retailId, mainMenuID).toString();
		}
		catch (ScanSeeException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			responseXml = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);

		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Response returned" + responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * This is a RestEasy WebService Method for fetching the Rebate Info. Method
	 * Type:POST.
	 * 
	 * @param userId
	 *            used to for Fetching rebate information.
	 * @param productId
	 *            used to for Fetching rebate information.
	 * @param retailId
	 *            used to for Fetching rebate information.
	 * @return String response rebate will be returned.
	 */
	public String getRebateInfo(Integer userId, Integer productId, Integer retailId, Integer mainMenuID)
	{
		final String methodName = "getRebateInfo of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved parameters are userId" + userId + "product Id" + productId + "retailId" + retailId);
		}
		String responseXml = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			responseXml = shoppingListService.getRebateInfo(userId, productId, retailId, mainMenuID);
		}
		catch (ScanSeeException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			responseXml = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * This is a RestEasy WebService Method adds/removes the products to/from
	 * Today's shopping list.Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the products.
	 * @return String response as Success or failure.
	 */
	public String addRemoveTodaySLProducts(String xml)
	{
		final String methodName = "addRemoveTodaySLProducts of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.addRemoveTodaySLProducts(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching shopping list and cart
	 * products for the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which shopping list and cart products need to be
	 *            fetched.If userId is null then it is invalid request.
	 * @return XML containing shopping list and cart products in the response.
	 */
	public String getTodaysNMasterSL(Integer userId)
	{
		final String methodName = "getTodaysNMasterSL of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved parameter userId" + userId);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getTodaysNMasterSL(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for add User notes for shopping
	 * list. Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the user entered notes.
	 * @return String response as Success or failure.
	 */
	@Override
	public String addUserNotes(String xml)
	{
		final String methodName = "addUserNotes of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		String s2 = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			/*
			 * final String s1 = xml.substring(xml.indexOf("<userNotes>"),
			 * xml.indexOf("</userNotes>")); LOG.info(s1); if (!s1.isEmpty()) {
			 * s2 = s1.substring(11, s1.length()); } LOG.info(s2); if
			 * (s2.indexOf(">") > 0 || s2.indexOf(">") == 0) {
			 * LOG.info("No greater than "); response =
			 * Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA,
			 * "Special character > should not be there"); return response; }
			 * else if (s2.indexOf("<") > 0 || s2.indexOf("<") == 0) {
			 * LOG.info("No less than "); response =
			 * Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA,
			 * "Special character < should not be there"); return response; }
			 * else if (s2.indexOf("\n") > 0 || s2.indexOf("\n") == 0) {
			 * LOG.info("No \"\n\" than "); response =
			 * Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA,
			 * "Special character \n should not be there"); return response; }
			 */
			// response =
			// shoppingListService.addUserNotes(Utility.removeSpecialCharsReverse(xml));
			response = shoppingListService.addUserNotes(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 *This is a RestEasy WebService Method for deleteUser notes for shopping
	 * list.Method Type:GET.
	 * 
	 * @param userId
	 *            user id id for deleting the notes.
	 * @return String response as Success or failure.
	 */
	@Override
	public String deleteUserNotes(Integer userId)
	{
		final String methodName = "deleteUserNotes of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved parameters userId" + userId);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.deleteUserNotes(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for retrieve User notes for shopping
	 * list. Method Type:GET.
	 * 
	 * @param userId
	 *            user id id for fetching the notes.
	 * @return String response contains user notes.
	 */
	@Override
	public String retrieveUserNotes(Integer userId)
	{
		final String methodName = "retrieveUserNotes of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved parameters userId" + userId);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getUserNotesDetails(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method declaration for fetching FindNearBy
	 * retailers. Method Type:GET.
	 * 
	 * @param userID
	 *            The user id in request.
	 * @param latitude
	 *            The latitude in request.
	 * @param productId
	 *            The product Id in request.
	 * @param longitude
	 *            The longitude in request.
	 * @param postalCode
	 *            in request.
	 * @param radius
	 *            The radius in request
	 * @return XML containing FindNearBy retailers in the response.
	 */
	@Override
	public String findNearBy(Integer userID, Integer productId, String latitude, String longitude, String postalCode, String radius, Integer mainMenuID)
	{
		final String methodName = "findNearBy of ResteasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved parameters userID" + userID + "productId" + productId + "latitude" + latitude + "radius" + radius);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.findNearBy(userID, productId, latitude, longitude, postalCode, radius, mainMenuID);
		}
		catch (ScanSeeException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method declaration for fetching retailers
	 * based on the lowest price. Method Type:GET.
	 * 
	 * @param userID
	 *            The user id in request.
	 * @param latitude
	 *            The latitude in request.
	 * @param productId
	 *            The product Id in request.
	 * @param longitude
	 *            The longitude in request.
	 * @param postalcode
	 *            in request.
	 * @param radius
	 *            The radius in request
	 * @param page
	 *            The page in request
	 * @return XML containing retailers based on the lowest price.
	 */
	public String findNearByLowestPrice(Integer userID, Integer productId, Double latitude, Double longitude, String postalcode, Integer radius,
			Integer page, Integer mainMenuID)
	{
		final String methodName = "findNearByLowestPrice";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		LOG.debug("Request recieved parameters userID" + userID + "productId" + productId + "latitude" + latitude + "longitude" + longitude
				+ "postalcode" + postalcode + "radius" + radius);
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.findNearByLowestPrice(userID, productId, latitude, longitude, postalcode, radius, page, mainMenuID);
		}
		catch (ScanSeeException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for removing all items from shopping
	 * cart list as CheckOut functionality.Method Type:GET.
	 * 
	 * @param userID
	 *            The user id in request.
	 * @param cartLatitude
	 *            The latitude in request.
	 * @param cartLongitude
	 *            The longitude in request.
	 * @return XML Checkout success or failure.
	 */
	@Override
	public String checkOut(Integer userID, float cartLatitude, float cartLongitude)
	{
		final String methodName = "checkOut";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved parameters userID " + userID + "cartLatitude" + cartLatitude + "cartLongitude" + cartLongitude);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.checkOutFromBasket(userID, cartLatitude, cartLongitude);
		}
		catch (ScanSeeException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for displaying audio,video and other
	 * information.Method Type:GET.
	 * 
	 * @param userId
	 *            used for fetching media information.
	 * @param productID
	 *            used for fetching media information.
	 * @param mediaType
	 *            fetching product media information.
	 * @return XML containing media information as in the response.
	 */
	@Override
	public String getMediaInfo(Integer userId, Integer productID, String mediaType, Integer prodListID)
	{

		final String methodName = "getMediaInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved parameters userId {} productID {} mediaType {}" + userId + "" + productID + "" + mediaType);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getMediaDetails(productID, mediaType, prodListID);
		}
		catch (ScanSeeException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Response returned is" + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching master shopping list
	 * retailers and categories for the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which master shopping list retailers and categories need
	 *            to be fetched.If userId is null then it is invalid request.
	 * @return XML containing master shopping list items in the response.
	 */

	@Override
	public String getMasterSLRetCatList(Integer userId)
	{

		final String methodName = "getMainSLRetCatList of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved request User ID is " + userId);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getMSLRetailerCategorylst(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching master shopping list
	 * products for the given input as XML. Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be fetched the MSL
	 *            products .
	 * @return XML containing master shopping list products in the response.
	 */

	@Override
	public String getMSLProducts(String xml)
	{
		final String methodName = "getMSLProducts of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved request User ID is" + xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getMSLProductslst(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching today shopping list
	 * products for the given input as XML. Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be fetched the TSL
	 *            products .
	 * @return XML containing TSL products in the response.
	 */

	@Override
	public String getTSLProducts(String xml)
	{
		final String methodName = "getTSLProducts of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved request User ID is" + xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getTSLProductslst(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for displaying coupon,rebates and
	 * loyalty information.Method Type:GET.
	 * 
	 * @param  xml
	 *            needed for displaying coupon,rebates and loyalty information
	 * @return XML containing coupon,rebates and loyalty information as
	 *         response.
	 */
	@Override
	public String fetchMSLCLRInfo(String xml)
	{
		final String methodName = "fetchMSLCLRInfo ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved xml " + xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.fetchMSLCLRDetails(xml);
		}
		catch (ScanSeeException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Response returned is " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching master shopping list
	 * products based on categories for the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which master shopping list categories and products need to
	 *            be fetched.If userId is null then it is invalid request.
	 * @param iLowLimit
	 * @return XML containing master shopping list items in the response.
	 */

	@Override
	public String getMSLCategoryProducts(Integer userId, Integer iLowLimit)
	{

		final String methodName = "getMSLCategoryProducts of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved request User ID " + userId);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getMSLCategoryProducts(userId, iLowLimit);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching product summary details
	 * for the given input as XML.
	 * 
	 * @param xml
	 *            -As a request parameter
	 * @return response containing product summary.
	 */
	@Override
	public String getProductSummary(String xml)
	{
		final String methodName = "getProductSummary";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved {}", xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.getProductSummary(xml);
		}
		catch (ScanSeeException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Response returned " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for finading online stores using
	 * External API. products for the given input as XML. Method Type:POST.
	 * 
	 * @param xml
	 *            containing the request for External API call .
	 * @return XML containing List of online stores.
	 */
	@Override
	public String findOnlineStores(String xml)
	{
		final String methodName = "findOnlineStores";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request xml is " + xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.findOnlineStores(xml);
		}
		catch (ScanSeeException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Response returned " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for adding today's SL product for
	 * the given product id and user id.
	 * 
	 * @param xml
	 *            -containing the request for External API call .
	 * @return XML containing List of SL products.
	 */

	@Override
	public String addTodaySLProductsBySearch(String xml)
	{
		final String methodName = "addTodaySLProductsBySearch of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.addTodaySLProductsBySearch(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching shopping list History
	 * products based on given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which shopping list history products need to be fetched.If
	 *            userId is null then it is invalid request.
	 * @return XML containing shopping list history items in the response.
	 */
	@Override
	public String fetchSLHistory(Integer userId,Integer lowerlimit)
	{
		final String methodName = "fetchSLHistory of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved request User ID " + userId);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.fetchSLHistoryItems(userId,lowerlimit);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for adding product from shopping
	 * list history to list or favorites or both. Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information needed to be add product form
	 *            shopping list history to list or favorites or both products .
	 * @return response: return the whether updated is successful or not.
	 */
	@Override
	public String addProuctFromHistoryToSL(String xml)
	{
		final String methodName = "addProuctFromHistoryToSL";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.REQUESTLOG, xml);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.addSLHistoryProdut(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + " {} ", response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for capturing Product Media click
	 * Method Type:GET.
	 * 
	 * @param pmListID
	 *            
	 * @return {@link Null} no return parameter
	 */
	@Override
	public void userTrackingProdMediaClick(Integer pmListID) {
		final String methodName = "userTrackingProdMediaClick";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request: Product Media List ID: " + pmListID);
		}
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.userTrackingProdMediaClick(pmListID);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Response" + " {} ", response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
	}

	/**
	 * This is a RestEasy WebService Method for capturing Online Product Click
	 * Method Type: GET.
	 * 
	 * @param onProdDetID
	 *            
	 * @return {@link Null} no return parameter
	 */
	@Override
	//public void userTrackingOnlineStoreClick(Integer onProdDetID) {
	public void userTrackingOnlineStoreClick(String xml) {
		final String methodName = "userTrackingOnlineStoreClick";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		/*if (ISDEBUGENABLED)
		{
			LOG.debug("Request: Product Media List ID: " + onProdDetID);
		}*/
		String response = null;
		final ShoppingListService shoppingListService = ServiceFactory.getShoppingListService();
		try
		{
			response = shoppingListService.userTrackingOnlineStoreClick(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Response" + " {} ", response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);	
	}

}
