package com.scansee.shoppinglist.controller;

import static com.scansee.common.constants.ScanSeeURLPath.ADDCOUPON;
import static com.scansee.common.constants.ScanSeeURLPath.ADDLOYALTY;
import static com.scansee.common.constants.ScanSeeURLPath.ADDPRODUCTTOSL;
import static com.scansee.common.constants.ScanSeeURLPath.ADDREBATE;
import static com.scansee.common.constants.ScanSeeURLPath.ADDREMBASKET;
import static com.scansee.common.constants.ScanSeeURLPath.ADDREMCART;
import static com.scansee.common.constants.ScanSeeURLPath.ADDSLHISTORYPRODUCT;
import static com.scansee.common.constants.ScanSeeURLPath.ADDTSLPRODUCTSEARCH;
import static com.scansee.common.constants.ScanSeeURLPath.ADDUNASSIGNEDPRODSL;
import static com.scansee.common.constants.ScanSeeURLPath.ADDUSERNOTES;
import static com.scansee.common.constants.ScanSeeURLPath.CHECKOUT;
import static com.scansee.common.constants.ScanSeeURLPath.DELETESLPRODUCT;
import static com.scansee.common.constants.ScanSeeURLPath.DELETEUSERNOTES;
import static com.scansee.common.constants.ScanSeeURLPath.FETCHPRODUCTCLRINFO;
import static com.scansee.common.constants.ScanSeeURLPath.FETCHSLHISTORY;
import static com.scansee.common.constants.ScanSeeURLPath.FINDNEARBY;
import static com.scansee.common.constants.ScanSeeURLPath.FINDNEARBYLOWESTPRICE;
import static com.scansee.common.constants.ScanSeeURLPath.FINDNEARBYRETAILERS;
import static com.scansee.common.constants.ScanSeeURLPath.GETCOUPONINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETLOYALTYINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETMSLCATEGORYPRODUCTS;
import static com.scansee.common.constants.ScanSeeURLPath.GETMSLPRODUCTS;
import static com.scansee.common.constants.ScanSeeURLPath.GETPRODUCTINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETPRODUCTSINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETPRODUCTSUMMARY;
import static com.scansee.common.constants.ScanSeeURLPath.GETREBATEINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETSBPRODUCTS;
import static com.scansee.common.constants.ScanSeeURLPath.GETSCPRODUCTS;
import static com.scansee.common.constants.ScanSeeURLPath.GETSHOPPINGLIST;
import static com.scansee.common.constants.ScanSeeURLPath.GETSHOPPLISTANDCARTPROD;
import static com.scansee.common.constants.ScanSeeURLPath.GETSLRETCATLIST;
import static com.scansee.common.constants.ScanSeeURLPath.GETTSLPRODUCTS;
import static com.scansee.common.constants.ScanSeeURLPath.GETUSERNOTES;
import static com.scansee.common.constants.ScanSeeURLPath.MEDIAINFO;
import static com.scansee.common.constants.ScanSeeURLPath.ONLINESTORES;
import static com.scansee.common.constants.ScanSeeURLPath.REMOVECOUPON;
import static com.scansee.common.constants.ScanSeeURLPath.REMOVELOYALTY;
import static com.scansee.common.constants.ScanSeeURLPath.REMOVEREBATE;
import static com.scansee.common.constants.ScanSeeURLPath.SHOPPINGLIST;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.inject.ImplementedBy;
import com.scansee.common.constants.ScanSeeURLPath;
import com.thoughtworks.xstream.alias.ClassMapper.Null;

/**
 * This rest easy for shopping list module.{@link ImplementedBy}
 * {@link ShoppingListRestEasyImpl}.
 * 
 * @author manjunath_gh
 */
@Path(SHOPPINGLIST)
public interface ShoppingListRestEasy
{

	/**
	 * This is a RestEasy WebService Method for fetching master shopping list
	 * items for the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which master shopping list items need to be fetched.If
	 *            userId is null then it is invalid request.
	 * @return XML containing master shopping list items in the response.
	 */

	@GET
	@Path(GETSHOPPINGLIST)
	@Produces("text/xml;charset=UTF-8")
	String getMainShoppingListItems(@QueryParam("USERID") Integer userId);

	/**
	 * This is a RestEasy WebService Method for fetching master shopping list
	 * retailers and categories for the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which master shopping list retailers and categories need
	 *            to be fetched.If userId is null then it is invalid request.
	 * @return XML containing master shopping list items in the response.
	 */
	@GET
	@Path(GETSLRETCATLIST)
	@Produces("text/xml;charset=UTF-8")
	String getMasterSLRetCatList(@QueryParam("userId") Integer userId);

	/**
	 * This is a RestEasy WebService Method for fetching master shopping list
	 * products for the given input as XML. Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be fetched the MSL
	 *            products .
	 * @return XML containing master shopping list products in the response.
	 */
	@POST
	@Path(GETMSLPRODUCTS)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getMSLProducts(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching today shopping list
	 * products for the given input as XML. Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be fetched the TSL
	 *            products .
	 * @return XML containing TSL products in the response.
	 */
	@POST
	@Path(GETTSLPRODUCTS)
	@Produces("text/xml;charset=UTF-8")
	String getTSLProducts(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching product information for
	 * the given userId and product search key. Method Type:GET.
	 * 
	 * @param userId
	 *            for which products information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @param prodSearchKey
	 *            for which products information need to be fetched.If
	 *            prodSearchKey is null then it is invalid request.
	 * @return XML containing products information in the response.
	 */
	@GET
	@Path(GETPRODUCTSINFO)
	@Produces("text/xml;charset=UTF-8")
	String getProductsInfo(@QueryParam("userId") Integer userId, @QueryParam("searchKey") String prodSearchKey);

	/**
	 * This is a RestEasy WebService Method for adding product to MSL. Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be add product to MSL
	 *            products .
	 * @return response: return the whether updated is successful or not.
	 */
	@POST
	@Path(ADDPRODUCTTOSL)
	@Produces("text/xml")
	@Consumes("text/xml")
	String addToMainShoppingList(String xml);

	/**
	 * This is a RestEasy WebService Method for deleting product from SL. Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be deleted product from
	 *            SL.
	 * @return response: return the whether updated is successful or not.
	 */

	@POST
	@Path(DELETESLPRODUCT)
	@Produces("text/xml")
	@Consumes("text/xml")
	String deleteMainShoppingList(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching product information for
	 * the given userId,productId and retailId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which product information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @param productId
	 *            for which product information need to be fetched.If productId
	 *            is null then it is invalid request.
	 * @param retailId
	 *            for which product information need to be fetched.If retailId
	 *            is null then it is invalid request.
	 * @return XML containing product information in the response.
	 */
	@GET
	@Path(GETPRODUCTINFO)
	@Produces("text/xml;charset=UTF-8")
	String getProductInfo(@QueryParam("userId") Integer userId,
			@QueryParam("productId") Integer productId,
			@QueryParam("retailId") Integer retailId,
			@QueryParam("SaleListID") Integer saleListID,
			@QueryParam("ProductistID") Integer prodListID,
			@QueryParam("MainMenuID") Integer mainMenuID,
			@QueryParam("ScanID") Integer scanID);
			
	/**
	 * This is a RestEasy WebService Method for fetching today shopping list for
	 * the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which today shopping list need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return XML containing product information in the response.
	 */

	@GET
	@Path(GETSCPRODUCTS)
	@Produces("text/xml;charset=UTF-8")
	String getTodaySL(@QueryParam("userId") Integer userId);

	/**
	 * This is a RestEasy WebService Method for adding unassigned product to SL.
	 * Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be add unassigned product
	 *            to SL.
	 * @return response: return the whether updated is successful or not.
	 */
	@POST
	@Path(ADDUNASSIGNEDPRODSL)
	@Produces("text/xml")
	@Consumes("text/xml")
	String addUnassignedProd(String xml);

	/**
	 * This is a RestEasy WebService Method for finding near by retailers.
	 * Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be find near by
	 *            retailers.
	 * @return XML containing near by retailers as response.
	 */
	@POST
	@Path(FINDNEARBYRETAILERS)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String findNearByRetailers(String xml);

	/**
	 * This is a RestEasy WebService Method for adds/removes products to/from
	 * shopping basket. Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be adds/removes products
	 *            to/from shopping basket.
	 * @return response: return the whether updated is successful or not.
	 */
	@POST
	@Path(ADDREMBASKET)
	@Produces("text/xml")
	@Consumes("text/xml")
	String addRemoveSBProducts(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching the products from
	 * shopping basket for the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which the products from shopping basket need to be
	 *            fetched.If userId is null then it is invalid request.
	 * @return String response as a string contains all shopping basket
	 *         products. if no products found No records found code will be
	 *         returned.
	 */
	@POST
	@Path(GETSBPRODUCTS)
	@Produces("text/xml")
	@Consumes("text/xml")
	String getShoppingBasketProd(String xml);

	/**
	 * This is a RestEasy WebService Method for the Adding a Coupon(to the
	 * Coupon Gallery). Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the Coupon.
	 * @return String response as Success or failure.
	 */

	@POST
	@Path(ADDCOUPON)
	@Produces("text/xml")
	@Consumes("text/xml")
	String couponAdd(String xml);

	/**
	 * This is a RestEasy WebService Method for the removing a Coupon(from the
	 * Coupon Gallery). Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the Coupon.
	 * @return String response as Success or failure.
	 */

	@POST
	@Path(REMOVECOUPON)
	@Produces("text/xml")
	@Consumes("text/xml")
	String couponRemove(String xml);

	/**
	 * This is a RestEasy WebService Method for adding Loyalty (to the Loyalty
	 * Gallery). Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the coupons.
	 * @return String as as Success or failure.
	 */

	@POST
	@Path(ADDLOYALTY)
	@Produces("text/xml")
	@Consumes("text/xml")
	String loyaltyAdd(String xml);

	/**
	 * This is a RestEasy WebService Method for removing Loyalty (from the
	 * Loyalty Gallery). Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the loyalty.
	 * @return String as as Success or failure.
	 */

	@POST
	@Path(REMOVELOYALTY)
	@Produces("text/xml")
	@Consumes("text/xml")
	String loyaltyRemove(String xml);

	/**
	 * This is a RestEasy WebService Method for adding Rebate (to the Rebate
	 * Gallery). Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the loyalty.
	 * @return String as as Success or failure.
	 */

	@POST
	@Path(ADDREBATE)
	@Produces("text/xml")
	@Consumes("text/xml")
	String rebateAdd(String xml);

	/**
	 * This is a RestEasy WebService Method for removing Rebate (from the Rebate
	 * Gallery). Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the rebates.
	 * @return String as as Success or failure.
	 */

	@POST
	@Path(REMOVEREBATE)
	@Produces("text/xml")
	@Consumes("text/xml")
	String rebateRemove(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching the Coupon Info. Method
	 * Type:POST.
	 * 
	 * @param userId
	 *            used to for Fetching coupon information.
	 * @param productId
	 *            used to for Fetching coupon information.
	 * @param retailId
	 *            used to for Fetching coupon information.
	 * @return String response coupons will be returned.
	 */

	@GET
	@Path(GETCOUPONINFO)
	@Produces("text/plain;charset=UTF-8")
	String getCouponInfo(@QueryParam("userId") Integer userId,
			@QueryParam("productId") Integer productId,
			@QueryParam("retailId") Integer retailId,
			@QueryParam("mainMenuID") Integer mainMenuID);

	/**
	 * This is a RestEasy WebService Method for fetching the Loyalty Info.
	 * Method Type:POST.
	 * 
	 * @param userId
	 *            used to for Fetching loyalty information.
	 * @param productId
	 *            used to for Fetching loyalty information.
	 * @param retailId
	 *            used to for Fetching loyalty information.
	 * @return String response loyalty will be returned.
	 */
	@GET
	@Path(GETLOYALTYINFO)
	@Produces("text/plain;charset=UTF-8")
	String getLoyaltyInfo(@QueryParam("userId") Integer userId, @QueryParam("productId") Integer productId, @QueryParam("retailId") Integer retailId, @QueryParam("mainMenuID") Integer mainMenuID);

	/**
	 * This is a RestEasy WebService Method for fetching the Rebate Info. Method
	 * Type:POST.
	 * 
	 * @param userId
	 *            used to for Fetching rebate information.
	 * @param productId
	 *            used to for Fetching rebate information.
	 * @param retailId
	 *            used to for Fetching rebate information.
	 * @return String response rebate will be returned.
	 */
	@GET
	@Path(GETREBATEINFO)
	@Produces("text/plain;charset=UTF-8")
	String getRebateInfo(@QueryParam("userId") Integer userId,
			@QueryParam("productId") Integer productId,
			@QueryParam("retailId") Integer retailId,
			@QueryParam("mainMenuID") Integer mainMenuID);

	/**
	 * This is a RestEasy WebService Method adds/removes the products to/from
	 * Today's shopping list.Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the products.
	 * @return String response as Success or failure.
	 */
	@POST
	@Path(ADDREMCART)
	@Produces("text/xml")
	@Consumes("text/xml")
	String addRemoveTodaySLProducts(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching shopping list and cart
	 * products for the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which shopping list and cart products need to be
	 *            fetched.If userId is null then it is invalid request.
	 * @return XML containing shopping list and cart products in the response.
	 */
	@GET
	@Path(GETSHOPPLISTANDCARTPROD)
	@Produces("text/xml;charset=UTF-8")
	String getTodaysNMasterSL(@QueryParam("userId") Integer userId);

	/**
	 * This is a RestEasy WebService Method for add User notes for shopping
	 * list. Method Type:POST.
	 * 
	 * @param xml
	 *            contains information about the user entered notes.
	 * @return String response as Success or failure.
	 */

	@POST
	@Path(ADDUSERNOTES)
	@Produces("text/xml")
	@Consumes("text/xml")
	String addUserNotes(String xml);

	/**
	 *This is a RestEasy WebService Method for deleteUser notes for shopping
	 * list.Method Type:GET.
	 * 
	 * @param userId
	 *            user id id for deleting the notes.
	 * @return String response as Success or failure.
	 */
	@GET
	@Path(DELETEUSERNOTES)
	@Produces("text/xml")
	String deleteUserNotes(@QueryParam("userId") Integer userId);

	/**
	 * This is a RestEasy WebService Method for retrieve User notes for shopping
	 * list. Method Type:GET.
	 * 
	 * @param userId
	 *            user id id for fetching the notes.
	 * @return String response contains user notes.
	 */
	@GET
	@Path(GETUSERNOTES)
	@Produces("text/xml;charset=UTF-8")
	String retrieveUserNotes(@QueryParam("userId") Integer userId);

	/**
	 * This is a RestEasy WebService Method declaration for fetching FindNearBy
	 * retailers. Method Type:GET.
	 * 
	 * @param userID
	 *            The user id in request.
	 * @param latitude
	 *            The latitude in request.
	 * @param productId
	 *            The product Id in request.
	 * @param longitude
	 *            The longitude in request.
	 * @param postalCode
	 *            in request.
	 * @param radius
	 *            The radius in request
	 * @return XML containing FindNearBy retailers in the response.
	 */

	@GET
	@Path(FINDNEARBY)
	@Produces("text/xml;charset=UTF-8")
	String findNearBy(@QueryParam("userID") Integer userID,
			@QueryParam("productId") Integer productId,
			@QueryParam("latitude") String latitude,
			@QueryParam("longitude") String longitude,
			@QueryParam("postalCode") String postalCode,
			@QueryParam("radius") String radius, @QueryParam("mainMenuID") Integer mainMenuID);

	/**
	 * This is a RestEasy WebService Method declaration for fetching retailers
	 * based on the lowest price. Method Type:GET.
	 * 
	 * @param userID
	 *            The user id in request.
	 * @param latitude
	 *            The latitude in request.
	 * @param productId
	 *            The product Id in request.
	 * @param longitude
	 *            The longitude in request.
	 * @param postalCode
	 *            in request.
	 * @param radius
	 *            The radius in request
	 * @param page
	 *            The page in request
	 * @return XML containing retailers based on the lowest price.
	 */

	@GET
	@Path(FINDNEARBYLOWESTPRICE)
	@Produces("text/xml;charset=UTF-8")
	String findNearByLowestPrice(@QueryParam("userID") Integer userID, @QueryParam("productId") Integer productId, @QueryParam("latitude") Double latitude,
			@QueryParam("longitude") Double longitude, @QueryParam("postalCode") String postalCode, @QueryParam("radius") Integer radius,
			@QueryParam("page") Integer page, @QueryParam("mainMenuID") Integer mainMenuID);

	/**
	 * This is a RestEasy WebService Method for removing all items from shopping
	 * cart list as CheckOut functionality.Method Type:GET.
	 * 
	 * @param userID
	 *            The user id in request.
	 * @param cartLatitude
	 *            The latitude in request.
	 * @param cartLongitude
	 *            The longitude in request.
	 * @return XML Checkout success or failure.
	 */

	@GET
	@Path(CHECKOUT)
	@Produces("text/xml")
	String checkOut(@QueryParam("userID") Integer userID, @QueryParam("cartLatitude") float cartLatitude, @QueryParam("cartLongitude") float cartLongitude);

	/**
	 * This is a RestEasy WebService Method for displaying audio,video and other
	 * information.Method Type:GET.
	 * 
	 * @param userId
	 *            used for fetching media information.
	 * @param productID
	 *            used for fetching media information.
	 * @param mediaType
	 *            fetching product media information.
	 * @return XML containing media information as in the response.
	 */

	@GET
	@Path(MEDIAINFO)
	@Produces("text/xml;charset=UTF-8")
	String getMediaInfo(@QueryParam("userId") Integer userId,
			@QueryParam("productID") Integer productID,
			@QueryParam("mediaType") String mediaType,
			@QueryParam("prodListID") Integer prodListID);
			
	/**
	 * This is a RestEasy WebService Method for displaying coupon,rebates and
	 * loyalty information.Method Type:GET.
	 * 
	 * @param xml
	 *           needed to fetch coupon,rebates and loyalty information
	 * @return XML containing coupon,rebates and loyalty information as
	 *         response.
	 */

	@POST
	@Path(FETCHPRODUCTCLRINFO)
	@Produces("text/xml;charset=UTF-8")
	String fetchMSLCLRInfo(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching master shopping list
	 * products for the given input as XML. Method Type:POST.
	 * 
	 * @param userId
	 *            containing input information need to be fetched the MSL
	 *            products
	 * @return XML containing master shopping list products in the response.
	 */
	@GET
	@Path(GETMSLCATEGORYPRODUCTS)
	@Produces("text/xml;charset=UTF-8")
	String getMSLCategoryProducts(@QueryParam("userId") Integer userId, @QueryParam("lowLimit") Integer lowerLimit);

	/**
	 * This is a RestEasy WebService Method for fetching product summary details
	 * for the given input as XML. Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be fetched product
	 *            summary
	 * @return XML containing product summary details in the response.
	 */
	@POST
	@Path(GETPRODUCTSUMMARY)
	@Produces("text/xml;charset=UTF-8")
	String getProductSummary(String xml);

	/**
	 * External API. products for the given input as XML. Method Type:POST.
	 * 
	 * @param xml
	 *            containing the request for External API call .
	 * @return XML containing List of online stores.
	 */
	@POST
	@Path(ONLINESTORES)
	@Produces("text/xml;charset=UTF-8")
	String findOnlineStores(String xml);

	/**
	 * This is a RestEasy WebService Method for adding today's SL product for
	 * the given product id and user id.
	 * 
	 * @param xml
	 *            -containing the request for External API call .
	 * @return XML containing List of SL products.
	 */
	/**
	 * This is a RestEasy WebService Method for adding search product Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            containing the product details to be added.
	 * @return XML containing success or failure.
	 */

	@POST
	@Path(ADDTSLPRODUCTSEARCH)
	@Produces("text/xml")
	@Consumes("text/xml")
	String addTodaySLProductsBySearch(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching shopping list history
	 * items for the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which shopping list history items need to be fetched.If
	 *            userId is null then it is invalid request.
	 * @return XML containing shopping list history items in the response.
	 */

	@GET
	@Path(FETCHSLHISTORY)
	@Produces("text/xml;charset=UTF-8")
	String fetchSLHistory(@QueryParam("userId") Integer userId,@QueryParam("lowerlimit") Integer lowerLimit);

	/**
	 * This is a RestEasy WebService Method for adding product from shopping
	 * list history to list or favorites or both Method Type:POST.
	 * 
	 * @param xml
	 *            containing input information need to be add product to
	 *            Shopping list or favorites or both
	 * @return response: return the whether updated is successful or not.
	 */
	@POST
	@Path(ADDSLHISTORYPRODUCT)
	@Produces("text/xml")
	@Consumes("text/xml")
	String addProuctFromHistoryToSL(String xml);
	
	/**
	 * This is a RestEasy WebService Method for capturing Product Media click
	 * Method Type:GET.
	 * 
	 * @param pmListID
	 *            
	 * @return {@link Null} no return parameter
	 */
	@GET
	@Path(ScanSeeURLPath.UTPMCLICK)
	void userTrackingProdMediaClick(@QueryParam("pmListID") Integer pmListID);
	
	/**
	 * This is a RestEasy WebService Method for capturing Online Product Click
	 * Method Type:GET.
	 * 
	 * @param onProdDetID
	 *            
	 * @return {@link Null} no return parameter
	 */
/*	@GET
	@Path(ScanSeeURLPath.UTONSTOCLICK)
	void userTrackingOnlineStoreClick(@QueryParam("onProdDetID") Integer onProdDetID);*/
	
	@POST
	@Path(ScanSeeURLPath.UTONSTOCLICK)
	@Produces("text/xml")
	@Consumes("text/xml")
	void userTrackingOnlineStoreClick(String xml);

}