package com.scansee.shoppinglist.query;

/**
 * This class contains query used for the Shopping List module.
 * 
 * @author manjunatha_gh.
 */
public class ShoppingListQueries
{

	/**
	 * This query used to fetch the Shopping product information.
	 */

	public static final String SHOPPINGCARTPRODQUERY = " SELECT R.RetailID retailerId , R.RetailName retailerName , UP.ProductID productId "
			+ ", P.ProductName productName FROM UserProduct UP LEFT JOIN Product P ON P.ProductID = UP.ProductID "
			+ "	LEFT JOIN Retailer R ON R.RetailID = UP.UserRetailPreferenceID  " + " WHERE   UP.ShopListItem = 1 AND UserID = ?";

	/**
	 * This query used to fetch the product information.
	 */
	public static final String PRODUCTINOQUERY = "SELECT p.ProductID, p.ProductName"
			+ " ,p.ManufacturerID, p.ModelNumber, p.ProductShortDescription,p.ProductLongDescription,"
			+ " p.ProductExpirationDate productExpDate, p.ProductImagePath imagePath, p.SuggestedRetailPrice,p.SKUNumber skuCode "
			+ ", p.Weight productWeight,p.ScanCode barCode, p.WeightUnits,M.ManufName manufacturersName FROM Product p LEFT JOIN Manufacturer M ON M.ManufacturerID = P.ManufacturerID WHERE ProductID =?";

	/**
	 * This query used to Add unassigned product information.
	 */
	public static final String UNASSIGNQUERY = "INSERT INTO UserProduct " + " (UserID,ShopListItem,WishListItem,ShopCartItem,ShopBasketItem,"
			+ " UnassignedProductName,UnassignedProductDescription) " + " VALUES (?,?,?,?,?,?,?)";

	/**
	 * This query used to fetch the Shopping User notes information.
	 */
	public static final String SHOPPINGUSERNOTESQUERY = "SELECT UserID userId,Note userNotes,UserShoppingCartNoteID userNoteId FROM [UserShoppingCartNote] WHERE UserID=?";

	/**
	 * This query used to fetch the User email id.
	 */
	public static final String FETCHUSEREMAILID = "select Email from Users where UserID = ?";

	/**
	 * This query used to fetch retailer information.
	 */
	public static final String FETCHRETAILERINFO = "SELECT Address1 retaileraddress1,Address2 retaileraddress2,Address3 retaileraddress3,Address4 retaileraddress4,City city,State state,PostalCode postalCode from RetailLocation where RetailID=? and RetailLocationID=?";

	/**
	 * This query used to fetch retailer information with retail id and product
	 * id.
	 */
	public static final String FETCHRETAILERINFOWITHRETAILERINFO = "SELECT R.RetailName retailerName ,RL.Address1 retaileraddress1,RL.Address2 retaileraddress2,"
			+ "RL.Address3 retaileraddress3,RL.Address4 retaileraddress4,RL.City city,RL.State state,RL.PostalCode postalCode,RLP.SalePrice salePrice "
			+ "FROM Retailer R inner join RetailLocation RL on R.RetailID=RL.RetailID inner join RetailLocationProduct RLP on RL.RetailLocationID=RLP.RetailLocationID"
			+ " where RL.RetailID=? and RL.RetailLocationID=? and RLP.ProductID=?";

	/**
	 * This query used to insert external api call url.
	 */
	public static final String INSERTEXTERNALAPICALURL = "insert into FindNearByLog values(?,?,?)";

	/**
	 * This query used to fetch stock info.
	 */
	public static final String FETCHSTOCKINFO = "select ScreenName stockHeader,ScreenContent stockImagePath from AppConfiguration where ConfigurationType='Stock'";

	
	/**
	 * This query used to fetch the product information.
	 */
	public static final String SHAREPRODUCTINOQUERY = "SELECT p.ProductID, p.ProductName shareProdName "
			+ " ,p.ManufacturerID, p.ModelNumber, p.ProductShortDescription,p.ProductLongDescription shareProdLongDesc,"
			+ " p.ProductExpirationDate productExpDate, p.ProductImagePath imagePath, p.SuggestedRetailPrice,p.SKUNumber skuCode "
			+ ", p.Weight productWeight,p.ScanCode barCode, p.WeightUnits,M.ManufName manufacturersName FROM Product p LEFT JOIN Manufacturer M ON M.ManufacturerID = P.ManufacturerID WHERE ProductID =?";

	/**
	 * A private constructor.
	 */
	private ShoppingListQueries()
	{

	}
}
