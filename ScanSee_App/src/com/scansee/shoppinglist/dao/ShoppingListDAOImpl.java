package com.scansee.shoppinglist.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AddRemoveCLR;
import com.scansee.common.pojos.AddRemoveSBProducts;
import com.scansee.common.pojos.AddSLRequest;
import com.scansee.common.pojos.AppConfiguration;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.Category;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.CouponsDetails;
import com.scansee.common.pojos.FindNearByDetail;
import com.scansee.common.pojos.FindNearByDetails;
import com.scansee.common.pojos.FindNearByIntactResponse;
import com.scansee.common.pojos.FindNearByPartialResponse;
import com.scansee.common.pojos.FindNearByResponse;
import com.scansee.common.pojos.LoyaltyDetail;
import com.scansee.common.pojos.LoyaltyDetails;
import com.scansee.common.pojos.MainSLRetailerCategory;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.ProductDetailsRequest;
import com.scansee.common.pojos.RebateDetail;
import com.scansee.common.pojos.RebateDetails;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailerInfo;
import com.scansee.common.pojos.ShoppingBasketProducts;
import com.scansee.common.pojos.ShoppingCartProducts;
import com.scansee.common.pojos.ShoppingListResultSet;
import com.scansee.common.pojos.ThisLocationRequest;
import com.scansee.common.pojos.UserNotesDetails;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.common.util.Utility;
import com.scansee.externalapi.common.pojos.ExternalAPIErrorLog;
import com.scansee.externalapi.common.pojos.ExternalAPISearchParameters;
import com.scansee.externalapi.common.pojos.ExternalAPIVendor;
import com.scansee.shoppinglist.query.ShoppingListQueries;

/**
 * This class for fetching shopping list details.
 * 
 * @author shyamsundara_hm
 */

public class ShoppingListDAOImpl implements ShoppingListDAO
{

	/**
	 * Logger instance.
	 */
	private static final Logger log = LoggerFactory.getLogger(ShoppingListDAOImpl.class.getName());
	/**
	 * for Jdbc connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set ParameterizedBeanPropertyRowMapper to map POJO.
	 */

	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * This method for to get data source Spring DI.
	 * 
	 * @param dataSource
	 *            for DB operations.
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * The DAO method for fetching the master shopping list items for the given
	 * userId from the database.
	 * 
	 * @param userId
	 *            - for which master shopping list items need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return ShoppingListResultSet as ArrayList.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<ShoppingListResultSet> getMainShoppingListItems(int userId) throws ScanSeeException
	{

		final String methodName = "getMainShoppingListItems";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ShoppingListResultSet> shoppingListResultSet = null;
		try
		{
			log.info(" Request received from Userid ", userId);

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingList");
			simpleJdbcCall.returningResultSet("productDetailslst", new BeanPropertyRowMapper<ShoppingListResultSet>(ShoppingListResultSet.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue(ApplicationConstants.USERID, userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (null != resultFromProcedure)
			{

				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{

					shoppingListResultSet = (ArrayList<ShoppingListResultSet>) resultFromProcedure.get("productDetailslst");

				}
				else
				{

					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_MasterShoppingList Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}
		}
		catch (DataAccessException exception)
		{
			log.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return shoppingListResultSet;
	}

	/**
	 * The DAO method for Adding the product to Main Shopping List.
	 * 
	 * @param addSLRequest
	 *            The XML with Add to Shopping List request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public ProductDetails addProductMainToShopList(AddSLRequest addSLRequest) throws ScanSeeException
	{

		String userId;
		final String methodName = "addProductMainToShopList in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		@SuppressWarnings("unused")
		final List<RetailerInfo> retailerDetailsList;
		MapSqlParameterSource scanQueryParams = null;
		ArrayList<ProductDetail> shoppingListResultSet = null;
		ProductDetails productList = null;

		Integer productExits = null;
		final List<ProductDetail> productDetails = addSLRequest.getProductDetails();
		try
		{
			userId = addSLRequest.getUserId();

			if (null != productDetails && !productDetails.isEmpty())
			{
				if (null != addSLRequest.getIsWishlst())
				{
					final String userProductIds = Utility.getCommaSepartedUserProdIdValues(productDetails);
					log.info("adding products from wish list {} to Master Shopping List for UserId {}", userProductIds, userId);
					simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
					simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
					simpleJdbcCall.withProcedureName("usp_WishListAddToMSL");
					simpleJdbcCall.returningResultSet("userProdId", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

					scanQueryParams = new MapSqlParameterSource();
					scanQueryParams.addValue("UserID", userId);
					scanQueryParams.addValue("UserProductID", userProductIds);
					scanQueryParams.addValue("MasterListAddDate", Utility.getFormattedDate());
					scanQueryParams.addValue("UserProductID", userProductIds);
					//for user tracking
					scanQueryParams.addValue(ApplicationConstants.MAINMENUID, addSLRequest.getMainMenuID());
					productList = new ProductDetails();
				}
				else
				{
					final String idString = Utility.getCommaSepartedValues(productDetails);
					simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
					simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
					simpleJdbcCall.withProcedureName("usp_MasterShoppingListSearchAddProduct");
					simpleJdbcCall.returningResultSet("userProdId", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

					scanQueryParams = new MapSqlParameterSource();
					scanQueryParams.addValue("UserID", userId);
					scanQueryParams.addValue("ProductID", idString);
					//for user tracking
					scanQueryParams.addValue(ApplicationConstants.MAINMENUID, addSLRequest.getMainMenuID());

					scanQueryParams.addValue("MasterListAddDate", Utility.getFormattedDate());
					productList = new ProductDetails();
				}

				final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

				if (null != resultFromProcedure)
				{

					if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
					{
						shoppingListResultSet = (ArrayList<ProductDetail>) resultFromProcedure.get("userProdId");

						if (null != shoppingListResultSet && !shoppingListResultSet.isEmpty())
						{
							productList = new ProductDetails();
							productList.setProductDetail(shoppingListResultSet);
							productExits = (Integer) resultFromProcedure.get("ProductExists");
							if (productExits != null)
							{
								if (productExits == 1)
								{
									productList.setProductIsThere(1);
								}
								else
								{
									productList.setProductIsThere(0);
								}
							}

						}
					}
					else
					{
						final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
						final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
						log.error(
								"Error occurred in usp_WishListAddToMSL or usp_MasterShoppingListSearchAddProduct Store Procedure error number: {} and error message: {}",
								errorNum, errorMsg);
						throw new ScanSeeException(errorMsg);

					}

				}

			}
		}
		catch (ParseException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());
		}
		catch (DataAccessException exception)
		{
			log.error("error occuredd in addProductToShopList", exception);
			throw new ScanSeeException(exception.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productList;
	}

	/**
	 * The method for Adding to Shopping List.
	 * 
	 * @param addSLRequest
	 *            The XML with delete from Shopping List request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */

	@Override
	public String dleteProductMainShopList(AddSLRequest addSLRequest) throws ScanSeeException
	{

		final String methodName = "dleteProductMainShopList";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String userId;
		Integer fromProc = 0;
		String response = null;
		try
		{
			userId = addSLRequest.getUserId();
			final List<ProductDetail> productDetails = addSLRequest.getProductDetails();
			if (null != productDetails && !productDetails.isEmpty())
			{
				final String idString = Utility.getCommaSepartedUserProdIdValues(productDetails);
				log.info("Removing products {} from Main Shopping List for User Id {}", idString, userId);
				simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
				simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
				simpleJdbcCall.withProcedureName("usp_MasterShoppingListSearchDeleteProduct");
				final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
				scanQueryParams.addValue("UserID", userId);
				scanQueryParams.addValue("UserProductID", idString);
				scanQueryParams.addValue("MasterListRemoveDate", Utility.getFormattedDate());
				final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

				fromProc = (Integer) resultFromProcedure.get("Status");
				if (fromProc == 0)
				{
					response = ApplicationConstants.SUCCESS;
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_MasterShoppingListSearchDeleteProduct Store Procedure error number: {} and error message: {}",
							errorNum, errorMsg);

					response = ApplicationConstants.FAILURE;
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (ParseException exception)
		{
			log.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());
		}
		catch (DataAccessException exception)
		{
			log.error("error occuredd in dleteProductToShopList", exception);
			throw new ScanSeeException(exception);

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method for fetching Shopping Cart Products.
	 * 
	 * @param userId
	 *            the userID in the request.
	 * @param productName
	 *            The product name in the request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object getShoppingCartProds(int userId, String productName) throws ScanSeeException
	{

		final String methodName = "getShoppingCartProds in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productDetailList = null;
		ProductDetails productDetails = null;

		try
		{
			productDetails = null;
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListSearch");
			simpleJdbcCall.returningResultSet("scproducts", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue(ApplicationConstants.USERID, userId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("scproducts");
		}

		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		if (null != productDetailList && !productDetailList.isEmpty())
		{
			productDetails = new ProductDetails();
			productDetails.setProductDetail(productDetailList);
			log.info(ApplicationConstants.METHODEND + methodName);
			return productDetails;
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * The DAO method to get the Products for adding to shopping list from the
	 * database based on the given userId and productName.
	 * 
	 * @param userId
	 *            requested user.
	 * @param productSearchKey
	 *            search key for search.
	 * @return ProductDetails with list of products.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public ProductDetails getProductsForShopping(int userId, String productSearchKey) throws ScanSeeException
	{

		final String methodName = "getProductsForShopping in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<ProductDetail> productDetailList = null;
		ProductDetails productDetails = null;

		try
		{
			productDetails = null;
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListSearch").returningResultSet("products",
					ParameterizedBeanPropertyRowMapper.newInstance(ProductDetail.class));
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserId", userId);
			scanQueryParams.addValue("ProdSearch", productSearchKey);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			productDetailList = (List<ProductDetail>) resultFromProcedure.get("products");

		}

		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		if (null != productDetailList && !productDetailList.isEmpty())
		{
			productDetails = new ProductDetails();
			productDetails.setProductDetail(productDetailList);
			log.info(ApplicationConstants.METHODEND + methodName);
			return productDetails;
		}
		else
		{
			log.info(ApplicationConstants.METHODEND + methodName);
			return productDetails;
		}

	}

	/**
	 * The method fetches FindNear by Retailers Details.
	 * 
	 * @param thisLocationRequest
	 *            as the request parameter
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<ShoppingListResultSet> fetchNearByDetails(ThisLocationRequest thisLocationRequest) throws ScanSeeException
	{

		final String methodName = "fetchNearByDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ShoppingListResultSet> shoppingListResultSet = null;
		try
		{
			log.info("Fetching fetchNearByDetails for the user id {}", thisLocationRequest.getUserId());

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ShoppingNearByRetailer");
			simpleJdbcCall.returningResultSet("findNearByDetails", new BeanPropertyRowMapper<ShoppingListResultSet>(ShoppingListResultSet.class));

			final MapSqlParameterSource fetchFindNearRetailersParameters = new MapSqlParameterSource();
			fetchFindNearRetailersParameters.addValue("UserID", thisLocationRequest.getUserId());
			fetchFindNearRetailersParameters.addValue("ProductId", thisLocationRequest.getProductId());
			fetchFindNearRetailersParameters.addValue("Latitude", thisLocationRequest.getLatitude());
			fetchFindNearRetailersParameters.addValue("Longitude", thisLocationRequest.getLongitude());
			fetchFindNearRetailersParameters.addValue("Radius", thisLocationRequest.getPreferredRadius());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchFindNearRetailersParameters);
			shoppingListResultSet = (ArrayList<ShoppingListResultSet>) resultFromProcedure.get("findNearByDetails");
		}

		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		log.info(ApplicationConstants.METHODEND + methodName);

		return shoppingListResultSet;
	}

	/**
	 * The method used for adding UnassignedProduct.
	 * 
	 * @param productDetailsRequest
	 *            The xml with ProductDetails Request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public ProductDetails addUnassignedPro(ProductDetailsRequest productDetailsRequest) throws ScanSeeException
	{
		final String methodName = "addUnassignedPro in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ProductDetail> productDetailsResultSet = null;
		ProductDetails productList = null;
		try
		{
			/*
			 * String toAdd = productDetailsRequest.getAddedTo(); if(toAdd
			 * !=null) { if ("Favourite".equalsIgnoreCase(toAdd)) { toAdd = "F";
			 * } else { toAdd = "L"; } }
			 */
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListUnassignedProducts").returningResultSet("addUnassigned",
					ParameterizedBeanPropertyRowMapper.newInstance(ProductDetail.class));
			log.info("Adding unassignd product {} and Description {} ", productDetailsRequest.getProductName(),
					productDetailsRequest.getProductDescription());
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", productDetailsRequest.getUserId());
			scanQueryParams.addValue("UnassignedProductName", productDetailsRequest.getProductName());
			scanQueryParams.addValue("UnassignedProductDescription", productDetailsRequest.getProductDescription());
			scanQueryParams.addValue("Date", Utility.getFormattedDate());
			scanQueryParams.addValue("ScreenFlag", productDetailsRequest.getAddedTo());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetailsResultSet = (ArrayList<ProductDetail>) resultFromProcedure.get("addUnassigned");
					if (null != productDetailsResultSet && !productDetailsResultSet.isEmpty())
					{
						productList = new ProductDetails();
						productList.setProductDetail(productDetailsResultSet);
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

					log.error("Error occurred in usp_MasterShoppingListUnassignedProducts Store Procedure error number: {} and error message: {}",
							errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}

		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		catch (ParseException e)
		{
			log.info("EmptyResultDataAccessException" + e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);

		return productList;

	}

	/**
	 * For displaying today shopping list products.
	 * 
	 * @param userId
	 *            as request
	 * @return ArrayList of items from Today's Shopping list.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<MainSLRetailerCategory> getTodaySLProducts(int userId) throws ScanSeeException
	{

		final String methodName = "getTodaySLProducts";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<MainSLRetailerCategory> mainSLRetailerCategorylst = null;

		try
		{
			log.info(" Request received from Userid {}", userId);

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_TodayShoppingListRetailerList");
			simpleJdbcCall.returningResultSet("productDetails", new BeanPropertyRowMapper<MainSLRetailerCategory>(MainSLRetailerCategory.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					mainSLRetailerCategorylst = (ArrayList<MainSLRetailerCategory>) resultFromProcedure.get("productDetails");
					if (null != mainSLRetailerCategorylst && !mainSLRetailerCategorylst.isEmpty())
					{
						log.info("mainSLRetailerCategorylst");

					}
					else
					{
						log.info("No Products are found for the search");
						return mainSLRetailerCategorylst;
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_MasterShoppingListRetailerList Store Procedure error number: {} " + errorNum
							+ " and error message is: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}

		}
		catch (DataAccessException exception)
		{
			log.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return mainSLRetailerCategorylst;

	}

	/**
	 * The method fetches ShoppingBasketProduct Details.
	 * 
	 * @param userId
	 *            as the request parameter
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<ShoppingListResultSet> fetchShoppingBasketProds(int userId) throws ScanSeeException
	{

		final String methodName = "fetchShoppingBasketProds in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ShoppingListResultSet> shoppingListResultSet = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ShoppingCartDispaly");
			simpleJdbcCall
					.returningResultSet("slbasketproductDetails", new BeanPropertyRowMapper<ShoppingListResultSet>(ShoppingListResultSet.class));

			final SqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource().addValue("UserID", userId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{

					shoppingListResultSet = (ArrayList<ShoppingListResultSet>) resultFromProcedure.get("slbasketproductDetails");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

					log.error("Error occurred in usp_ShoppingCartDispaly Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return shoppingListResultSet;
	}

	/**
	 * For displaying shopping basket product based on userId.
	 * 
	 * @param objUTData requested UserTrackingData.
	 * @param strApplnConst requested StringConstant.
	 * @return AddRemoveSBProducts list of cart products.
	 * @throws ScanSeeException
	 *             for exception
	 */

	@SuppressWarnings("unchecked")
	@Override
	public AddRemoveSBProducts getShopBasketProducts(UserTrackingData objUTData, String strApplnConst) throws ScanSeeException
	{
		final String methodName = "getShopBasketProducts in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		AddRemoveSBProducts addRemoveSBProducts =  new AddRemoveSBProducts();
		List<ProductDetail> productDetailList = null;
		ShoppingCartProducts cartProducts = null;
		ShoppingBasketProducts basketProducts = null;
		MapSqlParameterSource objTodayShopListParams = null;
		Boolean bNextPage = false;
		Integer iMaxCount = null;
		Map<String, Object> resultFromProcedure = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_TodayShoppingListCategoryProduct");
			simpleJdbcCall.returningResultSet("products", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			objTodayShopListParams = new MapSqlParameterSource();
			objTodayShopListParams.addValue("UserId", objUTData.getUserID());
			objTodayShopListParams.addValue("LowerLimit", objUTData.getLastVisited());
			objTodayShopListParams.addValue("ScreenName", strApplnConst);
			resultFromProcedure = simpleJdbcCall.execute(objTodayShopListParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetailList = (List<ProductDetail>) resultFromProcedure.get("products");
					final HashMap<String, ArrayList<ProductDetail>> productsCategorisedByRetailer = new HashMap<String, ArrayList<ProductDetail>>();
					// these products should be group by Category and set the
					// products to category and set category list to
					// AddRemoveSBProducts
					// Key is parent category Name and value is respective
					// products
					// details
					if (null != productDetailList && !productDetailList.isEmpty())
					{
						iMaxCount = (Integer) resultFromProcedure.get("MaxCnt");
						bNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						addRemoveSBProducts.setMaxCount(iMaxCount);
						if (bNextPage== true) {
							addRemoveSBProducts.setNextPage(1);
						} else {
							addRemoveSBProducts.setNextPage(0);
						}
						for (ProductDetail productDetail : productDetailList)
						{
							if (productsCategorisedByRetailer.containsKey(productDetail.getParentCategoryName()))
							{
								
								final ArrayList<ProductDetail> prodDetails = productsCategorisedByRetailer.get(productDetail.getParentCategoryName());
								prodDetails.add(productDetail);
							} else {
								final ArrayList<ProductDetail> productDetailsList = new ArrayList<ProductDetail>();
								productDetailsList.add(productDetail);
								productsCategorisedByRetailer.put(productDetail.getParentCategoryName(), productDetailsList);
							}
						}
						cartProducts = new ShoppingCartProducts();
						final ArrayList<Category> categorylist = new ArrayList<Category>();
						Category categoryDetails = null;
						ProductDetail obj = null;
						for (Map.Entry e : productsCategorisedByRetailer.entrySet())
						{
							categoryDetails = new Category();
							categoryDetails.setParentCategoryName(e.getKey().toString());

							categoryDetails.setProductDetails((ArrayList<ProductDetail>) e.getValue());
							obj = ((ArrayList<ProductDetail>) e.getValue()).get(0);
							categoryDetails.setCategoryId(obj.getCategoryID());
							categorylist.add(categoryDetails);
						}
						cartProducts.setCategoryDetails(categorylist);
					}
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_TodayShoppingListCategoryProduct Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
			//both cart and list product have been inlcuded in the procedure above so below code is commented. (Manju)
			/*if(objUTData.getLastVisited()!=0){
				simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
				simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
				simpleJdbcCall.withProcedureName("usp_ShoppingCartCategoryProduct");
				simpleJdbcCall.returningResultSet("products", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
				objTodayShopListParams = new MapSqlParameterSource();
				objTodayShopListParams.addValue("UserID", objUTData.getUserID());
				resultFromProcedure = simpleJdbcCall.execute(objTodayShopListParams);
				if (null != resultFromProcedure)
				{
					if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
					{
						productDetailList = (List<ProductDetail>) resultFromProcedure.get("products");
						final HashMap<String, ArrayList<ProductDetail>> productsCategorisedByRetailer = new HashMap<String, ArrayList<ProductDetail>>();
						if (null != productDetailList && !productDetailList.isEmpty())
						{
							for (ProductDetail productDetail : productDetailList)
							{
								if (productsCategorisedByRetailer.containsKey(productDetail.getParentCategoryName()))
								{
									final ArrayList<ProductDetail> prodDetails = productsCategorisedByRetailer.get(productDetail.getParentCategoryName());
									prodDetails.add(productDetail);
								} else {
									final ArrayList<ProductDetail> productDetailsList = new ArrayList<ProductDetail>();
									productDetailsList.add(productDetail);
									productsCategorisedByRetailer.put(productDetail.getParentCategoryName(), productDetailsList);
								}
							}
							final ArrayList<Category> categorylist = new ArrayList<Category>();
							Category categoryDetails = null;
							ProductDetail obj = null;
							for (Map.Entry e : productsCategorisedByRetailer.entrySet())
							{
								categoryDetails = new Category();
								categoryDetails.setParentCategoryName(e.getKey().toString());
								obj = ((ArrayList<ProductDetail>) e.getValue()).get(0);
								
								categoryDetails.setCategoryId(obj.getCategoryID());
								categoryDetails.setProductDetails((ArrayList<ProductDetail>) e.getValue());
								categorylist.add(categoryDetails);
								
							}
							basketProducts = new ShoppingBasketProducts();
							basketProducts.setCategoryDetails(categorylist);
						}
					}
					else
					{
						final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
						final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
						
						log.error("Error occurred in usp_ShoppingCartDispaly Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
						throw new ScanSeeException(errorMsg);
					}
				}
			}*/


			if (null != basketProducts)
			{
				
				addRemoveSBProducts.setBasketProducts(basketProducts);
			}

			if (null != cartProducts)
			{
				if (null == addRemoveSBProducts)
				{
					addRemoveSBProducts = new AddRemoveSBProducts();
				}
				addRemoveSBProducts.setCartProducts(cartProducts);
			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return addRemoveSBProducts;
	}

	/**
	 * Method for removing rebate from gallery.
	 * 
	 * @param addRemObj
	 *            xml with Add product to Shopping Basked(or removing)
	 * @return response xml with success or failure error code.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	@Override
	public String addRemoveSBProducts(AddRemoveSBProducts addRemObj) throws ScanSeeException
	{

		final String methodName = "addRemoveSBProducts in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = 0;
		String response = null;
		ProductDetail productDetails = null;
		String userCartProId = null;
		String userBaskProId = null;
		try
		{
			final ShoppingBasketProducts basketProducts = addRemObj.getBasketProducts();
			final ShoppingCartProducts cartProducts = addRemObj.getCartProducts();
			if (null != basketProducts)
			{
				productDetails = basketProducts.getProductDetail();
				if (null != productDetails)
				{
					userCartProId = String.valueOf(productDetails.getUserProductId());
					log.info("add/remove cart producst {} for user id {} ", userCartProId, addRemObj.getUserId());
				}
			}

			if (null != cartProducts)
			{
				productDetails = cartProducts.getProductDetail();
				if (null != productDetails)
				{
					userBaskProId = String.valueOf(productDetails.getUserProductId());
					log.info("add/remove cart producst {}", userBaskProId, addRemObj.getUserId());
				}
			}

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ShoppingCartAddDelete");
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", addRemObj.getUserId());
			scanQueryParams.addValue("TodayListUserProductID", userCartProId);
			scanQueryParams.addValue("CartUserProductID", userBaskProId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_ShoppingCartAddDelete Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
				response = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * Method for removing rebate from gallery.
	 * 
	 * @param rebateRemove
	 *            xml with Remove Rebate request.
	 * @return response xml with success or failure error code.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public String removeRebate(AddRemoveCLR rebateRemove) throws ScanSeeException
	{
		final String methodName = "removeRebate in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		Integer fromProc = 0;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListDeleteRebate");
			final SqlParameterSource removeRebate = new MapSqlParameterSource().addValue("RebateID", rebateRemove.getRebateId()).addValue("UserID",
					rebateRemove.getUserId());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(removeRebate);

			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListDeleteRebate Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				response = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Error ocurred in Remove Rebate", exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * Method for adding Coupon to gallery.
	 * 
	 * @param couponAddRemove
	 *            xml with Coupon details.
	 * @return response xml with success or failure error code.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public String addCoupon(AddRemoveCLR couponAddRemove) throws ScanSeeException
	{
		final String methodName = "addCoupon in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		Integer fromProc = 0;
		try
		{

			log.info("Inside Add Coupon for User {}", couponAddRemove.getUserId());
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListAddCoupon");
			final MapSqlParameterSource addCoupon = new MapSqlParameterSource();
			addCoupon.addValue("CouponID", couponAddRemove.getCouponId());
			addCoupon.addValue("UserID", couponAddRemove.getUserId());
			addCoupon.addValue("UserCouponClaimTypeID", null);
			addCoupon.addValue("CouponPayoutMethod", null);
			addCoupon.addValue("RetailLocationID", null);
			addCoupon.addValue("CouponClaimDate", Utility.getFormattedDate());
			// Adding CouponListID for usertracking.
			addCoupon.addValue("CouponListID", couponAddRemove.getCoupListID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(addCoupon);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListAddCoupon Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				response = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());
		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * Method for adding loyalty to gallery.
	 * 
	 * @param couponAddRemove
	 *            xml with Coupon details.
	 * @return response xml with success or failure error code.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public String addLoyalty(AddRemoveCLR couponAddRemove) throws ScanSeeException
	{
		final String methodName = "addLoyalty in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = 0;
		String response;
		try
		{
			log.info("Inside Add Loyalty");
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListAddLoyalty");
			final SqlParameterSource addLoyalty = new MapSqlParameterSource().addValue("UserID", couponAddRemove.getUserId())
					.addValue("LoyaltyDealID", couponAddRemove.getloyaltyDealId()).addValue("LoyaltyDealSource", null)
					.addValue("LoyaltyDealPayoutMethod", null).addValue("LoyaltyDealRedemptionDate", Utility.getFormattedDate())
					.addValue("LoyaltyCardNumber", couponAddRemove.getCardNumber());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(addLoyalty);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListAddLoyalty Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);

		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * Method for adding rebate to gallery.
	 * 
	 * @param rebateAddRemove
	 *            xml with Rebate details.
	 * @return response xml with success or failure error code.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public String addRebate(AddRemoveCLR rebateAddRemove) throws ScanSeeException
	{

		final String methodName = "addRebate";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		Integer fromProc = 0;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListAddRebate");
			final SqlParameterSource addRebate = new MapSqlParameterSource()
					.addValue("UserID", rebateAddRemove.getUserId())
					// .addValue("ProductID",rebateAddRemove.getProductId())
					.addValue("RebateID", rebateAddRemove.getRebateId()).addValue("RetailLocationID", null).addValue("RedemptionStatusID", null)
					.addValue("PurchaseAmount", null).addValue("PurchaseDate", Utility.getFormattedDate()).addValue("ProductSerialNumber", null)
					.addValue("ScanImage", null);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(addRebate);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListAddRebate Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			response = ApplicationConstants.FAILURE;
		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * Method for removing Coupon from gallery.
	 * 
	 * @param couponAddRemove
	 *            xml with remove Coupon request.
	 * @return response xml with success or failure error code.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public String removeCoupon(AddRemoveCLR couponAddRemove) throws ScanSeeException
	{

		final String methodName = "removeCoupon";

		log.info(ApplicationConstants.METHODSTART + methodName);
		String responseFromProc = null;
		Integer fromProc = null;

		try
		{
			log.info(" Executing the Remove coupon method User ID {}", couponAddRemove.getUserId());
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListDeleteCoupon");
			final SqlParameterSource removeCoupon = new MapSqlParameterSource().addValue("CouponID", couponAddRemove.getCouponId()).addValue(
					"UserID", couponAddRemove.getUserId());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(removeCoupon);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;

			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_MasterShoppingListDeleteCoupon method ..errorNum.{} errorMsg.. {}", errorNum, errorMsg);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);

		return responseFromProc;
	}

	/**
	 * Method for removing Loyalty from gallery.
	 * 
	 * @param couponAddRemove
	 *            xml with remove loyalty request.
	 * @return response xml with success or failure error code.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public String removeLoyalty(AddRemoveCLR couponAddRemove) throws ScanSeeException
	{
		final String methodName = "removeLoyalty in DAO layer";

		String responseFromProc = null;
		Integer fromProc = null;

		log.info(ApplicationConstants.METHODSTART + methodName);

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListDeleteLoyalty");
			final SqlParameterSource removeLoyalty = new MapSqlParameterSource().addValue("LoyaltyDealID", couponAddRemove.getloyaltyDealId())
					.addValue("UserID", couponAddRemove.getUserId());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(removeLoyalty);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;

			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListDeleteLoyalty Store Procedure error number: {} and error message: {}", errorNum,
						errorMsg);
				throw new ScanSeeException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

	/**
	 * Method for fetching Coupon Info. The method is called from the service
	 * layer.
	 * 
	 * @param userId
	 *            The user id in the request.
	 * @param retailId
	 *            The retail ID in the request.
	 * @param productId
	 *            The product id in the request.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 * @return couponsDetails The xml with Coupon Details.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public CouponsDetails fetchCouponInfo(int userId, int productId, int retailId, Integer mainMenuID) throws ScanSeeException
	{

		final String methodName = "fetchCouponInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		CouponsDetails couponsDetails = null;
		List<CouponDetails> couponDetailList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListCoupon");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			Integer retailerId = retailId;
			if (0 == retailId)
			{
				retailerId = null;
			}
			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue("ProductID", productId);
			fetchCouponParameters.addValue("RetailID", retailerId);
			fetchCouponParameters.addValue("UserID", userId);
			//For user tracking
			fetchCouponParameters.addValue(ApplicationConstants.MAINMENUID, mainMenuID);
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);
			couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get("ErrorNumber"))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_MasterShoppingListCoupon method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					if (null == couponDetailList || couponDetailList.isEmpty())
					{
						return couponsDetails;
					}
					else
					{
						couponsDetails = new CouponsDetails();

						couponsDetails.setCouponDetail(couponDetailList);
					}
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return couponsDetails;

	}

	/**
	 * Method for fetching Loyalty Info. The method is called from the service
	 * layer.
	 * 
	 * @param userId
	 *            The user id in the request.
	 * @param retailId
	 *            The retail ID in the request.
	 * @param productId
	 *            The product id in the request.
	 * @return loyaltyDetails The xml with loyaltyDetails.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public LoyaltyDetails fetchLoyaltyInfo(int userId, int productId, int retailId, Integer mainMenuID) throws ScanSeeException
	{
		final String methodName = "fetchLoyaltyInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<LoyaltyDetail> loyaltyDetailList;
		LoyaltyDetails loyaltyDetails = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListLoyalty");
			simpleJdbcCall.returningResultSet("SLLoyaltyDetails", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final MapSqlParameterSource fetchLoyaltyParameters = new MapSqlParameterSource();
			fetchLoyaltyParameters.addValue("ProductID", productId);
			fetchLoyaltyParameters.addValue("RetailID", retailId);
			fetchLoyaltyParameters.addValue("UserID", userId);
			fetchLoyaltyParameters.addValue(ApplicationConstants.MAINMENUID, mainMenuID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_MasterShoppingListLoyalty method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else
			{
				loyaltyDetailList = (List<LoyaltyDetail>) resultFromProcedure.get("SLLoyaltyDetails");
				if (null != loyaltyDetailList && !loyaltyDetailList.isEmpty())
				{

					loyaltyDetails = new LoyaltyDetails();
					loyaltyDetails.setLoyaltyDetail(loyaltyDetailList);
				}
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return loyaltyDetails;

	}

	/**
	 * Method for fetching Rebate Info. The method is called from the service
	 * layer.
	 * 
	 * @param userId
	 *            The user id in the request.
	 * @param retailId
	 *            The retail ID in the request.
	 * @param productId
	 *            The product id in the request.
	 * @return rebateDetails The xml with rebateDetails.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public RebateDetails fetchRebateInfo(int userId, int productId, int retailId, Integer mainMenuID) throws ScanSeeException
	{
		final String methodName = "fetchRebateInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<RebateDetail> rebateDetailList;
		RebateDetails rebateDetails = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListRebate");
			simpleJdbcCall.returningResultSet("SLRebateDetails", new BeanPropertyRowMapper<RebateDetail>(RebateDetail.class));

			final MapSqlParameterSource fetchRebateParameters = new MapSqlParameterSource();
			fetchRebateParameters.addValue("ProductID", productId);
			fetchRebateParameters.addValue("RetailID", retailId);
			fetchRebateParameters.addValue("UserID", userId);
			//For user tracking
			fetchRebateParameters.addValue(ApplicationConstants.MAINMENUID, mainMenuID);
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRebateParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_MasterShoppingListRebate  ..errorNum.{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);

			}
			else
			{
				rebateDetailList = (List<RebateDetail>) resultFromProcedure.get("SLRebateDetails");
				if (null != rebateDetailList && !rebateDetailList.isEmpty())
				{
					rebateDetails = new RebateDetails();
					rebateDetails.setRebateDetail(rebateDetailList);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return rebateDetails;
	}

	/**
	 * The method for adding to or removing from SL.
	 * 
	 * @param addSLRequest
	 *            The XML with request for Add to SL(or remove from SL).
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */

	@Override
	public String addRemoveTodaySLProducts(AddSLRequest addSLRequest) throws ScanSeeException
	{

		final String methodName = "addRemoveTodaySLProducts in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String userId;
		Integer productExits = null;
		Integer exists = null;
		try
		{
			userId = addSLRequest.getUserId();
			final List<ProductDetail> productDetails = addSLRequest.getProductDetails();

			if (null != productDetails && !productDetails.isEmpty())
			{
				if (null != addSLRequest.getIsaddToTSL())
				{
					if (addSLRequest.getIsaddToTSL())
					{
						final String idString = Utility.getCommaSepartedUserProdIdValues(productDetails);
						log.info("Executing addRemoveTodaySLProducts for Products ID {} for UserID", idString, userId);
						simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
						simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
						simpleJdbcCall.withProcedureName("usp_MasterShoppingListAddTSL");
						final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
						scanQueryParams.addValue("UserID", userId);
						scanQueryParams.addValue("UserProductID", idString);
						scanQueryParams.addValue("TodayListAddDate", Utility.getFormattedDate());

						@SuppressWarnings("unused")
						final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

						productExits = (Integer) resultFromProcedure.get("ProductExists");
						exists = (Integer) resultFromProcedure.get("Exists");
						if (productExits == null && exists == null)
						{
							response = ApplicationConstants.SUCCESS;
						}
						if (productExits != null)
						{
							if (productExits == 1)
							{
								response = ApplicationConstants.SLADDPRODTLEXISTS;
							}
							else
							{
								response = ApplicationConstants.SUCCESS;
							}
						}
						if (exists != null)
						{
							if (exists == 1)
							{
								response = ApplicationConstants.FAILURE;
							}
							else
							{
								response = ApplicationConstants.SUCCESS;
							}
						}

					}
				}
				else
				{
					final String idString = Utility.getCommaSepartedUserProdIdValues(productDetails);
					log.info("Executing addRemoveTodaySLProducts for Products ID {} for User ID", idString, userId);
					simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
					simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
					simpleJdbcCall.withProcedureName("usp_TodayListAddDelete");
					final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
					scanQueryParams.addValue("UserID", userId);
					scanQueryParams.addValue("UserProductId", idString);
					@SuppressWarnings("unused")
					final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
					response = ApplicationConstants.SUCCESS;
				}

			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			return ApplicationConstants.FAILURE;
		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method fetches ShoppingCartProduct Details.
	 * 
	 * @param userId
	 *            as the request parameter
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception of type SQL Exception.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<ShoppingListResultSet> getTodaysNMasterSL(int userId) throws ScanSeeException
	{

		final String methodName = "getTodaysNMasterSL";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ShoppingListResultSet> shoppingListResultSet = null;

		try
		{
			log.info("Executing getTodaysNMasterSL method for user ID {} ", userId);

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_TodayListDisplay");
			simpleJdbcCall.returningResultSet("productDetails", new BeanPropertyRowMapper<ShoppingListResultSet>(ShoppingListResultSet.class));

			final SqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource().addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsgis = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in removeHDProduct method ..errorNum..." + errorNum + "errorMsg.." + errorMsgis);
			}

			shoppingListResultSet = (ArrayList<ShoppingListResultSet>) resultFromProcedure.get("productDetails");

		}

		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return shoppingListResultSet;
	}

	/**
	 * The method for adding Shopping list User Notes.
	 * 
	 * @param userNotesDetails
	 *            as the request parameter
	 * @return Xml as the response.
	 * @throws ScanSeeException
	 *             of type SQL Exception.
	 */
	@Override
	public String addUserNotesDetails(UserNotesDetails userNotesDetails) throws ScanSeeException
	{

		final String methodName = "addUserNotesDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		Map<String, Object> resultFromProcedure = null;
		String response = null;
		Integer fromProc = null;

		try
		{
			log.info("Inside addUserNotesDetails method...");

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_TodayListNote");
			simpleJdbcCall.returningResultSet("addNotesDetails", new BeanPropertyRowMapper<UserNotesDetails>(UserNotesDetails.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", userNotesDetails.getUserId());
			scanQueryParams.addValue("Note", userNotesDetails.getUserNotes());
			scanQueryParams.addValue("NoteAddDate", Utility.getFormattedDate());
			resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_TodayListNote  ..errorNum...{} errorMsg", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}

		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		catch (ParseException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method for deleting Shopping list User Notes.
	 * 
	 * @param userId
	 *            The user ID in the request.
	 * @return Xml as the response.
	 * @throws ScanSeeException
	 *             of type SQL Exception.
	 */
	@Override
	public String deleteUserNotesDetails(int userId) throws ScanSeeException
	{

		final String methodName = "deleteUserNotesDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String responseFromProc = null;
		Integer fromProc = null;

		try
		{
			log.info("Inside addUserNotesDetails UserID {} ", userId);
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_TodayListNoteDelete");
			simpleJdbcCall.returningResultSet("NotesDetails", new BeanPropertyRowMapper<UserNotesDetails>(UserNotesDetails.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_TodayListNoteDelete  ..errorNum. {} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		}

		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

	/**
	 * The method for adding Shopping list User Notes.
	 * 
	 * @param userID
	 *            as the request parameter
	 * @return Xml as the response.
	 * @throws ScanSeeException
	 *             of type SQL Exception.
	 */
	@Override
	public UserNotesDetails getUserNotesDetails(int userID) throws ScanSeeException
	{

		final String methodName = "getUserNotesDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		UserNotesDetails userNotesDetails = null;
		try
		{
			log.info("Executing  UserNotesDetails UserID {} ", userID);
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			userNotesDetails = simpleJdbcTemplate.queryForObject(ShoppingListQueries.SHOPPINGUSERNOTESQUERY,
					new BeanPropertyRowMapper<UserNotesDetails>(UserNotesDetails.class), userID);
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return userNotesDetails;
	}

	/**
	 * method for fetching FindNearBy Info.
	 * 
	 * @param userID
	 *            The user id in request.
	 * @param latitude
	 *            The latitude in request.
	 * @param productId
	 *            The product Id in request.
	 * @param longitude
	 *            The longitude in request.
	 * @param postalCode
	 *            The postal code for which lowest price information is to be
	 * @param radius
	 *            The radius in request
	 * @return List of type FindNearByDetail objects. The method is called from
	 *         the service layer.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application
	 */

	@SuppressWarnings("unchecked")
	@Override
	public FindNearByDetails fetchNearByInfo(int userID, Integer productId, String latitude, String longitude, String postalCode, String radius, Integer mainMenuID)
			throws ScanSeeException
	{

		final String methodName = "fetchNearByInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		FindNearByDetails findNearByDetails = null;
		List<FindNearByDetail> findNearByDetaillst = null;
		Integer dbresult = 0;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FindNearByRetailer");
			simpleJdbcCall.returningResultSet("SLfetchNearByInfo", new BeanPropertyRowMapper<FindNearByDetail>(FindNearByDetail.class));

			final MapSqlParameterSource fetchNearByParameters = new MapSqlParameterSource();
			fetchNearByParameters.addValue("UserID", userID);
			fetchNearByParameters.addValue("ProductId", productId);
			fetchNearByParameters.addValue("Latitude", latitude);
			fetchNearByParameters.addValue("Longitude", longitude);
			fetchNearByParameters.addValue("PostalCode", postalCode);
			fetchNearByParameters.addValue("Radius", radius);
			//For user tracking
			fetchNearByParameters.addValue(ApplicationConstants.MAINMENUID, mainMenuID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchNearByParameters);

			if (null != resultFromProcedure)
			{
				dbresult = (Integer) resultFromProcedure.get("Result");
				if (dbresult != null)
				{
					if (dbresult == -1)
					{
						findNearByDetails = new FindNearByDetails();
						findNearByDetails.setFindNearBy(true);
						return findNearByDetails;
					}
				}
				findNearByDetaillst = (List<FindNearByDetail>) resultFromProcedure.get("SLfetchNearByInfo");
				if (findNearByDetaillst != null && !findNearByDetaillst.isEmpty())
				{
					findNearByDetails = new FindNearByDetails();
					findNearByDetails.setFindNearByDetail(findNearByDetaillst);
				}
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return findNearByDetails;
	}

	/**
	 * The method for fetching Lowest price for the product and total retailers
	 * where product is available.
	 * 
	 * @param userID
	 *            The userId in the user request.
	 * @param latitude
	 *            The latitude value.
	 * @param longitude
	 *            The longitude value.
	 * @param productId
	 *            The product ID for which lowest price information is to be
	 * @param postalcode
	 *            The postal code for which lowest price information is to be
	 *            displayed.
	 * @param radius
	 *            The radius specified by the user.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application
	 * @return findNearByLowestPrice The xml with NearBy-Lowest Price Info.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public FindNearByIntactResponse fetchNearByLowestPrice(int userID, Double latitude, int productId, Double longitude, String postalcode,
			double radius, Integer mainMenuID) throws ScanSeeException
	{
		final String methodName = "fetchNearByLowestPrice in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		int findNearByCount;
		String findNearByLowest;

		final FindNearByIntactResponse findNearByDetailsResultSet = new FindNearByIntactResponse();
		FindNearByPartialResponse findNearByPartialResponse = null;
		ArrayList<FindNearByResponse> findNearByDetailList = null;
		Integer dbresult = 0;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FindNearByRetailer");
			simpleJdbcCall.returningResultSet("FindNearByInfo", new BeanPropertyRowMapper<FindNearByResponse>(FindNearByResponse.class));

			final MapSqlParameterSource fetchNearByParameters = new MapSqlParameterSource();
			fetchNearByParameters.addValue("UserID", userID);
			fetchNearByParameters.addValue("Latitude", latitude);
			fetchNearByParameters.addValue("Longitude", longitude);
			fetchNearByParameters.addValue("ProductId", productId);
			fetchNearByParameters.addValue("PostalCode", postalcode);
			fetchNearByParameters.addValue("Radius", radius);
			//For user tracking
			fetchNearByParameters.addValue(ApplicationConstants.MAINMENUID, mainMenuID);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchNearByParameters);

			if (null != resultFromProcedure)
			{

				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{

					dbresult = (Integer) resultFromProcedure.get("Result");
					if (dbresult != null)
					{
						if (dbresult == -1)
						{

							findNearByDetailsResultSet.setFindNearBy(true);
							return findNearByDetailsResultSet;
						}
					}

					findNearByDetailList = (ArrayList<FindNearByResponse>) resultFromProcedure.get("FindNearByInfo");
					if (null != findNearByDetailList && !findNearByDetailList.isEmpty())
					{
						findNearByCount = findNearByDetailList.size();
						findNearByLowest = findNearByDetailList.get(0).getProductPrice();
						findNearByPartialResponse = new FindNearByPartialResponse();
						findNearByPartialResponse.setLowestPrice(findNearByLowest);
						findNearByPartialResponse.setTotalLocations(String.valueOf(findNearByCount) + ApplicationConstants.STORES);
						findNearByPartialResponse.setImagePath(findNearByDetailList.get(0).getImagePath());

						findNearByDetailsResultSet.setFindNearByDetails(findNearByDetailList);
						findNearByDetailsResultSet.setFindNearByMetaData(findNearByPartialResponse);

					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);

					log.error("Error occurred in usp_FindNearByRetailer Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}

		}

		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return findNearByDetailsResultSet;
	}

	/**
	 * The method clears out the Shopping Basket.
	 * 
	 * @param userID
	 *            The userID entered by User.
	 * @param cartLatitude
	 *            The Latitude value(required for entry in
	 *            UserShoppingCartHistoryProduct table).
	 * @param cartLongitude
	 *            The Longitude value (required for entry in
	 *            UserShoppingCartHistoryProduct table).
	 * @return String with success or error code based on success or failure of
	 *         operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application
	 */

	@Override
	public String checkOutFromBasket(int userID, float cartLatitude, float cartLongitude) throws ScanSeeException
	{
		final String methodName = "checkOutFromBasket in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String responseFromProc = null;
		Integer fromProc = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ShoppingCartClearAll");
			final MapSqlParameterSource checkoutParams = new MapSqlParameterSource();
			checkoutParams.addValue("UserID", userID).addValue("CartLatitude", cartLatitude).addValue("CartLongitude", cartLongitude);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(checkoutParams);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_ShoppingCartClearAll  ..errorNum.{} errorMsg{}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}

		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

	/**
	 * methods to get the products media info based on the media type from
	 * database.
	 * 
	 * @param productID
	 *            requested product.
	 * @param mediaType
	 *            type of media.
	 * @return ProductDetails with media details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ProductDetails getProductMediaDetails(int productID, String mediaType, Integer productListID) throws ScanSeeException
	{

		final String methodName = "getProductMediaDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<ProductDetail> productMediaDetaillst = null;
		ProductDetails productdetailsObj = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ProductDetailsMediaDispaly");
			simpleJdbcCall.returningResultSet("productMediaInfo", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource mediaParameters = new MapSqlParameterSource();
			if ("audio".equalsIgnoreCase(mediaType))
			{
				mediaType = "Audio Files";
			}
			else if ("video".equalsIgnoreCase(mediaType))
			{
				mediaType = "Video Files";
			}
			else
			{
				mediaType = "Other Files";
			}
			mediaParameters.addValue("ProductID", productID);
			mediaParameters.addValue("MediaType", mediaType);
			//for user tracking
			mediaParameters.addValue(ApplicationConstants.PRODUCTLISTID, productListID);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(mediaParameters);

			if (null != resultFromProcedure)
			{
				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in usp_HotDealSearch Store Procedure error number: {}and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}
				else
				{
					productMediaDetaillst = (List<ProductDetail>) resultFromProcedure.get("productMediaInfo");
					if (productMediaDetaillst != null && !productMediaDetaillst.isEmpty())
					{
						productdetailsObj = new ProductDetails();
						productdetailsObj.setMediaType(mediaType);
						productdetailsObj.setProductDetail(productMediaDetaillst);
					}
				}

			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productdetailsObj;
	}

	/**
	 * methods to get the external API List.
	 * 
	 * @param moduleName
	 *            requested moduleName.
	 * @return externalAPIList
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	@SuppressWarnings("unchecked")
	public ArrayList<ExternalAPIVendor> getExternalAPIList(String moduleName) throws ScanSeeException
	{

		final String methodName = "getExternalAPIList";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ExternalAPIVendor> externalAPIList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);

			// TODO:Change this for constant
			if ("FindOnlineStores".equals(moduleName) || "Find".equals(moduleName))
			{
				simpleJdbcCall.withProcedureName("usp_GetAPIListOnline");
			}
			else
			{
				simpleJdbcCall.withProcedureName("usp_GetAPIList");
			}

			simpleJdbcCall.returningResultSet("externalAPIListInfo", new BeanPropertyRowMapper<ExternalAPIVendor>(ExternalAPIVendor.class));

			final SqlParameterSource externalAPIListParameters = new MapSqlParameterSource().addValue("prSubModule", moduleName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			externalAPIList = (ArrayList<ExternalAPIVendor>) resultFromProcedure.get("externalAPIListInfo");

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get("ErrorNumber"));
				log.error("Error occurred in usp_GetAPIList Store Procedure error number: {}" + errorNum + " and error message:{}");
				throw new ScanSeeException(errorMsg);
			}
			else if (null != externalAPIList && !externalAPIList.isEmpty())
			{
				return externalAPIList;
			}

		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return externalAPIList;
	}

	/**
	 * methods to get the external API Input parameter List.
	 * 
	 * @param apiUsageID
	 *            requested APIUsageID.
	 * @param moduleName
	 *            requested moduleName.
	 * @return externalAPIInputParameters
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	@SuppressWarnings("unchecked")
	public ArrayList<ExternalAPISearchParameters> getExternalAPIInputParameters(Integer apiUsageID, String moduleName) throws ScanSeeException
	{
		final String methodName = "getExternalAPIInputParameters";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ExternalAPISearchParameters> externalAPIInputParameters = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetAPIInputParameters");
			simpleJdbcCall.returningResultSet("externalAPIInputParameters", new BeanPropertyRowMapper<ExternalAPISearchParameters>(
					ExternalAPISearchParameters.class));

			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("prAPIUsageID", apiUsageID);
			externalAPIListParameters.addValue("PrAPISubModuleName", moduleName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			externalAPIInputParameters = (ArrayList<ExternalAPISearchParameters>) resultFromProcedure.get("externalAPIInputParameters");

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get("ErrorNumber"));
				log.error("Error occurred in usp_GetAPIList Store Procedure error number: {}" + errorNum + " and error message -->: {}");
				throw new ScanSeeException(errorMsg);
			}
			else if (null != externalAPIInputParameters && !externalAPIInputParameters.isEmpty())
			{
				return externalAPIInputParameters;
			}
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return externalAPIInputParameters;

	}

	/**
	 * methods to get product info for external API.
	 * 
	 * @param productId
	 *            requested productId.
	 * @return ProductDetail object
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	@Override
	public ProductDetail getProductInfoforExternalAPI(Integer productId) throws ScanSeeException
	{

		final String methodName = "getProductInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetail productDetail = null;

		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			productDetail = simpleJdbcTemplate.queryForObject(ShoppingListQueries.PRODUCTINOQUERY, new BeanPropertyRowMapper<ProductDetail>(
					ProductDetail.class), productId);
			if (productDetail != null)
			{
				return productDetail;
			}
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			return null;

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetail;
	}

	/**
	 * The method for fetching MSL Retailers and Category information.
	 * 
	 * @param userId
	 *            as parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<MainSLRetailerCategory> getMasterSLRetailerCategoryDetails(int userId) throws ScanSeeException
	{
		final String methodName = "getMainShoppingListItems";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<MainSLRetailerCategory> mainSLRetailerCategorylst = null;

		try
		{
			log.info(" Request received from Userid {}", userId);

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListRetailerList");
			simpleJdbcCall.returningResultSet("productDetails", new BeanPropertyRowMapper<MainSLRetailerCategory>(MainSLRetailerCategory.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					mainSLRetailerCategorylst = (ArrayList<MainSLRetailerCategory>) resultFromProcedure.get("productDetails");
					if (null != mainSLRetailerCategorylst && !mainSLRetailerCategorylst.isEmpty())
					{
						log.info("mainSLRetailerCategorylst is empty" + mainSLRetailerCategorylst);
					}
					else
					{
						log.info("No Product details found for the search");
						return mainSLRetailerCategorylst;
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_MasterShoppingListRetailerList Store Procedure error number: {} " + errorNum
							+ " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}

		}
		catch (DataAccessException exception)
		{
			log.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return mainSLRetailerCategorylst;
	}

	/**
	 * The method for fetching MSL Products information based on
	 * subcategoryname.
	 * 
	 * @param productDetailsRequestObj
	 *            as parameter
	 * @return The XML as the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings({ "unchecked", "null" })
	@Override
	public ProductDetails getMSLProcuctDetailslst(ProductDetailsRequest productDetailsRequestObj) throws ScanSeeException
	{
		final String methodName = "getMSLProcuctDetailslst in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<ProductDetail> productDetailList = null;
		ProductDetails productDetails = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListProductList");
			simpleJdbcCall.returningResultSet("FetchMSLProductsList", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("UserID", productDetailsRequestObj.getUserId());
			productQueryParams.addValue("UserRetailPreferenceID", productDetailsRequestObj.getUserRetailPreferenceID());
			productQueryParams.addValue("ParentCategoryID", productDetailsRequestObj.getParentCategoryID());
			productQueryParams.addValue("RetailID", productDetailsRequestObj.getRetailID());
			productQueryParams.addValue("LowerLimit", productDetailsRequestObj.getLastVisitedProductNo());
			productQueryParams.addValue("ScreenName", ApplicationConstants.MSLSCREENNAME);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					productDetailList = (List<ProductDetail>) resultFromProcedure.get("FetchMSLProductsList");
				}
				if (null != productDetailList && !productDetailList.isEmpty())
				{
					productDetails = new ProductDetails();
					productDetails.setProductDetail(productDetailList);
					final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					if (nextpage != null)
					{
						if (nextpage)
						{
							productDetails.setNextPage(1);
						}
						else
						{
							productDetails.setNextPage(0);
						}
					}
				}
				else
				{
					log.info("No Products found for the search");
					return productDetails;
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListProductList Store Procedure error number:  " + errorNum + " and error message: {}"
						+ errorMsg);
				throw new ScanSeeException(errorMsg);
			}

		}

		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;

	}

	/**
	 * Method for fetching Coupon ,Rebates and Loyalty Info. The method is
	 * called from the service layer.
	 * 
	 * @param userId
	 *            The user id in the request.
	 * @param retailerId
	 *            The retail ID in the request.
	 * @param productId
	 *            The product id in the request.
	 * @param lowerLimit
	 *            The lowerLimit in the request.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 * @return couponsDetails The xml with Coupon Details.
	 */

	@SuppressWarnings({ "unchecked", "null" })
	@Override
	public CLRDetails fetchMasterSLCLRDetails(CLRDetails clrDetailsObj) throws ScanSeeException
	{

		final String methodName = "fetchCouponInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;
		/**
		 * This variables needed to check nextPage is 1 or 0 for pagination.
		 */
		boolean isCouponNextPage = false;
		boolean isLoyaltyNextPage = false;
		boolean isRebateNextPage = false;

		List<RebateDetail> rebateDetailList = null;
		List<LoyaltyDetail> loyaltyDetailList = null;
		List<CouponDetails> couponDetailList = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListCoupon");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue("ProductID", clrDetailsObj.getProductId());
			fetchCouponParameters.addValue("RetailID", clrDetailsObj.getRetailerId());
			fetchCouponParameters.addValue("UserID", clrDetailsObj.getUserId());
			fetchCouponParameters.addValue("LowerLimit", clrDetailsObj.getLowerLimit());
			fetchCouponParameters.addValue("ScreenName", ApplicationConstants.SLCLRSCREENNAME);
			
			//For user tracking
			fetchCouponParameters.addValue(ApplicationConstants.MAINMENUID, clrDetailsObj.getMainMenuID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get("ErrorNumber"))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in usp_MasterShoppingListCoupon method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");
					if (null != couponDetailList && !couponDetailList.isEmpty())
					{
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						if (nextpage != null)
						{
							if (nextpage)
							{
								isCouponNextPage = true;
							}
						}
					}
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListLoyalty");
			simpleJdbcCall.returningResultSet("LoyaltyDetails", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final MapSqlParameterSource fetchLoyaltyParameters = new MapSqlParameterSource();
			fetchLoyaltyParameters.addValue("ProductID", clrDetailsObj.getProductId());
			fetchLoyaltyParameters.addValue("RetailID", clrDetailsObj.getRetailerId());
			fetchLoyaltyParameters.addValue("UserID", clrDetailsObj.getUserId());
			fetchLoyaltyParameters.addValue("LowerLimit", clrDetailsObj.getLowerLimit());
			fetchLoyaltyParameters.addValue("ScreenName", ApplicationConstants.SLCLRSCREENNAME);		
			//For user tracking
			fetchLoyaltyParameters.addValue(ApplicationConstants.MAINMENUID, clrDetailsObj.getMainMenuID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_MasterShoppingListLoyalty method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else
			{
				loyaltyDetailList = (List<LoyaltyDetail>) resultFromProcedure.get("LoyaltyDetails");
				if (null != loyaltyDetailList && !loyaltyDetailList.isEmpty())
				{
					final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					if (nextpage != null)
					{
						if (nextpage)
						{
							isRebateNextPage = true;
						}
					}

				}
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListRebate");
			simpleJdbcCall.returningResultSet("RebateDetails", new BeanPropertyRowMapper<RebateDetail>(RebateDetail.class));

			final MapSqlParameterSource fetchRebateParameters = new MapSqlParameterSource();
			fetchRebateParameters.addValue("ProductID", clrDetailsObj.getProductId());
			fetchRebateParameters.addValue("RetailID", clrDetailsObj.getRetailerId());
			fetchRebateParameters.addValue("UserID", clrDetailsObj.getUserId());
			fetchRebateParameters.addValue("LowerLimit", clrDetailsObj.getLowerLimit());
			fetchRebateParameters.addValue("ScreenName", ApplicationConstants.SLCLRSCREENNAME);
			//For user tracking
			fetchRebateParameters.addValue(ApplicationConstants.MAINMENUID, clrDetailsObj.getMainMenuID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRebateParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in usp_MasterShoppingListRebate  ..errorNum.{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else
			{
				rebateDetailList = (List<RebateDetail>) resultFromProcedure.get("RebateDetails");
				if (null != rebateDetailList && !rebateDetailList.isEmpty())
				{
					final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					if (nextpage != null)
					{
						if (nextpage)
						{
							isLoyaltyNextPage = true;
						}
					}

				}

			}

		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		if (couponDetailList != null || !couponDetailList.isEmpty() || rebateDetailList != null || !rebateDetailList.isEmpty()
				|| loyaltyDetailList != null || !loyaltyDetailList.isEmpty())
		{
			clrDetatilsObj = new CLRDetails();
			if (isCouponNextPage == true || isRebateNextPage == true || isRebateNextPage == true)
			{
				clrDetatilsObj.setNextPageFlag(1);
			}
			if (isCouponNextPage == true)
			{
				clrDetatilsObj.setClrC(1);
			}
			else
			{
				clrDetatilsObj.setClrC(0);
			}
			if (isLoyaltyNextPage == true)
			{
				clrDetatilsObj.setClrL(1);
			}
			else
			{
				clrDetatilsObj.setClrL(0);
			}
			if (isRebateNextPage == true)
			{
				clrDetatilsObj.setClrR(1);
			}
			else
			{
				clrDetatilsObj.setClrR(0);
			}
			if (couponDetailList != null && !couponDetailList.isEmpty())
			{
				clrDetatilsObj.setIsCouponthere(true);
				clrDetatilsObj.setCouponDetails(couponDetailList);
			}
			else
			{
				clrDetatilsObj.setIsCouponthere(false);
			}
			if (rebateDetailList != null && !rebateDetailList.isEmpty())
			{
				clrDetatilsObj.setIsRebatethere(true);
				clrDetatilsObj.setRebateDetails(rebateDetailList);
			}
			else
			{
				clrDetatilsObj.setIsRebatethere(false);
			}
			if (loyaltyDetailList != null && !loyaltyDetailList.isEmpty())
			{
				clrDetatilsObj.setIsLoyaltythere(true);
				clrDetatilsObj.setLoyaltyDetails(loyaltyDetailList);
			}
			else
			{
				clrDetatilsObj.setIsLoyaltythere(false);
			}

		}
		return clrDetatilsObj;
	}

	/**
	 * This method get the today Shopping list products from the database.
	 * 
	 * @param productDetailsRequestObj
	 *            as request
	 * @return xml String XMl with list of shopping List items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	@SuppressWarnings({ "unchecked", "null" })
	@Override
	public ProductDetails getTSLProcuctDetailslst(ProductDetailsRequest productDetailsRequestObj) throws ScanSeeException
	{
		final String methodName = "getTSLProcuctDetailslst in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		List<ProductDetail> productDetailList = null;
		ProductDetails productDetails = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_TodayShoppingListProductList");
			simpleJdbcCall.returningResultSet("TSLProductsList", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("UserID", productDetailsRequestObj.getUserId());
			productQueryParams.addValue("UserRetailPreferenceID", productDetailsRequestObj.getUserRetailPreferenceID());
			productQueryParams.addValue("ParentCategoryID", productDetailsRequestObj.getParentCategoryID());
			productQueryParams.addValue("RetailID", productDetailsRequestObj.getRetailID());
			productQueryParams.addValue("LowerLimit", productDetailsRequestObj.getLastVisitedProductNo());
			productQueryParams.addValue("ScreenName", ApplicationConstants.MSLSCREENNAME);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					productDetailList = (List<ProductDetail>) resultFromProcedure.get("TSLProductsList");
				}
				if (null != productDetailList && !productDetailList.isEmpty())
				{
					productDetails = new ProductDetails();
					productDetails.setProductDetail(productDetailList);
					final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					if (nextpage != null)
					{
						if (nextpage)
						{
							productDetails.setNextPage(1);
						}
						else
						{
							productDetails.setNextPage(0);
						}
					}
				}
				else
				{
					log.info("No Products found for the search");
					return productDetails;
				}
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListProductList Store Procedure with  error number: {} " + errorNum
						+ " and error message: {}" + errorMsg);
				throw new ScanSeeException(errorMsg);
			}

		}

		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;

	}

	/**
	 * This method is used to logs the error messages in the APIRealTimeLog
	 * table.
	 * 
	 * @param externalAPIErrorLog
	 *            -As object parameter
	 * @return response whether success or failure of the query execution
	 * @throws ScanSeeException
	 *             -Throws if any exception.
	 */
	@Override
	public String logExternalApiErrorMessages(ExternalAPIErrorLog externalAPIErrorLog) throws ScanSeeException
	{
		final String methodName = "insertBatchStatus";
		log.info(ApplicationConstants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

			result = this.jdbcTemplate.update("insert into APIRealTimeLog (ExecutionDate, Status,Reason,APIPartnerName) values (?, ?,?,?)",
					Utility.getFormattedCurrentDate(), 0, externalAPIErrorLog.getReason(), externalAPIErrorLog.getaPIPartnerName());

			if (result == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				response = ApplicationConstants.FAILURE;
			}
		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		return response;

	}

	/**
	 * This method get the Master Shopping list products from the database.
	 * 
	 * @param userId as request parameter.
	 * @param iLowLimit as request parameter.
	 * @param strApplnConst as request parameter.
	 * @return xml String XMl with list of shopping List items for the user.
	 * @throws ScanSeeException throws if any exception.
	 */

	@SuppressWarnings({ "unchecked", "null" })
	@Override
	public ArrayList<MainSLRetailerCategory> getMSLCategoryProcuctDetails(Integer userId, Integer iLowLimit, String strApplnConst) throws ScanSeeException
	{
		final String methodName = "getMSLCategoryProcuctDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<MainSLRetailerCategory> masterSLCategorylst = null;
		
		Boolean bNextPage = false;
		Integer iMaxCount = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListCategoryProduct");
			simpleJdbcCall.returningResultSet("MSLProductsList", new BeanPropertyRowMapper<MainSLRetailerCategory>(MainSLRetailerCategory.class));

			final MapSqlParameterSource objMasterShopListParams = new MapSqlParameterSource();
			objMasterShopListParams.addValue("UserID", userId);
			objMasterShopListParams.addValue("LowerLimit", iLowLimit);
			objMasterShopListParams.addValue("ScreenName", strApplnConst);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(objMasterShopListParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					masterSLCategorylst = (ArrayList<MainSLRetailerCategory>) resultFromProcedure.get("MSLProductsList");
					iMaxCount = (Integer) resultFromProcedure.get("MaxCnt");
					bNextPage = (Boolean) resultFromProcedure.get("NxtPageFlag");
					if (masterSLCategorylst != null && !masterSLCategorylst.isEmpty()) {
						masterSLCategorylst.get(0).setMaxCount(iMaxCount);
						if (bNextPage == true) {
							masterSLCategorylst.get(0).setNextPage(1);
						} else {
							masterSLCategorylst.get(0).setNextPage(0);
						}
					}
				}
			} else {
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_MasterShoppingListProductList Store Procedure with error number: {} " + errorNum + " and error message: {}" + errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		} catch (DataAccessException e) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return masterSLCategorylst;
	}

	/**
	 * This method is for adding today's SL product for the given product id and
	 * user id.
	 * 
	 * @param addRemObj
	 *            -AS AddSLRequest object
	 * @return ProductDetail contains sale price ,categoryid etc.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	@SuppressWarnings({ "null", "unchecked" })
	@Override
	public ProductDetail addTodaySLProductsBySearch(AddSLRequest addRemObj) throws ScanSeeException
	{
		final String methodName = "addRemoveTodaySLProducts in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		String userId;
		List<ProductDetail> productDetailList = null;
		ProductDetail prodObj = null;
		Integer productExits = null;
		try
		{
			userId = addRemObj.getUserId();
			final List<ProductDetail> productDetails = addRemObj.getProductDetails();
			if (null != productDetails && !productDetails.isEmpty())
			{
				final String idString = Utility.getCommaSepartedValues(productDetails);
				log.info("Executing addRemoveTodaySLProducts for Products ID {} for User ID", idString, userId);
				simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
				simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
				simpleJdbcCall.withProcedureName("usp_TodayShoppingListSearchAddProduct");
				simpleJdbcCall.returningResultSet("userprodId", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
				final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
				scanQueryParams.addValue("UserID", userId);
				scanQueryParams.addValue("ProductId", idString);
				scanQueryParams.addValue("TodayListAddDate", Utility.getFormattedDate());
				//For user tracking
				scanQueryParams.addValue(ApplicationConstants.MAINMENUID, addRemObj.getMainMenuID());

				final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

				if (null != resultFromProcedure)
				{
					if (null == resultFromProcedure.get("ErrorNumber"))
					{

						productDetailList = (List<ProductDetail>) resultFromProcedure.get("userprodId");
						if (null != productDetailList && !productDetailList.isEmpty())
						{
							// Store procedure will return generated
							// UserProductID for product added to TSL
							// This Id will be returned to Client for further
							// Operation

							productExits = (Integer) resultFromProcedure.get("ProductExists");
							if (productExits != null)
							{
								if (productExits == 1)
								{
									productDetailList.get(0).setProductIsThere(1);
								}
								else
								{
									productDetailList.get(0).setProductIsThere(0);
								}
							}

							prodObj = productDetailList.get(0);

						}
					}

				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_TodayShoppingListSearchAddProduct Store Procedure error number: {} " + errorNum
							+ " and error message: {}" + errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return prodObj;
	}

	/**
	 * This method is used to insert apiurls into FindNearByLog table.
	 * 
	 * @param apiURL
	 *            -As String parameter
	 * @param userId
	 *            -As int parameter
	 * @return response as failure or success
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	@Override
	public String insertExternalAPIURL(String apiURL, int userId) throws ScanSeeException
	{
		final String methodName = "insertBatchStatus";
		log.info(ApplicationConstants.METHODSTART + methodName);
		int result = 1;
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);

			result = this.jdbcTemplate.update(ShoppingListQueries.INSERTEXTERNALAPICALURL, apiURL, Utility.getFormattedCurrentDate(), userId);
			if (result == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				response = ApplicationConstants.FAILURE;
			}
		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		return response;
	}

	/**
	 * This method get the Shopping list history products from the database.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @return xml containing list of shopping List history items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	/**
	 * This is a DAO Implementation Method for fetching shopping list history
	 * products for the given userId.
	 * 
	 * @param userId
	 *            containing input information need to be fetched the Shopping
	 *            list history products .
	 * @return XML containing shopping list history products in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings({ "unchecked", "null" })
	@Override
	public ArrayList<MainSLRetailerCategory> fetchSLHistoryProducts(Integer userId, Integer lowerlimit) throws ScanSeeException
	{
		final String methodName = "fetchSLHistoryProducts in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<MainSLRetailerCategory> masterSLCategorylst = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ShoppingListHistoryDisplay");
			simpleJdbcCall.returningResultSet("MSLProductsList", new BeanPropertyRowMapper<MainSLRetailerCategory>(MainSLRetailerCategory.class));

			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("UserID", userId);
			productQueryParams.addValue("LowerLimit", lowerlimit);
			productQueryParams.addValue("ScreenName", ApplicationConstants.SLHISTORYSCREENNAME);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{

					masterSLCategorylst = (ArrayList<MainSLRetailerCategory>) resultFromProcedure.get("MSLProductsList");

					if (!masterSLCategorylst.isEmpty() && masterSLCategorylst != null)
					{
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						if (nextpage != null)
						{
							if (nextpage)
							{
								masterSLCategorylst.get(0).setNextPage(1);
							}
							else
							{
								masterSLCategorylst.get(0).setNextPage(0);
							}
						}
					}
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_ShoppingListHistoryDisplay Store Procedure error number: {} " + errorNum + " and error message: {}"
						+ errorMsg);
				throw new ScanSeeException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return masterSLCategorylst;
	}

	/**
	 * This method for adding shopping list history product to list,favorites
	 * and both.
	 * 
	 * @param addSLRequestObj
	 *            contains product details needed for adding to list favorites
	 *            and both
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	/**
	 * This is a DAO Implementation Method for adding shopping list history
	 * product to List ,favorites and both for the given userId.
	 * 
	 * @param addSLRequestObj
	 *            containing product information needed for adding to
	 *            list,favorites and both.
	 * @return XML containing message saying it added to list or favorites or
	 *         both.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public ProductDetails addSLHistoryProductInfo(AddSLRequest addSLRequestObj) throws ScanSeeException
	{
		final String methodName = "addSLHistoryProductInfo in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = null;
		ProductDetails productDetailsObj = null;
		try
		{
			final List<ProductDetail> productDetails = addSLRequestObj.getProductDetails();
			if (null != productDetails && !productDetails.isEmpty())
			{
				final String userProductIdString = Utility.getCommaSepartedUserProdIdValues(productDetails);
				final String productIdString = Utility.getCommaSepartedValues(productDetails);
				log.info("Executing addSLHistoryProductInfo for Products ID {} for User ID", userProductIdString, addSLRequestObj.getUserId());
				simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
				simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
				simpleJdbcCall.withProcedureName("usp_ShoppingHistoryMoveItems");
				final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
				// scanQueryParams.addValue("UserID",
				// addSLRequestObj.getUserId());
				scanQueryParams.addValue("UserProductID", userProductIdString);
				scanQueryParams.addValue("ProductID", productIdString);
				scanQueryParams.addValue("AddTo", addSLRequestObj.getAddTo());
				//For user tracking
				scanQueryParams.addValue(ApplicationConstants.MAINMENUID, addSLRequestObj.getMainMenuID());

				final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
				fromProc = (Integer) resultFromProcedure.get("Status");
				if (fromProc == 0)
				{
					productDetailsObj = new ProductDetails();

					/**
					 * Created list and favorites flag to fetch output
					 * parameters ListFlag and FavoritesFlag. These flag
					 * indicates whether product added to list or favorites.
					 */
					Integer favoritesFlag = null;
					Integer listFlag = null;
					listFlag = (Integer) resultFromProcedure.get("ListFlag");
					if (listFlag != null)
					{
						if (listFlag == 1)
						{
							productDetailsObj.setResponseFlag(ApplicationConstants.SLHISTORYPRODUCTEXISTINLIST);
						}
						else if (listFlag == 0)
						{
							productDetailsObj.setResponseFlag(ApplicationConstants.ADDINGUNASSINGEDPRODUCT);
						}
					}
					favoritesFlag = (Integer) resultFromProcedure.get("FavoritesFlag");

					if (favoritesFlag != null)
					{
						if (favoritesFlag == 1)
						{
							productDetailsObj.setResponseFlag(ApplicationConstants.SLHISTORYPRODUCTEXISTINFAVORITES);
						}
						else if (favoritesFlag == 0)
						{
							productDetailsObj.setResponseFlag(ApplicationConstants.ADDPRODUCTMAINTOSHOPPINGLIST);
						}
						if (listFlag != null && favoritesFlag != null)
						{
							if (listFlag == 1 && favoritesFlag == 1)
							{
								productDetailsObj.setResponseFlag(ApplicationConstants.SLHISTORYPRODUCTEXISTINLISTANDFAVORITES);
							}
							else if (listFlag == 0 && favoritesFlag == 0)
							{
								productDetailsObj.setResponseFlag(ApplicationConstants.SLHISTORYPRODADDEDTOLISTFAVORITESTEXT);
							}
							else if (listFlag == 0 && favoritesFlag == 1)
							{
								productDetailsObj.setResponseFlag(ApplicationConstants.SLHSTADDEDTOLISTNOTFAVORITES);
							}
							else
							{
								productDetailsObj.setResponseFlag(ApplicationConstants.SLHSTADDEDTOFAVORITESNOTLIST);
							}
						}
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_ShoppingHistoryMoveItems Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
				}

			}
		}
		catch (DataAccessException exception)

		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return productDetailsObj;
	}

	/**
	 * This method Fetching App Configuration details.
	 * 
	 * @return ArrayList<AppConfiguration> .
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeException
	{

		final String methodName = "getStockInfo";

		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<AppConfiguration> appConfigurationList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent");
			simpleJdbcCall.returningResultSet("AppConfigurationList", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));

			final MapSqlParameterSource productQueryParams = new MapSqlParameterSource();
			productQueryParams.addValue("ConfigurationType", configType);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(productQueryParams);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					appConfigurationList = (ArrayList<AppConfiguration>) resultFromProcedure.get("AppConfigurationList");
				}

			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_GetScreenContent Store Procedure with error number: {} " + errorNum + " and error message: {}"
						+ errorMsg);
				throw new ScanSeeException(errorMsg);
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e);
		}
		// TODO Auto-generated method stub
		return appConfigurationList;
	}

	@Override
	public ArrayList<RetailerDetail> fetchCommissionJunctionData(Integer userId, Integer productId, Integer productListID, Integer mainMenuID) throws ScanSeeException
	{
		final String methodName = "fetchCommissionJunctionData";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<RetailerDetail> onlineRetailerlst = null;
		try
		{
			log.info(" Request received from Userid ", userId);

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_OnlineRetailerInformation");
			simpleJdbcCall.returningResultSet("commissionjunction", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue(ApplicationConstants.USERID, userId);
			fetchProductDetailsParameters.addValue("ProductID", productId);
			//for user tracking
			fetchProductDetailsParameters.addValue(ApplicationConstants.PRODUCTLISTID, productListID);
			fetchProductDetailsParameters.addValue(ApplicationConstants.MAINMENUID, mainMenuID);
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);
			if (null != resultFromProcedure)
			{

				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{

					onlineRetailerlst = (ArrayList<RetailerDetail>) resultFromProcedure.get("commissionjunction");

				}
				else
				{

					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in fetchCommissionJunctionData Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}
		}
		catch (DataAccessException exception)
		{
			log.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return onlineRetailerlst;
	}

	/**
	 * To update click on product media list to database for user tracking
	 * 
	 * @param pmListID
	 * @return success or failure string
	 * @throws ScanSeeException
	 */
	@Override
	public String userTrackingProdMediaClick(Integer pmListID) throws ScanSeeException {
		final String methodName = "userTrackingProdMediaClick";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserTrackingProductMediaListClicks");

			final MapSqlParameterSource mediaClick = new MapSqlParameterSource();
			mediaClick.addValue("ProductMediaListID", pmListID);
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(mediaClick);
			if (null != resultFromProcedure)	{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))	{
					response = ApplicationConstants.SUCCESSRESPONSETEXT;
				}
				else	{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_UserTrackingProductMediaListClicks Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (DataAccessException exception)	{
			log.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return response;
	}

	/**
	 * To update click on online products to database for user tracking
	 * 
	 * @param pmListID
	 * @return success or failure string
	 * @throws ScanSeeException
	 */
	@Override
	public String userTrackingOnlineStoreClick(AddSLRequest objAddSLRequest) throws ScanSeeException {
		final String methodName = "userTrackingOnlineStoreClick";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserTrackingOnlineStoreClick");

			final MapSqlParameterSource onlineClickParam = new MapSqlParameterSource();
			onlineClickParam.addValue("ProductID", objAddSLRequest.getProductID());
			onlineClickParam.addValue(ApplicationConstants.MAINMENUID, objAddSLRequest.getMainMenuID());
			onlineClickParam.addValue("RetailerName", objAddSLRequest.getRetailerName());
			onlineClickParam.addValue("OnlineProductDetailID", null);
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(onlineClickParam);
			if (null != resultFromProcedure) {
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))	{
					response = ApplicationConstants.SUCCESSRESPONSETEXT;
				}
				else	{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_UserTrackingOnlineStoreClick Store Procedure error number: {} and error message: {}", errorNum,
							errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (DataAccessException exception)	{
			log.info(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return response;
	}
}
