package com.scansee.shoppinglist.dao;

import java.util.ArrayList;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AddRemoveCLR;
import com.scansee.common.pojos.AddRemoveSBProducts;
import com.scansee.common.pojos.AddSLRequest;
import com.scansee.common.pojos.AppConfiguration;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.CouponsDetails;
import com.scansee.common.pojos.FindNearByDetails;
import com.scansee.common.pojos.FindNearByIntactResponse;
import com.scansee.common.pojos.LoyaltyDetails;
import com.scansee.common.pojos.MainSLRetailerCategory;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.ProductDetailsRequest;
import com.scansee.common.pojos.RebateDetails;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.ShoppingListResultSet;
import com.scansee.common.pojos.ThisLocationRequest;
import com.scansee.common.pojos.UserNotesDetails;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.externalapi.common.pojos.ExternalAPIErrorLog;
import com.scansee.externalapi.common.pojos.ExternalAPISearchParameters;
import com.scansee.externalapi.common.pojos.ExternalAPIVendor;

/**
 * The Interface for ShoppingListDAO. ImplementedBy {@link ShoppingListDAOImpl}
 * 
 * @author shyamsundara_hm
 */
public interface ShoppingListDAO
{
	/**
	 * The DAO method for fetching the master shopping list items for the given
	 * userId from the database.
	 * 
	 * @param userId
	 *            - for which master shopping list items need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return ShoppingListResultSet as ArrayList.
	 */
	ArrayList<ShoppingListResultSet> getMainShoppingListItems(int userId) throws ScanSeeException;

	/**
	 * The DAO method to get the Products for adding to shopping list from the
	 * database based on the given userId and productName.
	 * 
	 * @param userId
	 *            requested user.
	 * @param productName
	 *            search key for search.
	 * @return ProductDetails with list of products.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	ProductDetails getProductsForShopping(int userId, String productName) throws ScanSeeException;

	/**
	 * The DAO method updates database for requested products to shopping list.
	 * 
	 * @param addSLRequest
	 *            contain products need to be added to Shopping list.
	 * @return ProductDetails with User product id's for the added product.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	ProductDetails addProductMainToShopList(AddSLRequest addSLRequest) throws ScanSeeException;

	/**
	 * this methods updates database for requested products which need to be
	 * removed from shopping list.
	 * 
	 * @param addSLRequest
	 *            products need to be deleted from Shopping list.
	 * @return String with Success or failure information.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String dleteProductMainShopList(AddSLRequest addSLRequest) throws ScanSeeException;

	/**
	 * For displaying user products based on retailer wise.
	 * 
	 * @param userId
	 *            as request
	 * @param productName
	 *            as request
	 * @return Object String with list of items from Today's Shopping list.
	 * @throws ScanSeeException
	 *             for exception
	 */
	Object getShoppingCartProds(int userId, String productName) throws ScanSeeException;

	/**
	 * For displaying today shopping list products.
	 * 
	 * @param userId
	 *            as request
	 * @return ArrayList of items from Today's Shopping list.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	ArrayList<MainSLRetailerCategory> getTodaySLProducts(int userId) throws ScanSeeException;

	/**
	 * For displaying user products based on retailer wise.
	 * 
	 * @param userId
	 *            as request
	 * @return ArrayList ist of items from Today's Shopping list.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	ArrayList<ShoppingListResultSet> fetchShoppingBasketProds(int userId) throws ScanSeeException;

	/**
	 * This method fetches the retailer information from database for the given
	 * parameters.
	 * 
	 * @param thisLocationRequest
	 *            contains query parameters.
	 * @return ArrayList list of Retailers.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	ArrayList<ShoppingListResultSet> fetchNearByDetails(ThisLocationRequest thisLocationRequest) throws ScanSeeException;

	/**
	 * This method fetches products from database both shopping list and todays
	 * shopping list.
	 * 
	 * @param userId
	 *            as request
	 * @return ArrayList ist of items from Today's Shopping list.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	ArrayList<ShoppingListResultSet> getTodaysNMasterSL(int userId) throws ScanSeeException;

	/**
	 * this method fetches the coupon information from the database for the
	 * given product.
	 * 
	 * @param userId
	 *            requested user.
	 * @param productId
	 *            request product for coupons.
	 * @param retailId
	 *            retailer id.
	 * @return CouponsDetails with coupon info.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	CouponsDetails fetchCouponInfo(int userId, int productId, int retailId, Integer mainMenuID) throws ScanSeeException;

	/**
	 * this method fetches the loyalty information from the database for the
	 * given product.
	 * 
	 * @param userId
	 *            requested user.
	 * @param productId
	 *            request product for coupons.
	 * @param retailId
	 *            retailer id.
	 * @return LoyaltyDetails with loyalty info.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	LoyaltyDetails fetchLoyaltyInfo(int userId, int productId, int retailId, Integer mainMenuID) throws ScanSeeException;

	/**
	 * this method fetches the rebate information from the database for the
	 * given product.
	 * 
	 * @param userId
	 *            requested user.
	 * @param productId
	 *            requested product for coupons.
	 * @param retailId
	 *            retailer id.
	 * @return RebateDetails with rebate info.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	RebateDetails fetchRebateInfo(int userId, int productId, int retailId, Integer mainMenuID) throws ScanSeeException;

	/**
	 * methods to add the unassigned product to database.
	 * 
	 * @param productDetailsRequest
	 *            contains unassigned product information.
	 * @return ProductDetails with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	ProductDetails addUnassignedPro(ProductDetailsRequest productDetailsRequest) throws ScanSeeException;

	/**
	 * methods to add the coupons to database for the given user.
	 * 
	 * @param couponAddRemove
	 *            contains coupon info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	String addCoupon(AddRemoveCLR couponAddRemove) throws ScanSeeException;

	/**
	 * methods to add the Loyalty to database for the .
	 * 
	 * @param couponAddRemove
	 *            contains coupon info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String addLoyalty(AddRemoveCLR couponAddRemove) throws ScanSeeException;

	/**
	 * methods to remove the coupon to database for the given user.
	 * 
	 * @param couponAddRemove
	 *            contains coupon info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String removeCoupon(AddRemoveCLR couponAddRemove) throws ScanSeeException;

	/**
	 * methods to add the Rebate to database for the .
	 * 
	 * @param couponAddRemove
	 *            contains coupon info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String addRebate(AddRemoveCLR couponAddRemove) throws ScanSeeException;

	/**
	 * methods to remove the Loyalty to database for the given user.
	 * 
	 * @param couponAddRemove
	 *            contains Loyalty info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String removeLoyalty(AddRemoveCLR couponAddRemove) throws ScanSeeException;

	/**
	 * methods to remove the rebate to database for the given user.
	 * 
	 * @param couponAddRemove
	 *            contains rebate info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String removeRebate(AddRemoveCLR couponAddRemove) throws ScanSeeException;

	/**
	 * For displaying shopping basket product based on userId.
	 * 
	 * @param objUTData requested UserTrackingData.
	 * @param strApplnConst requested StringConstant.
	 * @return AddRemoveSBProducts list of cart products.
	 * @throws ScanSeeException
	 *             for exception
	 */
	AddRemoveSBProducts getShopBasketProducts(UserTrackingData objUTData, String strApplnConst) throws ScanSeeException;

	/**
	 * method updates the database to add/Remove product to/from cart.
	 * 
	 * @param addRemObj
	 *            products to be added/removed.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String addRemoveSBProducts(AddRemoveSBProducts addRemObj) throws ScanSeeException;

	/**
	 * method update database to add/remove the products to/from Today Shopping
	 * List.
	 * 
	 * @param addRemObj
	 *            contains products information.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String addRemoveTodaySLProducts(AddSLRequest addRemObj) throws ScanSeeException;

	/**
	 * method to add user notes to database.
	 * 
	 * @param userNotesDetails
	 *            contains user notes details.
	 * @return String with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String addUserNotesDetails(UserNotesDetails userNotesDetails) throws ScanSeeException;

	/**
	 * method to delete user notes from database.
	 * 
	 * @param userID
	 *            requested user.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String deleteUserNotesDetails(int userID) throws ScanSeeException;

	/**
	 * this method to get the user notes from database.
	 * 
	 * @param userID
	 *            for fetching user notes.
	 * @return UserNotesDetails contains User notes info.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	UserNotesDetails getUserNotesDetails(int userID) throws ScanSeeException;

	/**
	 * method to get the Retailers from the database based on query parameters.
	 * 
	 * @param userID
	 *            requested user.
	 * @param productId
	 *            products to be searched for.
	 * @param latitude
	 *            current location of user.
	 * @param longitude
	 *            current location of user.
	 * @param postalCode
	 *            current location of user.
	 * @param radius
	 *            search distnace.
	 * @return FindNearByDetails list of retailers.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */

	FindNearByDetails fetchNearByInfo(int userID, Integer productId, String latitude, String longitude, String postalCode, String radius, Integer mainMenuID)
			throws ScanSeeException;

	/**
	 * method to get the Retailers from the database based on query parameters.
	 * 
	 * @param userID
	 *            requested user.
	 * @param productId
	 *            products to be searched for.
	 * @param latitude
	 *            current location of user.
	 * @param longitude
	 *            current location of user.
	 * @param postalcode
	 *            current location of user.
	 * @param radius
	 *            search distance.
	 * @return FindNearByLowestPrice list of retailers.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	FindNearByIntactResponse fetchNearByLowestPrice(int userID, Double latitude, int productId, Double longitude, String postalcode, double radius, Integer mainMenuID)
			throws ScanSeeException;

	/**
	 * method to removes all the products from Cart.
	 * 
	 * @param userID
	 *            requested user.
	 * @param cartLatitude
	 *            location of user.
	 * @param cartLongitude
	 *            location of user.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String checkOutFromBasket(int userID, float cartLatitude, float cartLongitude) throws ScanSeeException;

	/**
	 * methods to get the products media info based on the media type from
	 * database.
	 * 
	 * @param productID
	 *            requested product.
	 * @param mediaType
	 *            type of media.
	 * @return ProductDetails with media details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	ProductDetails getProductMediaDetails(int productID, String mediaType, Integer productListID) throws ScanSeeException;

	/**
	 * methods to get the external API List.
	 * 
	 * @param moduleName
	 *            requested moduleName.
	 * @return externalAPIList
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	ArrayList<ExternalAPIVendor> getExternalAPIList(String moduleName) throws ScanSeeException;

	/**
	 * methods to get the external API Input parameter List.
	 * 
	 * @param apiUsageID
	 *            requested APIUsageID.
	 * @param moduleName
	 *            requested moduleName.
	 * @return externalAPIInputParameters
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	ArrayList<ExternalAPISearchParameters> getExternalAPIInputParameters(Integer apiUsageID, String moduleName) throws ScanSeeException;

	/**
	 * methods to get product info for external API.
	 * 
	 * @param productId
	 *            requested productId.
	 * @return ProductDetail object
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	ProductDetail getProductInfoforExternalAPI(Integer productId) throws ScanSeeException;

	/**
	 * This method get the Shopping list retailers and category list from the
	 * database.
	 * 
	 * @param userId
	 *            as request
	 * @return xml String XMl with list of shopping List items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	ArrayList<MainSLRetailerCategory> getMasterSLRetailerCategoryDetails(int userId) throws ScanSeeException;

	/**
	 * This method get the Master Shopping list products from the database.
	 * 
	 * @param productDetailsRequest
	 *            as request
	 * @return xml String XMl with list of shopping List items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	ProductDetails getMSLProcuctDetailslst(ProductDetailsRequest productDetailsRequest) throws ScanSeeException;

	/**
	 * This method get the Master Shopping list product CLR from the database.
	 * 
	 * @param userId
	 *            as request
	 * @param retailerId
	 *            as request
	 * @param productId
	 *            as request.
	 * @param lowerLimit
	 * 		as request.
	 * @return xml String XMl with list of shopping List items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	CLRDetails fetchMasterSLCLRDetails(CLRDetails clrDetails) throws ScanSeeException;

	/**
	 * This method get the today Shopping list products from the database.
	 * 
	 * @param productDetailsRequest
	 *            as request
	 * @return xml String XMl with list of shopping List items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	ProductDetails getTSLProcuctDetailslst(ProductDetailsRequest productDetailsRequest) throws ScanSeeException;

	/**
	 * This method logs External api error messages.
	 * 
	 * @param externalAPIErrorLog
	 *            -As a parameter
	 * @return response as failure or success
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	String logExternalApiErrorMessages(ExternalAPIErrorLog externalAPIErrorLog) throws ScanSeeException;

	/**
	 * This method get the Master Shopping list products from the database.
	 * 
	 * @param userId as request parameter.
	 * @param iLowLimit as request parameter.
	 * @param strApplnConst as request parameter.
	 * @return xml String XMl with list of shopping List items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	ArrayList<MainSLRetailerCategory> getMSLCategoryProcuctDetails(Integer userId, Integer iLowLimit, String strApplnConst) throws ScanSeeException;

	/**
	 * This method is for adding today's SL product for the given product id and
	 * user id.
	 * 
	 * @param addRemObj
	 *            -AS AddSLRequest object
	 * @return ProductDetail contains sale price ,categoryid etc.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	ProductDetail addTodaySLProductsBySearch(AddSLRequest addRemObj) throws ScanSeeException;

	/**
	 * This method is used to insert apiurls into FindNearByLog table.
	 * 
	 * @param apiURL
	 *            -As String parameter
	 * @param userId
	 *            -As int parameter
	 * @return response as failure or success
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	String insertExternalAPIURL(String apiURL, int userId) throws ScanSeeException;

	/**
	 * This method get the Shopping list history products from the database.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @return xml containing list of shopping List history items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	ArrayList<MainSLRetailerCategory> fetchSLHistoryProducts(Integer userId,Integer lowerlimit) throws ScanSeeException;

	/**
	 * This method for adding shopping list history product to list,favorites
	 * and both.
	 * 
	 * @param addSLRequestObj
	 *            contains product details needed for adding to list favorites
	 *            and both
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	ProductDetails addSLHistoryProductInfo(AddSLRequest addSLRequestObj) throws ScanSeeException;

	/**
	 * This method Fetching App Configuration details.
	 * 
	 * @return ArrayList.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	ArrayList<AppConfiguration> getAppConfig(String configType) throws ScanSeeException;
	
	ArrayList<RetailerDetail>fetchCommissionJunctionData(Integer userId, Integer productId, Integer productListId, Integer mainMenuId)throws ScanSeeException;
	
	/**
	 * To update click on product media list to database for user tracking
	 * 
	 * @param pmListID
	 * @return success or failure string
	 * @throws ScanSeeException
	 */
	String userTrackingProdMediaClick(Integer pmListID) throws ScanSeeException;
	
	/**
	 * To update click on online products to database for user tracking
	 * 
	 * @param pmListID
	 * @return success or failure string
	 * @throws ScanSeeException
	 */
	String userTrackingOnlineStoreClick(AddSLRequest objAddSLRequest) throws ScanSeeException;
}
