package com.scansee.shoppinglist.service;

import com.scansee.common.exception.ScanSeeException;

/**
 * This interface consists of service layer methods for shopping list.
 * 
 * @author manjunatha_gh
 */
public interface ShoppingListService
{

	/**
	 * This is a service Method for fetching main shopping list items for the
	 * given userId. This method checks for mandatory fields, if any mandatory
	 * fields are not available returns Insufficient Data error message else
	 * calls the DAO method.
	 * 
	 * @param userId
	 *            for which main shopping list items need to be fetched.If
	 *            userId is null then it is invalid request.
	 * @return XML containing main shopping list items in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getMainShoppingListItems(Integer userId) throws ScanSeeException;

	/**
	 * This is a service Method for fetching product information for the given
	 * userId and product search key. This method validates the userId and
	 * prodSearchKey if it is valid it will call the DAO method else returns
	 * Insufficient Data error message.
	 * 
	 * @param userId
	 *            for which products information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @param prodSearchKey
	 *            for which products information need to be fetched.If
	 *            prodSearchKey is null then it is invalid request.
	 * @return XML containing products information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String getProductsForShopping(Integer userId, String prodSearchKey) throws ScanSeeException;

	/**
	 * This is a Service Method for fetching product information for the given
	 * userId,productId and retailId.This method validates the userId, productId
	 * and retailId if it is valid it will call the DAO method else returns
	 * Insufficient Data error message.
	 * 
	 * @param userId
	 *            for which product information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @param productId
	 *            for which product information need to be fetched.If productId
	 *            is null then it is invalid request.
	 * @param retailId
	 *            for which product information need to be fetched.If retailId
	 *            is null then it is invalid request.
	 * @return XML containing product information in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String getProductsInfo(Integer userId, Integer productId, Integer retailId, Integer saleListID, Integer prodListID, Integer mainMenuID, Integer scanID) throws ScanSeeException;

	/**
	 * This is a service Method for adding product to MSL.This method validates
	 * the input xml and returns validation error if validation fails. and calls
	 * the DAO methods for adding products from DB.
	 * 
	 * @param xml
	 *            products need to be added to Shopping list.
	 * @return String with Success or failure information.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String addProductMainToShopList(String xml) throws ScanSeeException;

	/**
	 * This is a service Method for deleting product from SL. This method
	 * validates the input xml if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            containing input information need to be deleted product from
	 *            SL.
	 * @return response: return the whether updated is successful or not.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String dleteProductMainShopList(String xml) throws ScanSeeException;

	/**
	 * This is a Service Method for fetching today shopping list for the given
	 * userId.This method validates the input userId if it is valid it will call
	 * the DAO method else returns Insufficient Data error message.
	 * 
	 * @param userId
	 *            for which today shopping list need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return XML containing today shopping list in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String getTodaySLProducts(Integer userId) throws ScanSeeException;

	/**
	 * This is a Service Method for fetching the products from shopping basket
	 * for the given userId.This method validates the input userId if it is
	 * valid it will call the DAO method.
	 * 
	 * @param userId
	 *            for which the products from shopping basket need to be
	 *            fetched.If userId is null then it is invalid request.
	 * @return String response as a string contains all shopping basket
	 *         products. if no products found No records found code will be
	 *         returned.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String getShoppingBasketProds(Integer userId) throws ScanSeeException;

	/**
	 * This is service method for fetching the coupon info.This method validates
	 * the input parameters and returns validation error if validation fails.
	 * and calls DAO methods to get the coupons for product.
	 * 
	 * @param userId
	 *            requested user.
	 * @param productId
	 *            request product for coupons.
	 * @param clr
	 *            flag for identifying.
	 * @return String XML with coupon info.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String getCouponInfo(Integer userId, Integer productId, Integer clr, Integer mainMenuID) throws ScanSeeException;

	/**
	 * This is service method for fetching the loyalty info.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. and calls DAO methods.
	 * 
	 * @param userId
	 *            requested user.
	 * @param productId
	 *            request product for loyalty.
	 * @param retailId
	 *            flag for identifying.
	 * @return String XML with loyalty info.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String getLoyaltyInfo(Integer userId, Integer productId, Integer retailId, Integer mainMenuID) throws ScanSeeException;

	/**
	 * This is service method for fetching the rebate info.This method validates
	 * the input parameters and returns validation error if validation fails.
	 * and calls DAO methods.
	 * 
	 * @param userId
	 *            requested user.
	 * @param productId
	 *            request product for rebate.
	 * @param retailId
	 *            flag for identifying.
	 * @return String XML with rebate info.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String getRebateInfo(Integer userId, Integer productId, Integer retailId, Integer mainMenuID) throws ScanSeeException;

	/**
	 * This is a Service Method for finding near by retailers. This method
	 * validates the input xml if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            containing input information need to be find near by
	 *            retailers.
	 * @return XML containing near by retailers as response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String findNearByRetailers(String xml) throws ScanSeeException;

	/**
	 * This is a Service Method for adding unassigned product.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. and calls DAO methods to add the unassigned product.
	 * 
	 * @param xml
	 *            contains unassigned product information.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String addUnassigedProd(String xml) throws ScanSeeException;

	/**
	 * This is a Service Method for add/remove SB products.This method validates
	 * the input parameters and returns validation error if validation fails.
	 * and calls DAO methods to add/Remove product to/from cart.
	 * 
	 * @param xml
	 *            products to be added/removed.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String addRemoveSBProducts(String xml) throws ScanSeeException;

	/**
	 * This is a Service method for fetching the products from shopping basket
	 * for the given userId.This method validates userId and returns validation
	 * error if validation fails. and calls DAO methods.
	 * 
	 * @param userId
	 *            cart products for the user.
	 * @return String XML with list of Products.
	 *@throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String getShopBasketProducts(String xml) throws ScanSeeException;

	/**
	 * This is a Service method for the Adding a Coupon(to the Coupon
	 * Gallery).This method validates the input parameters and returns
	 * validation error if validation fails. calls DAO methods to add the
	 * coupons.
	 * 
	 * @param xml
	 *            contains coupon info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String couponAdd(String xml) throws ScanSeeException;

	/**
	 * This is Service method for the removing a Coupon(from the Coupon
	 * Gallery).This method validates the input parameters and returns
	 * validation error if validation fails. calls DAO methods to remove the
	 * coupons.
	 * 
	 * @param xml
	 *            contains coupon info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String removeCoupon(String xml) throws ScanSeeException;

	/**
	 * This is Service method for adding Loyalty (to the Loyalty Gallery).This
	 * method validates the input parameters and returns validation error if
	 * validation fails. calls DAO methods to add the Loyalty.
	 * 
	 * @param xml
	 *            contains Loyalty info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String addLoyalty(String xml) throws ScanSeeException;

	/**
	 * This is Service method for removing Rebate (from the Rebate Gallery).This
	 * method validates the input parameters and returns validation error if
	 * validation fails. calls DAO methods to remove the Loyalty.
	 * 
	 * @param xml
	 *            contains Loyalty info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String removeRebate(String xml) throws ScanSeeException;

	/**
	 * This is Service method for adding Rebate (to the Rebate Gallery).This
	 * method validates the input parameters and returns validation error if
	 * validation fails. calls DAO methods to add the Rebate.
	 * 
	 * @param xml
	 *            contains Rebate info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String addRebate(String xml) throws ScanSeeException;

	/**
	 * This is Service method for removing Loyalty (from the Loyalty
	 * Gallery).This method validates the input parameters and returns
	 * validation error if validation fails. calls DAO methods to remove the
	 * Rebate.
	 * 
	 * @param xml
	 *            contains Rebate info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String removeLoyalty(String xml) throws ScanSeeException;

	/**
	 * This is Service method for adds/removes the products to/from Today's
	 * shopping list.This method validates the input parameters and returns
	 * validation error if validation fails. calls DAO methods to add/remove the
	 * products to/from Today Shopping List.
	 * 
	 * @param xml
	 *            contains products information.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String addRemoveTodaySLProducts(String xml) throws ScanSeeException;

	/**
	 * This is Service method for add User notes for shopping list.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. calls DAO methods to add user notes.
	 * 
	 * @param xml
	 *            contains user notes details.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String addUserNotes(String xml) throws ScanSeeException;

	/**
	 * This is Service method for deleting User notes for shopping list.This
	 * method validates the input parameters and returns validation error if
	 * validation fails. calls DAO methods to delete user notes.
	 * 
	 * @param userId
	 *            requested user.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String deleteUserNotes(Integer userId) throws ScanSeeException;

	/**
	 * This is Service method for retrieve User notes for shopping list.This
	 * method validates the userId and returns validation error if validation
	 * fails.
	 * 
	 * @param userId
	 *            for fetching user notes.
	 * @return String XML contains User notes info.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String getUserNotesDetails(Integer userId) throws ScanSeeException;

	/**
	 * This is a Service Method for fetching shopping list and cart products for
	 * the given userId. This method validates the userId and returns validation
	 * error if validation fails.If it is valid it will call the DAO method.
	 * 
	 * @param userId
	 *            for which shopping list and cart products need to be
	 *            fetched.If userId is null then it is invalid request.
	 * @return XML containing shopping list and cart products in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String getTodaysNMasterSL(Integer userId) throws ScanSeeException;

	/**
	 * This is Service method for retrieving find near by retailers.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. calls DAO methods to get the Retailers.
	 * 
	 * @param userID
	 *            requested user.
	 * @param productId
	 *            products to be searched for.
	 * @param latitude
	 *            current location of user.
	 * @param longitude
	 *            current location of user.
	 * @param postalCode
	 *            current location of user.
	 * @param radius
	 *            search distance.
	 * @return String XMl list of retailers.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String findNearBy(Integer userID, Integer productId, String latitude, String longitude, String postalCode, String radius, Integer mainMenuID) throws ScanSeeException;

	/**
	 * This is Service method for find near by retailers based on lowest
	 * price.This method validates the input parameters and returns validation
	 * error if validation fails. calls DAO methods to get the number of
	 * Retailers and lowest price of the products searching for.
	 * 
	 * @param userID
	 *            requested user.
	 * @param productId
	 *            products to be searched for.
	 * @param latitude
	 *            current location of user.
	 * @param longitude
	 *            current location of user.
	 * @param postalcode
	 *            current location of user.
	 * @param radius
	 *            search distance.
	 * @param page
	 *            page.
	 * @return String XMl list of retailers.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String findNearByLowestPrice(Integer userID, Integer productId, Double latitude, Double longitude, String postalcode, Integer radius, Integer page, Integer mainMenuID)
			throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. calls DAO methods to removes the products from Cart.
	 * 
	 * @param userID
	 *            requested user.
	 * @param cartLatitude
	 *            location of user.
	 * @param cartLongitude
	 *            location of user.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String checkOutFromBasket(Integer userID, float cartLatitude, float cartLongitude) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to get the products media info
	 * based on the media type.
	 * 
	 * @param productID
	 *            requested product.
	 * @param mediaType
	 *            type of media.
	 * @return String XML with media details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String getMediaDetails(Integer productID, String mediaType, Integer productListID) throws ScanSeeException;

	/**
	 * This is a service Method for fetching master shopping list retailers and
	 * categories for the given userId. This method checks for mandatory fields,
	 * if any mandatory fields are not available returns Insufficient Data error
	 * message else calls the DAO method..
	 * 
	 * @param userId
	 *            for which master shopping list retailers and categories need
	 *            to be fetched.If userId is null then it is invalid request.
	 * @return XML containing master shopping list items in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String getMSLRetailerCategorylst(Integer userId) throws ScanSeeException;

	/**
	 * This is a service Method for fetching master shopping list products for
	 * the given input as XML. This method validates the input XML, if it is
	 * valid it will call the DAO method.
	 * 
	 * @param xml
	 *            containing input information need to be fetched the MSL
	 *            products .
	 * @return XML containing master shopping list products in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String getMSLProductslst(String xml) throws ScanSeeException;

	/**
	 * This is a service Method for fetching today shopping list products for
	 * the given input as XML. This method validates the input XML, if it is
	 * valid it will call the DAO method.
	 * 
	 * @param xml
	 *            containing input information need to be fetched the TSL
	 *            products .
	 * @return XML containing TSL products in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String getTSLProductslst(String xml) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to get the products media info
	 * based on the media type.
	 * 
	 * @param xml 
	 *            as the parameter.
	 * @return String XML with CLR details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String fetchMSLCLRDetails(String xml) throws ScanSeeException;

	/**
	 * This is a service Method for fetching product summary details for the
	 * given input as XML.
	 * 
	 * @param xml
	 *            -Containing input information need to be fetched product
	 *            summary
	 * @return XML containing product summary details in the response.
	 * @throws ScanSeeException
	 *             - Throws if exception occurs
	 */
	String getProductSummary(String xml) throws ScanSeeException;

	/**
	 * This is a service Method for fetching master shopping list products for
	 * the given input as XML. This method validates the input XML, if it is
	 * valid it will call the DAO method.
	 * 
	 * @param userId as request parameter.
	 * @param iLowLimit as request parameter.
	 * @return XML containing master shopping list products in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String getMSLCategoryProducts(Integer userId, Integer iLowLimit) throws ScanSeeException;

	/**
	 * This is a service Method for finding online stores using External API.
	 * products for the given input as XML.
	 * 
	 * @param xml
	 *            containing the request for External API call .
	 * @return XML containing List of online stores.
	 * @throws ScanSeeException
	 *             -If any exception occurs ScanSeeException will be thrown.
	 */

	String findOnlineStores(String xml) throws ScanSeeException;

	/**
	 * This service Method for adding today's SL product for the given product
	 * id and user id.
	 * 
	 * @param xml
	 *            -containing the request for External API call .
	 * @return XML containing List of SL products.
	 * @throws ScanSeeException
	 *             -If any exception occurs ScanSeeException will be thrown.
	 */
	String addTodaySLProductsBySearch(String xml) throws ScanSeeException;

	/**
	 * This is a service Method for fetching shopping list history items for the
	 * given userId. This method checks for mandatory fields, if any mandatory
	 * fields are not available returns Insufficient Data error message else
	 * calls the DAO method.
	 * 
	 * @param userId
	 *            for which shopping list history items need to be fetched.If
	 *            userId is null then it is invalid request.
	 * @return XML containing shopping list history items in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String fetchSLHistoryItems(Integer userId,Integer lowerlimit) throws ScanSeeException;

	/**
	 * This is a service Method for adding product from shopping list history to
	 * list or favorites or both.This method validates the input xml and returns
	 * validation error if validation fails. and calls the DAO method for adding
	 * products to list,favorites and both.
	 * 
	 * @param xml
	 *            containing products need to be added to Shopping
	 *            list,favorites and both.
	 * @return String with Success or failure information.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String addSLHistoryProdut(String xml) throws ScanSeeException;
	
	/**
	 * This is a service Method for capturing product media click
	 * 
	 * @param pmListID
	 *            
	 * @return String with Success or failure information.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String userTrackingProdMediaClick(Integer pmListID) throws ScanSeeException;
	
	/**
	 * This is a service Method for capturing Online Product Click
	 * 
	 * @param onProdDetID
	 *            
	 * @return String with Success or failure information.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String userTrackingOnlineStoreClick(String xml) throws ScanSeeException;

}
