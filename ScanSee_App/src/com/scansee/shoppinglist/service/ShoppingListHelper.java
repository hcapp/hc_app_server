package com.scansee.shoppinglist.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.pojos.Category;
import com.scansee.common.pojos.CategoryInfo;
import com.scansee.common.pojos.MainSLRetailerCategory;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.RetailerInfo;
import com.scansee.common.pojos.ShoppingListDetails;
import com.scansee.common.pojos.ShoppingListResultSet;
import com.scansee.common.pojos.ShoppingListRetailerCategoryList;
import com.scansee.common.util.Utility;

/**
 * This class contains Utility methods for converting Shopping List objects.
 * 
 * @author manjunatha_gh
 */
public class ShoppingListHelper
{
	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ShoppingListHelper.class);
	/**
	 * for ShoppingListHelper constructor.
	 */
	protected ShoppingListHelper()
	{
		LOG.info("Inside ShoppingListHelper class");
	}
	/**
	 * this method returns shopping list grouped by retailers.
	 * 
	 * @param shoppingListResultSet
	 *            contains list of productDetail objects.
	 * @return result list of ShoppingListDetails objects
	 */
	public static ShoppingListDetails getShoppingListDetails(ArrayList<ShoppingListResultSet> shoppingListResultSet)
	{

		final ShoppingListDetails result = new ShoppingListDetails();

		final HashMap<String, ArrayList<ProductDetail>> retailersMap = new HashMap<String, ArrayList<ProductDetail>>();

		ArrayList<ProductDetail> productDetailslst;
		String key = null; 

		for (ShoppingListResultSet listResultSet : shoppingListResultSet)
		{
			key = Utility.nullCheck(listResultSet.getRetailerName());
			if (retailersMap.containsKey(key))
			{
				productDetailslst = retailersMap.get(key);
				final ProductDetail productDetail = new ProductDetail();
				productDetail.setProductId(listResultSet.getProductId());
				productDetail.setUserProductId(listResultSet.getUserProductID());
				productDetail.setProductName(listResultSet.getProductName());
				productDetail.setCoupons(listResultSet.getCoupon());
				productDetail.setRebates(listResultSet.getRebate());
				productDetail.setLoyalties(listResultSet.getLoyalty());
				productDetail.setCoupon_Status(listResultSet.getCoupon_Status());
				productDetail.setRebate_Status(listResultSet.getRebate_Status());
				productDetail.setLoyalty_Status(listResultSet.getLoyalty_Status());
				productDetailslst.add(productDetail);
			}
			else
			{
				productDetailslst = new ArrayList<ProductDetail>();
				final ProductDetail productDetail = new ProductDetail();
				productDetailslst.add(productDetail);
				retailersMap.put(key, productDetailslst);
			}
		}

		final Set<Map.Entry<String, ArrayList<ProductDetail>>> set = retailersMap.entrySet();

		final ArrayList<RetailerInfo> retiArrayList = new ArrayList<RetailerInfo>();
		RetailerInfo retailerInfo;
		for (Map.Entry<String, ArrayList<ProductDetail>> entry : set)
		{
			retailerInfo = new RetailerInfo();
			retailerInfo.setRetailerName(entry.getKey());
			retailerInfo.setProductDetails(entry.getValue());
			retiArrayList.add(retailerInfo);
		}

		result.setRetailerDetails(retiArrayList);

		return result;
	}

	/**
	 * this method returns shopping list grouped by retailers.
	 * 
	 * @param shoppingListResultSet
	 *            contains list of productDetail objects.
	 * @return result list of ShoppingListDetails objects
	 */

	public static ShoppingListDetails getShoppingList(ArrayList<ShoppingListResultSet> shoppingListResultSet)
	{

		final ShoppingListDetails result = new ShoppingListDetails();

		final HashMap<String, RetailerInfo> retailersMap = new HashMap<String, RetailerInfo>();

		ArrayList<ProductDetail> productDetails;
		RetailerInfo retailerInfo = null;
		String key = null;
		for (ShoppingListResultSet listResultSet : shoppingListResultSet)
		{
			key = Utility.nullCheck(listResultSet.getRetailerName());
			if (retailersMap.containsKey(key))
			{
				retailerInfo = retailersMap.get(key);
				productDetails = retailerInfo.getProductDetails();
				if (null != productDetails)
				{
					final ProductDetail productDetail = new ProductDetail();
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductID());
					productDetail.setCLRFlag(listResultSet.getCLRFlag());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setShopListItem(listResultSet.getShopListItem());
					productDetail.setShopCartItem(listResultSet.getShopCartItem());
					productDetail.setCoupons(listResultSet.getCoupon());
					productDetail.setRebates(listResultSet.getRebate());
					productDetail.setLoyalties(listResultSet.getLoyalty());
					productDetail.setCoupon_Status(listResultSet.getCoupon_Status());
					productDetail.setRebate_Status(listResultSet.getRebate_Status());
					productDetail.setLoyalty_Status(listResultSet.getLoyalty_Status());
					productDetail.setUsage(listResultSet.getUsage());
					productDetail.setButtonFlag(listResultSet.getButtonFlag());
					productDetails.add(productDetail);
					retailerInfo.setProductDetails(productDetails);
				}
			}
			else
			{
				retailerInfo = new RetailerInfo();
				retailerInfo.setRetailerId(listResultSet.getRetailerId());
				retailerInfo.setRetailerName(listResultSet.getRetailerName());
				productDetails = new ArrayList<ProductDetail>();
				final ProductDetail productDetail = new ProductDetail();
				if (null != listResultSet.getProductId())
				{
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductID());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setCLRFlag(listResultSet.getCLRFlag());
					productDetail.setProductPrice(listResultSet.getProductPrice());
					productDetail.setShopListItem(listResultSet.getShopListItem());
					productDetail.setShopCartItem(listResultSet.getShopCartItem());
					productDetail.setCoupons(listResultSet.getCoupon());
					productDetail.setRebates(listResultSet.getRebate());
					productDetail.setLoyalties(listResultSet.getLoyalty());
					productDetail.setCoupon_Status(listResultSet.getCoupon_Status());
					productDetail.setRebate_Status(listResultSet.getRebate_Status());
					productDetail.setLoyalty_Status(listResultSet.getLoyalty_Status());
					productDetail.setUsage(listResultSet.getUsage());
					productDetail.setButtonFlag(listResultSet.getButtonFlag());
					productDetails.add(productDetail);
					retailerInfo.setProductDetails(productDetails);
				}
				retailersMap.put(key, retailerInfo);
			}
		}

		final Set<Map.Entry<String, RetailerInfo>> set = retailersMap.entrySet();

		final ArrayList<RetailerInfo> retiArrayList = new ArrayList<RetailerInfo>();

		for (Map.Entry<String, RetailerInfo> entry : set)
		{
			retiArrayList.add(entry.getValue());
		}

		result.setRetailerDetails(retiArrayList);
		return result;
	}

	/**
	 * this method returns shopping list grouped by category name.
	 * 
	 * @param shoppingListResultSet
	 *            contains list of productDetail objects.
	 * @return result list of ShoppingListDetails objects
	 */

	
	public static ShoppingListRetailerCategoryList getShoppingRetailersList(ArrayList<MainSLRetailerCategory> shoppingListResultSet)
	{

		final ShoppingListRetailerCategoryList shoppingListRetailerCategoryList = new ShoppingListRetailerCategoryList();

		final HashMap<String, RetailerInfo> retailersMap = new HashMap<String, RetailerInfo>();

		ArrayList<CategoryInfo> categoryInfolst;
		RetailerInfo retailerInfo = null;
		String key = null;
		for (MainSLRetailerCategory listResultSet : shoppingListResultSet)
		{
			key = Utility.nullCheck(listResultSet.getRetailName());
			if (retailersMap.containsKey(key))
			{

				retailerInfo = retailersMap.get(key);
				categoryInfolst = retailerInfo.getCategoryDetails();
				if (null != categoryInfolst)
				{
					final CategoryInfo categoryInfo = new CategoryInfo();
					categoryInfo.setParentCategoryName(listResultSet.getParentCategoryName());
					categoryInfo.setParentCategoryID(listResultSet.getParentCategoryID());

					categoryInfolst.add(categoryInfo);
					retailerInfo.setCategoryDetails(categoryInfolst);

				}
			}
			else
			{
				retailerInfo = new RetailerInfo();
				retailerInfo.setRetailerId(listResultSet.getRetailID());
				retailerInfo.setRetailerName(listResultSet.getRetailName());
				retailerInfo.setUserRetailPreferenceID(listResultSet.getUserRetailPreferenceID());
				categoryInfolst = new ArrayList<CategoryInfo>();
				final CategoryInfo categoryDetails = new CategoryInfo();
				if (null != listResultSet.getParentCategoryID())
				{

					categoryDetails.setParentCategoryName(listResultSet.getParentCategoryName());
					categoryDetails.setParentCategoryID(listResultSet.getParentCategoryID());

					categoryInfolst.add(categoryDetails);
					retailerInfo.setCategoryDetails(categoryInfolst);
				}
				retailersMap.put(key, retailerInfo);
			}
		}

		final Set<Map.Entry<String, RetailerInfo>> set = retailersMap.entrySet();

		final ArrayList<RetailerInfo> retiArrayList = new ArrayList<RetailerInfo>();

		for (Map.Entry<String, RetailerInfo> entry : set)
		{
			retiArrayList.add(entry.getValue());
		}

		shoppingListRetailerCategoryList.setRetailerInfolst(retiArrayList);
		return shoppingListRetailerCategoryList;
	}
	
	/**
	 * This method for Categorizing master shopping list products.
	 * It includes product details and clr information grouped by category id.
	 * @param shoppingListResultSet as request parameter.
	 * @return ShoppingListRetailerCategoryList as response
	 */
	public static ShoppingListRetailerCategoryList getShoppingListCategoryList(ArrayList<MainSLRetailerCategory> shoppingListResultSet)
	{

		final ShoppingListRetailerCategoryList shoppingListCategoryList = new ShoppingListRetailerCategoryList();

		final HashMap<Integer, Category> categoryMap = new HashMap<Integer, Category>();

		ArrayList<ProductDetail> productDetaillst=null;
		Category categoryInfo = null;
		Integer key = null;
		for (MainSLRetailerCategory listResultSet : shoppingListResultSet)
		{
			key = listResultSet.getParentCategoryID();
			
			if (categoryMap.containsKey(key))
			{
				
				categoryInfo = categoryMap.get(key);
				productDetaillst = categoryInfo.getProductDetails();
				if (null != productDetaillst)
			
				{
					final ProductDetail productDetail = new ProductDetail();
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductID());
					productDetail.setCLRFlag(listResultSet.getCLRFlag());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setProductShortDescription(listResultSet.getProductShortDescription());
					productDetail.setProductLongDescription(listResultSet.getProductLongDescription());
					productDetail.setCoupon_Status(listResultSet.getCoupon_Status());
					productDetail.setRebate_Status(listResultSet.getRebate_Status());
					productDetail.setLoyalty_Status(listResultSet.getLoyalty_Status());
					productDetail.setButtonFlag(listResultSet.getButtonFlag());
					productDetail.setSubCategoryID(listResultSet.getSubCategoryID());
					productDetail.setSubCategoryName(listResultSet.getSubCategoryName());
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductPrice(listResultSet.getPrice());
					productDetail.setRow_Num(listResultSet.getRowNum());
					productDetaillst.add(productDetail);
					categoryInfo.setProductDetails(productDetaillst);
				}
			}
			else
			{
				categoryInfo = new Category();
				categoryInfo.setCategoryId(listResultSet.getCategoryID());
				categoryInfo.setParentCategoryId(listResultSet.getParentCategoryID());
				categoryInfo.setParentCategoryName(listResultSet.getParentCategoryName());
				productDetaillst = new ArrayList<ProductDetail>();
				final ProductDetail productDetail = new ProductDetail();
				if (null != listResultSet.getProductId())
				{
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductID());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setCLRFlag(listResultSet.getCLRFlag());
					productDetail.setCoupon_Status(listResultSet.getCoupon_Status());
					productDetail.setRebate_Status(listResultSet.getRebate_Status());
					productDetail.setLoyalty_Status(listResultSet.getLoyalty_Status());
					productDetail.setProductShortDescription(listResultSet.getProductShortDescription());
					productDetail.setProductLongDescription(listResultSet.getProductLongDescription());
					productDetail.setButtonFlag(listResultSet.getButtonFlag());
					productDetail.setSubCategoryID(listResultSet.getSubCategoryID());
					productDetail.setSubCategoryName(listResultSet.getSubCategoryName());
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductPrice(listResultSet.getPrice());
					productDetail.setRow_Num(listResultSet.getRowNum());
					productDetaillst.add(productDetail);
					categoryInfo.setProductDetails(productDetaillst);
					
				}
				categoryMap.put(key, categoryInfo);
			}
		}

		final Set<Map.Entry<Integer, Category>> set = categoryMap.entrySet();

		final ArrayList<Category> categoryInfolst = new ArrayList<Category>();

		for (Map.Entry<Integer, Category> entry : set)
		{
			categoryInfolst.add(entry.getValue());
		}

		shoppingListCategoryList.setCategoryInfolst(categoryInfolst);
		return shoppingListCategoryList;
	}
	
	/**
	 * This method for displaying shopping list history grouped by parent category name.
	 * 
	 * @param shoppingListResultSet as request parameter.
	 * @return ShoppingListRetailerCategoryList as response
	 */
	public static ShoppingListRetailerCategoryList fetchSLHistoryCategoryDisplay(ArrayList<MainSLRetailerCategory> shoppingListResultSet)
	{

		final ShoppingListRetailerCategoryList shoppingListCategoryList = new ShoppingListRetailerCategoryList();

		final HashMap<String, Category> categoryMap = new HashMap<String, Category>();

		ArrayList<ProductDetail> productDetaillst=null;
		Category categoryInfo = null;
		String key = null;
		for (MainSLRetailerCategory listResultSet : shoppingListResultSet)
		{
			key = listResultSet.getParentCategoryName();
			
			if (categoryMap.containsKey(key))
			{
				
				categoryInfo = categoryMap.get(key);
				productDetaillst = categoryInfo.getProductDetails();
				if (null != productDetaillst)
			
				{
					final ProductDetail productDetail = new ProductDetail();
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductID());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setStarFlag(listResultSet.getStarFlag());		
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductShortDescription(listResultSet.getProductShortDescription());
					productDetail.setProductPrice(listResultSet.getPrice());
					productDetail.setRow_Num(listResultSet.getRowNum());
					productDetaillst.add(productDetail);
					categoryInfo.setProductDetails(productDetaillst);
				}
			}
			else
			{
				categoryInfo = new Category();
				categoryInfo.setParentCategoryId(listResultSet.getParentCategoryID());
				categoryInfo.setParentCategoryName(listResultSet.getParentCategoryName());
				productDetaillst = new ArrayList<ProductDetail>();
				final ProductDetail productDetail = new ProductDetail();
				if (null != listResultSet.getProductId())
				{
					productDetail.setProductId(listResultSet.getProductId());
					productDetail.setUserProductId(listResultSet.getUserProductID());
					productDetail.setProductName(listResultSet.getProductName());
					productDetail.setProductShortDescription(listResultSet.getProductShortDescription());
					productDetail.setStarFlag(listResultSet.getStarFlag());		
					productDetail.setProductImagePath(listResultSet.getProductImagePath());
					productDetail.setProductPrice(listResultSet.getPrice());
					productDetail.setRow_Num(listResultSet.getRowNum());
					productDetaillst.add(productDetail);
					categoryInfo.setProductDetails(productDetaillst);
				}
				categoryMap.put(key, categoryInfo);
			}
		}

		final Set<Map.Entry<String, Category>> set = categoryMap.entrySet();

		final ArrayList<Category> categoryInfolst = new ArrayList<Category>();

		for (Map.Entry<String, Category> entry : set)
		{
			categoryInfolst.add(entry.getValue());
		}

		shoppingListCategoryList.setCategoryInfolst(categoryInfolst);
		return shoppingListCategoryList;
	}
	
}
