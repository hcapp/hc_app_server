package com.scansee.shoppinglist.service;

import static com.scansee.common.util.Utility.formResponseXml;
import static com.scansee.common.util.Utility.isEmpty;
import static com.scansee.common.util.Utility.isEmptyOrNullString;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.cellfire.webservice.QBridgeServiceStub.ArrayOfInt;
import com.scansee.cellfire.webservice.QBridgeServiceStub.QClipRequests;
import com.scansee.cellfire.webservice.QBridgeServiceStub.QClipResponse;
import com.scansee.cellfire.webserviceclient.CellFireWebServiceClient;
import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.constants.CellFireResponseMessage;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.externalapi.ExternalAPIManager;
import com.scansee.common.helper.BaseDAO;
import com.scansee.common.helper.XstreamParserHelper;
import com.scansee.common.pojos.AddRemoveCLR;
import com.scansee.common.pojos.AddRemoveSBProducts;
import com.scansee.common.pojos.AddSLRequest;
import com.scansee.common.pojos.AppConfiguration;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.CellFireAPIKey;
import com.scansee.common.pojos.CellFireRequest;
import com.scansee.common.pojos.CouponsDetails;
import com.scansee.common.pojos.ExternalAPISearchParameters;
import com.scansee.common.pojos.ExternalAPIVendor;
import com.scansee.common.pojos.FindNearByDetails;
import com.scansee.common.pojos.FindNearByIntactResponse;
import com.scansee.common.pojos.LoyaltyDetails;
import com.scansee.common.pojos.MainSLRetailerCategory;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.ProductDetailsRequest;
import com.scansee.common.pojos.ProductRatingReview;
import com.scansee.common.pojos.ProductReview;
import com.scansee.common.pojos.RebateDetails;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.ShoppingListDetails;
import com.scansee.common.pojos.ShoppingListResultSet;
import com.scansee.common.pojos.ShoppingListRetailerCategoryList;
import com.scansee.common.pojos.ThisLocationRequest;
import com.scansee.common.pojos.UpdateUserInfo;
import com.scansee.common.pojos.UserNotesDetails;
import com.scansee.common.pojos.UserRatingInfo;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.common.util.Utility;
import com.scansee.externalapi.common.findnearby.pojos.FindNearByRequest;
import com.scansee.externalapi.common.pojos.ExternalAPIErrorLog;
import com.scansee.externalapi.common.pojos.ExternalAPIInformation;
import com.scansee.externalapi.findNearBy.FindNearByService;
import com.scansee.externalapi.findNearBy.FindNearByServiceImpl;
import com.scansee.externalapi.onlinestores.OnlineStoresService;
import com.scansee.externalapi.onlinestores.OnlineStoresServiceImpl;
import com.scansee.externalapi.shopzilla.pojos.OnlineStores;
import com.scansee.externalapi.shopzilla.pojos.OnlineStoresMetaData;
import com.scansee.externalapi.shopzilla.pojos.OnlineStoresRequest;
import com.scansee.firstuse.dao.FirstUseDAO;
import com.scansee.manageloyaltycard.dao.ManageLoyaltyCardDAO;
import com.scansee.managesettings.dao.ManageSettingsDAO;
import com.scansee.ratereview.dao.RateReviewDAO;
import com.scansee.shoppinglist.dao.ShoppingListDAO;
import com.thoughtworks.xstream.alias.ClassMapper.Null;

/**
 * This class is used to display user shopping list items depending on
 * retailers.
 * 
 * @author shyamsundara_hm
 */

public class ShoppingListServiceImpl implements ShoppingListService

{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ShoppingListServiceImpl.class);

	/**
	 * variable for checking Debugging.
	 */
	private static final boolean ISDEBUGENABLED = LOG.isDebugEnabled();

	/**
	 * Instance variable for Shopping list DAO instance.
	 */

	private ShoppingListDAO shoppingListDao;

	/**
	 * Instance variable for baseDao.
	 */

	private BaseDAO baseDao;

	/**
	 * Instance variable for Manage Settings DAO instance.
	 */
	private ManageSettingsDAO manageSettingsDao;

	/**
	 * Instance variable for Manage Settings DAO instance.
	 */
	private RateReviewDAO rateReviewDao;

	/**
	 * Instance variable for Manage Loyalty DAO instance.
	 */
	private ManageLoyaltyCardDAO manageLoyaltyCardDAO;
	
	/**
	 * Variable type for FirstUse
	 */
	private FirstUseDAO firstUseDao;

	/**
	 * holds the number of online Stores values;
	 */

	private String totalOnlineStores = null;

	/**
	 * holds the number of online Stores values;
	 */

	private String minimumPrice = null;

	/**
	 * sets the ShoppingListDAO DAO.
	 * 
	 * @param shoppingListDao
	 *            The instance for ShoppingListDAO
	 */
	public void setShoppingListDao(ShoppingListDAO shoppingListDao)
	{
		this.shoppingListDao = shoppingListDao;
	}

	/**
	 * for setting tManageSettingsDao.
	 * 
	 * @param manageSettingsDao
	 *            the manageSettingsDao to set
	 */
	public void setManageSettingsDao(ManageSettingsDAO manageSettingsDao)
	{
		this.manageSettingsDao = manageSettingsDao;
	}

	/**
	 * Setter for BaseDao.
	 * 
	 * @param baseDao
	 *            BaseDao object.
	 */

	public void setBaseDao(BaseDAO baseDao)
	{
		this.baseDao = baseDao;
	}

	/**
	 * Setter for rate review DAO.
	 * 
	 * @param rateReviewDao
	 *            the rateReviewDao to set
	 */
	public void setRateReviewDao(RateReviewDAO rateReviewDao)
	{
		this.rateReviewDao = rateReviewDao;
	}

	/**
	 * @param manageLoyaltyCardDAO
	 *            the manageLoyaltyCardDAO to set
	 */
	public void setManageLoyaltyCardDAO(ManageLoyaltyCardDAO manageLoyaltyCardDAO)
	{
		this.manageLoyaltyCardDAO = manageLoyaltyCardDAO;
	}
	
	/**
	 * Setter method for FirstUseDAO.
	 * 
	 * @param firstUseDAO
	 *            the object of type FirstUseDAO
	 */
	public void setFirstUseDao(FirstUseDAO firstUseDao)
	{
		this.firstUseDao = firstUseDao;
	}

	/**
	 * This is a service Method for fetching main shopping list items for the
	 * given userId.This method validates the userId, if it is valid it will
	 * call the DAO method.If userId is null then it is invalid request.
	 * 
	 * @param userId
	 *            for which main shopping list items need to be fetched.If
	 *            userId is null then it is invalid request.
	 * @return XML containing main shopping list items in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("static-access")
	@Override
	public String getMainShoppingListItems(Integer userId) throws ScanSeeException
	{

		final String methodName = "getMainShoppingListItems";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (userId == null)
		{
			LOG.info("Validation failed as User Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			ArrayList<ShoppingListResultSet> shoppingListResultSet;
			shoppingListResultSet = shoppingListDao.getMainShoppingListItems(userId);
			if (null != shoppingListResultSet && !shoppingListResultSet.isEmpty())
			{
				final ShoppingListHelper spoppingListHelper = new ShoppingListHelper();
				final ShoppingListDetails shoppingListDetails = spoppingListHelper.getShoppingList(shoppingListResultSet);
				response = XstreamParserHelper.produceXMlFromObject(shoppingListDetails);
				response = response.replaceAll("<ShoppingListDetails>", "<MasterShoppingList>");
				response = response.replaceAll("</ShoppingListDetails>", "</MasterShoppingList>");
			}
			else
			{
				LOG.info("records are not found for the userid", userId);
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOSHOPPINGLISTPRODUCTTEXT);
			}
		}
		if (ISDEBUGENABLED)
		{
			LOG.info("MainShoppingList Response Xml {} for user", response, userId);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a service Method for fetching product information for the given
	 * userId and product search key. This method validates the userId and
	 * prodSearchKey if it is valid it will call the DAO method else returns
	 * Insufficient Data error message.
	 * 
	 * @param userId
	 *            for which products information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @param productSearchkey
	 *            for which products information need to be fetched.If
	 *            prodSearchKey is null then it is invalid request.
	 * @return XML containing products information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@Override
	public String getProductsForShopping(Integer userId, String productSearchkey) throws ScanSeeException
	{

		final String methodName = "getProductsForShopping of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		if (userId == null || isEmptyOrNullString(productSearchkey))
		{
			LOG.info("Validation failed as User Id/productSearchkey is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
			return response;
		}

		final ProductDetails productDetails = shoppingListDao.getProductsForShopping(userId, productSearchkey);
		if (null != productDetails)
		{
			response = XstreamParserHelper.produceXMlFromObject(productDetails);
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * The service method for Adding the product to Main Shopping List. This
	 * method validates the userId if it is valid it will call the DAO method
	 * else returns Insufficient Data error message.
	 * 
	 * @param xml
	 *            The XML with Add to Shopping List request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@SuppressWarnings("static-access")
	@Override
	public String addProductMainToShopList(String xml) throws ScanSeeException
	{
		final String methodName = "addProductMainToShopList of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AddSLRequest addSLRequest = (AddSLRequest) streamHelper.parseXmlToObject(xml);
		if (null != addSLRequest)
		{
			if (null == addSLRequest.getUserId())
			{
				LOG.info("Validation failed as User Id is not available");
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
			}
			else
			{
				final ProductDetails userProdIdList = shoppingListDao.addProductMainToShopList(addSLRequest);
				if (null != userProdIdList)
				{
					if (null == addSLRequest.getIsWishlst())
					{
						if (userProdIdList.getProductIsThere() != null)
						{
							if (userProdIdList.getProductIsThere() == 1)
							{
								LOG.info("Returuning Succss response  After adding");
								final String userProdIDxml = streamHelper.produceXMlFromObject(userProdIdList);
								response = Utility.formResponseXml(ApplicationConstants.DUPLICATEPRODUCTCODE,
										ApplicationConstants.DUPLICATEPRODUCTTEXT, "UserProductIds", userProdIDxml);
							}
							else
							{
								LOG.info("Returuning Succss response  After adding");
								final String userProdIDxml = streamHelper.produceXMlFromObject(userProdIdList);
								response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE,
										ApplicationConstants.ADDPRODUCTMAINTOSHOPPINGLIST, "UserProductIds", userProdIDxml);
							}
						}
					}
					else
					{
						LOG.info("Returuning Succss response  After adding");
						response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.ADDPRODUCTSHOPPINGLISTFORDEBUG);
					}

				}
				else
				{
					response = Utility
							.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
				}
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method for removing to Shopping List product. This method validates
	 * the userId if it is valid it will call the DAO method else returns
	 * Insufficient Data error message.
	 * 
	 * @param xml
	 *            The XML with delete from Shopping List request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public String dleteProductMainShopList(String xml) throws ScanSeeException
	{
		final String methodName = "dleteProductMainShopList of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AddSLRequest addSLRequest = (AddSLRequest) streamHelper.parseXmlToObject(xml);
		if (null != addSLRequest)
		{

			if (null == addSLRequest.getUserId())
			{
				LOG.info("Validation failed because User Id is not available");
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
			}
			else if (addSLRequest.getProductDetails() != null && !addSLRequest.getProductDetails().isEmpty())
			{
				for (int i = 0; i < addSLRequest.getProductDetails().size(); i++)
				{
					if (addSLRequest.getProductDetails().get(i).getUserProductId() == null)

					{
						LOG.info("Validation failed because User Product  Id is not available");
						return response = Utility
								.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
					}
				}

				response = shoppingListDao.dleteProductMainShopList(addSLRequest);
				if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
				{
					response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.REMOVE);
				}
				else
				{
					response = Utility
							.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
				}

			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is a Service Method for fetching product information for the given
	 * userId,productId and retailId.This method validates the userId, productId
	 * and retailId if it is valid it will call the DAO method else returns
	 * Insufficient Data error message.
	 * 
	 * @param userId
	 *            for which product information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @param productId
	 *            for which product information need to be fetched.If productId
	 *            is null then it is invalid request.
	 * @param retailId
	 *            for which product information need to be fetched.If retailId
	 *            is null then it is invalid request.
	 * @return XML containing product information in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String getProductsInfo(Integer userId, Integer productId, Integer retailId, Integer saleListID, Integer prodListID, Integer mainMenuID, Integer scanID) throws ScanSeeException

	{

		final String methodName = "getProductsInfo of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		if (null == userId || null == productId)
		{
			LOG.info("Validation failed because UserId/ProductId is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			final ProductDetail productDetail = baseDao.getProductDetail(userId, productId, retailId, saleListID, prodListID, mainMenuID, scanID);
			if (null != productDetail)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetail);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a Service Method for fetching today shopping list for the given
	 * userId.This method validates the input userId if it is valid it will call
	 * the DAO method else returns Insufficient Data error message.
	 * 
	 * @param userId
	 *            for which today shopping list need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return XML containing today shopping list in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@SuppressWarnings("static-access")
	@Override
	public String getTodaySLProducts(Integer userId) throws ScanSeeException
	{

		final String methodName = "getTodaySLProducts";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (null == userId)
		{
			LOG.info("Validation failed because User Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			ArrayList<MainSLRetailerCategory> mainSLRetailerCategorylst = null;
			ShoppingListRetailerCategoryList shoppingListRetailerCategoryList = null;
			mainSLRetailerCategorylst = shoppingListDao.getTodaySLProducts(userId);
			if (null != mainSLRetailerCategorylst && !mainSLRetailerCategorylst.isEmpty())
			{
				final ShoppingListHelper spoppingListHelper = new ShoppingListHelper();
				shoppingListRetailerCategoryList = spoppingListHelper.getShoppingRetailersList(mainSLRetailerCategorylst);
				response = XstreamParserHelper.produceXMlFromObject(shoppingListRetailerCategoryList);
			}
			else
			{
				LOG.info("No records found for the userid", userId);
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORETAILERFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This is a Service Method for finding near by retailers. This method
	 * validates the input xml if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            containing input information need to be find near by
	 *            retailers.
	 * @return XML containing near by retailers as response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@SuppressWarnings("static-access")
	@Override
	public String findNearByRetailers(String xml) throws ScanSeeException
	{

		final String methodName = "findNearByRetailers of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();

		final ThisLocationRequest thisLocationRequest = (ThisLocationRequest) streamHelper.parseXmlToObject(xml);
		ArrayList<ShoppingListResultSet> shoppingListResultSet;
		shoppingListResultSet = shoppingListDao.fetchNearByDetails(thisLocationRequest);
		if (null != shoppingListResultSet && !shoppingListResultSet.isEmpty())
		{
			final ShoppingListHelper spoppingListHelper = new ShoppingListHelper();
			final ShoppingListDetails shoppingListDetails = spoppingListHelper.getShoppingListDetails(shoppingListResultSet);
			response = XstreamParserHelper.produceXMlFromObject(shoppingListDetails);

		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method used for adding UnassignedProduct.
	 * 
	 * @param xml
	 *            The xml with ProductDetails Request.
	 * @return ArrayList of ShoppingListResultSet.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public String addUnassigedProd(String xml) throws ScanSeeException
	{

		final String methodName = "addUnassigedProd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ProductDetailsRequest productDetailsRequest = (ProductDetailsRequest) streamHelper.parseXmlToObject(xml);
		if (isEmptyOrNullString(productDetailsRequest.getProductName()) || productDetailsRequest.getUserId() == null)
		{
			LOG.info("Validation failed because User Id/product Name is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			if (null == productDetailsRequest.getAddedTo())
			{
				productDetailsRequest.setAddedTo("F)");// it will added to
														// favorites(f)
			}
			final ProductDetails userProdIdList = shoppingListDao.addUnassignedPro(productDetailsRequest);
			if (null != userProdIdList)
			{
				final String userProdIDxml = XstreamParserHelper.produceXMlFromObject(userProdIdList);
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.ADDUNASSIGNEDPRODUCT, "UserProductIds",
						userProdIDxml);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;

	}

	/**
	 * This is a Service Method for fetching the products from shopping basket
	 * for the given userId.This method validates the input userId if it is
	 * valid it will call the DAO method.
	 * 
	 * @param userId
	 *            for which the products from shopping basket need to be
	 *            fetched.If userId is null then it is invalid request.
	 * @return String response as a string contains all shopping basket
	 *         products. if no products found No records found code will be
	 *         returned.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String getShoppingBasketProds(Integer userId) throws ScanSeeException
	{

		final String methodName = "getShoppingBasketProds of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		ArrayList<ShoppingListResultSet> shoppingListResultSet;
		if (null == userId)
		{
			LOG.info("Validation failed because User Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			shoppingListResultSet = shoppingListDao.fetchShoppingBasketProds(userId);
			if (null != shoppingListResultSet && !shoppingListResultSet.isEmpty())
			{

				final ShoppingListDetails shoppingListDetails = ShoppingListHelper.getShoppingList(shoppingListResultSet);

				response = XstreamParserHelper.produceXMlFromObject(shoppingListDetails);
				response = response.replaceAll("<ShoppingListDetails>", "<ShoppingCartDetails>");
				response = response.replaceAll("</ShoppingListDetails>", "</ShoppingCartDetails>");

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a Service Method for add/remove SB products.This method validates
	 * the input parameters and returns validation error if validation fails.
	 * and calls DAO methods to add/Remove product to/from cart.
	 * 
	 * @param xml
	 *            products to be added/removed.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String addRemoveSBProducts(String xml) throws ScanSeeException
	{

		final String methodName = "addRemoveSBProducts of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AddRemoveSBProducts addRemObj = (AddRemoveSBProducts) streamHelper.parseXmlToObject(xml);
		if (isEmptyOrNullString(addRemObj.getUserId()))
		{
			LOG.info("Request does not contain userId");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			response = shoppingListDao.addRemoveSBProducts(addRemObj);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.ADDREMOVESBPRODUCTS);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is a Service method for fetching the products from shopping basket
	 * for the given userId.This method validates userId and returns validation
	 * error if validation fails. and calls DAO methods.
	 * 
	 * @param userId
	 *            cart products for the user.
	 * @return String XML with list of Products.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String getShopBasketProducts(String xml) throws ScanSeeException
	{

		final String methodName = "getShopBasketProducts of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserTrackingData objUTData = (UserTrackingData) streamHelper.parseXmlToObject(xml);
		if (null == objUTData.getUserID())
		{
			LOG.info("Validation failed because User Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			Integer mainMenuID = null;
			if (objUTData.getModuleID() != null) {
				UserTrackingData objUserTrackingData = new UserTrackingData();
				objUserTrackingData.setUserID(objUTData.getUserID());
				objUserTrackingData.setModuleID(objUTData.getModuleID());
				objUserTrackingData.setPostalCode(objUTData.getZipcode());
				objUserTrackingData.setLatitude(objUTData.getLatitude());
				objUserTrackingData.setLongitude(objUTData.getLongitude());
				mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);
			}
			if (null == objUTData.getLastVisited())
			{
				objUTData.setLastVisited(0);
			}
			final AddRemoveSBProducts addRemoveSBProducts = shoppingListDao.getShopBasketProducts(objUTData, ApplicationConstants.TODAYSHOPLISTPAGINATIOM);
			
			if (null != addRemoveSBProducts)
			{
				addRemoveSBProducts.setMainMenuID(mainMenuID);
				response = XstreamParserHelper.produceXMlFromObject(addRemoveSBProducts);
			}
			else
			{
				String mmID = null;
				if(mainMenuID != null){
					mmID = mainMenuID.toString();
				} else{
					mmID = ApplicationConstants.STRING_ZERO;
				}
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND,	ApplicationConstants.NOPRODUCTFOUNDTEXT, "mainMenuID", mmID);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This is a Service method for the Adding a Coupon(to the Coupon
	 * Gallery).This method validates the input parameters and returns
	 * validation error if validation fails. calls DAO methods to add the
	 * coupons.
	 * 
	 * @param xml
	 *            contains coupon info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String couponAdd(String xml) throws ScanSeeException
	{

		final String methodName = "couponAdd of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AddRemoveCLR clrAddRemove = (AddRemoveCLR) streamHelper.parseXmlToObject(xml);

		if (null == clrAddRemove.getUserId() || null == clrAddRemove.getCouponId())

		{
			LOG.info("Validation failed because User Id/coupon Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			response = shoppingListDao.addCoupon(clrAddRemove);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.ADDCOUPON);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is Service method for adding Loyalty (to the Loyalty Gallery).This
	 * method validates the input parameters and returns validation error if
	 * validation fails. calls DAO methods to add the Loyalty.
	 * 
	 * @param xml
	 *            contains Loyalty info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String addLoyalty(String xml) throws ScanSeeException
	{
		final String methodName = " addLoyalty of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		String responseCode = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AddRemoveCLR clrAddRemove = (AddRemoveCLR) streamHelper.parseXmlToObject(xml);

		if (null == clrAddRemove.getUserId() || null == clrAddRemove.getloyaltyDealId() || null == clrAddRemove.getMerchantId())
		{
			LOG.info("Validation failed because User Id/loyatlty Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{

			LOG.info("CellFire Request Recieved From Client for Clip Coupon:::" + "\n Merchant ID" + clrAddRemove.getMerchantId() + "\n UserID"
					+ clrAddRemove.getUserId() + "\n Loyalty ID:" + clrAddRemove.getloyaltyDealId() + "\n");
			// boolean userExist =
			// manageSettingsDao.checkUserExist(clrAddRemove.getUserId());
			CellFireRequest cfr = manageLoyaltyCardDAO.getCardInfo(clrAddRemove);
			if (cfr.getCardNumber() != null)// card has not be registered by
											// user
			{
				CellFireAPIKey cellFireAPIKey = getExternalApiInformation();
				if (cellFireAPIKey.getApiKeyParameterValue() != null && cellFireAPIKey.getTrackingCodeParameterValue() != null
						&& null != cellFireAPIKey.getParnerId())
				{
					QClipResponse[] clipResponsesArray = clipCfCoupons(cellFireAPIKey, cfr);
					QClipResponse clipResp = clipResponsesArray[0];// hardcoding
																	// to 0 as
																	// respone
																	// will
																	// always
																	// have 1
																	// item.
					int statusCode = clipResp.getStatusCode();
					switch (statusCode)
					{
					case 0:
						clrAddRemove.setCardNumber(cfr.getCardNumber());
						if (cfr.isFirstTimeClip())
						{
							responseCode = ApplicationConstants.FISRTTIMECLIP;
							response = String.format(CellFireResponseMessage.FIRSTTIMECLIP, cfr.getWaitTime());
						}
						else
						{
							responseCode = ApplicationConstants.SUCCESSCODE;
							response = CellFireResponseMessage.CLIPSUCCESS;
						}
						shoppingListDao.addLoyalty(clrAddRemove);
						break;
					case -7:
						response = CellFireResponseMessage.CLIPFAILED;
						responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
						break;
					case -8:
						response = CellFireResponseMessage.INVALIDLOYALTYID;
						responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
						break;
					case -9:
						response = CellFireResponseMessage.UNAUTHORIZEDACCESS;
						responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
						break;
					case -10:
						response = CellFireResponseMessage.CLIPPEDBEFORE;
						responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
						break;
					case -11:
						response = CellFireResponseMessage.REDEEMEDBEFORE;
						responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
						break;
					case -12:
						response = CellFireResponseMessage.LATESTCONTENT;
						responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
						break;
					}

				}

			}
			else
			{
				response = CellFireResponseMessage.CARDNOTREG;
				responseCode = ApplicationConstants.CARDNOTREGISTERED;
			}
			response = Utility.formResponseXml(responseCode, response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is Service method for the removing a Coupon(from the Coupon
	 * Gallery).This method validates the input parameters and returns
	 * validation error if validation fails. calls DAO methods to remove the
	 * coupons.
	 * 
	 * @param xml
	 *            contains coupon info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String removeCoupon(String xml) throws ScanSeeException
	{

		final String methodName = " removeCoupon of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;

		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AddRemoveCLR couponAddRemove = (AddRemoveCLR) streamHelper.parseXmlToObject(xml);

		if (null == couponAddRemove.getUserId() || null == couponAddRemove.getCouponId())

		{
			LOG.info("Validation failed because User Id/coupon  Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			response = shoppingListDao.removeCoupon(couponAddRemove);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.REMOVE);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is Service method for adding Rebate (to the Rebate Gallery).This
	 * method validates the input parameters and returns validation error if
	 * validation fails. calls DAO methods to add the Rebate.
	 * 
	 * @param xml
	 *            contains Rebate info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String addRebate(String xml) throws ScanSeeException
	{

		final String methodName = " addRebate of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AddRemoveCLR clrAddRemove = (AddRemoveCLR) streamHelper.parseXmlToObject(xml);

		if (null == clrAddRemove.getUserId() || null == clrAddRemove.getRebateId())

		{
			LOG.info("Validation failed because User Id/Rebate  Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			response = shoppingListDao.addRebate(clrAddRemove);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.ADDREBATE);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is Service method for removing Loyalty (from the Loyalty
	 * Gallery).This method validates the input parameters and returns
	 * validation error if validation fails. calls DAO methods to remove the
	 * Rebate.
	 * 
	 * @param xml
	 *            contains Rebate info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String removeLoyalty(String xml) throws ScanSeeException
	{

		final String methodName = " removeLoyalty of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AddRemoveCLR clrAddRemove = (AddRemoveCLR) streamHelper.parseXmlToObject(xml);

		if (null == clrAddRemove.getUserId() || null == clrAddRemove.getloyaltyDealId())

		{
			LOG.info("Validation failed because User Id/Loyalty  Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			response = shoppingListDao.removeLoyalty(clrAddRemove);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.REMOVE);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is Service method for removing Rebate (from the Rebate Gallery).This
	 * method validates the input parameters and returns validation error if
	 * validation fails. calls DAO methods to remove the Loyalty.
	 * 
	 * @param xml
	 *            contains Loyalty info.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String removeRebate(String xml) throws ScanSeeException
	{
		final String methodName = " removeRebate of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;

		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AddRemoveCLR clrAddRemove = (AddRemoveCLR) streamHelper.parseXmlToObject(xml);

		if (null == clrAddRemove.getUserId() || null == clrAddRemove.getRebateId())

		{
			LOG.info("Validation failed because User Id/Rebate  Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			response = shoppingListDao.removeRebate(clrAddRemove);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.REMOVE);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This is service method for fetching the rebate info.This method validates
	 * the input parameters and returns validation error if validation fails.
	 * and calls DAO methods.
	 * 
	 * @param userId
	 *            requested user.
	 * @param productId
	 *            request product for rebate.
	 * @param retailId
	 *            flag for identifying.
	 * @return String XML with rebate info.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getRebateInfo(Integer userId, Integer productId, Integer retailId, Integer mainMenuID) throws ScanSeeException
	{

		final String methodName = "  getRebateInfo of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (null == userId || null == productId)

		{
			LOG.info("Validation failed because User Id/product  Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			final RebateDetails rebateDetails = shoppingListDao.fetchRebateInfo(userId, productId, retailId, mainMenuID);
			if (null != rebateDetails)
			{

				response = XstreamParserHelper.produceXMlFromObject(rebateDetails);

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is service method for fetching the loyalty info.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. and calls DAO methods.
	 * 
	 * @param userId
	 *            requested user.
	 * @param productId
	 *            request product for loyalty.
	 * @param retailId
	 *            flag for identifying.
	 * @return String XML with loyalty info.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getLoyaltyInfo(Integer userId, Integer productId, Integer retailId, Integer mainMenuID) throws ScanSeeException
	{

		final String methodName = "  getLoyaltyInfo of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (null == userId || null == productId)
		{
			LOG.info("Validation failed because User Id/product  Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			final LoyaltyDetails loyaltyDetails = shoppingListDao.fetchLoyaltyInfo(userId, productId, retailId, mainMenuID);
			if (null != loyaltyDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(loyaltyDetails);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is service method for fetching the coupon info.This method validates
	 * the input parameters and returns validation error if validation fails.
	 * and calls DAO methods to get the coupons for product.
	 * 
	 * @param userId
	 *            requested user.
	 * @param productId
	 *            request product for coupons.
	 * @param retailId
	 *            flag for identifying.
	 * @return String XML with coupon info.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String getCouponInfo(Integer userId, Integer productId, Integer retailId, Integer mainMenuID) throws ScanSeeException
	{

		final String methodName = "getCouponInfo of Service layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (null == userId || null == productId)
		{
			LOG.info("Validation failed because User Id/product  Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			final CouponsDetails couponDetails = shoppingListDao.fetchCouponInfo(userId, productId, retailId, mainMenuID);
			if (null != couponDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(couponDetails);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;

	}

	/**
	 * This is Service method for adds/removes the products to/from Today's
	 * shopping list.This method validates the input parameters and returns
	 * validation error if validation fails. calls DAO methods to add/remove the
	 * products to/from Today Shopping List.
	 * 
	 * @param xml
	 *            contains products information.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String addRemoveTodaySLProducts(String xml) throws ScanSeeException
	{

		final String methodName = "addRemoveTodaySLProducts of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AddSLRequest addRemObj = (AddSLRequest) streamHelper.parseXmlToObject(xml);
		if (null == addRemObj.getUserId())
		{
			LOG.info("Validation failed because User Id is not available");
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		if (!addRemObj.getProductDetails().isEmpty() && addRemObj.getProductDetails() != null)
		{

			for (int i = 0; i < addRemObj.getProductDetails().size(); i++)
			{

				if (addRemObj.getProductDetails().get(i).getUserProductId() == null)

				{
					response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
					return response;
				}
			}
			response = shoppingListDao.addRemoveTodaySLProducts(addRemObj);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SUCCESSRESPONSETEXT);
			}
			else if (ApplicationConstants.FAILURE.equalsIgnoreCase(response))
			{

				response = Utility.formResponseXml(ApplicationConstants.DUPLICATEPRODUCTCODE, ApplicationConstants.DUPLICATEPRODUCTTEXT);

			}
			else if (ApplicationConstants.SLADDPRODTLEXISTS.equalsIgnoreCase(response))
			{

				response = Utility.formResponseXml(ApplicationConstants.DUPLICATETSLPRODUCTCODE, ApplicationConstants.SLADDPRODTLEXISTS);

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is a Service Method for fetching shopping list and cart products for
	 * the given userId. This method validates the userId and returns validation
	 * error if validation fails.If it is valid it will call the DAO method.
	 * 
	 * @param userId
	 *            for which shopping list and cart products need to be
	 *            fetched.If userId is null then it is invalid request.
	 * @return XML containing shopping list and cart products in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("static-access")
	@Override
	public String getTodaysNMasterSL(Integer userId) throws ScanSeeException
	{
		final String methodName = " getTodaysNMasterSL of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		ArrayList<ShoppingListResultSet> shoppingListResultSet;
		if (null == userId)
		{
			LOG.info("Validation failed because User Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			shoppingListResultSet = shoppingListDao.getTodaysNMasterSL(userId);
			if (null != shoppingListResultSet && !shoppingListResultSet.isEmpty())
			{
				final ShoppingListHelper spoppingListHelper = new ShoppingListHelper();
				final ShoppingListDetails shoppingListDetails = spoppingListHelper.getShoppingList(shoppingListResultSet);
				response = XstreamParserHelper.produceXMlFromObject(shoppingListDetails);
				response = response.replaceAll("<ShoppingListDetails>", "<TodaysNMasterSLProdDetails>");
				response = response.replaceAll("</ShoppingListDetails>", "</TodaysNMasterSLProdDetails>");
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOSHOPPINGCARDPRODUCTTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is Service method for add User notes for shopping list.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. calls DAO methods to add user notes.
	 * 
	 * @param xml
	 *            contains user notes details.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String addUserNotes(String xml) throws ScanSeeException
	{

		final String methodName = " addUserNotes of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserNotesDetails userNotesDetails = (UserNotesDetails) streamHelper.parseXmlToObject(xml);
		if (null == userNotesDetails.getUserId())
		{
			LOG.info("Validation failed beacuse User Id/User notes is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			response = shoppingListDao.addUserNotesDetails(userNotesDetails);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.ADDUSERNOTES);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;

	}

	/**
	 * This is Service method for deleting User notes for shopping list.This
	 * method validates the input parameters and returns validation error if
	 * validation fails. calls DAO methods to delete user notes.
	 * 
	 * @param userId
	 *            requested user.
	 * @return String XML with success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String deleteUserNotes(Integer userId) throws ScanSeeException
	{

		final String methodName = "deleteUserNotes of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (null == userId)
		{
			LOG.info("Validation failed because User Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			response = shoppingListDao.deleteUserNotesDetails(userId);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.REMOVE);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is Service method for retrieve User notes for shopping listt.This
	 * method validates the userId and returns validation error if validation
	 * fails.
	 * 
	 * @param userId
	 *            for fetching user notes.
	 * @return String XML contains User notes info.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getUserNotesDetails(Integer userId) throws ScanSeeException
	{

		final String methodName = " getUserNotesDetails of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (null == userId)
		{
			LOG.info("Validation failed because User Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			final UserNotesDetails userNotesDetails = shoppingListDao.getUserNotesDetails(userId);
			if (null != userNotesDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(userNotesDetails);
			}
			else
			{

				response = formResponseXml(ApplicationConstants.NOUSERNOTES, ApplicationConstants.NORECORDSFOUND);

			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is Service method for retrieving find near by retailers.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. calls DAO methods to get the Retailers.
	 * 
	 * @param userID
	 *            requested user.
	 * @param productId
	 *            products to be searched for.
	 * @param latitude
	 *            current location of user.
	 * @param longitude
	 *            current location of user.
	 * @param postalCode
	 *            current location of user.
	 * @param radius
	 *            search distance.
	 * @return String XMl list of retailers.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String findNearBy(Integer userID, Integer productId, String latitude, String longitude, String postalCode, String radius, Integer mainMenuID)
			throws ScanSeeException
	{

		final String methodName = "findNearBy";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		FindNearByDetails findNearByDetails = null;
		String response = null;
		if (null == userID || null == productId || isEmpty(latitude) || isEmpty(longitude) || isEmpty(postalCode) || isEmpty(radius))
		{
			LOG.info("Validation failed because UserId/ProductId not available or Empty string for string parameter validation");
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			findNearByDetails = shoppingListDao.fetchNearByInfo(userID, productId, latitude, longitude, postalCode, radius, mainMenuID);
			if (null != findNearByDetails)
			{
				if (findNearByDetails.getFindNearBy())
				{
					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.FETCHNEARBYTEXT);
					response = response.replaceAll("<list>", "<FindNearByDetails>");
					response = response.replaceAll("</list>", "</FindNearByDetails>");
				}
				else
				{
					response = XstreamParserHelper.produceXMlFromObject(findNearByDetails.getFindNearByDetail());
					response = response.replaceAll("<list>", "<FindNearByDetails>");
					response = response.replaceAll("</list>", "</FindNearByDetails>");

				}

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORETAILERFOUNDTEXT);
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is Service method for find near by retailers based on lowest
	 * price.This method validates the input parameters and returns validation
	 * error if validation fails. calls DAO methods to get the number of
	 * Retailers and lowest price of the products searching for.
	 * 
	 * @param userID
	 *            requested user.
	 * @param productId
	 *            products to be searched for.
	 * @param latitude
	 *            current location of user.
	 * @param longitude
	 *            current location of user.
	 * @param postalcode
	 *            current location of user.
	 * @param radius
	 *            search distance.
	 * @param page
	 *            page.
	 * @return String XMl list of retailers.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String findNearByLowestPrice(Integer userID, Integer productId, Double latitude, Double longitude, String postalcode, Integer radius,
			Integer page, Integer mainMenuID) throws ScanSeeException
	{

		final String methodName = " findNearByLowestPrice of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (null == userID || null == productId || isEmpty(postalcode))
		{
			LOG.info("Validation failed because UserId/ProductId not available or Empty string validation");
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		final FindNearByIntactResponse findNearByDetailsResultSet = findRetailersLocalDataBase(userID, latitude, productId, longitude, postalcode,
				radius, mainMenuID);

		if (findNearByDetailsResultSet.isFindNearBy())
		{

			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.FETCHNEARBYTEXT);
			return response;

		}
		else if (null != findNearByDetailsResultSet.getFindNearByDetails() && null != findNearByDetailsResultSet.getFindNearByMetaData())
		{

			response = XstreamParserHelper.produceXMlFromObject(findNearByDetailsResultSet);
		}

		else
		{
			response = findRetailersExternalAPI(userID, productId, latitude, longitude, radius, page, postalcode);
		}
		if (null == response)
		{

			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORETAILERFOUNDTEXT);
		}
		return response;
	}

	/**
	 * This is Service method for finding retailers local database.This method
	 * validates the input parameters and returns validation error if validation
	 * fails. calls DAO methods to get the number of Retailers and lowest price
	 * of the products searching for.
	 * 
	 * @param userID
	 *            requested user.
	 * @param latitude
	 *            current location of user.
	 * @param productId
	 *            products to be searched for.
	 * @param longitude
	 *            current location of user.
	 * @param postalcode
	 *            current location of user.
	 * @param radius
	 *            search distance.
	 * @return String XMl list of retailers.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public FindNearByIntactResponse findRetailersLocalDataBase(Integer userID, Double latitude, Integer productId, Double longitude,
			String postalcode, int radius, Integer mainMenuID) throws ScanSeeException
	{
		final String methodName = " findRetailersLocalDataBase ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		LOG.info("Querying Local DataBase for Retailers");

		final FindNearByIntactResponse findNearByDetailsResultSet = shoppingListDao.fetchNearByLowestPrice(userID, latitude, productId, longitude,
				postalcode, radius, mainMenuID);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return findNearByDetailsResultSet;
	}

	/**
	 * This is Service method for find near by retailers based on lowest
	 * price.This method validates the input parameters and returns validation
	 * error if validation fails. calls DAO methods to get the number of
	 * Retailers and lowest price of the products searching for.
	 * 
	 * @param userID
	 *            requested user.
	 * @param latitude
	 *            current location of user.
	 * @param productId
	 *            products to be searched for.
	 * @param longitude
	 *            current location of user.
	 * @param postalcode
	 *            current location of user.
	 * @param radius
	 *            search distance.
	 * @return String XMl list of retailers.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public String findRetailersExternalAPI(Integer userID, Integer productId, Double latitude, Double longitude, Integer radius, Integer page,
			String postalcode) throws ScanSeeException
	{
		final String methodName = " findRetailersExternalAPI ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		LOG.info("::::::Invocation of External API::::::::::");
		if (null == userID || null == productId || isEmpty(postalcode))
		{
			LOG.info("Validation failed because UserId/ProductId not available or Empty string validation");
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}

		String response = null;
		final FindNearByRequest findNearByRequest = new FindNearByRequest();
		ProductDetail productInfo = null;
		ExternalAPIInformation externalAPIInformation = new ExternalAPIInformation();
		UpdateUserInfo updateUserInfo = null;
		String externalAPIURL;
		@SuppressWarnings("unused")
		ExternalAPIErrorLog externalAPIErrorLog = null;
		productInfo = shoppingListDao.getProductInfoforExternalAPI(productId);
		updateUserInfo = manageSettingsDao.fetchUserInfo(userID, false);

		if (null != productInfo)
		{

			if (null != productInfo.getBarCode() && !productInfo.getBarCode().equals(ApplicationConstants.NOTAPPLICABLE))
			{

				findNearByRequest.setUpcCode(productInfo.getBarCode());
			}
			else if (null != productInfo.getSkuCode() && !productInfo.getSkuCode().equals(ApplicationConstants.NOTAPPLICABLE))
			{
				findNearByRequest.setSkuCode(productInfo.getSkuCode());
			}
			else if (null != productInfo.getProductName() && !productInfo.getProductName().equals(ApplicationConstants.NOTAPPLICABLE))
			{
				findNearByRequest.setKeywords(productInfo.getProductName());
			}
			else
			{
				// No product Information is available to invoke external API
				LOG.info("No product Information is available to invoke external API");
				return null;
			}

			if (null != latitude && null != longitude)
			{
				findNearByRequest.setLatitude(String.valueOf(latitude));
				findNearByRequest.setLongiTude(String.valueOf(longitude));
			}
			else
			{
				if (null == postalcode)
				{
					if (null != updateUserInfo)
					{
						if (null != updateUserInfo.getPostalCode())
						{
							findNearByRequest.setZipCode(updateUserInfo.getPostalCode());
						}

					}
					else
					{
						LOG.info("Either Latitude and Longitude or ZipCode Available to invoke External API");
						return null;
					}
				}
				else
				{
					findNearByRequest.setZipCode(postalcode);
				}

			}

			findNearByRequest.setRadius(String.valueOf(radius));
			findNearByRequest.setFormat(ApplicationConstants.EXTERNALAPIFORMAT);
			findNearByRequest.setPage(String.valueOf(page));
			findNearByRequest.setUserId(userID);
			findNearByRequest.setValues();

			final FindNearByService findNearByService = new FindNearByServiceImpl();

			externalAPIInformation = getExternalAPIInformation(ApplicationConstants.FINDNEARBYMODULENAME);

			if (null != externalAPIInformation.getSerchParameters() && null != externalAPIInformation.getVendorList())
			{
				LOG.info("Invoking External API for Module::::::::" + ApplicationConstants.FINDNEARBYMODULENAME + " With Parameters:Product ID:"
						+ productId + " Product Name:" + productInfo.getProductName() + " Latitude::" + latitude + " Logitude::" + longitude
						+ " Radius::" + radius);

				response = findNearByService.processFindNearByRequest(externalAPIInformation, findNearByRequest);
				externalAPIURL = findNearByService.getApiURL();
				if (null != externalAPIURL)
				{
					shoppingListDao.insertExternalAPIURL(externalAPIURL, userID);

				}

				if (findNearByService.getResponsecode() != 11000)
				{
					response = null;
				}
				if (ApplicationConstants.EXTERNALAPIFAILURE == findNearByService.getResponsecode())
				{
					externalAPIErrorLog = findNearByService.getExternalAPIErrors();
					// shoppingListDao.LogExternalApiErrorMessages(externalAPIErrorLog);
				}

				LOG.info("XML Response from External API:::::" + response);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method is used to get external API information.
	 * 
	 * @param moduleName
	 *            -Requested module name.
	 * @return ExternalAPIInformation
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public ExternalAPIInformation getExternalAPIInformation(String moduleName) throws ScanSeeException
	{
		final String methodName = " getExternalAPIInformation ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ExternalAPIManager externalAPIManager = new ExternalAPIManager(moduleName, shoppingListDao);
		final ExternalAPIInformation externalAPIInformation = externalAPIManager.getExternalAPIInformation();
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return externalAPIInformation;
	}

	/**
	 * This method validates the input parameters and returns validation error
	 * if validation fails. calls DAO methods to removes the products from Cart.
	 * 
	 * @param userID
	 *            The userID entered by User.
	 * @param cartLatitude
	 *            The Latitude value(required for entry in
	 *            UserShoppingCartHistoryProduct table).
	 * @param cartLongitude
	 *            The Longitude value (required for entry in
	 *            UserShoppingCartHistoryProduct table).
	 * @return XML with success or error code based on success or failure of
	 *         operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application
	 */

	public String checkOutFromBasket(Integer userID, float cartLatitude, float cartLongitude) throws ScanSeeException
	{
		final String methodName = " checkOutFromBasket of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (null == userID)
		{
			LOG.info("Validation failed because User Id is not available");
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			response = shoppingListDao.checkOutFromBasket(userID, cartLatitude, cartLongitude);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.REMOVE);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to get the products media info
	 * based on the media type.
	 * 
	 * @param productID
	 *            requested product.
	 * @param mediaType
	 *            type of media.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String getMediaDetails(Integer productID, String mediaType, Integer productListID) throws ScanSeeException
	{
		final String methodName = " getMediaDetails of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		if (isEmptyOrNullString(mediaType))
		{
			LOG.info("Validation failed mediaType is not available");
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{
			final ProductDetails productDetails = shoppingListDao.getProductMediaDetails(productID, mediaType, productListID);
			if (null != productDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetails);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOMEDIA);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is a service Method for fetching master shopping list retailers and
	 * categories for the given userId. This method checks for mandatory fields,
	 * if any mandatory fields are not available returns Insufficient Data error
	 * message else calls the DAO method..
	 * 
	 * @param userId
	 *            for which master shopping list retailers and categories need
	 *            to be fetched.If userId is null then it is invalid request.
	 * @return XML containing master shopping list items in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 **/

	@SuppressWarnings("static-access")
	@Override
	public String getMSLRetailerCategorylst(Integer userId) throws ScanSeeException
	{
		final String methodName = "getMSLRetailerCategorylst";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (null == userId)
		{
			LOG.info("Validation failed because User Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			ArrayList<MainSLRetailerCategory> mainSLRetailerCategorylst = null;
			ShoppingListRetailerCategoryList shoppingListRetailerCategoryList = null;
			mainSLRetailerCategorylst = shoppingListDao.getMasterSLRetailerCategoryDetails(userId);
			if (null != mainSLRetailerCategorylst && !mainSLRetailerCategorylst.isEmpty())
			{
				final ShoppingListHelper spoppingListHelper = new ShoppingListHelper();
				shoppingListRetailerCategoryList = spoppingListHelper.getShoppingRetailersList(mainSLRetailerCategorylst);
				response = XstreamParserHelper.produceXMlFromObject(shoppingListRetailerCategoryList);
			}
			else
			{
				LOG.info("No records found for the userid", userId);
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORETAILERFOUNDTEXT);
			}

			if (ISDEBUGENABLED)
			{
				LOG.info("MainShoppingList Response Xml {} for user {}", response, userId);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a service Method for fetching master shopping list products for
	 * the given input as XML. This method validates the input XML, if it is
	 * valid it will call the DAO method.
	 * 
	 * @param xml
	 *            containing input information need to be fetched the MSL
	 *            products .
	 * @return XML containing master shopping list products in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String getMSLProductslst(String xml) throws ScanSeeException
	{
		final String methodName = "getMSLProductslst of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ProductDetailsRequest productDetailsRequest = (ProductDetailsRequest) streamHelper.parseXmlToObject(xml);
		if (null == productDetailsRequest.getUserId())
		{
			LOG.info("Request does not contain proper inforamtion");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			final ProductDetails productDetailslst = shoppingListDao.getMSLProcuctDetailslst(productDetailsRequest);
			if (null != productDetailslst)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetailslst);
			}
			else
			{
				LOG.info("No records found for userid", xml);
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOSHOPPINGLISTPRODUCTTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * The method calls JAXB Helper class method and parses the XML. The result
	 * is passed to ShoppingListDAO method.
	 * 
	 * @param xml
	 *            contains userId,productId,retailerId,lowerLimit which are
	 *            needed
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String fetchMSLCLRDetails(String xml) throws ScanSeeException
	{
		final String methodName = " fetchMSLCLRDetails of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final CLRDetails clrDetailsObj = (CLRDetails) streamHelper.parseXmlToObject(xml);

		if (clrDetailsObj.getClrC() == null)
		{
			clrDetailsObj.setClrC(0);

		}
		if (clrDetailsObj.getClrR() == null)
		{
			clrDetailsObj.setClrR(0);
		}
		if (clrDetailsObj.getClrL() == null)
		{
			clrDetailsObj.setClrL(0);
		}
		if (clrDetailsObj.getLowerLimit() == null)
		{
			clrDetailsObj.setLowerLimit(0);
		}
		if (clrDetailsObj.getUserId() == null)

		{
			LOG.info("Request does not contain proper inforamtion");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}

		else
		{
			final CLRDetails cLRDetailsObj = shoppingListDao.fetchMasterSLCLRDetails(clrDetailsObj);
			if (null != cLRDetailsObj)
			{
				response = XstreamParserHelper.produceXMlFromObject(cLRDetailsObj);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOMEDIA);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;

	}

	/**
	 * This is a service Method for fetching today shopping list products for
	 * the given input as XML. This method validates the input XML, if it is
	 * valid it will call the DAO method.
	 * 
	 * @param xml
	 *            containing input information need to be fetched the TSL
	 *            products .
	 * @return XML containing TSL products in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@Override
	public String getTSLProductslst(String xml) throws ScanSeeException
	{
		final String methodName = "getTSLProductslst of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ProductDetailsRequest productDetailsRequest = (ProductDetailsRequest) streamHelper.parseXmlToObject(xml);
		if (null == productDetailsRequest.getUserId())
		{
			LOG.info("Request does not contain UserId ");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			return response;
		}
		else
		{
			final ProductDetails productDetailslst = shoppingListDao.getTSLProcuctDetailslst(productDetailsRequest);
			if (null != productDetailslst)
			{
				response = XstreamParserHelper.produceXMlFromObject(productDetailslst);
			}
			else
			{
				LOG.info("No records found for userid {}", xml);
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOSHOPPINGLISTPRODUCTTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is a service Method for fetching master shopping list products for
	 * the given input as XML. This method validates the input XML, if it is
	 * valid it will call the DAO method.
	 * 
	 * @param userId
	 *            containing input information need to be fetched the MSL
	 *            products .
	 * @return XML containing master shopping list products in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("static-access")
	@Override
	public String getMSLCategoryProducts(Integer userId, Integer iLowLimit) throws ScanSeeException
	{
		final String methodName = "getMSLProductslst of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		Integer iNextPage = null;
		Integer iMaxCount = null;
		ShoppingListRetailerCategoryList shoppingListCategoryInfo = null;
		if (userId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		} else {
			if (iLowLimit == null) { iLowLimit = 0; }
			ArrayList<MainSLRetailerCategory> masterSLCategorylst = null;
			masterSLCategorylst = shoppingListDao.getMSLCategoryProcuctDetails(userId, iLowLimit, ApplicationConstants.MASTERSHOPLISTPAGINATIOM);
			if (null != masterSLCategorylst && !masterSLCategorylst.isEmpty())
			{
				iNextPage = masterSLCategorylst.get(0).getNextPage();
				iMaxCount = masterSLCategorylst.get(0).getMaxCount();
				final ShoppingListHelper shoppingListHelper = new ShoppingListHelper();
				shoppingListCategoryInfo = shoppingListHelper.getShoppingListCategoryList(masterSLCategorylst);
			}
			if (null != shoppingListCategoryInfo)
			{
				shoppingListCategoryInfo.setNextPage(iNextPage);
				shoppingListCategoryInfo.setMaxCount(iMaxCount);
				response = XstreamParserHelper.produceXMlFromObject(shoppingListCategoryInfo);
			} else {
				LOG.info("No records found for the userid {}", userId);
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOSHOPPINGLISTPRODUCTTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a service Method for fetching product summary for the given input
	 * as XML. This method validates the input XML, if it is valid it will call
	 * the DAO method.
	 * 
	 * @param xml
	 *            containing input information need to be fetched the product
	 *            summary
	 * @return XML containing product summary containing product summary in the
	 *         response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getProductSummary(String xml) throws ScanSeeException
	{
		final String methodName = "getProductSummary";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String localStoreResponse = null;
		final StringBuilder finalResponse = new StringBuilder("<ProductSummary>");
		String response = null;
		final long methodStartTime = new Date().getTime();

		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final ProductDetailsRequest productDetailsRequest = (ProductDetailsRequest) streamHelper.parseXmlToObject(xml);

		final Integer userId = productDetailsRequest.getUserId();
		Integer retailId = productDetailsRequest.getRetailID();
		final Integer productId = productDetailsRequest.getProductId();
		final String postalcode = productDetailsRequest.getPostalcode();
		int radius = 0;
		final Double latitude = productDetailsRequest.getLatitude();
		final Double longitude = productDetailsRequest.getLongitude();
		//for user tracking
		final Integer prodListID = productDetailsRequest.getProdListID();
		final Integer mainMenuID = productDetailsRequest.getMainMenuID();
		//For user tracking
		Integer saleListID = productDetailsRequest.getSaleListID();
		Integer scanID = productDetailsRequest.getScanID();
		Integer scanTypeID = productDetailsRequest.getScanTypeID();

		ArrayList<AppConfiguration> radiuslst = null;
		if (null == userId || null == productId)
		{
			LOG.info("Request does not contain UserId/ productId ");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			return response;
		}
		// Fetching user radius from App configuration table.
		radiuslst = shoppingListDao.getAppConfig(ApplicationConstants.CONFIGURATIONDEFAULTRADIUS);

		for (int i = 0; i < radiuslst.size(); i++)
		{
			if (radiuslst.get(i).getScreenName().equals(ApplicationConstants.DEFAULTRADIUSSCREEN))
			{
				if (radiuslst.get(i).getScreenContent() != null)
				{
					radius = Integer.parseInt(radiuslst.get(i).getScreenContent());

				}
			}

		}
		if (null == retailId)
		{
			retailId = 0;
		}
		// Get the configuration images and headers;
		final ArrayList<AppConfiguration> stockInfoList = shoppingListDao.getAppConfig(ApplicationConstants.CONFIGURATIONTYPESTOCK);
		final Map<String, String> map = new HashMap<String, String>();

		for (AppConfiguration i : stockInfoList)
		{
			map.put(i.getScreenName(), i.getScreenContent());
		}

		// Get the product information rating,Coupons etc.
		final long locaStoreStrt11 = new Date().getTime();
		final ProductDetail productDetail = baseDao.getProductDetail(userId, productId, retailId, saleListID, prodListID, mainMenuID, scanTypeID);

		if (null != productDetail)
		{
			final long locaStoreEnd11 = new Date().getTime();
			LOG.info("Total Time taken for fetching product details{}", (locaStoreEnd11 - locaStoreStrt11) / 1000);

			String productInfoRespone = XstreamParserHelper.produceXMlFromObject(productDetail);

			if (productInfoRespone != null)
			{

				finalResponse.append(productInfoRespone);
			}

			// First get the LocalStore information from database.
			final long locaStoreStrt1 = new Date().getTime();
			final FindNearByIntactResponse findNearByDetailsResultSet = findRetailersLocalDataBase(userId, latitude, productId, longitude, postalcode, radius, mainMenuID);
			final long locaStoreEnd1 = new Date().getTime();
			LOG.info("Total Time taken for the Local Store for local database{}", (locaStoreEnd1 - locaStoreStrt1) / 1000);

			/*
			 * if (findNearByDetailsResultSet.isFindNearBy()) { // if
			 * LocalStores are not available in Database make Call to //
			 * External API localStoreResponse = null; } else
			 */
			if (null != findNearByDetailsResultSet.getFindNearByDetails() && null != findNearByDetailsResultSet.getFindNearByMetaData())
			{
				// if LocalStores are available
				localStoreResponse = XstreamParserHelper.produceXMlFromObject(findNearByDetailsResultSet);
			}
			else
			{
				final long locaStoreStrt = new Date().getTime();
				localStoreResponse = findRetailersExternalAPI(userId, productId, latitude, longitude, radius, 0, postalcode);
				final long locaStoreEnd = new Date().getTime();
				LOG.info("Total Time taken for the Local Store {}", (locaStoreEnd - locaStoreStrt) / 1000);

			}

			if (localStoreResponse != null)
			{
				localStoreResponse = localStoreResponse.replaceAll(
						"<FindNearByDetailsResultSet>",
						"<FindNearByDetailsResultSet><stockImagePath>" + map.get(ApplicationConstants.NEARBYIMAGE) + "</stockImagePath><stockHeader>"
								+ map.get(ApplicationConstants.NEARBYHEADER) + "</stockHeader>" + "<stockImgPathDir>"
								+ map.get(ApplicationConstants.GETDIRIMAGE) + "</stockImgPathDir><stockHeaderDir>"
								+ map.get(ApplicationConstants.GETDIRHEADER) + "</stockHeaderDir>" + "<stockImgPathWebSite>"
								+ map.get(ApplicationConstants.BROWSEWEBSITEIMAGE) + "</stockImgPathWebSite><stockHeaderWebSite>"
								+ map.get(ApplicationConstants.BROWSEWEBSITEHEADER) + "</stockHeaderWebSite>");
				finalResponse.append(localStoreResponse);
			}
			LOG.info("Response returned by LocalStore API \n {}", localStoreResponse);

			// Request for Call to Online Stores External API.
			final OnlineStoresRequest onlineStoresRequest = new OnlineStoresRequest();
			onlineStoresRequest.setProductId(productId);
			onlineStoresRequest.setUserId(String.valueOf(userId));
			onlineStoresRequest.setPageStart(String.valueOf(0));
			final long locaStoreStrt = new Date().getTime();

			// Call External API for Online Stores.
			String onLineResponse = getOnlineStoreData(onlineStoresRequest);
			final long locaStoreEnd = new Date().getTime();
			LOG.debug("Total Time taken for the Online Store {}", (locaStoreEnd - locaStoreStrt) / 1000);
			LOG.debug("Response returned by Online API \n {}", onLineResponse);
			OnlineStoresMetaData onlineStoresMetaData = new OnlineStoresMetaData();
			OnlineStores onlineStoreInfoObj = null;

			if (null != onLineResponse)
			{
				onlineStoreInfoObj = (OnlineStores) streamHelper.parseXmlToObject(onLineResponse);

				onLineResponse = onLineResponse.replaceAll(
						"<OnlineStores>",
						"<OnlineStores><stockImagePath>" + map.get(ApplicationConstants.ONLINEIMAGE) + "</stockImagePath><stockHeader>"
								+ map.get(ApplicationConstants.ONLINEHEADER) + "</stockHeader>");
			}

			/**
			 * For commission junction data implementation
			 */
			ArrayList<RetailerDetail> onlineRetailerlst = null;
			ArrayList<Double> salePricelst = new ArrayList<Double>();
			String onlineRetailerRespone = null;
			onlineRetailerlst = shoppingListDao.fetchCommissionJunctionData(userId, productId, prodListID, mainMenuID);

			if (null != onlineRetailerlst && !onlineRetailerlst.isEmpty())
			{

				for (int i = 0; i < onlineRetailerlst.size(); i++)
				{
					if (onlineRetailerlst.get(i).getSalePrice() != null
							&& !(onlineRetailerlst.get(i).getSalePrice().equalsIgnoreCase(ApplicationConstants.NOTAPPLICABLE)))

					{
						String retSalePrice;
						retSalePrice = onlineRetailerlst.get(i).getSalePrice().substring(1, onlineRetailerlst.get(i).getSalePrice().length());
						salePricelst.add(Double.parseDouble(retSalePrice));
						
					}
				}
				Double onlineMinPrice = null;
				if (salePricelst != null && !salePricelst.isEmpty())
				{
					onlineMinPrice = Collections.min(salePricelst);
				}
				Double shopMinPrice = null;
				if (minimumPrice != null)
				{
					int dollerindex = minimumPrice.indexOf('$');
					minimumPrice = minimumPrice.substring(1, minimumPrice.length());
					shopMinPrice = Double.parseDouble(minimumPrice);
				}

				if (shopMinPrice != null && !shopMinPrice.equals(ApplicationConstants.NOTAPPLICABLE) && onlineMinPrice != null
						&& !shopMinPrice.equals(ApplicationConstants.NOTAPPLICABLE))
				{
					if (onlineMinPrice < shopMinPrice)
					{

						minimumPrice = String.valueOf(onlineMinPrice);
						minimumPrice = "$" + Utility.formatDecimalValue(minimumPrice);
					}
					else
					{
						minimumPrice = String.valueOf(shopMinPrice);
						minimumPrice = "$" + Utility.formatDecimalValue(minimumPrice);
					}

				}
				else
				{
					if (onlineMinPrice != null && !onlineMinPrice.equals(ApplicationConstants.NOTAPPLICABLE))
					{

						minimumPrice = String.valueOf(onlineMinPrice);
						minimumPrice = "$" + Utility.formatDecimalValue(minimumPrice);
					}
				}
				if (totalOnlineStores != null)
				{
					int total = Integer.parseInt(totalOnlineStores);
					totalOnlineStores = String.valueOf(total + onlineRetailerlst.size());
				}
				else
				{
					totalOnlineStores = String.valueOf(onlineRetailerlst.size());
				}
				onlineRetailerRespone = XstreamParserHelper.produceXMlFromObject(onlineRetailerlst);

			}

			if (onlineRetailerRespone != null)
			{
				onlineRetailerRespone = onlineRetailerRespone.replaceAll("<list>", "<CommissionJunctionData>");
				onlineRetailerRespone = onlineRetailerRespone.replaceAll("</list>", "</CommissionJunctionData>");
				if(minimumPrice == null)
				{
					minimumPrice=" ";
				}
				
				if (onLineResponse == null)
				{
					onlineRetailerRespone = onlineRetailerRespone.replace(
							"<CommissionJunctionData>",
							"<OnlineStores><onlineStoresMetaData><minPrice>" + minimumPrice + "</minPrice><totalStores>" + totalOnlineStores
									+ "</totalStores> <nextPage>0</nextPage>" + "</onlineStoresMetaData><stockImagePath>"
									+ map.get(ApplicationConstants.ONLINEIMAGE) + "</stockImagePath><stockHeader>"
									+ map.get(ApplicationConstants.ONLINEHEADER) + "</stockHeader><CommissionJunctionData>");
					onlineRetailerRespone = onlineRetailerRespone.replaceAll("</CommissionJunctionData>", "</CommissionJunctionData></OnlineStores>");
					onLineResponse = onlineRetailerRespone;
				}
				else
				{

					/*
					 * <minPrice>$4.05</minPrice> <totalStores>19</totalStores>
					 * <nextPage>1</nextPage> <pageStart>0</pageStart>
					 * <apiURL><!
					 * [CDATA[http://catalog.bizrate.com/services/catalog
					 * /v1/us/product
					 * ?apiKey=2530b6c85957403e091535980c9ddf0e&publisherId
					 * =58448
					 * &keyword=9781400000000&offersOnly=True&placementId=2
					 * &start
					 * =%d&results=26&sort=price_asc&format=xml]]></apiURL>
					 * <vendorName>Shopzilla</vendorName>
					 * <pageSize>26</pageSize>
					 */
					onlineStoresMetaData.setMinPrice(minimumPrice);
					onlineStoresMetaData.setTotalStores(totalOnlineStores);
					onlineStoresMetaData.setApiURL(onlineStoreInfoObj.getOnlineStoresMetaData().getApiURL());
					onlineStoresMetaData.setPageStart(onlineStoreInfoObj.getOnlineStoresMetaData().getPageStart());
					onlineStoresMetaData.setPageSize(onlineStoreInfoObj.getOnlineStoresMetaData().getPageSize());
					onlineStoresMetaData.setVendorName(onlineStoreInfoObj.getOnlineStoresMetaData().getVendorName());
					onlineStoresMetaData.setNextPage(onlineStoreInfoObj.getOnlineStoresMetaData().getNextPage());
					onlineStoreInfoObj.setOnlineStoresMetaData(onlineStoresMetaData);

					onLineResponse = XstreamParserHelper.produceXMlFromObject(onlineStoreInfoObj);

					if (null != onLineResponse)
					{
						onLineResponse = onLineResponse.replaceAll(
								"<OnlineStores>",
								"<OnlineStores><stockImagePath>" + map.get(ApplicationConstants.ONLINEIMAGE) + "</stockImagePath><stockHeader>"
										+ map.get(ApplicationConstants.ONLINEHEADER) + "</stockHeader>");
						// finalResponse.append(onLineResponse);
					}

					onlineRetailerRespone = onlineRetailerRespone.replaceAll("</CommissionJunctionData>", "</CommissionJunctionData></OnlineStores>");
					onLineResponse = onLineResponse.replace("</OnlineStores>", onlineRetailerRespone);

				}

			}

			if (null != onLineResponse)
			{
				finalResponse.append(onLineResponse);
			}
			/**
			 * Calling rate review dao method to get review and rating
			 * information. It includes reviews of the product and user ratings.
			 */
			String rateReviewresponse = null;
			ProductRatingReview productRatingReview = null;
			final ArrayList<ProductReview> productReviewslist = rateReviewDao.getProductReviews(userId, productId);
			UserRatingInfo userRatingInfo = rateReviewDao.fecthUserProductRating(userId, productId);

			productRatingReview = new ProductRatingReview();
			int reviewSize = 0;

			if (null == productReviewslist || productReviewslist.isEmpty())
			{
				reviewSize = 0;
				productRatingReview.setTotalReviews("0");
			}
			else
			{
				reviewSize = productReviewslist.size();
				productRatingReview.setProductReviews(productReviewslist);
				productRatingReview.setTotalReviews(String.valueOf(productReviewslist.size()));
			}

			if (null != userRatingInfo)
			{
				Integer currentRating = null;
				Integer averageRating = null;
				if (userRatingInfo.getCurrentRating() != null && !userRatingInfo.getCurrentRating().equals(""))
				{
					currentRating = Integer.valueOf(userRatingInfo.getCurrentRating());
				}
				if (userRatingInfo.getAvgRating() != null && !userRatingInfo.getCurrentRating().equals(""))
				{
					averageRating = Integer.valueOf(userRatingInfo.getAvgRating());
				}
				productRatingReview.setUserRatingInfo(userRatingInfo);

			}
			else
			{
				userRatingInfo = new UserRatingInfo();
				userRatingInfo.setCurrentRating("0");
				userRatingInfo.setAvgRating("0");
				productRatingReview.setUserRatingInfo(userRatingInfo);
			}

			if (productRatingReview != null)
			{
				// Check size of The Reviews and Check the Rat
				String avgRating = productRatingReview.getUserRatingInfo().getAvgRating();
				String currRating = productRatingReview.getUserRatingInfo().getCurrentRating();
				if (reviewSize != 0 || !avgRating.equals("0") || !currRating.equals("0"))
				{
					productRatingReview.setStockImagePath(map.get(ApplicationConstants.REVIEWIMAGE));
					productRatingReview.setStockHeader(map.get(ApplicationConstants.REVIEWHEADER));
					rateReviewresponse = XstreamParserHelper.produceXMlFromObject(productRatingReview);
					finalResponse.append(rateReviewresponse);
				}
			}

			// Append CLR information to final response
			final String couponStatus = productDetail.getCoupon_Status();
			final String loyaltyStatus = productDetail.getLoyalty_Status();
			final String rebateStatus = productDetail.getRebate_Status();
			if (!"Grey".equalsIgnoreCase(couponStatus) || !"Grey".equalsIgnoreCase(loyaltyStatus) || !"Grey".equalsIgnoreCase(rebateStatus))
			{
				final Map<String, String> clrMap = new HashMap<String, String>();
				clrMap.put("stockImagePath", map.get(ApplicationConstants.DISCOUNTIMAGE));
				clrMap.put("stockHeader", map.get(ApplicationConstants.DISCOUNTHEADER));
				clrMap.put("couponFlag", productDetail.getCoupon_Status());
				clrMap.put("loyaltyFlag", productDetail.getLoyalty_Status());
				clrMap.put("rebateFlag", productDetail.getRebate_Status());
				final String productClrIngfo = Utility.formResponseXml("CLRInfo", clrMap);
				finalResponse.append(productClrIngfo);
			}

			// Close Root tag.
			finalResponse.append("</ProductSummary>");
			totalOnlineStores = null;
			minimumPrice = null;
			LOG.info("Final Product Summary response \n {}", finalResponse);

			final long methodEndTime = new Date().getTime();
			LOG.info("Total Time taken by the Method \n {}", (methodEndTime - methodStartTime) / 1000);
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			return response;
		}
		return finalResponse.toString();
	}

	/**
	 * This is a service Method for finading online stores using External API.
	 * products for the given input as XML.
	 * 
	 * @param xml
	 *            containing the request for External API call .
	 * @return XML containing List of online stores.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String findOnlineStores(String xml) throws ScanSeeException
	{
		final String methodName = "findOnlineStores of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		final XstreamParserHelper streamHelper = new XstreamParserHelper();

		final OnlineStoresRequest onlineStoresRequest = (OnlineStoresRequest) streamHelper.parseXmlToObject(xml);

		response = getOnlineStoreData(onlineStoresRequest);
		if (null == response)
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOONLINESTORES);
		}
		return response;
	}

	/**
	 * This service method is used to get online store informations.
	 * 
	 * @param onlineStoresRequest
	 *            -Request object
	 * @return response as success or failure
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	private String getOnlineStoreData(OnlineStoresRequest onlineStoresRequest) throws ScanSeeException
	{

		ProductDetail productInfo = null;
		String moduleName = "FindOnlineStores";
		String response = null;

		@SuppressWarnings("unused")
		ExternalAPIErrorLog externalAPIErrorLog = null;
		ExternalAPIInformation externalAPIInformation = new ExternalAPIInformation();
		final OnlineStoresService onlineStoresService = new OnlineStoresServiceImpl();
		if (null == onlineStoresRequest.getApiURL())
		{
			productInfo = shoppingListDao.getProductInfoforExternalAPI(onlineStoresRequest.getProductId());

			if (null != productInfo)
			{
				if (productInfo.getBarCode() != ApplicationConstants.NOTAPPLICABLE)
				{
					final OnlineStoresRequest onlineStores = new OnlineStoresRequest();
					onlineStores.setPageStart(onlineStoresRequest.getPageStart());
					onlineStores.setUpcCode(productInfo.getBarCode());
					// onlineStores.setUpcCode("9781400000000");
					onlineStores.setUserId(onlineStoresRequest.getUserId());
					onlineStores.setValues();

					externalAPIInformation = getExternalAPIInformation(ApplicationConstants.FINDONLINESTOESMODULENAME);

					if (null != externalAPIInformation.getSerchParameters() && null != externalAPIInformation.getVendorList())
					{
						LOG.info("Invoking External API for Module::::::::" + moduleName + " With Parameters:Product UPC Code:"
								+ onlineStoresRequest.getUpcCode() + " Product Name:" + productInfo.getProductName());

						// onlineStoresService.onlineStoresInfoEmailTemplate(externalAPIInformation,
						// onlineStores);
						response = onlineStoresService.onlineStoresInfo(externalAPIInformation, onlineStores);

						if (ApplicationConstants.EXTERNALAPIFAILURE == onlineStoresService.getResponseCode())
						{
							externalAPIErrorLog = onlineStoresService.getExternalAPIErrors();
							// shoppingListDao.LogExternalApiErrorMessages(externalAPIErrorLog);
						}

						if (onlineStoresService.getResponseCode() != 11000)
						{
							response = null;
						}
						totalOnlineStores = onlineStoresService.getNumberOfStores();
						minimumPrice = onlineStoresService.getMinimumPrice();
						// LOG.info("Response XML from External API:::::" +
						// response);
					}

				}
				else
				{

					LOG.info("Product Barcode is not available to Invoke External External API");
				}
			}
		}
		else
		{
			LOG.info("Invoking ShopZilla For Pagination");

			response = onlineStoresService.onlineStoresInfo(null, onlineStoresRequest);
			if (onlineStoresService.getResponseCode() != 11000)
			{
				response = null;
			}
			LOG.info("Response XML from External API:::::" + response);
		}
		return response;
	}

	/**
	 * This service Method for adding today's SL product for the given product
	 * id and user id.
	 * 
	 * @param xml
	 *            -containing the request for External API call .
	 * @return XML containing List of SL products.
	 * @throws ScanSeeException
	 *             -If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String addTodaySLProductsBySearch(String xml) throws ScanSeeException
	{
		final String methodName = "addTodaySLProductsBySearch of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AddSLRequest addRemObj = (AddSLRequest) streamHelper.parseXmlToObject(xml);
		if (null == addRemObj.getUserId())
		{
			LOG.info("Validation failed User Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			final ProductDetail prodObj = shoppingListDao.addTodaySLProductsBySearch(addRemObj);
			if (null != prodObj)
			{
				if (prodObj.getProductIsThere() != null)
				{
					if (prodObj.getProductIsThere() == 1)
					{
						response = Utility.formResponseXml(ApplicationConstants.DUPLICATEPRODUCTCODE, ApplicationConstants.DUPLICATEPRODUCTTEXT,
								"UserProductIds", String.valueOf(prodObj.getUserProductId()));
					}
					else
					{
						response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.ADDTODAYSLPRODUCTS,
								"UserProductIds", String.valueOf(prodObj.getUserProductId()));
					}
				}
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is a service Method for fetching shopping list history products for
	 * the given userId. This method validates the input XML, if it is valid it
	 * will call the DAO method.
	 * 
	 * @param userId
	 *            containing input information need to be fetched the Shopping
	 *            list history products .
	 * @return XML containing shopping list history products in the response.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("static-access")
	@Override
	public String fetchSLHistoryItems(Integer userId, Integer lowerlimit) throws ScanSeeException
	{
		final String methodName = "fetchSLHistoryItems of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		ShoppingListRetailerCategoryList shoppingListCategoryInfo = null;
		if (userId == null)
		{
			LOG.info("Request does not contain");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			if (lowerlimit == null)
			{
				lowerlimit = 0;
			}
			ArrayList<MainSLRetailerCategory> masterSLCategorylst = null;
			masterSLCategorylst = shoppingListDao.fetchSLHistoryProducts(userId, lowerlimit);

			if (null != masterSLCategorylst && !masterSLCategorylst.isEmpty())
			{
				final ShoppingListHelper shoppingListHelper = new ShoppingListHelper();
				shoppingListCategoryInfo = shoppingListHelper.fetchSLHistoryCategoryDisplay(masterSLCategorylst);
				shoppingListCategoryInfo.setNextPage(masterSLCategorylst.get(0).getNextPage());
			}

			if (null != shoppingListCategoryInfo)
			{
				response = XstreamParserHelper.produceXMlFromObject(shoppingListCategoryInfo);
				response = response.replaceAll("<ShoppingListCategoryList>", "<ShoppingListHistory>");
				response = response.replaceAll("</ShoppingListCategoryList>", "</ShoppingListHistory>");
				response = StringEscapeUtils.unescapeXml(response);
			}
			else
			{
				LOG.info("No records found for the userid {}", userId);
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * This is a service Method for adding shopping list history product to List
	 * ,favorites and both for the given userId. This method validates the input
	 * XML, if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            containing product information needed for adding to
	 *            list,favorites and both.
	 * @return XML containing message saying it added to list or favorites or
	 *         both.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String addSLHistoryProdut(String xml) throws ScanSeeException
	{
		final String methodName = "addSLHistoryProdut of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		ProductDetails productDetailsObj = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AddSLRequest addSLRequest = (AddSLRequest) streamHelper.parseXmlToObject(xml);

		if (null == addSLRequest.getUserId())
		{
			LOG.info("Validation failed User Id is not available");
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			productDetailsObj = shoppingListDao.addSLHistoryProductInfo(addSLRequest);
			if (null != productDetailsObj)
			{

				// final Map<String, String> addSLHistoryMap = new
				// HashMap<String, String>();
				/*
				 * if (productDetailsObj.getResponseFlag() != null) {
				 * addSLHistoryMap.put("ResponseFlag",
				 * productDetailsObj.getResponseFlag()); }
				 */
				// response =
				// Utility.formResponseXml(ApplicationConstants.SUCCESSCODE,
				// ApplicationConstants.SUCCESSRESPONSETEXT, addSLHistoryMap);
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, productDetailsObj.getResponseFlag());

			}

			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	public CellFireAPIKey getExternalApiInformation() throws ScanSeeException
	{

		final String methodName = "getExternalApiInformation";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ExternalAPIVendor> externalAPIVendorList = manageLoyaltyCardDAO.getExternalAPIList(ApplicationConstants.LOYALTY);
		CellFireAPIKey cellFireAPIKey = new CellFireAPIKey();
		for (Iterator i = externalAPIVendorList.iterator(); i.hasNext();)
		{
			ExternalAPIVendor externalAPIVendor = (ExternalAPIVendor) i.next();
			ArrayList<ExternalAPISearchParameters> externalAPISearchParametersList = manageLoyaltyCardDAO.getExternalAPIInputParameters(
					Integer.valueOf(externalAPIVendor.getApiUsageID()), ApplicationConstants.LOYALTY);

			for (Iterator j = externalAPISearchParametersList.iterator(); j.hasNext();)
			{
				ExternalAPISearchParameters apiSearchParameters = (ExternalAPISearchParameters) j.next();

				if (apiSearchParameters.getApiParameter().equals(ApplicationConstants.APIKEY))
				{

					cellFireAPIKey.setApiKeyParameterName(apiSearchParameters.getApiParameter());
					cellFireAPIKey.setApiKeyParameterValue(apiSearchParameters.getApiParamtervalue());

				}
				else if (apiSearchParameters.getApiParameter().equals(ApplicationConstants.TLC))
				{

					cellFireAPIKey.setTrackingCodeParameterName(apiSearchParameters.getApiParameter());
					cellFireAPIKey.setTrackingCodeParameterValue(apiSearchParameters.getApiParamtervalue());
				}
				else if (apiSearchParameters.getApiParameter().equals(ApplicationConstants.CELLFIREPARTNERID))
				{

					cellFireAPIKey.setParnerId(apiSearchParameters.getApiParamtervalue());

				}
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return cellFireAPIKey;

	}

	public static QClipResponse[] clipCfCoupons(CellFireAPIKey cellFireAPIKey, CellFireRequest cfr) throws NumberFormatException, ScanSeeException
	{
		CellFireWebServiceClient cellFireWebServiceClient = new CellFireWebServiceClient(cellFireAPIKey.getApiKeyParameterName(),
				cellFireAPIKey.getApiKeyParameterValue(), cellFireAPIKey.getTrackingCodeParameterName(),
				cellFireAPIKey.getTrackingCodeParameterValue());
		int[] couponIds = { Integer.valueOf(cfr.getCfLoyaltyId()) };
		QClipRequests clipRequests = new QClipRequests();
		clipRequests.setLoyaltyCardNumber(cfr.getCardNumber());
		ArrayOfInt cfrIds = new ArrayOfInt();
		cfrIds.set_int(couponIds);
		clipRequests.setCouponIds(cfrIds);
		QClipResponse[] clipResponsesArray = cellFireWebServiceClient.clipCouponWithLoyaltyCard(clipRequests, Long.valueOf(cfr.getCfContentVersion()));
		return clipResponsesArray;
	}

	/**
	 * This is a service Method for capturing product media click
	 * 
	 * @param pmListID
	 *            
	 * @return String with Success or failure information.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String userTrackingProdMediaClick(Integer pmListID) throws ScanSeeException {
		final String methodName = "userTrackingProdMediaClick of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		if (null == pmListID)	{
			LOG.info("Validation failed product media list ID is not available");
			response = ApplicationConstants.FAILURE;
		}
		else	{
			response = shoppingListDao.userTrackingProdMediaClick(pmListID);
		}
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for capturing Online Product Click
	 * Method Type: GET.
	 * 
	 * @param onProdDetID
	 *            
	 * @return {@link Null} no return parameter
	 */
	@Override
	public String userTrackingOnlineStoreClick(String xml) throws ScanSeeException {
		final String methodName = "userTrackingOnlineStoreClick of service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final AddSLRequest addSLRequest = (AddSLRequest) streamHelper.parseXmlToObject(xml);
		if (null == addSLRequest.getProductID() || null == addSLRequest.getRetailerName() || null == addSLRequest.getMainMenuID())	{
			LOG.info("Validation failed product media list ID is not available");
			response = ApplicationConstants.FAILURE;
		} else {
			response = shoppingListDao.userTrackingOnlineStoreClick(addSLRequest);
			if (null == response) {
			}
		}
		return response;
	}
}
