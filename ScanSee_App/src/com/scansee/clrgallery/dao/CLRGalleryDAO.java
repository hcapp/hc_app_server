package com.scansee.clrgallery.dao;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.CouponsDetails;
import com.scansee.common.pojos.LoyaltyDetails;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.RebateDetails;

/**
 * The Interface for CLRGalleryDAO. ImplementedBy {@link CLRGalleryDAOImpl}
 * 
 * @author shyamsundara_hm
 */
public interface CLRGalleryDAO
{
	/**
	 * This method get the Master Shopping list product CLR from the database.
	 * 
	 * @param userId
	 *            as request
	 * @param retailerId
	 *            as request
	 * @param productId
	 *            as request.
	 * @return xml String XMl with list of shopping List items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	CLRDetails fetchCLRDetails(Integer userId, Integer retailerId, Integer productId) throws ScanSeeException;

	/**
	 * This method get the Master Shopping list product CLR from the database.
	 * 
	 * @param userId
	 *            as request
	 * @param retailId
	 *            as request
	 * @param productId
	 *            as request.
	 * @return xml String XMl with list of shopping List items for the user.
	 * @throws ScanSeeException
	 *             throws if any exception.
	 */
	CouponsDetails getCouponInfo(Integer userId, Integer productId, Integer retailId, Integer mainMenuID) throws ScanSeeException;

	/**
	 * This method fetches the loyalty information from the database for the
	 * given loyaltyId.
	 * 
	 * @param clrDetailsObj
	 *            , contains userId,loyaltyId which are needed for fetching
	 *            loyalty details.
	 * @return LoyaltyDetails with loyalty info.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	LoyaltyDetails getLoyaltyInfo(CLRDetails clrDetailsObj) throws ScanSeeException;

	/**
	 * This method fetches the rebate information from the database for the
	 * given rebateId.
	 * 
	 * @param clrDetailsObj
	 *            , contains userId,rebateId which are needed for fetching
	 *            rebate details.
	 * @return RebateDetails with rebate info.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	RebateDetails getRebateInfo(CLRDetails clrDetailsObj) throws ScanSeeException;

	/**
	 * This Method fetches Coupon details. Trebate information from the database
	 * for the given couponId.
	 * 
	 * @param clrDetailsObj
	 *            , contains userId,loyaltyId which are needed for fetching
	 *            coupon details.
	 * @return CouponDetails that contains Coupon details
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	CouponsDetails fetchCouponDetails(CLRDetails clrDetailsObj) throws ScanSeeException;
	
	/**
	 * This Method fetches Coupon details. Trebate information from the database
	 * for the given couponId.
	 * 
	 * @param clrDetailsObj
	 *            , contains userId,loyaltyId which are needed for fetching
	 *            coupon details.
	 * @return CouponDetails that contains Coupon details
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	ProductDetails getCLRProdInfo(CLRDetails clrDetailsObj) throws ScanSeeException;
	
	/**
	 * This Method fetches Coupon details. Trebate information from the database
	 * for the given couponId.
	 * 
	 * @param clrDetailsObj
	 *            , contains userId, CouponId which are needed for fetching
	 *            coupon details.
	 * @return CouponDetails that contains Coupon details
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	CouponsDetails fetchPromotionCouponDetails(CLRDetails clrDetailsObj) throws ScanSeeException;
	
	/**
	 * This Method fetches Coupon details. Trebate information from the database
	 * for the given couponId.
	 * 
	 * @param clrDetailsObj
	 *            , contains userId, CouponId which are needed for fetching
	 *            coupon all location details.
	 * @return CouponDetails that contains Coupon details
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	CouponsDetails fetchCouponLocationProduct(CLRDetails clrDetailsObj) throws ScanSeeException;

}
