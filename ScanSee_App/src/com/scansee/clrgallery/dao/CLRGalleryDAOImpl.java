package com.scansee.clrgallery.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.CouponDetails;
import com.scansee.common.pojos.CouponsDetails;
import com.scansee.common.pojos.LoyaltyDetail;
import com.scansee.common.pojos.LoyaltyDetails;
import com.scansee.common.pojos.ProductDetail;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.RebateDetail;
import com.scansee.common.pojos.RebateDetails;
import com.scansee.common.pojos.RetailerDetail;

/**
 * This class for fetching CLRGallery details.
 * 
 * @author shyamsundara_hm
 */
public class CLRGalleryDAOImpl implements CLRGalleryDAO
{
	/**
	 * Logger instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(CLRGalleryDAOImpl.class.getName());

	/**
	 * for jdbcTemplate connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * This method for to get data source Spring DI.
	 * 
	 * @param dataSource
	 *            for DB operations.
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * Method for fetching Coupon ,Rebates and Loyalty Info. The method is
	 * called from the service layer.
	 * 
	 * @param userId
	 *            The user id in the request.
	 * @param retailerId
	 *            The retail ID in the request.
	 * @param productId
	 *            The product id in the request.
	 * @return couponsDetails The xml with Coupon Details.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public CLRDetails fetchCLRDetails(Integer userId, Integer retailerId, Integer productId) throws ScanSeeException
	{

		final String methodName = "fetchCouponInfo in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		CLRDetails clrDetatilsObj = null;

		List<RebateDetail> rebateDetailList = null;
		List<LoyaltyDetail> loyaltyDetailList = null;
		List<CouponDetails> couponDetailList = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryCoupons");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final SqlParameterSource fetchCouponParameters = new MapSqlParameterSource().addValue(ApplicationConstants.USERID, userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Error Occured in usp_MasterShoppingListCoupon method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryLoyalty");
			simpleJdbcCall.returningResultSet("LoyaltyDetails", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final SqlParameterSource fetchLoyaltyParameters = new MapSqlParameterSource().addValue("ProductID", productId)
					.addValue("RetailID", retailerId).addValue(ApplicationConstants.USERID, userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.USERID))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Error Occured in usp_GalleryLoyalty method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else
			{
				loyaltyDetailList = (List<LoyaltyDetail>) resultFromProcedure.get("LoyaltyDetails");
			}

		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryRebate");
			simpleJdbcCall.returningResultSet("RebateDetails", new BeanPropertyRowMapper<RebateDetail>(RebateDetail.class));

			final SqlParameterSource fetchRebateParameters = new MapSqlParameterSource().addValue(ApplicationConstants.USERID, userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRebateParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Error Occured in usp_GalleryRebate  ..errorNum.{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);

			}
			else
			{
				rebateDetailList = (List<RebateDetail>) resultFromProcedure.get("RebateDetails");

			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		if (couponDetailList != null || !couponDetailList.isEmpty() || rebateDetailList != null || !rebateDetailList.isEmpty()
				|| loyaltyDetailList != null || !loyaltyDetailList.isEmpty())
		{
			clrDetatilsObj = new CLRDetails();
			if (couponDetailList != null && !couponDetailList.isEmpty())
			{
				clrDetatilsObj.setIsCouponthere(true);
				clrDetatilsObj.setCouponDetails(couponDetailList);
			}
			if (rebateDetailList != null && !rebateDetailList.isEmpty())
			{
				clrDetatilsObj.setIsRebatethere(true);
				clrDetatilsObj.setRebateDetails(rebateDetailList);
			}
			if (loyaltyDetailList != null && !loyaltyDetailList.isEmpty())
			{
				clrDetatilsObj.setIsLoyaltythere(true);
				clrDetatilsObj.setLoyaltyDetails(loyaltyDetailList);
			}

		}
		return clrDetatilsObj;
	}

	/**
	 * Method for fetching Coupon Info. The method is called from the service
	 * layer.
	 * 
	 * @param userId
	 *            The user id in the request.
	 * @param retailId
	 *            The retail ID in the request.
	 * @param productId
	 *            The product id in the request.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 * @return couponsDetails The xml with Coupon Details.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public CouponsDetails getCouponInfo(Integer userId, Integer productId, Integer retailId, Integer mainMenuID) throws ScanSeeException
	{

		final String methodName = "fetchCouponInfo in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		CouponsDetails couponsDetails = null;
		List<CouponDetails> couponDetailList = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MasterShoppingListCoupon");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			Integer retailerId = retailId;
			if (0 == retailId)
			{
				retailerId = null;
			}
			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue("ProductID", productId);
			fetchCouponParameters.addValue("RetailID", retailerId);
			fetchCouponParameters.addValue(ApplicationConstants.USERID, userId);
			//For user tracking
			fetchCouponParameters.addValue(ApplicationConstants.MAINMENUID, mainMenuID);
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);
			couponDetailList = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");

			if (null != resultFromProcedure)
			{

				if (null != resultFromProcedure.get("ErrorNumber"))
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.info("Error Occured in usp_MasterShoppingListCoupon method ..errorNum.{} errorMsg {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);

				}
				else
				{
					if (null == couponDetailList || couponDetailList.isEmpty())
					{
						return couponsDetails;
					}
					else
					{
						couponsDetails = new CouponsDetails();

						couponsDetails.setCouponDetail(couponDetailList);
					}
				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return couponsDetails;

	}

	/**
	 * Method for fetching Loyalty Info. The method is called from the service
	 * layer.
	 * 
	 * @param clrDetailsObj
	 *            contains userId,loyaltyId needed to fetch loyalty details.
	 * @return loyaltyDetails The xml with loyaltyDetails.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public LoyaltyDetails getLoyaltyInfo(CLRDetails clrDetailsObj) throws ScanSeeException
	{
		final String methodName = "fetchLoyaltyInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<LoyaltyDetail> loyaltyDetailList;
		LoyaltyDetails loyaltyInfo = null;
		ArrayList<ProductDetail> productlst = null;
		// final ArrayList<String> productNamesList = new ArrayList<String>();
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryLoyaltyDetails");
			simpleJdbcCall.returningResultSet("LoyaltyDetails", new BeanPropertyRowMapper<LoyaltyDetail>(LoyaltyDetail.class));

			final MapSqlParameterSource fetchLoyaltyParameters = new MapSqlParameterSource();
			fetchLoyaltyParameters.addValue(ApplicationConstants.USERID, clrDetailsObj.getUserId());
			fetchLoyaltyParameters.addValue("LoyaltyDealID", clrDetailsObj.getLoyaltyDealID());
			//for user tracking
			fetchLoyaltyParameters.addValue("LoyaltyListID", clrDetailsObj.getLoyaltyListID());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchLoyaltyParameters);

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Error Occured in usp_GalleryLoyaltyDetails method ..errorNum..{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);
			}
			else
			{
				loyaltyDetailList = (List<LoyaltyDetail>) resultFromProcedure.get("LoyaltyDetails");
				if (!loyaltyDetailList.isEmpty() && loyaltyDetailList != null)
				{
					productlst = new ArrayList<ProductDetail>();
					ProductDetail productDetailObj  =null;
					for (int i = 0; i < loyaltyDetailList.size(); i++)
					{
						// productNamesList.add(loyaltyDetailList.get(i).getProductName());
						productDetailObj= new ProductDetail();
						
						productDetailObj.setProductId(loyaltyDetailList.get(i).getProductId());
						productDetailObj.setProductName(loyaltyDetailList.get(i).getProductName());
						productDetailObj.setImagePath(loyaltyDetailList.get(i).getImagePath());
						
					}
					productlst.add(productDetailObj);
					loyaltyInfo = new LoyaltyDetails();
					loyaltyInfo.setProductLst(productlst);
					loyaltyDetailList.get(0).setProductName(null);
					loyaltyDetailList.get(0).setProductId(null);
					loyaltyDetailList.get(0).setImagePath(null);
					loyaltyInfo.setLoyaltyInfo(loyaltyDetailList.get(0));
				}
			}

		}
		catch (DataAccessException e)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return loyaltyInfo;

	}

	/**
	 * Method for fetching Rebate Info. The method is called from the service
	 * layer.
	 * 
	 * @param clrDetailsObj
	 *            contains userId,rebateId in the request.
	 * @return rebateDetails The xml with rebateDetails.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public RebateDetails getRebateInfo(CLRDetails clrDetailsObj) throws ScanSeeException
	{
		final String methodName = "fetchRebateInfo in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<RebateDetail> rebateDetailList;

		RebateDetails rebateInfo = null;
		// final ArrayList<String> productNamesList = new ArrayList<String>();
		ArrayList<ProductDetail> productlst = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryRebateDetails");
			simpleJdbcCall.returningResultSet("RebateDetails", new BeanPropertyRowMapper<RebateDetail>(RebateDetail.class));

			final SqlParameterSource fetchRebateParameters = new MapSqlParameterSource().addValue("UserID", clrDetailsObj.getUserId()).addValue(
					"RebateID", clrDetailsObj.getRebateId()).addValue(ApplicationConstants.MAINMENUID, clrDetailsObj.getMainMenuID());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRebateParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Error Occured in usp_GalleryRebateDetails  ..errorNum.{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);

			}
			else
			{
				rebateDetailList = (List<RebateDetail>) resultFromProcedure.get("RebateDetails");
				if (!rebateDetailList.isEmpty() && rebateDetailList != null)
				{
					productlst = new ArrayList<ProductDetail>();
					for (int i = 0; i < rebateDetailList.size(); i++)
					{
						// productNamesList.add(rebateDetailList.get(i).getProductName());

						ProductDetail productDetailObj = new ProductDetail();
						productDetailObj.setProductName(rebateDetailList.get(i).getProductName());
						productDetailObj.setProductId(rebateDetailList.get(i).getProductId());
						productlst.add(productDetailObj);

					}
					rebateInfo = new RebateDetails();
					rebateInfo.setProductLst(productlst);
					rebateDetailList.get(0).setProductName(null);
					rebateDetailList.get(0).setProductId(null);
					rebateInfo.setRebateInfo(rebateDetailList.get(0));

				}
			}

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return rebateInfo;
	}

	/**
	 * Method for fetching coupon details for particular coupon id. The method
	 * is called from the service layer.
	 * 
	 * @param clrDetailsObj
	 *            contains userId,couponId in the request.
	 * @return rebateDetails The xml with rebateDetails.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public CouponsDetails fetchCouponDetails(CLRDetails clrDetailsObj) throws ScanSeeException
	{
		final String methodName = "fetchCouponDetails in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<CouponDetails> couponDetailslst = null;
		CouponsDetails couponInfo = null;

		// final ArrayList<String> productNamesList = new ArrayList<String>();
		ArrayList<ProductDetail> productlst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryCouponsDetails");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue("ProductID", clrDetailsObj.getProductId());
			fetchCouponParameters.addValue("RetailID", clrDetailsObj.getRetailerId());
			fetchCouponParameters.addValue("UserID", clrDetailsObj.getUserId());
			fetchCouponParameters.addValue("CouponID", clrDetailsObj.getCouponId());
			//for user tracking
			fetchCouponParameters.addValue("CouponListID", clrDetailsObj.getCouponListID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);
			couponDetailslst = (List<CouponDetails>) resultFromProcedure.get("CouponDetails");

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				LOG.info("Error Occured in usp_GalleryRebateDetails  ..errorNum.{} errorMsg {}", errorNum, errorMsg);
				throw new ScanSeeException(errorMsg);

			}
			else
			{
				if (!couponDetailslst.isEmpty() && couponDetailslst != null)
				{

					productlst = new ArrayList<ProductDetail>();
					for (int i = 0; i < couponDetailslst.size(); i++)
					{
						ProductDetail productDetailObj = new ProductDetail();
						productDetailObj.setProductName(couponDetailslst.get(i).getProductName());
						productDetailObj.setProductId(couponDetailslst.get(i).getProductId());
						productlst.add(productDetailObj);

					}

					couponInfo = new CouponsDetails();
					// couponInfo.setProductNamesLst(productNamesList);
					couponInfo.setProductLst((ArrayList<ProductDetail>) productlst);
					couponDetailslst.get(0).setProductName(null);
					couponDetailslst.get(0).setProductId(null);
					couponInfo.setCouponInfo(couponDetailslst.get(0));

				}
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error("Exception occurred in fetchCouponDetails", exception);
			throw new ScanSeeException(exception);
		}
		return couponInfo;
	}

	@Override
	public ProductDetails getCLRProdInfo(CLRDetails clrDetailsObj) throws ScanSeeException
	{

		final String methodName = "fetchProductDetails in DAO layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		ProductDetails productDetails = null;
		List<ProductDetail> productDetailLst = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetCLRProductDetails");
			simpleJdbcCall.returningResultSet("productDetails", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();

			fetchProductDetailsParameters.addValue("UserID", clrDetailsObj.getUserId());
			fetchProductDetailsParameters.addValue("CouponID", clrDetailsObj.getCouponId());
			fetchProductDetailsParameters.addValue("LoyaltyID", clrDetailsObj.getLoyaltyDealID());
			fetchProductDetailsParameters.addValue("RebateID", clrDetailsObj.getRebateId());
			fetchProductDetailsParameters.addValue("ErrorNumber", null);
			fetchProductDetailsParameters.addValue("ErrorMessage", null);
			//for user tracking
			fetchProductDetailsParameters.addValue(ApplicationConstants.MAINMENUID, clrDetailsObj.getMainMenuID());
			fetchProductDetailsParameters.addValue("CouponListID", clrDetailsObj.getCouponListID());
			fetchProductDetailsParameters.addValue("LoyaltyListID", clrDetailsObj.getLoyaltyListID());
			fetchProductDetailsParameters.addValue("RebateListID", clrDetailsObj.getRebateListID());
			fetchProductDetailsParameters.addValue(ApplicationConstants.MAINMENUID, clrDetailsObj.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					productDetailLst = (List<ProductDetail>) resultFromProcedure.get("productDetails");
					if (null != productDetailLst && !productDetailLst.isEmpty())
					{
						productDetails = new ProductDetails();
						productDetails.setProductDetail(productDetailLst);
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						if (nextpage != null)
						{
							if (nextpage)
							{
								productDetails.setNextPage(1);

							}
							else
							{
								productDetails.setNextPage(0);

							}
						}
					}
					else
					{
						LOG.info("No Products found for the search");
						return productDetails;
					}
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					LOG.error("Error occurred in usp_FetchProductList Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}

		}
		catch (DataAccessException exception)
		{

			LOG.error("Exception occurred in scanBarCodeProduct", exception);
			throw new ScanSeeException(exception);

		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return productDetails;
	}

	/**
	 * Method for fetching coupon details for particular coupon id. The method
	 * is called from the service layer.
	 * 
	 * @param clrDetailsObj contains userId,couponId in the request.
	 * @return coupon detail The xml with coupon detail.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CouponsDetails fetchPromotionCouponDetails(CLRDetails clrDetailsObj) throws ScanSeeException
	{
		LOG.info("Inside CLRGalleryServiceImpl : fetchPromotionCouponDetails");
		List<CouponDetails> couponDetailslist = new ArrayList <CouponDetails>();
		CouponDetails couponInfo = null;
		CouponsDetails objCouponDetail = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryCouponsDetails");
			simpleJdbcCall.returningResultSet("CouponDetails", new BeanPropertyRowMapper<CouponDetails>(CouponDetails.class));

			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue("UserID", clrDetailsObj.getUserId());
			fetchCouponParameters.addValue("CouponID", clrDetailsObj.getCouponId());
			//for user tracking
			fetchCouponParameters.addValue("CouponListID", clrDetailsObj.getCouponListID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);
			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					couponDetailslist = (ArrayList<CouponDetails>) resultFromProcedure.get("CouponDetails");
					if (null!= couponDetailslist && !couponDetailslist.isEmpty()) {
						couponInfo = new CouponDetails();
						objCouponDetail = new CouponsDetails();
						
						if (couponDetailslist.get(0).getProductFlag()) {
							couponDetailslist.get(0).setCoupProductFlag(1);
						} else {
							couponDetailslist.get(0).setCoupProductFlag(0);
						}
						
						if (couponDetailslist.get(0).getLocationFlag()) {
							couponDetailslist.get(0).setCoupLocationFlag(1);
						} else {
							couponDetailslist.get(0).setCoupLocationFlag(0);
						}
						couponInfo =  couponDetailslist.get(0);
						objCouponDetail.setCouponInfo(couponInfo);
					}
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_GalleryCouponsDetails  Store Procedure error number: {} " + errorNum + " and error  message: {}"
							+ errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside CLRGalleryServiceImpl : fetchPromotionCouponDetails : ", exception);
			throw new ScanSeeException(exception);
		}
		return objCouponDetail;
	}
	
	/**
	 * Method for fetching coupon all product/location details for particular coupon id, UserID,CouponListID. The method
	 * is called from the DAOImpl layer.
	 * 
	 * @param clrDetailsObj contains userId,couponId in the request.
	 * @return coupon detail The xml with coupon detail.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CouponsDetails fetchCouponLocationProduct(CLRDetails clrDetailsObj) throws ScanSeeException
	{
		LOG.info("Inside CLRGalleryServiceImpl : fetchCouponLocationProduct");
		List<RetailerDetail> arRetLocationList = new ArrayList <RetailerDetail>();
		ArrayList<ProductDetail> arCoupProductList = new ArrayList <ProductDetail>();
		CouponsDetails objCouponsDetails = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			if ("location".equalsIgnoreCase(clrDetailsObj.getType())) {
				simpleJdbcCall.withProcedureName("usp_GalleryCouponLocationDisplay");
				simpleJdbcCall.returningResultSet("LocationDetails", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));
			} else if ("product".equalsIgnoreCase(clrDetailsObj.getType())) {
				simpleJdbcCall.withProcedureName("usp_GalleryCouponProductDisplay");
				simpleJdbcCall.returningResultSet("ProductDetail", new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class));
			}
			
			final MapSqlParameterSource fetchCouponParameters = new MapSqlParameterSource();
			fetchCouponParameters.addValue("UserID", clrDetailsObj.getUserId());
			fetchCouponParameters.addValue("CouponID", clrDetailsObj.getCouponId());
			//for user tracking
			fetchCouponParameters.addValue("CouponListID", clrDetailsObj.getCouponListID());
			fetchCouponParameters.addValue("MainMenuID", clrDetailsObj.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchCouponParameters);
			
			if (null != resultFromProcedure)
			{	
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					if ("location".equalsIgnoreCase(clrDetailsObj.getType())) {
						arRetLocationList = (ArrayList<RetailerDetail>) resultFromProcedure.get("LocationDetails");
						if (null!= arRetLocationList && !arRetLocationList.isEmpty()) {
							objCouponsDetails = new CouponsDetails();
							objCouponsDetails.setRetailDetailsList(arRetLocationList);
						}
					} else if ("product".equalsIgnoreCase(clrDetailsObj.getType())) {
						arCoupProductList = (ArrayList<ProductDetail>) resultFromProcedure.get("ProductDetail");
						if (null!= arCoupProductList && !arCoupProductList.isEmpty()) {
							objCouponsDetails = new CouponsDetails();
							objCouponsDetails.setProductLst(arCoupProductList);
						}
					}
				} else {
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					LOG.error("Error occurred in usp_GalleryCouponLocationDisplay/usp_GalleryCouponProductDisplay  Store Procedure error number: {} " + errorNum + " and error  message: {}"
							+ errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error("Inside CLRGalleryServiceImpl : fetchCouponLocationProduct : ", exception);
			throw new ScanSeeException(exception);
		}
		return objCouponsDetails;
	}
	
}
