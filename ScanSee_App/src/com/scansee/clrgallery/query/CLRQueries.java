package com.scansee.clrgallery.query;
/**
 * The CLRQueries for Queries.
 * 
 * @author shyamsundara_hm.
 */
public class CLRQueries
{
	/**
	 * For displaying Hotdeals Category.
	 */
	public static final String FETCHCOUPONDETAILSQUERY ="Select CouponID couponId,CouponName," +
			"CouponDiscountType,CouponDiscountAmount,CouponDiscountPct,CouponDateAdded,CouponStartDate,CouponExpireDate,CouponShortDescription," +
			"CouponLongDescription  from Coupon where CouponID =?";
	/**
	 * Constructor CLRQueries.
	 */
	
	private CLRQueries(){
		
	}
}
