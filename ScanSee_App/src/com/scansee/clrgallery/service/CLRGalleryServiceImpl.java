package com.scansee.clrgallery.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.clrgallery.dao.CLRGalleryDAO;
import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.helper.BaseDAO;
import com.scansee.common.helper.XstreamParserHelper;
import com.scansee.common.pojos.CLRDetails;
import com.scansee.common.pojos.CouponsDetails;
import com.scansee.common.pojos.LoyaltyDetails;
import com.scansee.common.pojos.ProductDetails;
import com.scansee.common.pojos.RebateDetails;
import com.scansee.common.util.Utility;

/**
 * This class is used to display userCLRGallery items depending on retailers.
 * 
 * @author sowjanya_d
 */

public class CLRGalleryServiceImpl implements CLRGalleryService
{
	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(CLRGalleryServiceImpl.class);

	/**
	 * Instance variable for Shopping list DAO instance.
	 */

	private CLRGalleryDAO clrGalleryDao;

	/**
	 * Instance variable for baseDao.
	 */

	private BaseDAO baseDao;

	/**
	 * sets the CLRGalleryDAO DAO.
	 * 
	 * @param clrGalleryDao
	 *            The instance for CLRGalleryDAO
	 */

	public void setClrGalleryDao(CLRGalleryDAO clrGalleryDao)
	{
		this.clrGalleryDao = clrGalleryDao;
	}

	/**
	 * sets the BaseDAO DAO.
	 * 
	 * @param baseDao
	 *            The instance for BaseDAO
	 */
	public void setBaseDao(BaseDAO baseDao)
	{
		this.baseDao = baseDao;
	}

	/**
	 * The method calls JAXB Helper class method and parses the XML. The result
	 * is passed to ShoppingListDAO method.
	 * 
	 * @param userId
	 *            as the parameter.
	 * @param retailId
	 *            as the parameter.
	 * @param productId
	 *            as the parameter.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String fetchCLRDetails(Integer userId, Integer retailId, Integer productId) throws ScanSeeException
	{
		final String methodName = " fetchMSLCLRDetails of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (userId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);

		}
		else
		{
			final CLRDetails clrDetailsObj = clrGalleryDao.fetchCLRDetails(userId, retailId, productId);
			if (null != clrDetailsObj)
			{
				response = XstreamParserHelper.produceXMlFromObject(clrDetailsObj);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOMEDIA);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * Method for fetching Coupon Info. The method is called from the service
	 * layer.
	 * 
	 * @param userId
	 *            The user id in the request.
	 * @param retailId
	 *            The retail ID in the request.
	 * @param productId
	 *            The product id in the request.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 * @return couponsDetails The xml with Coupon Details.
	 */

	@Override
	public String getWishListCouponInfo(Integer userId, Integer productId, Integer retailId, Integer mainMenuID) throws ScanSeeException
	{

		final String methodName = "  getWishListCouponInfo of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		if (userId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);

		}
		else
		{
			final CouponsDetails couponDetails = clrGalleryDao.getCouponInfo(userId, productId, retailId, mainMenuID);
			if (null != couponDetails)
			{
				response = XstreamParserHelper.produceXMlFromObject(couponDetails);

				final String updateResposne = baseDao.upDatePushNotFlag(productId, userId);
				if (null != updateResposne && ApplicationConstants.FAILURE.equals(updateResposne))
				{
					// in case update failure. sending failure message
					response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
				}
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * Method for fetching Coupon/Rebate/Loyalty details. The method is called
	 * from the service layer.
	 * 
	 * @param xml
	 *            , contains couponId,rebateId,loyaltyId, userId which are
	 *            needed for fetching coupon/rebate/loyalty details.
	 * @return coupon/rebate/loyalty details.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public String fetchCLRDetails(String xml) throws ScanSeeException
	{
		final String methodName = "fetchCouponRebateLoyaltyDetails of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final CLRDetails clrDetailsObj = (CLRDetails) streamHelper.parseXmlToObject(xml);
		CouponsDetails couponDetails = null;

		LoyaltyDetails loyaltyDetailsObj = null;

		RebateDetails rebateDetailsObj = null;

		if (clrDetailsObj.getUserId() == null && clrDetailsObj.getCouponId() == null && clrDetailsObj.getRebateId() == null
				&& clrDetailsObj.getRebateId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{

			if (clrDetailsObj.getCouponId() != null)
			{
				couponDetails = clrGalleryDao.fetchCouponDetails(clrDetailsObj);
				if (null != couponDetails)
				{

					response = XstreamParserHelper.produceXMlFromObject(couponDetails);
					response = response.replaceAll("<string>", "<productName>");
					response = response.replaceAll("</string>", "</productName>");

				}
				else
				{
					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
				}
			}

			if (clrDetailsObj.getLoyaltyDealID() != null)
			{
				loyaltyDetailsObj = clrGalleryDao.getLoyaltyInfo(clrDetailsObj);
				if (null != loyaltyDetailsObj)
				{

					response = XstreamParserHelper.produceXMlFromObject(loyaltyDetailsObj);
					response = response.replaceAll("<string>", "<productName>");
					response = response.replaceAll("</string>", "</productName>");

				}
				else
				{
					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
				}
			}
			if (clrDetailsObj.getRebateId() != null)
			{
				rebateDetailsObj = clrGalleryDao.getRebateInfo(clrDetailsObj);
				if (null != rebateDetailsObj)
				{

					response = XstreamParserHelper.produceXMlFromObject(rebateDetailsObj);
					response = response.replaceAll("<string>", "<productName>");
					response = response.replaceAll("</string>", "</productName>");
				}
				else
				{
					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
				}
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String getCLRProdInfo(String xml) throws ScanSeeException
	{
		final String methodName = "getCLRProdInfo of Service layer ";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final CLRDetails clrDetailsObj = (CLRDetails) streamHelper.parseXmlToObject(xml);
		ProductDetails productDetailsObj = null;

		if (clrDetailsObj.getUserId() == null && clrDetailsObj.getCouponId() == null && clrDetailsObj.getRebateId() == null
				&& clrDetailsObj.getRebateId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{

			if (clrDetailsObj.getCouponId() != null)
			{
				productDetailsObj = clrGalleryDao.getCLRProdInfo(clrDetailsObj);
				if (null != productDetailsObj)
				{

					response = XstreamParserHelper.produceXMlFromObject(productDetailsObj);
				}
				else
				{
					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOPRODUCTFOUNDTEXT);
				}
			}

			if (clrDetailsObj.getLoyaltyDealID() != null)
			{
				productDetailsObj = clrGalleryDao.getCLRProdInfo(clrDetailsObj);
				if (null != productDetailsObj)
				{

					response = XstreamParserHelper.produceXMlFromObject(productDetailsObj);
				}
				else
				{
					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
				}
			}
			if (clrDetailsObj.getRebateId() != null)
			{
				productDetailsObj = clrGalleryDao.getCLRProdInfo(clrDetailsObj);
				if (null != productDetailsObj)
				{

					response = XstreamParserHelper.produceXMlFromObject(productDetailsObj);
				}
				else
				{
					response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
				}
			}

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	
	/**
	 * Method for fetching Coupon details. The method is called
	 * from the service layer.
	 * 
	 * @param xml, contains couponId, userId which are
	 *            needed for fetching coupon details.
	 * @return coupon details.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public String fetchCouponDetails(String xml) throws ScanSeeException
	{
		LOG.info("Inside CLRGalleryServiceImpl : fetchCouponDetails");
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final CLRDetails clrDetailsObj = (CLRDetails) streamHelper.parseXmlToObject(xml);
		CouponsDetails couponDetails = null;
		if (clrDetailsObj.getUserId() == null && clrDetailsObj.getCouponId() == null )
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		} else {
			couponDetails = clrGalleryDao.fetchPromotionCouponDetails(clrDetailsObj);
			if (null != couponDetails) {
				response = XstreamParserHelper.produceXMlFromObject(couponDetails);
				response = response.replaceAll("<couponInfo>", "");
				response = response.replaceAll("</couponInfo>", "");
			} else {
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		return response;
	}
	
	
	/**
	 * Method for fetching Coupon location details. The method is called
	 * from the service layer.
	 * 
	 * @param xml
	 *            , contains couponId, userId which are
	 *            needed for fetching coupon location details.
	 * @return coupon location details.
	 * @throws ScanSeeException
	 *             The exception defined for the application.
	 */

	@Override
	public String fetchCouponLocationProduct(String xml) throws ScanSeeException
	{
		LOG.info("Inside CLRGalleryServiceImpl : fetchCouponLocationProduct");
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final CLRDetails clrDetailsObj = (CLRDetails) streamHelper.parseXmlToObject(xml);
		CouponsDetails couponDetails = null;
		if (clrDetailsObj.getUserId() == null && clrDetailsObj.getCouponId() == null )
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		} else {
			couponDetails = clrGalleryDao.fetchCouponLocationProduct(clrDetailsObj);
			if (null != couponDetails) {
				response = XstreamParserHelper.produceXMlFromObject(couponDetails);
				response = response.replaceAll("<productLst>", "");
				response = response.replaceAll("</productLst>", "");
				response = response.replaceAll("<retailDetailList>", "");
				response = response.replaceAll("</retailDetailList>", "");
			} else {
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		return response;
	}


}
