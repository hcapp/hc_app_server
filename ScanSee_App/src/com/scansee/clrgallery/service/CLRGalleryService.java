package com.scansee.clrgallery.service;

import com.scansee.common.exception.ScanSeeException;

/**
 * The CLRGalleryService Interface. {@link ImplementedBy}
 * {@link CLRGalleryServiceImpl}
 * 
 * @author dileepa_cc
 */
public interface CLRGalleryService
{
	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. Calls DAO methods to get the products media info
	 * based on the media type.
	 * 
	 * @param userId
	 *            requested user.
	 * @param productId
	 *            requested product.
	 * @param retailId
	 *            requested retail
	 * @return String XML with media details.
	 * @throws ScanSeeException
	 *             throws if exception occurs
	 */
	String fetchCLRDetails(Integer userId, Integer retailId, Integer productId) throws ScanSeeException;

	/**
	 * this method validates the input parameters and returns validation error
	 * if validation fails. and calls DAO methods to get the coupons for
	 * product.
	 * 
	 * @param userId
	 *            requested user.
	 * @param productId
	 *            request product for coupons.
	 * @param clr
	 *            flag for identifying.
	 * @return String XML with coupon info.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */
	String getWishListCouponInfo(Integer userId, Integer productId, Integer clr, Integer mainMenuID) throws ScanSeeException;

	/**
	 * This method validates the input parameters and returns validation error
	 * if validation fails. and calls DAO methods to get the
	 * coupon/rebate/loyalty Details for userId.
	 * 
	 * @param xml
	 *            , contains couponId,rebateId,loyaltyId,userId,productId.
	 * @return String XML with coupon/rebate/loyalty information.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	String fetchCLRDetails(String xml) throws ScanSeeException;
	
	/**
	 * This method validates the input parameters and returns validation error
	 * if validation fails. and calls DAO methods to get the
	 * coupon/rebate/loyalty Details for userId.
	 * 
	 * @param xml
	 *            , contains couponId,rebateId,loyaltyId,userId,productId.
	 * @return String XML with coupon/rebate/loyalty information.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	String getCLRProdInfo(String xml) throws ScanSeeException;
	
 	/**
	 * Method for fetching Coupon details. The method calls
	 * calls DAO methods to get the coupon Details.
	 * 
	 * @param xml contains couponId,userId,CouponListID which
	 *            are needed for fetching coupon details.
	 * @return String XML with coupon information.
	 * @throws ScanSeeException
	 *             throws if exception occurs.
	 */

	String fetchCouponDetails(String xml) throws ScanSeeException;
	
	/**
	 * Method for fetching the Coupon All Location Details , accepts
	 * couponId,userId,CouponListID returns XML
	 * with Coupon details or with error code.
	 * 
	 * @param xml used for Fetching Coupon All Location Details.
	 * @return String response Coupon details will be returned.
	 */

	String fetchCouponLocationProduct(String xml) throws ScanSeeException;
}
