package com.scansee.clrgallery.controller;

import static com.scansee.common.util.Utility.formResponseXml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.clrgallery.service.CLRGalleryService;
import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.servicefactory.ServiceFactory;

/**
 * The class accepts the web service requests for CLRGalleryRestEasyModule.
 * 
 * @author dileepa_cc
 */
public class CLRGalleryRestEasyImpl implements CLRGalleryRestEasy
{
	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(CLRGalleryRestEasyImpl.class);
	/**
	 * debugger flag for logging.
	 */
	private static final boolean ISDEBUGENABLED = LOG.isDebugEnabled();

	/**
	 * clrGalleryService object for service call.
	 */
	private CLRGalleryService clrGalleryService = ServiceFactory.getCLRGalleryService();

	/**
	 * This method for getting Master shopping list product list CLR information
	 * based on userId,product id and retailer id.
	 * 
	 * @param userId
	 *            -As request parameter
	 * @param retailerId
	 *            -As request parameter
	 * @param productID
	 *            -As request parameter
	 * @return XML with success or error code based on success or failure of
	 *         operation.
	 */
	@Override
	public String fetchproductclrinfo(Integer userId, Integer retailerId, Integer productID)
	{
		final String methodName = "fetchMSLCLRInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved parameters userId {} productID {} retailer id {}" + userId + "" + productID + "" + retailerId);
		}
		String response = null;
		clrGalleryService = ServiceFactory.getCLRGalleryService();
		try
		{
			response = clrGalleryService.fetchCLRDetails(userId, retailerId, productID);
		}
		catch (ScanSeeException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Response returned " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * Method for fetching Coupon Info. The method is called from the service
	 * layer.
	 * 
	 * @param userId
	 *            The user id in the request.
	 * @param retailId
	 *            The retail ID in the request.
	 * @param productId
	 *            The product id in the request.
	 * @return couponsDetails The XML with Coupon Details.
	 */
	@Override
	public String getWishListCouponInfo(Integer userId, Integer productId, Integer retailId, Integer mainMenuID)
	{
		final String methodName = "getWishListCouponInfo of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("parameters received  userId" + userId + "productId :" + productId + "retailId :" + retailId);
		}
		String responseXml = null;
		clrGalleryService = ServiceFactory.getCLRGalleryService();
		try
		{
			responseXml = clrGalleryService.getWishListCouponInfo(userId, productId, retailId, mainMenuID);
		}
		catch (ScanSeeException e)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, e);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	/**
	 * Method for fetching Coupon/Rebate/Loyalty details. The method calls
	 * service layer.
	 * 
	 * @param xml
	 *            contains couponId,rebateId,loyaltyId,userId,productId which
	 *            are needed for fetching coupon/rebate/loyalty details.
	 * @return xml that contains Coupon/Rebate/Loyalty details
	 */

	@Override
	public String fetchCLRDetails(String xml)
	{
		String methodName = "fetchCouponRebateLoyaltyDetails of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("parameters received  :" + xml);
		}
		String responseXml = null;

		try
		{
			responseXml = clrGalleryService.fetchCLRDetails(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			responseXml = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}

	@Override
	public String getCLRProdInfo(String xml)
	{
		String methodName = "getCLRProdInfo of RestEasyLayer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("parameters received  :" + xml);
		}
		String responseXml = null;

		try
		{
			responseXml = clrGalleryService.getCLRProdInfo(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
			responseXml = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, responseXml);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXml;
	}
	
	/**
     * Method for fetching Coupon details. The method calls
     * service layer.
     * 
      * @param xml contains couponId,userId,CouponListID which
     *            are needed for fetching coupon details.
     * @return xml that contains Coupon details.
     */
     @Override
     public String fetchCouponDetails(String xml)
     {
           LOG.info("Inside CLRGalleryRestEasyImpl : fetchCouponDetails");
           String responseXml = null;
           try {
                 responseXml = clrGalleryService.fetchCouponDetails(xml);
           } catch (ScanSeeException exception) {
                 LOG.error("Inside CLRGalleryRestEasyImpl : fetchCouponDetails : ", exception);
                 responseXml = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
           }
           return responseXml;
     }
     
     /**
     * Method for fetching the Coupon All Location Details , accepts
     * couponId,userId,CouponListID returns XML
     * with Coupon details or with error code.
     * 
      * @param xml used for Fetching Coupon All Location Details.
     * @return String response Coupon details will be returned.
     */
     @Override
     public String fetchCouponLocationProduct(String xml)
     {
           LOG.info("Inside CLRGalleryRestEasyImpl : fetchCouponDetails");
           String responseXml = null;
           try {
                 responseXml = clrGalleryService.fetchCouponLocationProduct(xml);
           } catch (ScanSeeException exception) {
                 LOG.error("Inside CLRGalleryRestEasyImpl : fetchCouponDetails : ", exception);
                 responseXml = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
           }
           return responseXml;
     }


}
