package com.scansee.clrgallery.controller;

import static com.scansee.common.constants.ScanSeeURLPath.CLRGALLERY;
import static com.scansee.common.constants.ScanSeeURLPath.FETCHPRODUCTCLRINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETCLRDETAILS;
import static com.scansee.common.constants.ScanSeeURLPath.GETCLRPRODINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETWISHLISTCOUPONINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETCOUPONDETAILS;
import static com.scansee.common.constants.ScanSeeURLPath.GETCOUPONPRODUCTLOCATIONLIST;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 * This is a RestEasy WebService Interface for capturing CLRGallery requests.
 * 
 * @author shyamsundara_hm
 */
@Path(CLRGALLERY)
public interface CLRGalleryRestEasy
{
	/**
	 * Method for displaying coupon,rebates and loyalty information.
	 * 
	 * @param userId
	 *            used for fetching media information.
	 * @param productID
	 *            used for fetching media information.
	 * @param retailerId
	 *            fetching product media information.
	 * @return List of clr objects.
	 */

	@GET
	@Path(FETCHPRODUCTCLRINFO)
	@Produces("text/xml")
	String fetchproductclrinfo(@QueryParam("userId") Integer userId, @QueryParam("retailerId") Integer retailerId,
			@QueryParam("productId") Integer productID);

	/**
	 * Method for fetching the Coupon Info. accepts XML with the add rebate
	 * requests and returns XML with Coupon Info or with Error code. Calls the
	 * corresponding method in the service layer.
	 * 
	 * @param userId
	 *            used to for Fetching coupon information.
	 * @param productId
	 *            used to for Fetching coupon information.
	 * @param retailId
	 *            used to for Fetching coupon information.
	 * @return String response coupons will be returned.
	 */

	@GET
	@Path(GETWISHLISTCOUPONINFO)
	@Produces("text/xml")
	String getWishListCouponInfo(@QueryParam("userId") Integer userId, @QueryParam("productId") Integer productId,
			@QueryParam("retailId") Integer retailId, @QueryParam("mainMenuID") Integer mainMenuID);

	/**
	 * Method for fetching the Coupon/Rebate/Loyalty Details , accepts
	 * userId,productId,retailerId couponId,rebateId and loyaltyId returns XML
	 * with Coupon details or with error code.
	 * 
	 * @param xml
	 *            used for Fetching Coupon/Rebate/Loyalty details.
	 * @return String response Coupon/Rebate/Loyalty details will be returned.
	 */

	@POST
	@Path(GETCLRDETAILS)
	@Produces("text/xml;charset=UTF-8")
	String fetchCLRDetails(String xml);
	
	/**
	 * Method for fetching the CLR associated product information . accepts
	 * XML with the add rebate requests and returns XML with Coupon Info or with
	 * Error code. Calls the corresponding method in the service layer.
	 * 
	 * @param xml
	 *            used to for Fetching coupons,loyalty and rebate information.
	 * @return String response coupons will be returned.
	 */

	@POST
	@Path(GETCLRPRODINFO)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getCLRProdInfo(String xml);
	
	/**
	 * Method for fetching the Coupon Details , accepts
	 * couponId,userId,CouponListID returns XML
	 * with Coupon details or with error code.
	 * 
	 * @param xml used for Fetching Coupon details.
	 * @return String response Coupon details will be returned.
	 */
	@POST
	@Path(GETCOUPONDETAILS)
	@Produces("text/xml;charset=UTF-8")
	String fetchCouponDetails(String xml);
	
	/**
	 * Method for fetching the Coupon All Location Details , accepts
	 * couponId,userId,CouponListID returns XML
	 * with Coupon details or with error code.
	 * 
	 * @param xml used for Fetching Coupon All Location Details.
	 * @return String response Coupon details will be returned.
	 */
	@POST
	@Path(GETCOUPONPRODUCTLOCATIONLIST)
	@Produces("text/xml;charset=UTF-8")
	String fetchCouponLocationProduct(String xml);

}
