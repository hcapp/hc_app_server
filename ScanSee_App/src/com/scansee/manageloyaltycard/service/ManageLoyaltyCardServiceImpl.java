package com.scansee.manageloyaltycard.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.cellfire.webservice.QBridgeServiceStub.AddLoyaltyCardResponse;
import com.scansee.cellfire.webservice.QBridgeServiceStub.InfoValue;
import com.scansee.cellfire.webservice.QBridgeServiceStub.LoyaltyCard;
import com.scansee.cellfire.webservice.QBridgeServiceStub.RemoveLoyaltyCardFromUserResponse;
import com.scansee.cellfire.webservice.QBridgeServiceStub.UpdateUserInfoResponse;
import com.scansee.cellfire.webserviceclient.CellFireWebServiceClient;
import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.constants.CellFireResponseMessage;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.helper.XstreamParserHelper;
import com.scansee.common.pojos.CellFireAPIKey;
import com.scansee.common.pojos.CellFireInfoValue;
import com.scansee.common.pojos.CellFireInfoValueList;
import com.scansee.common.pojos.CellFireMerchant;
import com.scansee.common.pojos.CellFireRequest;
import com.scansee.common.pojos.ExternalAPISearchParameters;
import com.scansee.common.pojos.ExternalAPIVendor;
import com.scansee.common.pojos.LoyaltyCards;
import com.scansee.common.util.Utility;
import com.scansee.manageloyaltycard.dao.ManageLoyaltyCardDAO;

public class ManageLoyaltyCardServiceImpl implements ManageLoyaltyCardService
{

	/**
	 * Getting the logger instance.
	 */

	private static final Logger log = LoggerFactory.getLogger(ManageLoyaltyCardServiceImpl.class.getName());

	/**
	 * Instance variable for FirstUseDAO.
	 */

	/**
	 * Instance variable for Shopping list DAO instance.
	 */

	private ManageLoyaltyCardDAO manageLoyaltyCardDAO;

	public void setManageLoyaltyCardDAO(ManageLoyaltyCardDAO manageLoyaltyCardDAO)
	{
		this.manageLoyaltyCardDAO = manageLoyaltyCardDAO;
	}

	@Override
	public String updateUserInfo(String xml) throws ScanSeeException
	{

		final String methodName = "updateUserInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		int cellFireResponse;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final CellFireInfoValueList cellFireInfoValue = (CellFireInfoValueList) streamHelper.parseXmlToObject(xml);
		InfoValue[] infoValuesArry = null;
		int arrayIndex = 0;
		String responseCode = null;
		boolean isInformationSharingEnabled = false;
		if (null != cellFireInfoValue)
		{

			CellFireAPIKey cellFireAPIKey = getExternalApiInformation();
			if (cellFireAPIKey.getApiKeyParameterValue() != null && cellFireAPIKey.getTrackingCodeParameterValue() != null
					&& null != cellFireAPIKey.getParnerId())
			{
				if (cellFireInfoValue.getUserId() > 0)
				{

					CellFireWebServiceClient cellFireWebServiceClient = new CellFireWebServiceClient(cellFireAPIKey.getApiKeyParameterName(),
							cellFireAPIKey.getApiKeyParameterValue(), cellFireAPIKey.getTrackingCodeParameterName(),
							cellFireAPIKey.getTrackingCodeParameterValue());

					if (cellFireInfoValue.getInfoValList() != null && cellFireInfoValue.getInfoValList().size() > 0)
					{
						infoValuesArry = new InfoValue[cellFireInfoValue.getInfoValList().size()];

						List<CellFireInfoValue> infoValList = cellFireInfoValue.getInfoValList();
						for (CellFireInfoValue infoValue : infoValList)
						{
							InfoValue cellFireValues = new InfoValue();
							cellFireValues.setId(infoValue.getId());
							cellFireValues.setValue(infoValue.getValue());
							infoValuesArry[arrayIndex++] = cellFireValues;

							if (infoValue.getId() == 2)
							{

								if (infoValue.getValue() != null && infoValue.getValue().equals("1"))
								{
									isInformationSharingEnabled = true;

								}
								else if (infoValue.getValue() != null && infoValue.getValue().equals("0"))
								{
									isInformationSharingEnabled = false;
								}

							}
						}
					}

					UpdateUserInfoResponse userInfoResponse = cellFireWebServiceClient.updateUserInfo(String.valueOf(cellFireInfoValue.getUserId()),
							cellFireAPIKey.getParnerId(), infoValuesArry);

					cellFireResponse = userInfoResponse.getOut();
					switch (cellFireResponse)
					{
					case 0:
						responseCode = ApplicationConstants.SUCCESSCODE;
						response = CellFireResponseMessage.UPDATEUSERINFOSUCCES;
						manageLoyaltyCardDAO.updateCellFireInformationSharing(cellFireInfoValue.getUserId(), isInformationSharingEnabled);
						break;
					case -1:
						response = CellFireResponseMessage.WRONGZIPCODE;
						responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
						break;
					case -2:
						response = CellFireResponseMessage.WRONGEMAILFORMAT;
						responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
						break;
					case -3:
						response = CellFireResponseMessage.WRONGPHONEFORMAT;
						responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
						break;
					case -4:
						response = CellFireResponseMessage.INAVALIDAGE;
						responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
						break;

					}
					response = Utility.formResponseXml(responseCode, response);
				}
				else
				{

					response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
				}

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);

			}
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String addLoyaltyCard(String imputXml) throws ScanSeeException
	{
		final String methodName = "addLoyaltyCard";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		int cellFireResponse;
		String cardNumber = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final CellFireRequest cellFireRequest = (CellFireRequest) streamHelper.parseXmlToObject(imputXml);

		if (null != cellFireRequest
				&& (null != Utility.nullCheck(cellFireRequest.getCardNumber()) && null != Utility.nullCheck(cellFireRequest.getReferenceId()) && null != Utility
						.nullCheck(cellFireRequest.getMerchantId())))
		{

			log.info("CellFire Request Recieved From Client for Add Loyalty Card:::\n Card Number:" + cellFireRequest.getCardNumber()
					+ "\n Merchant ID" + cellFireRequest.getMerchantId() + "\n UserID" + cellFireRequest.getReferenceId());

			cardNumber = cellFireRequest.getCardNumber();

			if (Utility.validateSpecialChar(cardNumber))
			{

				return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDCARDNUMBERTEXT);

			}

			CellFireAPIKey cellFireAPIKey = getExternalApiInformation();
			if (cellFireAPIKey.getApiKeyParameterValue() != null && cellFireAPIKey.getTrackingCodeParameterValue() != null
					&& null != cellFireAPIKey.getParnerId())
			{

				AddLoyaltyCardResponse addLoyaltyCardResponse = addCFLoyaltyCards(cellFireRequest, cellFireAPIKey);
				cellFireResponse = addLoyaltyCardResponse.getOut();
				String responseCode = null;

				switch (cellFireResponse)
				{

				case 0:
					response = CellFireResponseMessage.SUCCESSFULASSOCIATION;
					responseCode = ApplicationConstants.SUCCESSCODE;
					manageLoyaltyCardDAO.addLoyaltyCard(cellFireRequest, String.valueOf(cellFireResponse));
					break;
				case -1:
					response = CellFireResponseMessage.WRONGCARDNUMBER;
					responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
					manageLoyaltyCardDAO.addLoyaltyCard(cellFireRequest, String.valueOf(cellFireResponse));
					break;
				case -2:
					response = CellFireResponseMessage.DIFFERENTMERCHANT;
					responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
					manageLoyaltyCardDAO.addLoyaltyCard(cellFireRequest, String.valueOf(cellFireResponse));
					break;
				case -3:
					response = CellFireResponseMessage.ADDINGCARDFAILED;
					responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
					manageLoyaltyCardDAO.addLoyaltyCard(cellFireRequest, String.valueOf(cellFireResponse));
					break;

				default:
					response = ApplicationConstants.TECHNICALPROBLEMERRORTEXT;
					responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
					break;
				}

				//
				response = Utility.formResponseXml(responseCode, response);
			}
			else
			{

				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}
		else
		{

			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String removeLoyaltyCardFromUser(String xml) throws ScanSeeException
	{
		final String methodName = "removeLoyaltyCardFromUser";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		int cellFireResponse;
		String responseCode = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final CellFireRequest cellFireRequest = (CellFireRequest) streamHelper.parseXmlToObject(xml);

		if (null != cellFireRequest
				&& (null != Utility.nullCheck(cellFireRequest.getReferenceId()) && null != Utility.nullCheck(cellFireRequest.getMerchantId())))
		{

			log.info("CellFire Request Recieved From Client for Remove Loyalty Card:::" + "\n Merchant ID" + cellFireRequest.getMerchantId()
					+ "\n UserID" + cellFireRequest.getReferenceId());
			CellFireAPIKey cellFireAPIKey = getExternalApiInformation();
			if (cellFireAPIKey.getApiKeyParameterValue() != null && cellFireAPIKey.getTrackingCodeParameterValue() != null
					&& null != cellFireAPIKey.getParnerId())
			{
				RemoveLoyaltyCardFromUserResponse removeLoyaltyCardFromUserResponse = removeCFLoyaltyCards(cellFireRequest, cellFireAPIKey);
				cellFireResponse = removeLoyaltyCardFromUserResponse.getOut();

				switch (cellFireResponse)
				{

				case 0:
					response = CellFireResponseMessage.SUCCESSFULREMOVAL;
					responseCode = ApplicationConstants.SUCCESSCODE;
					manageLoyaltyCardDAO.removeLoyaltyCard(cellFireRequest, String.valueOf(cellFireResponse));
					break;

				case -1:
					response = CellFireResponseMessage.REMOVECARDFAILED;
					responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
					manageLoyaltyCardDAO.removeLoyaltyCard(cellFireRequest, String.valueOf(cellFireResponse));
					break;

				default:
					response = ApplicationConstants.TECHNICALPROBLEMERRORTEXT;
					responseCode = ApplicationConstants.TECHNICALPROBLEMERRORCODE;
					break;
				}
				response = Utility.formResponseXml(responseCode, response);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

		}
		else
		{

			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@Override
	public String getSavedLoyaltyCards(int userId) throws ScanSeeException
	{
		final String methodName = "getSavedLoyaltyCards";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		List<CellFireMerchant> merchantList = null;
		LoyaltyCards lacrds = null;

		try
		{
			merchantList = manageLoyaltyCardDAO.getMerchants((long) userId);

			if (merchantList != null && !merchantList.isEmpty())
			{
				int arrayIndex = 0;
				CellFireAPIKey cellFireAPIKey = getExternalApiInformation();
				if (cellFireAPIKey.getApiKeyParameterValue() != null && cellFireAPIKey.getTrackingCodeParameterValue() != null
						&& null != cellFireAPIKey.getParnerId())
				{

					LoyaltyCard[] loyaltyCardsArray = getSavedCFLoyaltyCrds(cellFireAPIKey, userId);
					if (null != loyaltyCardsArray && loyaltyCardsArray.length > 0)
					{

						if (null != merchantList && !merchantList.isEmpty())
						{

							for (LoyaltyCard card : loyaltyCardsArray)
							{
								arrayIndex = 0;

								for (CellFireMerchant mechant : merchantList)
								{
									if (Integer.parseInt(mechant.getMerchId()) == card.getMerchantId())
									{
										merchantList.get(arrayIndex).setCardNumber(card.getCardNumber());
										break;
									}
									arrayIndex++;

								}

							}

						}

					}
					lacrds = new LoyaltyCards();
					// convert objects to xml.
					lacrds.setCellFireMerchant(merchantList);
					response = XstreamParserHelper.produceXMlFromObject(lacrds);
				}
				else
				{
					response = Utility
							.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
				}

			}
			else
			{
				return Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.NOMERCHANTFOUNDTEXT);
			}

			// Get the available loyalty merchants cards from the database

			return response;
		}
		catch (ScanSeeException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());
		}

	}

	/**
	 * The method for getting external API information.
	 * 
	 * @throws CellFireAPIException
	 *             The exceptions are caught and a Exception defined for the
	 *             application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public CellFireAPIKey getExternalApiInformation() throws ScanSeeException
	{

		final String methodName = "getExternalApiInformation";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<ExternalAPIVendor> externalAPIVendorList = manageLoyaltyCardDAO.getExternalAPIList(ApplicationConstants.LOYALTY);
		CellFireAPIKey cellFireAPIKey = new CellFireAPIKey();
		for (Iterator i = externalAPIVendorList.iterator(); i.hasNext();)
		{
			ExternalAPIVendor externalAPIVendor = (ExternalAPIVendor) i.next();
			ArrayList<ExternalAPISearchParameters> externalAPISearchParametersList = manageLoyaltyCardDAO.getExternalAPIInputParameters(
					Integer.valueOf(externalAPIVendor.getApiUsageID()), ApplicationConstants.LOYALTY);

			for (Iterator j = externalAPISearchParametersList.iterator(); j.hasNext();)
			{
				ExternalAPISearchParameters apiSearchParameters = (ExternalAPISearchParameters) j.next();

				if (apiSearchParameters.getApiParameter().equals(ApplicationConstants.APIKEY))
				{

					cellFireAPIKey.setApiKeyParameterName(apiSearchParameters.getApiParameter());
					cellFireAPIKey.setApiKeyParameterValue(apiSearchParameters.getApiParamtervalue());

				}
				else if (apiSearchParameters.getApiParameter().equals(ApplicationConstants.TLC))
				{

					cellFireAPIKey.setTrackingCodeParameterName(apiSearchParameters.getApiParameter());
					cellFireAPIKey.setTrackingCodeParameterValue(apiSearchParameters.getApiParamtervalue());
				}
				else if (apiSearchParameters.getApiParameter().equals(ApplicationConstants.CELLFIREPARTNERID))
				{

					cellFireAPIKey.setParnerId(apiSearchParameters.getApiParamtervalue());

				}

				// cellFireAPIKey.setParnerId("scansee-ios");
			}

		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return cellFireAPIKey;

	}

	public LoyaltyCard[] getSavedCFLoyaltyCrds(CellFireAPIKey cellFireAPIKey, int userId) throws ScanSeeException
	{

		CellFireWebServiceClient cellFireWebServiceClient = new CellFireWebServiceClient(cellFireAPIKey.getApiKeyParameterName(),
				cellFireAPIKey.getApiKeyParameterValue(), cellFireAPIKey.getTrackingCodeParameterName(),
				cellFireAPIKey.getTrackingCodeParameterValue());

		// Get Saved loyalty for a particular user by invoking cellfire
		// getLoyaltyCards API
		LoyaltyCard[] loyaltyCardsArray = cellFireWebServiceClient.getSavedLoyaltyCards(String.valueOf(userId), cellFireAPIKey.getParnerId());

		return loyaltyCardsArray;

	}

	public AddLoyaltyCardResponse addCFLoyaltyCards(CellFireRequest cellFireRequest, CellFireAPIKey cellFireAPIKey) throws ScanSeeException
	{
		CellFireWebServiceClient cellFireWebServiceClient = new CellFireWebServiceClient(cellFireAPIKey.getApiKeyParameterName(),
				cellFireAPIKey.getApiKeyParameterValue(), cellFireAPIKey.getTrackingCodeParameterName(),
				cellFireAPIKey.getTrackingCodeParameterValue());
		AddLoyaltyCardResponse addLoyaltyCardResponse = cellFireWebServiceClient.addLoyaltyCard(cellFireRequest, cellFireAPIKey.getParnerId());
		return addLoyaltyCardResponse;
	}

	public RemoveLoyaltyCardFromUserResponse removeCFLoyaltyCards(CellFireRequest cellFireRequest, CellFireAPIKey cellFireAPIKey)
			throws ScanSeeException
	{
		CellFireWebServiceClient cFWSClient = new CellFireWebServiceClient(cellFireAPIKey.getApiKeyParameterName(),
				cellFireAPIKey.getApiKeyParameterValue(), cellFireAPIKey.getTrackingCodeParameterName(),
				cellFireAPIKey.getTrackingCodeParameterValue());
		RemoveLoyaltyCardFromUserResponse removeLoyaltyCardFromUserResponse = cFWSClient.removeLoyaltyCardFromUser(cellFireRequest.getReferenceId(),
				cellFireAPIKey.getParnerId(), Integer.valueOf(cellFireRequest.getMerchantId()));
		return removeLoyaltyCardFromUserResponse;

	}

	@Override
	public String getMerchantCardDetail(int merchantId) throws ScanSeeException
	{
		final String methodName = "getMerchantCardDetail";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		CellFireMerchant cellFireMerchant = null;

		if (merchantId > 0)
		{

			cellFireMerchant = manageLoyaltyCardDAO.getLoyaltyCardDetails(merchantId);

			if (null != cellFireMerchant)
			{
				cellFireMerchant.setCardInfo(String.format(ApplicationConstants.LOYALTYCARDINFO, cellFireMerchant.getMerchName(),
						cellFireMerchant.getLoyaltyProgramName()));
				cellFireMerchant.setMerchName(null);
				cellFireMerchant.setLoyaltyProgramName(null);
				response = XstreamParserHelper.produceXMlFromObject(cellFireMerchant);

			}

		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.INVALIDREQUESTERRORCODE, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		return response;
	}

}
