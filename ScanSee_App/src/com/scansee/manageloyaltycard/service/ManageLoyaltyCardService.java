package com.scansee.manageloyaltycard.service;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.CellFireAPIKey;

public interface ManageLoyaltyCardService
{

	String updateUserInfo(String xml) throws ScanSeeException;

	String addLoyaltyCard(String imputXml) throws ScanSeeException;

	String removeLoyaltyCardFromUser(String xml) throws ScanSeeException;

	String getSavedLoyaltyCards(int userId) throws ScanSeeException;

	/**
	 * The method for getting external API information.
	 * 
	 * @throws CellFireAPIException
	 *             The exceptions are caught and a Exception defined for the
	 *             application is thrown which is caught in the Controller
	 *             layer.
	 */

	CellFireAPIKey getExternalApiInformation() throws ScanSeeException;
	
	String getMerchantCardDetail(int merchantId) throws ScanSeeException;
}
