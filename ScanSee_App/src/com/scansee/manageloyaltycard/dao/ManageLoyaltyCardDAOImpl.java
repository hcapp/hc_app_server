package com.scansee.manageloyaltycard.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AddRemoveCLR;
import com.scansee.common.pojos.CellFireMerchant;
import com.scansee.common.pojos.CellFireRequest;
import com.scansee.common.pojos.ExternalAPISearchParameters;
import com.scansee.common.pojos.ExternalAPIVendor;

public class ManageLoyaltyCardDAOImpl implements ManageLoyaltyCardDAO
{

	/**
	 * Logger instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ManageLoyaltyCardDAOImpl.class);
	/**
	 * Variable for jdbcTemplate.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedure.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * The method for getting api list information.
	 * 
	 * @param moduleName
	 *            request parameter
	 * @return ExternalAPIVendor.
	 * @throws WishPondExternalAPIException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown.
	 */

	@Override
	public ArrayList<ExternalAPIVendor> getExternalAPIList(String moduleName) throws ScanSeeException
	{
		final String methodName = "getExternalAPIList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ExternalAPIVendor> externalAPIList = null;
		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);

			simpleJdbcCall.withProcedureName("usp_GetAPIList");
			simpleJdbcCall.returningResultSet("externalAPIListInfo", new BeanPropertyRowMapper<ExternalAPIVendor>(ExternalAPIVendor.class));

			final SqlParameterSource externalAPIListParameters = new MapSqlParameterSource().addValue("prSubModule", moduleName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			externalAPIList = (ArrayList<ExternalAPIVendor>) resultFromProcedure.get("externalAPIListInfo");

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get("ErrorNumber"));
				LOG.error("Error occurred in usp_GetAPIList Store Procedure error number: {}" + errorNum + " and error message:{}");
				throw new ScanSeeException(errorMsg);
			}
			else if (null != externalAPIList && !externalAPIList.isEmpty())
			{
				return externalAPIList;
			}

		}
		catch (EmptyResultDataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return externalAPIList;
	}

	/**
	 * The method for getting external api input parameters.
	 * 
	 * @param apiUsageID
	 *            as a parameter.
	 * @param moduleName
	 *            as a parameter.
	 * @return ExternalAPISearchParameters as ArrayList.
	 * @throws WishPondExternalAPIException
	 *             The exceptions are caught and a Exception defined for the
	 *             application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public ArrayList<ExternalAPISearchParameters> getExternalAPIInputParameters(Integer apiUsageID, String moduleName) throws ScanSeeException
	{
		final String methodName = "getExternalAPIList";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		ArrayList<ExternalAPISearchParameters> externalAPIInputParameters = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetAPIInputParameters");
			simpleJdbcCall.returningResultSet("externalAPIInputParameters", new BeanPropertyRowMapper<ExternalAPISearchParameters>(
					ExternalAPISearchParameters.class));

			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("prAPIUsageID", apiUsageID);
			externalAPIListParameters.addValue("PrAPISubModuleName", moduleName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			externalAPIInputParameters = (ArrayList<ExternalAPISearchParameters>) resultFromProcedure.get("externalAPIInputParameters");

			if (null != resultFromProcedure.get("ErrorNumber"))
			{
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get("ErrorNumber"));
				LOG.error("Error occurred in usp_GetAPIList Store Procedure error number: {}" + errorNum + " and error message -->: {}");
				throw new ScanSeeException(errorMsg);
			}
			else if (null != externalAPIInputParameters && !externalAPIInputParameters.isEmpty())
			{
				return externalAPIInputParameters;
			}
		}
		catch (EmptyResultDataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);

		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return externalAPIInputParameters;
	}

	@Override
	public List<CellFireMerchant> getMerchants(Long userId) throws ScanSeeException
	{
		final String methodName = "getMerchants";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<CellFireMerchant> merchList = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_CellFireRetailerList");
			simpleJdbcCall.withSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.returningResultSet("merchants", new BeanPropertyRowMapper<CellFireMerchant>() {
				public CellFireMerchant mapRow(ResultSet rs, int row) throws SQLException
				{
					CellFireMerchant cfm = new CellFireMerchant();
					cfm.setRetailId(rs.getString("RetailID"));
					cfm.setMerchId(rs.getString("CellFireMerchantID"));
					cfm.setMerchName(rs.getString("RetailName"));
					cfm.setMerchLogoBigImg(rs.getString("merchLogoBigImg"));
					cfm.setMerchLogoSmlImg(rs.getString("merchLogoSmlImg"));
					cfm.setCardNumber(null);
					return cfm;
				}
			});
			// simpleJdbcCall.returningResultSet("merchants", new
			// BeanPropertyRowMapper<CellFireMerchant>(CellFireMerchant.class));
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("UserID", userId);
			Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			if (null == resultFromProcedure.get("ErrorNumber"))
			{
				merchList = (List<CellFireMerchant>) resultFromProcedure.get("merchants");
			}
			else
			{
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get("ErrorNumber"));
				LOG.error("Error occurred in usp_CellFireRetailerList Store Procedure error number: {}" + errorNum + " and error message -->: {}");
				throw new ScanSeeException(errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());

		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return merchList;
	}

	@Override
	public String addLoyaltyCard(CellFireRequest cellFireRequest,String statusCode) throws ScanSeeException
	{
		final String methodName = "addLoyaltyCard";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_CellFireAddLoyaltyCard");
			simpleJdbcCall.withSchemaName(ApplicationConstants.SCHEMANAME);
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("UserID", cellFireRequest.getReferenceId());
			externalAPIListParameters.addValue("RetailID", cellFireRequest.getRetailId());
			externalAPIListParameters.addValue("LoyaltyCardNumber", cellFireRequest.getCardNumber());
			externalAPIListParameters.addValue("StatusCode", statusCode);
			Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			if (null == resultFromProcedure.get("ErrorNumber"))
			{
				int addStatus = (Integer) resultFromProcedure.get("Status");
				if (addStatus == 0)
				{
					response = ApplicationConstants.SUCCESS;
					LOG.info("Add Loyalty card usp_CellFireAddLoyaltyCard executes successfully");
				}
				else
				{
					LOG.error("Failed add loyalty card usp_CellFireAddLoyaltyCard");
					throw new ScanSeeException("Failed add loyalty card usp_CellFireAddLoyaltyCard");
				}
			}
			else
			{
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get("ErrorNumber"));
				LOG.error("Error occurred in usp_CellFireAddLoyaltyCard Store Procedure error number: {}" + errorNum + " and error message -->: {}");
				throw new ScanSeeException(errorMsg);
			}
		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());
		}

		return response;
	}

	@Override
	public String removeLoyaltyCard(CellFireRequest cellFireRequest,String statusCode) throws ScanSeeException
	{
		final String methodName = "addLoyaltyCard";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_CellFireRemoveLoyaltyCard");
			simpleJdbcCall.withSchemaName(ApplicationConstants.SCHEMANAME);
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("UserID", cellFireRequest.getReferenceId());
			externalAPIListParameters.addValue("LoyaltyCardNumber", cellFireRequest.getCardNumber());
			externalAPIListParameters.addValue("StatusCode", statusCode);
			Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			if (null == resultFromProcedure.get("ErrorNumber"))
			{
				int addStatus = (Integer) resultFromProcedure.get("Status");
				if (addStatus == 0)
				{
					response = ApplicationConstants.SUCCESS;
					LOG.info("remove Loyalty card usp_CellFireRemoveLoyaltyCard executes successfully");
				}
				else
				{
					LOG.error("Failed add loyalty card usp_CellFireRemoveLoyaltyCard");
					throw new ScanSeeException("Failed add loyalty card usp_CellFireRemoveLoyaltyCard");
				}
			}
			else
			{
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get("ErrorNumber"));
				LOG.error("Error occurred in usp_CellFireRemoveLoyaltyCard Store Procedure error number: {}" + errorNum
						+ " and error message -->: {}" + errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());
		}

		return response;
	}


	@Override
	public CellFireRequest getCardInfo(AddRemoveCLR clrAddRemove) throws ScanSeeException
	{
		
		final String methodName = "getCardInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		CellFireRequest cfrequest = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_CellFireUserCouponandCardInfoDisplay");
			simpleJdbcCall.withSchemaName(ApplicationConstants.SCHEMANAME);
			final MapSqlParameterSource loyaltyparams = new MapSqlParameterSource();
			loyaltyparams.addValue("UserID", clrAddRemove.getUserId());
			loyaltyparams.addValue("LoyaltyDealID", clrAddRemove.getloyaltyDealId());
			loyaltyparams.addValue("RetailerID", clrAddRemove.getMerchantId());
			Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(loyaltyparams);
			if (null == resultFromProcedure.get("ErrorNumber"))
			{
				cfrequest = new CellFireRequest();
				cfrequest.setCardNumber((String) resultFromProcedure.get("CardNumber"));
				cfrequest.setCfLoyaltyId((String) resultFromProcedure.get("CellFireCouponID"));
				cfrequest.setCfContentVersion((String) resultFromProcedure.get("CellFireVersionNumber"));
				cfrequest.setFirstTimeClip((Boolean)resultFromProcedure.get("FirstMerchantClip"));
				cfrequest.setWaitTime(String.valueOf(resultFromProcedure.get("WaitTimeInMinutes")));
					LOG.info("usp_CellFireUserCouponandCardInfoDisplay executes successfully");
			}
			else
			{
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get("ErrorNumber"));
				LOG.error("Error occurred in usp_CellFireUserCouponandCardInfoDisplay Store Procedure error number: {}" + errorNum + " and error message -->: {}"+errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());
		}
		return cfrequest;
	}


	@Override
	public CellFireMerchant getLoyaltyCardDetails(int merchantId) throws ScanSeeException
	{
		final String methodName = "getLoyaltyCardDetails";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		List<CellFireMerchant> cardDetail = null;
		CellFireMerchant merchantCardDetail = null;
		String retailName = null;
		String loyaltyProgramName = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_UserAddLoyaltyCardGetMerchantDetails");
			simpleJdbcCall.withSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.returningResultSet("cardDetail", new BeanPropertyRowMapper<CellFireMerchant>() {
				public CellFireMerchant mapRow(ResultSet rs, int row) throws SQLException
				{
					CellFireMerchant cfm = new CellFireMerchant();
					cfm.setRetailId(rs.getString("RetailID"));
					cfm.setMerchId(rs.getString("CellFireMerchantID"));
					cfm.setMerchName(rs.getString("RetailName"));
					cfm.setLoyaltyProgramName(rs.getString("LoyaltyProgramName"));
					cfm.setCardSupportMessage(rs.getString("CardSupportMessage"));
					cfm.setLoyaltySmallImg(rs.getString("LoyaltySmallImage"));
					cfm.setLoyaltyLargeImg(rs.getString("LoyaltyLargeImage"));

					return cfm;
				}
			});
			// simpleJdbcCall.returningResultSet("merchants", new
			// BeanPropertyRowMapper<CellFireMerchant>(CellFireMerchant.class));
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("CellFireMerchantID", merchantId);
			Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			if (null == resultFromProcedure.get("ErrorNumber"))
			{
				cardDetail = (List<CellFireMerchant>) resultFromProcedure.get("cardDetail");
				if (cardDetail != null && !cardDetail.isEmpty())
				{

					merchantCardDetail = cardDetail.get(0);
				}
			}
			else
			{
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get("ErrorNumber"));
				LOG.error("Error occurred in usp_UserAddLoyaltyCardGetMerchantDetails Store Procedure error number: {}" + errorNum
						+ " and error message -->: {}");
				throw new ScanSeeException(errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());

		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return merchantCardDetail;
	}

	@Override
	public String updateCellFireInformationSharing(int userId, boolean isEnabled) throws ScanSeeException
	{
		final String methodName = "addLoyaltyCard";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.withProcedureName("usp_CellFireUserInfoUpdation");
			simpleJdbcCall.withSchemaName(ApplicationConstants.SCHEMANAME);
			final MapSqlParameterSource externalAPIListParameters = new MapSqlParameterSource();
			externalAPIListParameters.addValue("UserID", userId);
			externalAPIListParameters.addValue("CellFireInformationFlag", isEnabled);
			
			Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(externalAPIListParameters);
			if (null == resultFromProcedure.get("ErrorNumber"))
			{
				int addStatus = (Integer) resultFromProcedure.get("Status");
				if (addStatus == 0)
				{
					response = ApplicationConstants.SUCCESS;
					LOG.info("Update CellFire Information Sharing usp_CellFireUserInfoUpdation executes successfully");
				}
				else
				{
					LOG.error("Failed add loyalty card usp_CellFireAddLoyaltyCard");
					throw new ScanSeeException("Failed to Update CellFire Information Sharing usp_CellFireUserInfoUpdation ");
				}
			}
			else
			{
				final String errorMsg = (String) resultFromProcedure.get("ErrorMessage");
				final String errorNum = Integer.toString((Integer) resultFromProcedure.get("ErrorNumber"));
				LOG.error("Error occurred in usp_CellFireUserInfoUpdation Store Procedure error number: {}" + errorNum + " and error message -->: {}");
				throw new ScanSeeException(errorMsg);
			}
		}
		catch (Exception exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());
		}

		return response;
	}


}
