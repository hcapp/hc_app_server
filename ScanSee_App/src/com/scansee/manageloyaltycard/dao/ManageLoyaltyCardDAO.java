package com.scansee.manageloyaltycard.dao;

import java.util.ArrayList;
import java.util.List;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AddRemoveCLR;
import com.scansee.common.pojos.CellFireMerchant;
import com.scansee.common.pojos.CellFireRequest;
import com.scansee.common.pojos.ExternalAPISearchParameters;
import com.scansee.common.pojos.ExternalAPIVendor;

public interface ManageLoyaltyCardDAO
{

	/**
	 * The method for getting external API information.
	 * 
	 * @param moduleName
	 *            request parameter
	 * @return ExternalAPIVendor as ArrayList.
	 * @throws WishPondExternalAPIException
	 *             The exceptions are caught and a Exception defined for the
	 *             application is thrown which is caught in the Controller
	 *             layer.
	 */
	ArrayList<ExternalAPIVendor> getExternalAPIList(String moduleName) throws ScanSeeException;

	/**
	 * The method for getting external api input parameters.
	 * 
	 * @param apiUsageID
	 *            as a parameter.
	 * @param moduleName
	 *            as a parameter.
	 * @return ExternalAPISearchParameters as ArrayList.
	 * @throws WishPondExternalAPIException
	 *             The exceptions are caught and a Exception defined for the
	 *             application is thrown which is caught in the Controller
	 *             layer.
	 */
	ArrayList<ExternalAPISearchParameters> getExternalAPIInputParameters(Integer apiUsageID, String moduleName) throws ScanSeeException;

	List<CellFireMerchant> getMerchants(Long userId) throws ScanSeeException;

	String addLoyaltyCard(CellFireRequest cellFireRequest,String statusCode) throws ScanSeeException;

	String removeLoyaltyCard(CellFireRequest cellFireRequest,String statusCode) throws ScanSeeException;

	CellFireMerchant getLoyaltyCardDetails(int merchantId) throws ScanSeeException;

	String updateCellFireInformationSharing(int userId, boolean isEnabled) throws ScanSeeException;
	
	CellFireRequest getCardInfo(AddRemoveCLR clrAddRemove)throws ScanSeeException;
}
