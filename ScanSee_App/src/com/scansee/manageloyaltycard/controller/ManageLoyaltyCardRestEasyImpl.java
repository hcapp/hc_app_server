package com.scansee.manageloyaltycard.controller;

import static com.scansee.common.util.Utility.formResponseXml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.servicefactory.ServiceFactory;
import com.scansee.manageloyaltycard.service.ManageLoyaltyCardService;

public class ManageLoyaltyCardRestEasyImpl implements ManageLoyaltyCardRestEasy
{

	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(ManageLoyaltyCardRestEasyImpl.class);
	/**
	 * debugger flag for logging.
	 */
	private static final boolean ISDEBUGENABLED = LOG.isDebugEnabled();

	@Override
	public String cellFireUpdateUserInfo(String xml)
	{

		final String methodName = "authenticateLoginUser";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved XML :" + xml);
		}
		String responseXML = null;
		
		final ManageLoyaltyCardService manageLoyaltyCardService = ServiceFactory.getManageLoyaltyCardService();
		try
		{
			responseXML = manageLoyaltyCardService.updateUserInfo(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response is:" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is a RestEasy method to Add loyalty card. Method Type:POST
	 * 
	 * @param xml
	 *            containing user information.
	 * @return the XML contains add loyalty card status status .
	 */
	@Override
	public String cellFireAddLoyaltyCard(String xml)
	{
		final String methodName = "cellFireAddLoyaltyCard";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved XML :" + xml);
		}
		String responseXML = null;

		final ManageLoyaltyCardService manageLoyaltyCardService = ServiceFactory.getManageLoyaltyCardService();
		try
		{
			responseXML = manageLoyaltyCardService.addLoyaltyCard(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response is:" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is a RestEasy method to remove loyalty card. Method Type:POST
	 * 
	 * @param xml
	 *            containing user information.
	 * @return the XML contains status of remove loyalty .
	 */
	@Override
	public String cellFireRemoveLoyaltyCard(String xml)
	{
		final String methodName = "cellFireRemoveLoyaltyCard";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved XML :" + xml);
		}
		String responseXML = null;

		final ManageLoyaltyCardService manageLoyaltyCardService = ServiceFactory.getManageLoyaltyCardService();
		try
		{
			responseXML = manageLoyaltyCardService.removeLoyaltyCardFromUser(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response is:" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is a RestEasy method to get saved loyaty cards. Method Type:POST
	 * 
	 * @param userId
	 * @return the XML contains loyalty programs.
	 */
	@Override
	public String getSavedLoyaltyCards(Integer userId)
	{

		final String methodName = "cellFireRemoveLoyaltyCard";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXML = null;

		final ManageLoyaltyCardService manageLoyaltyCardService = ServiceFactory.getManageLoyaltyCardService();
		try
		{
			responseXML = manageLoyaltyCardService.getSavedLoyaltyCards(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response is:" + responseXML);
		}
		return responseXML;
	}

	@Override
	public String getMerchantCardDetail(Integer merchantId)
	{
		final String methodName = "getMerchantCardDetail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXML = null;

		final ManageLoyaltyCardService manageLoyaltyCardService = ServiceFactory.getManageLoyaltyCardService();
		try
		{
			responseXML = manageLoyaltyCardService.getMerchantCardDetail(merchantId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response is:" + responseXML);
		}
		return responseXML;
	}

}
