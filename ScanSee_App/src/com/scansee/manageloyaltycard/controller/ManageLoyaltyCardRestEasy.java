package com.scansee.manageloyaltycard.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.scansee.common.constants.ScanSeeURLPath;

@Path(ScanSeeURLPath.MANAGELOYALTYCARD)
public interface ManageLoyaltyCardRestEasy
{

	/**
	 * This is a RestEasy method to update cellfire user information. Method
	 * Type:POST
	 * 
	 * @param xml
	 *            containing user information.
	 * @return the XML contains user profile updation status .
	 */
	@POST
	@Path(ScanSeeURLPath.CELLFIREUPDATEUSERINFO)
	@Produces("text/xml")
	@Consumes("text/xml")
	String cellFireUpdateUserInfo(String xml);

	
	/**
	 * This is a RestEasy method to Add loyalty card. Method
	 * Type:POST
	 * 
	 * @param xml
	 *            containing user information.
	 * @return the XML contains add loyalty card status status .
	 */
	@POST
	@Path(ScanSeeURLPath.CELLFIREADDLOYALTYCARD)
	@Produces("text/xml")
	@Consumes("text/xml")
	String cellFireAddLoyaltyCard(String xml);

	/**
	 * This is a RestEasy method to remove loyalty card. Method
	 * Type:POST
	 * 
	 * @param xml
	 *            containing user information.
	 * @return the XML contains status of remove loyalty .
	 */
	@POST
	@Path(ScanSeeURLPath.CELLFIREREMOVELOYALTYCARD)
	@Produces("text/xml")
	@Consumes("text/xml")
	String cellFireRemoveLoyaltyCard(String xml);

	/**
	 * This is a RestEasy method to get saved loyaty cards. Method
	 * Type:POST
	 * 
	 * @param userId
	 * @return the XML contains loyalty programs.
	 */
	@GET
	@Path(ScanSeeURLPath.GETMERCHANTS)
	@Produces("text/xml")
	String getSavedLoyaltyCards(@QueryParam("userId") Integer userId);
	
	/**
	 * This is a RestEasy method to get saved loyaty cards Detail. Method
	 * Type:POST
	 * 
	 * @param userId
	 * @return the XML contains loyalty programs.
	 */
	@GET
	@Path(ScanSeeURLPath.GETCARDDETAIL)
	@Produces("text/xml")
	String getMerchantCardDetail(@QueryParam("merchantId") Integer merchantId);

}
