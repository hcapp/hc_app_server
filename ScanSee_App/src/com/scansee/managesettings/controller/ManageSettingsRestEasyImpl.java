package com.scansee.managesettings.controller;

import static com.scansee.common.util.Utility.formResponseXml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.servicefactory.ServiceFactory;
import com.scansee.managesettings.service.ManageSettingsService;

/**
 * The ManageSettingRestEasy class has methods to accept requests for manage
 * settings module from client. The method is selected based on the URL Path and
 * the type of request(GET,POST). It invokes the appropriate method in Service
 * layer.
 * 
 * @author dileepa_cc.
 */

public class ManageSettingsRestEasyImpl implements ManageSettingsRestEasy
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ManageSettingsRestEasyImpl.class);

	/**
	 * debugger flag for logging.
	 */
	private static final boolean ISDEBUGENABLED = LOG.isDebugEnabled();

	/**
	 * The variable for return XML.
	 */
	private String responseXML = "";

	/**
	 * The default constructor.
	 */

	public ManageSettingsRestEasyImpl()
	{

	}

	/**
	 * This is a RestEasy WebService Method for fetching user information for
	 * the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return XML containing user information in the response.
	 */
	public String fetchUserInfo(Integer userId, boolean isCellFireRequest)
	{
		final String methodName = "fetchUserInfo in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.info("Request recieved userId" + userId);
		}

		final ManageSettingsService manageSettings = ServiceFactory.getManageSettingsService();
		try
		{

			responseXML = manageSettings.fetchUserInfo(userId,isCellFireRequest);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Response returned for fetchUserInfo" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is a RestEasy WebService method for saving user information. Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            containing user information.
	 * @return the XML in response with Success or failure error code.
	 */
	public String insertUserInfo(String xml)
	{
		final String methodName = "insertUserInfo in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ManageSettingsService manageSettings = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettings.insertUserInfo(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXML;
	}

	/**
	 * The RestEasy WebService method for fetchiong user preferences for the
	 * given userID.Method Type:GET.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return the XML containing settings information in the response.
	 */
	public String fetchSettings(Integer userId)
	{
		final String methodName = "fetchUserPreference";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved userId:" + userId);
		}
		final ManageSettingsService manageSettings = ServiceFactory.getManageSettingsService();

		try
		{
			responseXML = manageSettings.fetchUserPreferences(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXML;
	}

	/**
	 * The RestEasy WebService method for saving user preferences Information.
	 * Method Type: POST.
	 * 
	 * @param xml
	 *            containing settings information.
	 * @return the XML in response based on the success or failure.
	 */
	@Override
	public String saveSettings(String xml)
	{
		final String methodName = "saveUserPreference in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART);
		String responseXML = null;
		final ManageSettingsService manageSettings = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettings.saveUserPreference(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}

	/**
	 * The RestEasy WebService method for fetching user favourite categories.
	 * Method Type:GET.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.If userId is
	 *            null then it is invalid request
	 * @param lowerlimit
	 *            as request parameter
	 * @return the XML containing user favourite categories information in the
	 *         response.
	 */

	@Override
	public String getUserFavCategories(Integer userId, Integer lowerlimit)
	{
		final String methodName = "getUserFavCategories in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved userId:" + userId);
		}

		final ManageSettingsService manageSettingsService = ServiceFactory.getManageSettingsService();

		try
		{
			responseXML = manageSettingsService.getUserFavCategories(userId, lowerlimit);
		}
		catch (ScanSeeException e)
		{
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + e);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);

		}
		if (ISDEBUGENABLED)
		{
			LOG.debug("Response returned getUserFavCategories:", responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXML;

	}

	/**
	 * The RestEasy WebService method for saving User Favourite Categories.
	 * Method Type:POST.
	 * 
	 * @param inputXML
	 *            contaning user favourite categories information.
	 * @return the XML in the response with Success or failure error code.
	 */

	@Override
	public String setUserFavCategories(String inputXML)
	{
		final String methodName = "setUserFavCategories in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ManageSettingsService manageSettingsService = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettingsService.setUserFavCategories(inputXML);
		}
		catch (ScanSeeException e)
		{
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + e);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}

	/**
	 * The RestEasy WebService method for fetching the user Preferred retailers.
	 * Method Type:POST.
	 * 
	 * @param xml
	 *            containing the request information.
	 * @return the XML containing user preferred retailers information.
	 */
	@Override
	public String getPreferredRetailers(String xml)
	{
		final String methodName = "getPreferredRetailers in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ManageSettingsService manageSettingsService = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettingsService.getPreferredRetailers(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);

		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXML;

	}

	/**
	 * The RestEasy WebService method for saving User Preferred Retailers.Method
	 * Type:POST.
	 * 
	 * @param inputXML
	 *            in the request containing retailer information.
	 * @return response XML xml with Success or failure error code.
	 */
	@Override
	public String setPreferredRetailers(String inputXML)
	{

		final String methodName = "setPreferredRetailers";
		final ManageSettingsService manageSettingsService = ServiceFactory.getManageSettingsService();

		try
		{
			responseXML = manageSettingsService.setPreferredRetailers(inputXML);
		}
		catch (ScanSeeException e)
		{
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + e);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}

	/**
	 * The RestEasy WebService method for fetching all countries. Method
	 * Type:GET
	 * 
	 * @return the XML in response.
	 */
	@Override
	public String fetchAllCounries()
	{
		final String methodName = "getFetchAllCounries in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ManageSettingsService manageSettings = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettings.fetchAllCountries();
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}

	/**
	 * The RestEasy WebService method for fetching all states.Method Type:GET.
	 * 
	 * @return the XML in response.
	 */
	@Override
	public String fetchAllStates()
	{
		final String methodName = "getFetchAllStates in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ManageSettingsService manageSettings = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettings.fetchAllStates();
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}

	/**
	 * The RestEasy WebService method for fetching all cities for the given
	 * state.Method Type:GET.
	 * 
	 * @param stateName
	 *            for which the cities to be fetched.
	 * @param searchKeyword
	 *            for which the cities to be fetched.
	 * @return the XML in response containing cities information.
	 */
	@Override
	public String fetchAllCities(String stateName, String searchKeyword)
	{
		final String methodName = "getFetchAllStates in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved Data" + "StateName: " + stateName + " searchKeyword: " + searchKeyword);
		}
		final ManageSettingsService manageSettings = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettings.fetchAllCities(stateName, searchKeyword);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}

	/**
	 * The RestEasy WebService method which fetches states and cities for the
	 * given ZipCode.Method Type:GET.
	 * 
	 * @param zipCode
	 *            for which coutries and states to be fetched.
	 * @return the XML in response contaning states and cities information.
	 */

	@Override
	public String fetchAllStatesAndCities(String zipCode)
	{
		final String methodName = "getFetchAllStatesAndCities in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved zipCode:" + zipCode);
		}
		final ManageSettingsService manageSettings = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettings.fetchAllCitieAndStates(zipCode);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}

	/**
	 * The RestEasy WebService method to change User Password.Method Type:POST.
	 * 
	 * @param inputXML
	 *            in the request
	 * @return the XML in response based on Success or failure code.
	 */

	@Override
	public String changePassword(String inputXML)
	{
		final String methodName = "changePassword in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final ManageSettingsService manageSettingsService = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettingsService.changePassword(inputXML);
		}
		catch (ScanSeeException e)
		{
			LOG.info(ApplicationConstants.EXCEPTIONOCCURRED + e);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);

		}
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG, responseXML);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return responseXML;
	}

	/**
	 * This method for fetching all cities group by state.Call the methods in
	 * service layer Method Type:GET.
	 * 
	 * @return the XML in response based on Success or failure code.
	 */
	@Override
	public String fetchCategorizedCities()
	{
		final String methodName = "fetchCategorizedCities in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved zipCode:");
		}
		final ManageSettingsService manageSettings = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettings.fetchCategorizedCities();
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}

	@Override
	public String saveUserEmailId(Integer userId, String emailId)
	{
		final String methodName = "getFetchAllStates in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Request recieved Data" + "userId: " + userId + " emailId: " + emailId);
		}
		final ManageSettingsService manageSettings = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettings.saveUserEmailId(userId, emailId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}

	@Override
	public String fetchUniversityStates()
	{
		final String methodName = "fetchUniversityStates in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("No input parameter");
		}
		final ManageSettingsService manageSettings = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettings.fetchUnivStates();
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}

	@Override
	public String fetchUniversities(String stateAbv)
	{
		final String methodName = "fetchUniversities in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(" input parameter stateAbv:" + stateAbv);
		}
		final ManageSettingsService manageSettings = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettings.fetchUniversities(stateAbv);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}

	/**
	 * The method for fetching postal codes from the database for the given
	 * state and city.
	 * 
	 * @param stateName
	 *            - for which postal code information need to be fetched.
	 * @param cityName
	 *            - for which postal code information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return list of postal codes.
	 */
	@Override
	public String fetchPostalCode(String stateName, String cityName)
	{
		final String methodName = "fetchPostalCode in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(" input parameter stateName:" + stateName + "City name :" + cityName);
		}
		final ManageSettingsService manageSettings = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettings.fetchPostalCode(stateName, cityName);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is a RestEasy WebService Method get PushNotification Alert on/off
	 * and to get wish list radius
	 * 
	 * @param userId
	 *            as request
	 * @return XML containing push notification alert on/off and wish list
	 *         radius.
	 */
	@Override
	public String getUserSettings(String xml)
	{
		final String methodName = "getUserSettings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Received XML " + xml);
		}
		String responseXML = null;
		final ManageSettingsService manageSettingsService = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettingsService.getUserSettings(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is a RestEasy WebService Method for enabling or disabling
	 * PushNotification Alert and to set wish list radius
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */
	@Override
	public String setUserSettings(String xml)
	{
		final String methodName = "setUserSettings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Received XML " + xml);
		}
		String responseXML = null;
		final ManageSettingsService manageSettingsService = ServiceFactory.getManageSettingsService();
		try
		{
			responseXML = manageSettingsService.setUserSettings(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + responseXML);
		}
		return responseXML;
	}
}
