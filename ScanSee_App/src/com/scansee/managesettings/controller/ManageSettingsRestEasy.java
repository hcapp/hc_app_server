package com.scansee.managesettings.controller;

import static com.scansee.common.constants.ScanSeeURLPath.FETCHUSERINFO;
import static com.scansee.common.constants.ScanSeeURLPath.INSERTUSERINFO;
import static com.scansee.common.constants.ScanSeeURLPath.MANAGESETTINGSBASEURL;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.inject.ImplementedBy;
import com.scansee.common.constants.ScanSeeURLPath;

/**
 * This resteasy for managesettings module.{@link ImplementedBy}
 * {@link ManageSettingsRestEasyImpl}.
 * 
 * @author dileepa_cc
 */
@Path(MANAGESETTINGSBASEURL)
public interface ManageSettingsRestEasy {

	/**
	 * This is a RestEasy WebService Method for fetching user information for
	 * the given userId. Method Type:GET.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return XML containing user information in the response.
	 */
	@GET
	@Path(FETCHUSERINFO)
	@Produces("text/xml;charset=UTF-8")
	String fetchUserInfo(@QueryParam("USERID") Integer userId,
			@QueryParam("iscellfirereq") boolean isCellFireRequest);

	/**
	 * This is a RestEasy WebService method for saving user information. Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            containing user information.
	 * @return the XML in response with Success or failure error code.
	 */
	@POST
	@Path(INSERTUSERINFO)
	@Produces("text/xml")
	@Consumes("text/xml")
	String insertUserInfo(String xml);

	/**
	 * The RestEasy WebService method for fetching user preferences for the
	 * given userID.Method Type:GET.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return the XML containing settings information in the response.
	 */

	@GET
	@Path("/fetchsettings")
	@Produces("text/xml;charset=UTF-8")
	String fetchSettings(@QueryParam("userId") Integer userId);

	/**
	 * The RestEasy WebService method for saving user preferences Information.
	 * Method Type: POST.
	 * 
	 * @param xml
	 *            containing settings information.
	 * @return the XML in response based on the success or failure.
	 */

	@POST
	@Path(ScanSeeURLPath.SAVEUSERPREFERENCE)
	@Produces("text/xml")
	@Consumes("text/xml")
	String saveSettings(String xml);

	/**
	 * The RestEasy WebService method for fetching user favorite categories.
	 * Method Type:GET.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.If userId is
	 *            null then it is invalid request
	 * @param lowerLimit
	 *            as request parameter for pagination
	 * @return the XML containing user favorite categories information in the
	 *         response.
	 */
	@GET
	@Path(ScanSeeURLPath.GETFAVCATEGORIES)
	@Produces("text/xml;charset=UTF-8")
	String getUserFavCategories(@QueryParam("userId") Integer userId,
			@QueryParam("lowerlimit") Integer lowerLimit);

	/**
	 * The RestEasy WebService method for saving User Favorite Categories.
	 * Method Type:POST.
	 * 
	 * @param inputXML
	 *            Containing user favorite categories information.
	 * @return the XML in the response with Success or failure error code.
	 */

	@POST
	@Path(ScanSeeURLPath.SETFAVCATEGORIES)
	@Produces("text/xml")
	@Consumes("text/xml")
	String setUserFavCategories(String inputXML);

	/**
	 * The RestEasy WebService method for fetching the user Preferred retailers.
	 * Method Type:POST.
	 * 
	 * @param inputXML
	 *            containing the request information.
	 * @return the XML containing user preferred retailers information.
	 */

	@POST
	@Path(ScanSeeURLPath.GETPREFEREDRETAILERS)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getPreferredRetailers(String inputXML);

	/**
	 * The RestEasy WebService method for saving User Preferred Retailers.Method
	 * Type:POST.
	 * 
	 * @param inputXML
	 *            in the request containing retailer information.
	 * @return response XML xml with Success or failure error code.
	 */
	@POST
	@Path(ScanSeeURLPath.SETPREFEREDRETAILERS)
	@Produces("text/xml")
	@Consumes("text/xml")
	String setPreferredRetailers(String inputXML);

	/**
	 * The RestEasy WebService method for fetching all countries. Method
	 * Type:GET
	 * 
	 * @return the XML in response.
	 */
	@GET
	@Path(ScanSeeURLPath.FETCHALLCOUNTRIES)
	@Produces("text/xml")
	String fetchAllCounries();

	/**
	 * The RestEasy WebService method for fetching all states.Method Type:GET.
	 * 
	 * @return the XML in response.
	 */
	@GET
	@Path(ScanSeeURLPath.FETCHALLSTATES)
	@Produces("text/xml")
	String fetchAllStates();

	/**
	 * The RestEasy WebService method for fetching all cities for the given
	 * state.Method Type:GET.
	 * 
	 * @param stateName
	 *            for which the cities to be fetched.
	 * @param searchKeyword
	 *            for which the cities to be fetched.
	 * @return the XML in response containing cities information.
	 */
	@GET
	@Path(ScanSeeURLPath.FETCHALLCITIES)
	@Produces("text/xml")
	String fetchAllCities(@QueryParam("State") String stateName,
			@QueryParam("SearchKeyword") String searchKeyword);

	/**
	 * The RestEasy WebService method which fetches states and cities for the
	 * given ZipCode.Method Type:GET.
	 * 
	 * @param zipCode
	 *            for which countries and states to be fetched.
	 * @return the XML in response containing states and cities information.
	 */
	@GET
	@Path(ScanSeeURLPath.FETCHALLSTATESANDCITIES)
	@Produces("text/xml")
	String fetchAllStatesAndCities(@QueryParam("PostalCode") String zipCode);

	/**
	 * The RestEasy WebService method to change User Password.Method Type:POST.
	 * 
	 * @param inputXML
	 *            in the request
	 * @return the XML in response based on Success or failure code.
	 */
	@POST
	@Path(ScanSeeURLPath.CHANGEPASSWORD)
	@Produces("text/xml")
	@Consumes("text/xml")
	String changePassword(String inputXML);

	/**
	 * The RestEasy WebService method which fetches states and cities for the
	 * given ZipCode.Method Type:GET.
	 * 
	 * @return the XML in response containing states and cities information.
	 */
	@GET
	@Path(ScanSeeURLPath.FETCHCATEGORIZEDCITIES)
	@Produces("text/xml")
	String fetchCategorizedCities();

	/**
	 * The RestEasy WebService method for saving user email id state.Method
	 * Type:GET.
	 * 
	 * @param userId
	 *            for which the email to be stored.
	 * @param emailId
	 *            to be stored for user.
	 * @return the XML in response based on Success or failure code.
	 */
	@GET
	@Path(ScanSeeURLPath.SAVEUSEREMID)
	@Produces("text/xml")
	String saveUserEmailId(@QueryParam("userid") Integer userId,
			@QueryParam("emaiid") String emailId);

	/**
	 * The RestEasy WebService method for saving user email id state.Method
	 * Type:GET.
	 * 
	 * @param userId
	 *            for which the email to be stored.
	 * @param emailId
	 *            to be stored for user.
	 * @return the XML in response based on Success or failure code.
	 */
	@GET
	@Path(ScanSeeURLPath.FETCHUNIVSTATES)
	@Produces("text/xml")
	String fetchUniversityStates();

	/**
	 * The RestEasy WebService method for saving user email id state.Method
	 * Type:GET.
	 * 
	 * @param userId
	 *            for which the email to be stored.
	 * @param emailId
	 *            to be stored for user.
	 * @return the XML in response based on Success or failure code.
	 */
	@GET
	@Path(ScanSeeURLPath.FETCHUNIVERSITIES)
	@Produces("text/xml;charset=UTF-8")
	String fetchUniversities(@QueryParam("stateab") String stateAbv);

	/**
	 * The RestEasy WebService method for fetching all postal code for the given
	 * state name and city name.Method Type:GET.
	 * 
	 * @param stateName
	 *            for which the postal codes to be fetched.
	 * @param cityName
	 *            for which the postal codes to be fetched.
	 * @return the XML in response containing cities information.
	 */
	@GET
	@Path(ScanSeeURLPath.GETPOSTALCODES)
	@Produces("text/xml")
	String fetchPostalCode(@QueryParam("state") String stateName,
			@QueryParam("city") String cityName);

	/**
	 * This is a RestEasy WebService Method get PushNotification Alert on/off
	 * and to get wish list radius
	 * 
	 * @param userId
	 *            as request
	 * @return XML containing push notification alert on/off and wish list
	 *         radius.
	 */
	@POST
	@Path("/getusersettings")
	@Produces("text/xml")
	@Consumes("text/xml")
	String getUserSettings(String xml);

	/**
	 * This is a RestEasy WebService Method for enabling or disabling
	 * PushNotification Alert and to set wish list radius
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */
	@POST
	@Path("/setusersettings")
	@Produces("text/xml")
	@Consumes("text/xml")
	String setUserSettings(String xml);
}