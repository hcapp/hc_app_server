package com.scansee.managesettings.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AuthenticateUser;
import com.scansee.common.pojos.Categories;
import com.scansee.common.pojos.Category;
import com.scansee.common.pojos.CategoryDetail;
import com.scansee.common.pojos.CountryCode;
import com.scansee.common.pojos.CreateOrUpdateUserPreference;
import com.scansee.common.pojos.MainCategoryDetail;
import com.scansee.common.pojos.RetailerDetail;
import com.scansee.common.pojos.RetailerInfoReq;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.SubCategoryDetail;
import com.scansee.common.pojos.UpdateUserInfo;
import com.scansee.common.pojos.UserPreference;
import com.scansee.common.pojos.UserRegistrationInfo;
import com.scansee.common.pojos.UserSettings;
import com.scansee.common.util.Utility;
import com.scansee.firstuse.query.FirstUseQueries;
import com.scansee.managesettings.query.ManageSettingQueries;

/**
 * This class for fetching and updating information from.
 * User,DemographyDetails,UserPrefences,UserRetailPrefence
 * 
 * @author shyamsundara_hm
 */
public class ManageSettingsDAOImpl implements ManageSettingsDAO
{

	/**
	 * To create logger instance.
	 */
	private static final Logger log = LoggerFactory.getLogger(ManageSettingsDAOImpl.class.getName());

	/**
	 * for Jdbc connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedue.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set ParameterizedBeanPropertyRowMapper to map Pojos.
	 */
	private SimpleJdbcTemplate simpleJdbcTemplate;

	/**
	 * To get the datasource from xml..
	 * 
	 * @param dataSource
	 *            of DataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * The DAO method for fetching user information from the database for the
	 * given userID.
	 * 
	 * @param userID
	 *            for which user information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return UserInformation object.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public UpdateUserInfo fetchUserInfo(int userID, boolean isCellFireRequest) throws ScanSeeException
	{
		final String methodName = "fetchUserInfo";
		log.info("In DAO...." + ApplicationConstants.METHODSTART + methodName);
		log.info("userID...." + userID);
		UpdateUserInfo updateUserInfo = null;
		List<UpdateUserInfo> updateUserInfoList = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserInfoDisplay");
			simpleJdbcCall.returningResultSet("UserInfo", new BeanPropertyRowMapper<UpdateUserInfo>(UpdateUserInfo.class));

			final MapSqlParameterSource fetchUserInfoParameters = new MapSqlParameterSource();
			fetchUserInfoParameters.addValue("UserId", userID);
			fetchUserInfoParameters.addValue("IsCellFireRequest", isCellFireRequest);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchUserInfoParameters);
			updateUserInfoList = (ArrayList<UpdateUserInfo>) resultFromProcedure.get("UserInfo");
			if (null != updateUserInfoList && !updateUserInfoList.isEmpty())
			{
				updateUserInfo = new UpdateUserInfo();
				updateUserInfo = updateUserInfoList.get(0);
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e);
		}
		if (null != updateUserInfo)
		{
			return updateUserInfo;
		}
		log.info(ApplicationConstants.METHODEND + methodName);

		return updateUserInfo;
	}

	/**
	 * The DAO method for saving user information in the database.
	 * 
	 * @param objUserRegInfo
	 *            - of user information
	 * @return Insertion status SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	@Override
	public String insertUserInfo(UserRegistrationInfo objUserRegInfo) throws ScanSeeException
	{
		final String methodName = "insertUserInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer result = 1;
		String response = null;
		Timestamp modifiedDate = null;

		if (objUserRegInfo.getChildren() == null)
		{
			objUserRegInfo.setChildren(0);
		}

		try
		{
			modifiedDate = Utility.getFormattedDate();
		}
		catch (ParseException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			response = ApplicationConstants.FAILURE;
		}

		log.info("userID...." + objUserRegInfo.getUserId());
		simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
		simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
		simpleJdbcCall.withProcedureName("usp_UserInfo");

		final MapSqlParameterSource regParam = new MapSqlParameterSource();
		regParam.addValue(ApplicationConstants.USERID, objUserRegInfo.getUserId());
		regParam.addValue("FirstName", objUserRegInfo.getFirstName());
		regParam.addValue("Lastname", objUserRegInfo.getLastName());
		//registrationParameters.addValue("Address1", userRegistrationInfo.getAddress1());
		//registrationParameters.addValue("Address2", userRegistrationInfo.getAddress2());
		//registrationParameters.addValue("Address3", userRegistrationInfo.getAddress3());
		//registrationParameters.addValue("Address4", userRegistrationInfo.getAddress4());
		//registrationParameters.addValue("City", userRegistrationInfo.getCity());
		//registrationParameters.addValue("State", userRegistrationInfo.getState());
		regParam.addValue("PostalCode", objUserRegInfo.getPostalCode());
		//registrationParameters.addValue("CountryID", userRegistrationInfo.getCountryID());
		regParam.addValue("Gender", objUserRegInfo.getGender());
		//registrationParameters.addValue("DOB", userRegistrationInfo.getDob());
		//registrationParameters.addValue("IncomeRangeID", userRegistrationInfo.getIncomeRangeID());
		//registrationParameters.addValue("EducationLevelID", userRegistrationInfo.getEducationLevelId());
		//registrationParameters.addValue("HomeOwner", userRegistrationInfo.getHomeOwner());
		//registrationParameters.addValue("Children", userRegistrationInfo.getChildren());
		//registrationParameters.addValue("MaritalStatus", userRegistrationInfo.getMaritalStatus());
		regParam.addValue("DateModified", modifiedDate);
		regParam.addValue("MobilePhone", objUserRegInfo.getMobileNumber());
		regParam.addValue("DeviceID", objUserRegInfo.getDeviceID());
		regParam.addValue("Email", objUserRegInfo.getEmail());
		regParam.addValue("UniversityIDs", objUserRegInfo.getUniversityIDs());

		try
		{
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(regParam);
			result = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);
			if (result == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + errorNum + "errorMsg is" + errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The DAO method returns the Settings information from the Database.
	 * 
	 * @param userId
	 *            - requested user.
	 * @return UserSettingPrefferenec object
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	@Override
	public CreateOrUpdateUserPreference fetchUserPreference(int userId) throws ScanSeeException
	{
		log.info("In fetchUserPreference method.");

		final String methodName = "fetchUserPreference";

		log.info(ApplicationConstants.METHODSTART + methodName);

		CreateOrUpdateUserPreference createOrUpdateUserPreference = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			createOrUpdateUserPreference = simpleJdbcTemplate.queryForObject(FirstUseQueries.FETCHUSERPREFERENCEQUERY,
					new BeanPropertyRowMapper<CreateOrUpdateUserPreference>(CreateOrUpdateUserPreference.class), userId);
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e);
		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return createOrUpdateUserPreference;

	}

	/**
	 * the DAO method for storing user settings preference Info.
	 * 
	 * @param userPreference
	 *            the userPreference in the request.
	 * @return the XML in response.
	 * @author shyamsundara_hm
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	@Override
	public String saveUserPreferenceDetails(UserPreference userPreference) throws ScanSeeException
	{
		final String methodName = "saveUserPreferenceDetails";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc = 0;
		Map<String, Object> resultFromProcedure = null;
		String response = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserPreferences");
			final MapSqlParameterSource userPreferenceParameters = new MapSqlParameterSource();

			userPreferenceParameters.addValue(ApplicationConstants.USERID, userPreference.getUserID());
			userPreferenceParameters.addValue("LocaleRadius", userPreference.getLocaleRadius());
			userPreferenceParameters.addValue("ScannerSilent", userPreference.getScannerSilent());
			userPreferenceParameters.addValue("DisplayCoupons", userPreference.getDisplayCoupons());
			userPreferenceParameters.addValue("DisplayRebates", userPreference.getDisplayRebates());
			userPreferenceParameters.addValue("DisplayLoyaltyRewards", userPreference.getDisplayLoyaltyRewards());
			userPreferenceParameters.addValue("SavingsActivated", userPreference.getSavingsActivated());
			userPreferenceParameters.addValue("SleepStatus", userPreference.getSleepStatus());
			userPreferenceParameters.addValue("DateModified", Utility.getFormattedDate());
			resultFromProcedure = simpleJdbcCall.execute(userPreferenceParameters);
			/**
			 * For getting response from stored procedure ,sp return Result as
			 * response success as 0 failure as 1 it will return list resultset
			 * so casting into map and getting Result param value
			 */
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in saveUserPreferenceDetails method ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
				response = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in saveUserPreferenceDetails", ApplicationConstants.DATAACCESSEXCEPTIONCODE, exception);
			throw new ScanSeeException(exception);
		}
		catch (ParseException exception)
		{
			log.error("ParseException occurred in saveUserPreferenceDetails", exception);
			throw new ScanSeeException(exception);
		}
		return response;
	}

	/**
	 * The DAO method for fetching user favourite categories.
	 * 
	 * @param userId
	 *            for which the information to be fetched.
	 * @param lowerLimit
	 *            used for pagination
	 * @return CategoryDetail object.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public CategoryDetail fetchUserFavCategories(Integer userId, Integer lowerLimit) throws ScanSeeException
	{

		final String methodName = "fetchUserFavCategories";

		log.info(ApplicationConstants.METHODSTART + methodName);

		List<SubCategoryDetail> subList = null;
		final List<MainCategoryDetail> mainList = new ArrayList<MainCategoryDetail>();
		MainCategoryDetail mainCategoryDetail = null;
		CategoryDetail categoryDetailResp = null;

		final Map<String, Object> resultFromProcedure;

		List<CategoryDetail> categoryDetailList = null;

		int preMainCategorId = 0;
		CategoryDetail categoryDetail = null;
		int currentCatId;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_PreferredCategoryDisplay");
			simpleJdbcCall.returningResultSet("fetchPreferredCategories", new BeanPropertyRowMapper<CategoryDetail>(CategoryDetail.class));

			final MapSqlParameterSource fetchPreferredRetailersParams = new MapSqlParameterSource();
			fetchPreferredRetailersParams.addValue("UserID", userId);
			// fetchPreferredRetailersParams.addValue("LowerLimit", lowerLimit);
			// fetchPreferredRetailersParams.addValue("ScreenName",
			// ApplicationConstants.USERFAVCATEGORIES);
			resultFromProcedure = simpleJdbcCall.execute(fetchPreferredRetailersParams);
			categoryDetailList = (List<CategoryDetail>) resultFromProcedure.get("fetchPreferredCategories");

		}
		catch (DataAccessException e)
		{

			log.error("fetchUserFavCategories" + e);

			throw new ScanSeeException(e.getMessage());

		}

		for (Iterator<CategoryDetail> iterator = categoryDetailList.iterator(); iterator.hasNext();)
		{
			categoryDetail = iterator.next();
			currentCatId = categoryDetail.getParentCategoryID();
			if (preMainCategorId == 0)
			{
				preMainCategorId = categoryDetail.getParentCategoryID();
				mainCategoryDetail = new MainCategoryDetail();
				mainCategoryDetail.setMainCategoryId(preMainCategorId);
				mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());
				subList = new ArrayList<SubCategoryDetail>();
			}
			if (currentCatId != preMainCategorId)
			{
				mainCategoryDetail.setSubCategoryDetailLst(subList);
				mainList.add(mainCategoryDetail);
				subList = new ArrayList<SubCategoryDetail>();
				final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
				subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
				subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
				subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
				// subCategoryDetail.setRowNum(categoryDetail.getRowNum());
				subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
				subCategoryDetail.setDisplayed(categoryDetail.getDisplayed());
				subList.add(subCategoryDetail);
				mainCategoryDetail = new MainCategoryDetail();
				mainCategoryDetail.setMainCategoryId(currentCatId);
				mainCategoryDetail.setMainCategoryName(categoryDetail.getParentCategoryName());

				preMainCategorId = categoryDetail.getParentCategoryID();
			}
			else
			{
				final SubCategoryDetail subCategoryDetail = new SubCategoryDetail();
				subCategoryDetail.setSubCategoryId(categoryDetail.getSubCategoryID());
				subCategoryDetail.setSubCategoryName(categoryDetail.getSubCategoryName());
				subCategoryDetail.setCategoryId(categoryDetail.getCategoryID());
				subCategoryDetail.setDisplayed(categoryDetail.getDisplayed());
				// subCategoryDetail.setRowNum(categoryDetail.getRowNum());
				subList.add(subCategoryDetail);
			}
		}

		if (null != mainList && !mainList.isEmpty())
		{
			categoryDetailResp = new CategoryDetail();
			categoryDetailResp.setMainCategoryDetaillst(mainList);
			/*
			 * final Boolean nextpage = (Boolean)
			 * resultFromProcedure.get("NxtPageFlag"); if (nextpage) {
			 * categoryDetailResp.setNextPage(1); } else {
			 * categoryDetailResp.setNextPage(0); }
			 */

		}

		log.info(ApplicationConstants.METHODEND + methodName);

		return categoryDetailResp;

	}

	/**
	 * The DAO method used to update the retailer prefereces.
	 * 
	 * @param userRetailPref
	 *            retailer details
	 * @return Insertion status SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	public String createOrUpdateRetailerPreferences(RetailersDetails userRetailPref) throws ScanSeeException
	{

		final String methodName = "createOrUpdateRetailerPreferences";

		log.info(ApplicationConstants.METHODSTART + methodName + "in DAO layer");
		int listSize;
		int fromProc = 0;

		final Date currentDate = new Date();

		final StringBuilder retailerIdBuilder = new StringBuilder();
		if (null != userRetailPref.getRetailerDetail() && !userRetailPref.getRetailerDetail().isEmpty())
		{
			int i = 0;
			listSize = userRetailPref.getRetailerDetail().size();
			for (RetailerDetail userRetailPreferences : userRetailPref.getRetailerDetail())
			{
				i++;

				retailerIdBuilder.append(userRetailPreferences.getRetailerId());
				if (i != listSize)
				{
					retailerIdBuilder.append(",");
				}
			}
		}
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_PreferredRetailers");
			SqlParameterSource updateRetailerPreferenceParameters;
			updateRetailerPreferenceParameters = new MapSqlParameterSource()

			.addValue("userID", userRetailPref.getUserId()).addValue("RetailID", retailerIdBuilder)
					.addValue("DateAdded", new java.sql.Timestamp(currentDate.getTime()));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(updateRetailerPreferenceParameters);
			fromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

		}
		catch (DataAccessException e)
		{

			log.error("Data Access exception" + e);

			throw new ScanSeeException(e.getMessage());

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		if (fromProc == 1)
		{
			return ApplicationConstants.FAILURE;
		}
		else
		{
			return ApplicationConstants.SUCCESS;
		}

	}

	/**
	 * The DAo method for saving user favourite categories in the database.
	 * 
	 * @param categoryDetail
	 *            categories details.
	 * @return Insertion status SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	@Override
	public String createOrUpdateFavCategories(Categories categoryDetail) throws ScanSeeException

	{
		final String methodName = "createOrUpdateFavCategories";

		log.info(ApplicationConstants.METHODSTART + methodName + "in DAO layer");

		int fromProc = 0;

		final Date currentDate = new Date();

		final StringBuilder categoryIdBuilder = new StringBuilder();
		if (null != categoryDetail.getCategoryInfo() && !categoryDetail.getCategoryInfo().isEmpty())
		{
			int i = 0;
			final int listSize = categoryDetail.getCategoryInfo().size();
			for (Category catDetail : categoryDetail.getCategoryInfo())
			{
				i++;

				categoryIdBuilder.append(catDetail.getCategoryId());
				if (i != listSize)
				{
					categoryIdBuilder.append(",");
				}
			}
		}
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_PreferredCategory");
			SqlParameterSource updateFavCategoriesParams;
			updateFavCategoriesParams = new MapSqlParameterSource()

			.addValue("userID", categoryDetail.getUserId()).addValue("Category", categoryIdBuilder)
					.addValue("Date", new java.sql.Timestamp(currentDate.getTime()));
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(updateFavCategoriesParams);

			fromProc = (Integer) resultFromProcedure.get(ApplicationConstants.STATUS);

		}
		catch (DataAccessException e)
		{

			log.error("Data Access exception" + e);

			throw new ScanSeeException(e.getMessage());

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		if (fromProc == 1)
		{
			return ApplicationConstants.FAILURE;
		}
		else
		{
			return ApplicationConstants.SUCCESS;
		}
	}

	/**
	 * The DAO method for fetching user preffered retailers.
	 * 
	 * @param retailerInfoReq
	 *            - retailer details.
	 * @return RetailersDetails object.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public RetailersDetails fetchPreferredRetailers(RetailerInfoReq retailerInfoReq) throws ScanSeeException
	{

		final String methodName = "fetchPreferredRetailers in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<RetailerDetail> retailerDetailLst = null;
		RetailersDetails retailersDetailsObj = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_PreferredRetailersDisplayPagination");
			simpleJdbcCall.returningResultSet("PreferedRetailers", new BeanPropertyRowMapper<RetailerDetail>(RetailerDetail.class));

			final MapSqlParameterSource fetchRetailerDetailsParameters = new MapSqlParameterSource();

			fetchRetailerDetailsParameters.addValue(ApplicationConstants.USERID, retailerInfoReq.getUserID());
			fetchRetailerDetailsParameters.addValue("LowerLimit", retailerInfoReq.getLastVisitedRecord());
			fetchRetailerDetailsParameters.addValue("ScreenName", ApplicationConstants.MANAGESETTINGSPREFERREDRETAILERS);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchRetailerDetailsParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					retailerDetailLst = (List<RetailerDetail>) resultFromProcedure.get("PreferedRetailers");
					if (null != retailerDetailLst && !retailerDetailLst.isEmpty())
					{
						retailersDetailsObj = new RetailersDetails();
						retailersDetailsObj.setRetailerDetail(retailerDetailLst);
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						if (nextpage != null)
						{
							if (nextpage)
							{
								retailersDetailsObj.setNextPage(1);
							}
							else
							{
								retailersDetailsObj.setNextPage(0);
							}
						}
					}
					else
					{
						log.info("No Prefered Retailers found ");
						return retailersDetailsObj;
					}
				}
				else
				{
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer.toString((Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER));
					log.error("Error occurred in usp_FetchRetailerList Store Procedure error number: {} and error message: {}", errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.ERROROCCURRED + methodName, e);
			throw new ScanSeeException(e.getMessage());

		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return retailersDetailsObj;
	}

	/**
	 * The DAO method to saves user password.
	 * 
	 * @param userID
	 *            For which password needs to be updated
	 * @param password
	 *            user new password
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	@Override
	public String changePassword(Integer userID, String password) throws ScanSeeException
	{
		final String methodName = "changePassword";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc;
		Map<String, Object> resultFromProcedure = null;
		String response = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_ChangePassword");
			final MapSqlParameterSource changePasswordParameters = new MapSqlParameterSource();
			changePasswordParameters.addValue("userid", userID);
			changePasswordParameters.addValue("password", password);
			resultFromProcedure = simpleJdbcCall.execute(changePasswordParameters);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in saveUserPreferenceDetails method ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in saveUserPreferenceDetails", ApplicationConstants.DATAACCESSEXCEPTIONCODE, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	// old code..
	/*
	 * String methodName = "fetchPreferredRetailers";
	 * log.info(ApplicationConstants.METHODSTART + methodName + "in DAO layer");
	 * RetailersDetails preferredRetailerDetails = null; final
	 * List<RetailerDetail> retailerDetails; try { simpleJdbcCall = new
	 * SimpleJdbcCall
	 * (jdbcTemplate).withProcedureName("usp_PreferredRetailersDisplay"
	 * ).returningResultSet( "fetchUserPrefferedRetailers",
	 * ParameterizedBeanPropertyRowMapper.newInstance(RetailerDetail.class));
	 * final SqlParameterSource preferredRetailersParams = new
	 * MapSqlParameterSource().addValue("UserID", userId); final Map<String,
	 * Object> resultFromProcedure =
	 * simpleJdbcCall.execute(preferredRetailersParams); retailerDetails =
	 * (List<RetailerDetail>)
	 * resultFromProcedure.get("fetchUserPrefferedRetailers"); } catch
	 * (DataAccessException e) {
	 * log.error("exception in fetchPreferredRetailers method of DAO" + e);
	 * throw new ScanSeeException(e.getMessage()); } preferredRetailerDetails =
	 * new RetailersDetails();
	 * preferredRetailerDetails.setRetailerDetail(retailerDetails);
	 * log.info(ApplicationConstants.METHODEND + methodName + "in DAO layer");
	 * return preferredRetailerDetails; }
	 */

	/**
	 * The DAO method used get All Countries.
	 * 
	 * @return List of Countries.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	public List<CountryCode> getAllCountries() throws ScanSeeException
	{
		final String methodName = "getAllCountries";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CountryCode> countryList = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			countryList = this.jdbcTemplate.query(ManageSettingQueries.FETCHCOUNTRYCODESQUERY,
					ParameterizedBeanPropertyRowMapper.newInstance(CountryCode.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return countryList;
	}

	/**
	 * The DAO method used get All States.
	 * 
	 * @return List of states.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	public List<CountryCode> getAllStates() throws ScanSeeException
	{
		final String methodName = "getAllStates";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CountryCode> stateList = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			stateList = this.jdbcTemplate.query(ManageSettingQueries.FETCHSTATEQUERY,
					ParameterizedBeanPropertyRowMapper.newInstance(CountryCode.class));
		}
		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return stateList;
	}

	/**
	 * this method used get All cities.
	 * 
	 * @param stateName
	 *            -For which cities to be fetched.
	 * @param searchKeyword
	 *            - For which cities to be fetched.
	 * @return List of cities.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	public List<CountryCode> getAllCities(String stateName, String searchKeyword) throws ScanSeeException
	{
		final String methodName = "getAllCities";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<CountryCode> cityList = null;

		try
		{
			if (null != stateName && null == searchKeyword)
			{
				cityList = this.jdbcTemplate.query(ManageSettingQueries.FETCHCITYQUERY, new Object[] { stateName }, new RowMapper<CountryCode>() {
					public CountryCode mapRow(ResultSet rs, int rowNum) throws SQLException
					{
						final CountryCode countryCode = new CountryCode();
						countryCode.setCityName(rs.getString("cityName"));
						return countryCode;
					}

				});
			}
			else if (null == stateName && null == searchKeyword)
			{
				// if statename is null,fetch population centers for Hot Deals.
				cityList = this.jdbcTemplate.query(ManageSettingQueries.POPULATIONCENTER, new RowMapper<CountryCode>() {
					public CountryCode mapRow(ResultSet rs, int rowNum) throws SQLException
					{
						final CountryCode countryCode = new CountryCode();
						countryCode.setCityName(rs.getString("cityName"));
						return countryCode;
					}

				});
			}
			else if (null == stateName && null != searchKeyword)
			{

				// This is excecuted when user search for city from HotDeal
				// population center screen
				cityList = this.jdbcTemplate.query(ManageSettingQueries.POPULATIONCENTERSEARCHCITY, new Object[] { "%" + searchKeyword + "%" },
						new RowMapper<CountryCode>() {
							public CountryCode mapRow(ResultSet rs, int rowNum) throws SQLException
							{
								final CountryCode countryCode = new CountryCode();
								countryCode.setCityName(rs.getString("cityName"));
								return countryCode;
							}

						});
			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return cityList;
	}

	/**
	 * this method used get All cities and states.
	 * 
	 * @param zipCode
	 *            for which states and cities to be fetched.
	 * @return List of cities and states.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	public CountryCode fetchAllCitieAndStates(String zipCode) throws ScanSeeException
	{
		final String methodName = "getAllCities";
		log.info(ApplicationConstants.METHODSTART + methodName);
		CountryCode cityAndStates = null;
		try
		{

			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			cityAndStates = simpleJdbcTemplate.queryForObject(ManageSettingQueries.FETCHCITYANDSTATESQUERY, new BeanPropertyRowMapper<CountryCode>(
					CountryCode.class), zipCode);
		}

		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return cityAndStates;
	}

	/**
	 * this method used fetching categorised cities .
	 * 
	 * @return List of cities and states.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	@Override
	public ArrayList<CountryCode> fetchCategirizedCities() throws ScanSeeException
	{
		final String methodName = "getAllStates";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<CountryCode> stateCityList = null;
		try
		{
			simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
			stateCityList = (ArrayList<CountryCode>) this.jdbcTemplate.query(ManageSettingQueries.POPULATIONCATEGORIZEDCITIES,
					ParameterizedBeanPropertyRowMapper.newInstance(CountryCode.class));
		}

		catch (EmptyResultDataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			return null;
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return stateCityList;
	}

	@Override
	public String saveUserEmailId(Integer userID, String emailId) throws ScanSeeException
	{
		final String methodName = "saveUserEmailId";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer fromProc;
		Map<String, Object> resultFromProcedure = null;
		String response = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserEmailUpdation");
			final MapSqlParameterSource changePasswordParameters = new MapSqlParameterSource();
			changePasswordParameters.addValue("UserID", userID);
			changePasswordParameters.addValue("Email", emailId);
			resultFromProcedure = simpleJdbcCall.execute(changePasswordParameters);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in saveUserEmailId method ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in saveUserEmailId", ApplicationConstants.DATAACCESSEXCEPTIONCODE, exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<CountryCode> fetchUnivStates() throws ScanSeeException
	{
		final String methodName = "fetchUnivStates";
		log.info("In DAO...." + ApplicationConstants.METHODSTART + methodName);
		log.info("userID....");

		ArrayList<CountryCode> univstateslst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveState");
			simpleJdbcCall.returningResultSet("univstates", new BeanPropertyRowMapper<CountryCode>(CountryCode.class));

			final MapSqlParameterSource fetchUserInfoParameters = new MapSqlParameterSource();

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchUserInfoParameters);
			univstateslst = (ArrayList<CountryCode>) resultFromProcedure.get("univstates");

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e);
		}
		return univstateslst;
	}

	@Override
	public ArrayList<CountryCode> fetchUniversities(String stateAbv) throws ScanSeeException
	{
		final String methodName = "fetchUniversities";
		log.info("In DAO...." + ApplicationConstants.METHODSTART + methodName);
		log.info("userID....");

		ArrayList<CountryCode> univstateslst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_WebRetrieveUniversity");
			simpleJdbcCall.returningResultSet("universities", new BeanPropertyRowMapper<CountryCode>(CountryCode.class));

			final MapSqlParameterSource fetchUserInfoParameters = new MapSqlParameterSource();
			fetchUserInfoParameters.addValue("State", stateAbv);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchUserInfoParameters);
			univstateslst = (ArrayList<CountryCode>) resultFromProcedure.get("universities");

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e);
		}
		return univstateslst;
	}

	@Override
	public ArrayList<String> fetchPostalCode(String stateName, String cityName) throws ScanSeeException
	{
		final String methodName = "fetchPostalCode";
		log.info("In DAO...." + ApplicationConstants.METHODSTART + methodName);
		log.info("userID....");

		ArrayList<CountryCode> codelst = null;
		ArrayList<String> postalcodeslist = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserInformationPostalCode");
			simpleJdbcCall.returningResultSet("postalcodes", new BeanPropertyRowMapper<CountryCode>(CountryCode.class));

			final MapSqlParameterSource fetchUserInfoParameters = new MapSqlParameterSource();
			fetchUserInfoParameters.addValue("State", stateName);
			fetchUserInfoParameters.addValue("City", cityName);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchUserInfoParameters);
			codelst = (ArrayList<CountryCode>) resultFromProcedure.get("postalcodes");

			if (!codelst.isEmpty() && codelst != null)
			{
				postalcodeslist = new ArrayList<String>();

				for (int i = 0; i < codelst.size(); i++)
				{
					postalcodeslist.add(codelst.get(i).getPostalCode());
				}
			}

		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e);
		}
		return postalcodeslist;
	}

	/**
	 * The DAO method to get push notification alert for a specidfied user from
	 * database or to get wish list radius
	 * 
	 * @param userId
	 * @return userSettings object
	 * @throws ScanSeeException
	 */
	public ArrayList<UserSettings> getUserSettings(AuthenticateUser objAuthenticateUser) throws ScanSeeException
	{
		final String methodName = "getUserSettings";
		log.info(ApplicationConstants.METHODSTART + methodName);
		ArrayList<UserSettings> userSettingsList = null;

		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FetchUserPreferences");
			simpleJdbcCall.returningResultSet("usersettings", new BeanPropertyRowMapper<UserSettings>(UserSettings.class));

			final MapSqlParameterSource fetchUserSettings = new MapSqlParameterSource();
			fetchUserSettings.addValue(ApplicationConstants.USERID, objAuthenticateUser.getUserId());
			fetchUserSettings.addValue("DeviceID", objAuthenticateUser.getDeviceID());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchUserSettings);
			userSettingsList = (ArrayList<UserSettings>) resultFromProcedure.get("usersettings");
		}
		catch (DataAccessException dataAccessException)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + " " + methodName + " " + dataAccessException);
			throw new ScanSeeException(dataAccessException);
		}
		return userSettingsList;
	}

	/**
	 * The DAO method to update database to enable or disable push notification
	 * alert for a specified user or to update wish list radius
	 * 
	 * @param userSettings
	 *            instance of UserSettings pojo class.
	 * @return success or failure.
	 * @throws ScanSeeException
	 */
	public String setUserSettings(UserSettings userSettings) throws ScanSeeException
	{
		final String methodName = "setUserSettings";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;

		simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
		simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
		simpleJdbcCall.withProcedureName("usp_UpdateUserPreferences");

		final MapSqlParameterSource userSettingsParameters = new MapSqlParameterSource();
		userSettingsParameters.addValue(ApplicationConstants.USERID, userSettings.getUserId());
		userSettingsParameters.addValue("PushNotify", userSettings.getPushNotify());
		userSettingsParameters.addValue("WishListDefaultRadius", userSettings.getLocaleRadius());
		userSettingsParameters.addValue("DeviceID", userSettings.getDeviceId());

		try
		{
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(userSettingsParameters);
			Integer result = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
			if (result == null)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED + errorNum + "errorMsg is" + errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		}
		catch (DataAccessException e)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName + e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

}
