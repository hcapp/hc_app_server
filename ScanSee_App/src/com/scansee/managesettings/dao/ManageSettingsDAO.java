package com.scansee.managesettings.dao;

import java.util.ArrayList;
import java.util.List;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AuthenticateUser;
import com.scansee.common.pojos.Categories;
import com.scansee.common.pojos.CategoryDetail;
import com.scansee.common.pojos.CountryCode;
import com.scansee.common.pojos.CreateOrUpdateUserPreference;
import com.scansee.common.pojos.RetailerInfoReq;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.UpdateUserInfo;
import com.scansee.common.pojos.UserPreference;
import com.scansee.common.pojos.UserRegistrationInfo;
import com.scansee.common.pojos.UserSettings;

/**
 * The Interface for Manage Settings DAO. ImplementedBy
 * {@link ManageSettingsDAOImpl}
 * 
 * @author dileepa_cc
 */
public interface ManageSettingsDAO
{

	/**
	 * The DAO method for fetching user information from the database for the
	 * given userID.
	 * 
	 * @param userID
	 *            - for which user information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return UserInformation object.
	 */
	UpdateUserInfo fetchUserInfo(int userID, boolean isCellFireRequest) throws ScanSeeException;

	/**
	 * The DAO method for saving user information in the database.
	 * 
	 * @param userRegistrationInfo
	 *            - of user information
	 * @return Insertion status SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String insertUserInfo(UserRegistrationInfo userRegistrationInfo) throws ScanSeeException;

	/**
	 * The DAO method returns the Settings information from the Database.
	 * 
	 * @param userID
	 *            - requested user.
	 * @return UserSettingPrefferenec object
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	CreateOrUpdateUserPreference fetchUserPreference(int userID) throws ScanSeeException;

	/**
	 * the DAO method for storing user settings preference Info.
	 * 
	 * @param userPreference
	 *            the userPreference in the request.
	 * @return the XML in response.
	 * @author shyamsundara_hm
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String saveUserPreferenceDetails(UserPreference userPreference) throws ScanSeeException;

	/**
	 * The DAO method for fetching user preffered retailers.
	 * 
	 * @param retailerInfoReq
	 *            - retailer details.
	 * @return RetailersDetails object.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	RetailersDetails fetchPreferredRetailers(RetailerInfoReq retailerInfoReq) throws ScanSeeException;

	/**
	 * The DAO method used to update the retailer prefereces.
	 * 
	 * @param retailersDetails
	 *            -retailer details
	 * @return Insertion status SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	String createOrUpdateRetailerPreferences(RetailersDetails retailersDetails) throws ScanSeeException;

	/**
	 * The DAO method for fetching user favourite categories.
	 * 
	 * @param userId
	 *            - for which the information to be fetched.
	 * @param lowerLimit
	 *            used for pagination
	 * @return CategoryDetail object.
	 * @throws ScanSeeException
	 *             - If any exception occures ScanSeeException will be thrown.
	 */
	CategoryDetail fetchUserFavCategories(Integer userId, Integer lowerLimit) throws ScanSeeException;

	/**
	 * The DAo method for saving user favourite categories in the database.
	 * 
	 * @param categoryDetaiil
	 *            - categories details.
	 * @return Insertion status SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String createOrUpdateFavCategories(Categories categoryDetaiil) throws ScanSeeException;

	/**
	 * The DAO method used get All Countries.
	 * 
	 * @return List of Countries.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	List<CountryCode> getAllCountries() throws ScanSeeException;

	/**
	 * The DAO method used get All States.
	 * 
	 * @return List of states.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	List<CountryCode> getAllStates() throws ScanSeeException;

	/**
	 * this method used get All cities.
	 * 
	 * @param stateName
	 *            -for which cities to be fetched.
	 * @param searchKeyword
	 *            - for which cities to be fetched.
	 * @return List of cities.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	List<CountryCode> getAllCities(String stateName, String searchKeyword) throws ScanSeeException;

	/**
	 * this method used get All cities and states.
	 * 
	 * @param zipCode
	 *            - for which states and cities to be fetched.
	 * @return List of cities and states.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	CountryCode fetchAllCitieAndStates(String zipCode) throws ScanSeeException;

	/**
	 * The DAO method to saves user password.
	 * 
	 * @param userID
	 *            -For which password needs to be updated
	 * @param password
	 *            -user new password
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String changePassword(Integer userID, String password) throws ScanSeeException;

	/**
	 * this method used get All cities and states.
	 * 
	 * @return List of cities and states.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */

	ArrayList<CountryCode> fetchCategirizedCities() throws ScanSeeException;
	
	/**
	 * The DAO method for fetching user information from the database for the
	 * given userID.
	 * 
	 * @param userID
	 *            - for which user information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return UserInformation object.
	 */
	String saveUserEmailId(Integer userID,String emailId) throws ScanSeeException;
	
	/**
	 * The DAO method for fetching user information from the database for the
	 * given userID.
	 * 
	 * @param userID
	 *            - for which user information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return UserInformation object.
	 */
	ArrayList<CountryCode> fetchUnivStates() throws ScanSeeException;
	
	/**
	 * The DAO method for fetching user information from the database for the
	 * given userID.
	 * 
	 * @param userID
	 *            - for which user information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return UserInformation object.
	 */
	ArrayList<CountryCode> fetchUniversities(String stateAbv)throws ScanSeeException;
	/**
	 * The DAO method for fetching postal codes  from the database for the
	 * given state and city.
	 * 
	 * @param stateName
	 *            - for which postal code information need to be fetched.
	 * @param cityName
	 *            - for which postal code information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return list of postal codes.
	 */
	
	ArrayList<String> fetchPostalCode(String stateName, String cityName) throws ScanSeeException;
	
	
	

	// ******************UNUSED
	// METHODS**************************************************************************************************************//

	/**
	 * This DAO for fetching user preference Info.
	 * 
	 * @param userId
	 *            - the userId in the request.
	 * @return the XML in response.
	 * @author shyamsundara_hm
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	/*
	 * UserPreference getUserPreferenceInfo(Integer userId) throws
	 * ScanSeeException;
	 */

	/**
	 * The DAO method updates the Settings information to the Database.
	 * 
	 * @param userPrefer
	 *            - preference values.
	 * @return Insertion status SUCCESS or FAILURE.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	// String updateUserPreference(CreateOrUpdateUserPreference userPrefer)
	// throws ScanSeeException;

	/**
	 * The DAO method updated the retailer preference to Database.
	 * 
	 * @param userRetailPrefer
	 *            - retailer info.
	 * @return The XML with Success or Error code.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	// String updateRetailerPreference(CreateOrUpdateUserRetailPreferences
	// userRetailPrefer) throws ScanSeeException;

	/**
	 * This method updates the user information to daabase.
	 * 
	 * @param updateUserInfoService
	 *            -user information.
	 * @return The XML with Success or Error code.
	 * @throws ScanSeeException
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	/*
	 * String updateUser(UpdateUserInfo updateUserInfoService) throws
	 * ScanSeeException;
	 */
	
	/**
	 * The DAO interface to get push notification alert for a specidfied user from database
	 * or to get wish list radius
	 * @param userId
	 * 				
	 * @return userSettings object
	 * @throws ScanSeeException
	 */
	ArrayList<UserSettings> getUserSettings(AuthenticateUser objAuthenticateUser) throws ScanSeeException;
	
	/**
	 * The DAO interface to update database to enable or diable push notification alert for a specidfied user
	 * or to update wish list radius
	 * @param userSettings
	 * 				instance of UserSettings pojo class.
	 * @return success or failure.
	 * @throws ScanSeeException
	 */
	String setUserSettings(UserSettings userSettings) throws ScanSeeException;
	

}
