package com.scansee.managesettings.service;

import static com.scansee.common.util.Utility.isEmptyOrNullString;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.helper.BaseDAO;
import com.scansee.common.helper.XstreamParserHelper;
import com.scansee.common.pojos.AuthenticateUser;
import com.scansee.common.pojos.Categories;
import com.scansee.common.pojos.CategoryDetail;
import com.scansee.common.pojos.CellFireRequest;
import com.scansee.common.pojos.CountryCode;
import com.scansee.common.pojos.CountryCodes;
import com.scansee.common.pojos.CountryDetails;
import com.scansee.common.pojos.CreateOrUpdateUserPreference;
import com.scansee.common.pojos.EducationalLevelDetails;
import com.scansee.common.pojos.EducationalLevelInfo;
import com.scansee.common.pojos.IncomeRange;
import com.scansee.common.pojos.IncomeRanges;
import com.scansee.common.pojos.MaritalStatusDetails;
import com.scansee.common.pojos.MaritalStatusInfo;
import com.scansee.common.pojos.RetailerInfoReq;
import com.scansee.common.pojos.RetailersDetails;
import com.scansee.common.pojos.UpdateUserInfo;
import com.scansee.common.pojos.UserInfo;
import com.scansee.common.pojos.UserPreference;
import com.scansee.common.pojos.UserRegistrationInfo;
import com.scansee.common.pojos.UserSettings;
import com.scansee.common.util.EncryptDecryptPwd;
import com.scansee.common.util.Utility;
import com.scansee.managesettings.dao.ManageSettingsDAO;

/**
 * The Manage Setting service implementation class implements the
 * ManageSettingsService Interface. The methods of this class are called from
 * the ManageSettings Controller Layer. The methods contain call to the
 * ManageSettings DAO layer.
 * 
 * @author dileepa_cc
 */

public class ManageSettingsServiceImpl implements ManageSettingsService
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(ManageSettingsServiceImpl.class);

	/**
	 * Instance variable for Manage Settings DAO instance.
	 */
	private ManageSettingsDAO manageSettingsDao;

	// private Object useRetailersInfo;
	/**
	 * Instance variable for BaseDAO instance.
	 */
	private BaseDAO baseDao;

	/**
	 * sets the ManageSettings DAO.
	 * 
	 * @param manageSettingsDao
	 *            The instance for ManageSettingsDAO
	 */

	public void setManageSettingsDao(ManageSettingsDAO manageSettingsDao)
	{
		this.manageSettingsDao = manageSettingsDao;
	}

	/**
	 * sets the BaseDAO DAO.
	 * 
	 * @param baseDao
	 *            The instance for BaseDAO
	 */
	public void setBaseDao(BaseDAO baseDao)
	{
		this.baseDao = baseDao;
	}

	/**
	 * The service method for fetching user information. This method validates
	 * the userId, if it is valid it will call the DAO method.
	 * 
	 * @param userID
	 *            for which user information need to be fetched.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String fetchUserInfo(Integer userID, boolean isCellFireRequest) throws ScanSeeException
	{
		final String methodName = "fetchUserInfo";
		LOG.info("In Service " + ApplicationConstants.METHODSTART + methodName);
		String response = null;
		UpdateUserInfo updateUserInfo = null;
		new UpdateUserInfo();
		List<IncomeRange> incomeRange = null;
		List<CountryCode> countryCode = null;
		List<EducationalLevelInfo> eduLevelInfo = null;
		List<MaritalStatusInfo> maritalStatusInfo = null;
		if (userID == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{
			UserInfo userInfo = null;
			if (!isCellFireRequest)
			{

//				incomeRange = baseDao.getIncomeRanges();
//				countryCode = baseDao.getCountryCodes();
//				eduLevelInfo = baseDao.getEducationalLevel();
//				maritalStatusInfo = baseDao.getMaritalStatusInfo();
				updateUserInfo = manageSettingsDao.fetchUserInfo(userID, isCellFireRequest);
				userInfo = new UserInfo();
				if (null != updateUserInfo)
				{
					
					userInfo.setUpdateUserInfo(updateUserInfo);
					
				}
				if (null != incomeRange)
				{
					final IncomeRanges iranges = new IncomeRanges();
					iranges.setIncomeRange(incomeRange);
					userInfo.setIncomeRanges(iranges);
				}
				if (null != countryCode)
				{
					final CountryCodes countryCodes = new CountryCodes();
					countryCodes.setCountryCode(countryCode);
					userInfo.setCountryCodes(countryCodes);
				}
				if (null != userInfo)
				{
					response = XstreamParserHelper.produceXMlFromObject(userInfo);
				}
				if (null != eduLevelInfo)
				{
					final EducationalLevelDetails eduLevelDetails = new EducationalLevelDetails();
					eduLevelDetails.setEducationalInfo(eduLevelInfo);
					userInfo.setEducationalLevelDetail(eduLevelDetails);
				}
				if (null != maritalStatusInfo)
				{
					final MaritalStatusDetails maritalStatusDetails = new MaritalStatusDetails();
					maritalStatusDetails.setMaritalInfo(maritalStatusInfo);
					userInfo.setMaritalStatusDetail(maritalStatusDetails);
				}
				if (null != userInfo)
				{
					response = XstreamParserHelper.produceXMlFromObject(userInfo);
				}

			}
			else
			{

				updateUserInfo = manageSettingsDao.fetchUserInfo(userID, isCellFireRequest);
				userInfo = new UserInfo();
				
				userInfo.setUpdateUserInfo(updateUserInfo);
				response = XstreamParserHelper.produceXMlFromObject(userInfo);
			}

			LOG.info("In managesettings Service" + ApplicationConstants.METHODEND + methodName);
		}

		return response;
	}

	/**
	 * The Service method for Saving User Information. This method checks for
	 * mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method.
	 * 
	 * @param xml
	 *            containing user information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String insertUserInfo(String xml) throws ScanSeeException
	{
		final String methodName = "insertUserInfo";
		LOG.info("In Service " + ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserRegistrationInfo userRegistrationInfo = (UserRegistrationInfo) streamHelper.parseXmlToObject(xml);
		// final String validated =
		// ManageSettingHelper.validateUserValues(userRegistrationInfo);

		/*
		 * if (validated != null) { response = validated; }
		 */
		if (userRegistrationInfo.getUserId() == null)
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.UserID);

		}
		else if (userRegistrationInfo.getEmail() != null)
		{
			boolean isValid = Utility.validateEmailId(userRegistrationInfo.getEmail());
			if (!isValid)
			{
				return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDEMAILADDRESSTEXT);
			}

		}

		response = manageSettingsDao.insertUserInfo(userRegistrationInfo);
		if (response.equals(ApplicationConstants.SUCCESS))
		{
			response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SAVEUSERINFORMATION);
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info("In Service..." + ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * The service method for fetching user setting preferences information.
	 * This method validates the userId, if it is valid it will call the DAO
	 * method.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String fetchUserPreferences(Integer userId) throws ScanSeeException
	{
		final String methodName = "fetchUserPreferences";
		LOG.info("Inside fetchUserPreferences method");
		String response = null;
		CreateOrUpdateUserPreference userPref;
		if (userId == null)
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);
		}
		else
		{
			userPref = manageSettingsDao.fetchUserPreference(userId);
			if (null != userPref)
			{
				response = XstreamParserHelper.produceXMlFromObject(userPref);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
			LOG.info("In Service..." + ApplicationConstants.METHODEND + methodName);
		}
		return response;
	}

	/**
	 * The Service method for Saving User setting preffereces information. This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param inputXml
	 *            Contains user preferred categories information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String saveUserPreference(String inputXml) throws ScanSeeException
	{
		final String methodName = "saveUserPreference";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserPreference userPreference = (UserPreference) streamHelper.parseXmlToObject(inputXml);
		if (userPreference.getUserID() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{

			if (null != userPreference)
			{
				response = manageSettingsDao.saveUserPreferenceDetails(userPreference);
			}
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.USERPREFERENCEDATA);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);

			}
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}

		return response;
	}

	/**
	 * The Service method for Saving User favourite categories Information. This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param inputXML
	 *            Contains user favourite categories information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@Override
	public String setUserFavCategories(String inputXML) throws ScanSeeException
	{
		final String methodName = "setUserFavCategories";

		LOG.info(ApplicationConstants.METHODSTART + methodName + "in managesettings Service layer");
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final Categories categories = (Categories) streamHelper.parseXmlToObject(inputXML);

		if (categories.getUserId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOUSERID);

		}
		else
		{
			response = manageSettingsDao.createOrUpdateFavCategories(categories);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SETUSERFAVCATEGORIES);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
			LOG.info(ApplicationConstants.METHODEND + methodName + "In manage settings Service layer");
		}
		return response;
	}

	/**
	 * The Service method for Saving User preferred retailers information. This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param inputXML
	 *            Contains User preferred retailers information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@Override
	public String setPreferredRetailers(String inputXML) throws ScanSeeException
	{
		final String methodName = "setPreferredRetailers";

		LOG.info(ApplicationConstants.METHODSTART + methodName + "In manage settings Service layer");
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final RetailersDetails retailersDetails = (RetailersDetails) streamHelper.parseXmlToObject(inputXML);
		if (retailersDetails.getUserId() == null || (retailersDetails.getRetailerDetail()).isEmpty() == true)
		{

			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			response = manageSettingsDao.createOrUpdateRetailerPreferences(retailersDetails);
			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SETPREFFEREDRETAILERS);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
			LOG.info(ApplicationConstants.METHODEND + methodName + "In Service layer");
		}
		return response;
	}

	/**
	 * The service method for fetching user preferred retailers information.
	 * This method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param xml
	 *            Contains the request for user preffered retailers information.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	public String getPreferredRetailers(String xml) throws ScanSeeException
	{

		final String methodName = "getPreferredRetailers";

		LOG.info(ApplicationConstants.METHODSTART + methodName + "In settings Service layer");

		String response = null;

		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final RetailerInfoReq retailerInfoReq = (RetailerInfoReq) streamHelper.parseXmlToObject(xml);

		if (retailerInfoReq.getUserID() == null || retailerInfoReq.getLastVisitedRecord() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			final RetailersDetails retailersDetailsResponse = manageSettingsDao.fetchPreferredRetailers(retailerInfoReq);
			if (null != retailersDetailsResponse)
			{
				response = XstreamParserHelper.produceXMlFromObject(retailersDetailsResponse);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORETAILERFOUNDTEXT);
			}

			LOG.info(ApplicationConstants.METHODEND + methodName + "In Service layer");
		}
		return response;

	}

	/**
	 * The service method for fetching user favorite categories information.
	 * This method validates the userId, if it is valid it will call the DAO
	 * method.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.
	 * @param lowerlimit
	 *            for pagination
	 * @return XML containing user favourite categories information in the
	 *         response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String getUserFavCategories(Integer userId, Integer lowerlimit) throws ScanSeeException
	{

		final String methodName = "getUserFavCategories";

		LOG.info(ApplicationConstants.METHODSTART + methodName + "in settings Service layer");
		String response = null;
		CategoryDetail categoryDetailresponse = null;
		if (userId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{

			if (lowerlimit == null)
			{
				lowerlimit = 0;
			}
			categoryDetailresponse = manageSettingsDao.fetchUserFavCategories(userId, lowerlimit);

			if (null != categoryDetailresponse)
			{
				response = XstreamParserHelper.produceXMlFromObject(categoryDetailresponse);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
			LOG.info(ApplicationConstants.METHODEND + methodName + "in settings Service layer");
		}
		return response;

	}

	/**
	 * The service method for all the countries.
	 * 
	 * @return the XML containing Countries information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	public String fetchAllCountries() throws ScanSeeException
	{
		final String methodName = "fetchAllCountries";
		LOG.info("In mangesettings Service" + ApplicationConstants.METHODSTART + methodName);
		List<CountryCode> countryList = null;
		CountryDetails countryDetails = null;
		String response = null;
		countryList = manageSettingsDao.getAllCountries();
		if (null != countryList && !countryList.isEmpty())
		{
			countryDetails = new CountryDetails();
			countryDetails.setCountryInfo(countryList);
			response = XstreamParserHelper.produceXMlFromObject(countryDetails);
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method for all the States.
	 * 
	 * @return the XML containing states information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	public String fetchAllStates() throws ScanSeeException
	{
		final String methodName = "fetchAllStates";
		LOG.info("In manage settings Service...." + ApplicationConstants.METHODSTART + methodName);
		List<CountryCode> stateList = null;
		CountryDetails statesDetails = null;
		String response = null;
		stateList = manageSettingsDao.getAllStates();

		if (null != stateList && !stateList.isEmpty())
		{
			statesDetails = new CountryDetails();
			statesDetails.setStateInfo(stateList);
			response = XstreamParserHelper.produceXMlFromObject(statesDetails);
			response = response.replaceAll("<CountryCode>", "<CountryInfo>");
			response = response.replaceAll("</CountryCode>", "</CountryInfo>");
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}

		LOG.info("In manage settings Service" + ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method for all the cities for the given statename and search
	 * keyword.
	 * 
	 * @param stateName
	 *            -for which cities to be fetched
	 * @param searchKeyword
	 *            -for which cities to be fetched
	 * @return the XML containing cities information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	public String fetchAllCities(String stateName, String searchKeyword) throws ScanSeeException
	{
		final String methodName = "fetchAllCities";
		LOG.info("In Service layer:" + ApplicationConstants.METHODSTART + methodName);
		List<CountryCode> citiList = null;
		CountryDetails citiDetails = null;
		String response = null;

		citiList = manageSettingsDao.getAllCities(stateName, searchKeyword);
		if (null != citiList && !citiList.isEmpty())
		{
			if (null != stateName)
			{
				citiDetails = new CountryDetails();
				citiDetails.setCityInfo(citiList);
				response = XstreamParserHelper.produceXMlFromObject(citiDetails);
				response = response.replaceAll("<CountryCode>", "<CountryInfo>");
				response = response.replaceAll("</CountryCode>", "</CountryInfo>");
			}
			else
			{
				citiDetails = new CountryDetails();
				citiDetails.setCityInfo(citiList);
				response = XstreamParserHelper.produceXMlFromObject(citiDetails);
				response = response.replaceAll("<CountryCode>", "<CityInfo>");
				response = response.replaceAll("</CountryCode>", "</CityInfo>");

				response = response.replaceAll("<CountryDetails>", "<CityDetails>");
				response = response.replaceAll("</CountryDetails>", "</CityDetails>");
			}

		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}

		LOG.info("In Service :" + ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * The service method for all the states and cities for the given zipCode.
	 * 
	 * @param zipCode
	 *            for which states and cities to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return cities and states
	 */
	public String fetchAllCitieAndStates(String zipCode) throws ScanSeeException
	{
		final String methodName = "fetchAllCitieAndStates";
		LOG.info("In Service...." + ApplicationConstants.METHODSTART + methodName);
		CountryCode cityAndStates = null;
		String response = null;
		if (isEmptyOrNullString(zipCode))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			cityAndStates = manageSettingsDao.fetchAllCitieAndStates(zipCode);
			if (null != cityAndStates)
			{

				response = XstreamParserHelper.produceXMlFromObject(cityAndStates);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}

			LOG.info("In Service" + ApplicationConstants.METHODEND + methodName);
		}
		return response;
	}

	/**
	 * The service Method takes the user new password and encrypt it. Calls the
	 * dao method with encrypted password and userID.
	 * 
	 * @param inputXML
	 *            in the User Request.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String changePassword(String inputXML) throws ScanSeeException
	{
		final String methodName = "changePassword";
		LOG.info(ApplicationConstants.METHODSTART + methodName + "in Service layer");
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserRegistrationInfo userRegistrationInfo = (UserRegistrationInfo) streamHelper.parseXmlToObject(inputXML);
		EncryptDecryptPwd encryptDecryptPwd;
		String encryptedpwd = null;
		try
		{
			encryptDecryptPwd = new EncryptDecryptPwd();
			encryptedpwd = encryptDecryptPwd.encrypt(userRegistrationInfo.getPassword());
			if (userRegistrationInfo.getUserId() == null || isEmptyOrNullString(encryptedpwd))
			{
				return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			}
		}
		catch (NoSuchAlgorithmException e)
		{
			LOG.error("Exception in Encryption of password..", e);
			throw new ScanSeeException(e.getMessage());
		}
		catch (NoSuchPaddingException e)
		{
			LOG.error("Exception in Encryption of user password..", e);
			throw new ScanSeeException(e.getMessage());
		}
		catch (InvalidKeyException e)
		{
			LOG.error("user password Encryption Exception    ", e);
			throw new ScanSeeException(e.getMessage());
		}
		catch (InvalidAlgorithmParameterException e)
		{
			LOG.error("Exception while Encryption  ", e);
			throw new ScanSeeException(e.getMessage());
		}
		catch (InvalidKeySpecException e)
		{
			LOG.error("Exception during Encryption ", e);
			throw new ScanSeeException(e.getMessage());
		}
		catch (IllegalBlockSizeException e)
		{
			LOG.error("Exception in user password Encryption ", e);
			throw new ScanSeeException(e.getMessage());
		}
		catch (BadPaddingException e)
		{
			LOG.error("Exception in Encryption ", e);
			throw new ScanSeeException(e.getMessage());
		}

		response = manageSettingsDao.changePassword(userRegistrationInfo.getUserId(), encryptedpwd);

		if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
		{
			response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.PASSWORDUPDATED);
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName + "in Service layer");
		return response;

	}

	/**
	 * The service Method fetch CategorizedCities. Calls the DAO method with
	 * encrypted password and userID.
	 * 
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("static-access")
	@Override
	public String fetchCategorizedCities() throws ScanSeeException
	{
		final String methodName = "fetchCategorizedCities";
		LOG.info("In Service...." + ApplicationConstants.METHODSTART + methodName);
		ArrayList<CountryCode> stateCityList;
		String response = null;
		stateCityList = manageSettingsDao.fetchCategirizedCities();
		ArrayList<CountryCodes> countryCodesList;
		if (null != stateCityList)
		{
			ManageSettingHelper manageSettingHelperObj = new ManageSettingHelper();
			countryCodesList = manageSettingHelperObj.getCategorizedCities(stateCityList);
			response = XstreamParserHelper.produceXMlFromObject(countryCodesList);
			response = response.replaceAll("<list>", "<PopulationCenters>");
			response = response.replaceAll("</list>", "</PopulationCenters>");
			response = response.replaceAll("<CountryCode>", "<Cities>");
			response = response.replaceAll("</CountryCode>", "</Cities>");

		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}

		LOG.info("In Service" + ApplicationConstants.METHODEND + methodName);

		return response;
	}

	@Override
	public String saveUserEmailId(Integer userID, String emailId) throws ScanSeeException
	{

		final String methodName = "saveUserEmailId";

		LOG.info(ApplicationConstants.METHODSTART + methodName + "in settings Service layer");
		String response = null;

		if (userID == null && emailId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{

			boolean isValid = Utility.validateEmailId(emailId);
			if (!isValid)
			{
				return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDEMAILADDRESSTEXT);
			}
			response = manageSettingsDao.saveUserEmailId(userID, emailId);

			if (ApplicationConstants.SUCCESS.equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.USEREMAILUPDATE);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}

			LOG.info(ApplicationConstants.METHODEND + methodName + "in settings Service layer");
		}
		return response;

	}

	@Override
	public String fetchUnivStates() throws ScanSeeException
	{
		final String methodName = "fetchUnivStates";
		LOG.info("In Service...." + ApplicationConstants.METHODSTART + methodName);
		ArrayList<CountryCode> stateCityList;
		String response = null;
		stateCityList = manageSettingsDao.fetchUnivStates();
		ArrayList<CountryCodes> countryCodesList;
		if (null != stateCityList && !stateCityList.isEmpty())
		{
			response = XstreamParserHelper.produceXMlFromObject(stateCityList);
			response = response.replaceAll("<list>", "<UniversityStates>");
			response = response.replaceAll("</list>", "</UniversityStates>");
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}

		LOG.info("In Service" + ApplicationConstants.METHODEND + methodName);

		return response;
	}

	@Override
	public String fetchUniversities(String stateAbv) throws ScanSeeException
	{
		final String methodName = "fetchUnivStates";
		LOG.info("In Service...." + ApplicationConstants.METHODSTART + methodName);
		ArrayList<CountryCode> stateCityList;
		String response = null;
		stateCityList = manageSettingsDao.fetchUniversities(stateAbv);

		if (null != stateCityList && !stateCityList.isEmpty())
		{
			response = XstreamParserHelper.produceXMlFromObject(stateCityList);
			response = response.replaceAll("<list>", "<UniversityInfo>");
			response = response.replaceAll("</list>", "</UniversityInfo>");
			response = StringEscapeUtils.unescapeXml(response);

		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}

		LOG.info("In Service" + ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * The method for fetching postal codes from the database for the given
	 * state and city.
	 * 
	 * @param stateName
	 *            - for which postal code information need to be fetched.
	 * @param cityName
	 *            - for which postal code information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return list of postal codes.
	 */
	@Override
	public String fetchPostalCode(String stateName, String cityName) throws ScanSeeException
	{
		final String methodName = "fetchPostalCode";
		LOG.info("In Service...." + ApplicationConstants.METHODSTART + methodName);
		ArrayList<String> postalCodeslst;
		String response = null;
		postalCodeslst = manageSettingsDao.fetchPostalCode(stateName, cityName);

		if (stateName == null || stateName.isEmpty() || cityName == null || cityName.isEmpty())
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else if (null != postalCodeslst && !postalCodeslst.isEmpty())
		{
			response = XstreamParserHelper.produceXMlFromObject(postalCodeslst);
			response = response.replaceAll("<list>", "<PostalCodeLst>");
			response = response.replaceAll("</list>", "</PostalCodeLst>");
			response = response.replaceAll("<string>", "<pc>");
			response = response.replaceAll("</string>", "</pc>");
			response = StringEscapeUtils.unescapeXml(response);

		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}

		LOG.info("In Service" + ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * Method to get push notification alert on/off and wish list radius. Calls
	 * XStream helper class methods for parsing
	 * 
	 * @param userId
	 *            - the request xml.
	 * @return response xml with push notification alert on/off and wish list
	 *         radius.
	 * @throws ScanSeeException
	 *             If any exception occurs, ScanSeeException will be thrown.
	 */
	public String getUserSettings(String xml) throws ScanSeeException
	{
		final String methodName = "getUserSettings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		UserSettings userSettings = null;
		ArrayList<UserSettings> userSettingsList = null;
		XstreamParserHelper helper = new XstreamParserHelper();
		AuthenticateUser objAuthenticateUser = (AuthenticateUser) helper.parseXmlToObject(xml);

		if (null == objAuthenticateUser.getUserId() || null == objAuthenticateUser.getDeviceID())
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			userSettingsList = manageSettingsDao.getUserSettings(objAuthenticateUser);
			if (null != userSettingsList && !userSettingsList.isEmpty())
			{
				userSettings = userSettingsList.get(0);
				response = XstreamParserHelper.produceXMlFromObject(userSettings);

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		LOG.info("In Service" + ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * Method to enable or disable push notification alerts. Calls XStream
	 * helper class methods for parsing
	 * 
	 * @param xml
	 *            - the request xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             If any exception occurs, ScanSeeException will be thrown.
	 */
	public String setUserSettings(String xml) throws ScanSeeException
	{
		final String methodName = "setUserSettings";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserSettings userSettings = (UserSettings) streamHelper.parseXmlToObject(xml);
		if (null != userSettings)
		{
			if ((userSettings.getUserId() == null) || (userSettings.getPushNotify() == null) || (userSettings.getLocaleRadius() == null))
			{
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			}
			else
			{
				response = manageSettingsDao.setUserSettings(userSettings);
				if (null != response && !response.equals(ApplicationConstants.FAILURE))
				{
					response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.USERSETTINGSUPDATETEXT);
				}
				else
				{
					response = Utility
							.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
				}
			}
		}
		LOG.info("In Service" + ApplicationConstants.METHODEND + methodName);
		return response;
	}
}
