package com.scansee.managesettings.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.pojos.CountryCode;
import com.scansee.common.pojos.CountryCodes;
import com.scansee.common.pojos.UserRegistrationInfo;
import com.scansee.common.util.Utility;

public class ManageSettingHelper
{

	public static String validateUserValues(UserRegistrationInfo userRegistrationInfo)
	{
		if(userRegistrationInfo.getEmail() !=null)
		{	
		 
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.EMAILID);
		}
		
		
/*
		if (userRegistrationInfo.getUserId() == null)
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.UserID);

		}
		else if (isEmptyOrNullString(userRegistrationInfo.getFirstName()))
		{

			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOFIRSTNAME);
		}

		else if (isEmptyOrNullString(userRegistrationInfo.getEmail()))
		{

			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.EMAILID);
		}

		else

		if (isEmptyOrNullString(userRegistrationInfo.getLastName()))
		{

			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOLASTNAME);

		}
		else

		if (isEmptyOrNullString(userRegistrationInfo.getAddress1()))
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.NOADDRESS);

		}

		else

		if (isEmptyOrNullString(userRegistrationInfo.getCity()))
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else if (isEmptyOrNullString(userRegistrationInfo.getState()))

		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else if (isEmptyOrNullString(userRegistrationInfo.getPostalCode()))

		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else if (userRegistrationInfo.getCountryID() == null)
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else if (userRegistrationInfo.getGender() == null)

		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else if (isEmptyOrNullString(userRegistrationInfo.getDob()))
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else if (userRegistrationInfo.getChildren() == null)
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}

		else if (isEmptyOrNullString(userRegistrationInfo.getMaritalStatus()))
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else if (userRegistrationInfo.getEducationLevelId() == null)

		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else if (userRegistrationInfo.getHomeOwner() == null)
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else if (isEmptyOrNullString(userRegistrationInfo.getMobileNumber()))
		{
			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else if (userRegistrationInfo.getIncomeRangeID() == null)
		{

			return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}*/

		return null;

	}
	
	/**
	 * this method returns shopping list grouped by retailers.
	 * 
	 * @param shoppingListResultSet
	 *            contains list of productDetail objects.
	 * @return result list of ShoppingListDetails objects
	 */
public static ArrayList<CountryCodes> getCategorizedCities(ArrayList<CountryCode> cityList)
	{


	String key = null;
	final HashMap<String, CountryCodes> hotDealsAPIInfoMap = new HashMap<String, CountryCodes>();

	//HotDealAPIResultSet hotDealAPIResultSet = null;
	//ArrayList<HotDealsDetails> hotDealsDetailsLst = null;
	ArrayList<CountryCode> hotDealsDetailsLst = null;
	CountryCodes countryCodes=null;
	for (CountryCode hotDealsDetails : cityList)
	{
		key = Utility.nullCheck(hotDealsDetails.getStateName());
		if (hotDealsAPIInfoMap.containsKey(key))
		{
			countryCodes = hotDealsAPIInfoMap.get(key);
			hotDealsDetailsLst = (ArrayList<CountryCode>) countryCodes.getCountryCode();
			if (null != hotDealsDetailsLst)
			{
				final CountryCode hotDealsDetailsObj = new CountryCode();
				hotDealsDetailsObj.setCityName(hotDealsDetails.getCityName());
				hotDealsDetailsLst.add(hotDealsDetailsObj);
				countryCodes.setCountryCode(hotDealsDetailsLst);
			}

		}
		else
		{
			countryCodes = new CountryCodes();
			
			countryCodes.setState(hotDealsDetails.getStateName());
			hotDealsDetailsLst = new ArrayList<CountryCode>();
			final CountryCode countryCode = new CountryCode();
			countryCode.setCityName(hotDealsDetails.getCityName());
			hotDealsDetailsLst.add(countryCode);
			countryCodes.setCountryCode(hotDealsDetailsLst);
		}
		hotDealsAPIInfoMap.put(key, countryCodes);
	}

	final Set<Map.Entry<String, CountryCodes>> set = hotDealsAPIInfoMap.entrySet();

	final ArrayList<CountryCodes> hotDealsCategoryInfolst = new ArrayList<CountryCodes>();

	for (Map.Entry<String, CountryCodes> entry : set)
	{
		hotDealsCategoryInfolst.add(entry.getValue());
	}

	//return hotDealsCategoryInfolst;

	//	result.setRetailerDetails(retiArrayList);

		return hotDealsCategoryInfolst;
	}
}
