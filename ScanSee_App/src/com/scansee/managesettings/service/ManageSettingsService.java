package com.scansee.managesettings.service;

import com.scansee.common.exception.ScanSeeException;

/**
 * The Manage Setting Service interface. ImplementedBy
 * {@link ManageSettingsServiceImpl}
 * 
 * @author saurabh_r
 */

public interface ManageSettingsService
{

	/**
	 * The service method for fetching user information. This method validates
	 * the userId, if it is valid it will call the DAO method.
	 * 
	 * @param userID
	 *            for which user information need to be fetched.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String fetchUserInfo(Integer userID, boolean isCellFireRequest) throws ScanSeeException;

	/**
	 * The Service method for Saving User Information. This method checks for
	 * mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method.
	 * 
	 * @param xml
	 *            containing user information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String insertUserInfo(String xml) throws ScanSeeException;

	/**
	 * The service method for fetching user setting preferrences information.
	 * This method validates the userId, if it is valid it will call the DAO
	 * method.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String fetchUserPreferences(Integer userId) throws ScanSeeException;

	/**
	 * The Service method for Saving User setting preffereces information. This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param inputXml
	 *            Contains user proffered categories information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String saveUserPreference(String inputXml) throws ScanSeeException;

	/**
	 * The service method for fetching user favorite categories information.
	 * This method validates the userId, if it is valid it will call the DAO
	 * method.
	 * 
	 * @param userId
	 *            for which user information need to be fetched.
	 * @param lowerLimit
	 *            for which used for pagination
	 * @return XML containing user favorite categories information in the
	 *         response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String getUserFavCategories(Integer userId, Integer lowerLimit) throws ScanSeeException;

	/**
	 * The Service method for Saving User favoutire categories Information. This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param inputXML
	 *            Contains user favorite categories information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String setUserFavCategories(String inputXML) throws ScanSeeException;

	/**
	 * The service method for fetching user preferred retailers information.
	 * This method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param inputXML
	 *            Contains the request for user proffered retailers information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String getPreferredRetailers(String inputXML) throws ScanSeeException;

	/**
	 * The Service method for Saving User proffered retailers information. This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param inputXML
	 *            Contains User proffered retailers information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String setPreferredRetailers(String inputXML) throws ScanSeeException;

	/**
	 * The service method for all the countries.
	 * 
	 * @return the XML containing Countries information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String fetchAllCountries() throws ScanSeeException;

	/**
	 * The service Method takes the user new password and encrypt it. Calls the
	 * dao method with encrypted password and userID.
	 * 
	 * @param inputXML
	 *            in the User Request.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String changePassword(String inputXML) throws ScanSeeException;

	/**
	 * The service method for all the States.
	 * 
	 * @return the XML containing states information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	String fetchAllStates() throws ScanSeeException;

	/**
	 * The service method for all the cities for the given state name.
	 * 
	 * @param stateName
	 *            -For which cities to be fetched
	 * @param searchKeyword
	 *            -For which cities to be fetched
	 * @return the XML containing cities information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String fetchAllCities(String stateName, String searchKeyword) throws ScanSeeException;

	/**
	 * The service method for all the states and cities for the given zipCode.
	 * 
	 * @param zipCode
	 *            for which states and cities to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return cities and states
	 */

	String fetchAllCitieAndStates(String zipCode) throws ScanSeeException;

	/**
	 * The service method for all the states and cities for the given zipCode.
	 * 
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 * @return cities and states
	 */

	String fetchCategorizedCities() throws ScanSeeException;
	
	/**
	 * The service method for fetching user information. This method validates
	 * the userId, if it is valid it will call the DAO method.
	 * 
	 * @param userID
	 *            for which user information need to be fetched.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String saveUserEmailId(Integer userID,String emailId) throws ScanSeeException;
	
	/**
	 * The Service method for Saving User Information. This method checks for
	 * mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method.
	 * 
	 * @param xml
	 *            containing user information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String fetchUnivStates() throws ScanSeeException;
	
	/**
	 * The Service method for Saving User Information. This method checks for
	 * mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method.
	 * 
	 * @param xml
	 *            containing user information.
	 * @return returns the an XML depending on SUCCESS or ERROR.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	String fetchUniversities(String stateAbv) throws ScanSeeException;
	
	String fetchPostalCode(String stateName, String cityName) throws ScanSeeException;
	
	/**
	 * Method to get push notification alert on/off and wish list radius. Calls XStream
	 * helper class methods for parsing
	 * 
	 * @param userId
	 *            - the request xml.
	 * 
	 * @return response xml with push notification alert on/off and wish list radius.
	 * @throws ScanSeeException
	 *             If any exception occurs, ScanSeeException will be thrown.
	 */
	String getUserSettings(String xml) throws ScanSeeException;
	
	/**
	 * Method to enable or disable push notification alerts. Calls XStream
	 * helper class methods for parsing
	 * 
	 * @param xml
	 *            - the request xml.
	 * 
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             If any exception occurs, ScanSeeException will be thrown.
	 */
	String setUserSettings(String xml) throws ScanSeeException;
}
