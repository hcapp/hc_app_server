package com.scansee.managesettings.query;

/**
 * The class has fields for Query used in ManageSettings Module. (used in
 * ManageSettingsDAOImpl class)
 * 
 * @author dileepa_cc
 */

public class ManageSettingQueries
{
    
	/**
	 * The query for Updating the User Info.
	 */

	public static final String UPDATEUSERINFOQUERY = "update  [Users]  set" + " [Users].FirstName=?,[Users].Lastname=? ,"
			+ " [Users].Address1=?,[Users].Address2=?," + " [Users].Address3=?,[Users].Address4=?," + " [Users].City=?,[Users].State=?,"
			+ " [Users].PostalCode=?,[Users].CountryID=?," + " [Users].MobilePhone=?,[Users].Email=?," + " [Users].Password=?,[Users].FieldAgent=?,"
			+ " [Users].AverageFieldAgentRating=?,[Users].DateCreated=?," + " [Users].CreateUserID=?,[Users].DateModified=?,"
			+ " [Users].ModifyUserID=?, where UserName= ?";

	/**
	 * The query for Updating the User Info.
	 */
	public static final String FETCHUSERINFOQUERY = "Select u.* , c.CountryName from Users u join [CountryCode] c on u.CountryID = c.CountryID where u.UserID= ?";

	/**
	 * The query for fetchIncomeRanges.
	 */
	public static final String FETCHINCOMERANGESQUERY = "select IncomeRangeID,IncomeRange from IncomeRange";
	/**
	 * The query for fetch Country Codes.
	 */
	public static final String FETCHCOUNTRYCODESQUERY = "select CountryID,CountryName from CountryCode where Active = 1";
	/**
	 * The query for get Payment Interval Details.
	 */
	public static final String GETPAYMENTINTERVALDETAILSQUERY = " select PaymentIntervalID,PayMonthly,PayIntervalAmount from PaymentInterval ";
	/**
	 * The query for get PaymentTypeDetails.
	 */
	public static final String GETPAYMENTTYPEDETAILSQUERY = "select PaymentTypeID, PaymentType as PaymentTypeName from PaymentType";
	/**
	 * The query for get PayoutDetails.
	 */
	public static final String GETPAYOUTDETAILSQUERY = " select * from PayoutType ";
	/**
	 * The query for fetch EducationalLevel.
	 */
	public static final String FETCHEDUCATIONALLEVELQUERY = "SELECT EducationLevelID as educationalLevelId, EducationLevelDescription as educationalLevelDesc FROM EducationLevel";
	/**
	 * The query for fetch MaritalStatus.
	 */
	public static final String FETCHMARITALSTATUSQUERY = "SELECT MaritalStatusID as maritalStatusId  , MaritalStatusName maritalStatusName FROM MaritalStatus";
	/**
	 * The query for fetch State.
	 */
	public static final String FETCHSTATEQUERY = "SELECT DISTINCT State stateName FROM GeoPosition ORDER BY state";
	/**
	 * The query for fetch City.
	 */
	public static final String FETCHCITYQUERY = "SELECT DISTINCT city cityName FROM GeoPosition WHERE State=? order by city";
	/**
	 * The query for fetch CityAndStates.
	 */
	public static final String FETCHCITYANDSTATESQUERY = "SELECT State stateName, city cityName FROM GeoPosition WHERE PostalCode=?";
	/**
	 * The query for Population Center cities.
	 */
	public static final String POPULATIONCENTER = "SELECT DISTINCT City cityName FROM GeoPosition WHERE PopulationCenterFlag = 1 ORDER BY City";
	
	/**
	 * The query for Population research Center cities.
	 */
	public static final String POPULATIONCENTERSEARCHCITY = "SELECT DISTINCT City cityName FROM GeoPosition WHERE PopulationCenterFlag = 1 and City like ? ORDER BY City";
    
	/**
	 * The query for Population research Center cities.
	 */
	public static final String POPULATIONCATEGORIZEDCITIES = "SELECT DISTINCT state stateName,City  cityName FROM GeoPosition WHERE PopulationCenterFlag = 1 ORDER BY State, City";
	/**
	 * constructor for ManageSettingQueries.
	 */
	private ManageSettingQueries()
	{

	}
}
