package com.scansee.firstuse.controller;

import static com.scansee.common.constants.ScanSeeURLPath.DISPLAYMAINMENU;
import static com.scansee.common.constants.ScanSeeURLPath.FETCHTUTORIALMEDIA;
import static com.scansee.common.constants.ScanSeeURLPath.FIRSTUSEBASEURL;
import static com.scansee.common.constants.ScanSeeURLPath.PUSHNOTIFYALERT;
import static com.scansee.common.constants.ScanSeeURLPath.SAVEUSERMEDIA;
import static com.scansee.common.constants.ScanSeeURLPath.SCREENTEXTINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GENERALSCREENTEXTINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETSTARTEDIMAGES;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;


import com.scansee.common.constants.ScanSeeURLPath;

/**
 * The FirstUseRestEasy Interface. {@link ImplementedBy}
 * {@link FirstUseRestEasyImpl}
 * 
 * @author dileepa_cc
 */
@Path(FIRSTUSEBASEURL)
public interface FirstUseRestEasy
{

	/**
	 * This is a RestEasy WebService Method for Authenticate User Login. Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */

	@POST
	@Path("/authenticate")
	@Produces("text/xml")
	@Consumes("text/xml")
	String authenticateLoginUser(String xml);

	/**
	 * This is a RestEasy WebService Method for Register User Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */

	@POST
	@Path("/saveUser")
	@Produces("text/xml")
	@Consumes("text/xml")
	String registerUser(String xml);

	/**
	 * This is a RestEasy WebService Method for push notification alert for the
	 * given userId and pushNotify. Method Type:GET.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param pushNotify
	 *            as request parameter. for updating the push notify info.
	 * @return XML containing success or failure in the response.
	 */

	@GET
	@Path(PUSHNOTIFYALERT)
	@Produces("text/xml")
	String pushNotifyAlert(@QueryParam("USERID") Integer userId, @QueryParam("pushNotify") String pushNotify);

	/**
	 * This is a RestEasy WebService Method for display Main menu based on the
	 * the given userId. Method Type:GET
	 * 
	 * @param userId
	 *            for which user information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return XML containing user information in the response.
	 */

	@POST
	@Path(DISPLAYMAINMENU)
	@Produces("text/xml")
	@Consumes("text/xml")
	String displayMainMenu(String xml);

	/**
	 * This is a RestEasy WebService Method to Save User viewed Media info. the
	 * given userId. Method Type:POST
	 * 
	 * @param xml
	 *            for which user information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return XML containing user information in the response.
	 */

	@POST
	@Path(SAVEUSERMEDIA)
	@Produces("text/xml")
	@Consumes("text/xml")
	String saveUserMediaInfo(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching tutorial media
	 * information. Method Type:GET.
	 * @param userId
	 *            for which tutorial mediainfo need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return XML containing tutorial media information.
	 */

	@GET
	@Path(FETCHTUTORIALMEDIA)
	@Produces("text/xml;charset=UTF-8")
	String fetchTutorialMediaInfo(@QueryParam("userId") Integer userId);

	/**
	 * This is a RestEasy WebService Method for fetching Screen Text Info for
	 * the given screenName. Method Type:GET.
	 * 
	 * @param screenName
	 *            for which Screen Text Info need to be fetched.If screenName is
	 *            null then it is invalid request.
	 * @return XML containing Screen text information in the response.
	 */

	@GET
	@Path(SCREENTEXTINFO)
	@Produces("text/xml;charset=UTF-8")
	String fetchScreenTextInfo(@QueryParam("screenName") String screenName);

	/**
	 * This is a RestEasy WebService Method for sends password to user EmailId.
	 * Method Type:GET.
	 * 
	 * @param userName
	 *            for which sends password to user EmailId.If emailId is null
	 *            then it is invalid request.
	 * @return XML containing password in the response.
	 */

	@GET
	@Path(ScanSeeURLPath.FORGOTPASSWORD)
	@Produces("text/xml")
	String forgotPassword(@QueryParam("username") String userName);

	/**
	 * This is a RestEasy WebService Method for email notify. Method Type:POST.
	 * 
	 * @param xml
	 *            for which Decline email notification.
	 * @return XML containing success or failure in the response.
	 */

	@POST
	@Path(ScanSeeURLPath.EMAILDECLAIMNOTIFICATION)
	@Produces("text/xml")
	@Consumes("text/xml")
	String emailDeclaimNotification(String xml);

	

	/**
	 * This is a RestEasy WebService Method for Register facebook User Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */

	@POST
	@Path(ScanSeeURLPath.SAVEFACEBOOKUSER)
	@Produces("text/xml")
	@Consumes("text/xml")
	String registerFaceBookUser(String xml);
	
	/**
	 * This is a RestEasy WebService Method for fetching Screen Text Info for
	 * the given screenName. Method Type:GET.
	 * 
	 * @param screenName
	 *            for which Screen Text Info need to be fetched.If screenName is
	 *            null then it is invalid request.
	 * @return XML containing Screen text information in the response.
	 */

	@GET
	@Path(GENERALSCREENTEXTINFO)
	@Produces("text/xml;charset=UTF-8")
	String fetchGeneralScreenTextInfo(@QueryParam("screenName") String screenName);
	

	/**
	 * This is a RestEasy WebService Method for Register device Id and token id for push notification
	 *  Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */

	@POST
	@Path(ScanSeeURLPath.REGISTERPUSHNOTIFICATION)
	@Produces("text/xml")
	@Consumes("text/xml")
	String registerPushNotification(String xml);
	
	/**
	 * This is a RestEasy WebService Method for Register User Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */

	@POST
	@Path(ScanSeeURLPath.UPDATEAPPVERSION)
	@Produces("text/xml")
	@Consumes("text/xml")
	String updateAppVersion(String xml);
	

	/**
	 * This is a RestEasy WebService Method for Register User Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */

	@POST
	@Path(ScanSeeURLPath.ADDDEVICETOUSER)
	@Produces("text/xml")
	@Consumes("text/xml")
	String addDeviceToUser(String xml);
	
	/**
	 * For user tracking. To be called after selecting modules.
	 *  
	 * @param xml 
	 * @return XML containing SUCCESS or FAILURE response.
	 */
	@POST
	@Path(ScanSeeURLPath.UTMODULESELECT)
	@Produces("text/xml")
	String userTrackingModuleClick(String xml);
	
	/**
	 * This is a RestEasy WebService Method is get started images information. 
	 * Method Type:GET.
	 * @param userId, as request parameter.
	 * @return XML containing started images  information.
	 */

	@GET
	@Path(GETSTARTEDIMAGES)
	@Produces("text/xml;charset=UTF-8")
	String getStartedImageInfo(@QueryParam("userId") Integer iUserId);
	
	/**
	 * This is a rest easy method to contact ScanSee via email.
	 * @param xml as input request.
	 * @return xml string containing SUCCESS or FAILURE.
	 */
	@POST
	@Path("/contactscansee")
	@Consumes("text/xml")
	@Produces("text/xml")
	String contactScanSeeByEmail(String xml);
	
}
