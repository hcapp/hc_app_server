package com.scansee.firstuse.controller;

import static com.scansee.common.util.Utility.formResponseXml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.servicefactory.ServiceFactory;
import com.scansee.firstuse.service.FirstUseService;

/**
 * The FirstUse RestEasy has methods to accept First Use requests.These methods
 * are called on the Client request. The method is selected based on the URL
 * Path and the type of request(GET,POST). It invokes the appropriate method in
 * Service layer.
 * 
 * @author dileepa_cc
 */
public class FirstUseRestEasyImpl implements FirstUseRestEasy
{
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(FirstUseRestEasyImpl.class);
	/**
	 * debugger flag for logging.
	 */
	private static final boolean ISDEBUGENABLED = LOG.isDebugEnabled();

	/**
	 * The FirstUseRestEasy Constructor.
	 */

	public FirstUseRestEasyImpl()
	{

	}

	/**
	 * This is a RestEasy WebService Method for Authenticate User Login. Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the Valid user or Invalid User/Password in the
	 *         response.
	 */

	public String authenticateLoginUser(String xml)
	{
		final String methodName = "authenticateLoginUser";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved XML :" + xml);
		}
		String responseXML = null;
		final FirstUseService firstUse = ServiceFactory.getFirstUseService();
		try
		{
			responseXML = firstUse.isValidUser(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response is:" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is a RestEasy WebService Method for Register User Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */
	public String registerUser(String xml)
	{
		final String methodName = "registerUser";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved XML " + xml);
		}
		String responseXML = null;
		final FirstUseService firstUse = ServiceFactory.getFirstUseService();
		try
		{
			responseXML = firstUse.processFirstUSe(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		LOG.info("Returned Response is :" + responseXML);
		return responseXML;
	}

	/**
	 * This is a RestEasy WebService Method for push notification alert for the
	 * given userId and pushNotify. Method Type:GET.
	 * 
	 * @param userId
	 *            as request parameter.
	 * @param pushNotify
	 *            as request parameter. for updating the push notify info.
	 * @return XML containing success or failure in the response.
	 **/

	public String pushNotifyAlert(Integer userId, String pushNotify)
	{
		String response = null;
		final String methodName = "pushNotifyAlert";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Received Data:" + "User Id:" + userId + "pushNotify:" + pushNotify);
		}
		final FirstUseService firstUseService = ServiceFactory.getFirstUseService();
		try
		{
			response = firstUseService.pushNotifyAlert(userId, pushNotify);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response ::" + response);
		}
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for display Main menu based on the
	 * the given userId. Method Type:GET
	 * 
	 * @param userId
	 *            for which user information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return XML containing user information in the response.
	 */

	@Override
	public String displayMainMenu(String xml)
	{
		final String methodName = "displayMainMenu";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved Data: " + xml);
		}
		String responseXML = null;
		final FirstUseService firstUseService = ServiceFactory.getFirstUseService();
		try
		{
			responseXML = firstUseService.displayMainMenu(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response ::" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is a RestEasy WebService Method to Save User viewed Media info. the
	 * given userId. Method Type:POST
	 * 
	 * @param xml
	 *            for which user information need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return XML containing user information in the response.
	 */

	@Override
	public String saveUserMediaInfo(String xml)
	{
		final String methodName = "saveUserMediaInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved XML :" + xml);
		}
		String responseXML = null;
		final FirstUseService firstUseService = ServiceFactory.getFirstUseService();
		try
		{

			responseXML = firstUseService.saveUserMediaInfo(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response :::" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is a RestEasy WebService Method for fetching tutorial media
	 * information. Method Type:GET.
	 * @param userId
	 *            for which tutorial mediainfo need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return XML containing tutorial media information.
	 */

	@Override
	public String fetchTutorialMediaInfo(Integer userId)
	{
		final String methodName = "fetchTutorialMediaInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String responseXML = null;
		final FirstUseService firstUseService = ServiceFactory.getFirstUseService();
		if (ISDEBUGENABLED)
		{
			LOG.debug("Requested UserID::" + userId);
		}

		try
		{
			responseXML = firstUseService.fetchTutorialMediaInfo(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response --:" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is a RestEasy WebService Method for sends password to user EmailId.
	 * Method Type:GET.
	 * 
	 * @param userName
	 *            for which sends password to user EmailId.If emailId is null
	 *            then it is invalid request.
	 * @return XML containing password in the response.
	 */

	@Override
	public String forgotPassword(String userName)
	{
		final String methodName = "forgotPassword in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved Data: emailId:" + userName);
		}
		String response = null;
		final FirstUseService firstUseService = ServiceFactory.getFirstUseService();
		try
		{
			LOG.info("User with email id {}  has request for  password", userName);
			response = firstUseService.forgotPassword(userName);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Returned Response:" + response);
		}
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching Screen Text Info for
	 * the given screenName. Method Type:GET.
	 * 
	 * @param screenName
	 *            for which Screen Text Info need to be fetched.If screenName is
	 *            null then it is invalid request.
	 * @return XML containing user information in the response.
	 */

	@Override
	public String fetchScreenTextInfo(String screenName)
	{
		final String methodName = "fetchScreenTextInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved Data: screenName:" + screenName);
		}
		String responseXML = null;
		final FirstUseService firstUseService = ServiceFactory.getFirstUseService();
		try
		{
			responseXML = firstUseService.fetchStaticScreenInfo(screenName);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response -->" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is a RestEasy WebService Method for email notify. Method Type:POST.
	 * 
	 * @param xml
	 *            for which Decline email notification.
	 * @return XML containing success or failure in the response.
	 */

	public String emailDeclaimNotification(String xml)
	{
		final String methodName = "emailDeclaimNotification";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved XML " + xml);
		}
		String responseXML = null;
		final FirstUseService firstUse = ServiceFactory.getFirstUseService();
		try
		{
			responseXML = firstUse.emailNotification(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response :" + responseXML);
		}
		return responseXML;
	}
	
	
	/**
	 * This is a RestEasy WebService Method for Register facebook User Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */
	
	public String registerFaceBookUser(String xml)
	{
		final String methodName = "registerFaceBookUser";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved XML::" + xml);
		}
		String responseXML = null;
		final FirstUseService firstUse = ServiceFactory.getFirstUseService();
		try
		{
			responseXML = firstUse.processFaceBookUser(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		LOG.info("Returned Response is -->" + responseXML);
		return responseXML;
	}
	/**
	 * This is a RestEasy WebService Method for fetching General Screen Text Info for
	 * the given screenName. Method Type:GET.
	 * 
	 * @param screenName
	 *            for which Screen Text Info need to be fetched.If screenName is
	 *            null then it is invalid request.
	 * @return XML containing user information in the response.
	 */
	@Override
	public String fetchGeneralScreenTextInfo(String screenName)
	{
		final String methodName = "fetchGeneralScreenTextInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved Data: screenName:" + screenName);
		}
		String responseXML = null;
		final FirstUseService firstUseService = ServiceFactory.getFirstUseService();
		try
		{
			responseXML = firstUseService.fetchGeneralStaticScreenInfo(screenName);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Returned Response :" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is a RestEasy WebService Method for Register device id and token id
	 *  Method Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */
	@Override
	public String registerPushNotification(String xml)
	{
		final String methodName = "registerPushNotification";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved XML::" + xml);
		}
		String responseXML = null;
		final FirstUseService firstUse = ServiceFactory.getFirstUseService();
		try
		{
			responseXML = firstUse.registerPushNotification(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		LOG.info("Returned Response is -->" + responseXML);
		return responseXML;
	}

	@Override
	public String updateAppVersion(String xml)
	{
		final String methodName = "updateAppVersion";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved XML " + xml);
		}
		String responseXML = null;
		final FirstUseService firstUse = ServiceFactory.getFirstUseService();
		try
		{
			responseXML = firstUse.updateAppVersion(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		LOG.info("Returned Response is :" + responseXML);
		return responseXML;
	}

	@Override
	public String addDeviceToUser(String xml)
	{
		final String methodName = "addDeviceToUser";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved XML " + xml);
		}
		String responseXML = null;
		final FirstUseService firstUse = ServiceFactory.getFirstUseService();
		try
		{
			responseXML = firstUse.addDeviceToUser(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		LOG.info("Returned Response is :" + responseXML);
		return responseXML;
	}

	/**
	 * For user tracking. To be called after selecting modules.
	 *  
	 * @return XML containing SUCCESS or FAILURE response.
	 */
	@Override
	public String userTrackingModuleClick(String xml) {
		final String methodName = "userTrackingModuleClick";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED)
		{
			LOG.debug("Recieved XML: " + xml);
		}
		String responseXML = null;
		final FirstUseService firstUse = ServiceFactory.getFirstUseService();
		try
		{
			responseXML = firstUse.userTrackingModuleClick(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		LOG.info("Returned Response is :" + responseXML);
		return responseXML;
	}

	/**
	 * This is a RestEasy WebService Method is get started images information. 
	 * Method Type:GET.
	 * @param userId, as request parameter.
	 * @return XML containing started images information.
	 */
	public String getStartedImageInfo(Integer iUserId)
	{
		final String methodName = "getStartedImageInfo";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponseXML = null;
		final FirstUseService firstUseService = ServiceFactory.getFirstUseService();
		try {
			strResponseXML = firstUseService.getStartedImageInfo(iUserId);
		} catch (ScanSeeException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			strResponseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponseXML;
	}

	/**
	 * This is a rest easy method to contact ScanSee via email.
	 * @param xml as input request.
	 * @return xml string containing SUCCESS or FAILURE.
	 */
	@Override
	public String contactScanSeeByEmail(String xml) {
		final String methodName = "contactScanSeeByEmail";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponseXML = null;
		final FirstUseService firstUseService = ServiceFactory.getFirstUseService();
		try {
			strResponseXML = firstUseService.contactScanSeeByEmail(xml);
		} catch (ScanSeeException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			strResponseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponseXML;
	}
}
