package com.scansee.firstuse.dao;

import java.util.List;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AuthenticateUser;
import com.scansee.common.pojos.DisplayMainMenu;
import com.scansee.common.pojos.DisplayMainMenuResultSet;
import com.scansee.common.pojos.EmailDecline;
import com.scansee.common.pojos.ScreenTextInfo;
import com.scansee.common.pojos.TutorialMediaResultSet;
import com.scansee.common.pojos.UserRegistrationInfo;
import com.scansee.common.pojos.UserTrackingData;

/**
 * The interface implemented by FirstUseDAOImpl class for inserting user
 * registration details,finding user exists and validating user. ImplementedBy
 * {@link FirstUseDAOImpl}
 * 
 * @author shyamsundara_hm
 */

public interface FirstUseDAO
{

	/**
	 * This method insert the user registration details.
	 * 
	 * @param userRegistrationInfo
	 *            instance of UserRegistrationInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String registerUser(UserRegistrationInfo userRegistrationInfo) throws ScanSeeException;

	/**
	 * This method checks if user is Valid.
	 * 
	 * @param userName
	 *            the UserName in the request.
	 * @param passWord
	 *            the passWord in the request.
	 * @param userId
	 *            n the request.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String isValidUser(String userName, String passWord, Integer userId) throws ScanSeeException;

	/**
	 * This method for updating status of pushNotify.
	 * 
	 * @param userId
	 *            in the request.
	 * @param pushNotify
	 *            in the request.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String pushNotifyAlert(int userId, String pushNotify) throws ScanSeeException;

	/**
	 * The DAO method to display Main menu from the database for the given
	 * userID.
	 * 
	 * @param displayMainMenuObj
	 *            for display main menu.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return DisplayMainMenuResultSet object.
	 */

	DisplayMainMenuResultSet displayMainMenu(DisplayMainMenu displayMainMenuObj) throws ScanSeeException;

	/**
	 * The DAO method to Save User Media info in the database for the given
	 * userID and mediaId.
	 * 
	 * @param userId
	 *            for Save User viewed Media info.
	 * @param mediaId
	 *            for Save User viewed Media info.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return response.
	 */

	String saveUserMediaInfo(Integer userId, Integer mediaId) throws ScanSeeException;

	/**
	 * The DAO method to fetch tutorial media details from the database.
	 * 
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return TutorialMediaResultSet object.
	 */

	TutorialMediaResultSet fetchTutorialMediaInfo() throws ScanSeeException;

	/**
	 * This method fetches user details for sending password to user email id.
	 * 
	 * @param userName
	 *            request parameter
	 * @return User Details
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	AuthenticateUser fetchUserDetailsforForgotPassword(String userName) throws ScanSeeException;

	/**
	 * The DAO method for fetching welcome screen details from the database for
	 * the given screenName.
	 * 
	 * @param screenName
	 *            for which user information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return ScreenTextInfo object.
	 */

	ScreenTextInfo fetchStaticScreenDetails(String screenName) throws ScanSeeException;

	/**
	 * The DAO method for Authenticate login to the database for the based on
	 * the given userName and password.
	 * 
	 * @param authenticateUserObj
	 *            as request parameter.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return AuthenticateUser object.
	 */

	AuthenticateUser loginAuthentication(AuthenticateUser authenticateUserObj) throws ScanSeeException;

	/**
	 * The DAO method for fetching emailDeclaimNotification details from the
	 * database for the given configurationType.
	 * 
	 * @param configurationType
	 *            for which emailDeclaimNotification information need to be
	 *            fetched.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return screenContent List object.
	 */

	List<EmailDecline> emailDeclaimNotification(String configurationType) throws ScanSeeException;

	/**
	 * The DAO method for fetches details for sending email. Based on the given
	 * configurationType.
	 * 
	 * @param configurationType
	 *            as request parameter.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return list of from address and subject.
	 */

	List<EmailDecline> fetchFrmAddressAndSubjectforForgotPassword(String configurationType) throws ScanSeeException;

	/**
	 * This method insert the facebook user registration details.
	 * 
	 * @param userRegistrationInfo
	 *            instance of UserRegistrationInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	AuthenticateUser registerFaceBookUser(UserRegistrationInfo userRegistrationInfo) throws ScanSeeException;

	/**
	 * The DAO method for fetching welcome screen details from the database for
	 * the given screenName.
	 * 
	 * @param screenName
	 *            for which user information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return ScreenTextInfo object.
	 */
	ScreenTextInfo GeneralStaticScreenContent(String screenName) throws ScanSeeException;

	/**
	 * This method insert the device id and token id for push notification.
	 * 
	 * @param userRegistrationInfo
	 *            instance of UserRegistrationInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String registerPushNotification(UserRegistrationInfo userRegistrationInfo) throws ScanSeeException;

	/**
	 * To update app version.
	 * 
	 * @param userRegistrationInfo
	 *            containing user information
	 * @return String with SUCCESS or FAILURE
	 * @throws ScanSeeException
	 *             The exception are cought and a ScanSee Exception defined for
	 *             the application is thrown whicj is caught in Controller layer
	 */
	String updateAppVersion(UserRegistrationInfo userRegistrationInfo) throws ScanSeeException;

	/**
	 * 
	 * @param userRegistrationInfo
	 *            contains userID, deviceID and app version
	 * @return String with SUCCESS or FAILURE
	 * @throws ScanSeeException
	 *             The exception are cought and a ScanSee Exception defined for
	 *             the application is thrown whicj is caught in Controller layer
	 */
	String addDeviceToUser(UserRegistrationInfo userRegistrationInfo) throws ScanSeeException;
	
	/**
	 * For user tracking. To be executed when user selects the modules
	 * 
	 * @return SUCCESS or FAILURE response
	 * @throws ScanSeeException
	 */
	Integer userTrackingModuleClick(UserTrackingData userTrackingData) throws ScanSeeException;
	
	/**
	 * The DAO method to get started images information from the database.
	 * 
	 * @throws ScanSeeException If any exception occurs ScanSeeException will be thrown.
	 * @return TutorialMediaResultSet object.
	 */
	TutorialMediaResultSet getStartedImageInfo() throws ScanSeeException;
	
	/**
	 * This method to fetch contact scansee email address.
	 * @param xml as input request.
	 * @return xml string containing SUCCESS or FAILURE, if SUCCESS mail will be sent to scansee.
	 */
	String contactScanSeeByEmail() throws ScanSeeException;
}
