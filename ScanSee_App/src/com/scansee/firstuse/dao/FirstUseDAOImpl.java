package com.scansee.firstuse.dao;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.AppConfiguration;
import com.scansee.common.pojos.AuthenticateUser;
import com.scansee.common.pojos.DisplayMainMenu;
import com.scansee.common.pojos.DisplayMainMenuResultSet;
import com.scansee.common.pojos.EmailDecline;
import com.scansee.common.pojos.ScreenTextInfo;
import com.scansee.common.pojos.SectionContent;
import com.scansee.common.pojos.TickerInfo;
import com.scansee.common.pojos.TutorialMedia;
import com.scansee.common.pojos.TutorialMediaResultSet;
import com.scansee.common.pojos.UserMediaInfo;
import com.scansee.common.pojos.UserRegistrationInfo;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.common.util.EncryptDecryptPwd;
import com.scansee.common.util.Utility;
import com.scansee.firstuse.query.FirstUseQueries;

/**
 * The implementation class for FirstUseDAO. Implements the FirstUseDAO. This
 * class for inserting user registration parameters,finding user exists and
 * validating user. The methods of this class are called from the FirstUse
 * Service layer.
 * 
 * @author shyamsundara_hm
 */
public class FirstUseDAOImpl implements FirstUseDAO {
	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 */
	private static final Logger log = LoggerFactory.getLogger(FirstUseDAOImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * This method insert the user registration details.
	 * 
	 * @param userRegistrationInfo
	 *            instance of UserRegistrationInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String registerUser(UserRegistrationInfo userRegistrationInfo)
			throws ScanSeeException {
		final String methodName = "registerUser";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		String response = null;
		EncryptDecryptPwd encryptDecryptPwd;
		String encryptedpwd = null;
		try {
			encryptDecryptPwd = new EncryptDecryptPwd();
			encryptedpwd = encryptDecryptPwd.encrypt(userRegistrationInfo.getPassword());
		} catch (NoSuchAlgorithmException e) {
			log.error("Exception in Encryption of password..", e);
			throw new ScanSeeException(e.getMessage());
		} catch (NoSuchPaddingException e) {
			log.error("Exception in Encryption of user password..", e);
			throw new ScanSeeException(e.getMessage());
		} catch (InvalidKeyException e) {
			log.error("user password Encryption Exception    ", e);
			throw new ScanSeeException(e.getMessage());
		} catch (InvalidAlgorithmParameterException e) {
			log.error("Exception while Encryption  ", e);
			throw new ScanSeeException(e.getMessage());
		} catch (InvalidKeySpecException e) {
			log.error("Exception during Encryption ", e);
			throw new ScanSeeException(e.getMessage());
		} catch (IllegalBlockSizeException e) {
			log.error("Exception in user password Encryption ", e);
			throw new ScanSeeException(e.getMessage());
		} catch (BadPaddingException e) {
			log.error("Exception in Encryption ", e);
			throw new ScanSeeException(e.getMessage());
		}
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FirstUse");
			final MapSqlParameterSource userRegistrationParameter = new MapSqlParameterSource();
			userRegistrationParameter.addValue(ApplicationConstants.USERNAME, userRegistrationInfo.getEmail());
			userRegistrationParameter.addValue(ApplicationConstants.PASSWORD, encryptedpwd);
			userRegistrationParameter.addValue("DateCreated", Utility.getFormattedDate());
			userRegistrationParameter.addValue("DeviceID", userRegistrationInfo.getDeviceID());
			userRegistrationParameter.addValue("UserLongitude", userRegistrationInfo.getUserLongitude());
			userRegistrationParameter.addValue("UserLatitude", userRegistrationInfo.getUserLatitude());
			userRegistrationParameter.addValue("AppVersion", userRegistrationInfo.getAppVersion());
			userRegistrationParameter.addValue("Gender", userRegistrationInfo.getGender());
			userRegistrationParameter.addValue("EmailSubscription", userRegistrationInfo.getSubscribe());
			resultFromProcedure = simpleJdbcCall.execute(userRegistrationParameter);
			/**
			 * For getting response from stored procedure ,sp return Result as
			 * response success as 0 failure as 1 it will return list resultset
			 * so casting into map and getting Result param value
			 */
			responseFromProc = (Integer) resultFromProcedure
					.get(ApplicationConstants.STATUS);

			if (null != responseFromProc && responseFromProc.intValue() == 0) {
				response = ((Integer) resultFromProcedure.get("FirstUserID"))
						.toString();
			} else if ((Integer) resultFromProcedure.get("Result") == -1) {
				response = ApplicationConstants.FAILURE;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure
						.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure
						.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in registerUser method ..errorNum..."
						+ errorNum + "errorMsg  .." + errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		} catch (DataAccessException exception) {
			log.error("Exception occurred in registerUser ",
					ApplicationConstants.DATAACCESSEXCEPTIONCODE, exception);
			throw new ScanSeeException(exception);
		} catch (ParseException exception) {
			log.error("Exception occurred in registerUser ", exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method check the user is valid user or not.
	 * 
	 * @param userName
	 *            for validUser
	 * @param passWord
	 *            for validUser
	 * @param userId
	 *            for validUser
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public String isValidUser(String userName, String passWord, Integer userId)
			throws ScanSeeException {
		final String methodName = "isValidUser";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String result = null;
		String userNameFromDb = null;
		String passwordFromDb = null;
		String decryptedPassword = null;
		EncryptDecryptPwd enryptDecryptpwd;
		String struserId = null;
		/**
		 * Implement the RowMapper call back interface
		 */
		AuthenticateUser authenticateUser = null;
		try {
			if (userName != null) {
				authenticateUser = this.jdbcTemplate.queryForObject(
						FirstUseQueries.ISVALIDUSERQUERY,
						new Object[] { String.valueOf(userName) },
						new RowMapper<AuthenticateUser>() {
							public AuthenticateUser mapRow(ResultSet rs,
									int rowNum) throws SQLException {
								final AuthenticateUser authenticateUser = new AuthenticateUser();
								// authenticateUser.setEmail(rs.getString(ApplicationConstants.EMAIL));
								authenticateUser.setUserName(rs
										.getString(ApplicationConstants.USERNAME));
								authenticateUser.setPassword(rs
										.getString(ApplicationConstants.PASSWORD));
								authenticateUser.setUserId(rs
										.getInt(ApplicationConstants.USERID));
								return authenticateUser;
							}
						});
			} else {
				authenticateUser = this.jdbcTemplate.queryForObject(
						FirstUseQueries.AUTHENTICATEUSERQUERY,
						new Object[] { userId },
						new RowMapper<AuthenticateUser>() {
							public AuthenticateUser mapRow(ResultSet rs,
									int rowNum) throws SQLException {
								final AuthenticateUser authenticateUser = new AuthenticateUser();
								// authenticateUser.setEmail(rs.getString(ApplicationConstants.EMAIL));
								authenticateUser.setUserName(rs
										.getString(ApplicationConstants.USERNAME));
								authenticateUser.setPassword(rs
										.getString(ApplicationConstants.PASSWORD));
								authenticateUser.setUserId(rs
										.getInt(ApplicationConstants.USERID));
								return authenticateUser;
							}
						});
			}
		} catch (EmptyResultDataAccessException exception) {
			return ApplicationConstants.FAILURE;
		}
		// userNameFromDb = authenticateUser.getEmail();
		userNameFromDb = authenticateUser.getUserName();
		passwordFromDb = authenticateUser.getPassword();
		struserId = String.valueOf(authenticateUser.getUserId());
		if (null == userNameFromDb) {
			result = ApplicationConstants.FAILURE;
			return result;
		}
		try {
			enryptDecryptpwd = new EncryptDecryptPwd();
			decryptedPassword = enryptDecryptpwd.decrypt(passwordFromDb);
		} catch (NoSuchAlgorithmException e) {
			log.error("Exception in Decryption ", e);
			throw new ScanSeeException(e.getMessage());
		} catch (NoSuchPaddingException e) {
			log.error("Exception in user password Decryption ", e);
			throw new ScanSeeException(e.getMessage());
		} catch (InvalidKeyException e) {
			log.error("Password Decryption Exception  ", e);
			throw new ScanSeeException(e.getMessage());
		} catch (InvalidAlgorithmParameterException e) {
			log.error(" Decryption  Password Exception ", e);
			throw new ScanSeeException(e.getMessage());
		} catch (InvalidKeySpecException e) {
			log.error("user Password Exception in Decryption ", e);
			throw new ScanSeeException(e.getMessage());
		} catch (IllegalBlockSizeException e) {
			log.error("exception Decryption Password   ", e);
			throw new ScanSeeException(e.getMessage());
		} catch (BadPaddingException e) {
			log.error("user password decrypted exception ", e);
			throw new ScanSeeException(e.getMessage());
		} catch (IOException e) {
			log.error("Exception in Decryption ", e);
			throw new ScanSeeException(e.getMessage());
		}
		if (userName != null) {
			if (userName.equals(userNameFromDb)
					&& (passWord.equals(decryptedPassword))) {
				return struserId;
			} else {
				result = ApplicationConstants.FAILURE;
			}
		} else {
			if (passWord.equals(decryptedPassword)) {
				result = ApplicationConstants.SUCCESS;
			} else {

				result = ApplicationConstants.FAILURE;
			}
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return result;
	}

	/**
	 * This method for updating status of pushNotify.
	 * 
	 * @param userId
	 *            in the request.
	 * @param pushNotify
	 *            in the request.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String pushNotifyAlert(int userId, String pushNotify)
			throws ScanSeeException {
		final String methodName = "pushNotifyAlert";
		log.info(ApplicationConstants.METHODSTART + methodName);
		int pushnotifyStatus = 1;
		int pushNotifyVal = 0;
		int firstUseComplete = 0;
		String result = null;
		try {
			if ("yes".equalsIgnoreCase(pushNotify)) {
				pushNotifyVal = 1;
				firstUseComplete = 1;
			}
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_PushNotification");
			final SqlParameterSource scanQueryParams = new MapSqlParameterSource()
					.addValue(ApplicationConstants.USERID, userId)
					.addValue("PushNotify", pushNotifyVal)
					.addValue("FirstUseComplete", firstUseComplete)
					.addValue("Date", Utility.getFormattedDate());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall
					.execute(scanQueryParams);
			pushnotifyStatus = (Integer) resultFromProcedure
					.get(ApplicationConstants.STATUS);
			if (pushnotifyStatus == 0) {
				result = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure
						.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure
						.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED
						+ "usp_PushNotification ..errorNum..." + errorNum
						+ " errorMsg.." + errorMsg);
				result = ApplicationConstants.FAILURE;
			}
		} catch (ParseException e) {
			log.error("exception in pushNotifyAlert" + e);
			throw new ScanSeeException(e.getMessage());
		} catch (DataAccessException e) {
			log.error("exception in pushNotifyAlert" + e);
			throw new ScanSeeException(e.getMessage());
		}

		return result;
	}

	/**
	 * The method to display Main menu based on the media viewed.
	 * 
	 * @param userId
	 *            request parameter
	 * @return displaymainmenu result set
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public DisplayMainMenuResultSet displayMainMenu(DisplayMainMenu displayMainMenuObj) throws ScanSeeException {
		final String methodName = "displayMainMenu";
		log.info(ApplicationConstants.METHODSTART + methodName);
		DisplayMainMenuResultSet displayMainMenuResultSet = null;
		List<DisplayMainMenu> displayMainMenu = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_MainMenuDisplay");
			simpleJdbcCall.returningResultSet("mainMenuDisplayItems", new BeanPropertyRowMapper<DisplayMainMenu>(DisplayMainMenu.class));
			final MapSqlParameterSource displayMainmenuParameters = new MapSqlParameterSource();
			displayMainmenuParameters.addValue("UserID", displayMainMenuObj.getUsrId());
			displayMainmenuParameters.addValue("Latitude", displayMainMenuObj.getLat());
			displayMainmenuParameters.addValue("Longitude", displayMainMenuObj.getLng());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(displayMainmenuParameters);
			displayMainMenu = (List<DisplayMainMenu>) resultFromProcedure.get("mainMenuDisplayItems");

			Integer retailGroupID = (Integer) resultFromProcedure.get("RetailGroupID");
			Integer partnerCount = (Integer) resultFromProcedure.get("RetailAffiliateCount");
			Integer retAffID = (Integer) resultFromProcedure.get("RetailAffiliateID");
			String retAffName = (String) resultFromProcedure.get("RetailAffiliateName");

			if (null != displayMainMenu && !displayMainMenu.isEmpty())
			{
				displayMainMenuResultSet = new DisplayMainMenuResultSet();
				displayMainMenuResultSet.setDisplayMainmenu(displayMainMenu);
				
				if(null != retailGroupID)	{
					displayMainMenuResultSet.setRetGroupID(retailGroupID);
				} else	{
					displayMainMenuResultSet.setRetGroupID(0);
				}
				
				displayMainMenuResultSet.setPartnerCount(partnerCount);
				
				if(null != retAffID)	{
					displayMainMenuResultSet.setRetAffID(retAffID);
				} else 	{
					displayMainMenuResultSet.setRetAffID(0);
				}
				
				displayMainMenuResultSet.setRetAffName(retAffName);
			}
		} catch (DataAccessException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return displayMainMenuResultSet;
	}

	/**
	 * The DAO method to Save User Media info in the database for the given
	 * userID and mediaId.
	 * 
	 * @param userId
	 *            for Save User viewed Media info.
	 * @param mediaId
	 *            for Save User viewed Media info.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return response.
	 */
	@Override
	public String saveUserMediaInfo(Integer userId, Integer mediaId)
			throws ScanSeeException {
		final String methodName = "saveUserMediaInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);

		int saveUserMediaInfoStatus;
		String response = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserMedia");
			simpleJdbcCall.returningResultSet("usermediainfo",
					new BeanPropertyRowMapper<UserMediaInfo>(
							UserMediaInfo.class));
			final SqlParameterSource displayMainmenuParameters = new MapSqlParameterSource()
					.addValue(ApplicationConstants.USERID, userId)
					.addValue("MediaID", mediaId)
					.addValue("Date", Utility.getFormattedDate());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall
					.execute(displayMainmenuParameters);
			saveUserMediaInfoStatus = (Integer) resultFromProcedure
					.get(ApplicationConstants.STATUS);
			if (saveUserMediaInfoStatus == 0) {
				response = ApplicationConstants.SUCCESS;
			} else {
				final Integer errorNum = (Integer) resultFromProcedure
						.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure
						.get(ApplicationConstants.ERRORMESSAGE);
				log.info(ApplicationConstants.ERROROCCURRED
						+ "usp_UserMedia ..errorNum..." + errorNum
						+ "errorMsg.." + errorMsg);
				response = ApplicationConstants.FAILURE;
			}
		} catch (ParseException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception.getMessage());
		} catch (DataAccessException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception.getMessage());
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The DAO method to fetch tutorial media details from the database.
	 * 
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return TutorialMediaResultSet object.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public TutorialMediaResultSet fetchTutorialMediaInfo() throws ScanSeeException {
		final String methodName = "TutorialMediaResultSet";
		log.info(ApplicationConstants.METHODSTART + methodName);
		TutorialMediaResultSet tutorialMediaResultSet = null;
		List<TutorialMedia> tutorialDetails = null;
		List<SectionContent> sectionContentLst = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_fetchTutorialMediaInfo");
			simpleJdbcCall.returningResultSet("fetchTutorialMediaInfo",	new BeanPropertyRowMapper<TutorialMedia>(TutorialMedia.class));
			final MapSqlParameterSource fetchTutMedia = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchTutMedia);
			tutorialDetails = (List<TutorialMedia>) resultFromProcedure.get("fetchTutorialMediaInfo");
			String welcomeVideo = (String) resultFromProcedure.get("WelcomeVideo");
			log.info(ApplicationConstants.METHODSTART + methodName);
			if (null != tutorialDetails && !tutorialDetails.isEmpty()) {
				tutorialMediaResultSet = new TutorialMediaResultSet();
				tutorialMediaResultSet.setTutorialMedia(tutorialDetails);
				tutorialMediaResultSet.setWelcomeVideo(welcomeVideo);
			}
			try {
				simpleJdbcTemplate = new SimpleJdbcTemplate(jdbcTemplate);
				sectionContentLst = simpleJdbcTemplate.query(FirstUseQueries.FETCHFIRSTUSESCREENINFO, new BeanPropertyRowMapper<SectionContent>(SectionContent.class),
						ApplicationConstants.FIRSTUSESCREENNAME);

				if (sectionContentLst != null && !sectionContentLst.isEmpty()) {
					tutorialMediaResultSet.setSectionContent((ArrayList<SectionContent>) sectionContentLst);
				}
			} catch (DataAccessException exception) {
				log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName
						+ exception);
				throw new ScanSeeException(exception);
			}
		} catch (DataAccessException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception.getMessage());

		}

		return tutorialMediaResultSet;
	}

	/**
	 * This method fetches user details for sending password to user email id.
	 * 
	 * @param userName
	 *            request parameter
	 * @return User Details
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public AuthenticateUser fetchUserDetailsforForgotPassword(String userName)
			throws ScanSeeException {
		final String methodName = "fetchUserDetailsforForgotPassword";
		log.info(ApplicationConstants.METHODSTART + methodName);
		AuthenticateUser authenticateUser = null;
		try {
			authenticateUser = this.jdbcTemplate.queryForObject(
					FirstUseQueries.AUTHENTICATEUSEREMAILIDQUERY,
					new Object[] {userName},
					new RowMapper<AuthenticateUser>() {
						public AuthenticateUser mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							final AuthenticateUser authenticateUser = new AuthenticateUser();
							authenticateUser.setEmail(rs
									.getString(ApplicationConstants.EMAIL));
							authenticateUser.setPassword(rs
									.getString(ApplicationConstants.PASSWORD));
							authenticateUser.setUserId(rs
									.getInt(ApplicationConstants.USERID));
							return authenticateUser;
						}
					});
		} catch (EmptyResultDataAccessException exception) {
			return null;
		} catch (DataAccessException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return authenticateUser;
	}

	/**
	 * The DAO method for Authenticate login to the database for the based on
	 * the given userName and password.
	 * 
	 * @param authenticateUserObj
	 *            userID, password and deviceID as request parameter. If any
	 *            exception occurs ScanSeeException will be thrown.
	 * @return AuthenticateUser object.
	 */
	public AuthenticateUser loginAuthentication(
			AuthenticateUser authenticateUserObj) throws ScanSeeException {
		final String methodName = "loginAuthentication";
		log.info(ApplicationConstants.METHODSTART + methodName);
		EncryptDecryptPwd enryptDecryptpwd;
		String encryptedpwd = null;
		Integer checkPwd = null; 
		AuthenticateUser userInfo = null;
		try {
			enryptDecryptpwd = new EncryptDecryptPwd();
			encryptedpwd = enryptDecryptpwd.encrypt(authenticateUserObj
					.getPassword());

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserLogin");
			final MapSqlParameterSource loginCredParams = new MapSqlParameterSource();
			loginCredParams.addValue("UserName", authenticateUserObj.getEmail());
			loginCredParams.addValue(ApplicationConstants.PASSWORD, encryptedpwd);
			loginCredParams.addValue("DeviceID", authenticateUserObj.getDeviceID());
			loginCredParams.addValue("AppVersion", authenticateUserObj.getAppVersion());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(loginCredParams);
			if (null != resultFromProcedure) {
				Integer checkLogin;
				checkLogin = (Integer) resultFromProcedure.get("Login");
				/*
				 * "User Not Found" If user valid, check Password, if password
				 * invalid -> "Invalid Password"
				 * userInfo.setLoginSuccess(false);
				 */
				if (null != checkLogin && checkLogin == -1) {
					userInfo = new AuthenticateUser();
					userInfo.setLoginSuccess(false);
					checkPwd = (Integer) resultFromProcedure.get("InvalidPassword");
					if (checkPwd != null && (checkPwd == 1)) {
						userInfo.setChkPassword(true);
					} else {
						userInfo.setChkPassword(false);
					}
				} else {
					userInfo = new AuthenticateUser();
					userInfo.setLoginSuccess(true);
					final Integer userId = (Integer) resultFromProcedure
							.get(ApplicationConstants.USERID);
					final Integer loginStatus = (Integer) resultFromProcedure
							.get("Result");
					userInfo.setUserId(userId);
					userInfo.setLoginStatus(Integer.toString(loginStatus));

					// checking user has latest app version,If he has older
					// version sending url and message to download new version

					final Boolean addDeviceToUser = (Boolean) resultFromProcedure
							.get("AddDeviceToUser");
					if (addDeviceToUser != null) {
						if (addDeviceToUser) {

							userInfo.setAddDeviceToUser(true);

						} else {
							userInfo.setAddDeviceToUser(false);
						}
					}
				}
			}
		} catch (DataAccessException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception.getMessage());
		} catch (NoSuchAlgorithmException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception.getMessage());
		} catch (NoSuchPaddingException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception.getMessage());
		} catch (InvalidKeyException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception.getMessage());
		} catch (InvalidAlgorithmParameterException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception.getMessage());
		} catch (InvalidKeySpecException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception.getMessage());
		} catch (IllegalBlockSizeException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception.getMessage());
		} catch (BadPaddingException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception.getMessage());
		}
		return userInfo;
	}

	/**
	 * The DAO method for fetching welcome screen details from the database for
	 * the given screenName.
	 * 
	 * @param screenName
	 *            for which user information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return ScreenTextInfo object.
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public ScreenTextInfo fetchStaticScreenDetails(String screenName)
			throws ScanSeeException {
		final String methodName = "fetchStaticScreenDetails in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<SectionContent> sectionContentlst = null;
		ScreenTextInfo screenTextInfoObj = null;
		List<SectionContent> respContentlst = null;
		List<TickerInfo> tickerInfolst = null;
		try

		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetWelcomeScreenContent");
			simpleJdbcCall.returningResultSet("staticScreenDetails",
					new BeanPropertyRowMapper<SectionContent>(
							SectionContent.class));
			final MapSqlParameterSource staticScreenParameters = new MapSqlParameterSource();
			staticScreenParameters.addValue(ApplicationConstants.SCREENNAME,
					screenName);
			final StringBuilder trickerInfo = new StringBuilder();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall
					.execute(staticScreenParameters);
			if (null != resultFromProcedure) {
				if (null != resultFromProcedure
						.get(ApplicationConstants.ERRORNUMBER)) {
					final String errorMsg = (String) resultFromProcedure
							.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer
							.toString((Integer) resultFromProcedure
									.get(ApplicationConstants.ERRORNUMBER));
					log.error(
							"Error occurred in usp_GetScreenStaticContent Store Procedure error number: {}and error message: {}",
							errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				} else {
					sectionContentlst = (List<SectionContent>) resultFromProcedure
							.get("staticScreenDetails");
					if (sectionContentlst != null
							&& !sectionContentlst.isEmpty()) {
						screenTextInfoObj = new ScreenTextInfo();
						tickerInfolst = new ArrayList<TickerInfo>();
						respContentlst = new ArrayList<SectionContent>();
						for (SectionContent sectionContent : sectionContentlst) {
							if (sectionContent
									.getConfigurationType()
									.equalsIgnoreCase(
											ApplicationConstants.FIRSTUSESCREENWELCOMEVIDEO)) {
								final SectionContent secContent = sectionContent;
								secContent.setWelcomeVideo(sectionContent
										.getSectionContent());
								respContentlst.add(secContent);
								secContent.setSectionContent(null);
							} else if (sectionContent
									.getConfigurationType()
									.equalsIgnoreCase(
											ApplicationConstants.FIRSTUSESCREENWELCOMEIMAGE)) {
								final SectionContent secContent = sectionContent;
								secContent.setWelcomeImage(sectionContent
										.getSectionContent());
								respContentlst.add(secContent);
								secContent.setSectionContent(null);
							} else if (sectionContent
									.getConfigurationType()
									.trim()
									.equalsIgnoreCase(
											ApplicationConstants.FIRSTUSESCREENTICKER)) {
								final TickerInfo tickerInfo = new TickerInfo();
								tickerInfo.setTicker(sectionContent
										.getSectionContent());
								tickerInfolst.add(tickerInfo);
							} else if (sectionContent
									.getConfigurationType()
									.equalsIgnoreCase(
											ApplicationConstants.FIRSTUSESCREENSTATICSCREENCONTENT)) {
								respContentlst.add(sectionContent);
							}
						}

						final SectionContent secContent = new SectionContent();
						secContent
								.setConfigurationType(ApplicationConstants.FIRSTUSESCREENTICKER);
						if (null != tickerInfolst && !tickerInfolst.isEmpty()) {
							secContent
									.setTickerInfolst((ArrayList<TickerInfo>) tickerInfolst);
						} else {
							trickerInfo
									.append(ApplicationConstants.NOTAPPLICABLE);
						}
						respContentlst.add(secContent);
						screenTextInfoObj
								.setSectionContent((ArrayList<SectionContent>) respContentlst);
					}
				}
			}
		}

		catch (DataAccessException e) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return screenTextInfoObj;
	}

	/**
	 * The DAO method for fetching emailDeclaimNotification details from the
	 * database for the given configurationType.
	 * 
	 * @param configurationType
	 *            for which emailDeclaimNotification information need to be
	 *            fetched.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return screenContent List object.
	 */
	@Override
	public List<EmailDecline> emailDeclaimNotification(String configurationType)
			throws ScanSeeException {
		final String methodName = "emailDeclaimNotification";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<EmailDecline> screenContent = null;
		try {
			screenContent = this.jdbcTemplate.query(
					FirstUseQueries.FETCHDATAFORFORGOTPASSWORD,
					new Object[] { configurationType },
					new RowMapper<EmailDecline>() {
						public EmailDecline mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							final EmailDecline emailDecline = new EmailDecline();
							emailDecline.setConfigurationType(rs
									.getString("ConfigurationType"));
							emailDecline.setScreenName(rs
									.getString(ApplicationConstants.SCREENNAME));
							emailDecline.setScreenContent(rs
									.getString("ScreenContent"));
							return emailDecline;
						}

					});
		} catch (EmptyResultDataAccessException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception);
		} catch (DataAccessException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return screenContent;
	}

	/**
	 * The DAO method for fetches details for sending email. Based on the given
	 * configurationType.
	 * 
	 * @param configurationType
	 *            as request parameter.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return list of from address and subject.
	 */
	@Override
	public List<EmailDecline> fetchFrmAddressAndSubjectforForgotPassword(
			String configurationType) throws ScanSeeException {
		final String methodName = "fetchFrmAddressAndSubjectforForgotPassword";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<EmailDecline> screenContent = null;
		try {
			screenContent = this.jdbcTemplate.query(
					FirstUseQueries.FETCHDATAFORFORGOTPASSWORD,
					new Object[] { configurationType },
					new RowMapper<EmailDecline>() {
						public EmailDecline mapRow(ResultSet rs, int rowNum)
								throws SQLException {
							final EmailDecline emailDecline = new EmailDecline();
							emailDecline.setConfigurationType(rs
									.getString("ConfigurationType"));
							emailDecline.setScreenName(rs
									.getString(ApplicationConstants.SCREENNAME));
							emailDecline.setScreenContent(rs
									.getString("ScreenContent"));
							return emailDecline;
						}
					});
		}

		catch (EmptyResultDataAccessException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception);
		} catch (DataAccessException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,
					exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return screenContent;
	}

	/**
	 * This method insert the user registration details.
	 * 
	 * @param userRegistrationInfo
	 *            instance of UserRegistrationInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public AuthenticateUser registerFaceBookUser(
			UserRegistrationInfo userRegistrationInfo) throws ScanSeeException {
		final String methodName = "registerFaceBookUser";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		Integer responseFromProc = null;
		String response = null;
		String faceBook = null;
		AuthenticateUser authenticateUser = null;
		final String loginValue = userRegistrationInfo.getLogin();

		try {
			if ("faceBook".equals(loginValue)) {

				faceBook = "1";
				simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
				simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
				simpleJdbcCall.withProcedureName("usp_UserFaceBookLogIn");
				final MapSqlParameterSource userRegistrationParameter = new MapSqlParameterSource();
				userRegistrationParameter.addValue(
						ApplicationConstants.USERNAME,
						userRegistrationInfo.getEmail());
				userRegistrationParameter.addValue(
						ApplicationConstants.FACEBOOK, faceBook);
				userRegistrationParameter.addValue("DateCreated",
						Utility.getFormattedDate());
				userRegistrationParameter.addValue("DeviceID",
						userRegistrationInfo.getDeviceID());
				userRegistrationParameter.addValue("UserLongitude",
						userRegistrationInfo.getUserLongitude());
				userRegistrationParameter.addValue("UserLatitude",
						userRegistrationInfo.getUserLatitude());
				resultFromProcedure = simpleJdbcCall
						.execute(userRegistrationParameter);
				/**
				 * For getting response from stored procedure ,sp return Result
				 * as response success as 0 failure as 1 it will return list
				 * resultset so casting into map and getting Result param value
				 */
				responseFromProc = (Integer) resultFromProcedure
						.get(ApplicationConstants.STATUS);
				authenticateUser = new AuthenticateUser();

				if (null != responseFromProc
						&& responseFromProc.intValue() == 0) {
					response = ((Integer) resultFromProcedure
							.get("FirstUserID")).toString();
					if (null != response) {
						Integer id = Integer.parseInt(response);

						authenticateUser.setUserId(id);

						final Boolean isExistingUser = (Boolean) resultFromProcedure
								.get("ExistingUser");
						if (isExistingUser != null) {
							if (isExistingUser) {
								authenticateUser.setExistingUser(true);

							} else {
								authenticateUser.setExistingUser(false);
							}
						}

					}

				}

				/*
				 * Checking the user exist or logging for first time
				 */
				/*
				 * else if (null != responseFromProc && null !=
				 * resultFromProcedure.get("ExistingUser")) { response =
				 * resultFromProcedure.get("ExistingUser").toString(); }
				 */

				else {
					final Integer errorNum = (Integer) resultFromProcedure
							.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure
							.get(ApplicationConstants.ERRORMESSAGE);
					log.info("Error Occured in registerUser method ..errorNum..."
							+ errorNum + "errorMsg  .." + errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}
		} catch (DataAccessException exception) {
			log.error("Exception occurred in registerUser ",
					ApplicationConstants.DATAACCESSEXCEPTIONCODE, exception);
			throw new ScanSeeException(exception);
		} catch (ParseException exception) {
			log.error("Exception occurred in registerUser", exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return authenticateUser;
	}

	/**
	 * The DAO method for fetching welcome screen details from the database for
	 * the given screenName.
	 * 
	 * @param screenName
	 *            for which user information need to be fetched.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return ScreenTextInfo object.
	 */
	@SuppressWarnings({ "unchecked" })
	@Override
	public ScreenTextInfo GeneralStaticScreenContent(String screenName)
			throws ScanSeeException {
		final String methodName = "GeneralStaticScreenContent in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<SectionContent> sectionContentlst = null;
		ScreenTextInfo screenTextInfoObj = null;

		try

		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetScreenStaticContent");
			simpleJdbcCall.returningResultSet("staticScreenDetails",
					new BeanPropertyRowMapper<SectionContent>(
							SectionContent.class));
			final MapSqlParameterSource staticScreenParameters = new MapSqlParameterSource();
			staticScreenParameters.addValue(ApplicationConstants.SCREENNAME,
					screenName);
			final StringBuilder trickerInfo = new StringBuilder();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall
					.execute(staticScreenParameters);
			if (null != resultFromProcedure) {
				if (null != resultFromProcedure
						.get(ApplicationConstants.ERRORNUMBER)) {
					final String errorMsg = (String) resultFromProcedure
							.get(ApplicationConstants.ERRORMESSAGE);
					final String errorNum = Integer
							.toString((Integer) resultFromProcedure
									.get(ApplicationConstants.ERRORNUMBER));
					log.error(
							"Error occurred in usp_GetScreenStaticContent Store Procedure error number: {}and error message: {}",
							errorNum, errorMsg);
					throw new ScanSeeException(errorMsg);
				} else {
					sectionContentlst = (List<SectionContent>) resultFromProcedure
							.get("staticScreenDetails");
					if (sectionContentlst != null
							&& !sectionContentlst.isEmpty()) {
						screenTextInfoObj = new ScreenTextInfo();
						screenTextInfoObj
								.setSectionContent((ArrayList<SectionContent>) sectionContentlst);
					}

				}
			}

		} catch (DataAccessException e) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, e);
			throw new ScanSeeException(e);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return screenTextInfoObj;
	}

	/**
	 * This method insert the user push notification details.
	 * 
	 * @param userRegistrationInfo
	 *            instance of UserRegistrationInfo.
	 * @return success or failure depending upon the result of operation.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String registerPushNotification(
			UserRegistrationInfo userRegistrationInfo) throws ScanSeeException {
		final String methodName = "registerPushNotification";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		String responseFromProc = null;
		Integer alreadyExists = null;
		Integer fromProc = null;

		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserTokenInsertion");
			final MapSqlParameterSource userRegistrationParameter = new MapSqlParameterSource();
			userRegistrationParameter.addValue("DeviceID",
					userRegistrationInfo.getDeviceID());
			// suserRegistrationParameter.addValue("DateCreated",
			// Utility.getFormattedDate());
			userRegistrationParameter.addValue("UserToken",
					userRegistrationInfo.getUserTokenID());
			userRegistrationParameter.addValue("FaceBookAuthenticatedUser", 0);
			resultFromProcedure = simpleJdbcCall
					.execute(userRegistrationParameter);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0) {
				responseFromProc = ApplicationConstants.SUCCESS;
				alreadyExists = (Integer) resultFromProcedure.get("Result");

				if (alreadyExists != null) {
					if (alreadyExists == -1) {
						responseFromProc = "Exists";
					}
				}
			} else {
				responseFromProc = ApplicationConstants.FAILURE;
			}
			if (null != resultFromProcedure
					.get(ApplicationConstants.ERRORNUMBER)) {
				final Integer errorNum = (Integer) resultFromProcedure
						.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure
						.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in removeHDProduct method ..errorNum..."
						+ errorNum + "errorMsg.." + errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		} catch (DataAccessException exception) {
			log.error("Exception occurred in registerUser",
					ApplicationConstants.DATAACCESSEXCEPTIONCODE, exception);
			throw new ScanSeeException(exception);
		}
		return responseFromProc;
	}

	/**
	 * To update app version.
	 * 
	 * @param userRegistrationInfo
	 *            containing user information
	 * @return String with SUCCESS or FAILURE
	 * @throws ScanSeeException
	 *             The exception are cought and a ScanSee Exception defined for
	 *             the application is thrown whicj is caught in Controller layer
	 */
	@Override
	public String updateAppVersion(UserRegistrationInfo userRegistrationInfo)
			throws ScanSeeException {
		final String methodName = "updateAppVersion";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		String responseFromProc = null;
		Integer fromProc = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UpdateUserDeviceAppVersion");
			final MapSqlParameterSource appVersionUpdateParameters = new MapSqlParameterSource();
			appVersionUpdateParameters.addValue("DeviceID",
					userRegistrationInfo.getDeviceID());
			appVersionUpdateParameters.addValue("AppVersion",
					userRegistrationInfo.getAppVersion());

			resultFromProcedure = simpleJdbcCall
					.execute(appVersionUpdateParameters);

			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0) {
				responseFromProc = ApplicationConstants.SUCCESS;

			} else {
				responseFromProc = ApplicationConstants.FAILURE;
			}
			if (null != resultFromProcedure
					.get(ApplicationConstants.ERRORNUMBER)) {
				final Integer errorNum = (Integer) resultFromProcedure
						.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure
						.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in updateAppVersion method ..errorNum..."
						+ errorNum + "errorMsg.." + errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		} catch (DataAccessException exception) {
			log.error("Exception occurred in updateAppVersion",
					ApplicationConstants.DATAACCESSEXCEPTIONCODE, exception);
			throw new ScanSeeException(exception);
		}
		return responseFromProc;
	}

	/**
	 * 
	 * @param userRegistrationInfo
	 *            contains userID, deviceID and app version
	 * @return String with SUCCESS or FAILURE
	 * @throws ScanSeeException
	 *             The exception are cought and a ScanSee Exception defined for
	 *             the application is thrown whicj is caught in Controller layer
	 */
	@Override
	public String addDeviceToUser(UserRegistrationInfo userRegistrationInfo)
			throws ScanSeeException {
		final String methodName = "addDeviceToUser";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Map<String, Object> resultFromProcedure = null;
		String responseFromProc = null;
		Integer fromProc = null;

		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_AddDeviceToUser");
			final MapSqlParameterSource appVersionUpdateParameters = new MapSqlParameterSource();
			appVersionUpdateParameters.addValue(ApplicationConstants.USERID,
					userRegistrationInfo.getUserId());
			appVersionUpdateParameters.addValue("DeviceID",
					userRegistrationInfo.getDeviceID());
			appVersionUpdateParameters.addValue("AppVersion",
					userRegistrationInfo.getAppVersion());

			resultFromProcedure = simpleJdbcCall
					.execute(appVersionUpdateParameters);

			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0) {
				responseFromProc = ApplicationConstants.SUCCESS;

			} else {
				responseFromProc = ApplicationConstants.FAILURE;
			}
			if (null != resultFromProcedure
					.get(ApplicationConstants.ERRORNUMBER)) {
				final Integer errorNum = (Integer) resultFromProcedure
						.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure
						.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in updateAppVersion method ..errorNum..."
						+ errorNum + "errorMsg.." + errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		} catch (DataAccessException exception) {
			log.error("Exception occurred in updateAppVersion",
					ApplicationConstants.DATAACCESSEXCEPTIONCODE, exception);
			throw new ScanSeeException(exception);
		}
		return responseFromProc;
	}

	/**
	 * For user tracking. To be executed when user selects the modules
	 * 
	 * @return SUCCESS or FAILURE response
	 * @throws ScanSeeException
	 */
	@Override
	public Integer userTrackingModuleClick(UserTrackingData userTrackingData) throws ScanSeeException {
		final String methodName = "userTrackingModuleClick in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		Integer mainMenuID;
		
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserTrackingMainMenuCreation");

			final MapSqlParameterSource userTrackingDetails = new MapSqlParameterSource();
			userTrackingDetails.addValue(ApplicationConstants.USERID, userTrackingData.getUserID());
			userTrackingDetails.addValue(ApplicationConstants.MODULEID, userTrackingData.getModuleID());
			userTrackingDetails.addValue("Latitude", userTrackingData.getLatitude());
			userTrackingDetails.addValue("Longitude", userTrackingData.getLongitude());
			userTrackingDetails.addValue("Postalcode", userTrackingData.getPostalCode());
			
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(userTrackingDetails);
			
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				log.info("Error Occured in usp_UserTrackingMainMenuCreation SP");
				throw new ScanSeeException();
			}
			else  {
				mainMenuID =  (Integer) resultFromProcedure.get("MainMenuID");
			}
		}
		catch (DataAccessException exception)
		{
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception);
		}
		return mainMenuID;
	}

	
	/**
	 * The DAOImpl method to get started images information from the database.
	 * 
	 * @throws ScanSeeException If any exception occurs ScanSeeException will be thrown.
	 * @return TutorialMediaResultSet object.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public TutorialMediaResultSet getStartedImageInfo() throws ScanSeeException {
		final String methodName = "getStartedImageInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		TutorialMediaResultSet tutorialMediaResultSet = null;
		List<TutorialMedia> arTutorialMediaParam = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_FetchTutorialImages");
			simpleJdbcCall.returningResultSet("fetchTutorialImagesInfo", new BeanPropertyRowMapper<TutorialMedia>(TutorialMedia.class));
			final MapSqlParameterSource fetchTutMedia = new MapSqlParameterSource();
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchTutMedia);
			arTutorialMediaParam = (List<TutorialMedia>) resultFromProcedure.get("fetchTutorialImagesInfo");
			if (null != arTutorialMediaParam && !arTutorialMediaParam.isEmpty()) {
				tutorialMediaResultSet = new TutorialMediaResultSet();
				tutorialMediaResultSet.setTutorialMedia(arTutorialMediaParam);
			}
		} catch (DataAccessException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,exception);
			throw new ScanSeeException(exception.getMessage());
		}
		return tutorialMediaResultSet;
	}

	/**
	 * Method to fetch contact scansee email address from database.
	 * 
	 * @return String containing email address
	 */
	@Override
	public String contactScanSeeByEmail() throws ScanSeeException {
		final String methodName = "contactScanSeeByEmail";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<AppConfiguration> appConfigList;
		String contactScanSee = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GetScreenContent");
			simpleJdbcCall.returningResultSet("getScreenContent", new BeanPropertyRowMapper<AppConfiguration>(AppConfiguration.class));
			final MapSqlParameterSource getScreenContent = new MapSqlParameterSource();
			getScreenContent.addValue("ConfigurationType", "Decline Email");
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(getScreenContent);
			appConfigList = (List<AppConfiguration>) resultFromProcedure.get("getScreenContent");
			if (null != appConfigList && !appConfigList.isEmpty()) {
				for(AppConfiguration appConfig : appConfigList)	{
					if(appConfig.getScreenName().equalsIgnoreCase("AdminEmailId"))	{
						contactScanSee = appConfig.getScreenContent();
						break;
					}
				}
			}
		} catch (DataAccessException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName,exception);
			throw new ScanSeeException(exception.getMessage());
		}
		return contactScanSee;
	}
}
