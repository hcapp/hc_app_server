package com.scansee.firstuse.query;

/**
 * For first use module queries.
 * 
 * @author shyamsundara_hm
 */
public class FirstUseQueries
{
	
    
	/**
	 * for user exists query.
	 */
	public static final String DOESUSEREXISTSQUERY = "select Email  from [Users] where Email = ?";
	/**
	 * For fetching retailers.
	 */

	public static final String FETCHRETAILERSINFOQUERY = "select RetailID,RetailName from Retailer";
	/**
	 * for Checking valid and invalid user.
	 */

	//public static final String ISVALIDUSERQUERY = "select Email,password,userId from [Users] where Email = ?";
	public static final String ISVALIDUSERQUERY = "select UserName,password,userId from [Users] where UserName = ?";

	/**
	 * for updateUserInfoQuery.
	 */
	public static final String UPDATEUSERINFOQUERY = "update  [Users]  set" + " [Users].FirstName=?,[Users].Lastname=? ,"
			+ " [Users].Address1=?,[Users].Address2=?," + " [Users].Address3=?,[Users].Address4=?," + " [Users].City=?,[Users].State=?,"
			+ " [Users].PostalCode=?,[Users].CountryID=?," + " [Users].MobilePhone=?,[Users].Email=?," + " [Users].Password=?,[Users].FieldAgent=?,"
			+ " [Users].AverageFieldAgentRating=?,[Users].DateCreated=?," + " [Users].CreateUserID=?,[Users].DateModified=?,"
			+ " [Users].ModifyUserID=?, where UserID= ?";
	/**
	 * for UPDATEDEMOGRAPHYDETAILSQUERY.
	 */

	public static final String UPDATEDEMOGRAPHYDETAILSQUERY = "update  [UserDemographic]  set"
			+ " [UserDemographic].Gender=?,[UserDemographic].DOB=? ," + " [UserDemographic].HouseholdIncome=?,[UserDemographic].NumberofChildern=?,"
			+ " [UserDemographic].NumberofPets=?,[UserDemographic].OwnPrimaryResidence=?," + "Select UserID from [Users] where UserID = ?";

	/**
	 * for fetchDemographyDetailsQuery.
	 */
	public static final String FETCHDEMOGRAPHYDETAILSQUERY = "select * from [UserDemographic] where UserId =?";

	/**
	 * for fetchUserInfoQuery.
	 */
	public static final String FETCHUSERINFOQUERY = "Select u.* , c.CountryName from Users u join [CountryCode] c on u.CountryID = c.CountryID where u.UserID= ?";

	/**
	 * FOR fetchUserPreferenceQuery.
	 */
	public static final String FETCHUSERPREFERENCEQUERY = " select up.LocaleRadius locationRadius,up.ScannerSilent, up.DisplayCoupons,up.DisplayLoyaltyRewards,up.DisplayFieldAgent,up.FieldAgentRadius,up.SavingsActivated "
			+ ", up.SleepStatus  sleepStatus, up.DisplayRebates displayRebates from [Users] u join [UserPreference] up on u.UserID = up.UserID where u.UserID= ?";

	/**
	 * FOR fetchUserPayInfoQuery.
	 */
	public static final String FETCHUSERPAYINFOQUERY = " Select uf.UseDefaultAddress,uf.PayCity,uf.PayState,uf.PayPostalCode,uf.PayAddress1,uf.PayAddress2 "
			+ "  from [UserPayInfo] uf join [Users] u on  uf.UserID = u.UserID join [PaymentInterval] pay on uf.PaymentIntervalID = pay.PaymentIntervalID "
			+ "  where u.UserID=?";

	/**
	 * for fetchUserRetailPrefeencQuery.
	 */
	public static final String FETCHUSERRETAILPREFEENCEQUERY = "select ur.RetailID,rt.RetailName,u.Email from [UserRetailPreference] ur join [Users] u"
			+ " on ur.UserID = u.UserID join [Retailer] rt on rt.RetailID = ur.RetailID  where u.UserID=?";

	/**
	 * for updatePushNotify.
	 */
	public static final String UPDATEPUSHNOTIFY = "update Users set PushNotify=? where UserID=?";
	/**
	 * FOR authenticateUserQuery.
	 */

//	public static final String AUTHENTICATEUSERQUERY = "select Email,password,userId from [Users] where userId=?";
	public static final String AUTHENTICATEUSERQUERY = "select UserName,password,userId from [Users] where userId=?";

	/**
	 * FOR fetchMediaInfo.
	 */
	public static final String FETCHMEDIAINFO = "select MediaID, MediaName,ModuleID, MediaShortDescription from [Media]";
	/**
	 * FOR firstUseQuery.
	 */

	public static final String FIRSTUSEQUERY = "select DeviceID,FirstUseComplete  from [Users] where DeviceID = ?";

	/**
	 * FOR authenticateUserEmailIdQuery.
	 */
	public static final String AUTHENTICATEUSEREMAILIDQUERY = "select Email,password,userId from [Users] where BINARY_CHECKSUM(userName) = BINARY_CHECKSUM(?)";
	/**
	 * for fetch for forgot password.
	 */
	
	public static final String FETCHDATAFORFORGOTPASSWORD = "select ConfigurationType,ScreenName,ScreenContent from AppConfiguration where ConfigurationType = ? and Active = 1";
	/**
	 * for fetch for First tutorial screen info.
	 */
	public static final String FETCHFIRSTUSESCREENINFO = "SELECT * FROM AppConfiguration WHERE ScreenName = ?  ORDER BY Section";
	/**
	 * for FETCHFIRSTUSESCREENTICKERINFO.
	 */
	
	public static final String FETCHFIRSTUSESCREENTICKERINFO = "SELECT ScreenContent sectionContent, Section sectionNumber, ConfigurationType FROM AppConfiguration WHERE ScreenName = 'Welcome_Screen' AND ConfigurationType = 'Ticker' AND Active = 1 ORDER BY  Section ";
    
	/**
	 * FOR check user has email or not.
	 */

//	public static final String AUTHENTICATEUSERQUERY = "select Email,password,userId from [Users] where userId=?";
	public static final String AUHTENTICATEUSERMAILQUERY = "select Email,password,userId from [Users] where userId=?";
	
	/**
	 * for constructor.
	 */
	private FirstUseQueries()
	{
		
	}
	
}
