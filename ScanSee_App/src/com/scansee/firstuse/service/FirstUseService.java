package com.scansee.firstuse.service;

import com.scansee.common.exception.ScanSeeException;

/**
 *The FirstUseService Interface. {@link ImplementedBy}
 * {@link FirstUseServiceImpl}
 * 
 * @author shyamsundara_hm
 */
public interface FirstUseService
{

	
	
	/**
	 *The service method for Validating User. This method checks for mandatory
	 * fields, if any mandatory fields are not available returns Insufficient
	 * Data error message else calls the DAO method..
	 * 
	 * @param xml
	 *            The input xml.
	 * @return XML containing success or failure in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */


	String isValidUser(String xml) throws ScanSeeException;


	
	
	/**
	 * The method for ProcessFirstUse. Calls the XStreams helper class methods
	 * for parsing.
	 * 
	 * @param inputXml
	 *            the request xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	
	String processFirstUSe(String inputXml) throws ScanSeeException;


	
	
	
	/**
	 *The service method for updating status of pushNotify. This method checks for mandatory
	 * fields, if any mandatory fields are not available returns Insufficient
	 * Data error message else calls the DAO method..
	 * @param userId
	 *            of the user
	 * @param pushNotify
	 *            are the request parameters
	 *            
	 * @return XML containing success or failure in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	
	
	
	String pushNotifyAlert(Integer userId, String pushNotify) throws ScanSeeException;

	
	
	
	/**
	 *The service method for Displaying Main Menu. This method checks for mandatory
	 * fields, if any mandatory fields are not available returns Insufficient
	 * Data error message else calls the DAO method..
	 * 
	 * @param xml
	 *            of the user.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	
	String displayMainMenu(String xml) throws ScanSeeException;

	
	/**
	 *The service method to save user media information. This method validates the
	 * userId, and mediaId if it is valid it will call the DAO method.
	 * 
	 * @param xml
	 *            of the user.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	
	String saveUserMediaInfo(String xml) throws ScanSeeException;

	/**
     *The service method for fetching tutorial media details.
     *@param userId
	 *            of the user.

     * @return XML containing user information in the response.
     * @throws ScanSeeException
     *             If any exception occurs ScanSeeException will be thrown.
     */

	String fetchTutorialMediaInfo(Integer userId) throws ScanSeeException;

	/**
	 * This method fetches user details and sends password to user email id.This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param emailId
	 *            request parameter
	 * @return The Account Information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String forgotPassword(String emailId) throws ScanSeeException;

	
	   /**
     *The service method for fetching welcome screen details. This method checks for mandatory
	 * fields, if any mandatory fields are not available returns Insufficient
	 * Data error message else calls the DAO method..
	 * 
      * @param screenName
     *            of the user.
     * @return XML containing user information in the response.
     * @throws ScanSeeException
     *             If any exception occurs ScanSeeException will be thrown.
     */

	String fetchStaticScreenInfo(String screenName) throws ScanSeeException;

		

    /**
     *The service method for fetching welcome screen details. This method checks for mandatory
	 * fields, if any mandatory fields are not available returns Insufficient
	 * Data error message else calls the DAO method..
	 * 
     * @param xml
     *       the request parameter   
     *           
     * @return XML containing success or failure in the response.
     * @throws ScanSeeException
     *             If any exception occurs ScanSeeException will be thrown.
     */
	
	String emailNotification(String xml) throws ScanSeeException;
	
	
	/**
	 * The method for processFaceBookUser. Calls the XStreams helper class methods
	 * for parsing.
	 * 
	 * @param inputXml
	 *            the request xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	
	String processFaceBookUser(String inputXml) throws ScanSeeException;

	  /**
     *The service method for fetching welcome screen details. This method checks for mandatory
	 * fields, if any mandatory fields are not available returns Insufficient
	 * Data error message else calls the DAO method..
	 * 
     * @param screenName
     *            of the user.
     * @return XML containing user information in the response.
     * @throws ScanSeeException
     *             If any exception occurs ScanSeeException will be thrown.
     */

	String fetchGeneralStaticScreenInfo(String screenName) throws ScanSeeException;
	
	/**
	 * The method for push notification. Calls the XStreams helper class methods
	 * for parsing.
	 * 
	 * @param inputXml
	 *            the request xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	
	String registerPushNotification(String inputXml) throws ScanSeeException;

	/**
	 * To update app version.
	 * 
	 * @param xml the request xml
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String updateAppVersion(String xml) throws ScanSeeException;

	/**
	 * 
	 * @param xml the request xml
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String addDeviceToUser(String xml) throws ScanSeeException;
	
	/**
	 * For user tracking. To be called after user selecting module.
	 * 
	 * @param xml
	 * @return xml containing SUCCESS or FAILUR response
	 * @throws ScanSeeException
	 */
	String userTrackingModuleClick(String xml) throws ScanSeeException;
	
	/**
     * The service method to get started images information.
     * @param userId as request parameter.
     * @return XML containing user information in the response.
     * @throws ScanSeeException If any exception occurs ScanSeeException will be thrown.
     */
	String getStartedImageInfo(Integer userId) throws ScanSeeException;
	
	/**
	 * This method to contact ScanSee via email.
	 * @param xml as input request.
	 * @return xml string containing SUCCESS or FAILURE.
	 */
	String contactScanSeeByEmail(String xml) throws ScanSeeException;
}
