package com.scansee.firstuse.service;

import static com.scansee.common.util.Utility.isEmpty;
import static com.scansee.common.util.Utility.isEmptyOrNullString;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.email.EmailComponent;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.helper.XstreamParserHelper;
import com.scansee.common.pojos.AppConfiguration;
import com.scansee.common.pojos.AuthenticateUser;
import com.scansee.common.pojos.DisplayMainMenu;
import com.scansee.common.pojos.DisplayMainMenuResultSet;
import com.scansee.common.pojos.EmailDecline;
import com.scansee.common.pojos.ScreenTextInfo;
import com.scansee.common.pojos.TutorialMediaResultSet;
import com.scansee.common.pojos.UserMediaInfo;
import com.scansee.common.pojos.UserRegistrationInfo;
import com.scansee.common.util.EncryptDecryptPwd;
import com.scansee.common.util.Utility;
import com.scansee.firstuse.dao.FirstUseDAO;
import com.scansee.ratereview.dao.RateReviewDAO;
import com.scansee.shoppinglist.dao.ShoppingListDAO;

/**
 * The class implements FirstUseService interface and has methods for
 * ProcessFirstUse and UserValidation. The methods of this class are called from
 * the FirstUse Controller Layer. The methods contain call to the FirstUse DAO
 * layer.
 * 
 * @author dileepa_cc
 */

public class FirstUseServiceImpl implements FirstUseService
{

	/**
	 * Getting the logger instance.
	 */

	private static final Logger log = LoggerFactory.getLogger(FirstUseServiceImpl.class.getName());

	/**
	 * Instance variable for FirstUseDAO.
	 */

	private FirstUseDAO firstUseDao;

	/**
	 * Instance variable for Shopping list DAO instance.
	 */

	private ShoppingListDAO shoppingListDao;
	
	private RateReviewDAO rateReviewDao;

	/**
	 * Setter method for FirstUseDAO.
	 * 
	 * @param firstUseDao
	 *            Instance of type FirstUseDAO.
	 */

	public void setFirstUseDao(FirstUseDAO firstUseDao)
	{
		this.firstUseDao = firstUseDao;
	}

	/**
	 * sets the ShoppingListDAO DAO.
	 * 
	 * @param shoppingListDao
	 *            The instance for ShoppingListDAO
	 */
	public void setShoppingListDao(ShoppingListDAO shoppingListDao)
	{
		this.shoppingListDao = shoppingListDao;
	}
	
	public void setRateReviewDao(RateReviewDAO rateReviewDao)
	{
		this.rateReviewDao = rateReviewDao;
	}

	/**
	 * The method for ProcessFirstUse. Calls the XStreams helper class methods
	 * for parsing.
	 * 
	 * @param inputXml
	 *            the request xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String processFirstUSe(final String inputXml) throws ScanSeeException
	{
		final String methodName = "processFirstUSe";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String strEmail = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserRegistrationInfo userRegistrationInfo = (UserRegistrationInfo) streamHelper.parseXmlToObject(inputXml);
		if (null != userRegistrationInfo)
		{
			strEmail = userRegistrationInfo.getEmail();
			final String pwd = userRegistrationInfo.getPassword();
			if (isEmptyOrNullString(strEmail) || isEmptyOrNullString(pwd) || isEmptyOrNullString(userRegistrationInfo.getDeviceID())
					|| isEmptyOrNullString(userRegistrationInfo.getAppVersion()))
			{
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
				return response;
			} else if (strEmail != null) {
				boolean isValid = Utility.validateEmailId(userRegistrationInfo.getEmail());
				if (!isValid) {
					return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDEMAILADDRESSTEXT);
				}
			}
			response = firstUseDao.registerUser(userRegistrationInfo);
			if (null != response && !response.equals(ApplicationConstants.FAILURE))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.AFTERREGISTRATIONTEXT,
						ApplicationConstants.ELEMENTNAME, response);
			} else {
				response = Utility.formResponseXml(ApplicationConstants.DUPLICATEUSER, ApplicationConstants.DUPLICATEEMAILTEXT);
			}
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method for Validating User. This method checks for mandatory
	 * fields, if any mandatory fields are not available returns Insufficient
	 * Data error message else calls the DAO method..
	 * 
	 * @param xml
	 *            The input xml.
	 * @return XML containing success or failure in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@Override
	public String isValidUser(String xml) throws ScanSeeException
	{
		final String methodName = "isValidUser";
		log.info(ApplicationConstants.METHODSTART + methodName);
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		String response = null;
		AuthenticateUser daoResponse = null;
		final AuthenticateUser authenticateUser = (AuthenticateUser) streamHelper.parseXmlToObject(xml);
		if (isEmptyOrNullString(authenticateUser.getEmail()) || isEmptyOrNullString(authenticateUser.getPassword())
				|| isEmptyOrNullString(authenticateUser.getDeviceID()))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		} else {
			/*if (authenticateUser.getEmail() != null) {
				boolean isValid = Utility.validateEmailId(authenticateUser.getEmail());
				if (!isValid) {
					return Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDEMAILADDRESSTEXT);
				}
			}*/
			daoResponse = firstUseDao.loginAuthentication(authenticateUser);
			if (null != daoResponse && daoResponse.isLoginSuccess() != null && daoResponse.isLoginSuccess() == true)
			{
				final Map<String, String> resultTags = new HashMap<String, String>();
				final Integer userid = daoResponse.getUserId();
				resultTags.put(ApplicationConstants.ELEMENTNAME, Integer.toString(userid));
				resultTags.put(ApplicationConstants.LOGINSTATUS, daoResponse.getLoginStatus());
				if (daoResponse.getAddDeviceToUser() != null)
				{
					resultTags.put(ApplicationConstants.ADDDEVICETOUSER, Boolean.toString(daoResponse.getAddDeviceToUser()));
				}
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.VALIDUSER, resultTags);
			} else {
				if (daoResponse.getChkPassword() != null && daoResponse.getChkPassword() == true) {
					response = Utility.formResponseXml(ApplicationConstants.INVALIDUSERNAMEORPASSWORD, ApplicationConstants.INVALIDPASSWORDTEXT);
				} else {
					response = Utility.formResponseXml(ApplicationConstants.INVALIDUSERNAMEORPASSWORD, ApplicationConstants.EMAILIDNOTFOUND);
				}
			}
		}
			log.info(ApplicationConstants.METHODEND + methodName);
			return response;
		}

	/**
	 * The service method for updating status of pushNotify. This method checks
	 * for mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method..
	 * 
	 * @param userId
	 *            of the user
	 * @param pushNotify
	 *            are the request parameters
	 * @return XML containing success or failure in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@Override
	public String pushNotifyAlert(Integer userId, String pushNotify) throws ScanSeeException
	{
		String response = null;
		final String methodName = "pushNotifyAlert";
		log.info(ApplicationConstants.METHODSTART + methodName);

		if (isEmptyOrNullString(pushNotify) || userId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{

			response = firstUseDao.pushNotifyAlert(userId, pushNotify);

			if ("SUCCESS".equalsIgnoreCase(response))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.PUSHNOTIFYALERT);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
			log.info(ApplicationConstants.METHODEND + methodName);

		}
		return response;
	}

	/**
	 * The service method for Displaying Main Menu. This method checks for
	 * mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method..
	 * 
	 * @param userId
	 *            of the user.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@Override
	public String displayMainMenu(String xml) throws ScanSeeException
	{
		final String methodName = "displayMainMenu";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		DisplayMainMenuResultSet displayMainMenuResultSet = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final DisplayMainMenu displayMainMenuObj = (DisplayMainMenu) streamHelper.parseXmlToObject(xml);
		if (displayMainMenuObj.getUsrId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{
			displayMainMenuResultSet = firstUseDao.displayMainMenu(displayMainMenuObj);

			if (null != displayMainMenuResultSet)
			{

				response = XstreamParserHelper.produceXMlFromObject(displayMainMenuResultSet);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
			log.info(ApplicationConstants.METHODEND + methodName);

		}
		return response;
	}

	/**
	 * The service method to save user media information.This method checks for
	 * mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method..
	 * 
	 * @param xml
	 *            of the user.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	@Override
	public String saveUserMediaInfo(String xml) throws ScanSeeException
	{
		final String methodName = "saveUserMediaInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserMediaInfo userMediaInfo = (UserMediaInfo) streamHelper.parseXmlToObject(xml);
		if (userMediaInfo.getUserId() == null || userMediaInfo.getMediaId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{

			response = firstUseDao.saveUserMediaInfo(userMediaInfo.getUserId(), userMediaInfo.getMediaId());

			if (null != response && !response.equals(ApplicationConstants.FAILURE))
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.USERMEDIAINFOADDED);

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
			log.info(ApplicationConstants.METHODEND + methodName);

		}
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching tutorial media
	 * information. Method Type:GET.
	 * 
	 * @param userId
	 *            for which tutorial media info need to be fetched.If userId is
	 *            null then it is invalid request.
	 * @return XML containing tutorial media information.
	 * @throws ScanSeeException
	 *             If any exception occures ScanSeeException will be thrown.
	 */
	@Override
	public String fetchTutorialMediaInfo(Integer userId) throws ScanSeeException
	{
		final String methodName = "fetchTutorialMediaInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		String screenImgPath = null;
		TutorialMediaResultSet tutorialMediaResultSet = null;

		if (null == userId)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			tutorialMediaResultSet = firstUseDao.fetchTutorialMediaInfo();

			final ArrayList<AppConfiguration> appConfiguration = shoppingListDao.getAppConfig(ApplicationConstants.TUTORIALSCREENIMAGE);

			for (int i = 0; i < appConfiguration.size(); i++)
			{
				if ("TutorialImageURL".equals(appConfiguration.get(i).getScreenName()))
				{
					screenImgPath = appConfiguration.get(i).getScreenContent();
				}
			}
			if (null != tutorialMediaResultSet)
			{
				tutorialMediaResultSet.setScreenImgPath(screenImgPath);

				response = XstreamParserHelper.produceXMlFromObject(tutorialMediaResultSet);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}

		}

		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This method fetches user details and sends password to user email id.This
	 * method checks for mandatory fields, if any mandatory fields are not
	 * available returns Insufficient Data error message else calls the DAO
	 * method.
	 * 
	 * @param userName
	 *            request parameter
	 * @return The Account Information.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String forgotPassword(String userName) throws ScanSeeException
	{
		final String methodName = "forgotPassword";
		log.info(ApplicationConstants.METHODSTART + methodName);
		AuthenticateUser authenticateUser = null;
		EncryptDecryptPwd enryptDecryptpwd;
		String decryptedPassword = null;
		String toAddress = null;
		String subject = null;
		String msgBody = null;
		String fromAddress = null;
		String responseXML = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;
		List<EmailDecline> frmAddressAndSubjectList = null;
		if (isEmptyOrNullString(userName))
		{
			log.info("userName missing in request");
			responseXML = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{
			authenticateUser = firstUseDao.fetchUserDetailsforForgotPassword(userName);
			if (null != authenticateUser)
			{
				if (authenticateUser.getEmail() == null)
				{
					log.info("Validation failed To Email is not available");
					responseXML = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.USEREMAILIDNOTEXIST4);
					return responseXML;
				}
				frmAddressAndSubjectList = firstUseDao.fetchFrmAddressAndSubjectforForgotPassword(ApplicationConstants.FORGOTEMAILCONFIGURATIONTYPE);

				try
				{
					enryptDecryptpwd = new EncryptDecryptPwd();
					decryptedPassword = enryptDecryptpwd.decrypt(authenticateUser.getPassword());
					toAddress = authenticateUser.getEmail();
					if (frmAddressAndSubjectList != null)
					{
						for (int i = 0; i < frmAddressAndSubjectList.size(); i++)
						{
							if ("Forgot Password".equalsIgnoreCase(frmAddressAndSubjectList.get(i).getConfigurationType()))
							{
								if ("FromAddress".equalsIgnoreCase(frmAddressAndSubjectList.get(i).getScreenName()))
								{
									fromAddress = frmAddressAndSubjectList.get(i).getScreenContent();
								}
								if ("Subject".equalsIgnoreCase(frmAddressAndSubjectList.get(i).getScreenName()))
								{
									subject = frmAddressAndSubjectList.get(i).getScreenContent();
								}
							}
						}
						emailConf = shoppingListDao.getAppConfig(ApplicationConstants.EMAILCONFIG);

						for (int j = 0; j < emailConf.size(); j++)
						{
							if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
							{
								smtpHost = emailConf.get(j).getScreenContent();
							}
							if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
							{
								smtpPort = emailConf.get(j).getScreenContent();
							}
						}

						msgBody = Utility.formForgotPasswordEmailTemplateHTML(decryptedPassword);
						if (null != smtpHost || null != smtpPort)
						{
							EmailComponent.mailingComponent(fromAddress, toAddress, subject, msgBody, smtpHost, smtpPort);
						}

						responseXML = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.FORGOTPASSWORD);
						log.info("Password sent to user Email Address:");
					}
				}
				catch (NoSuchAlgorithmException e)
				{
					log.error("Exception in Decryption ", e);
					throw new ScanSeeException(e.getMessage());
				}
				catch (NoSuchPaddingException e)
				{
					log.error("Exception in user password Decryption ", e);
					throw new ScanSeeException(e.getMessage());
				}
				catch (InvalidKeyException e)
				{
					log.error("Password Decryption Exception  ", e);
					throw new ScanSeeException(e.getMessage());
				}
				catch (InvalidAlgorithmParameterException e)
				{
					log.error(" Decryption  Password Exception ", e);
					throw new ScanSeeException(e.getMessage());
				}
				catch (InvalidKeySpecException e)
				{
					log.error("user Password Exception in Decryption ", e);
					throw new ScanSeeException(e.getMessage());
				}
				catch (IllegalBlockSizeException e)
				{
					log.error("exception Decryption Password   ", e);
					throw new ScanSeeException(e.getMessage());
				}
				catch (BadPaddingException e)
				{
					log.error("user password decrypted exception ", e);
					throw new ScanSeeException(e.getMessage());
				}
				catch (IOException e)
				{
					log.error("Exception in Decryption ", e);
					throw new ScanSeeException(e.getMessage());
				}
				catch (MessagingException e)
				{

					log.error("Exception in Sending Mail ", e);
					responseXML = Utility.formResponseXml(ApplicationConstants.INVALIDEMAILADDRESS, ApplicationConstants.INVALIDEMAILADDRESSTEXT);
				}

			}
			else
			{
				log.error("Invalid UserId {}", userName);
				responseXML = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
			log.info(ApplicationConstants.METHODEND + methodName);

		}
		return responseXML;

	}

	/**
	 * The service method for fetching welcome screen details. This method
	 * checks for mandatory fields, if any mandatory fields are not available
	 * returns Insufficient Data error message else calls the DAO method..
	 * 
	 * @param screenName
	 *            of the user.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@Override
	public String fetchStaticScreenInfo(String screenName) throws ScanSeeException
	{
		final String methodName = "fetchStaticScreenInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (isEmptyOrNullString(screenName))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{
			ScreenTextInfo screenTextInfoObj = null;
			screenTextInfoObj = firstUseDao.fetchStaticScreenDetails(screenName);
			if (null != screenTextInfoObj)
			{
				response = XstreamParserHelper.produceXMlFromObject(screenTextInfoObj);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.FIRSTUSESTATICINFO);
			}
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * The service method for fetching welcome screen details. This method
	 * validates the fromAddress and msgBody , if it is valid it will call the
	 * DAO method.
	 * 
	 * @param xml
	 *            the request parameter
	 * @return XML containing success or failure in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */
	@Override
	public String emailNotification(String xml) throws ScanSeeException
	{
		final String methodName = "emailNotification";
		log.info(ApplicationConstants.METHODSTART + methodName);
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		String toAddress = null;
		String fromAddress = null;
		String subject = null;
		String msgBody = null;
		String responseXML = null;
		AuthenticateUser userInfo = null;
		List<EmailDecline> finalResult = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;
		userInfo = (AuthenticateUser) streamHelper.parseXmlToObject(xml);
		fromAddress = userInfo.getFromAddress();
		msgBody = userInfo.getMsgBody();
		if (isEmptyOrNullString(fromAddress) || isEmptyOrNullString(msgBody))
		{
			log.info("fromAddress missing in request");
			responseXML = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		}
		else
		{
			try
			{
				finalResult = firstUseDao.emailDeclaimNotification(ApplicationConstants.CONFIGURATIONTYPE);
				if (finalResult != null)
				{
					for (int i = 0; i < finalResult.size(); i++)
					{
						if ("AdminEmailId".equalsIgnoreCase(finalResult.get(i).getScreenName()))
						{
							toAddress = finalResult.get(i).getScreenContent();
						}
						if ("DeclineSubject".equalsIgnoreCase(finalResult.get(i).getScreenName()))
						{
							subject = finalResult.get(i).getScreenContent();
						}
					}

					emailConf = shoppingListDao.getAppConfig(ApplicationConstants.EMAILCONFIG);

					for (int j = 0; j < emailConf.size(); j++)
					{
						if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))
						{
							smtpHost = emailConf.get(j).getScreenContent();
						}
						if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))
						{
							smtpPort = emailConf.get(j).getScreenContent();
						}
					}
					if (null != smtpHost || null != smtpPort)
					{
						EmailComponent.mailingComponent(fromAddress, toAddress, subject, msgBody, smtpHost, smtpPort);
					}

					responseXML = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.EMAILNOTIFICATION);
				}
				else
				{
					responseXML = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
				}
			}
			catch (MessagingException e)
			{
				log.error("Exception in Sending Mail ", e);
				responseXML = Utility.formResponseXml(ApplicationConstants.INVALIDEMAILADDRESS, ApplicationConstants.INVALIDEMAILADDRESSTEXT);
			}
			log.info(ApplicationConstants.METHODEND + methodName);

		}
		return responseXML;
	}

	/**
	 * The method for processFaceBookUser. Calls the XStreams helper class
	 * methods for parsing.
	 * 
	 * @param inputXml
	 *            the request xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String processFaceBookUser(final String inputXml) throws ScanSeeException
	{
		final String methodName = "processFaceBookUser";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		AuthenticateUser responseDAO = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserRegistrationInfo userRegistrationInfo = (UserRegistrationInfo) streamHelper.parseXmlToObject(inputXml);
		if (null != userRegistrationInfo)
		{
			final String email = userRegistrationInfo.getEmail();
			final String login = userRegistrationInfo.getLogin();
			if (isEmptyOrNullString(email) || isEmptyOrNullString(login) || isEmpty(userRegistrationInfo.getDeviceID()))
			{
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			}

			else
			{
				responseDAO = firstUseDao.registerFaceBookUser(userRegistrationInfo);
				if (responseDAO != null)
				{
					// response =
					// Utility.formResponseXml(ApplicationConstants.SUCCESSCODE,
					// ApplicationConstants.SUCCESS,
					// ApplicationConstants.ELEMENTNAME, response);

					response = XstreamParserHelper.produceXMlFromObject(responseDAO);
				}

				else
				{
					response = Utility
							.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
				}

			}
			log.info(ApplicationConstants.METHODEND + methodName);
		}
		return response;
	}

	/**
	 * The service method for fetching welcome screen details. This method
	 * checks for mandatory fields, if any mandatory fields are not available
	 * returns Insufficient Data error message else calls the DAO method..
	 * 
	 * @param screenName
	 *            of the user.
	 * @return XML containing user information in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@Override
	public String fetchGeneralStaticScreenInfo(String screenName) throws ScanSeeException
	{
		final String methodName = "fetchGeneralStaticScreenInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (isEmptyOrNullString(screenName))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);

		}
		else
		{
			ScreenTextInfo screenTextInfoObj = null;
			screenTextInfoObj = firstUseDao.GeneralStaticScreenContent(screenName);
			if (null != screenTextInfoObj)
			{
				response = XstreamParserHelper.produceXMlFromObject(screenTextInfoObj);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.FIRSTUSESTATICINFO);
			}
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The method for register push notification. Calls the XStreams helper
	 * class methods for parsing.
	 * 
	 * @param inputXml
	 *            the request xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String registerPushNotification(String inputXml) throws ScanSeeException
	{
		final String methodName = "registerPushNotification";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserRegistrationInfo userRegistrationInfo = (UserRegistrationInfo) streamHelper.parseXmlToObject(inputXml);
		if (null != userRegistrationInfo)
		{
			final String deviceid = userRegistrationInfo.getDeviceID();
			final String tokernid = userRegistrationInfo.getUserTokenID();
			if (isEmptyOrNullString(deviceid) || isEmptyOrNullString(tokernid))
			{
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
			}

			else
			{

				response = firstUseDao.registerPushNotification(userRegistrationInfo);
				if (null != response && !response.equals(ApplicationConstants.FAILURE))
				{
					if (response.equalsIgnoreCase("Exists"))
					{
						response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.PUSHNOTIFREGISTRATION);
					}
					else
					{
						response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SUCCESSRESPONSETEXT);
					}
				}

				else
				{
					response = Utility
							.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
				}

			}
			log.info(ApplicationConstants.METHODEND + methodName);
		}
		return response;
	}

	/**
	 * To update app version.
	 * 
	 * @param xml the request xml
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String updateAppVersion(String xml) throws ScanSeeException
	{
		final String methodName = "updateAppVersion";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserRegistrationInfo userRegistrationInfo = (UserRegistrationInfo) streamHelper.parseXmlToObject(xml);
		if (null != userRegistrationInfo)
		{

			if (isEmpty(userRegistrationInfo.getDeviceID()) || isEmptyOrNullString(userRegistrationInfo.getAppVersion()))
			{
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
				return response;
			}
			else
			{
				response = firstUseDao.updateAppVersion(userRegistrationInfo);

				if (null != response && !response.equals(ApplicationConstants.FAILURE))
				{
					response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.UPDATEAPPVERSIONTEXT);
				}
				else
				{
					response = Utility
							.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
				}
			}
		}

		log.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * 
	 * @param xml the request xml
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String addDeviceToUser(String xml) throws ScanSeeException
	{
		final String methodName = "addDeviceToUser";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserRegistrationInfo userRegistrationInfo = (UserRegistrationInfo) streamHelper.parseXmlToObject(xml);
		if (null != userRegistrationInfo)
		{
			if (userRegistrationInfo.getUserId() == null || isEmpty(userRegistrationInfo.getDeviceID())
					|| isEmptyOrNullString(userRegistrationInfo.getAppVersion()))
			{
				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
				return response;
			}
			else
			{
				response = firstUseDao.addDeviceToUser(userRegistrationInfo);

				if (null != response && !response.equals(ApplicationConstants.FAILURE))
				{
					response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.UPDATEUSERDEVICEIDTEXT);
				}
				else
				{
					response = Utility
							.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
				}
			}
		}

		log.info(ApplicationConstants.METHODEND + methodName);

		return response;
	}

	/**
	 * For user tracking. To be called after user selecting module.
	 * 
	 * @param xml
	 * @return xml containing SUCCESS or FAILUR response
	 * @throws ScanSeeException
	 */
	@Override
	public String userTrackingModuleClick(String xml) throws ScanSeeException {
//		final String methodName = "userTrackingModuleClick";
//		log.info(ApplicationConstants.METHODSTART + methodName);
//		String response = null;
//		final XstreamParserHelper xmlParser = new XstreamParserHelper();
//		final UserTrackingData userTrackingData = (UserTrackingData) xmlParser.parseXmlToObject(xml);
//		
//		if(null != userTrackingData)	
//		{
//			if(userTrackingData.getUserID() == null || isEmpty(userTrackingData.getUserID().toString()))	{
//				response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
//			}
//			else	{
//				UserTrackingData objUserTrack;
//				objUserTrack = firstUseDao.userTrackingModuleClick(userTrackingData);
//				if(objUserTrack != null)	{
//					response = XstreamParserHelper.produceXMlFromObject(objUserTrack);
//				}
//				else	{
//					response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.FAILURE);
//				}
//			}
//		}
		return null;
	}
	
	
	/**
     * The serviceImpl method to get started images information.
     * @param iUserId as request parameter.
     * @return XML containing user information in the response.
     * @throws ScanSeeException If any exception occurs ScanSeeException will be thrown.
     */
	@Override
	public String getStartedImageInfo(Integer iUserId) throws ScanSeeException
	{
		final String methodName = "getStartedImageInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		TutorialMediaResultSet objTutMediaResultSet = null;
		if (null == iUserId)
		{
			strResponse = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		} else {
			objTutMediaResultSet = firstUseDao.getStartedImageInfo();
			if (null != objTutMediaResultSet) {
				strResponse = XstreamParserHelper.produceXMlFromObject(objTutMediaResultSet);
			} else {
				strResponse = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * This method to contact ScanSee via email.
	 * @param xml as input request.
	 * @return xml string containing SUCCESS or FAILURE, if SUCCESS mail will be sent to scansee.
	 */
	@Override
	public String contactScanSeeByEmail(String xml) throws ScanSeeException {
		final String methodName = "contactScanSeeByEmail";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final UserRegistrationInfo userRegistrationInfo = (UserRegistrationInfo) streamHelper.parseXmlToObject(xml);
		
		String userEmailId = null;
		String toEmailId = null; //ScanSee email address
		String subject = null;
		String msgBody = null;
		String smtpHost = null;
		String smtpPort = null;
		ArrayList<AppConfiguration> emailConf = null;
		
		if (userRegistrationInfo.getUserId() == null || "".equals(userRegistrationInfo.getUserId()) || userRegistrationInfo.getEmail() == null || "".equals(userRegistrationInfo.getEmail())) {
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		} else {
			userEmailId = userRegistrationInfo.getEmail();
			toEmailId = firstUseDao.contactScanSeeByEmail();
			if(null == toEmailId || toEmailId.equals(""))	{
				log.error("Contact ScanSee email address not found in SP usp_GetScreenContent for ConfigurationType - Decline Email");
				return Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
			subject = userRegistrationInfo.getSubject();
			if(subject == null)	{
				subject = "";
			}
			StringBuilder userDetails = new StringBuilder();
			boolean nameFlag = false, postalcodeFlag = false;
			if (null != userRegistrationInfo.getUserName() && !"".equals(userRegistrationInfo.getUserName()))	{
				userDetails.append("Name: " + userRegistrationInfo.getUserName() + "<br/>");
				nameFlag = true;
			}
			if (null != userRegistrationInfo.getPostalCode() && !"".equals(userRegistrationInfo.getPostalCode()))	{
				userDetails.append("Zipcode: " + userRegistrationInfo.getPostalCode() + "<br/>");
				postalcodeFlag = true;
			}
			//To provide empty line after adding sender details.
			if (nameFlag == true || postalcodeFlag == true)	{
				userDetails.append("<br/>");
			}
			
			msgBody = userDetails.toString() + userRegistrationInfo.getMessage();
			msgBody = msgBody.replaceAll(" ", "&nbsp;");
			
			try
			{
				emailConf = shoppingListDao.getAppConfig(ApplicationConstants.EMAILCONFIG);
				for (int j = 0; j < emailConf.size(); j++)
				{
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPHOST))	{
						smtpHost = emailConf.get(j).getScreenContent();
					}
					if (emailConf.get(j).getScreenName().equals(ApplicationConstants.SMTPPORT))	{
						smtpPort = emailConf.get(j).getScreenContent();
					}
				}
				if (null != smtpHost || null != smtpPort)	{
					EmailComponent.mailingComponent(userEmailId, toEmailId, subject, msgBody, smtpHost, smtpPort);
					response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SUCCESSRESPONSETEXT);
				}
				log.info("Mail delivered to:" + toEmailId);
			}
			catch (MessagingException exception)
			{
				log.error(ApplicationConstants.ERROROCCURRED + methodName, exception);
				response = Utility.formResponseXml(ApplicationConstants.INVALIDEMAILADDRESS, ApplicationConstants.INVALIDEMAILADDRESSTEXT);
			}
		}
		return response;
	}
}
