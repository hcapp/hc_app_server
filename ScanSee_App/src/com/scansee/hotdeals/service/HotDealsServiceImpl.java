package com.scansee.hotdeals.service;

import static com.scansee.common.util.Utility.isEmpty;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.helper.XstreamParserHelper;
import com.scansee.common.pojos.HotDealAPIResultSet;
import com.scansee.common.pojos.HotDealsCategoryInfo;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.HotDealsListRequest;
import com.scansee.common.pojos.HotDealsListResultSet;
import com.scansee.common.pojos.HotDealsResultSet;
import com.scansee.common.pojos.UserTrackingData;
import com.scansee.common.util.Utility;
import com.scansee.firstuse.dao.FirstUseDAO;
import com.scansee.firstuse.service.FirstUseServiceImpl;
import com.scansee.hotdeals.dao.HotDealsDAO;

/**
 * This class is implementation of HotDealsService methods.
 * 
 * @author shyamsundara_hm
 */
public class HotDealsServiceImpl implements HotDealsService
{

	/**
	 * Getting the logger instance.
	 */

	private static final Logger log = LoggerFactory.getLogger(FirstUseServiceImpl.class.getName());
	/**
	 * The variable of type HotDealsDAO.
	 */

	private HotDealsDAO hotDealsDao;
	
	/**
	 * Instance variable for firstUse DAO instance.
	 */
	private FirstUseDAO firstUseDao;

	/**
	 * Setter method for HotDealsDAO.
	 * 
	 * @param hotDealsDao
	 *            the Variable of type HotDealsDAO
	 */

	public void setHotDealsDao(HotDealsDAO hotDealsDao)
	{
		this.hotDealsDao = hotDealsDao;
	}
	
	/**
	 * sets the FirstUseDAO
	 * 
	 * @param firstUseDao
	 * 				The instance for FirstUseDAO
	 */
	public void setFirstUseDao(FirstUseDAO firstUseDao) 
	{
		this.firstUseDao = firstUseDao;
	}

	/**
	 * The service implementation method for fetching hot deals list. Calls the
	 * XStreams helper class methods for parsing.
	 * 
	 * @param xml
	 *            the request xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getHotDealslst(String xml) throws ScanSeeException
	{
		final String methodName = "getHotDealslst";
		log.info(ApplicationConstants.METHODSTART + methodName);
//		HotDealsListResultSet hotDealsListResultSet = null;
//		List<HotDealsCategoryInfo> hotDealsCategoryInfo = null;
//		ArrayList<HotDealsDetails> hotDealsDetailsArrayLst = null;
//		ArrayList<HotDealAPIResultSet> hotDealsCategoryInfolst = null;
//		final List<HotDealsCategoryInfo> hotDealsCategoryInfoList = new ArrayList<HotDealsCategoryInfo>();
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final HotDealsListRequest hotDealsListRequest = (HotDealsListRequest) streamHelper.parseXmlToObject(xml);
		if (hotDealsListRequest.getUserId() == null || isEmpty(hotDealsListRequest.getLatitude()) || isEmpty(hotDealsListRequest.getLongitude())
				|| isEmpty(hotDealsListRequest.getCity()))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);

		}
		else
		{
			ArrayList<HotDealsResultSet> hotDealsDetailsResultSet;

			//For user tracking
			Integer mainMenuID = null;
			if(hotDealsListRequest.getMainMenuID() == null)	{
				UserTrackingData objUserTrackingData = new UserTrackingData();
				objUserTrackingData.setUserID(hotDealsListRequest.getUserId());
				objUserTrackingData.setModuleID(hotDealsListRequest.getModuleID());
				if (hotDealsListRequest.getLatitude() != null && hotDealsListRequest.getLongitude() != null) {
					objUserTrackingData.setLatitude(Double.valueOf(hotDealsListRequest.getLatitude()));
					objUserTrackingData.setLongitude(Double.valueOf(hotDealsListRequest.getLongitude()));
				}
				mainMenuID = firstUseDao.userTrackingModuleClick(objUserTrackingData);
			}
			else	{
				mainMenuID = hotDealsListRequest.getMainMenuID();
			}
			
			hotDealsListRequest.setMainMenuID(mainMenuID);
			
			HotDealsListResultSet dbDealsResult = hotDealsDao.fetchHotDealsDetails(hotDealsListRequest, ApplicationConstants.HOTDEALSSEARCHSCREEN);
			hotDealsDetailsResultSet = (ArrayList<HotDealsResultSet>) dbDealsResult.getHotDealsListResponselst();
			
			HotDealsListResultSet objHotDealResults = null;
			HotDealAPIResultSet apiResultSet = null;
			ArrayList<HotDealAPIResultSet> hdAPIList = null; 
			if (null != hotDealsDetailsResultSet && !hotDealsDetailsResultSet.isEmpty())
			{
				objHotDealResults = new HotDealsListResultSet();
				hdAPIList = new ArrayList<HotDealAPIResultSet>();
				hdAPIList = HotDealsHelper.sortHotdealsByAPINames(hotDealsDetailsResultSet);
				ArrayList<HotDealAPIResultSet> hdAPIResultSet = new ArrayList<HotDealAPIResultSet>();
				if(hdAPIList != null && !hdAPIList.isEmpty())	{
					HotDealsListResultSet hdCatInfo = null;	
					for(int i = 0; i < hdAPIList.size(); i++)	{
						apiResultSet = new HotDealAPIResultSet();
						hdCatInfo = new HotDealsListResultSet();
						apiResultSet.setApiPartnerId(hdAPIList.get(i).getApiPartnerId());
						apiResultSet.setApiPartnerName(hdAPIList.get(i).getApiPartnerName());
						hdCatInfo = HotDealsHelper.sortHotDealsListByCategory(hdAPIList.get(i).getHotDealsDetailslst());
						apiResultSet.setHdCatInfoList((ArrayList<HotDealsCategoryInfo>) hdCatInfo.getHotDealsCategoryInfo());
						hdAPIResultSet.add(apiResultSet);
					}
				}
				objHotDealResults.setHdAPIResult(hdAPIResultSet);
				objHotDealResults.setNextPage(dbDealsResult.getNextPage());
				objHotDealResults.setFavCat(dbDealsResult.getFavCat());
				objHotDealResults.setCategoryFlag(dbDealsResult.getCategoryFlag());
				objHotDealResults.setCategory(dbDealsResult.getCategory());
			}
		
			if (objHotDealResults != null)	{
				objHotDealResults.setMainMenuID(mainMenuID);
				response = XstreamParserHelper.produceXMlFromObject(objHotDealResults);
			}
			else
			{
				String mmID = null;
				if(null != mainMenuID){
					mmID = mainMenuID.toString();
				} else{
					mmID = ApplicationConstants.STRING_ZERO;
				}
				Integer favFlag = dbDealsResult.getFavCat();
				Integer categoryFlag = dbDealsResult.getCategoryFlag();
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOHOTDEALSFOUNDTEXT, "FavCatFlag",
						String.valueOf(favFlag), "categoryFlag", String.valueOf(categoryFlag), "mainMenuID", mmID);
			}
			
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service implementation method for getHotDeal information. Calls the
	 * XStreams helper class methods for parsing.
	 * 
	 * @param userId
	 *            the request .
	 * @param hotDealId
	 *            the request parameter.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String getHdProdInfo(Integer userId, Integer hotDealId, Integer hotDealListId) throws ScanSeeException
	{
		final String methodName = "getHdProdInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final HotDealsCategoryInfo hotDealsCategoryInfo = hotDealsDao.getHotDealProductDetail(userId, hotDealId, hotDealListId);
		
		if (userId == null || hotDealId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);

		}
		else
		{
			
			if (null != hotDealsCategoryInfo)
			{
				//Changing the date format.
				if(hotDealsCategoryInfo.getHotDealsDetailsArrayLst() != null && !hotDealsCategoryInfo.getHotDealsDetailsArrayLst().isEmpty()) {
					for(HotDealsDetails hdDetails : hotDealsCategoryInfo.getHotDealsDetailsArrayLst())	{
						hdDetails.setIsDateFormated(false);
						hdDetails.sethDStartDate(hdDetails.gethDStartDate());
						hdDetails.sethDEndDate(hdDetails.gethDEndDate());
						hdDetails.sethDExpDate(hdDetails.gethDExpDate());
					}
				}
				
				response = XstreamParserHelper.produceXMlFromObject(hotDealsCategoryInfo);
				response = response.replaceAll("<HotDealsCategoryInfo>", "<HotDealsListResultSet>");
				response = response.replaceAll("</HotDealsCategoryInfo>", "</HotDealsListResultSet>");

			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOHOTDEALSPRODUCTDETAILFOUNDTEXT);
			}
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service implementation method for remove Hot deals. Calls the XStream
	 * helper class methods for parsing.
	 * 
	 * @param xml
	 *            the request .
	 * @return response string with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	public String removeHDProd(String xml) throws ScanSeeException
	{
		final String methodName = "removeHDProd";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final HotDealsListRequest hotDealsListRequest = (HotDealsListRequest) streamHelper.parseXmlToObject(xml);
		if (hotDealsListRequest.getUserId() == null || hotDealsListRequest.gethDInterested() == null || hotDealsListRequest.getHotDealId() == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);

		}
		else
		{
			response = hotDealsDao.removeHDProduct(hotDealsListRequest);
			if (null != response)
			{
				response = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.HOTDEALSUPDATETEXT);
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This service implementation method for getting hot deals Category. Calls
	 * the XStreams helper class methods for parsing.
	 * 
	 * @param userId
	 *            as request parameter
	 * @return response string with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getHdCategoryInfo(Integer userId) throws ScanSeeException
	{
		final String methodName = "getHdCategoryInfo";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		final List<HotDealsCategoryInfo> hotDealCategorylst = hotDealsDao.getHdCategoryDetail(userId);
		if (!hotDealCategorylst.isEmpty())
		{
			response = XstreamParserHelper.produceXMlFromObject(hotDealCategorylst);
			response = response.replaceAll("<list>", "<HotDealsCategoryList>");
			response = response.replaceAll("</list>", "</HotDealsCategoryList>");
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.HOTDEALSCATEGORYNOTFOUND);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This service implementation method for getting hot deals population
	 * center. Calls the XStreams helper class methods for parsing.
	 * 
	 * @return response string with population centers
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getHdPopulationCenters(Integer userId) throws ScanSeeException
	{
		final String methodName = "getHdPopulationCenters";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		 List<HotDealsCategoryInfo> hotDealCategorylst=null;
		if ( userId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else{
		hotDealCategorylst = hotDealsDao.getHdPopulationCenters(userId);
		}
		if (!hotDealCategorylst.isEmpty())
		{
			response = XstreamParserHelper.produceXMlFromObject(hotDealCategorylst);
			response = response.replaceAll("<list>", "<HdPopulationCentersList>");
			response = response.replaceAll("</list>", "</HdPopulationCentersList>");
		}
		else
		{
			response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This service implementation method for searching hot deals population
	 * center. Calls the XStreams helper class methods for parsing.
	 * 
	 * @param dmaName
	 *            as request parameter
	 * @return response string with population centers
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String searchHdPopulationCenters(String dmaName,Integer userId) throws ScanSeeException
	{
		final String methodName = "getHdPopulationCenters";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (dmaName == null || userId == null)
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else
		{

			final List<HotDealsCategoryInfo> hotDealCategorylst = hotDealsDao.searchHdPopulationCenters(dmaName,userId);
			if (hotDealCategorylst != null && !hotDealCategorylst.isEmpty())
			{
				response = XstreamParserHelper.produceXMlFromObject(hotDealCategorylst);
				response = response.replaceAll("<list>", "<HdPopulationCentersList>");
				response = response.replaceAll("</list>", "</HdPopulationCentersList>");
			}
			else
			{
				response = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NORECORDSFOUNDTEXT);
			}
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}
	
	/**
	 * The service method For capturing hot deal click.
	 * 
	 * @param userId
	 *            as request
	 * @param hotDealId
	 *            as request
	 * @param hotDealListId
	 *            as request
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */
	public String UserTrackingGetHotDealClick(Integer userId, Integer hotDealId, Integer hotDealListId) throws ScanSeeException
	{
		final String methodName = "UserTrackingGetHotDealClick";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		
		if (userId == null || "".equals(userId) || hotDealId == null || "".equals(hotDealId) || hotDealListId == null || "".equals(hotDealListId))
		{
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
			log.info(ApplicationConstants.METHODEND + response);
		}
		else
		{
			response = hotDealsDao.UserTrackingGetHotDealClick(userId, hotDealId, hotDealListId);
			if(ApplicationConstants.FAILURE.equalsIgnoreCase(response))	{
				log.info(ApplicationConstants.METHODEND + response);
			}
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service implementation method for list out the categories of the hot
	 * deals. Calls the XStreams helper class methods for parsing.
	 * 
	 * @param xml
	 *            the request xml.
	 * @return response xml with success code or error code.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String getHDProdByCategory(String xml) throws ScanSeeException {
		final String methodName = "getHDProdByCategory";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final HotDealsListRequest hDListRequest = (HotDealsListRequest) streamHelper.parseXmlToObject(xml);
		if (hDListRequest.getUserId() == null || (Utility.isEmptyOrNullString(hDListRequest.getLatitude()) && Utility.isEmptyOrNullString(hDListRequest.getZipCode()))
				|| (Utility.isEmptyOrNullString(hDListRequest.getLongitude()) && Utility.isEmptyOrNullString(hDListRequest.getZipCode()))) {
			strResponse = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		} 
		else {
			ArrayList<HotDealsResultSet> arHDDetResultSetParam;
			HotDealsListResultSet dbDealsResult = hotDealsDao.getHDProdByCategory(hDListRequest);
			arHDDetResultSetParam = (ArrayList<HotDealsResultSet>) dbDealsResult.getHotDealsListResponselst();
			if (null != arHDDetResultSetParam && !arHDDetResultSetParam.isEmpty()) {
				strResponse = XstreamParserHelper.produceXMlFromObject(arHDDetResultSetParam);
				strResponse = strResponse.replaceAll("<list>", "<HotDealsResultResponse>");
				strResponse = strResponse.replaceAll("</list>", "</HotDealsResultResponse>");
			} else {
				strResponse = Utility.formResponseXml(ApplicationConstants.NORECORDSFOUND, ApplicationConstants.NOHOTDEALSFOUNDTEXT);
			}
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * This Service Implementation Method to claim hot deal.
	 * Method Type: POST
	 * 
	 * @param xml
	 * 			containing userId, hotDealId and mainMenuID
	 * 
	 * @return XMl containing SUCCESS or FAILURE message 
	 */
	@Override
	public String hotDealClaim(String xml) throws ScanSeeException {
		final String methodName = "hotDealClaim";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final HotDealsListRequest objHdListRequest = (HotDealsListRequest) streamHelper.parseXmlToObject(xml);
		if(objHdListRequest.getUserId() == null || objHdListRequest.getHotDealId() == null || objHdListRequest.getMainMenuID() == null)	{
			strResponse = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else	{
			strResponse = hotDealsDao.hotDealClaim(objHdListRequest);
			if(ApplicationConstants.SUCCESSRESPONSETEXT.equalsIgnoreCase(strResponse))	{
				strResponse = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SUCCESSRESPONSETEXT);				
			}
			else	{
				strResponse = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.FAILURE);
			}
		}	
//		log.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * This Service Implementation Method to redeem hot deal.
	 * Method Type: POST
	 * 
	 * @param xml
	 * 			containing userId, hotDealId and mainMenuID
	 * 
	 * @return XMl containing SUCCESS or FAILURE message 
	 * 
	 * @throws ScanSeeException is thrown
	 */
	@Override
	public String hotDealRedeem(String xml) throws ScanSeeException {
		final String methodName = "hotDealRedeem";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		final HotDealsListRequest objHdListRequest = (HotDealsListRequest) streamHelper.parseXmlToObject(xml);
		if (objHdListRequest.getUserId() == null || objHdListRequest.getHotDealId() == null || objHdListRequest.getMainMenuID() == null)	{
			strResponse = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else	{
			strResponse = hotDealsDao.hotDealRedeem(objHdListRequest);
			if (ApplicationConstants.SUCCESSRESPONSETEXT.equalsIgnoreCase(strResponse))	{
				strResponse = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SUCCESSRESPONSETEXT);				
			}
			else	{
				strResponse = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.FAILURE);
			}
		}	
//		log.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * This is a RestEasy WebService Method to delete used hot deal.
	 * Method Type: POST
	 * 
	 * @param hdGallId as request
	 * 
	 * @return XMl containing SUCCESS or FAILURE message 
	 * 
	 * @throws ScanSeeException is thrown
	 */
	@Override
	public String deleteGalleryUsedHotDeals(Integer hdGallId) throws ScanSeeException {
		final String methodName = "hotDealRedeem";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		if (hdGallId == null || "".equals(hdGallId.toString()))	{
			strResponse = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INSUFFICENTTEXT);
		}
		else	{
			strResponse = hotDealsDao.deleteGalleryUsedHotDeals(hdGallId);
			if (ApplicationConstants.SUCCESSRESPONSETEXT.equalsIgnoreCase(strResponse))	{
				strResponse = Utility.formResponseXml(ApplicationConstants.SUCCESSCODE, ApplicationConstants.SUCCESSRESPONSETEXT);				
			}
			else	{
				strResponse = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.FAILURE);
			}
		}	
		return strResponse;
	}

}
