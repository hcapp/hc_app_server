package com.scansee.hotdeals.service;

import com.scansee.common.exception.ScanSeeException;

/**
 * This interface for hot deals methods, which are implemented by hotdealservice
 * implementation class.
 * 
 * @author shyamsundara_hm
 */
public interface HotDealsService
{

	/**
	 * The service method For displaying hot deals list based on category wise.
	 * 
	 * @param xml
	 *            as request
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */

	String getHotDealslst(String xml) throws ScanSeeException;

	/**
	 * The service method For retrieving each hot deals details.
	 * 
	 * @param userId
	 *            as request
	 * @param hotDealId
	 *            as request
	 * @param hotDealListId
	 *            as request
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */

	String getHdProdInfo(Integer userId, Integer hotDealId, Integer hotDealListId) throws ScanSeeException;

	/**
	 * The service method For remove hot deal details.
	 * 
	 * @param xml
	 *            as request
	 * @return String based success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */

	String removeHDProd(String xml) throws ScanSeeException;

	/**
	 * The service method For displaying hot deal Categories. No query parameter
	 * @param userId as request parameter
	 * @return String based success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */

	String getHdCategoryInfo(Integer userId) throws ScanSeeException;

	/**
	 * The service method For displaying hot deal Categories. No query parameter
	 * 
	 * @return String based success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */

	String getHdPopulationCenters(Integer userId) throws ScanSeeException;

	/**
	 * The service method For displaying hot deal Categories. No query parameter
	 * @param dmaName as request parameter
	 * @return String based success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */

	String searchHdPopulationCenters(String dmaName,Integer userId) throws ScanSeeException;
	
	/**
	 * The service method For capturing hot deal click.
	 * 
	 * @param userId
	 *            as request
	 * @param hotDealId
	 *            as request
	 * @param hotDealListId
	 *            as request
	 * @return xml based on success or failure
	 * @throws ScanSeeException
	 *             for exception
	 */
	String UserTrackingGetHotDealClick(Integer userId, Integer hotDealId, Integer hotDealListId) throws ScanSeeException;
	
	/**
	 * The service method list out the categories of the hot deals.
	 * 
	 * @param xml as request.
	 * @return xml based on data.
	 * @throws ScanSeeException for exception.
	 */
	String getHDProdByCategory(String xml) throws ScanSeeException;
	
	/**
	 * The Service Method to claim hot deals.
	 * @param xml is request parameter.
	 * @return xml containing SUCCESS or FAILURE message
	 * @throws ScanSeeException is thrown.
	 */
	String hotDealClaim(String xml) throws ScanSeeException;
	
	/**
	 * The Service Method to redeem hot deals.
	 * @param xml is request.
	 * @return xml containing SUCCESS or FAILURE message
	 * @throws ScanSeeException is thrown.
	 */
	String hotDealRedeem(String xml) throws ScanSeeException;
	
	/**
	 * This is a RestEasy WebService Method to delete used hot deal.
	 * Method Type: POST
	 * 
	 * @param hdGallId request parameter.
	 * @return XMl containing SUCCESS or FAILURE message 
	 * @throws ScanSeeException is thrown.
	 */
	String deleteGalleryUsedHotDeals(Integer hdGallId) throws ScanSeeException;
}
