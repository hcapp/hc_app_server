package com.scansee.hotdeals.controller;

import static com.scansee.common.constants.ScanSeeURLPath.GETHDCATEGORY;
import static com.scansee.common.constants.ScanSeeURLPath.GETHOTDEALPRODINFO;
import static com.scansee.common.constants.ScanSeeURLPath.GETHOTDEALPRODS;
import static com.scansee.common.constants.ScanSeeURLPath.HOTDEALBASEURL;
import static com.scansee.common.constants.ScanSeeURLPath.REMOVEHDPROD;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.inject.ImplementedBy;
import com.scansee.common.constants.ScanSeeURLPath;

/**
 * The HotDealsRestEasy Interface. {@link ImplementedBy}
 * {@link HotDealsRestEasyImpl}
 * 
 * @author shyamsundara_hm.
 */
@Path(HOTDEALBASEURL)
public interface HotDealsRestEasy
{

	/**
	 * This is a RestEasy WebService Method for fetching Hot deals list for the
	 * given Search text or category. Method Type:POST
	 * 
	 * @param xml
	 *            for which category or search information needed to fetch Hot
	 *            deals list.If category is null then it is invalid request.
	 * @return XML containing Hot deals list information in the response.
	 */

	@POST
	@Path(GETHOTDEALPRODS)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getHotDeallist(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching each Hot deals
	 * information for the given Hot deal ID. Method Type:GET
	 * 
	 * @param userId
	 *            for which Hot deals information need to be fetched
	 * @param hotDealId
	 *            for which Hot deals information need to be fetched.If Hot deal
	 *            ID is null then it is invalid request.
	 * @return XML containing Hot deals information in the response.
	 */

	@GET
	@Path(GETHOTDEALPRODINFO)
	@Produces("text/xml;charset=UTF-8")
	String getHdProdInfo(@QueryParam("userId") Integer userId, @QueryParam("hotDealId") Integer hotDealId, @QueryParam("hotDealListID") Integer hotDealListID);

	/**
	 * This is a RestEasy WebService Method for removal Hot Deals information
	 * for the given Hot deal ID. Method Type:POST
	 * 
	 * @param xml
	 *            containing hot deal id and user id for which Hot Deals to be
	 *            removed.If Hot Deal ID is null then it is invalid request.
	 * @return XML containing success or failure in the response.
	 */

	@POST
	@Path(REMOVEHDPROD)
	@Produces("text/xml")
	@Consumes("text/xml")
	String removeHdProd(String xml);

	/**
	 * This is a RestEasy WebService Method for fetching Hot Deals category
	 * information. Method Type:GET
	 * 
	 * @param userId
	 *            as request parameter
	 * @return XML containing success or failure in the response.
	 */
	@GET
	@Path(GETHDCATEGORY)
	@Produces("text/xml;charset=UTF-8")
	String getHdCategory(@QueryParam("userId") Integer userId);

	/**
	 * The RestEasy WebService method which fetches states and cities for the
	 * given ZipCode.Method Type:GET.
	 * 
	 * @return the XML in response containing population centers information.
	 */
	@GET
	@Path(ScanSeeURLPath.GETHOTDEALPOPULATIONCENTERS)
	@Produces("text/xml;charset=UTF-8")
	String getHotDealPopulationCenters(@QueryParam("userId") Integer userId);

	/**
	 * The RestEasy WebService method which fetches states and cities for the
	 * given ZipCode.Method Type:GET.
	 * 
	 * @param dmaName
	 *            for which countries and states to be fetched.
	 * @return the XML in response containing states and cities information.
	 */
	@GET
	@Path(ScanSeeURLPath.SEARCHHDPOPULATIONCENTERS)
	@Produces("text/xml;charset=UTF-8")
	String searchHdPopulationCenters(@QueryParam("dmaname") String dmaName,@QueryParam("userId") Integer userId);
	
	/**
	 * This is a RestEasy WebService Method for Catching get hot deal click
	 * Need to be executed when user taps on get hot deal in Hot deals screen
	 * 
	 * @param userId
	 *            for which Hot deals information need to be fetched
	 * @param hotDealId
	 *            for which Hot deals information need to be fetched.If Hot deal
	 *            ID is null then it is invalid request.
	 * 
	 */
	@GET
	@Path(ScanSeeURLPath.UTGETHOTDEALCLICK)
	@Produces("text/xml;charset=UTF-8")
	void userTrackingGetHotDealClick(@QueryParam("userId") Integer userId, @QueryParam("hotDealId") Integer hotDealId, @QueryParam("hotDealListID") Integer hotDealListID);

	
	/**
	 * This is a RestEasy WebService Method for list out the categories of the hot deals.
	 * Method Type:POST
	 * 
	 * @param xml
	 *            for which category or search information needed to fetch Hot
	 *            deals list.If category is null then it is invalid request.
	 * @return XML containing Hot deals list information in the response.
	 * 
	 * Note : This services is not using. 
	 *        Implementation is done in  Path(GETHOTDEALPRODS) and Method --> String getHotDeallist(String xml);
	 */

/*	@POST
	@Path(ScanSeeURLPath.GETHDPRODSBYCATEGORY)
	@Produces("text/xml;charset=UTF-8")
	@Consumes("text/xml")
	String getHDProdByCategory(String xml);*/
	
	/**
	 * This is a RestEasy WebService Method to claim hot deal.
	 * Method Type: POST
	 * 
	 * @param xml
	 * 			containing userId, hotDealId and mainMenuID
	 * 
	 * @return XMl containing SUCCESS or FAILURE message 
	 */
	@POST
	@Path(ScanSeeURLPath.HOTDEALCLAIM)
	@Produces("text/xml")
	@Consumes("text/xml")
	String hotDealClaim(String xml);
	
	/**
	 * This is a RestEasy WebService Method to redeem hot deal.
	 * Method Type: POST
	 * 
	 * @param xml
	 * 			containing userId, hotDealId and mainMenuID
	 * 
	 * @return XMl containing SUCCESS or FAILURE message 
	 */
	@POST
	@Path(ScanSeeURLPath.HOTDEALREDEEM)
	@Produces("text/xml")
	@Consumes("text/xml")
	String hotDealRedeem(String xml);
	
	/**
	 * This is a RestEasy WebService Method to delete used hot deal.
	 * Method Type: POST
	 * 
	 * @param hdGallId request paramater.
	 * 
	 * @return XMl containing SUCCESS or FAILURE message 
	 */
	@GET
	@Path("/deletegallusedhd")
	@Produces("text/xml")
	String deleteGalleryUsedHotDeals(@QueryParam("hdGallId") Integer hdGallId);

}
