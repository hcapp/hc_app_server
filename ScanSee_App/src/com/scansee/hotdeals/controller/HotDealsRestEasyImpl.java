package com.scansee.hotdeals.controller;

import static com.scansee.common.util.Utility.formResponseXml;

import javax.ws.rs.QueryParam;

import org.apache.log4j.Logger;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.servicefactory.ServiceFactory;
import com.scansee.hotdeals.service.HotDealsService;

/**
 * The HotDealsRestEasyImpl has methods to accept HotDealsRestEasy
 * requests.These methods are called on the Client request. The method is
 * selected based on the URL Path and the type of request(GET,POST). It invokes
 * the appropriate method in Service layer.
 * 
 * @author shyamsundara_hm
 */

public class HotDealsRestEasyImpl implements HotDealsRestEasy
{

	/**
	 * Getting the Logger Instance.
	 */
	private static final Logger LOG = Logger.getLogger(HotDealsRestEasyImpl.class);

	/**
	 * This is a RestEasyImplimentation WebService method for get the hodDeal
	 * list. Calls method in service layer. accepts a POST request, MIME type is
	 * text/xml.
	 * 
	 * @param xml
	 *            input request for which category or search information needed
	 *            to fetch Hot deals list.If category is null then it is invalid
	 *            request.
	 * @return XML containing Hot deals list information in the response.
	 */

	public String getHotDeallist(String xml)
	{
		final String methodName = "getHotDeallist in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final HotDealsService hotDealsService = ServiceFactory.getHotDealsService();
		try
		{
			response = hotDealsService.getHotDealslst(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned for" + methodName + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This is a RestEasyImplimentation WebService Method for fetching each Hot
	 * deals information for the given Hot deal ID. Method Type:GET
	 * 
	 * @param userId
	 *            for which Hot deals information need to be fetched
	 * @param hotDealId
	 *            for which Hot deals information need to be fetched.If Hot deal
	 *            ID is null then it is invalid request.
	 *            
	 * @param hotDealListID
	 * 				For user tracking, hotDealListID is captures
	 * 
	 * @return XML containing Hot deals information in the response.
	 */
	public String getHdProdInfo(@QueryParam("userId") Integer userId, @QueryParam("hotDealId") Integer hotDealId,@QueryParam("hotDealListID") Integer hotDealListID)
	{
		final String methodName = "getHdProdInfo in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Requested parameters are userId" + userId + "hotDealId" + hotDealId);
		}
		String response = null;
		final HotDealsService hotDealsService = ServiceFactory.getHotDealsService();
		try
		{
			response = hotDealsService.getHdProdInfo(userId, hotDealId, hotDealListID);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned for " + methodName + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This is a RestEasy WebService Method for removal Hot Deals information
	 * for the given Hot deal ID. Method Type:POST
	 * 
	 * @param xml
	 *            containing hot deal id and user id for which Hot Deals to be
	 *            removed.If Hot Deal ID is null then it is invalid request.
	 * @return XML containing success or failure in the response.
	 */
	public String removeHdProd(String xml)
	{
		final String methodName = "removeHdProd in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request recieved " + xml);
		}
		String response = null;
		final HotDealsService hotDealsService = ServiceFactory.getHotDealsService();
		try
		{
			response = hotDealsService.removeHDProd(xml);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;

	}

	/**
	 * This is a RestEasy WebService Method for fetching Hot Deals category
	 * information. Method Type:GET
	 * 
	 * @param userId
	 *            as request parameter
	 * @return XML containing success or failure in the response.
	 */
	@Override
	public String getHdCategory(Integer userId)
	{
		final String methodName = "getHdCategory in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request Parameters User Id:" + userId);
		}
		String response = null;
		final HotDealsService hotDealsService = ServiceFactory.getHotDealsService();
		try
		{
			response = hotDealsService.getHdCategoryInfo(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned " + response);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for fetching hot deals population
	 * centres information. Method Type:GET
	 * 
	 * @return XML containing success or failure in the response.
	 */

	@Override
	public String getHotDealPopulationCenters(Integer userId)
	{
		final String methodName = "getHotDealPopulationCenters in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Request is "+userId);
		}
		final HotDealsService hotDealsService = ServiceFactory.getHotDealsService();
		try
		{
			response = hotDealsService.getHdPopulationCenters(userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + response);
		}
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for searching hot deals population
	 * centres information. Method Type:GET
	 * 
	 * @param dmaName
	 *            as request parameter
	 * @return XML containing success or failure in the response.
	 */
	@Override
	public String searchHdPopulationCenters(String dmaName,Integer userId)
	{
		final String methodName = "searchHdPopulationCenters in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		if (LOG.isDebugEnabled())
		{
			LOG.debug(" Request DMA name:" + dmaName +"user ID"+ userId);
		}
		final HotDealsService hotDealsService = ServiceFactory.getHotDealsService();
		try
		{
			response = hotDealsService.searchHdPopulationCenters(dmaName,userId);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (LOG.isDebugEnabled())
		{
			LOG.debug(ApplicationConstants.RESPONSELOG + response);
		}
		return response;
	}

	/**
	 * This is a RestEasy WebService Method for Catching get hot deal click
	 * Need to be executed when user taps on get hot deal in Hot deals screen
	 * 
	 * @param userId
	 *            for which Hot deals information need to be fetched
	 * @param hotDealId
	 *            for which Hot deals information need to be fetched.If Hot deal
	 *            ID is null then it is invalid request.
	 * @param hotDealListID
	 * 				For user tracking, hotDealListID is captures
	 */
	@Override
	public void userTrackingGetHotDealClick(Integer userId, Integer hotDealId, Integer hotDealListID) {
		final String methodName = "UserTrackingGetHotDealClick in RestEasy Layer";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String response;
		
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Requested parameters are userId" + userId + "hotDealId" + hotDealId + "hotDealListID" + hotDealListID);
		}
		final HotDealsService hotDealsService = ServiceFactory.getHotDealsService();
		try
		{
			response = hotDealsService.UserTrackingGetHotDealClick(userId, hotDealId, hotDealListID);
		}
		catch (ScanSeeException exception)
		{
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			response = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Response returned for " + methodName + response);
		}
	}
	
	/**
	 * This is a RestEasyImplimentation WebService method for list out the categories of the hot deals
	 * Calls method in service layer. accepts a POST request, MIME type is text/xml.
	 * 
	 * @param xml
	 *            input request for which category or search information needed
	 *            to fetch Hot deals list.If category is null then it is invalid
	 *            request.
	 * @return XML containing Hot deals list information in the response.
	 */

	public String getHDProdByCategory(String xml)
	{
		final String methodName = "getHDProdByCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		final HotDealsService hotDealsService = ServiceFactory.getHotDealsService();
		try {
			strResponse = hotDealsService.getHDProdByCategory(xml);
		} catch (ScanSeeException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			strResponse = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * This is a RestEasy WebService Method to claim hot deal.
	 * Method Type: POST
	 * 
	 * @param xml
	 * 			containing userId, hotDealId and mainMenuID
	 * 
	 * @return XMl containing SUCCESS or FAILURE message 
	 */
	@Override
	public String hotDealClaim(String xml) {
		final String methodName = "hotDealClaim";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		final HotDealsService hotDealsService = ServiceFactory.getHotDealsService();
		try {
			strResponse = hotDealsService.hotDealClaim(xml);
		} catch (ScanSeeException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			strResponse = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * This is a RestEasy WebService Method to redeem hot deal.
	 * Method Type: POST
	 * 
	 * @param xml
	 * 			containing userId, hotDealId and mainMenuID
	 * 
	 * @return XMl containing SUCCESS or FAILURE message 
	 */
	@Override
	public String hotDealRedeem(String xml) {
		final String methodName = "getHDProdByCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		final HotDealsService hotDealsService = ServiceFactory.getHotDealsService();
		try {
			strResponse = hotDealsService.hotDealRedeem(xml);
		} catch (ScanSeeException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			strResponse = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}

	/**
	 * This is a RestEasy WebService Method to delete used hot deal.
	 * Method Type: POST
	 * 
	 * @param hdGallId request paramater.
	 * 
	 * @return XMl containing SUCCESS or FAILURE message 
	 */
	@Override
	public String deleteGalleryUsedHotDeals(Integer hdGallId) {
		final String methodName = "getHDProdByCategory";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponse = null;
		final HotDealsService hotDealsService = ServiceFactory.getHotDealsService();
		try {
			strResponse = hotDealsService.deleteGalleryUsedHotDeals(hdGallId);
		} catch (ScanSeeException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			strResponse = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponse;
	}
	
}
