package com.scansee.hotdeals.query;

/**
 * The HotDealsQueries for Queries.
 * 
 * @author shyamsundara_hm.
 */
public class HotDealsQueries
{

	/**
	 * For displaying Hotdeals Category.
	 */
	public static final String HOTDEALCATEGORY = "SELECT LTRIM(Category) categoryName FROM (SELECT 'All' Category UNION ALL SELECT DISTINCT Category FROM ProductHotDeal WHERE Category IS NOT NULL ) Category ORDER BY CASE WHEN Category = 'All' THEN 0 ELSE 1 END";

	/**
	 * For displaying Hotdeals Category.
	 */
	public static final String HOTDEALCATEGORY_NEW = "SELECT CategoryID, Category categoryName from "
			+ "(SELECT 0 CategoryID , 'All' Category  UNION ALL " + "SELECT CategoryID, Category = ParentCategoryName + ' - ' + SubCategoryName "
			+ "FROM Category) Category " + "ORDER BY CASE WHEN Category = 'All' THEN 0 ELSE 1 END, Category ";

	/**
	 * constructor for HotDealsQueries.
	 */
	private HotDealsQueries()
	{

	}

}
