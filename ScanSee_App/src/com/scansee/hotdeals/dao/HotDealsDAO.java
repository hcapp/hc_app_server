package com.scansee.hotdeals.dao;

import java.util.List;

import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.HotDealsCategoryInfo;
import com.scansee.common.pojos.HotDealsListRequest;
import com.scansee.common.pojos.HotDealsListResultSet;

/**
 * This interface is used for HotDeals module.It has methods which are
 * implemented by HotDealsDAOImpl.
 * 
 * @author shyamsundara_hm
 */
public interface HotDealsDAO
{

	/**
	 * The DAO method fetches the HotDeals Details.
	 * 
	 * @param hotDealsListRequest
	 *            Instance of HotDealsListRequest.
	 * @param screenName
	 *            as query parameter
	 * @return HotDealsListResultSet.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	HotDealsListResultSet fetchHotDealsDetails(HotDealsListRequest hotDealsListRequest, String screenName) throws ScanSeeException;

	/**
	 * The DAO method fetches the hotdeal products information.
	 * 
	 * @param userId
	 *            as Query parameter
	 * @param hotDealId
	 *            as Query parameter
	 * @return HotDealsListResultSet.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	HotDealsCategoryInfo getHotDealProductDetail(Integer userId, Integer hotDealId, Integer hotDealListId) throws ScanSeeException;

	/**
	 * The DAO method removes hotDeal product.
	 * 
	 * @param hotDealsListRequest
	 *            as Query parameter
	 * @return response based on success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	String removeHDProduct(HotDealsListRequest hotDealsListRequest) throws ScanSeeException;

	/**
	 * The DAO method fetches the HotDeal Category Details. no Query parameter
	 * 
	 * @param userId
	 *            as request parameter
	 * @return HotDealsCategoryInfo.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	List<HotDealsCategoryInfo> getHdCategoryDetail(Integer userId) throws ScanSeeException;

	/**
	 * The DAO method fetches the HotDeal Category Details. no Query parameter
	 * 
	 * @return HotDealsCategoryInfo.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	List<HotDealsCategoryInfo> getHdPopulationCenters(Integer userId) throws ScanSeeException;

	/**
	 * The DAO method fetches the HotDeal Category Details. no Query parameter
	 * 
	 * @param dmaName
	 *            as request parameter
	 * @return HotDealsCategoryInfo.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	List<HotDealsCategoryInfo> searchHdPopulationCenters(String dmaName,Integer userId) throws ScanSeeException;
	
	/**
	 * The DAO method to update hot deal click impression to database.
	 * 
	 * @param userId
	 *            as Query parameter
	 * @param hotDealId
	 *            as Query parameter
	 * @return String SUCCESS or FAILURE
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	String UserTrackingGetHotDealClick(Integer userId, Integer hotDealId, Integer hotDealListId) throws ScanSeeException;
	
	/**
	 * The DAO method for list out the categories of the hot deals..
	 * 
	 * @param objHDListReq Instance of HotDealsListRequest.
	 * @return HotDealsListResultSet.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	HotDealsListResultSet getHDProdByCategory(HotDealsListRequest objHDListReq) throws ScanSeeException;
	
	/**
	 * This DAO Method to claim hot deal.
	 * Method Type: POST
	 * 
	 * @param xml
	 * 			containing userId, hotDealId and mainMenuID
	 * 
	 * @return XMl containing SUCCESS or FAILURE message 
	 */
	String hotDealClaim(HotDealsListRequest objHotDealListRequest) throws ScanSeeException;
	
	/**
	 * This DAO Method to redeem hot deal.
	 * Method Type: POST
	 * 
	 * @param xml
	 * 			containing userId, hotDealId and mainMenuID
	 * 
	 * @return XMl containing SUCCESS or FAILURE message 
	 */
	String hotDealRedeem(HotDealsListRequest objHotDealListRequest) throws ScanSeeException;
	
	/**
	 * This is a RestEasy WebService Method to delete used hot deal.
	 * Method Type: POST
	 * 
	 * @param xml
	 * 			containing userId, hotDealId and mainMenuID
	 * 
	 * @return XMl containing SUCCESS or FAILURE message 
	 */
	String deleteGalleryUsedHotDeals(Integer hdGallId) throws ScanSeeException;
}
