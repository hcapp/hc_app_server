package com.scansee.hotdeals.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.scansee.common.constants.ApplicationConstants;
import com.scansee.common.exception.ScanSeeException;
import com.scansee.common.pojos.Category;
import com.scansee.common.pojos.HotDealsCategoryInfo;
import com.scansee.common.pojos.HotDealsDetails;
import com.scansee.common.pojos.HotDealsListRequest;
import com.scansee.common.pojos.HotDealsListResultSet;
import com.scansee.common.pojos.HotDealsResultSet;
import com.scansee.common.util.Utility;

/**
 * This is implementation class for HotDealsDAO. This class has methods for Hot
 * Deals Module. The methods of this class are called from the HotDeals Service
 * layer.
 * 
 * @author shyamsundara_hm
 */
public class HotDealsDAOImpl implements HotDealsDAO

{

	/**
	 * for getting logger...
	 */

	private static final Logger log = Logger.getLogger(HotDealsDAOImpl.class.getName());

	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * To call stored procedue.
	 */

	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To get the datasource from xml..
	 * 
	 * @param dataSource
	 *            for setDataSource
	 */
	public void setDataSource(DataSource dataSource)
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * This DAO Implementation fir method fetches hotDeals list.
	 * 
	 * @param hotDealsListRequest
	 *            of HotDealsListRequest
	 * @param screenName
	 *            as query parameter.
	 * @return HotDealsListResultSet
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public HotDealsListResultSet fetchHotDealsDetails(HotDealsListRequest hotDealsListRequest, String screenName) throws ScanSeeException
	{
		final String methodName = "fetchHotDealsDetails";
		log.info(ApplicationConstants.METHODSTART + methodName + "requested userid is -->" + hotDealsListRequest.getUserId());
		List<HotDealsResultSet> hotDealsListResponselst = null;
		List<Category> arCategoryList = null;
		HotDealsListResultSet hotDealsListResultSet = null;
		String postalcode = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_HotDealSearchPagination");
			simpleJdbcCall.returningResultSet("hotDealslst", new BeanPropertyRowMapper<HotDealsResultSet>(HotDealsResultSet.class));
			simpleJdbcCall.returningResultSet("categoryList", new BeanPropertyRowMapper<Category>(Category.class));
			final MapSqlParameterSource fetchHotDealsParameters = new MapSqlParameterSource();
			fetchHotDealsParameters.addValue(ApplicationConstants.USERID, hotDealsListRequest.getUserId());
			/**
			 * This is commented now because ,we implemented only for local
			 * database. It has other options like Internet,All are not implemented.
			 */
			fetchHotDealsParameters.addValue("Search", hotDealsListRequest.getSearchItem());
			fetchHotDealsParameters.addValue("ScreenName", screenName);
			fetchHotDealsParameters.addValue("Category", hotDealsListRequest.getCategory());
			fetchHotDealsParameters.addValue("LowerLimit", hotDealsListRequest.getLastVisitedProductNo());
			fetchHotDealsParameters.addValue("PopulationCentreID", hotDealsListRequest.getPopulationCentreID());

			/**
			 * Calling utility function get postal code by passing latitude and longitude.
			 */
			if (hotDealsListRequest.getLatitude() != null && hotDealsListRequest.getLongitude() != null) {
				String latlong = hotDealsListRequest.getLatitude() + "," + hotDealsListRequest.getLongitude();
				postalcode = Utility.getGeocodePostalcode(latlong.trim());
				fetchHotDealsParameters.addValue("PostalCode", postalcode);
			} else {
				fetchHotDealsParameters.addValue("PostalCode", postalcode);
			}
			fetchHotDealsParameters.addValue("Latitude", hotDealsListRequest.getLatitude());
			fetchHotDealsParameters.addValue("Longitude", hotDealsListRequest.getLongitude());
			fetchHotDealsParameters.addValue("Radius", hotDealsListRequest.getRadius());
			fetchHotDealsParameters.addValue(ApplicationConstants.ERRORNUMBER, null);
			fetchHotDealsParameters.addValue(ApplicationConstants.ERRORMESSAGE, null);

			//For user tracking
			fetchHotDealsParameters.addValue(ApplicationConstants.MAINMENUID, hotDealsListRequest.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchHotDealsParameters);
			if (null != resultFromProcedure)
			{
				hotDealsListResultSet = new HotDealsListResultSet();
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					hotDealsListResponselst = (List<HotDealsResultSet>) resultFromProcedure.get("hotDealslst");
					arCategoryList = (List<Category>) resultFromProcedure.get("categoryList");
					final Boolean favCat = (Boolean) resultFromProcedure.get("FavCatFlag");
					final Boolean categoryFlag = (Boolean) resultFromProcedure.get("ByCategoryFlag");
					if (null != hotDealsListResponselst && !hotDealsListResponselst.isEmpty() )
					{
						hotDealsListResultSet.setHotDealsListResponselst(hotDealsListResponselst);
						hotDealsListResultSet.setCategory(arCategoryList);
						final Boolean nextpage = (Boolean) resultFromProcedure.get("NxtPageFlag");
						if (nextpage != null)
						{
							if (nextpage) {
								hotDealsListResultSet.setNextPage(1);
							} else {
								hotDealsListResultSet.setNextPage(0);
							}
						}
						if (favCat != null) {
							if (favCat) {
								hotDealsListResultSet.setFavCat(1);

							} else {
								hotDealsListResultSet.setFavCat(0);
							}
						}
					}
					if (favCat != null)
					{
						if (favCat)
						{
							hotDealsListResultSet.setFavCat(1);
						} else {
							hotDealsListResultSet.setFavCat(0);
						}
					}
					if (categoryFlag != null)
					{
						if (categoryFlag)
						{
							hotDealsListResultSet.setCategoryFlag(1);
						} else {
							hotDealsListResultSet.setCategoryFlag(0);
						}
					}
				}
			}
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_HotDealSearchPagination Store Procedure error number: {}" + errorNum + "  and error message: {}" + errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in fetchHotDealsDetails", exception);
			throw new ScanSeeException(exception);
		} catch (Exception exception) {
			log.error("Exception occurred in fetchHotDealsDetails", exception);
			throw new ScanSeeException(exception);
		}
		log.info("Response from server is " + hotDealsListResponselst);
		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsListResultSet;
	}

	/**
	 * This DAO Implementation method fetches each hot deals information.
	 * 
	 * @param userId
	 *            as query parameter
	 * @param hotDealId
	 *            as query parameter
	 * @return HotDealsListResponse
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("unchecked")
	@Override
	public HotDealsCategoryInfo getHotDealProductDetail(Integer userId, Integer hotDealId, Integer hotDealListID) throws ScanSeeException
	{
		final String methodName = "getHotDealProductDetail in DAO layer";
		final List<HotDealsDetails> hotDealsListResponselst;
		log.info(ApplicationConstants.METHODSTART + methodName + "Requested user id is -->" + userId);
		HotDealsCategoryInfo hotDealsCategoryInfolst = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_HotDealDetails");
			simpleJdbcCall.returningResultSet("HotDealDetails", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue("UserID", userId);
			fetchProductDetailsParameters.addValue("HotDealID",	hotDealId);
			//For user tracking
			fetchProductDetailsParameters.addValue("HotDealListID", hotDealListID);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get("ErrorNumber"))
				{
					hotDealsListResponselst = (List<HotDealsDetails>) resultFromProcedure.get("HotDealDetails");
					if (null != hotDealsListResponselst && !hotDealsListResponselst.isEmpty())
					{
						//hotDealsListResponselst.get(0).setHotDealListID(hotDealListID);
						hotDealsCategoryInfolst = new HotDealsCategoryInfo();
						hotDealsCategoryInfolst.setHotDealsDetailsArrayLst((ArrayList<HotDealsDetails>) hotDealsListResponselst);
					}
					else
					{
						log.info("No Products found for the search");
						return hotDealsCategoryInfolst;
					}
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred  in usp_HotDealDetails Store Procedure error number: {} " + errorNum + " and  error message: {}"
							+ errorMsg);
					throw new ScanSeeException(errorMsg);
				}

			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getHotDealProductDetail ", exception);
			throw new ScanSeeException(exception);
		}
		log.info("Response frm server in getHotDealProductDetail " + hotDealsCategoryInfolst);
		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsCategoryInfolst;
	}

	/**
	 * This DAO Implementation method removes hot deals .
	 * 
	 * @param hotDealsListRequest
	 *            as query parameter
	 * @return response based on success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@Override
	public String removeHDProduct(HotDealsListRequest hotDealsListRequest) throws ScanSeeException
	{
		final String methodName = "removeHDProduct in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName + "requested user id is -->" + hotDealsListRequest.getUserId());
		String responseFromProc = null;
		Integer fromProc = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_HotDealInterest");
			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("UserID", hotDealsListRequest.getUserId());
			scanQueryParams.addValue("HotDealID", hotDealsListRequest.getHotDealId());
			scanQueryParams.addValue("Interested", hotDealsListRequest.gethDInterested());
			scanQueryParams.addValue("InterestDate", Utility.getFormattedDate());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);
			fromProc = (Integer) resultFromProcedure.get("Status");
			if (fromProc == 0)
			{
				responseFromProc = ApplicationConstants.SUCCESS;
			}
			else
			{
				responseFromProc = ApplicationConstants.FAILURE;
			}
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.info("Error Occured in removeHDProduct method ..errorNum..." + errorNum + "errorMsg.." + errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in removeHDProduct", exception);
			throw new ScanSeeException(exception);
		}
		catch (ParseException exception)
		{
			log.error("Exception occurred in removeHDProduct", exception);
			throw new ScanSeeException(exception);
		}
		log.info("Response frm removeHDProduct " + responseFromProc);
		log.info(ApplicationConstants.METHODEND + methodName);
		return responseFromProc;
	}

	/**
	 * This DAO Implementation method for getting hot deals category list . No
	 * query parameter.
	 * 
	 * @param userId
	 *            as request parameter
	 * @return response based on success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<HotDealsCategoryInfo> getHdCategoryDetail(Integer userId) throws ScanSeeException
	{
		final String methodName = "getHdCategoryDetail in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsCategoryInfo> hotDealsCategorylst = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_HotDealsCategoryList");
			simpleJdbcCall.returningResultSet("HotDealCategorylist", new BeanPropertyRowMapper<HotDealsCategoryInfo>(HotDealsCategoryInfo.class));

			final MapSqlParameterSource hotDealCategoryQueryParams = new MapSqlParameterSource();
			hotDealCategoryQueryParams.addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(hotDealCategoryQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					hotDealsCategorylst = (List<HotDealsCategoryInfo>) resultFromProcedure.get("HotDealCategorylist");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_HotDealsCategoryList  Store Procedure error number: {} " + errorNum + " and error  message: {}"
							+ errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in  getHotDealProductDetail", exception);
			throw new ScanSeeException(exception);
		}
		log.info("Response frm server in  getHotDealProductDetail" + hotDealsCategorylst);
		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsCategorylst;
	}

	/**
	 * This DAO Implementation method for getting hot deals population centres .
	 * No query parameter.
	 * 
	 * @return response based on success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public List<HotDealsCategoryInfo> getHdPopulationCenters(Integer userId) throws ScanSeeException
	{
		final String methodName = "getHdPopulationCenters in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsCategoryInfo> hotDealsCategorylst = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_HotDealPopulationCentres");
			simpleJdbcCall.returningResultSet("HotDealCategorylist", new BeanPropertyRowMapper<HotDealsCategoryInfo>(HotDealsCategoryInfo.class));

			final MapSqlParameterSource hotDealPopulationcenterQueryParams = new MapSqlParameterSource();
			hotDealPopulationcenterQueryParams.addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(hotDealPopulationcenterQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					hotDealsCategorylst = (List<HotDealsCategoryInfo>) resultFromProcedure.get("HotDealCategorylist");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_HotDealPopulationCentres Store Procedure error number: {} " + errorNum + " and error message: {}"
							+ errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getHotDealProductDetail", exception);
			throw new ScanSeeException(exception);
		}
		log.info("Response frm server in getHotDealProductDetail" + hotDealsCategorylst);
		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsCategorylst;
	}

	/**
	 * This DAO Implementation method for getting hot deals population centres .
	 * No query parameter.
	 * 
	 * @param dmaName
	 *            as request parameter
	 * @return response based on success or failure.
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<HotDealsCategoryInfo> searchHdPopulationCenters(String dmaName, Integer userId) throws ScanSeeException
	{
		final String methodName = "getHdPopulationCenters in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		List<HotDealsCategoryInfo> hotDealsCategorylst = null;

		try
		{

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_HotDealSearchPopulationCenter");
			simpleJdbcCall.returningResultSet("HotDealCategorylist", new BeanPropertyRowMapper<HotDealsCategoryInfo>(HotDealsCategoryInfo.class));

			final MapSqlParameterSource scanQueryParams = new MapSqlParameterSource();
			scanQueryParams.addValue("PopulationCenter", dmaName);
			scanQueryParams.addValue("UserID", userId);
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(scanQueryParams);

			if (null != resultFromProcedure)
			{
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					hotDealsCategorylst = (List<HotDealsCategoryInfo>) resultFromProcedure.get("HotDealCategorylist");
				}
				else
				{
					final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
					final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
					log.error("Error occurred in usp_HotDealSearchPopulationCenter Store Procedure error number: {} " + errorNum + " and error message: {}"
							+ errorMsg);
					throw new ScanSeeException(errorMsg);
				}
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getHotDealProductDetail", exception);
			throw new ScanSeeException(exception);
		}
		log.info("Response frm server in getHotDealProductDetail" + hotDealsCategorylst);
		log.info(ApplicationConstants.METHODEND + methodName);
		return hotDealsCategorylst;
	}

	/**
	 * The DAO method to update hot deal click impression to database for user tracking
	 * 
	 * @param userId
	 *            as Query parameter
	 * @param hotDealId
	 *            as Query parameter
	 * @return String SUCCESS or FAILURE
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	@Override
	public String UserTrackingGetHotDealClick(Integer userId, Integer hotDealId, Integer hotDealListId) throws ScanSeeException {
		final String methodName = "UserTrackingGetHotDealClick in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName + "Requested user id is -->" + userId);
		String response;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_UserTrackingHotDealGetDealClick");
			simpleJdbcCall.returningResultSet("HotDealDetails", new BeanPropertyRowMapper<HotDealsDetails>(HotDealsDetails.class));

			final MapSqlParameterSource fetchProductDetailsParameters = new MapSqlParameterSource();
			fetchProductDetailsParameters.addValue("UserID", userId);
			fetchProductDetailsParameters.addValue("HotDealID",	hotDealId);
			fetchProductDetailsParameters.addValue("HotDealListID", hotDealListId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchProductDetailsParameters);

			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				response = ApplicationConstants.SUCCESS;
			}
			else	{
				response = ApplicationConstants.FAILURE;
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getHotDealProductDetail ", exception);
			throw new ScanSeeException(exception);
		}
		return null;
	}

	
	
	/**
	 * This DAO Implementation for list out the categories of the hot deals.
	 * 
	 * @param objHDListReq of HotDealsListRequest
	 * @return HotDealsListResultSet
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	@SuppressWarnings("unchecked")
	public HotDealsListResultSet getHDProdByCategory(HotDealsListRequest objHDListReq) throws ScanSeeException
	{
		final String methodName = "getHDProdByCategory";
		List<HotDealsResultSet> hDResponselst = null;
		HotDealsListResultSet hDListResultSet = null;
		String postalcode = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_HotDealCategoryDisplay");
			simpleJdbcCall.returningResultSet("hDByCategorylst", new BeanPropertyRowMapper<HotDealsResultSet>(HotDealsResultSet.class));
			final MapSqlParameterSource fetchHDByCategoryParam = new MapSqlParameterSource();
			fetchHDByCategoryParam.addValue(ApplicationConstants.USERID, objHDListReq.getUserId());
			fetchHDByCategoryParam.addValue("Search", objHDListReq.getSearchItem());
			fetchHDByCategoryParam.addValue("Category", objHDListReq.getCategory());
			fetchHDByCategoryParam.addValue("PopulationCentreID", objHDListReq.getPopulationCentreID());

			/**
			 * Calling utility function get postal code by passing latitude and longitude.
			 */
			if (objHDListReq.getLatitude() != null && objHDListReq.getLongitude() != null)
			{
				String latlong = objHDListReq.getLatitude() + "," + objHDListReq.getLongitude();
				postalcode = Utility.getGeocodePostalcode(latlong.trim());
				fetchHDByCategoryParam.addValue("PostalCode", postalcode);
			} else {
				fetchHDByCategoryParam.addValue("PostalCode", postalcode);
			}

			fetchHDByCategoryParam.addValue("Latitude", objHDListReq.getLatitude());
			fetchHDByCategoryParam.addValue("Longitude", objHDListReq.getLongitude());
			fetchHDByCategoryParam.addValue("Radius", objHDListReq.getRadius());
			fetchHDByCategoryParam.addValue(ApplicationConstants.ERRORNUMBER, null);
			fetchHDByCategoryParam.addValue(ApplicationConstants.ERRORMESSAGE, null);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchHDByCategoryParam);
			if (null != resultFromProcedure)
			{
				hDListResultSet = new HotDealsListResultSet();
				if (null == resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
				{
					hDResponselst = (List<HotDealsResultSet>) resultFromProcedure.get("hDByCategorylst");
					if (null != hDResponselst && !hDResponselst.isEmpty())
					{
						hDListResultSet.setHotDealsListResponselst(hDResponselst);
					}
				}
			}
			if (null != resultFromProcedure.get(ApplicationConstants.ERRORNUMBER))
			{
				final Integer errorNum = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
				final String errorMsg = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Error occurred in usp_HotDealCategoryDisplay Store Procedure error number: {}" + errorNum + "  and error message: {}" + errorMsg);
				throw new ScanSeeException(errorMsg);
			}
		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in getHDProdByCategory", exception);
			throw new ScanSeeException(exception);
		}
		catch (Exception exception)
		{
			log.error("Exception occurred in getHDProdByCategory", exception);
			throw new ScanSeeException(exception);
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return hDListResultSet;
	}

	/**
	 * This DAO Method to claim hot deal.
	 * Method Type: POST
	 * 
	 * @param objHotDealListRequest containing userId, hotDealId and mainMenuID
	 * 
	 * @return XMl containing SUCCESS or FAILURE message
	 * @throws ScanSeeException is thrown 
	 */
	@Override
	public String hotDealClaim(HotDealsListRequest objHotDealListRequest) throws ScanSeeException {
		final String methodName = "hotDealClaim in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_HotDealClip");

			final MapSqlParameterSource fetchHotDealClaim = new MapSqlParameterSource();
			fetchHotDealClaim.addValue("UserID", objHotDealListRequest.getUserId());
			fetchHotDealClaim.addValue("HotDealId",	objHotDealListRequest.getHotDealId());
			fetchHotDealClaim.addValue("MainMenuId", objHotDealListRequest.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchHotDealClaim);
			final Integer errorNumber = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);
			
			if (null == errorNumber)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else	{
				response = ApplicationConstants.FAILURE;
				
				final String errorMessage = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Exception occurred in usp_HotDealRedeem " + ApplicationConstants.ERRORNUMBER + ": " + errorNumber + ApplicationConstants.ERRORMESSAGE + ": " + errorMessage);
			
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in usp_HotDealClip ", exception);
			throw new ScanSeeException(exception);
		}
		catch(Exception exception)	{
			log.error("Exception occurred in usp_HotDealClip ", exception);
			throw new ScanSeeException(exception);
		}
		return response;
	}

	/**
	 * This DAO Method to redeem hot deal.
	 * Method Type: POST
	 * 
	 * @param objHotDealListRequest containing userId, hotDealId and mainMenuID
	 * @return XMl containing SUCCESS or FAILURE message
	 * @throws ScanSeeException is thrown 
	 */
	@Override
	public String hotDealRedeem(HotDealsListRequest objHotDealListRequest) throws ScanSeeException {
		final String methodName = "hotDealRedeem in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_HotDealRedeem");

			final MapSqlParameterSource fetchHotDealClaim = new MapSqlParameterSource();
			fetchHotDealClaim.addValue(ApplicationConstants.USERID, objHotDealListRequest.getUserId());
			fetchHotDealClaim.addValue("HotDealId",	objHotDealListRequest.getHotDealId());
			fetchHotDealClaim.addValue("MainMenuId", objHotDealListRequest.getMainMenuID());

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(fetchHotDealClaim);
			final Integer errorNumber = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);

			if (null == errorNumber)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else	{
				response = ApplicationConstants.FAILURE;
				
				final String errorMessage = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Exception occurred in usp_HotDealRedeem " + ApplicationConstants.ERRORNUMBER + ": " + errorNumber + ApplicationConstants.ERRORMESSAGE + ": " + errorMessage);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in usp_HotDealRedeem ", exception);
			throw new ScanSeeException(exception);
		}
		catch (Exception exception)	{
			log.error("Exception occurred in usp_HotDealRedeem ", exception);
			throw new ScanSeeException(exception);
		}
		return response;
	}

	/**
	 * This is a RestEasy WebService Method to delete used hot deal.
	 * Method Type: POST
	 * 
	 * @param hdGallId as request
	 * @return XMl containing SUCCESS or FAILURE message 
	 * @throws ScanSeeException is thrown.
	 */
	@Override
	public String deleteGalleryUsedHotDeals(Integer hdGallId) throws ScanSeeException {
		final String methodName = "deleteUsedHotDeals in DAO layer";
		log.info(ApplicationConstants.METHODSTART + methodName);
		String response = null;
		try
		{
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_GalleryDeleteUsedHotDeals");

			final MapSqlParameterSource deleteHotdeal = new MapSqlParameterSource();
			deleteHotdeal.addValue("UserHotDealGalleryID", hdGallId);

			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(deleteHotdeal);
			final Integer errorNumber = (Integer) resultFromProcedure.get(ApplicationConstants.ERRORNUMBER);

			if (null == errorNumber)
			{
				response = ApplicationConstants.SUCCESS;
			}
			else	{
				response = ApplicationConstants.FAILURE;
				
				final String errorMessage = (String) resultFromProcedure.get(ApplicationConstants.ERRORMESSAGE);
				log.error("Exception occurred in SP usp_GalleryDeleteUsedHotDeals " + ApplicationConstants.ERRORNUMBER + ": " + errorNumber + ApplicationConstants.ERRORMESSAGE + ": " + errorMessage);
			}

		}
		catch (DataAccessException exception)
		{
			log.error("Exception occurred in usp_HotDealRedeem ", exception);
			throw new ScanSeeException(exception);
		}
		catch (Exception exception)	{
			log.error("Exception occurred in usp_HotDealRedeem ", exception);
			throw new ScanSeeException(exception);
		}
		return response;
	}
}
