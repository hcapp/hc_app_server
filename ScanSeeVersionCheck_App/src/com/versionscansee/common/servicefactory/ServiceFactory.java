package com.versionscansee.common.servicefactory;

import com.versionscansee.firstuse.service.FirstUseService;
import com.versionscansee.firstuse.service.FirstUseServiceImpl;

/**
 * The class is for handling user defined ScanSeeException.
 * 
 * @author =
 */

public class ServiceFactory
{
	/**
	 * Constructor.
	 */
	private ServiceFactory()
	{
	}

	/**
	 * @return the FirstUseService
	 */
	public static FirstUseService getFirstUseService()
	{
		return (FirstUseService) ScanSeeServices.getBean("firstUseService", FirstUseServiceImpl.class);
	}

}
