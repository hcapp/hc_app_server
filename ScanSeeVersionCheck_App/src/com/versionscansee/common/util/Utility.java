package com.versionscansee.common.util;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.versionscansee.common.constants.ApplicationConstants;

/**
 * The Utility class common methods used in the application Includes methods for
 * forming ResponseXML,getting formatted date.
 * 
 * @author manjunatha_gh.
 */

public class Utility {

	/**
	 * Getting the logger instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(Utility.class);

	/**
	 * variable for checking Debugging.
	 */
	private static final boolean ISDEBUGENABLED = LOG.isDebugEnabled();
	/**
	 * The constant for &#x0.
	 */

	private static final char[] NULL = "&#x0;".toCharArray();

	/**
	 * The constant for &amp.
	 */

	private static final char[] AMP = "&amp;".toCharArray();

	/**
	 * The constant for &lt.
	 */

	private static final char[] LT = "&lt;".toCharArray();

	/**
	 * The constant for &gt.
	 */

	private static final char[] GT = "&gt;".toCharArray();

	/**
	 * The constant for &quot.
	 */

	private static final char[] QUOT = "&quot;".toCharArray();

	/**
	 * The constant for &apos.
	 */

	private static final char[] APOS = "&apos;".toCharArray();

	/**
	 * The constant for HTML Praragraph opening tag.
	 */

	private static final char[] HTMLPARAGRAPHOPEN = "<p>".toCharArray();
	/**
	 * The constant for HTML Praragraph Closing tag.
	 */

	private static final char[] HTMLPARAGRAPHCLOSE = "</p>".toCharArray();
	/**
	 * The constant for HTML Bold Opening tag.
	 */
	private static final char[] HTMLBOLDOPEN = "<b>".toCharArray();

	/**
	 * The constant for HTML Bold Closing tag.
	 */
	private static final char[] HTMLBOLDCLOSE = "</b>".toCharArray();
	/**
	 * The constant for HTML Break tag.
	 */
	private static final char[] HTMLBREAKCLOSE = "<br/>".toCharArray();

	/**
	 * The constant for HTML Break tag.
	 */
	private static final char[] HTMLBREAK = "<br>".toCharArray();

	/**
	 * The constant for HTML List Open tag.
	 */
	private static final char[] HTMLLISTITEMOPEN = "<li>".toCharArray();
	/**
	 * The constant for HTML List closing tag.
	 */
	private static final char[] HTMLLISTITEMCLOSE = "</li>".toCharArray();
	/**
	 * The constant for HTML Unordered List Opening tag.
	 */
	private static final char[] HTMUNORDEREDLISTOPEN = "<ul>".toCharArray();

	/**
	 * The constant for HTML Unordered List Closing tag.
	 */
	private static final char[] HTMLUNORDEREDLISTCLOSE = "</ul>".toCharArray();

	/**
	 * The method to remove HTML tags.
	 * 
	 * @param inPutXml
	 *            The input xml.
	 * @return the xml with special charcters removed.
	 */

	public static String removeHTMLTags(String inPutXml) {

		final String methodName = "removeHTMLTags";

		String outPutxML = "";
		if (null != inPutXml) {
			outPutxML = null;
			outPutxML = inPutXml.replaceAll(new String(HTMLPARAGRAPHOPEN), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLPARAGRAPHCLOSE), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLBREAK), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLBREAKCLOSE), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLLISTITEMOPEN), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLLISTITEMCLOSE), "");
			outPutxML = outPutxML.replaceAll(new String(HTMUNORDEREDLISTOPEN), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLUNORDEREDLISTCLOSE), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLBOLDOPEN), "");
			outPutxML = outPutxML.replaceAll(new String(HTMLBOLDCLOSE), "");
		}

		return outPutxML;
	}

	/**
	 * this method returns the xml String for the given Values.
	 * 
	 * @param errorCode
	 *            The error code.
	 * @param errorResponse
	 *            The error response.
	 * @return response with formed response.
	 */

	public static String formResponseXml(String errorCode, String errorResponse) {
		final String methodName = "formResponseXml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuilder response = new StringBuilder();
		response.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		response.append("<response>");
		response.append("<responseCode>");
		response.append(errorCode);
		response.append("</responseCode>");
		response.append("<responseText>");
		response.append(errorResponse);
		response.append("</responseText>");
		response.append("</response>");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response.toString();
	}

	/**
	 * this method returns the xml String for the given Values.
	 * 
	 * @param errorCode
	 *            The error code.
	 * @param errorResponse
	 *            The error response.
	 * @param addElementName
	 *            The element name.
	 * @param addElementValue
	 *            The element value.
	 * @return response with formed response.
	 */

	public static String formResponseXml(String errorCode, String errorResponse, String addElementName, String addElementValue) {

		final String methodName = "formResponseXml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuilder response = new StringBuilder();
		response.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		response.append("<response>");
		response.append("<responseCode>");
		response.append(errorCode);
		response.append("</responseCode>");
		response.append("<responseText>");
		response.append(errorResponse);
		response.append("</responseText>");
		response.append("<" + addElementName + ">");
		response.append(addElementValue);
		response.append("</" + addElementName + ">");
		response.append("</response>");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response.toString();
	}

	/**
	 * this method returns the xml String for the given Values.
	 * 
	 * @param errorCode
	 *            The error code.
	 * @param errorResponse
	 *            The error response.
	 * @param addElementName
	 *            The element name.
	 * @param addElementValue
	 *            The element value.
	 * @return response with formed response.
	 */

	public static String formResponseXml(String errorCode, String errorResponse, String addElementName, String addElementValue,
			String addElementName2, String addElementValue2) {

		final String methodName = "formResponseXml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final StringBuilder response = new StringBuilder();
		response.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		response.append("<response>");
		response.append("<responseCode>");
		response.append(errorCode);
		response.append("</responseCode>");
		response.append("<responseText>");
		response.append(errorResponse);
		response.append("</responseText>");
		response.append("<" + addElementName + ">");
		response.append(addElementValue);
		response.append("</" + addElementName + ">");
		response.append("<" + addElementName2 + ">");
		response.append(addElementValue2);
		response.append("</" + addElementName2 + ">");
		response.append("</response>");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response.toString();
	}

	/**
	 * The method to get the current date formated in yyyy-MM-dd HH:mm:ss
	 * format.
	 * 
	 * @return The current date formatted in yyyy-MM-dd HH:mm:ss forma
	 * @throws java.text.ParseException
	 *             The exception of type java.text.ParseException
	 */

	public static java.sql.Timestamp getFormattedDate() throws java.text.ParseException {
		final String methodName = "getFormattedDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final Date date = new Date();
		java.sql.Timestamp sqltDate = null;
		/*
		 * formatting the current date.
		 */
		// final DateFormat formater = new SimpleDateFormat("yyyy-dd-MM");
		final DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
		java.util.Date parsedUtilDate;
		parsedUtilDate = formater.parse(formater.format(date));
		sqltDate = new java.sql.Timestamp(parsedUtilDate.getTime());
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return sqltDate;
	}

	/**
	 * The method to get the current date formated in yyyy-MM-dd HH:mm:ss
	 * format.
	 * 
	 * @param dbdate
	 *            -As parameter
	 * @return The current date formatted in yyyy-MM-dd HH:mm:ss forma
	 */

	public static String convertDBdate(String dbdate) {
		String javaDate = null;
		try {

			final String methodName = "convertDBdate";
			LOG.info(ApplicationConstants.METHODSTART + methodName);
			SimpleDateFormat formatter, FORMATTER;
			// formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date = formatter.parse(dbdate);
			FORMATTER = new SimpleDateFormat("MM-dd-yyyy");
			LOG.info("NewDate-->" + FORMATTER.format(date));
			javaDate = FORMATTER.format(date);
		} catch (ParseException exception) {
			LOG.info("Exception in convertDBdate method" + exception.getMessage());
			return null;
		}
		return javaDate;
	}

	/**
	 * The method for formatting the date in yyyy-MM-dd HH:mm:ss format.
	 * 
	 * @param userDate
	 *            -As parameter
	 * @return The date formatted in yyyy-MM-dd HH:mm:ss format.
	 * @throws java.text.ParseException
	 *             - exception.
	 */

	public static Date getFormattedUserDate(String userDate) throws java.text.ParseException {

		final String methodName = "getFormattedUserDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName + userDate);

		java.util.Date utilDate = null;
		final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		utilDate = dateFormat.parse(userDate);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new java.sql.Timestamp(utilDate.getTime());

	}

	/**
	 * The method for formatting the date in yyyy-MM-dd HH:mm:ss format.
	 * 
	 * @param userDate
	 *            date to be formated.
	 * @return The date formatted in yyyy-MM-dd HH:mm:ss format.
	 * @throws java.text.ParseException
	 *             excpetion.
	 */
	public static Date getFormattedUserDateNoTimeStamp(String userDate) throws java.text.ParseException {

		final String methodName = "getFormattedUserDateNoTimeStamp";
		LOG.info(ApplicationConstants.METHODSTART + methodName + userDate);

		java.util.Date utilDate = null;
		final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		utilDate = dateFormat.parse(userDate);
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return new java.sql.Timestamp(utilDate.getTime());
	}

	/**
	 * The method for checking if a date is valid.
	 * 
	 * @param inDate
	 *            The date to be validated.
	 * @return true or false based on validation result.
	 */

	public static boolean isValidDate(String inDate) {

		final String methodName = "isValidDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName + "date" + inDate);

		final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/DD/yyyy");

		try {
			// parse the inDate parameter
			dateFormat.parse(inDate.trim());
		} catch (ParseException pe) {
			return false;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return true;
	}

	/**
	 * The method to trim the whitespaces of a string.
	 * 
	 * @param inputString
	 *            The string which needs to be trimmed.
	 * @return The trimmed string.
	 */

	public static String trimString(String inputString) {

		final String methodName = "trimString";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (null != inputString && "".equals(inputString)) {
			inputString = inputString.trim();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return inputString;
	}

	/**
	 * The method to remove the special characters.
	 * 
	 * @param inPutXml
	 *            The input xml.
	 * @return the xml with special charcters removed.
	 */

	public static String removeSpecialChars(String inPutXml) {

		final String methodName = "removeSpecialChars";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String outPutxML = "";
		if (null != inPutXml) {
			outPutxML = null;
			outPutxML = inPutXml.replaceAll(new String(AMP), "&");
			outPutxML = outPutxML.replaceAll(new String(GT), ">");
			outPutxML = outPutxML.replaceAll(new String(LT), "<");
			outPutxML = outPutxML.replaceAll(new String(NULL), "null");
			outPutxML = outPutxML.replaceAll(new String(QUOT), "\"\"");
			outPutxML = outPutxML.replaceAll(new String(APOS), "'");
			outPutxML = outPutxML.replaceAll("\n", "");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return outPutxML;
	}

	/**
	 * This method is used to remove special characters.
	 * 
	 * @param inPutXml
	 *            -As parameter
	 * @return outPutxML
	 */
	public static String removeSpecialCharsReverse(String inPutXml) {

		final String methodName = "removeSpecialChars";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		String outPutxML = "";
		if (null != inPutXml) {
			outPutxML = null;
			outPutxML = inPutXml.replaceAll("&", new String(AMP));
			// outPutxML = outPutxML.replaceAll( "<" , new String(LT));
			/*
			 * outPutxML = outPutxML.replaceAll(new String(GT), ">"); outPutxML
			 * = outPutxML.replaceAll(new String(LT), "<"); outPutxML =
			 * outPutxML.replaceAll(new String(NULL), "null"); outPutxML =
			 * outPutxML.replaceAll(new String(QUOT), "\"\""); outPutxML =
			 * outPutxML.replaceAll(new String(APOS), "'");
			 */
			// outPutxML = outPutxML.replaceAll("\n", "");
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return outPutxML;
	}

	/**
	 * The method for null checks.
	 * 
	 * @param arg
	 *            the input string
	 * @return the response string.
	 */

	public static String nullCheck(String arg) {

		final String methodName = "nullCheck";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (null != arg && !"".equals(arg)) {
			arg.trim();
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arg;
	}

	/**
	 * This method is used to check nulls.
	 * 
	 * @param arg
	 *            -As parameter
	 * @return arg
	 */
	public static String isNull(String arg) {

		final String methodName = "nullCheck";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (null != arg && !"".equals(arg)) {
			arg.trim();
		} else {
			arg = "";
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arg;
	}

	/**
	 * This method is used to check Empty strings or "".
	 * 
	 * @param arg
	 *            -As parameter
	 * @return arg
	 */
	public static boolean isEmptyOrNullString(String arg) {

		final String methodName = "isEmptyString";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final boolean emptyString;

		if (null == arg || "".equals(arg.trim())) {
			emptyString = true;
		} else {
			emptyString = false;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emptyString;
	}

	/**
	 * The method for forming ForgotPassword Email Template HTMl.
	 * 
	 * @param password
	 *            The password.
	 * @return mailData The email template for Forgot password.
	 */

	public static String formForgotPasswordEmailTemplateHTML(String password) {
		final String methodName = "formForgotPasswordEmailTemplateHTML";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final String mailData = "<html><head></head><body><p>Here is the account information that you requested.<br/>Your password: "
				+ "<b>"
				+ password
				+ "</b><br/><br/>Log in now:<br/>http://www.scansee.com or through your application.<br/><br/>Please note:  For your account security, we only provide your password in this email. "
				+ "Once you have logged in to your account, you can change your password. To keep your account secure, we recommend that you delete this email and keep a copy of your log-in name"
				+ " and password in a secure place.<br/><br/>If you have any additional questions about your account log-in, reply to this email or email us at support@hubciti.com. For the fastest response, please include the text of this message along with your"
				+ " question.<br/><br/></p><p>Sincerely,<br/>Scansee Support</p></body></html>";

		LOG.info(ApplicationConstants.METHODEND + methodName);

		return mailData;
	}

	/**
	 * This method takes any number and sets precision of 2 decimal with
	 * rounding.
	 * 
	 * @param value
	 *            values which will be formated.
	 * @return value formated values.
	 */
	public static String formatDecimalValue(String value) {
		final String methodName = "formatDecimalValue";
		if (ISDEBUGENABLED) {
			LOG.info(ApplicationConstants.METHODSTART + methodName);
		}
		if (null != value && !"".equals(value) && !(value.equals(ApplicationConstants.NOTAPPLICABLE))) {
			final Double price = new Double(value);
			final DecimalFormat df = new DecimalFormat("#########.00");
			value = df.format(price);
		} else {
			value = ApplicationConstants.NOTAPPLICABLE;
		}
		if (ISDEBUGENABLED) {
			LOG.info(ApplicationConstants.METHODEND + methodName);
		}
		return value;

	}

	/**
	 * this method returns the xml String for the given Values.
	 * 
	 * @param errorCode
	 *            The error code.
	 * @param errorResponse
	 *            The error response.
	 * @param responseMap
	 *            The response map.
	 * @return response with formed response.
	 */

	public static String formResponseXml(String errorCode, String errorResponse, Map<String, String> responseMap) {

		final String methodName = "formResponseXml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Iterator<String> itr = responseMap.keySet().iterator();
		final StringBuilder response = new StringBuilder();
		response.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		response.append("<response>");
		response.append("<responseCode>");
		response.append(errorCode);
		response.append("</responseCode>");
		response.append("<responseText>");
		response.append(errorResponse);
		response.append("</responseText>");
		while (itr.hasNext()) {
			String key = itr.next();
			String value = responseMap.get(key);
			response.append("<" + key + ">");
			response.append(value);
			response.append("</" + key + ">");
		}
		response.append("</response>");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response.toString();
	}

	/**
	 * The method to get the current date formated in yyyy-MM-dd HH:mm:ss
	 * format.
	 * 
	 * @return The current date formatted in yyyy-MM-dd HH:mm:ss forma
	 */

	public static String getFormattedCurrentDate() {
		final String methodName = "getFormattedDate";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		final Date date = new Date();
		/*
		 * formatting the current date.
		 */
		String currentDateStr = null;
		try {
			final DateFormat formater = new SimpleDateFormat("MM-dd-yyyy");
			final Date parsedUtilDate = formater.parse(formater.format(date));
			currentDateStr = formater.format(parsedUtilDate);
		} catch (ParseException exception) {
			LOG.info("Exception in convertDBdate method" + exception.getMessage());
			return ApplicationConstants.NOTAPPLICABLE;
		}

		LOG.info(ApplicationConstants.METHODEND + methodName);
		return currentDateStr;
	}

	/**
	 * Thsi method is used to round the values to nearest value.
	 * 
	 * @param distance
	 *            -As String parameter
	 * @return distance rounded value
	 */
	public static String roundNearestValues(String distance) {
		if (null != distance && !"".equals(distance)) {
			try {
				final Float fltDistance = new Float(distance);
				distance = String.valueOf(Math.round(fltDistance));
			} catch (NumberFormatException exception) {
				LOG.info("Exception in roundNearestValues method" + exception.getMessage());
				distance = ApplicationConstants.NOTAPPLICABLE;
			}

		} else {
			distance = ApplicationConstants.NOTAPPLICABLE;
		}
		return distance;
	}

	/**
	 * This method is used to form xml.
	 * 
	 * @param rootTag
	 *            -The root tag
	 * @param responseMap
	 *            -As parameter
	 * @return response
	 */
	public static String formResponseXml(String rootTag, Map<String, String> responseMap) {
		final String methodName = "formResponseXml";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		Iterator<String> itr = responseMap.keySet().iterator();
		final StringBuilder response = new StringBuilder();
		response.append("<" + rootTag + ">");
		while (itr.hasNext()) {
			String key = itr.next();
			String value = responseMap.get(key);
			response.append("<" + key + ">");
			response.append(value);
			response.append("</" + key + ">");
		}
		response.append("</" + rootTag + ">");
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return response.toString();
	}

	/**
	 * The method for null checks.
	 * 
	 * @param arg
	 *            the input string
	 * @return the response string.
	 */

	public static String checkNull(String arg) {

		final String methodName = "nullCheck";
		LOG.info(ApplicationConstants.METHODSTART + methodName);

		if (null != arg && !"".equals(arg)) {
			arg.trim();
		} else {

		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return arg;
	}

	public static boolean isEmpty(String value) {
		final String methodName = "isEmptyString";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		final boolean emptyString;

		if ("".equals(value)) {
			emptyString = true;
		} else {
			emptyString = false;
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return emptyString;
	}

	/**
	 * Validate Email Id with regular expression
	 * 
	 * @param strEmailId
	 *            strEmailId for validation
	 * @return true valid strEmailId, false invalid strEmailId
	 */
	public static boolean validateEmailId(final String strEmailId) {

		Pattern pattern = null;
		Matcher matcher;
		final String EMAIL_PATTERN = ApplicationConstants.EMAIL_PATTERN;

		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(strEmailId);
		return matcher.matches();

	}

	public static void main(String[] args) {
		String shopMinPrice = "sdf";

		System.out.println("value is " + !"".equals(shopMinPrice));
	}

	public static boolean isEmptyString(String value) {

		final boolean emptyString;

		if ("".equals(value)) {
			emptyString = true;
		} else {
			emptyString = false;
		}

		return emptyString;
	}

	/**
	 * Method to Check if the String object is null
	 * 
	 * @param strValue
	 *            String
	 * @return strValue String
	 */
	public static String checkForNull(String strValue) {
		if (strValue == null || strValue.equals("null") || "".equals(strValue)) {
			return "";
		} else {
			return strValue.trim();
		}
	}

	private Utility() {

	}

}