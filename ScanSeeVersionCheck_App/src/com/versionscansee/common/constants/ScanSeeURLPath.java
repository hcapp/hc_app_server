package com.versionscansee.common.constants;

/**
 * The class having all constants which are used in the application.
 * 
 * @author team
 */
public class ScanSeeURLPath {

	// Base URL path parameters

	/**
	 * firstUseBaseURL declared as String for firstUse module.
	 */
	public static final String FIRSTUSEBASEURL = "/firstUse";

	/**
	 * AUTHENTICATE declared as String for firstUse module.
	 */
	public static final String VERSIONCHECK = "/versioncheck";
	/**
	 * VERSIONCHECKANDROID declared as String for firstUse Android.
	 */
	public static final String VERSIONCHECKANDROID = "/versioncheckAndroid";
	/**
	 * VERSIONCHECKHUBCITI declared as String for firstUse IOS.
	 */
	public static final String VERSIONCHECKHUBCITI = "/versioncheckhubciti";

	protected ScanSeeURLPath() {

	}

}
