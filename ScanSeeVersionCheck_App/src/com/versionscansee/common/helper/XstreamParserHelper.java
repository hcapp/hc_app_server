package com.versionscansee.common.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;
import com.versionscansee.common.exception.ScanSeeException;
import com.versionscansee.common.util.Utility;

/**
 * The class having xstream alias names for response xmls. This class provides
 * method for Marshalling and Unamarhalling.
 * 
 * @author manjunatha_gh
 */
public class XstreamParserHelper
{
	/**
	 * for getting logger.
	 */

	private final Logger log = LoggerFactory.getLogger(XstreamParserHelper.class);
	/**
	 * for xstream initializes with null.
	 */
	private static XStream xstream;

	/**
	 * This method for alias names for response xmls.
	 * 
	 * @return XStream
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */

	private static XStream getXStreamParser() throws ScanSeeException
	{
		try
		{
			if (xstream == null)
			{
				xstream = new XStream();
				// xstream.setClassLoader(com.scansee.common.pojos.BaseObject.class.getClassLoader());
				xstream.alias("AuthenticateUser", com.versionscansee.common.pojo.AuthenticateUser.class);

			}
		}
		catch (Exception exception)
		{
			throw new ScanSeeException(exception);
		}
		return xstream;

	}

	/**
	 * This method produces the response xml.
	 * 
	 * @param object
	 *            as requested xstream object.
	 * @return outPutXml as String
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public static String produceXMlFromObject(Object object) throws ScanSeeException
	{
		String outPutXml = "";
		xstream = getXStreamParser();
		outPutXml = xstream.toXML(object);
		return Utility.removeSpecialChars(outPutXml);
	}

	/**
	 * This method parses input xml to required object.
	 * 
	 * @param xml
	 *            as requested .
	 * @return Object
	 * @throws ScanSeeException
	 *             The exceptions are caught and a ScanSee Exception defined for
	 *             the application is thrown which is caught in the Controller
	 *             layer.
	 */
	public Object parseXmlToObject(String xml) throws ScanSeeException
	{
		log.info("In parseXmlToObject method");
		Object obj = null;
		try
		{
			xstream = getXStreamParser();

			obj = xstream.fromXML(xml);
		}
		catch (ClassCastException exception)
		{
			log.error("In ClassCastException:", exception);
			throw new ScanSeeException(exception);
		}
		catch (Exception exception)
		{
			log.error("In exception:", exception);
			throw new ScanSeeException(exception);
		}

		return obj;
	}

	/**
	 * Main method.
	 * 
	 * @param args
	 *            as the request. * @throws ScanSeeException The exceptions are
	 *            caught and a ScanSee Exception defined for the application is
	 *            thrown which is caught in the Controller layer.
	 */
	public static void main(String[] args) throws ScanSeeException
	{

		System.out.println("hai");

	}
}
