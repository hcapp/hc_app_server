package com.versionscansee.firstuse.service;

import com.google.inject.ImplementedBy;
import com.versionscansee.common.exception.ScanSeeException;

/**
 * The FirstUseService Interface. {@link ImplementedBy}
 * {@link FirstUseServiceImpl}
 * 
 * @author shyamsundara_hm
 */
public interface FirstUseService
{

	/**
	 * The service method for Validating User. This method checks for mandatory
	 * fields, if any mandatory fields are not available returns Insufficient
	 * Data error message else calls the DAO method..
	 * 
	 * @param xml
	 *            The input xml.
	 * @return XML containing success or failure in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	String versionCheck(String xml) throws ScanSeeException;


	/**
	 * The service method for version check for Android. This method checks for mandatory
	 * fields, if any mandatory fields are not available returns Insufficient
	 * Data error message else calls the DAO method.
	 * 
	 * @param xml The input xml.
	 * @return XML containing the output (forcefulUpdate/optionalUpdate status true/false) based on input data (appVersion 1.3.0, 1.5.1, 1.2.0).
	 * @throws ScanSeeException, If any exception occurs ScanSeeException will be thrown.
	 */

	String versionCheckAndroid(String xml) throws ScanSeeException;

	/**
	 * The service method for version check for IOS HubCiti. This method checks for mandatory
	 * fields, if any mandatory fields are not available returns Insufficient
	 * Data error message else calls the DAO method.
	 * 
	 * @param xml The input xml.
	 * @return XML containing the output (forcefulUpdate/optionalUpdate status true/false) based on input data ( HubCiti appVersion 1.1, 2.0, .....).
	 * @throws ScanSeeException, If any exception occurs ScanSeeException will be thrown.
	 */
	String versionCheckHubCiti(String xml) throws ScanSeeException;
}
