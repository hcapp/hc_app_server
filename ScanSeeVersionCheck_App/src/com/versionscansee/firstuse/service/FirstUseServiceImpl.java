package com.versionscansee.firstuse.service;

import static com.versionscansee.common.util.Utility.isEmptyOrNullString;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.versionscansee.common.constants.ApplicationConstants;
import com.versionscansee.common.exception.ScanSeeException;
import com.versionscansee.common.helper.XstreamParserHelper;
import com.versionscansee.common.pojo.AuthenticateUser;
import com.versionscansee.common.util.Utility;
import com.versionscansee.firstuse.dao.FirstUseDAO;

/**
 * The class implements FirstUseService interface and has methods for
 * ProcessFirstUse and UserValidation. The methods of this class are called from
 * the FirstUse Controller Layer. The methods contain call to the FirstUse DAO
 * layer.
 * 
 * @author dileepa_cc
 */

public class FirstUseServiceImpl implements FirstUseService {

	/**
	 * Getting the logger instance.
	 */

	private static final Logger log = LoggerFactory.getLogger(FirstUseServiceImpl.class.getName());

	/**
	 * Instance variable for FirstUseDAO.
	 */

	private FirstUseDAO firstUseDao;

	/**
	 * Setter method for FirstUseDAO.
	 * 
	 * @param firstUseDao
	 *            Instance of type FirstUseDAO.
	 */

	public void setFirstUseDao(FirstUseDAO firstUseDao) {
		this.firstUseDao = firstUseDao;
	}

	/**
	 * The service method for Validating User. This method checks for mandatory
	 * fields, if any mandatory fields are not available returns Insufficient
	 * Data error message else calls the DAO method..
	 * 
	 * @param xml
	 *            The input xml.
	 * @return XML containing success or failure in the response.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@Override
	public String versionCheck(String xml) throws ScanSeeException {
		final String methodName = "versionCheck";
		log.info(ApplicationConstants.METHODSTART + methodName);
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		String response = null;
		AuthenticateUser versinCheckResponse = null;
		final AuthenticateUser authenticateUser = (AuthenticateUser) streamHelper.parseXmlToObject(xml);
		// if (isEmptyString(authenticateUser.getDeviceID()) ||
		// isEmptyOrNullString(authenticateUser.getAppVersion()))
		if (isEmptyOrNullString(authenticateUser.getAppVersion())) {
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		} else {
			versinCheckResponse = firstUseDao.versionCheck(authenticateUser);
			if (versinCheckResponse != null) {
				response = XstreamParserHelper.produceXMlFromObject(versinCheckResponse);
			} else {
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);

			}
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method for for version check for Android. This method checks
	 * for mandatory fields, if any mandatory fields are not available returns
	 * Insufficient Data error message else calls the DAO method.
	 * 
	 * @param xml
	 *            The input xml.
	 * @return XML containing the output (forcefulUpdate/optionalUpdate status
	 *         true/false) based on input data (appVersion 1.3.0, 1.5.1, 1.2.0).
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	@Override
	public String versionCheckAndroid(String xml) throws ScanSeeException {
		final String methodName = "versionCheckAndroid";
		log.info(ApplicationConstants.METHODSTART + methodName);
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		String response = null;
		AuthenticateUser versinCheckResponse = null;
		final AuthenticateUser authenticateUser = (AuthenticateUser) streamHelper.parseXmlToObject(xml);
		if (isEmptyOrNullString(authenticateUser.getAppVersion())) {
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		} else {
			versinCheckResponse = firstUseDao.versionCheckAndroid(authenticateUser);
			if (versinCheckResponse != null) {
				response = XstreamParserHelper.produceXMlFromObject(versinCheckResponse);
			} else {
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		log.info(ApplicationConstants.METHODEND + methodName);
		return response;
	}

	/**
	 * The service method for version check for IOS/Andriod HubCiti. This method
	 * checks for mandatory fields, if any mandatory fields are not available
	 * returns Insufficient Data error message else calls the DAO method.
	 * 
	 * @param xml
	 *            The input xml.
	 * @return XML containing the output (forcefulUpdate status true/false)
	 *         based on input data ( HubCiti appVersion 1.1, 2.0, .....).
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 */

	public String versionCheckHubCiti(String xml) throws ScanSeeException {
		log.info("Inside FirstUseServiceImpl : versionCheckHubCiti");
		final XstreamParserHelper streamHelper = new XstreamParserHelper();
		String response = null;
		AuthenticateUser objAuthent = null;
		final AuthenticateUser authenticateUser = (AuthenticateUser) streamHelper.parseXmlToObject(xml);
		if ("".equals(Utility.checkForNull(authenticateUser.getAppVersion())) || "".equals(Utility.checkForNull(authenticateUser.getPlatform()))
				|| "".equals(Utility.checkForNull(authenticateUser.getHubCitiKey()))) {
			response = Utility.formResponseXml(ApplicationConstants.INSUFFICIENTDATA, ApplicationConstants.INVALIDREQUESTERRORTEXT);
		} else {
			objAuthent = firstUseDao.versionCheckHubCiti(authenticateUser);
			if (objAuthent != null) {
				objAuthent.setResponseCode(ApplicationConstants.SUCCESSCODE);
				objAuthent.setResponseText(ApplicationConstants.SUCCESS);
				response = XstreamParserHelper.produceXMlFromObject(objAuthent);
			} else {
				response = Utility.formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
			}
		}
		return response;
	}
}