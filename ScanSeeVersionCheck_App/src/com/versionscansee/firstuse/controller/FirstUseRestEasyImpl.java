package com.versionscansee.firstuse.controller;

import static com.versionscansee.common.util.Utility.formResponseXml;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.versionscansee.common.constants.ApplicationConstants;
import com.versionscansee.common.exception.ScanSeeException;
import com.versionscansee.common.servicefactory.ServiceFactory;
import com.versionscansee.firstuse.service.FirstUseService;

/**
 * The FirstUse RestEasy has methods to accept First Use requests.These methods
 * are called on the Client request. The method is selected based on the URL
 * Path and the type of request(GET,POST). It invokes the appropriate method in
 * Service layer.
 * 
 * @author dileepa_cc
 */
public class FirstUseRestEasyImpl implements FirstUseRestEasy {
	/**
	 * Getting the logger Instance.
	 */

	private static final Logger LOG = LoggerFactory.getLogger(FirstUseRestEasyImpl.class);
	/**
	 * debugger flag for logging.
	 */
	private static final boolean ISDEBUGENABLED = LOG.isDebugEnabled();

	/**
	 * The FirstUseRestEasy Constructor.
	 */

	public FirstUseRestEasyImpl() {

	}

	/**
	 * This is a RestEasy WebService Method for Authenticate User Login. Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the Valid user or Invalid User/Password in the
	 *         response.
	 */

	@Override
	public String versionCheck(String xml) {
		final String methodName = "versionCheck";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		if (ISDEBUGENABLED) {
			((org.slf4j.Logger) LOG).debug("Recieved XML :" + xml);
		}
		String responseXML = null;
		final FirstUseService firstUse = ServiceFactory.getFirstUseService();
		try {
			responseXML = firstUse.versionCheck(xml);
		} catch (ScanSeeException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			responseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		if (ISDEBUGENABLED) {
			LOG.debug("Returned Response is:" + responseXML);
		}
		return responseXML;
	}

	/**
	 * This is a RestEasyImpl WebService Method for version check for Android.
	 * Method Type:POST.
	 * 
	 * @param xml
	 *            as request.
	 * @return XML XML containing the output (forcefulUpdate/optionalUpdate
	 *         status true/false) based on input data (appVersion 1.3.0, 1.5.1,
	 *         1.2.0).
	 */

	@Override
	public String versionCheckAndroid(String xml) {
		final String methodName = "versionCheckAndroid";
		LOG.info(ApplicationConstants.METHODSTART + methodName);
		String strResponseXML = null;
		final FirstUseService firstUse = ServiceFactory.getFirstUseService();
		try {
			strResponseXML = firstUse.versionCheckAndroid(xml);
		} catch (ScanSeeException exception) {
			LOG.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			strResponseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		LOG.info(ApplicationConstants.METHODEND + methodName);
		return strResponseXML;
	}

	/**
	 * This is a RestEasyImpl WebService Method ffor version check for
	 * IOS/Andriod Hubciti. Method Type:POST.
	 * 
	 * @param xml
	 *            as request.
	 * @return XML containing the output (forcefulUpdate status true/false)
	 *         based on input data ( HubCiti appVersion 1.1, 2.0, .....).
	 */
	public String versionCheckHubCiti(String xml) {
		LOG.info("Inside FirstUseRestEasyImpl : versionCheckHubCiti");
		String strResponseXML = null;
		final FirstUseService firstUse = ServiceFactory.getFirstUseService();
		try {
			strResponseXML = firstUse.versionCheckHubCiti(xml);
		} catch (ScanSeeException exception) {
			LOG.error("Inside FirstUseRestEasyImpl : versionCheckHubCiti : " + exception);
			strResponseXML = formResponseXml(ApplicationConstants.TECHNICALPROBLEMERRORCODE, ApplicationConstants.TECHNICALPROBLEMERRORTEXT);
		}
		return strResponseXML;
	}
}
