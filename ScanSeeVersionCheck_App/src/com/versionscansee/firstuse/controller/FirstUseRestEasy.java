package com.versionscansee.firstuse.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import static com.versionscansee.common.constants.ScanSeeURLPath.FIRSTUSEBASEURL;
import static com.versionscansee.common.constants.ScanSeeURLPath.VERSIONCHECK;
import static com.versionscansee.common.constants.ScanSeeURLPath.VERSIONCHECKANDROID;
import static com.versionscansee.common.constants.ScanSeeURLPath.VERSIONCHECKHUBCITI;
import com.google.inject.ImplementedBy;

/**
 * The FirstUseRestEasy Interface. {@link ImplementedBy}
 * {@link FirstUseRestEasyImpl}
 * 
 * @author dileepa_cc
 */
@Path(FIRSTUSEBASEURL)
public interface FirstUseRestEasy {

	/**
	 * This is a RestEasy WebService Method for Authenticate User Login. Method
	 * Type:POST.
	 * 
	 * @param xml
	 *            as request
	 * @return XML containing the success or failure in the response.
	 */

	@POST
	@Path(VERSIONCHECK)
	@Produces("text/xml")
	@Consumes("text/xml")
	String versionCheck(String xml);

	/**
	 * This is a RestEasy WebService Method for version check for Android.
	 * Method Type:POST.
	 * 
	 * @param xml
	 *            as request.
	 * @return XML containing the output (forcefulUpdate/optionalUpdate status
	 *         true/false) based on input data (appVersion 1.3.0, 1.5.1, 1.2.0).
	 */

	@POST
	@Path(VERSIONCHECKANDROID)
	@Produces("text/xml")
	@Consumes("text/xml")
	String versionCheckAndroid(String xml);

	/**
	 * This is a RestEasy WebService Method for version check for IOS/Andriod
	 * Hubciti. Method Type:POST.
	 * 
	 * @param xml
	 *            as request.
	 * @return XML containing the output (forcefulUpdate status true/false)
	 *         based on input data ( HubCiti appVersion 1.1, 2.0, .....).
	 */

	@POST
	@Path(VERSIONCHECKHUBCITI)
	@Produces("text/xml")
	@Consumes("text/xml")
	String versionCheckHubCiti(String xml);
}
