package com.versionscansee.firstuse.dao;

import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import com.versionscansee.common.constants.ApplicationConstants;
import com.versionscansee.common.exception.ScanSeeException;
import com.versionscansee.common.pojo.AuthenticateUser;

/**
 * The implementation class for FirstUseDAO. Implements the FirstUseDAO. This
 * class for inserting user registration parameters,finding user exists and
 * validating user. The methods of this class are called from the FirstUse
 * Service layer.
 * 
 * @author shyamsundara_hm
 */
public class FirstUseDAOImpl implements FirstUseDAO {
	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 */
	private static final Logger log = LoggerFactory.getLogger(FirstUseDAOImpl.class);
	/**
	 * For JDBC connection.
	 */
	private JdbcTemplate jdbcTemplate;
	/**
	 * Getting the SimpleJdbcTemplate Instance.
	 */
	private SimpleJdbcTemplate simpleJdbcTemplate;
	/**
	 * To call the StoredProcedure.
	 */
	private SimpleJdbcCall simpleJdbcCall;

	/**
	 * To set the dataSource to jdbcTemplate...
	 * 
	 * @param dataSource
	 *            from DataSource
	 */
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);

	}

	/**
	 * The DAO method for Authenticate login to the database for the based on
	 * the given userName and password.
	 * 
	 * @param userName
	 *            as request parameter.
	 * @param password
	 *            as request parameter.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return AuthenticateUser object.
	 */

	@Override
	public AuthenticateUser versionCheck(AuthenticateUser authenticateUserObj) throws ScanSeeException {
		final String methodName = "versionCheck";
		log.info(ApplicationConstants.METHODSTART + methodName);

		AuthenticateUser userInfo = null;
		try {

			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_CheckDeviceAppVersion");
			final MapSqlParameterSource versionCheckParams = new MapSqlParameterSource();
			versionCheckParams.addValue("DeviceID", authenticateUserObj.getDeviceID());
			versionCheckParams.addValue("AppVersion", authenticateUserObj.getAppVersion());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(versionCheckParams);
			if (null != resultFromProcedure) {

				// checking user has latest app version,If he has older
				// version then sending url and message to download new version
				// forcefully.

				userInfo = new AuthenticateUser();

				// For Optinal flag
				final Boolean optionalUpdateVersionPrompt = (Boolean) resultFromProcedure.get("OptionalUpdateVersionPrompt");

				if (optionalUpdateVersionPrompt != null) {
					if (optionalUpdateVersionPrompt) {

						String latestVersionPath = (String) resultFromProcedure.get("LatestAppVersionURL");
						userInfo.setOptionalUpdateVersionFlag(true);
						userInfo.setUpdateVersionText(ApplicationConstants.LATESTAPPVERSIONDOWNLOADTEXT);
						userInfo.setLatestAppVerUrl(latestVersionPath);
					} else {
						userInfo.setOptionalUpdateVersionFlag(false);
					}

				}
				// For Forceful flag

				final Boolean forcefulUpdateVersionPrompt = (Boolean) resultFromProcedure.get("ForcefulUpdateVersionPrompt");

				if (forcefulUpdateVersionPrompt != null) {
					if (forcefulUpdateVersionPrompt) {

						String latestVersionPath = (String) resultFromProcedure.get("LatestAppVersionURL");
						userInfo.setForcefulUpdateVersionFlag(true);
						userInfo.setUpdateVersionText(ApplicationConstants.FORCEFULAPPVERSIONDOWNLOADTEXT);
						userInfo.setLatestAppVerUrl(latestVersionPath);
					} else {
						userInfo.setForcefulUpdateVersionFlag(false);
					}

				}
			}
		} catch (DataAccessException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());
		}

		return userInfo;
	}

	/**
	 * The DAO method for Authenticate login to the database for the based on
	 * the given userName and password (for version check for Android).
	 * 
	 * @param userName
	 *            as request parameter.
	 * @param password
	 *            as request parameter.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return AuthenticateUser object (forcefulUpdate/optionalUpdate status
	 *         true/false based on input data appVersion 1.3.0, 1.5.1, 1.2.0).
	 */

	@Override
	public AuthenticateUser versionCheckAndroid(AuthenticateUser authenticateUserObj) throws ScanSeeException {
		final String methodName = "versionCheckAndroid";
		log.info(ApplicationConstants.METHODSTART + methodName);
		AuthenticateUser objAuthenUser = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_CheckDeviceAppVersionAndroid");
			final MapSqlParameterSource versionCheckParams = new MapSqlParameterSource();
			versionCheckParams.addValue("DeviceID", authenticateUserObj.getDeviceID());
			versionCheckParams.addValue("AppVersion", authenticateUserObj.getAppVersion());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(versionCheckParams);
			if (null != resultFromProcedure) {
				objAuthenUser = new AuthenticateUser();
				// For Optinal flag
				final Boolean optionalUpdateVersionPrompt = (Boolean) resultFromProcedure.get("OptionalUpdateVersionPrompt");
				if (optionalUpdateVersionPrompt != null) {
					if (optionalUpdateVersionPrompt) {
						String strLatVersionPath = (String) resultFromProcedure.get("LatestAppVersionURL");
						objAuthenUser.setOptionalUpdateVersionFlag(true);
						objAuthenUser.setUpdateVersionText(ApplicationConstants.LATESTAPPVERSIONDOWNLOADTEXT);
						objAuthenUser.setLatestAppVerUrl(strLatVersionPath);
					} else {
						objAuthenUser.setOptionalUpdateVersionFlag(false);
					}
				}
				// For Forceful flag

				final Boolean forcefulUpdateVersionPrompt = (Boolean) resultFromProcedure.get("ForcefulUpdateVersionPrompt");
				if (forcefulUpdateVersionPrompt != null) {
					if (forcefulUpdateVersionPrompt) {

						String latestVersionPath = (String) resultFromProcedure.get("LatestAppVersionURL");
						objAuthenUser.setForcefulUpdateVersionFlag(true);
						objAuthenUser.setUpdateVersionText(ApplicationConstants.FORCEFULAPPVERSIONDOWNLOADTEXT);
						objAuthenUser.setLatestAppVerUrl(latestVersionPath);
					} else {
						objAuthenUser.setForcefulUpdateVersionFlag(false);
					}
				}
			}
		} catch (DataAccessException exception) {
			log.error(ApplicationConstants.EXCEPTIONOCCURRED + methodName, exception);
			throw new ScanSeeException(exception.getMessage());
		}
		return objAuthenUser;
	}

	/**
	 * The DAO method for Authenticate login to the database for the based on
	 * the given userName and password (for version check for IOS/Andriod).
	 * 
	 * @param userName
	 *            as request parameter.
	 * @param password
	 *            as request parameter.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return AuthenticateUser object (forcefulUpdate status true/false) based
	 *         on input data ( HubCiti appVersion 1.1, 2.0, .....).
	 */

	public AuthenticateUser versionCheckHubCiti(AuthenticateUser authenticateUserObj) throws ScanSeeException {
		log.info("Inside FirstUseDAOImpl : versionCheckHubCiti");
		AuthenticateUser objAuthenUser = null;
		String latestIOSURLPath = null;
		String latestAndriodURLPath = null;
		try {
			simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate);
			simpleJdbcCall.setSchemaName(ApplicationConstants.SCHEMANAME);
			simpleJdbcCall.withProcedureName("usp_HcCheckDeviceAppVersion");
			final MapSqlParameterSource versionCheckParams = new MapSqlParameterSource();
			versionCheckParams.addValue("RequestPlatformType", authenticateUserObj.getPlatform());
			versionCheckParams.addValue("AppVersion", authenticateUserObj.getAppVersion());
			versionCheckParams.addValue("HubCitiKey", authenticateUserObj.getHubCitiKey());
			final Map<String, Object> resultFromProcedure = simpleJdbcCall.execute(versionCheckParams);
			if (null != resultFromProcedure) {
				objAuthenUser = new AuthenticateUser();
				// For Forceful flag
				final Boolean forcefulUpdateVersionPrompt = (Boolean) resultFromProcedure.get("ForcefulUpdateVersionPrompt");
				if (forcefulUpdateVersionPrompt != null) {
					if (forcefulUpdateVersionPrompt) {
						latestIOSURLPath = (String) resultFromProcedure.get("DownLoadLinkIOS");
						latestAndriodURLPath = (String) resultFromProcedure.get("DownLoadLinkAndroid");
						objAuthenUser.setForcefulUpdateVersionFlag(true);
						objAuthenUser.setUpdateVersionText(ApplicationConstants.FORCEFULAPPVERSIONDOWNLOADTEXT);
						objAuthenUser.setLatestAppVerUrl(latestIOSURLPath);
						objAuthenUser.setLatAndriodVerUrl(latestAndriodURLPath);
					} else {
						objAuthenUser.setForcefulUpdateVersionFlag(false);
					}
				}
			}
		} catch (DataAccessException exception) {
			log.error("Inside FirstUseDAOImpl : versionCheckHubCiti : " + exception);
			throw new ScanSeeException(exception.getMessage());
		}
		return objAuthenUser;
	}
}
