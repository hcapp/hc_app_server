package com.versionscansee.firstuse.dao;

import com.versionscansee.common.exception.ScanSeeException;
import com.versionscansee.common.pojo.AuthenticateUser;

/**
 * The interface implemented by FirstUseDAOImpl class for inserting user
 * registration details,finding user exists and validating user. ImplementedBy
 * {@link FirstUseDAOImpl}
 * 
 * @author shyamsundara_hm
 */

public interface FirstUseDAO
{

	/**
	 * The DAO method for Authenticate login to the database for the based on
	 * the given userName and password.
	 * 
	 * @param userName
	 *            as request parameter.
	 * @param password
	 *            as request parameter.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return AuthenticateUser object.
	 */

	AuthenticateUser versionCheck(AuthenticateUser authenticateUserObj) throws ScanSeeException;


	/**
	 * The DAO method for Authenticate login to the database for the based on
	 * the given userName and password(for version check for Android).
	 * 
	 * @param userName
	 *            as request parameter.
	 * @param password
	 *            as request parameter.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return AuthenticateUser object (forcefulUpdate/optionalUpdate status true/false based on input data appVersion 1.3.0, 1.5.1, 1.2.0).
	 */

	AuthenticateUser versionCheckAndroid(AuthenticateUser authenticateUserObj) throws ScanSeeException;

	/**
	 * The DAO method for Authenticate login to the database for the based on
	 * the given userName and password(version check for IOS/Andriod  HubCiti.).
	 * 
	 * @param userName
	 *            as request parameter.
	 * @param password
	 *            as request parameter.
	 * @throws ScanSeeException
	 *             If any exception occurs ScanSeeException will be thrown.
	 * @return AuthenticateUser object (forcefulUpdate status true/false based on input data ( HubCiti appVersion 1.1, 2.0, .....).
	 */

	AuthenticateUser versionCheckHubCiti(AuthenticateUser authenticateUserObj) throws ScanSeeException;

}
