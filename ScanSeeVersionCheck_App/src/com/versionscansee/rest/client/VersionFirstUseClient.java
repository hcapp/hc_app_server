package com.versionscansee.rest.client;

import javax.ws.rs.core.MediaType;

import org.jboss.resteasy.client.ClientRequest;

public class VersionFirstUseClient {

	public static void main(String[] args) {
		executeFirstUse();
	}

	public static void executeFirstUse() {
		// testLogin();
//		 testAndriodLogin();
		testHubcitiLogin();
	}

	public static void testLogin() {
		// ><AuthenticateUser><deviceID>78b44d2204eefbe3a0dd13f251c71ac8ae219968</deviceID><appVersion>1.3.0</appVersion></AuthenticateUser>

		final String logon = "http://199.36.142.83:8080/ScanSeeVersionCheck/firstUse/versioncheck";
		// final String logon =
		// "http://localhost:8080/ScanSee/firstUse/authenticate";
		final ClientRequest request = new ClientRequest(logon);
		final StringBuilder sb = new StringBuilder();
		sb.append("<AuthenticateUser>");
		sb.append("<appVersion>1.2</appVersion>");
		sb.append("<deviceID>78b44d2204eefbe3a0dd13f251c71ac8ae219968</deviceID>");
		sb.append("</AuthenticateUser>");
		request.accept("text/xml").body(MediaType.TEXT_XML, sb.toString());
		request.getHeaders();
		try {
			final String response = request.postTarget(String.class);
			System.out.println("response in Login" + response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testAndriodLogin() {
		// final String logon =
		// "https://app.scansee.net/ScanSeeVersionCheck/firstUse/versioncheckAndroid";
		final String logon = "http://199.36.142.83:8080/ScanSeeVersionCheck/firstUse/versioncheckAndroid";
		final ClientRequest request = new ClientRequest(logon);
		final StringBuilder sb = new StringBuilder();
		sb.append("<AuthenticateUser>");
		sb.append("<appVersion>1.2.0</appVersion>");
		sb.append("<deviceID>94D77E5E-378C-52CF-B36663E02-3A3980B6D06E</deviceID>");
		sb.append("</AuthenticateUser>");
		request.accept("text/xml").body(MediaType.TEXT_XML, sb.toString());
		request.getHeaders();
		try {
			final String response = request.postTarget(String.class);
			System.out.println("response in Login" + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void testHubcitiLogin() {
		// final String logon =
		// "https://app.scansee.net/ScanSeeVersionCheck/firstUse/versioncheckhubciti";
		//final String logon = "http://localhost:8080/ScanSeeVersionCheck/firstUse/versioncheckhubciti";
		final String logon = "https://app.scansee.net/ScanSeeVersionCheck/firstUse/versioncheckhubciti";
		final ClientRequest request = new ClientRequest(logon);
		final StringBuilder sb = new StringBuilder();
		sb.append("<AuthenticateUser>");
		sb.append("<appVersion>1.2</appVersion>");
		sb.append("<platform>IOS</platform>");// IOS,Andriod
		sb.append("<hubCitiKey>Marble Falls10</hubCitiKey>");// IOS,Andriod
		sb.append("</AuthenticateUser>");
		request.accept("text/xml").body(MediaType.TEXT_XML, sb.toString());
		request.getHeaders();
		try {
			final String response = request.postTarget(String.class);
			System.out.println("response in Login" + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}