package com.versionscansee.rest.controllers.urimappings;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.versionscansee.firstuse.controller.FirstUseRestEasyImpl;

/**
 * The class extends javax.ws.rs.core.Application class. The RestEasy Services
 * are instantiated here. This class needs to be defined in web.xml
 * 
 * @author murali_pnvb
 */

public class AttachRestEasyControllersToURI extends Application
{

	/**
	 * Getting the Logger Instance.
	 */

	private static final Logger log = LoggerFactory.getLogger(AttachRestEasyControllersToURI.class.getName());

	/**
	 * Getting Set instance.
	 */
	private Set<Object> singletons = new HashSet<Object>();

	/**
	 * Getting Set instance.
	 */
	private Set<Class<?>> empty = new HashSet<Class<?>>();

	/**
	 * The Constructor which calls InitializeServices method.
	 */

	public AttachRestEasyControllersToURI()
	{
		log.info("Initalizing the Resteasy");
		initializeServices();
	}

	/**
	 * This method is used to Get a set of root resource and provider classes.
	 * 
	 * @return empty -a set of root resource and provider instances
	 */
	@Override
	public Set<Class<?>> getClasses()
	{
		return empty;
	}

	/**
	 * This method is used to get singleton objects.
	 * 
	 * @return singletons -a set of root resource and provider instances
	 */
	@Override
	public Set<Object> getSingletons()
	{
		return singletons;
	}

	/**
	 * Initialize the Bean.xml and Data source configuration instantiating the
	 * RestEasy Services.
	 */
	private void initializeServices()
	{
		/*
		 * Initialize the Bean.xml and Data source configuration instantiating
		 * the RestEasy Services.
		 */
		singletons.add(new FirstUseRestEasyImpl());

	}

}
